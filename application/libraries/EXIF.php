<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EXIF {

	public $data = [];

    //画像のExif情報を取得し、クラス「ImageInfo」に収納
    public function get_image_info($filename) {

        $exif = exif_read_data($filename, 0, true);

        if (isset($exif['FILE']) && isset($exif['FILE']['FileName'])) {
            $this->data['FileName'] = $exif['FILE']['FileName'];
        }
        if (isset($exif['IFD0'])) {
            if (isset($exif['IFD0']['Make'])) {
                $this->data['Make'] = $exif['IFD0']['Make'];
            }
            if (isset($exif['IFD0']['Model'])) {
                $this->data['Model'] = $exif['IFD0']['Model'];
            }
        }
        if (isset($exif['EXIF'])) {
            if (isset($exif['EXIF']['DateTimeOriginal'])) {
                $this->data['DateTimeOriginal'] = $exif['EXIF']['DateTimeOriginal'];
            }
        }
        if (isset($exif['GPS'])) {
            $gps = $exif['GPS'];
            //緯度
            if (isset($gps['GPSLatitude']) && isset($gps['GPSLatitudeRef'])) {
                $lat = $gps['GPSLatitude'];
                $lat_d = explode("/", $lat[0]); //度
                $lat_m = explode("/", $lat[1]); //分
                $lat_s = explode("/", $lat[2]); //秒
                //60進数→10進数
                $this->data['Lat'] = intval($lat_d[0]) / intval($lat_d[1]) + (intval($lat_m[0]) / intval($lat_m[1])) / 60 + (intval($lat_s[0]) / intval($lat_s[1])) / 3600;
                if ($gps['GPSLatitudeRef'] == "S") {
                    $this->data['Lat'] = $this->data['Lat'] * -1; //南半球の場合マイナスにする
                }
            }
            //経度
            if (isset($gps['GPSLongitude']) && isset($gps['GPSLongitudeRef'])) {
                $lng = $gps['GPSLongitude'];
                $lng_d = explode("/", $lng[0]); //度
                $lng_m = explode("/", $lng[1]); //分
                $lng_s = explode("/", $lng[2]); //秒
                //60進数→10進数
                $this->data['Lng'] = intval($lng_d[0]) / intval($lng_d[1]) + (intval($lng_m[0]) / intval($lng_m[1])) / 60 + (intval($lng_s[0]) / intval($lng_s[1])) / 3600;
                if ($gps['GPSLongitudeRef'] == "W") {
                    $this->data['Lng'] = $this->data['Lng'] * -1; //西経の場合マイナスにする
                }
            }
            if (isset($gps['GPSImgDirection'])) {
                $degreeArray = explode("/", $gps['GPSImgDirection']);
                $degree = intval($degreeArray[0]) / intval($degreeArray[1]);
                //$this->data['Direction'] = getDirection($degree);
                $this->data['Direction'] = $degree;
            }
        }
        if ($this->data['FileName'] && !empty($this->data['Lat']) && !empty($this->data['Lng']) && abs($this->data['Lat']) <= 90 && abs($this->data['Lng']) <= 180) {
            return $this->data;
        } else {
            return false;
        }
    }

    //方向データ（北からの時計回りの角度）を16方位テキストに変換
    private function getDirection($degree) {
        $directionArray = array('北', '北北東', '北東', '東北東', '東', '東南東', '南東', '南南東',
            '南', '南南西', '南西', '西南西', '西', '西北西', '北西', '北北西');
        $directionNo = (int) (($degree + 11.25) / 22.5) % 16;
        return $directionArray[$directionNo];
    }

}
