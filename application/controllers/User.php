<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    private $code = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(['EXIF']);
        if(!isset($_SESSION["username"]))
        {
            redirect("user_login");
        }
        $this->config->set_item('language', 'japanese');
    }

    public function record_action($menu, $action)
    {
        $logs = 
        [
            "username" => $_SESSION["username"],
            "menu" => $menu,
            "action" => $action,
            "access" => $_SESSION["access"],
        ];
        $this->load->model('menu_logs_model',"menu_logs");
        $this->menu_logs->insert_log($logs);
    }

    public function index()
    {   
        $ehe['afiliate_name'] = $_SESSION["affilate_name"];
        $data['username'] = $_SESSION["username"];
        // $data['access_login'] = $_SESSION["access"];

        // print_r($ehe);die();

        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["customers"] = $this->customer->fetch_all_construction_by($ehe['afiliate_name']);
        $this->load->view('user/dashboard/dashuser_v',$data);
    }

    public function dashboard()
    {
        $ehe['afiliate_name'] = $_SESSION["affilate_name"];
        $data['username'] = $_SESSION["username"];
        // $data['access_login'] = $_SESSION["access"];

        // print_r($ehe['afiliate_name']);die();

        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["customers"] = $this->customer->fetch_all_construction_by($ehe['afiliate_name']);
        $this->load->view('user/dashboard/dashuser_v',$data);
    }

    public function menu()
    {
        // $data['access_login'] = $_SESSION["access"];
        $data['username'] = $_SESSION["username"];
        $data['aff'] = $_SESSION["affilate_name"];

        $need = $this->input->post("customer");
        $splits = explode(";",$need);

        $data['customer'] = $splits[1];
        
        $sess_array = array(
            'const_id' => $splits[0],
        );
        $this->session->set_userdata($sess_array);

        $data['btn_menu'] = $this->input->post("btn_menu");

        if(!empty($data['customer'])){

        }
        else{
            redirect('user');
        }

        if($data['btn_menu'] == "list"){
            $this->record_action("project_list", "view");
            $this->load->model('customer_model', "customer");

            $ehe['afiliator'] = $_SESSION["affilate_name"];
            $data['file_project'] = $this->customer->view_file_project($ehe,$splits[0]);


            // echo json_encode($data, JSON_UNESCAPED_UNICODE);
            $this->record_action("list_file", "view");
            $this->load->view('user/dashboard/file_list_v',$data);

        }
        else
        {
            $_SESSION['customer'] = $data['customer'];
            $this->record_action("select_category", "view");
            $this->load->view('user/dashboard/category_v',$data);
        }
    }


    public function download($file)
    {
        // $this->load->helper('download');

        // $this->load->helper('download');
        // $path = file_get_contents(base_url()."uploads/files/".$file); // get file name
        // force_download($file, $path); // start download`

        $path = FCPATH."uploads/files/".urldecode($file); // get file name
        //get the file extension
        $info = new SplFileInfo($path);
        // var_dump($info->getExtension());


        if (file_exists($path)) {
            header('Content-Description: Download Files');
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename="' . basename(urldecode($path)).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();
            readfile($path);
        }
    }

    public function view_filed($file)
    {

        $path = FCPATH."uploads/files/".urldecode($file); // get file name
        //get the file extension
        $info = new SplFileInfo($path);
        // var_dump($info->getExtension());


        if (file_exists($path)) {
            header('Content-Description: View Files');
            header('Content-Type: application/pdf');
            header('Content-Disposition: inline; filename="' . basename(urldecode($path)).'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            ob_clean();
            flush();
            readfile($path);
        }
    }


    public function select_category($code = NULL)
    {
        $data['username'] = $_SESSION["username"];
        $min = '1001';
        $max = '1016';

        if(($min <= $code) && ($code <= $max))
        {
            $data['customer'] = $_SESSION['customer'];
            $data['const_id'] = $_SESSION['const_id'];
            $data['code_project'] = $code;

            $this->load->model('customer_model', "customer");
            $data["list_category"] = $this->customer->get_project_category($code)[0];
            $data["category_item"] = $this->customer->get_categoryitem($code);
            $job_list = $this->customer->get_projectjoblist($code);

            if($code == '1001'){
                $filterBy1 = '11001';
                $filterBy2 = '11002';
            
                $data['dropdown1'] = array_filter($job_list, function ($var) use ($filterBy1) {
                    return ($var['id_item'] == $filterBy1);
                });

                $data['dropdown2'] = array_filter($job_list, function ($var) use ($filterBy2) {
                    return ($var['id_item'] == $filterBy2);
                });
            }

            if($code == '1002'){
                $filterBy3 = '11003';
            
                $data['dropdown3'] = array_filter($job_list, function ($var) use ($filterBy3) {
                    return ($var['id_item'] == $filterBy3);
                });

            }

            if($code == '1003'){
                $filterBy4 = '11004';
                $filterBy5 = '11005';
                $filterBy6 = '11006';
            
                $data['dropdown4'] = array_filter($job_list, function ($var) use ($filterBy4) {
                    return ($var['id_item'] == $filterBy4);
                });

                $data['dropdown5'] = array_filter($job_list, function ($var) use ($filterBy5) {
                    return ($var['id_item'] == $filterBy5);
                });

                $data['dropdown6'] = array_filter($job_list, function ($var) use ($filterBy6) {
                    return ($var['id_item'] == $filterBy6);
                });
            }

            if($code == '1004'){
                $filterBy7 = '11007';
                $filterBy8 = '11008';
            
                $data['dropdown7'] = array_filter($job_list, function ($var) use ($filterBy7) {
                    return ($var['id_item'] == $filterBy7);
                });

                $data['dropdown8'] = array_filter($job_list, function ($var) use ($filterBy8) {
                    return ($var['id_item'] == $filterBy8);
                });
            }

            if($code == '1005'){
                $filterBy9 = '11009';
                $filterBy10 = '11010';
            
                $data['dropdown9'] = array_filter($job_list, function ($var) use ($filterBy9) {
                    return ($var['id_item'] == $filterBy9);
                });

                $data['dropdown10'] = array_filter($job_list, function ($var) use ($filterBy10) {
                    return ($var['id_item'] == $filterBy10);
                });
            }

            if($code == '1006'){
                $filterBy11 = '11011';
            
                $data['dropdown11'] = array_filter($job_list, function ($var) use ($filterBy11) {
                    return ($var['id_item'] == $filterBy11);
                });

            }

            if($code == '1007'){
                $filterBy12 = '11012';
            
                $data['dropdown12'] = array_filter($job_list, function ($var) use ($filterBy12) {
                    return ($var['id_item'] == $filterBy12);
                });
            }

            if($code == '1008'){
                $filterBy13 = '11013';
            
                $data['dropdown13'] = array_filter($job_list, function ($var) use ($filterBy13) {
                    return ($var['id_item'] == $filterBy13);
                });

            }

            if($code == '1009'){
                $filterBy14 = '11014';
            
                $data['dropdown14'] = array_filter($job_list, function ($var) use ($filterBy14) {
                    return ($var['id_item'] == $filterBy14);
                });

            }

            if($code == '1010'){
                $filterBy15 = '11015';
                $filterBy16 = '11016';
                $filterBy17 = '11017';
                $filterBy18 = '11018';
            
                $data['dropdown15'] = array_filter($job_list, function ($var) use ($filterBy15) {
                    return ($var['id_item'] == $filterBy15);
                });

                $data['dropdown16'] = array_filter($job_list, function ($var) use ($filterBy16) {
                    return ($var['id_item'] == $filterBy16);
                });

                $data['dropdown17'] = array_filter($job_list, function ($var) use ($filterBy17) {
                    return ($var['id_item'] == $filterBy17);
                });

                $data['dropdown18'] = array_filter($job_list, function ($var) use ($filterBy18) {
                    return ($var['id_item'] == $filterBy18);
                });
            }

            if($code == '1011'){
                $filterBy19 = '11019';
                $filterBy20 = '11020';
                $filterBy21 = '11021';
            
                $data['dropdown19'] = array_filter($job_list, function ($var) use ($filterBy19) {
                    return ($var['id_item'] == $filterBy19);
                });

                $data['dropdown20'] = array_filter($job_list, function ($var) use ($filterBy20) {
                    return ($var['id_item'] == $filterBy20);
                });

                
                $data['dropdown21'] = array_filter($job_list, function ($var) use ($filterBy21) {
                    return ($var['id_item'] == $filterBy21);
                });
            }

            if($code == '1012'){
                $filterBy22 = '11022';
            
                $data['dropdown22'] = array_filter($job_list, function ($var) use ($filterBy22) {
                    return ($var['id_item'] == $filterBy22);
                });

            }

            if($code == '1013'){
                $filterBy23 = '11023';

                $data['dropdown23'] = array_filter($job_list, function ($var) use ($filterBy23) {
                    return ($var['id_item'] == $filterBy23);
                });
            }

            if($code == '1014'){
                $filterBy24 = '11024';

                $data['dropdown24'] = array_filter($job_list, function ($var) use ($filterBy24) {
                    return ($var['id_item'] == $filterBy24);
                });
            }

            if($code == '1015'){
                $filterBy25 = '11025';

                $data['dropdown25'] = array_filter($job_list, function ($var) use ($filterBy25) {
                    return ($var['id_item'] == $filterBy25);
                });
            }

            if($code == '1016'){
                $filterBy26 = '11026';
  
                $data['dropdown26'] = array_filter($job_list, function ($var) use ($filterBy26) {
                    return ($var['id_item'] == $filterBy26);
                });
            }

            $this->record_action("select_category_item", "view");
            $this->load->view('user/dashboard/selection_v',$data);

            // echo json_encode($data["category_item"],JSON_UNESCAPED_UNICODE);
        }
        else
        {
            redirect('user/menu');
        }
    }


    public function photo_selection()
    {
        $data['username'] = $_SESSION["username"];

        if($this->input->post('edit_btn') == 'back')
        {
            $data['customer'] = $this->input->post("customer");
            $this->record_action("select_category", "view");
            $this->load->view('user/dashboard/category_v',$data);
        }
        else
        {
   
            $data['customer'] = $this->input->post("customer");
            $data['code_project'] = $this->input->post("code_project");
            $data['color'] = $this->input->post("color");
            $data['const_id'] = $this->input->post("const_id");

            $code = $this->input->post("code_project");

            $this->load->model('customer_model', "customer");
            $data["category_name"] = $this->input->post("category_name");
            
            if($code == '1001'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1002'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
    
            }
    
            if($code == '1003'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
                if(!empty($this->input->post("selection3"))) {
                    $data1 = explode(";",$this->input->post("selection3"));
                }
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1004'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1005'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1006'){
                $data1 = explode(";",$this->input->post("selection1"));
                
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
    
            }
    
            if($code == '1007'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1008'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1009'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1010'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
                if(!empty($this->input->post("selection3"))) {
                    $data1 = explode(";",$this->input->post("selection3"));
                }
                if(!empty($this->input->post("selection4"))) {
                    $data1 = explode(";",$this->input->post("selection4"));
                }
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1011'){
                if(!empty($this->input->post("selection1"))) {
                    $data1 = explode(";",$this->input->post("selection1"));
                }
                if(!empty($this->input->post("selection2"))) {
                    $data1 = explode(";",$this->input->post("selection2"));
                }
                if(!empty($this->input->post("selection3"))) {
                    $data1 = explode(";",$this->input->post("selection3"));
                }
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1012'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1013'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1014'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1015'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            if($code == '1016'){
                $data1 = explode(";",$this->input->post("selection1"));
    
                $data['selection1'] = $data1[0];
                $data['joblist_files'] = $data1[1];
            }
    
            $this->record_action("select_category_item", "view");
            $this->load->view('user/dashboard/photo_upload_v',$data);
        }
     
    }

    public function check_data()
    {
        $data['username'] = $_SESSION["username"];
        
        $fileupload = $this->input->post("upload_files");

        if($this->input->post('edit_btn') == 'back')
        {
            $data['customer'] = $this->input->post("customer");
            $data["category_name"] = $this->input->post("category_name");
            $data['code_project'] = $this->input->post("code_project");
            $data['color'] = $this->input->post("color");
            $data['const_id'] = $this->input->post("const_id");
            // $data['joblist_files'] = $this->input->post("joblist_files");
            // $data['selection1'] = $this->input->post("selection1");
            // $data['selection2'] = $this->input->post("selection2");
            // $data['selection3'] = $this->input->post("selection3");
            // $data['selection4'] = $this->input->post("selection4");
            $this->record_action("select_category", "view");
            // $this->load->view('user/dashboard/selection_v',$data);
            redirect('user/select_category/'.$data['code_project'],$data);
        }
        else
        {
            if(!empty($_FILES['photo']['size']))
            {
                if($fileupload == '1')
                {
                    $data_photo1['username'] = $_SESSION["username"];
                    $data_photo1['customer'] = $this->input->post("customer");
                    $data_photo1["category_name"] = $this->input->post("category_name");
                    $data_photo1['code_project'] = $this->input->post("code_project");
                    $data_photo1['color'] = $this->input->post("color");
                    $data_photo1['joblist_files'] = $this->input->post("joblist_files");
                    $data_photo1['const_id'] = $this->input->post("const_id");  
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_photo1['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_photo1['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_photo1['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_photo1['selection1'] = $this->input->post("selection4");
                    }
                    

                    if(!empty($this->input->post("selection_name"))) {
                        $data_photo1['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_photo1['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_photo1['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_photo1['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['photo']['size']!=0)
                    // {
                    //     $file_photo1 = $this->exif->get_image_info($_FILES['photo']['tmp_name']);
                    //     $data_photo1['latlng1'] = $file_photo1["Lat"].';'.$file_photo1["Lng"];
                    // }
                    // $data_photo1['photo_images1'] = $_FILES['photo']['name'];

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_photo1['latlng1'] = $lat1.';'.$lat2;

                    $data_photo1['lat'] = $lat1;
                    $data_photo1['lang'] = $lat2;

                    // if(empty($lat1) || empty($lat2))
                    if(empty($_FILES['photo']['size']))
                    {
                        $data_photo1['username'] = $_SESSION["username"];
                        $data_photo1['customer'] = $this->input->post("customer");
                        $data_photo1["category_name"] = $this->input->post("category_name");
                        $data_photo1['code_project'] = $this->input->post("code_project");
                        $data_photo1['color'] = $this->input->post("color");
                        $data_photo1['joblist_files'] = $this->input->post("joblist_files");
                        $data_photo1['showing'] = "show";
                        $data_photo1['lat'] = $lat1;
                        $data_photo1['lang'] = $lat2;  
                        $data_photo1['const_id'] = $this->input->post("const_id");  
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_photo1);
                    }
                    else
                    {
                        
                        $config['upload_path'] = APPPATH.'../uploads/images'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size'] = '50000'; // in KlioBytes

                        $this->load->library('upload',$config); 
                        if ( ! $this->upload->do_upload('photo'))
                        {
                            print_r($this->upload->display_errors());
                            die();
                        }
                        else
                        {
                            $uploadData1 = $this->upload->data();
                            $filename_photo = $uploadData1['file_name'];
                            $data_photo1["photo_images1"]= $filename_photo;
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_photo1);

                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_photo1);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_photo1);
                        }

                        // echo json_encode($insert, JSON_UNESCAPED_UNICODE);
                    }
                }

                if($fileupload == '2')
                {
                    $data_photo2['username'] = $_SESSION["username"];
                    $data_photo2['customer'] = $this->input->post("customer");
                    $data_photo2["category_name"] = $this->input->post("category_name");
                    $data_photo2['code_project'] = $this->input->post("code_project");
                    $data_photo2['color'] = $this->input->post("color");
                    $data_photo2['joblist_files'] = $this->input->post("joblist_files");
                    $data_photo2['const_id'] = $this->input->post("const_id");  
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_photo2['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_photo2['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_photo2['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_photo2['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_photo2['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_photo2['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_photo2['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_photo2['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['photo']['size']!=0)
                    // {
                    //     for ($i=0; $i < 2; $i++) {
                    //         $file_photo1 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file_photo2 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['photo']['name'][$i];
                    //     }
                    // }

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_photo2['latlng1'] = $lat1.';'.$lat2;
                    $data_photo2['latlng2'] = $lat1.';'.$lat2;

                    $data_photo2['lat'] = $lat1;
                    $data_photo2['lang'] = $lat2;

                    // if(empty($lat1) && empty($lat2))
                    if(empty($_FILES['photo']['size']))
                    {
                        $data_photo2['showing'] = "show";
                        // $data_photo2['gambar'] = 'ada dua photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_photo2);

                        // echo json_encode($data_photo2, JSON_UNESCAPED_UNICODE);
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['photo'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['photo[]']['name']= $files['name'][$key];
                            $_FILES['photo[]']['type']= $files['type'][$key];
                            $_FILES['photo[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['photo[]']['error']= $files['error'][$key];
                            $_FILES['photo[]']['size']= $files['size'][$key];
                
                            $fileName_photo2 = 'harada_'. $image;
                
                            $images[] = $fileName_photo2;
                
                            $config['file_name'] = $fileName_photo2;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('photo[]')) {
                                $uploadData = $this->upload->data();
                                $filename_photo2 = $uploadData['file_name'];
                                $data_photo2['photo_images'.$i]= $filename_photo2;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_photo2);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_photo2);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_photo2);
                        }  
                        // $data_photo2['gambar'] = 'ada dua photo';
                        // echo json_encode($data_photo2, JSON_UNESCAPED_UNICODE);
                    }
                }

                if($fileupload == '3')
                {
                    $data_photo3['username'] = $_SESSION["username"];
                    $data_photo3['customer'] = $this->input->post("customer");
                    $data_photo3["category_name"] = $this->input->post("category_name");
                    $data_photo3['code_project'] = $this->input->post("code_project");
                    $data_photo3['color'] = $this->input->post("color");
                    $data_photo3['joblist_files'] = $this->input->post("joblist_files");
                    $data_photo3['const_id'] = $this->input->post("const_id");  
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_photo3['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_photo3['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_photo3['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_photo3['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_photo3['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_photo3['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_photo3['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_photo3['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['photo']['size']!=0)
                    // {
                    //     for ($i=0; $i < 3; $i++) {
                    //         $file1 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file2 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file3 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['photo']['name'][$i];
                    //     }
                    // }

                    // $data_photo3['latlng1'] = $file1["Lat"].$file1["Lng"];
                    // $data_photo3['latlng2'] = $file2["Lat"].$file2["Lng"];
                    // $data_photo3['latlng3'] = $file2["Lat"].$file2["Lng"];

                    // $data_photo3['lat'] = $file1["Lat"];
                    // $data_photo3['lang'] = $file1["Lng"];

                    
                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_photo3['latlng1'] = $lat1.';'.$lat2;
                    $data_photo3['latlng2'] = $lat1.';'.$lat2;
                    $data_photo3['latlng3'] = $lat1.';'.$lat2;

                    $data_photo3['lat'] = $lat1;
                    $data_photo3['lang'] = $lat2;

                    // if(empty($lat1) && empty($lat2))
                    if(empty($_FILES['photo']['size']))
                    {
                        $data_photo3['showing'] = "show";
                        // $data_photo3['gambar'] = 'ada tiga photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_photo3);

                        // echo json_encode($data_photo3, JSON_UNESCAPED_UNICODE);
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['photo'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['photo[]']['name']= $files['name'][$key];
                            $_FILES['photo[]']['type']= $files['type'][$key];
                            $_FILES['photo[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['photo[]']['error']= $files['error'][$key];
                            $_FILES['photo[]']['size']= $files['size'][$key];
                
                            $fileName_photo3 = 'harada_'. $image;
                
                            $images[] = $fileName_photo3;
                
                            $config['file_name'] = $fileName_photo3;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('photo[]')) {
                                $uploadData = $this->upload->data();
                                $filename_photo3 = $uploadData['file_name'];
                                $data_photo3['photo_images'.$i]= $filename_photo3;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_photo3);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_photo3);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("check_photo", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_photo3);
                        }
                        // $data_photo3['gambar'] = 'ada tiga photo';
                        // echo json_encode($data_photo3, JSON_UNESCAPED_UNICODE);
                    }
                }

                if($fileupload == '4')
                {
                    $data_photo4['username'] = $_SESSION["username"];
                    $data_photo4['customer'] = $this->input->post("customer");
                    $data_photo4["category_name"] = $this->input->post("category_name");
                    $data_photo4['code_project'] = $this->input->post("code_project");
                    $data_photo4['color'] = $this->input->post("color");
                    $data_photo4['joblist_files'] = $this->input->post("joblist_files");
                    $data_photo4['const_id'] = $this->input->post("const_id");  
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_photo4['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_photo4['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_photo4['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_photo4['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_photo4['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_photo4['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_photo4['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_photo4['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['photo']['size']!=0)
                    // {
                    //     for ($i=0; $i < 4; $i++) {
                    //         $file1 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file2 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file3 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         $file4 = $this->exif->get_image_info($_FILES['photo']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['photo']['name'][$i];
                    //     }
                    // }

                    // $data_photo4['latlng1'] = $file1["Lat"].$file1["Lng"];
                    // $data_photo4['latlng2'] = $file2["Lat"].$file2["Lng"];
                    // $data_photo4['latlng3'] = $file1["Lat"].$file1["Lng"];
                    // $data_photo4['latlng4'] = $file2["Lat"].$file2["Lng"];

                    // $data_photo4['lat'] = $file1["Lat"];
                    // $data_photo4['lang'] = $file1["Lng"];

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_photo4['latlng1'] = $lat1.';'.$lat2;
                    $data_photo4['latlng2'] = $lat1.';'.$lat2;
                    $data_photo4['latlng3'] = $lat1.';'.$lat2;
                    $data_photo4['latlng4'] = $lat1.';'.$lat2;

                    $data_photo4['lat'] = $lat1;
                    $data_photo4['lang'] = $lat2;

                    // if(empty($lat1) && empty($lat2))
                    if(empty($_FILES['photo']['size']))
                    {
                        $data_photo4['showing'] = "show";
                        // $data_photo4['gambar'] = 'ada empat photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_photo4);

                        // echo json_encode($data_photo4, JSON_UNESCAPED_UNICODE);
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['photo'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['photo[]']['name']= $files['name'][$key];
                            $_FILES['photo[]']['type']= $files['type'][$key];
                            $_FILES['photo[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['photo[]']['error']= $files['error'][$key];
                            $_FILES['photo[]']['size']= $files['size'][$key];
                
                            $fileName_photo4 = 'harada_'. $image;
                
                            $images[] = $fileName_photo4;
                
                            $config['file_name'] = $fileName_photo4;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('photo[]')) {
                                $uploadData = $this->upload->data();
                                $filename_photo4 = $uploadData['file_name'];
                                $data_photo4['photo_images'.$i]= $filename_photo4;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_photo4);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_photo4);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_photo4);
                        }

                        // $data_photo4['gambar'] = 'ada empat photo';
                        // echo json_encode($data_photo4, JSON_UNESCAPED_UNICODE);
                    }
                }
            }
            if(!empty($_FILES['camera']['size']))
            {
                if($fileupload == '1')
                {
                    $data_camera1['username'] = $_SESSION["username"];
                    $data_camera1['customer'] = $this->input->post("customer");
                    $data_camera1["category_name"] = $this->input->post("category_name");
                    $data_camera1['code_project'] = $this->input->post("code_project");
                    $data_camera1['color'] = $this->input->post("color");
                    $data_camera1['joblist_files'] = $this->input->post("joblist_files");
                    $data_camera1['const_id'] = $this->input->post("const_id");  
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_camera1['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_camera1['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_camera1['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_camera1['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_camera1['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_camera1['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_camera1['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_camera1['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['camera']['size']!=0)
                    // {
                    //     $file_camera1 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //     $data_camera1['latlng1'] = $file_camera1["Lat"].$file_camera1["Lng"];
                    // }
                    
                    // $data_camera1['lat'] = $file_camera1["Lat"];
                    // $data_camera1['lang'] = $file_camera1["Lng"];


                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_camera1['latlng1'] = $lat1.';'.$lat2;

                    $data_camera1['lat'] = $lat1;
                    $data_camera1['lang'] = $lat2;


                    // if(empty($lat1) || empty($lat2))
                    if(empty($_FILES['camera']['size']))
                    {
                        $data_camera1['showing'] = "show";
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_camera1);
                    }
                    else
                    {
                        $config['upload_path'] = APPPATH.'../uploads/images'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size'] = '50000'; // in KlioBytes

                        $this->load->library('upload',$config); 
                        if ( ! $this->upload->do_upload('camera'))
                        {
                            print_r($this->upload->display_errors());
                            die();
                        }
                        else
                        {
                            $uploadData1 = $this->upload->data();
                            $filename_camera1 = $uploadData1['file_name'];
                            $data_camera1["photo_images1"]= $filename_camera1;
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_camera1);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_camera1);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_camera1);
                        }

                        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
                    }
                }
                if($fileupload == '2')
                {
                    $data_camera2['username'] = $_SESSION["username"];
                    $data_camera2['customer'] = $this->input->post("customer");
                    $data_camera2["category_name"] = $this->input->post("category_name");
                    $data_camera2['code_project'] = $this->input->post("code_project");
                    $data_camera2['color'] = $this->input->post("color");
                    $data_camera2['joblist_files'] = $this->input->post("joblist_files");
                    $data_camera2['const_id'] = $this->input->post("const_id");
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_camera2['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_camera2['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_camera2['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_camera2['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_camera2['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_camera2['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_camera2['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_camera2['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['camera']['tmp_name']!=0)
                    // {
                    //     for ($i=0; $i < 2; $i++) {
                    //         $file_camera1 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera2 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['camera']['name'][$i];
                    //     }
                    // }

                    // $data_camera2['latlng1'] = $file_camera1["Lat"].$file_camera1["Lng"];
                    // $data_camera2['latlng2'] = $file_camera2["Lat"].$file_camera2["Lng"];

                    // $data_camera2['lat'] = $file_camera1["Lat"];
                    // $data_camera2['lang'] = $file_camera1["Lng"];

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_camera2['latlng1'] = $lat1.';'.$lat2;
                    $data_camera2['latlng2'] = $lat1.';'.$lat2;

                    $data_camera2['lat'] = $lat1;
                    $data_camera2['lang'] = $lat2;

                    // if(empty($lat1) && empty($lat2))
                    if(empty($_FILES['camera']['size']))
                    {
                        $data_camera2['showing'] = "show";
                        // $data_camera2['gambar'] = 'ada dua camera photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_camera2);

                        // echo json_encode($data_camera2, JSON_UNESCAPED_UNICODE);
                        
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['camera'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['camera[]']['name']= $files['name'][$key];
                            $_FILES['camera[]']['type']= $files['type'][$key];
                            $_FILES['camera[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['camera[]']['error']= $files['error'][$key];
                            $_FILES['camera[]']['size']= $files['size'][$key];
                
                            $fileName_camera2 = 'harada_'. $image;
                
                            $images[] = $fileName_camera2;
                
                            $config['file_name'] = $fileName_camera2;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('camera[]')) {
                                $uploadData = $this->upload->data();
                                $filename_camera2 = $uploadData['file_name'];
                                $data_camera2['photo_images'.$i]= $filename_camera2;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_camera2);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_camera2);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_camera2);
                        }
                        // $data_camera2['gambar'] = 'ada dua camera photo';
                        // echo json_encode($data_camera2, JSON_UNESCAPED_UNICODE);
                    }
                    
                }
                if($fileupload == '3')
                {
                    $data_camera3['username'] = $_SESSION["username"];
                    $data_camera3['customer'] = $this->input->post("customer");
                    $data_camera3["category_name"] = $this->input->post("category_name");
                    $data_camera3['code_project'] = $this->input->post("code_project");
                    $data_camera3['color'] = $this->input->post("color");
                    $data_camera3['joblist_files'] = $this->input->post("joblist_files");
                    $data_camera3['const_id'] = $this->input->post("const_id");
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_camera3['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_camera3['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_camera3['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_camera3['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_camera3['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_camera3['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_camera3['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_camera3['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['camera']['size']!=0)
                    // {
                    //     for ($i=0; $i < 3; $i++) {
                    //         $file_camera1 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera2 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera3 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['camera']['name'][$i];
                    //     }
                    // }

                    // $data_camera3['latlng1'] = $file_camera1["Lat"].$file_camera1["Lng"];
                    // $data_camera3['latlng2'] = $file_camera2["Lat"].$file_camera2["Lng"];
                    // $data_camera3['latlng3'] = $file_camera2["Lat"].$file_camera2["Lng"];

                    // $data_camera3['lat'] = $file_camera1["Lat"];
                    // $data_camera3['lang'] = $file_camera1["Lng"];

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_camera3['latlng1'] = $lat1.';'.$lat2;
                    $data_camera3['latlng2'] = $lat1.';'.$lat2;
                    $data_camera3['latlng3'] = $lat1.';'.$lat2;

                    $data_camera3['lat'] = $lat1;
                    $data_camera3['lang'] = $lat2;

                    // if(empty($file_camera1["Lat"]) && empty($file_camera1["Lng"]))
                    if(empty($_FILES['camera']['size']))
                    {
                        $data_camera3['showing'] = "show";
                        // $data_camera3['gambar'] = 'ada tiga camera photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_camera3);

                        // echo json_encode($data_camera3, JSON_UNESCAPED_UNICODE);
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['camera'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['camera[]']['name']= $files['name'][$key];
                            $_FILES['camera[]']['type']= $files['type'][$key];
                            $_FILES['camera[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['camera[]']['error']= $files['error'][$key];
                            $_FILES['camera[]']['size']= $files['size'][$key];
                
                            $fileName_camera3 = 'harada_'. $image;
                
                            $images[] = $fileName_camera3;
                
                            $config['file_name'] = $fileName_camera3;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('camera[]')) {
                                $uploadData = $this->upload->data();
                                $filename_camera3 = $uploadData['file_name'];
                                $data_camera3['photo_images'.$i]= $filename_camera3;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_camera3);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_camera3);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data_camera3);
                        }
                        // $data_camera3['gambar'] = 'ada tiga camera photo';
                        // echo json_encode($data_camera, JSON_UNESCAPED_UNICODE);
                    } 
                }

                if($fileupload == '4')
                {
                    $data_camera4['username'] = $_SESSION["username"];
                    $data_camera4['customer'] = $this->input->post("customer");
                    $data_camera4["category_name"] = $this->input->post("category_name");
                    $data_camera4['code_project'] = $this->input->post("code_project");
                    $data_camera4['color'] = $this->input->post("color");
                    $data_camera4['joblist_files'] = $this->input->post("joblist_files");
                    $data_camera4['const_id'] = $this->input->post("const_id");
        
                    if(!empty($this->input->post("selection1"))) {
                        $data_camera4['selection1'] = $this->input->post("selection1");
                    }
                    if(!empty($this->input->post("selection2"))) {
                        $data_camera4['selection1'] = $this->input->post("selection2");
                    }
                    if(!empty($this->input->post("selection3"))) {
                        $data_camera4['selection1'] = $this->input->post("selection3");
                    }
                    if(!empty($this->input->post("selection4"))) {
                        $data_camera4['selection1'] = $this->input->post("selection4");
                    }

                    if(!empty($this->input->post("selection_name"))) {
                        $data_camera4['selection_name'] = $this->input->post("selection_name");
                    }
                    if(!empty($this->input->post("selection_name2"))) {
                        $data_camera4['selection_name'] = $this->input->post("selection_name2");
                    }
                    if(!empty($this->input->post("selection_name3"))) {
                        $data_camera4['selection_name'] = $this->input->post("selection_name3");
                    }
                    if(!empty($this->input->post("selection_name4"))) {
                        $data_camera4['selection_name'] = $this->input->post("selection_name4");
                    }

                    // if($_FILES['camera']['size']!=0)
                    // {
                    //     for ($i=0; $i < 4; $i++) {
                    //         $file_camera1 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera2 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera3 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         $file_camera4 = $this->exif->get_image_info($_FILES['camera']['tmp_name'][$i]);
                    //         // $data['photo_images'.$i] = $_FILES['camera']['name'][$i];
                    //     }
                    // }

                    // $data_camera4['latlng1'] = $file_camera1["Lat"].$file_camera1["Lng"];
                    // $data_camera4['latlng2'] = $file2["Lat"].$file2["Lng"];

                    // $data_camera4['lat'] = $file_camera1["Lat"];
                    // $data_camera4['lang'] = $file_camera1["Lng"];

                    $lat1 = $this->input->post("lat");
                    $lat2 = $this->input->post("lang");

                    $data_camera4['latlng1'] = $lat1.';'.$lat2;
                    $data_camera4['latlng2'] = $lat1.';'.$lat2;
                    $data_camera4['latlng3'] = $lat1.';'.$lat2;
                    $data_camera4['latlng4'] = $lat1.';'.$lat2;

                    $data_camera4['lat'] = $lat1;
                    $data_camera4['lang'] = $lat2;

                    // if(empty($lat1) && empty($lat2))
                    if(empty($_FILES['camera']['size']))
                    {
                        $data_camera4['showing'] = "show";
                        // $data_camera4['gambar'] = 'ada empats camera photo';
                        $this->record_action("check_photo", "view");
                        $this->load->view('user/dashboard/photo_upload_v',$data_camera4);

                        // echo json_encode($data_camera4, JSON_UNESCAPED_UNICODE);
                    }
                    else
                    {
                        $config = array(
                            'upload_path'   => APPPATH.'../uploads/images',
                            'allowed_types' => 'jpg|jpeg|png|gif',                     
                        );
                
                        $this->load->library('upload', $config);

                        $files = $_FILES['camera'];
                
                        $images = array();
                        $i = 0;

                        foreach ($files['name'] as $key => $image) {
                            $i++;

                            $_FILES['camera[]']['name']= $files['name'][$key];
                            $_FILES['camera[]']['type']= $files['type'][$key];
                            $_FILES['camera[]']['tmp_name']= $files['tmp_name'][$key];
                            $_FILES['camera[]']['error']= $files['error'][$key];
                            $_FILES['camera[]']['size']= $files['size'][$key];
                
                            $fileName_camera4 = 'harada_'. $image;
                
                            $images[] = $fileName_camera4;
                
                            $config['file_name'] = $fileName_camera4;
                
                            $this->upload->initialize($config);
                
                            if ($this->upload->do_upload('camera[]')) {
                                $uploadData = $this->upload->data();
                                $filename_camera4 = $uploadData['file_name'];
                                $data_camera4['photo_images'.$i]= $filename_camera4;
                            } else {
                                return false;
                            }
                        }

                        $this->load->model('users_model',"user");
                        $insert = $this->user->insert_project($data_camera4);
                        if($insert)
                        {
                            $this->record_action("add_project","execute");
                            $this->load->view('user/dashboard/finish_v',$data_camera4);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'insertion was failed.');
                            $this->record_action("error", "view");
                            redirect('user/select_category/'.$data['code_project'],$data);
                        }
                        // $data_camera4['gambar'] = 'ada empats camera photo';
                        // echo json_encode($data_camera4, JSON_UNESCAPED_UNICODE);
                    } 
                }
            }
        }
    }

















    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function test()
    {

        $config                   = array();
        $config['allowed_types']  = 'pdf';
        $config['overwrite']      = TRUE;
        $config['remove_spaces']  = TRUE;
        
        $this->load->library('upload', $config);
        
        // Image manipulation library
        $this->load->library('image_lib');
        
        foreach ($notes['name'] as $key => $note) 
        {
            $_FILES['notes']['name']      = $notes['name'][$key];
            $_FILES['notes']['type']      = $notes['type'][$key];
            $_FILES['notes']['tmp_name']  = $notes['tmp_name'][$key];
            $_FILES['notes']['error']     = $notes['error'][$key];
            $_FILES['notes']['size']      = $notes['size'][$key];
        
            $extension                    = pathinfo($_FILES['notes']['name'], PATHINFO_EXTENSION);
            $unique_no                    = uniqid(rand(), true);
            $filename[$key]               = $unique_no.'.'.$extension; // with ex
            $filename2[$key]              = $unique_no; // without ex
        
            $target_path                  = "notes_files/";
        
            if (!is_dir($target_path))
            {
                mkdir('./'.$target_path, 0777, true);
            }
        
            $config['file_name']          = $filename[$key];
            $config['upload_path']        = './'.$target_path;
        
            $this->upload->initialize($config);
        
            if (! $this->upload->do_upload('notes')) 
            {
                return array('error' => $this->upload->display_errors());
            }
        
            // converting pdf to images with imagick
            $im             = new Imagick();
            $im->setResolution(160,220);
        
            $ig = 0;
        
            while(true)
            {
                try {
                    $im->readimage($config['upload_path'].$config['file_name']."[$ig]");
                } catch (Exception $e) {
                    $ig = -1;
                }
        
                if($ig === -1) break;
        
                $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(imagick::ALPHACHANNEL_REMOVE);
                $im->mergeImageLayers(imagick::LAYERMETHOD_FLATTEN);
                $im->setImageFormat('jpg'); 
        
                $image_name     = $filename2[$key]."_$ig".'.jpg';
                $imageprops     = $im->getImageGeometry();
        
                $im->writeImage($config['upload_path'] .$image_name); 
                $im->clear(); 
                $im->destroy();
        
                // change file permission for file manipulation
                chmod($config['upload_path'].$image_name, 0777); // CHMOD file
        
                // add watermark to image
                $img_manip              = array();
                $img_manip              = array(
                    'image_library'     => 'gd2', 
                    'wm_type'           => 'overlay',
                    'wm_overlay_path'   => FCPATH . '/uploads/institutes/'.$institute_logo, // path to watermark image
                    'wm_x_transp'       => '10',
                    'wm_y_transp'       => '10',
                    'wm_opacity'        => '10',
                    'wm_vrt_alignment'  => 'middle',
                    'wm_hor_alignment'  => 'center',
                    'source_image'      => $config['upload_path'].$image_name
                );
        
                $this->image_lib->initialize($img_manip);
                $this->image_lib->watermark();
        
                ImageJPEG(ImageCreateFromString(file_get_contents($config['upload_path'].$image_name)), $config['upload_path'].$image_name, );
        
                $ig++;
            }
        
            // unlink the original pdf file
            chmod($config['upload_path'].$config['file_name'], 0777); // CHMOD file
            unlink($config['upload_path'].$config['file_name']);    // remove file
        }
        // echo '<p>Success</p>';exit;
        die(json_encode(array(
            'data' => 'Success',
            'status' => 'success'
        )));

        
    }


    public function test_array($code = NULL)
    {
        $min = '1001';
        $max = '1016';

        if(($min <= $code) && ($code <= $max))
        {

            $this->load->model('customer_model', "customer");
            $data["job_list"] = $this->customer->get_projectjoblist($code);

            $arr = $data["job_list"];

            $filterBy = '11001';

            $new = array_filter($arr, function ($var) use ($filterBy) {
                return ($var['id_item'] == $filterBy);
            });

            print("<pre>".print_r($new,true)."</pre>");
        }
    }


    public function test_exif()
    {
        $file = APPPATH.'../uploads/images/1de51a8439fa2d75071391b7563904fd.JPG'; 

        $data = $this->exif->get_image_info($file);

        $lat = $data["Lat"];
        $lng = $data["Lng"]; 
        $direction = $data["Direction"]; 

        echo json_encode($lat.','.$lng.' de :'.$direction);

    }

    public function test_view()
    {
        $this->load->model('customer_model', "customer");
        $ehe['afiliator'] = $this->input->get('customer');
        $data['file_project'] = $this->customer->view_file_project($ehe);

        echo json_encode($data,JSON_UNESCAPED_UNICODE);
    }

    public function view_files_contract($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("user_login");
        }
        else 
        {
            $path = FCPATH."uploads/contractor/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_cust($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("user_login");
        }
        else 
        {
            $path = FCPATH."uploads/customer/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_employ($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("user_login");
        }
        else 
        {
            $path = FCPATH."uploads/employee/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files($file_name)
    {
        $this->load->helper('file');
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("user_login");
        }
        else 
        {
            $path = FCPATH."uploads/files/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_images($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("user_login");
        }
        else 
        {
            $path = FCPATH."uploads/images/".urldecode($file_name); // get file name
            //get the file extension
            $info = mime_content_type($path);
            // var_dump($info->getExtension());
    
            // switch ($info->getExtension()) {
            //     // case 'pdf':
            //     // case 'png':
            //     case 'pdf':
            //         $contentDisposition = 'inline';
            //         break;
            //     default:
            //     $contentDisposition = 'attachment';
            // }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: ".$info);
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }


    public function logout()
    {
        
        $this->record_action("logout","execute");
        session_destroy();
        redirect("user");
    }



}