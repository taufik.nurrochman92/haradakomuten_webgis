<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        if(!isset($_SESSION["username"]))
        {
            redirect("login");
        }
    }
    

	public function index()
	{
        $this->load->model('items_model',"items");
        if(isset($_GET["category"]))
        {
            // print_r($_GET["category"]);die();
            switch ($_GET["category"])
            {
                case "sweet":
                    $cat = 'SOB';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                case "believe":
                    $cat = 'BOB';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                case "reform":
                    $cat = '小工事';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                default :
                    $data["data"] = $this->items->fetch_items_new();
            }
        }
        else 
        {
            $this->load->model('customer_model',"customer");
            $data["data"] = $this->customer->fetch_all_new();
        }


		$this->load->view('map/map_v',$data);
	}

    public function map2()
    {
        $this->load->model('items_model',"items");
        if(isset($_GET["category"]))
        {
            // print_r($_GET["category"]);die();
            switch ($_GET["category"])
            {
                case "sweet":
                    $cat = 'SOB';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                case "believe":
                    $cat = 'BOB';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                case "reform":
                    $cat = '小工事';
                    $data["data"] = $this->items->fetch_items_new($cat);
                    break;
                default :
                    $data["data"] = $this->items->fetch_items_new();
            }
        }
        else 
        {
            $this->load->model('customer_model',"customer");
            $data["data"] = $this->customer->fetch_all_new();
        }

        if(isset($_GET["find"]))
        {
            $data["latlong"] = $_GET["find"];
        }
        else
        {
            $data["latlong"] = null;
        }
		$this->load->view('map/map2_v',$data);
    }

    public function map3()
    {
        $this->load->model('items_model',"items");
        if(isset($_GET["category"]))
        {
            switch ($_GET["category"])
            {
                case "A":
                    $data["data"] = $this->items->fetch_items("A");
                    break;
                case "B":
                    $data["data"] = $this->items->fetch_items("B");
                    break;
                case "ALL":
                    $data["data"] = $this->items->fetch_items();
                    break;
            }
        }
        else 
        {
            $data["data"] = $this->items->fetch_items();
        }
		$this->load->view('map/map3_v',$data);
    }

    public function input_mark()
    {

        $this->load->view('map/input_mark');
    }

    public function insert_mark()
    {
        print_r($this->input->post());
        $this->load->model('items_model',"items");
        $insert = $this->items->insert_item($this->input->post());
        if($insert != false)
        {
            redirect('map','refresh');   
        }
        else 
        {
            redirect('map/input_mark','refresh');   
        }
    }
}
