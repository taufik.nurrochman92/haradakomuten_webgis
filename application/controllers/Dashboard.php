<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        if(!isset($_SESSION["username"]))
        {
            redirect("login");
        }
        if($_SESSION["access"] == '3')
        {
            redirect("user_login");
        }

        $this->load->library('upload');
        $this->config->set_item('language', 'japanese');
    }

    public function record_action($menu, $action)
    {
        $logs = 
        [
            "username" => $_SESSION["username"],
            "menu" => $menu,
            "action" => $action,
            "access" => $_SESSION["access"],
        ];
        $this->load->model('menu_logs_model',"menu_logs");
        $this->menu_logs->insert_log($logs);
    }

	// public function index()
	// {
    //     $this->record_action("dashboard", "view");
    //     $this->load->model('menu_logs_model',"menu_logs");
    //     $total = $this->menu_logs->count_all();
    //     $data = 
    //     [
    //         "dashboard" => $this->menu_logs->count_menu("dashboard")->total,
    //         "customer" => $this->menu_logs->count_menu_like("customer")->total,
    //         "customer_list" => $this->menu_logs->count_menu("customer_list")->total,
    //         "add_customer" => $this->menu_logs->count_menu("add_customer")->total,
    //         "add_customer_view" => $this->menu_logs->count_menu_action("add_customer","view")->total,
    //         "add_customer_execute" => $this->menu_logs->count_menu_action("add_customer","execute")->total,
    //         "edit_customer" => $this->menu_logs->count_menu("edit_customer")->total,
    //         "edit_customer_view" => $this->menu_logs->count_menu_action("edit_customer","view")->total,
    //         "edit_customer_execute" => $this->menu_logs->count_menu_action("edit_customer","execute")->total,
    //         "delete_customer" => $this->menu_logs->count_menu("delete_customer")->total,
    //         "inspection" => $this->menu_logs->count_menu_like("inspection")->total,
    //         "inspection_list" => $this->menu_logs->count_menu("inspection_list")->total,
    //         "add_inspection" => $this->menu_logs->count_menu("add_inspection")->total,
    //         "add_inspection_view" => $this->menu_logs->count_menu_action("add_inspection","view")->total,
    //         "add_inspection_execute" => $this->menu_logs->count_menu_action("add_inspection","execute")->total,
    //         "edit_inspection" => $this->menu_logs->count_menu("edit_inspection")->total,
    //         "edit_inspection_view" => $this->menu_logs->count_menu_action("edit_inspection","view")->total,
    //         "edit_inspection_execute" => $this->menu_logs->count_menu_action("edit_inspection","execute")->total,
    //         "delete_inspection" => $this->menu_logs->count_menu("delete_inspection")->total,
    //         "partner" => $this->menu_logs->count_menu_like("partner")->total,
    //         "partner_list" => $this->menu_logs->count_menu("partner_list")->total,
    //         "add_partner" => $this->menu_logs->count_menu("add_partner")->total,
    //         "add_partner_view" => $this->menu_logs->count_menu_action("add_partner","view")->total,
    //         "add_partner_execute" => $this->menu_logs->count_menu_action("add_partner","execute")->total,
    //         "edit_partner" => $this->menu_logs->count_menu("edit_partner")->total,
    //         "edit_partner_view" => $this->menu_logs->count_menu_action("edit_partner","view")->total,
    //         "edit_partner_execute" => $this->menu_logs->count_menu_action("edit_partner","execute")->total,
    //         "delete_partner" => $this->menu_logs->count_menu("delete_partner")->total,
    //         "public_work" => $this->menu_logs->count_menu_like("public_work")->total,
    //         "public_work_list" => $this->menu_logs->count_menu("public_work_list")->total,
    //         "add_public_work" => $this->menu_logs->count_menu("add_public_work")->total,
    //         "add_public_work_view" => $this->menu_logs->count_menu_action("add_public_work","view")->total,
    //         "add_public_work_execute" => $this->menu_logs->count_menu_action("add_public_work","execute")->total,
    //         "edit_public_work" => $this->menu_logs->count_menu("edit_public_work")->total,
    //         "edit_public_work_view" => $this->menu_logs->count_menu_action("edit_public_work","view")->total,
    //         "edit_public_work_execute" => $this->menu_logs->count_menu_action("edit_public_work","execute")->total,
    //         "delete_public_work" => $this->menu_logs->count_menu("delete_public_work")->total,
    //         "job_master" => $this->menu_logs->count_menu_like("job_master")->total,
    //         "job_master_list" => $this->menu_logs->count_menu("job_master_list")->total,
    //         "add_job_master" => $this->menu_logs->count_menu("add_job_master")->total,
    //         "add_job_master_view" => $this->menu_logs->count_menu_action("add_job_master","view")->total,
    //         "add_job_master_execute" => $this->menu_logs->count_menu_action("add_job_master","execute")->total,
    //         "edit_job_master" => $this->menu_logs->count_menu("edit_job_master")->total,
    //         "edit_job_master_view" => $this->menu_logs->count_menu_action("edit_job_master","view")->total,
    //         "edit_job_master_execute" => $this->menu_logs->count_menu_action("edit_job_master","execute")->total,
    //         "delete_job_master" => $this->menu_logs->count_menu("delete_job_master")->total,
    //         "assignment_master" => $this->menu_logs->count_menu_like("assignment_master")->total,
    //         "assignment_master_list" => $this->menu_logs->count_menu("assignment_master_list")->total,
    //         "add_assignment_master" => $this->menu_logs->count_menu("add_assignment_master")->total,
    //         "add_assignment_master_view" => $this->menu_logs->count_menu_action("add_assignment_master","view")->total,
    //         "add_assignment_master_execute" => $this->menu_logs->count_menu_action("add_assignment_master","execute")->total,
    //         "edit_assignment_master" => $this->menu_logs->count_menu("edit_assignment_master")->total,
    //         "edit_assignment_master_view" => $this->menu_logs->count_menu_action("edit_assignment_master","view")->total,
    //         "edit_assignment_master_execute" => $this->menu_logs->count_menu_action("edit_assignment_master","execute")->total,
    //         "delete_assignment_master" => $this->menu_logs->count_menu("delete_assignment_master")->total,
    //         "employee_master" => $this->menu_logs->count_menu_like("employee_master")->total,
    //         "employee_master_list" => $this->menu_logs->count_menu("employee_master_list")->total,
    //         "add_employee_master" => $this->menu_logs->count_menu("add_employee_master")->total,
    //         "add_employee_master_view" => $this->menu_logs->count_menu_action("add_employee_master","view")->total,
    //         "add_employee_master_execute" => $this->menu_logs->count_menu_action("add_employee_master","execute")->total,
    //         "edit_employee_master" => $this->menu_logs->count_menu("edit_employee_master")->total,
    //         "edit_employee_master_view" => $this->menu_logs->count_menu_action("edit_employee_master","view")->total,
    //         "edit_employee_master_execute" => $this->menu_logs->count_menu_action("edit_employee_master","execute")->total,
    //         "delete_employee_master" => $this->menu_logs->count_menu("delete_employee_master")->total,
    //     ];
    //     $this->load->view('dashboard/dashboard_v',$data);
	// }

    public function index()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_bank"] = $this->customer->fetch_finances();
        $this->load->view('dashboard/dashboard_v',$data);
    }

    public function customer_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all();
        $data["selector"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/list_v',$data);
    }


    //---------------------------------------NEW------------------------------------------//

    public function customers_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $data["selector_customers"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customers/list_v',$data);
    }

    public function view_customers($id)
    {
        $this->record_action("customer_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_new($id)[0];
        $data["data_const"] = $this->customer->fetch_one_construction_data($data["data"]->cust_code)[0];
        $this->load->view('dashboard/customers/view_v',$data);

        // print_r($data["data_const"]);
    }


    public function client_list()
    {
        
        $this->record_action("client_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_client();
        $data["datas"] = $this->customer->fetch_employees_names();
        $data["selector_client"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/list_v',$data);
    }


    public function search_client_list()
    {
        $this->record_action("client_search_list", "view");
        $this->load->model('customer_model', "customer");

        $postData = $this->input->post();
        $data = $this->security->xss_clean($postData);

        // print_r($postData);die();

        $data["data"] = $this->customer->fetch_search_client($data);
        $data["datas"] = $this->customer->fetch_employees_names();

        $data["selector_client"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/list_v',$data);
    }


    public function view_client($id)
    {
        $this->record_action("client_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_client_new($id)[0];
        $this->load->view('dashboard/customer/view_v',$data);
    }


    public function employees_list()
    {
        
        $this->record_action("employee_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_employees_new();
        $data["selector_employees"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/employees/list_v',$data);
    }

    public function view_employees($id)
    {
        $this->record_action("employee_view", "view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $data["datar"] = $this->user->fetch_one_username($id)[0];
        $this->load->view('dashboard/employees/view_v',$data);
    }

    public function contractor_list()
    {
        
        $this->record_action("contractor_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_contractor_new();
        $data["selector_contractor"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/contractor/list_v',$data);
    }

    public function view_contractor($id)
    {
        $this->record_action("contractor_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];

    
        $this->load->model('users_model', "user");
        $name = $this->user->get_name_affiliate($id)[0];

        $data["data_affiliate"] = $this->user->fetch_all_user_by($name);
        $this->load->view('dashboard/contractor/view_v',$data);

        // echo json_encode($name, JSON_UNESCAPED_UNICODE);
    }


    public function email()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $this->load->view('dashboard/email/list_v',$data);
    }


    public function constructor_list()
    {
        
        $this->record_action("construction_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_construction_new();
        $data["selector_costruction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/list_v',$data);
    }

    public function constructor_list_all()
    {
        
        $this->record_action("construction_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_construction_new();
        $data["selector_costruction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/list_all_v',$data);
    }

    public function view_construction($id)
    {
        $this->record_action("construction_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_construction_new($id)[0];
        $this->load->view('dashboard/constructor/view_v',$data);

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function view_constructor_list($id)
    {
        $this->record_action("client_view", "view");
        $this->load->model('customer_model', "customer");
        $ehe = $this->customer->get_customer_name($id);
        $data["data"] = $this->customer->get_project_list($id);
        $this->load->view('dashboard/constructor/list_project',$data);

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }



    //-----------------------------------------------------------------------//

    public function add_customer()
    {
        $this->record_action("add_customer", "view");
        $this->load->view('dashboard/customer/insert_v');
    }


    //---------------------------------------NEW-----------------------------------------//

    public function add_customers()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_id"] = $this->customer->get_last_id_inspect()[0];
        $data["for_bank"] = $this->customer->fetch_finances();
        $data["selector_add_customers"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customers/insert_v',$data);
    }

    public function add_client()
    {
        $this->record_action("add_client", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_id"] = $this->customer->get_last_id_client()[0];
        $data["selector_add_client"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/insert_v',$data);
    }


    public function add_employees()
    {
        $this->load->model('customer_model', "customer");
        $this->record_action("add_customer", "view");
        $data["selector_add_employees"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $data['jumlah'] = $this->customer->check_admin();
        $this->load->view('dashboard/employees/insert_v',$data);
    }

    public function add_contractor()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["for_id"] = $this->customer->get_last_id_affiliate()[0];
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_contractor"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/contractor/insert_v',$data);
    }

    public function add_construction()
    {
        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data_employ"] = $this->customer->fetch_employees_names();
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_construction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/insert_v',$data);
    }


    //-----------------------------------------------------------------------//


    public function insert_customer()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
            $this->load->model('customer_model',"customer");
            $insert = $this->customer->insert_customer($data);
            if($insert)
            {
                $this->record_action("add_customer","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/customer_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'OB顧客の登録が失敗いたしました。');
                $this->load->view('dashboard/customers/insert_v',$data);
            }
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            $this->load->view('dashboard/customers/insert_v',$data);
        }

    }


    //---------------------------------------NEW-----------------------------------------//


    public function insert_customers()
    {
        // print_r($_POST);die();
        $data = [
            "gid" => $this->input->post("gid"),
            "cust_code" => $this->input->post("cust_code"),
            "氏名" => $this->input->post("name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "生年月日" => $this->input->post("birthday"),
            "age" => $this->input->post("age"),
            "共同住宅" => $this->input->post("building_name"),
            "電　　話" => $this->input->post("phone_number"),
            "種別" => $this->input->post("kinds"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("calendar"),
            "メールアドレス" => $this->input->post("email"),
            "備考" => $this->input->post("marks"),
            "氏名_かな" => $this->input->post("name_kana"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "長期優良住宅" => $this->input->post("longterm"),
            "設備10年保証加入" => $this->input->post("warranty"),
            "引渡日" => $this->input->post("delivery_date"),
            "定期点検項目_3ヶ月" => $this->input->post("inspection_time1"),
            "定期点検項目_6ヶ月" => $this->input->post("inspection_time2"),
            "定期点検項目_1年" => $this->input->post("inspection_time3"),
            "定期点検項目_2年" => $this->input->post("inspection_time4"),
            "定期点検項目_3年" => $this->input->post("inspection_time5"),
            "定期点検項目_5年" => $this->input->post("inspection_time6"),
            "定期点検項目_10年" => $this->input->post("inspection_time7"),
            "借入金融機関" => $this->input->post("bank"),
            "火災保険" => $this->input->post("insurance_company"),
            "火災保険備考" => $this->input->post("insurance"),
            "グリーン化事業補助金" => $this->input->post("subsidy"),
            "地盤改良工事" => $this->input->post("ground_improve"),
            "感謝祭" => $this->input->post("thanksgiving"),
            "イベントDM" => $this->input->post("dm")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/customers_list");
        // }
        if(!empty($this->input->post("sales_staff1")))
        {
            $data["営業担当"] = $this->input->post("sales_staff1");
        }
        else
        {
            $data["営業担当"] = $this->input->post("sales_staff");
        }

        if(!empty($this->input->post("pic_construction1")))
        {
            $data["工務担当"] = $this->input->post("pic_construction1");
        }
        else
        {
            $data["工務担当"] = $this->input->post("pic_construction");
        }


        if(!empty($this->input->post("incharge_coordinator1")))
        {
            $data["コーデ担当"] = $this->input->post("incharge_coordinator1");
        }
        else
        {
            $data["コーデ担当"] = $this->input->post("incharge_coordinator");
        }
        

        


        $data["date_insert"] = date('Y-m-d H:i:s');
        $data = $this->security->xss_clean($data);
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_customers_new($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'OB顧客の登録が失敗いたしました。');
            $this->load->view('dashboard/customers/insert_v',$data);
        }
    }

    public function insert_client()
    {
        // print_r($_POST);die();
        $data = [
            "gid" => $this->input->post("gid"),
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "birth_date" => $this->input->post("birth_date"),
            "age" => $this->input->post("age"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "土地　有無" => $this->input->post("with_or_without"),
            "ＤＭ" => $this->input->post("dm"),
            "備考" => $this->input->post("remarks"),
            "氏名_かな" => $this->input->post("full_name_kana"),
            "メールアドレス" => $this->input->post("email")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/client_list");
        // }
        if(!empty($this->input->post("first_time_change1")))
        {
            $data["初回担当"] = $this->input->post("first_time_change1");
        }
        else
        {
            $data["初回担当"] = $this->input->post("first_time_change");
        }

        if(!empty($this->input->post("pic_person1")))
        {
            $data["担当"] = $this->input->post("pic_person1");
        }
        else
        {
            $data["担当"] = $this->input->post("pic_person");
        }


        $data["date_insert"] = date('Y-m-d H:i:s');
        $data = $this->security->xss_clean($data);
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_client_new($data);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/client_list");
        }
        else
        {
            $data["for_id"] = $this->customer->get_last_id_client()[0];
            $this->session->set_flashdata('error', '営業中顧客の登録が失敗いたしました。');
            // redirect("dashboard/client_list");
            $this->load->view('dashboard/customer/insert_v',$data);
        }
    }

    public function insert_employees()
    {


            $data["社員コード"] = $this->input->post("employee_code");
            $data["姓"] = $this->input->post("last_name");
            $data["名"] = $this->input->post("first_name");
            $data["せい"] = $this->input->post("last_furigana");
            $data["めい"] = $this->input->post("first_furigana");
            $data["保有資格1"] = $this->input->post("qualification_1");
            $data["保有資格2"] = $this->input->post("qualification_2");
            $data["保有資格3"] = $this->input->post("qualification_3");
            $data["保有資格4"] = $this->input->post("qualification_4");
            $data["保有資格5"] = $this->input->post("qualification_5");
            $data["入社日"] = $this->input->post("hire_date");
            $data["郵便番号1"] = $this->input->post("zip_code");
            $data["住所1"] = $this->input->post("address_line");
            $data["住所1番地"] = $this->input->post("address");
            $data["電話番号"] = $this->input->post("telephone_number");
            $data["携帯番号"] = $this->input->post("cellphone_number");
            $data["email"] = $this->input->post("email");
            $data["username"] = $this->input->post("username");
            $data["password"] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data["view_password"] = $this->input->post("password");
            $data["privilage"] = $this->input->post("privilage");
            $data["date_insert"] = date('Y-m-d H:i:s');


            $config['upload_path'] = APPPATH.'../uploads/employee'; 
            $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            $config['max_size'] = '500000'; // in KlioBytes

            if($_FILES['sertificate_1']['size']!=0)
            {
                $new_names1 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_1"));
                $config['file_name'] = $new_names1;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('sertificate_1'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["sertificate_1"]= $filename1;
                }
            }

            if($_FILES['sertificate_2']['size']!=0)
            {
                $new_names2 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_2"));
                $config['file_name'] = $new_names2;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('sertificate_2'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData2 = $this->upload->data();
                    $filename2 = $uploadData2['file_name'];
                    $data["sertificate_2"]= $filename2;
                }
            }

            if($_FILES['sertificate_3']['size']!=0)
            {
                $new_names3 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_3"));
                $config['file_name'] = $new_names3;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('sertificate_3'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData3 = $this->upload->data();
                    $filename3 = $uploadData3['file_name'];
                    $data["sertificate_3"]= $filename3;
                }
            }

            if($_FILES['sertificate_4']['size']!=0)
            {
                $new_names4 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_4"));
                $config['file_name'] = $new_names4;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('sertificate_4'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData4 = $this->upload->data();
                    $filename4 = $uploadData4['file_name'];
                    $data["sertificate_4"]= $filename4;
                }
            }

            if($_FILES['sertificate_5']['size']!=0)
            {
                $new_names5 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_5"));
                $config['file_name'] = $new_names5;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('sertificate_5'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData5 = $this->upload->data();
                    $filename5 = $uploadData5['file_name'];
                    $data["sertificate_5"]= $filename5;
                }
            }

        if(empty($this->input->post("employee_code"))) {
            $this->session->set_flashdata('error', '社員コードを入力してください');
            $this->load->view('dashboard/employees/insert_v',$data);
        }
        else
        {
            $data = $this->security->xss_clean($data);

            $this->load->model('customer_model',"customer");
            $insert = $this->customer->insert_employees_new($data);
            $this->load->model('users_model',"user");
            $inserted = $this->user->insert_user($data);

            if($insert && $inserted)
            {
                $this->record_action("add_customer","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/employees_list");
            }
            else
            {
                $this->session->set_flashdata('error', '社員の登録が失敗いたしました。');
                $this->load->view('dashboard/employees/insert_v',$data);
            }
        }
    }


    public function insert_contractor()
    {
        $this->load->model('customer_model',"customer");
        $data["for_id"] = $this->customer->get_last_id_affiliate()[0];

        if(!empty($this->input->post("kinds")) && empty($this->input->post("username")))
        {
            $data = [
                "gid" => $this->input->post("contractor_code"),
                "種別" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name")),
                "連名" => $this->input->post("joint_name"),
                "〒" => $this->input->post("zip_code"),
                "都道府県" => $this->input->post("prefecture"),
                "住　　　所" => $this->input->post("address"),
                "住所2" => $this->input->post("address2"),
                "電　　話" => $this->input->post("telephone"),
                "invoice" => "T".$this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status")
            ];

            $datar = [
                "kinds" => $this->input->post("kinds"),
                "b" => $this->input->post("latitude"),
                "l" => $this->input->post("longitude"),
                "no" => $this->input->post("no"),
                "name" => str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name")),
                "joint_name" => $this->input->post("joint_name"),
                "zip_code" => $this->input->post("zip_code"),
                "prefecture" => $this->input->post("prefecture"),
                "address" => $this->input->post("address"),
                "address2" => $this->input->post("address2"),
                "telephone" => $this->input->post("telephone"),
                "invoice" => "T".$this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status")
            ];

            $data["date_insert"] = date('Y-m-d H:i:s');

            $config2['upload_path'] = APPPATH.'../uploads/contractor'; 
            $config2['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            $config2['max_size'] = '50000'; // in KlioBytes

            

            if($_FILES['admission']['size']!=0)
            {
                $new_names = str_replace(array(";",",","@","!","#",":","(",")"),"",$_FILES['admission']['name']);
                $config2['file_name'] = $new_names;
                $this->upload->initialize($config2);
                if ( ! $this->upload->do_upload('admission'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $data["admission"]= $filename;
                }
            }

            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data["b"] = $this->input->post("latitude");
                $data["l"] = $this->input->post("longitude");

                $data = $this->security->xss_clean($data);

            
                $check = $this->customer->check_no($data["no"]);

                $check_name = $this->customer->check_name($data['name']);


                if($check == 1) {
                    $this->record_action("duplicate_no","execute");
                    $this->session->set_flashdata('error', '「No.」が他協力業者の「No.」と同じく入力できません。');
                    $this->load->view('dashboard/contractor/insert_v',$datar);
                }
                else if($check_name == 'have')
                {
                    $this->session->set_flashdata('error', '協力業者名は既に存在しています');
                    $this->load->view('dashboard/contractor/insert_v',$datar);
                }
                else{
                    $insert = $this->customer->insert_contractor_new($data);
                    if($insert)
                    {
                        $this->record_action("add_contractorr","execute");
                        $this->session->set_flashdata('success', '更新しました');
                        redirect("dashboard/contractor_list");
                    }
                    else
                    {
                        $this->session->set_flashdata('error', '業者登録が失敗しました');
                        $this->load->view('dashboard/contractor/insert_v',$datar);
                    }
                }
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                $this->load->view('dashboard/contractor/insert_v',$datar);
            }

        }
        else if(!empty($this->input->post('username')) && empty($this->input->post("kinds")))
        {
            $this->session->set_flashdata('error', '協力業者のデーターを入力してください。');
            $this->load->view('dashboard/contractor/insert_v');
        }
        else if(!empty($this->input->post("kinds")) && !empty($this->input->post('username')))
        {
            
            $data1["gid"] = $this->input->post("contractor_code");
            $data1["種別"] = $this->input->post("kinds");
            $data1["no"] = $this->input->post("no");
            $data1["name"] = str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name"));
            $data1["連名"] = $this->input->post("joint_name");
            $data1["〒"] = $this->input->post("zip_code");
            $data1["都道府県"] = $this->input->post("prefecture");
            $data1["住　　　所"] = $this->input->post("address");
            $data1["住所2"] = $this->input->post("address2");
            $data1["電　　話"] = $this->input->post("telephone");
            $data1["invoice"] = $this->input->post("invoice");
            $data1["pic"] = $this->input->post("pic");
            $data1["phone"] = $this->input->post("phone");
            $data1["fax"] = $this->input->post("fax");
            $data1["email"] = $this->input->post("email");
            $data1["pic_nd"] = $this->input->post("pic_nd");
            $data1["phone_nd"] = $this->input->post("phone_nd");
            $data1["email_nd"] = $this->input->post("email_nd");
            $data1["project_stat"] = $this->input->post("project_stat");
            $data1["status"] = $this->input->post("status");
            $data1["date_insert"] = date('Y-m-d H:i:s');

            $datar = [
                "b" => $this->input->post("latitude"),
                "l" => $this->input->post("longitude"),
                "kinds" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => $this->input->post("name"),
                "joint_name" => $this->input->post("joint_name"),
                "zip_code" => $this->input->post("zip_code"),
                "prefecture" => $this->input->post("prefecture"),
                "address" => $this->input->post("address"),
                "address2" => $this->input->post("address2"),
                "telephone" => $this->input->post("telephone"),
                "invoice" => "T".$this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status"),
                "usernamerr" => $this->input->post("username"),
                "view_passworddd" => $this->input->post("password"),
            ];
            

            $config2['upload_path'] = APPPATH.'../uploads/contractor'; 
            $config2['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            $config2['max_size'] = '50000'; // in KlioBytes

            if($_FILES['admission']['size']!=0)
            {
                $new_names = str_replace(array(";",",","@","!","#",":","(",")"),"",$_FILES['admission']['name']);
                $config2['file_name'] = $new_names;
                $this->upload->initialize($config2);
                if ( ! $this->upload->do_upload('admission'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $data1["admission"]= $filename;
                }
            }

            $data2['username'] = $this->input->post('username');
            $data2['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data2['view_password'] = $this->input->post('password');
            $data2['access'] = '3';
            $data2['affilliate_from'] = $this->input->post('name');
            $data2['date_regist'] = date('Y-m-d');

            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data1["b"] = $this->input->post("latitude");
                $data1["l"] = $this->input->post("longitude");

                

                $data1 = $this->security->xss_clean($data1);
                $data2 = $this->security->xss_clean($data2);

                $this->load->model('customer_model',"customer");
                $this->load->model('users_model',"user");
                $check = $this->customer->check_no($data1["no"]);

                if($check == 1) {
                    $this->record_action("duplicate_no","execute");
                    $this->session->set_flashdata('error', '「No.」が他協力業者の「No.」と同じく入力できません。');
                    $this->load->view('dashboard/contractor/insert_v',$datar);
                }
                else
                {
                    $insert2 = $this->user->insert_user_new($data2);
                    $insert1 = $this->customer->insert_contractor_new($data1);
        
                    if($insert1 && $insert2)
                    {
                        $this->record_action("add_user","execute");
                        $this->session->set_flashdata('success', '更新しました');
                        redirect("dashboard/contractor_list");
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                        $this->load->view('dashboard/contractor/insert_v',$datar);
                    }
                }
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                $this->load->view('dashboard/contractor/insert_v',$datar);
            }

        }
        else if(empty($this->input->post("kinds")))
        {
            $datar = [
                "b" => $this->input->post("b"),
                "l" => $this->input->post("l"),
                "no" => $this->input->post("no"),
                "name" => str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name")),
                "joint_name" => $this->input->post("joint_name"),
                "zip_code" => $this->input->post("zip_code"),
                "prefecture" => $this->input->post("prefecture"),
                "address" => $this->input->post("address"),
                "address2" => $this->input->post("address2"),
                "telephone" => $this->input->post("telephone"),
                "invoice" => "T".$this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status"),
                "usernamerr" => $this->input->post("username"),
                "view_passworddd" => $this->input->post("password"),
            ];

            $this->session->set_flashdata('error', '種別を入力してください。');
            $this->load->view('dashboard/contractor/insert_v',$datar);
        }
        else
        {
            $this->session->set_flashdata('error', '協力業者のデーターを入力してください。');
            $this->load->view('dashboard/contractor/insert_v');
        }
        
    }


    public function insert_constructor()
    {
        // redirect("dashboard/underconstruc");
        $data = [
            "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
            "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
            "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
            "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
            "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
            "contract_date" => $this->input->post("contract_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "upper_building_date" => $this->input->post("upper_building_date"),
            "completion_date" => $this->input->post("completion_date"),
            "delivery_date" => $this->input->post("delivery_date"),
            "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
            "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
            "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
            "contract" => mb_convert_kana($this->input->post("contract"), "as"),
            "constract_drawing1" => mb_convert_kana($this->input->post("constract_drawing1"), "as"),
            "check1" => mb_convert_kana($this->input->post("check1"), "as"),
            "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
            "constract_drawing2" => mb_convert_kana($this->input->post("constract_drawing2"), "as"),
            "check2" => mb_convert_kana($this->input->post("check2"), "as"),
            "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
            "constract_drawing3" => mb_convert_kana($this->input->post("constract_drawing3"), "as"),
            "check3" => mb_convert_kana($this->input->post("check3"), "as"),
            "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
            "constract_drawing4" => mb_convert_kana($this->input->post("constract_drawing4"), "as"),
            "check4" => mb_convert_kana($this->input->post("check4"), "as"),
            "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
            "constract_drawing5" => mb_convert_kana($this->input->post("constract_drawing5"), "as"),
            "check5" => mb_convert_kana($this->input->post("check5"), "as"),
            "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
            "constract_drawing6" => mb_convert_kana($this->input->post("constract_drawing6"), "as"),
            "check6" => mb_convert_kana($this->input->post("check6"), "as"),
            "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
            "constract_drawing7" => mb_convert_kana($this->input->post("constract_drawing7"), "as"),
            "check7" => mb_convert_kana($this->input->post("check7"), "as"),
            "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
            "constract_drawing8" => mb_convert_kana($this->input->post("constract_drawing8"), "as"),
            "check8" => mb_convert_kana($this->input->post("check8"), "as"),
            "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
            "constract_drawing9" => mb_convert_kana($this->input->post("constract_drawing9"), "as"),
            "check9" => mb_convert_kana($this->input->post("check9"), "as"),
            "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
        ];

        if (!empty($this->input->post("co_owner1")))
        {
            $data["co_owner1"] = implode(";",mb_convert_kana($this->input->post("co_owner1"), "as"));
        }
        else
        {
            $data["co_owner1"] = '';
        }
        
        if (!empty($this->input->post("co_owner2")))
        {
            $data["co_owner2"] = implode(";",mb_convert_kana($this->input->post("co_owner"), "as"));
        }
        else
        {
            $data["co_owner2"] = '';
        }
        
        if (!empty($this->input->post("co_owner3")))
        {
            $data["co_owner3"] = implode(";",mb_convert_kana($this->input->post("co_owner3"), "as"));
        }
        else
        {
            $data["co_owner3"] = '';
        }
        
        if (!empty($this->input->post("co_owner4")))
        {
            $data["co_owner4"] = implode(";",mb_convert_kana($this->input->post("co_owner4"), "as"));
        }
        else
        {
            $data["co_owner4"] = '';
        }
        
        if (!empty($this->input->post("co_owner5")))
        {
            $data["co_owner5"] = implode(";",mb_convert_kana($this->input->post("co_owner5"), "as"));
        }
        else
        {
            $data["co_owner5"] = '';
        }
        
        if (!empty($this->input->post("co_owner6")))
        {
            $data["co_owner6"] = implode(";",mb_convert_kana($this->input->post("co_owner6"), "as"));
        }
        else
        {
            $data["co_owner6"] = '';
        }
        
        if (!empty($this->input->post("co_owner7")))
        {
            $data["co_owner7"] = implode(";",mb_convert_kana($this->input->post("co_owner7"), "as"));
        }
        else
        {
            $data["co_owner7"] = '';
        }
        
        if (!empty($this->input->post("co_owner8")))
        {
            $data["co_owner8"] = implode(";",mb_convert_kana($this->input->post("co_owner8"), "as"));
        }
        else
        {
            $data["co_owner8"] = '';
        }
        
        if (!empty($this->input->post("co_owner9")))
        {
            $data["co_owner9"] = implode(";",mb_convert_kana($this->input->post("co_owner9"), "as"));
        }
        else
        {
            $data["co_owner9"] = '';
        }

        $config['upload_path'] = APPPATH.'../uploads/files'; 
        $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        $config['max_size'] = '500000'; // in KlioBytes

        for ($i=1; $i<= 9 ; $i++) {
            if($_FILES['constract_drawing'.$i]['name'] != "")
            {
                // $new_name = $_FILES['file_name1']['name'].$i;
                // $config['file_name'] = $new_name;
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('constract_drawing'.$i))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    //file is uploaded successfully
                    //now get the file uploaded data 
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $data['constract_drawing'.$i]= $filename;
                }
            }
        } 

        $data = $this->security->xss_clean($data);

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_constructor_new($data);
        if($insert)
        {
            $this->record_action("add_constructor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/constructor_list");
        }
        else
        {
            $this->session->set_flashdata('error', '工事の登録が失敗いたしました。');
            redirect("dashboard/constructor_list");
        }
    }


    //-----------------------------------------------------------------------//

    public function preview_data()
    {
        $data["cust_code"] = mb_convert_kana($this->input->post("cust_code"), "as");
        $data["cust_name"] = mb_convert_kana($this->input->post("cust_name"), "as");
        $data["kinds"] = mb_convert_kana($this->input->post("kinds"), "as");
        $data["const_no"] = mb_convert_kana($this->input->post("const_no"), "as");
        $data["remarks"] = mb_convert_kana($this->input->post("remarks"), "as");
        $data["contract_date"] = $this->input->post("contract_date");
        $data["construction_start_date"] = $this->input->post("construction_start_date");
        $data["upper_building_date"] = $this->input->post("upper_building_date");
        $data["completion_date"] = $this->input->post("completion_date");
        $data["delivery_date"] = $this->input->post("delivery_date");
        $data["amount_money"] = mb_convert_kana($this->input->post("amount_money"), "as");
        $data["construct_stat"] = $this->input->post("construct_stat");

        // $config['upload_path'] = APPPATH.'../uploads/files'; 
        // $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        // $config['max_size'] = '500000'; // in KlioBytes

        if(!empty($this->input->post("sales_staff1")))
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff1"), "as");
        }
        else
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff"), "as");
        }

        if(!empty($this->input->post("incharge_construction1")))
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction1"), "as");
        }
        else
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction"), "as");
        }

        if(!empty($this->input->post("incharge_coordinator1")))
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator1"), "as");
        }
        else
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator"), "as");
        }


        if(!empty($this->input->post("constract_drawing1blueatext")))
        // if($_FILES['constract_drawing1bluea']['size']!=0)
        {

            $data["check1bluea"] = $this->input->post("check1bluea");
            $data["desc_item1bluea"] = mb_convert_kana($this->input->post("desc_item1bluea"), "as");
            $data["check2bluea"] = $this->input->post("check2bluea");
            $data["desc_item2bluea"] = mb_convert_kana($this->input->post("desc_item2bluea"), "as");
            $data["check3bluea"] = $this->input->post("check3bluea");
            $data["desc_item3bluea"] = mb_convert_kana($this->input->post("desc_item3bluea"), "as");
            $data["check4bluea"] = $this->input->post("check4bluea");
            $data["desc_item4bluea"] = mb_convert_kana($this->input->post("desc_item4bluea"), "as");
            $data["check5bluea"] = $this->input->post("check5bluea");
            $data["desc_item5bluea"] = mb_convert_kana($this->input->post("desc_item5bluea"), "as");
            $data["check6bluea"] = $this->input->post("check6bluea");
            $data["desc_item6bluea"] = mb_convert_kana($this->input->post("desc_item6bluea"), "as");
            $data["check7bluea"] = $this->input->post("check7bluea");
            $data["desc_item7bluea"] = mb_convert_kana($this->input->post("desc_item7bluea"), "as");
            $data["check8bluea"] = $this->input->post("check8bluea");
            $data["desc_item8bluea"] = mb_convert_kana($this->input->post("desc_item8bluea"), "as");
            $data["check9bluea"] = $this->input->post("check9bluea");
            $data["desc_item9bluea"] = mb_convert_kana($this->input->post("desc_item9bluea"), "as");
            $data["check10bluea"] = $this->input->post("check10bluea");
            $data["desc_item10bluea"] = mb_convert_kana($this->input->post("desc_item10bluea"), "as");
            $data["category_name1bluea"] = "確認申請書一式";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1bluea")))
            {
                $data["co_owner1bluea"] = $this->input->post("co_owner1bluea");
            }
            else
            {
                $data["co_owner1bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluea")))
            {
                $data["co_owner2bluea"] = $this->input->post("co_owner2bluea");
            }
            else
            {
                $data["co_owner2bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluea")))
            {
                $data["co_owner3bluea"] = $this->input->post("co_owner3bluea");
            }
            else
            {
                $data["co_owner3bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluea")))
            {
                $data["co_owner4bluea"] = $this->input->post("co_owner4bluea");
            }
            else
            {
                $data["co_owner4bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluea")))
            {
                $data["co_owner5bluea"] = $this->input->post("co_owner5bluea");
            }
            else
            {
                $data["co_owner5bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluea")))
            {
                $data["co_owner6bluea"] = $this->input->post("co_owner6bluea");
            }
            else
            {
                $data["co_owner6bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluea")))
            {
                $data["co_owner7bluea"] = $this->input->post("co_owner7bluea");
            }
            else
            {
                $data["co_owner7bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluea")))
            {
                $data["co_owner8bluea"] = $this->input->post("co_owner8bluea");
            }
            else
            {
                $data["co_owner8bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluea")))
            {
                $data["co_owner9bluea"] = $this->input->post("co_owner9bluea");
            }
            else
            {
                $data["co_owner9bluea"] = '';
            }

            if (!empty($this->input->post("co_owner10bluea")))
            {
                $data["co_owner10bluea"] = $this->input->post("co_owner10bluea");
            }
            else
            {
                $data["co_owner10bluea"] = '';
            }

            $data["constract_drawing1bluea"]= $this->input->post("constract_drawing1blueatext");
            $data["constract_drawing2bluea"]= $this->input->post("constract_drawing2blueatext");
            $data["constract_drawing3bluea"]= $this->input->post("constract_drawing3blueatext");
            $data["constract_drawing4bluea"]= $this->input->post("constract_drawing4blueatext");
            $data["constract_drawing5bluea"]= $this->input->post("constract_drawing5blueatext");
            $data["constract_drawing6bluea"]= $this->input->post("constract_drawing6blueatext");
            $data["constract_drawing7bluea"]= $this->input->post("constract_drawing7blueatext");
            $data["constract_drawing8bluea"]= $this->input->post("constract_drawing8blueatext");
            $data["constract_drawing9bluea"]= $this->input->post("constract_drawing9blueatext");
            $data["constract_drawing10bluea"]= $this->input->post("constract_drawing10blueatext");

            $data["date_insert_drawing1bluea"] = date('Y-m-d');
            $data["date_insert_drawing2bluea"] = date('Y-m-d');
            $data["date_insert_drawing3bluea"] = date('Y-m-d');
            $data["date_insert_drawing4bluea"] = date('Y-m-d');
            $data["date_insert_drawing5bluea"] = date('Y-m-d');
            $data["date_insert_drawing6bluea"] = date('Y-m-d');
            $data["date_insert_drawing7bluea"] = date('Y-m-d');
            $data["date_insert_drawing8bluea"] = date('Y-m-d');
            $data["date_insert_drawing9bluea"] = date('Y-m-d');
            $data["date_insert_drawing10bluea"] = date('Y-m-d');

        }
        
        if(!empty($this->input->post("constract_drawing1bluebtext")))
        // if($_FILES['constract_drawing1blueb']['size']!=0)
        {

            $data["check1blueb"] = $this->input->post("check1blueb");
            $data["desc_item1blueb"] = mb_convert_kana($this->input->post("desc_item1blueb"), "as");
            $data["check2blueb"] = $this->input->post("check2blueb");
            $data["desc_item2blueb"] = mb_convert_kana($this->input->post("desc_item2blueb"), "as");
            $data["check3blueb"] = $this->input->post("check3blueb");
            $data["desc_item3blueb"] = mb_convert_kana($this->input->post("desc_item3blueb"), "as");
            $data["check4blueb"] = $this->input->post("check4blueb");
            $data["desc_item4blueb"] = mb_convert_kana($this->input->post("desc_item4blueb"), "as");
            $data["check5blueb"] = $this->input->post("check5blueb");
            $data["desc_item5blueb"] = mb_convert_kana($this->input->post("desc_item5blueb"), "as");
            $data["check6blueb"] = $this->input->post("check6blueb");
            $data["desc_item6blueb"] = mb_convert_kana($this->input->post("desc_item6blueb"), "as");
            $data["check7blueb"] = $this->input->post("check7blueb");
            $data["desc_item7blueb"] = mb_convert_kana($this->input->post("desc_item7blueb"), "as");
            $data["check8blueb"] = $this->input->post("check8blueb");
            $data["desc_item8blueb"] = mb_convert_kana($this->input->post("desc_item8blueb"), "as");
            $data["check9blueb"] = $this->input->post("check9blueb");
            $data["desc_item9blueb"] = mb_convert_kana($this->input->post("desc_item9blueb"), "as");
            $data["check10blueb"] = $this->input->post("check10blueb");
            $data["desc_item10blueb"] = mb_convert_kana($this->input->post("desc_item10blueb"), "as");
            $data["category_name1blueb"] = "工事工程表";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1blueb")))
            {
                $data["co_owner1blueb"] = $this->input->post("co_owner1blueb");
            }
            else
            {
                $data["co_owner1blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueb")))
            {
                $data["co_owner2blueb"] = $this->input->post("co_owner2blueb");
            }
            else
            {
                $data["co_owner2blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueb")))
            {
                $data["co_owner3blueb"] = $this->input->post("co_owner3blueb");
            }
            else
            {
                $data["co_owner3blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueb")))
            {
                $data["co_owner4blueb"] = $this->input->post("co_owner4blueb");
            }
            else
            {
                $data["co_owner4blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueb")))
            {
                $data["co_owner5blueb"] = $this->input->post("co_owner5blueb");
            }
            else
            {
                $data["co_owner5blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueb")))
            {
                $data["co_owner6blueb"] = $this->input->post("co_owner6blueb");
            }
            else
            {
                $data["co_owner6blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueb")))
            {
                $data["co_owner7blueb"] = $this->input->post("co_owner7blueb");
            }
            else
            {
                $data["co_owner7blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueb")))
            {
                $data["co_owner8blueb"] = $this->input->post("co_owner8blueb");
            }
            else
            {
                $data["co_owner8blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueb")))
            {
                $data["co_owner9blueb"] = $this->input->post("co_owner9blueb");
            }
            else
            {
                $data["co_owner9blueb"] = '';
            }

            if (!empty($this->input->post("co_owner10blueb")))
            {
                $data["co_owner10blueb"] = $this->input->post("co_owner10blueb");
            }
            else
            {
                $data["co_owner10blueb"] = '';
            }

            $data["constract_drawing1blueb"]= $this->input->post("constract_drawing1bluebtext");
            $data["constract_drawing2blueb"]= $this->input->post("constract_drawing2bluebtext");
            $data["constract_drawing3blueb"]= $this->input->post("constract_drawing3bluebtext");
            $data["constract_drawing4blueb"]= $this->input->post("constract_drawing4bluebtext");
            $data["constract_drawing5blueb"]= $this->input->post("constract_drawing5bluebtext");
            $data["constract_drawing6blueb"]= $this->input->post("constract_drawing6bluebtext");
            $data["constract_drawing7blueb"]= $this->input->post("constract_drawing7bluebtext");
            $data["constract_drawing8blueb"]= $this->input->post("constract_drawing8bluebtext");
            $data["constract_drawing9blueb"]= $this->input->post("constract_drawing9bluebtext");
            $data["constract_drawing10blueb"]= $this->input->post("constract_drawing10bluebtext");

            $data["date_insert_drawing1blueb"] = date('Y-m-d');
            $data["date_insert_drawing2blueb"] = date('Y-m-d');
            $data["date_insert_drawing3blueb"] = date('Y-m-d');
            $data["date_insert_drawing4blueb"] = date('Y-m-d');
            $data["date_insert_drawing5blueb"] = date('Y-m-d');
            $data["date_insert_drawing6blueb"] = date('Y-m-d');
            $data["date_insert_drawing7blueb"] = date('Y-m-d');
            $data["date_insert_drawing8blueb"] = date('Y-m-d');
            $data["date_insert_drawing9blueb"] = date('Y-m-d');
            $data["date_insert_drawing10blueb"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluectext")))
        // if($_FILES['constract_drawing1bluec']['size']!=0)
        {

            $data["check1bluec"] = $this->input->post("check1bluec");
            $data["desc_item1bluec"] = mb_convert_kana($this->input->post("desc_item1bluec"), "as");
            $data["check2bluec"] = $this->input->post("check2bluec");
            $data["desc_item2bluec"] = mb_convert_kana($this->input->post("desc_item2bluec"), "as");
            $data["check3bluec"] = $this->input->post("check3bluec");
            $data["desc_item3bluec"] = mb_convert_kana($this->input->post("desc_item3bluec"), "as");
            $data["check4bluec"] = $this->input->post("check4bluec");
            $data["desc_item4bluec"] = mb_convert_kana($this->input->post("desc_item4bluec"), "as");
            $data["check5bluec"] = $this->input->post("check5bluec");
            $data["desc_item5bluec"] = mb_convert_kana($this->input->post("desc_item5bluec"), "as");
            $data["check6bluec"] = $this->input->post("check6bluec");
            $data["desc_item6bluec"] = mb_convert_kana($this->input->post("desc_item6bluec"), "as");
            $data["check7bluec"] = $this->input->post("check7bluec");
            $data["desc_item7bluec"] = mb_convert_kana($this->input->post("desc_item7bluec"), "as");
            $data["check8bluec"] = $this->input->post("check8bluec");
            $data["desc_item8bluec"] = mb_convert_kana($this->input->post("desc_item8bluec"), "as");
            $data["check9bluec"] = $this->input->post("check9bluec");
            $data["desc_item9bluec"] = mb_convert_kana($this->input->post("desc_item9bluec"), "as");
            $data["check10bluec"] = $this->input->post("check10bluec");
            $data["desc_item10bluec"] = mb_convert_kana($this->input->post("desc_item10bluec"), "as");
            $data["category_name1bluec"] = "工事着工資料";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1bluec")))
            {
                $data["co_owner1bluec"] = $this->input->post("co_owner1bluec");
            }
            else
            {
                $data["co_owner1bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluec")))
            {
                $data["co_owner2bluec"] = $this->input->post("co_owner2bluec");
            }
            else
            {
                $data["co_owner2bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluec")))
            {
                $data["co_owner3bluec"] = $this->input->post("co_owner3bluec");
            }
            else
            {
                $data["co_owner3bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluec")))
            {
                $data["co_owner4bluec"] = $this->input->post("co_owner4bluec");
            }
            else
            {
                $data["co_owner4bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluec")))
            {
                $data["co_owner5bluec"] = $this->input->post("co_owner5bluec");
            }
            else
            {
                $data["co_owner5bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluec")))
            {
                $data["co_owner6bluec"] = $this->input->post("co_owner6bluec");
            }
            else
            {
                $data["co_owner6bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluec")))
            {
                $data["co_owner7bluec"] = $this->input->post("co_owner7bluec");
            }
            else
            {
                $data["co_owner7bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluec")))
            {
                $data["co_owner8bluec"] = $this->input->post("co_owner8bluec");
            }
            else
            {
                $data["co_owner8bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluec")))
            {
                $data["co_owner9bluec"] = $this->input->post("co_owner9bluec");
            }
            else
            {
                $data["co_owner9bluec"] = '';
            }

            if (!empty($this->input->post("co_owner10bluec")))
            {
                $data["co_owner10bluec"] = $this->input->post("co_owner10bluec");
            }
            else
            {
                $data["co_owner10bluec"] = '';
            }

            $data["constract_drawing1bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing2bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing3bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing4bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing5bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing6bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing7bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing8bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing9bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing10bluec"]= $this->input->post("constract_drawing1bluectext");


            $data["date_insert_drawing1bluec"] = date('Y-m-d');
            $data["date_insert_drawing2bluec"] = date('Y-m-d');
            $data["date_insert_drawing3bluec"] = date('Y-m-d');
            $data["date_insert_drawing4bluec"] = date('Y-m-d');
            $data["date_insert_drawing5bluec"] = date('Y-m-d');
            $data["date_insert_drawing6bluec"] = date('Y-m-d');
            $data["date_insert_drawing7bluec"] = date('Y-m-d');
            $data["date_insert_drawing8bluec"] = date('Y-m-d');
            $data["date_insert_drawing9bluec"] = date('Y-m-d');
            $data["date_insert_drawing10bluec"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluedtext")))
        // if($_FILES['constract_drawing1blued']['size']!=0)
        {


            $data["check1blued"] = $this->input->post("check1blued");
            $data["desc_item1blued"] = mb_convert_kana($this->input->post("desc_item1blued"), "as");
            $data["check2blued"] = $this->input->post("check2blued");
            $data["desc_item2blued"] = mb_convert_kana($this->input->post("desc_item2blued"), "as");
            $data["check3blued"] = $this->input->post("check3blued");
            $data["desc_item3blued"] = mb_convert_kana($this->input->post("desc_item3blued"), "as");
            $data["check4blued"] = $this->input->post("check4blued");
            $data["desc_item4blued"] = mb_convert_kana($this->input->post("desc_item4blued"), "as");
            $data["check5blued"] = $this->input->post("check5blued");
            $data["desc_item5blued"] = mb_convert_kana($this->input->post("desc_item5blued"), "as");
            $data["check6blued"] = $this->input->post("check6blued");
            $data["desc_item6blued"] = mb_convert_kana($this->input->post("desc_item6blued"), "as");
            $data["check7blued"] = $this->input->post("check7blued");
            $data["desc_item7blued"] = mb_convert_kana($this->input->post("desc_item7blued"), "as");
            $data["check8blued"] = $this->input->post("check8blued");
            $data["desc_item8blued"] = mb_convert_kana($this->input->post("desc_item8blued"), "as");
            $data["check9blued"] = $this->input->post("check9blued");
            $data["desc_item9blued"] = mb_convert_kana($this->input->post("desc_item9blued"), "as");
            $data["check10blued"] = $this->input->post("check10blued");
            $data["desc_item10blued"] = mb_convert_kana($this->input->post("desc_item10blued"), "as");
            $data["category_name1blued"] = "施工図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blued")))
            {
                $data["co_owner1blued"] = $this->input->post("co_owner1blued");
            }
            else
            {
                $data["co_owner1blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blued")))
            {
                $data["co_owner2blued"] = $this->input->post("co_owner2blued");
            }
            else
            {
                $data["co_owner2blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blued")))
            {
                $data["co_owner3blued"] = $this->input->post("co_owner3blued");
            }
            else
            {
                $data["co_owner3blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blued")))
            {
                $data["co_owner4blued"] = $this->input->post("co_owner4blued");
            }
            else
            {
                $data["co_owner4blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blued")))
            {
                $data["co_owner5blued"] = $this->input->post("co_owner5blued");
            }
            else
            {
                $data["co_owner5blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blued")))
            {
                $data["co_owner6blued"] = $this->input->post("co_owner6blued");
            }
            else
            {
                $data["co_owner6blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blued")))
            {
                $data["co_owner7blued"] = $this->input->post("co_owner7blued");
            }
            else
            {
                $data["co_owner7blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blued")))
            {
                $data["co_owner8blued"] = $this->input->post("co_owner8blued");
            }
            else
            {
                $data["co_owner8blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blued")))
            {
                $data["co_owner9blued"] = $this->input->post("co_owner9blued");
            }
            else
            {
                $data["co_owner9blued"] = '';
            }

            if (!empty($this->input->post("co_owner10blued")))
            {
                $data["co_owner10blued"] = $this->input->post("co_owner10blued");
            }
            else
            {
                $data["co_owner10blued"] = '';
            }
            
            $data["constract_drawing1blued"]= $this->input->post("constract_drawing1bluedtext");
            $data["constract_drawing2blued"]= $this->input->post("constract_drawing2bluedtext");
            $data["constract_drawing3blued"]= $this->input->post("constract_drawing3bluedtext");
            $data["constract_drawing4blued"]= $this->input->post("constract_drawing4bluedtext");
            $data["constract_drawing5blued"]= $this->input->post("constract_drawing5bluedtext");
            $data["constract_drawing6blued"]= $this->input->post("constract_drawing6bluedtext");
            $data["constract_drawing7blued"]= $this->input->post("constract_drawing7bluedtext");
            $data["constract_drawing8blued"]= $this->input->post("constract_drawing8bluedtext");
            $data["constract_drawing9blued"]= $this->input->post("constract_drawing9bluedtext");
            $data["constract_drawing10blued"]= $this->input->post("constract_drawing10bluedtext");


            $data["date_insert_drawing1blued"] = date('Y-m-d');
            $data["date_insert_drawing2blued"] = date('Y-m-d');
            $data["date_insert_drawing3blued"] = date('Y-m-d');
            $data["date_insert_drawing4blued"] = date('Y-m-d');
            $data["date_insert_drawing5blued"] = date('Y-m-d');
            $data["date_insert_drawing6blued"] = date('Y-m-d');
            $data["date_insert_drawing7blued"] = date('Y-m-d');
            $data["date_insert_drawing8blued"] = date('Y-m-d');
            $data["date_insert_drawing9blued"] = date('Y-m-d');
            $data["date_insert_drawing10blued"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueetext")))
        // if($_FILES['constract_drawing1bluee']['size']!=0)
        {

            $data["check1bluee"] = $this->input->post("check1bluee");
            $data["desc_item1bluee"] = mb_convert_kana($this->input->post("desc_item1bluee"), "as");
            $data["check2bluee"] = $this->input->post("check2bluee");
            $data["desc_item2bluee"] = mb_convert_kana($this->input->post("desc_item2bluee"), "as");
            $data["check3bluee"] = $this->input->post("check3bluee");
            $data["desc_item3bluee"] = mb_convert_kana($this->input->post("desc_item3bluee"), "as");
            $data["check4bluee"] = $this->input->post("check4bluee");
            $data["desc_item4bluee"] = mb_convert_kana($this->input->post("desc_item4bluee"), "as");
            $data["check5bluee"] = $this->input->post("check5bluee");
            $data["desc_item5bluee"] = mb_convert_kana($this->input->post("desc_item5bluee"), "as");
            $data["check6bluee"] = $this->input->post("check6bluee");
            $data["desc_item6bluee"] = mb_convert_kana($this->input->post("desc_item6bluee"), "as");
            $data["check7bluee"] = $this->input->post("check7bluee");
            $data["desc_item7bluee"] = mb_convert_kana($this->input->post("desc_item7bluee"), "as");
            $data["check8bluee"] = $this->input->post("check8bluee");
            $data["desc_item8bluee"] = mb_convert_kana($this->input->post("desc_item8bluee"), "as");
            $data["check9bluee"] = $this->input->post("check9bluee");
            $data["desc_item9bluee"] = mb_convert_kana($this->input->post("desc_item9bluee"), "as");
            $data["check10bluee"] = $this->input->post("check10bluee");
            $data["desc_item10bluee"] = mb_convert_kana($this->input->post("desc_item10bluee"), "as");
            $data["category_name1bluee"] = "変更図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluee")))
            {
                $data["co_owner1bluee"] = $this->input->post("co_owner1bluee");
            }
            else
            {
                $data["co_owner1bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluee")))
            {
                $data["co_owner2bluee"] = $this->input->post("co_owner2bluee");
            }
            else
            {
                $data["co_owner2bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluee")))
            {
                $data["co_owner3bluee"] = $this->input->post("co_owner3bluee");
            }
            else
            {
                $data["co_owner3bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluee")))
            {
                $data["co_owner4bluee"] = $this->input->post("co_owner4bluee");
            }
            else
            {
                $data["co_owner4bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluee")))
            {
                $data["co_owner5bluee"] = $this->input->post("co_owner5bluee");
            }
            else
            {
                $data["co_owner5bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluee")))
            {
                $data["co_owner6bluee"] = $this->input->post("co_owner6bluee");
            }
            else
            {
                $data["co_owner6bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluee")))
            {
                $data["co_owner7bluee"] = $this->input->post("co_owner7bluee");
            }
            else
            {
                $data["co_owner7bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluee")))
            {
                $data["co_owner8bluee"] = $this->input->post("co_owner8bluee");
            }
            else
            {
                $data["co_owner8bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluee")))
            {
                $data["co_owner9bluee"] = $this->input->post("co_owner9bluee");
            }
            else
            {
                $data["co_owner9bluee"] = '';
            }

            if (!empty($this->input->post("co_owner10bluee")))
            {
                $data["co_owner10bluee"] = $this->input->post("co_owner10bluee");
            }
            else
            {
                $data["co_owner10bluee"] = '';
            }


            $data["constract_drawing1bluee"]= $this->input->post("constract_drawing1blueetext");
            $data["constract_drawing2bluee"]= $this->input->post("constract_drawing2blueetext");
            $data["constract_drawing3bluee"]= $this->input->post("constract_drawing3blueetext");
            $data["constract_drawing4bluee"]= $this->input->post("constract_drawing4blueetext");
            $data["constract_drawing5bluee"]= $this->input->post("constract_drawing5blueetext");
            $data["constract_drawing6bluee"]= $this->input->post("constract_drawing6blueetext");
            $data["constract_drawing7bluee"]= $this->input->post("constract_drawing7blueetext");
            $data["constract_drawing8bluee"]= $this->input->post("constract_drawing8blueetext");
            $data["constract_drawing9bluee"]= $this->input->post("constract_drawing9blueetext");
            $data["constract_drawing10bluee"]= $this->input->post("constract_drawing10blueetext");


            $data["date_insert_drawing1bluee"] = date('Y-m-d');
            $data["date_insert_drawing2bluee"] = date('Y-m-d');
            $data["date_insert_drawing3bluee"] = date('Y-m-d');
            $data["date_insert_drawing4bluee"] = date('Y-m-d');
            $data["date_insert_drawing5bluee"] = date('Y-m-d');
            $data["date_insert_drawing6bluee"] = date('Y-m-d');
            $data["date_insert_drawing7bluee"] = date('Y-m-d');
            $data["date_insert_drawing8bluee"] = date('Y-m-d');
            $data["date_insert_drawing9bluee"] = date('Y-m-d');
            $data["date_insert_drawing10bluee"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueftext")))
        // if($_FILES['constract_drawing1bluef']['size']!=0)
        {


            $data["check1bluef"] = $this->input->post("check1bluef");
            $data["desc_item1bluef"] = mb_convert_kana($this->input->post("desc_item1bluef"), "as");
            $data["check2bluef"] = $this->input->post("check2bluef");
            $data["desc_item2bluef"] = mb_convert_kana($this->input->post("desc_item2bluef"), "as");
            $data["check3bluef"] = $this->input->post("check3bluef");
            $data["desc_item3bluef"] = mb_convert_kana($this->input->post("desc_item3bluef"), "as");
            $data["check4bluef"] = $this->input->post("check4bluef");
            $data["desc_item4bluef"] = mb_convert_kana($this->input->post("desc_item4bluef"), "as");
            $data["check5bluef"] = $this->input->post("check5bluef");
            $data["desc_item5bluef"] = mb_convert_kana($this->input->post("desc_item5bluef"), "as");
            $data["check6bluef"] = $this->input->post("check6bluef");
            $data["desc_item6bluef"] = mb_convert_kana($this->input->post("desc_item6bluef"), "as");
            $data["check7bluef"] = $this->input->post("check7bluef");
            $data["desc_item7bluef"] = mb_convert_kana($this->input->post("desc_item7bluef"), "as");
            $data["check8bluef"] = $this->input->post("check8bluef");
            $data["desc_item8bluef"] = mb_convert_kana($this->input->post("desc_item8bluef"), "as");
            $data["check9bluef"] = $this->input->post("check9bluef");
            $data["desc_item9bluef"] = mb_convert_kana($this->input->post("desc_item9bluef"), "as");
            $data["check10bluef"] = $this->input->post("check10bluef");
            $data["desc_item10bluef"] = mb_convert_kana($this->input->post("desc_item10bluef"), "as");
            $data["category_name1bluef"] = "設備プランシート";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluef")))
            {
                $data["co_owner1bluef"] = $this->input->post("co_owner1bluef");
            }
            else
            {
                $data["co_owner1bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluef")))
            {
                $data["co_owner2bluef"] = $this->input->post("co_owner2bluef");
            }
            else
            {
                $data["co_owner2bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluef")))
            {
                $data["co_owner3bluef"] = $this->input->post("co_owner3bluef");
            }
            else
            {
                $data["co_owner3bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluef")))
            {
                $data["co_owner4bluef"] = $this->input->post("co_owner4bluef");
            }
            else
            {
                $data["co_owner4bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluef")))
            {
                $data["co_owner5bluef"] = $this->input->post("co_owner5bluef");
            }
            else
            {
                $data["co_owner5bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluef")))
            {
                $data["co_owner6bluef"] = $this->input->post("co_owner6bluef");
            }
            else
            {
                $data["co_owner6bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluef")))
            {
                $data["co_owner7bluef"] = $this->input->post("co_owner7bluef");
            }
            else
            {
                $data["co_owner7bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluef")))
            {
                $data["co_owner8bluef"] = $this->input->post("co_owner8bluef");
            }
            else
            {
                $data["co_owner8bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluef")))
            {
                $data["co_owner9bluef"] = $this->input->post("co_owner9bluef");
            }
            else
            {
                $data["co_owner9bluef"] = '';
            }

            if (!empty($this->input->post("co_owner10bluef")))
            {
                $data["co_owner10bluef"] = $this->input->post("co_owner10bluef");
            }
            else
            {
                $data["co_owner10bluef"] = '';
            }


            $data["constract_drawing1bluef"]= $this->input->post("constract_drawing1blueftext");
            $data["constract_drawing2bluef"]= $this->input->post("constract_drawing2blueftext");
            $data["constract_drawing3bluef"]= $this->input->post("constract_drawing3blueftext");
            $data["constract_drawing4bluef"]= $this->input->post("constract_drawing4blueftext");
            $data["constract_drawing5bluef"]= $this->input->post("constract_drawing5blueftext");
            $data["constract_drawing6bluef"]= $this->input->post("constract_drawing6blueftext");
            $data["constract_drawing7bluef"]= $this->input->post("constract_drawing7blueftext");
            $data["constract_drawing8bluef"]= $this->input->post("constract_drawing8blueftext");
            $data["constract_drawing9bluef"]= $this->input->post("constract_drawing9blueftext");
            $data["constract_drawing10bluef"]= $this->input->post("constract_drawing10blueftext");


            $data["date_insert_drawing1bluef"] = date('Y-m-d');
            $data["date_insert_drawing2bluef"] = date('Y-m-d');
            $data["date_insert_drawing3bluef"] = date('Y-m-d');
            $data["date_insert_drawing4bluef"] = date('Y-m-d');
            $data["date_insert_drawing5bluef"] = date('Y-m-d');
            $data["date_insert_drawing6bluef"] = date('Y-m-d');
            $data["date_insert_drawing7bluef"] = date('Y-m-d');
            $data["date_insert_drawing8bluef"] = date('Y-m-d');
            $data["date_insert_drawing9bluef"] = date('Y-m-d');
            $data["date_insert_drawing10bluef"] = date('Y-m-d');
        }
         
        if(!empty($this->input->post("constract_drawing1bluegtext")))
        // if($_FILES['constract_drawing1blueg']['size']!=0)
        {


            $data["check1blueg"] = $this->input->post("check1blueg");
            $data["desc_item1blueg"] = mb_convert_kana($this->input->post("desc_item1blueg"), "as");
            $data["check2blueg"] = $this->input->post("check2blueg");
            $data["desc_item2blueg"] = mb_convert_kana($this->input->post("desc_item2blueg"), "as");
            $data["check3blueg"] = $this->input->post("check3blueg");
            $data["desc_item3blueg"] = mb_convert_kana($this->input->post("desc_item3blueg"), "as");
            $data["check4blueg"] = $this->input->post("check4blueg");
            $data["desc_item4blueg"] = mb_convert_kana($this->input->post("desc_item4blueg"), "as");
            $data["check5blueg"] = $this->input->post("check5blueg");
            $data["desc_item5blueg"] = mb_convert_kana($this->input->post("desc_item5blueg"), "as");
            $data["check6blueg"] = $this->input->post("check6blueg");
            $data["desc_item6blueg"] = mb_convert_kana($this->input->post("desc_item6blueg"), "as");
            $data["check7blueg"] = $this->input->post("check7blueg");
            $data["desc_item7blueg"] = mb_convert_kana($this->input->post("desc_item7blueg"), "as");
            $data["check8blueg"] = $this->input->post("check8blueg");
            $data["desc_item8blueg"] = mb_convert_kana($this->input->post("desc_item8blueg"), "as");
            $data["check9blueg"] = $this->input->post("check9blueg");
            $data["desc_item9blueg"] = mb_convert_kana($this->input->post("desc_item9blueg"), "as");
            $data["check10blueg"] = $this->input->post("check10blueg");
            $data["desc_item10blueg"] = mb_convert_kana($this->input->post("desc_item10blueg"), "as");
            $data["category_name1blueg"] = "その他";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blueg")))
            {
                $data["co_owner1blueg"] = $this->input->post("co_owner1blueg");
            }
            else
            {
                $data["co_owner1blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueg")))
            {
                $data["co_owner2blueg"] = $this->input->post("co_owner2blueg");
            }
            else
            {
                $data["co_owner2blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueg")))
            {
                $data["co_owner3blueg"] = $this->input->post("co_owner3blueg");
            }
            else
            {
                $data["co_owner3blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueg")))
            {
                $data["co_owner4blueg"] = $this->input->post("co_owner4blueg");
            }
            else
            {
                $data["co_owner4blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueg")))
            {
                $data["co_owner5blueg"] = $this->input->post("co_owner5blueg");
            }
            else
            {
                $data["co_owner5blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueg")))
            {
                $data["co_owner6blueg"] = $this->input->post("co_owner6blueg");
            }
            else
            {
                $data["co_owner6blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueg")))
            {
                $data["co_owner7blueg"] = $this->input->post("co_owner7blueg");
            }
            else
            {
                $data["co_owner7blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueg")))
            {
                $data["co_owner8blueg"] = $this->input->post("co_owner8blueg");
            }
            else
            {
                $data["co_owner8blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueg")))
            {
                $data["co_owner9blueg"] = $this->input->post("co_owner9blueg");
            }
            else
            {
                $data["co_owner9blueg"] = '';
            }

            if (!empty($this->input->post("co_owner10blueg")))
            {
                $data["co_owner10blueg"] = $this->input->post("co_owner10blueg");
            }
            else
            {
                $data["co_owner10blueg"] = '';
            }

            $data["constract_drawing1blueg"]= $this->input->post("constract_drawing1bluegtext");
            $data["constract_drawing2blueg"]= $this->input->post("constract_drawing2bluegtext");
            $data["constract_drawing3blueg"]= $this->input->post("constract_drawing3bluegtext");
            $data["constract_drawing4blueg"]= $this->input->post("constract_drawing4bluegtext");
            $data["constract_drawing5blueg"]= $this->input->post("constract_drawing5bluegtext");
            $data["constract_drawing6blueg"]= $this->input->post("constract_drawing6bluegtext");
            $data["constract_drawing7blueg"]= $this->input->post("constract_drawing7bluegtext");
            $data["constract_drawing8blueg"]= $this->input->post("constract_drawing8bluegtext");
            $data["constract_drawing9blueg"]= $this->input->post("constract_drawing9bluegtext");
            $data["constract_drawing10blueg"]= $this->input->post("constract_drawing10bluegtext");


            $data["date_insert_drawing1blueg"] = date('Y-m-d');
            $data["date_insert_drawing2blueg"] = date('Y-m-d');
            $data["date_insert_drawing3blueg"] = date('Y-m-d');
            $data["date_insert_drawing4blueg"] = date('Y-m-d');
            $data["date_insert_drawing5blueg"] = date('Y-m-d');
            $data["date_insert_drawing6blueg"] = date('Y-m-d');
            $data["date_insert_drawing7blueg"] = date('Y-m-d');
            $data["date_insert_drawing8blueg"] = date('Y-m-d');
            $data["date_insert_drawing9blueg"] = date('Y-m-d');
            $data["date_insert_drawing10blueg"] = date('Y-m-d');
        }

        //admin//

        if(!empty($this->input->post("constract_drawing1text")))
        // if($_FILES['constract_drawing1']['size']!=0)
        {

         
            $data["check1"] = $this->input->post("check1");
            $data["desc_item1"] = mb_convert_kana($this->input->post("desc_item1"), "as");
            $data["check2"] = $this->input->post("check2");
            $data["desc_item2"] = mb_convert_kana($this->input->post("desc_item2"), "as");
            $data["check3"] = $this->input->post("check3");
            $data["desc_item3"] = mb_convert_kana($this->input->post("desc_item3"), "as");
            $data["check4"] = $this->input->post("check4");
            $data["desc_item4"] = mb_convert_kana($this->input->post("desc_item4"), "as");
            $data["check5"] = $this->input->post("check5");
            $data["desc_item5"] = mb_convert_kana($this->input->post("desc_item5"), "as");
            $data["check6"] = $this->input->post("check6");
            $data["desc_item6"] = mb_convert_kana($this->input->post("desc_item6"), "as");
            $data["check7"] = $this->input->post("check7");
            $data["desc_item7"] = mb_convert_kana($this->input->post("desc_item7"), "as");
            $data["check8"] = $this->input->post("check8");
            $data["desc_item8"] = mb_convert_kana($this->input->post("desc_item8"), "as");
            $data["check9"] = $this->input->post("check9");
            $data["desc_item9"] = mb_convert_kana($this->input->post("desc_item9"), "as");
            $data["check10"] = $this->input->post("check10");
            $data["desc_item10"] = mb_convert_kana($this->input->post("desc_item10"), "as");
            $data["category_name"] = "請負契約書・土地契約書";
            // $data["type_item"] = "admin";


            $data["constract_drawing1"]= $this->input->post("constract_drawing1text");
            $data["constract_drawing2"]= $this->input->post("constract_drawing2text");
            $data["constract_drawing3"]= $this->input->post("constract_drawing3text");
            $data["constract_drawing4"]= $this->input->post("constract_drawing4text");
            $data["constract_drawing5"]= $this->input->post("constract_drawing5text");
            $data["constract_drawing6"]= $this->input->post("constract_drawing6text");
            $data["constract_drawing7"]= $this->input->post("constract_drawing7text");
            $data["constract_drawing8"]= $this->input->post("constract_drawing8text");
            $data["constract_drawing9"]= $this->input->post("constract_drawing9text");
            $data["constract_drawing10"]= $this->input->post("constract_drawing10text");


            $data["date_insert_drawing1"] = date('Y-m-d');
            $data["date_insert_drawing2"] = date('Y-m-d');
            $data["date_insert_drawing3"] = date('Y-m-d');
            $data["date_insert_drawing4"] = date('Y-m-d');
            $data["date_insert_drawing5"] = date('Y-m-d');
            $data["date_insert_drawing6"] = date('Y-m-d');
            $data["date_insert_drawing7"] = date('Y-m-d');
            $data["date_insert_drawing8"] = date('Y-m-d');
            $data["date_insert_drawing9"] = date('Y-m-d');
            $data["date_insert_drawing10"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1btext")))
        // if($_FILES['constract_drawing1b']['size']!=0)
        {

            $data["check1b"] = $this->input->post("check1b");
            $data["desc_item1b"] = mb_convert_kana($this->input->post("desc_item1b"), "as");
            $data["check2b"] = $this->input->post("check2b");
            $data["desc_item2b"] = mb_convert_kana($this->input->post("desc_item2b"), "as");
            $data["check3b"] = $this->input->post("check3b");
            $data["desc_item3b"] = mb_convert_kana($this->input->post("desc_item3b"), "as");
            $data["check4b"] = $this->input->post("check4b");
            $data["desc_item4b"] = mb_convert_kana($this->input->post("desc_item4b"), "as");
            $data["check5b"] = $this->input->post("check5b");
            $data["desc_item5b"] = mb_convert_kana($this->input->post("desc_item5b"), "as");
            $data["check6b"] = $this->input->post("check6b");
            $data["desc_item6b"] = mb_convert_kana($this->input->post("desc_item6b"), "as");
            $data["check7b"] = $this->input->post("check7b");
            $data["desc_item7b"] = mb_convert_kana($this->input->post("desc_item7b"), "as");
            $data["check8b"] = $this->input->post("check8b");
            $data["desc_item8b"] = mb_convert_kana($this->input->post("desc_item8b"), "as");
            $data["check9b"] = $this->input->post("check9b");
            $data["desc_item9b"] = mb_convert_kana($this->input->post("desc_item9b"), "as");
            $data["check10b"] = $this->input->post("check10b");
            $data["desc_item10b"] = mb_convert_kana($this->input->post("desc_item10b"), "as");
            $data["category_name1b"] = "長期優良住宅認定書・性能評価書";
            // $data["type_item"] = "admin";

            $data["constract_drawing1b"]= $this->input->post("constract_drawing1btext");
            $data["constract_drawing2b"]= $this->input->post("constract_drawing2btext");
            $data["constract_drawing3b"]= $this->input->post("constract_drawing3btext");
            $data["constract_drawing4b"]= $this->input->post("constract_drawing4btext");
            $data["constract_drawing5b"]= $this->input->post("constract_drawing5btext");
            $data["constract_drawing6b"]= $this->input->post("constract_drawing6btext");
            $data["constract_drawing7b"]= $this->input->post("constract_drawing7btext");
            $data["constract_drawing8b"]= $this->input->post("constract_drawing8btext");
            $data["constract_drawing9b"]= $this->input->post("constract_drawing9btext");
            $data["constract_drawing10b"]= $this->input->post("constract_drawing10btext");

            $data["date_insert_drawing1b"] = date('Y-m-d');
            $data["date_insert_drawing2b"] = date('Y-m-d');
            $data["date_insert_drawing3b"] = date('Y-m-d');
            $data["date_insert_drawing4b"] = date('Y-m-d');
            $data["date_insert_drawing5b"] = date('Y-m-d');
            $data["date_insert_drawing6b"] = date('Y-m-d');
            $data["date_insert_drawing7b"] = date('Y-m-d');
            $data["date_insert_drawing8b"] = date('Y-m-d');
            $data["date_insert_drawing9b"] = date('Y-m-d');
            $data["date_insert_drawing10b"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ctext")))
        // if($_FILES['constract_drawing1c']['size']!=0)
        {

            $data["check1c"] = $this->input->post("check1c");
            $data["desc_item1c"] = mb_convert_kana($this->input->post("desc_item1c"), "as");
            $data["check2c"] = $this->input->post("check2c");
            $data["desc_item2c"] = mb_convert_kana($this->input->post("desc_item2c"), "as");
            $data["check3c"] = $this->input->post("check3c");
            $data["desc_item3c"] = mb_convert_kana($this->input->post("desc_item3c"), "as");
            $data["check4c"] = $this->input->post("check4c");
            $data["desc_item4c"] = mb_convert_kana($this->input->post("desc_item4c"), "as");
            $data["check5c"] = $this->input->post("check5c");
            $data["desc_item5c"] = mb_convert_kana($this->input->post("desc_item5c"), "as");
            $data["check6c"] = $this->input->post("check6c");
            $data["desc_item6c"] = mb_convert_kana($this->input->post("desc_item6c"), "as");
            $data["check7c"] = $this->input->post("check7c");
            $data["desc_item7c"] = mb_convert_kana($this->input->post("desc_item7c"), "as");
            $data["check8c"] = $this->input->post("check8c");
            $data["desc_item8c"] = mb_convert_kana($this->input->post("desc_item8c"), "as");
            $data["check9c"] = $this->input->post("check9c");
            $data["desc_item9c"] = mb_convert_kana($this->input->post("desc_item9c"), "as");
            $data["check10c"] = $this->input->post("check10c");
            $data["desc_item10c"] = mb_convert_kana($this->input->post("desc_item10c"), "as");
            $data["category_name1c"] = "地盤調査・地盤改良";
            // $data["type_item"] = "admin";


            $data["constract_drawing1c"]= $this->input->post("constract_drawing1ctext");
            $data["constract_drawing2c"]= $this->input->post("constract_drawing2ctext");
            $data["constract_drawing3c"]= $this->input->post("constract_drawing3ctext");
            $data["constract_drawing4c"]= $this->input->post("constract_drawing4ctext");
            $data["constract_drawing5c"]= $this->input->post("constract_drawing5ctext");
            $data["constract_drawing6c"]= $this->input->post("constract_drawing6ctext");
            $data["constract_drawing7c"]= $this->input->post("constract_drawing7ctext");
            $data["constract_drawing8c"]= $this->input->post("constract_drawing8ctext");
            $data["constract_drawing9c"]= $this->input->post("constract_drawing9ctext");
            $data["constract_drawing10c"]= $this->input->post("constract_drawing10ctext");

            $data["date_insert_drawing1c"] = date('Y-m-d');
            $data["date_insert_drawing2c"] = date('Y-m-d');
            $data["date_insert_drawing3c"] = date('Y-m-d');
            $data["date_insert_drawing4c"] = date('Y-m-d');
            $data["date_insert_drawing5c"] = date('Y-m-d');
            $data["date_insert_drawing6c"] = date('Y-m-d');
            $data["date_insert_drawing7c"] = date('Y-m-d');
            $data["date_insert_drawing8c"] = date('Y-m-d');
            $data["date_insert_drawing9c"] = date('Y-m-d');
            $data["date_insert_drawing10c"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1dtext")))
        // if($_FILES['constract_drawing1d']['size']!=0)
        {


            $data["check1d"] = $this->input->post("check1d");
            $data["desc_item1d"] = mb_convert_kana($this->input->post("desc_item1d"), "as");
            $data["check2d"] = $this->input->post("check2d");
            $data["desc_item2d"] = mb_convert_kana($this->input->post("desc_item2d"), "as");
            $data["check3d"] = $this->input->post("check3d");
            $data["desc_item3d"] = mb_convert_kana($this->input->post("desc_item3d"), "as");
            $data["check4d"] = $this->input->post("check4d");
            $data["desc_item4d"] = mb_convert_kana($this->input->post("desc_item4d"), "as");
            $data["check5d"] = $this->input->post("check5d");
            $data["desc_item5d"] = mb_convert_kana($this->input->post("desc_item5d"), "as");
            $data["check6d"] = $this->input->post("check6d");
            $data["desc_item6d"] = mb_convert_kana($this->input->post("desc_item6d"), "as");
            $data["check7d"] = $this->input->post("check7d");
            $data["desc_item7d"] = mb_convert_kana($this->input->post("desc_item7d"), "as");
            $data["check8d"] = $this->input->post("check8d");
            $data["desc_item8d"] = mb_convert_kana($this->input->post("desc_item8d"), "as");
            $data["check9d"] = $this->input->post("check9d");
            $data["desc_item9d"] = mb_convert_kana($this->input->post("desc_item9d"), "as");
            $data["check10d"] = $this->input->post("check10d");
            $data["desc_item10d"] = mb_convert_kana($this->input->post("desc_item10d"), "as");
            $data["category_name1d"] = "点検履歴";
            // $data["type_item"] = "admin";


            $data["constract_drawing1d"]= $this->input->post("constract_drawing1dtext");
            $data["constract_drawing2d"]= $this->input->post("constract_drawing2dtext");
            $data["constract_drawing3d"]= $this->input->post("constract_drawing3dtext");
            $data["constract_drawing4d"]= $this->input->post("constract_drawing4dtext");
            $data["constract_drawing5d"]= $this->input->post("constract_drawing5dtext");
            $data["constract_drawing6d"]= $this->input->post("constract_drawing6dtext");
            $data["constract_drawing7d"]= $this->input->post("constract_drawing7dtext");
            $data["constract_drawing8d"]= $this->input->post("constract_drawing8dtext");
            $data["constract_drawing9d"]= $this->input->post("constract_drawing9dtext");
            $data["constract_drawing10d"]= $this->input->post("constract_drawing10dtext");

            $data["date_insert_drawing1d"] = date('Y-m-d');
            $data["date_insert_drawing2d"] = date('Y-m-d');
            $data["date_insert_drawing3d"] = date('Y-m-d');
            $data["date_insert_drawing4d"] = date('Y-m-d');
            $data["date_insert_drawing5d"] = date('Y-m-d');
            $data["date_insert_drawing6d"] = date('Y-m-d');
            $data["date_insert_drawing7d"] = date('Y-m-d');
            $data["date_insert_drawing8d"] = date('Y-m-d');
            $data["date_insert_drawing9d"] = date('Y-m-d');
            $data["date_insert_drawing10d"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1etext")))
        // if($_FILES['constract_drawing1e']['size']!=0)
        {

            $data["check1e"] = $this->input->post("check1e");
            $data["desc_item1e"] = mb_convert_kana($this->input->post("desc_item1e"), "as");
            $data["check2e"] = $this->input->post("check2e");
            $data["desc_item2e"] = mb_convert_kana($this->input->post("desc_item2e"), "as");
            $data["check3e"] = $this->input->post("check3e");
            $data["desc_item3e"] = mb_convert_kana($this->input->post("desc_item3e"), "as");
            $data["check4e"] = $this->input->post("check4e");
            $data["desc_item4e"] = mb_convert_kana($this->input->post("desc_item4e"), "as");
            $data["check5e"] = $this->input->post("check5e");
            $data["desc_item5e"] = mb_convert_kana($this->input->post("desc_item5e"), "as");
            $data["check6e"] = $this->input->post("check6e");
            $data["desc_item6e"] = mb_convert_kana($this->input->post("desc_item6e"), "as");
            $data["check7e"] = $this->input->post("check7e");
            $data["desc_item7e"] = mb_convert_kana($this->input->post("desc_item7e"), "as");
            $data["check8e"] = $this->input->post("check8e");
            $data["desc_item8e"] = mb_convert_kana($this->input->post("desc_item8e"), "as");
            $data["check9e"] = $this->input->post("check9e");
            $data["desc_item9e"] = mb_convert_kana($this->input->post("desc_item9e"), "as");
            $data["check10e"] = $this->input->post("check10e");
            $data["desc_item10e"] = mb_convert_kana($this->input->post("desc_item10e"), "as");
            $data["category_name1e"] = "三者引継ぎ合意書";
            // $data["type_item"] = "admin";
 
            $data["constract_drawing1e"]= $this->input->post("constract_drawing1etext");
            $data["constract_drawing2e"]= $this->input->post("constract_drawing2etext");
            $data["constract_drawing3e"]= $this->input->post("constract_drawing3etext");
            $data["constract_drawing4e"]= $this->input->post("constract_drawing4etext");
            $data["constract_drawing5e"]= $this->input->post("constract_drawing5etext");
            $data["constract_drawing6e"]= $this->input->post("constract_drawing6etext");
            $data["constract_drawing7e"]= $this->input->post("constract_drawing7etext");
            $data["constract_drawing8e"]= $this->input->post("constract_drawing8etext");
            $data["constract_drawing9e"]= $this->input->post("constract_drawing9etext");
            $data["constract_drawing10e"]= $this->input->post("constract_drawing10etext");

            $data["date_insert_drawing1e"] = date('Y-m-d');
            $data["date_insert_drawing2e"] = date('Y-m-d');
            $data["date_insert_drawing3e"] = date('Y-m-d');
            $data["date_insert_drawing4e"] = date('Y-m-d');
            $data["date_insert_drawing5e"] = date('Y-m-d');
            $data["date_insert_drawing6e"] = date('Y-m-d');
            $data["date_insert_drawing7e"] = date('Y-m-d');
            $data["date_insert_drawing8e"] = date('Y-m-d');
            $data["date_insert_drawing9e"] = date('Y-m-d');
            $data["date_insert_drawing10e"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ftext")))
        // if($_FILES['constract_drawing1f']['size']!=0)
        {

            $data["check1f"] = $this->input->post("check1f");
            $data["desc_item1f"] = mb_convert_kana($this->input->post("desc_item1f"), "as");
            $data["check2f"] = $this->input->post("check2f");
            $data["desc_item2f"] = mb_convert_kana($this->input->post("desc_item2f"), "as");
            $data["check3f"] = $this->input->post("check3f");
            $data["desc_item3f"] = mb_convert_kana($this->input->post("desc_item3f"), "as");
            $data["check4f"] = $this->input->post("check4f");
            $data["desc_item4f"] = mb_convert_kana($this->input->post("desc_item4f"), "as");
            $data["check5f"] = $this->input->post("check5f");
            $data["desc_item5f"] = mb_convert_kana($this->input->post("desc_item5f"), "as");
            $data["check6f"] = $this->input->post("check6f");
            $data["desc_item6f"] = mb_convert_kana($this->input->post("desc_item6f"), "as");
            $data["check7f"] = $this->input->post("check7f");
            $data["desc_item7f"] = mb_convert_kana($this->input->post("desc_item7f"), "as");
            $data["check8f"] = $this->input->post("check8f");
            $data["desc_item8f"] = mb_convert_kana($this->input->post("desc_item8f"), "as");
            $data["check9f"] = $this->input->post("check9f");
            $data["desc_item9f"] = mb_convert_kana($this->input->post("desc_item9f"), "as");
            $data["check10f"] = $this->input->post("check10f");
            $data["desc_item10f"] = mb_convert_kana($this->input->post("desc_item10f"), "as");
            $data["category_name1f"] = "住宅仕様書・プレカット資料";
            // $data["type_item"] = "admin";

            $data["constract_drawing1f"]= $this->input->post("constract_drawing1ftext");
            $data["constract_drawing2f"]= $this->input->post("constract_drawing2ftext");
            $data["constract_drawing3f"]= $this->input->post("constract_drawing3ftext");
            $data["constract_drawing4f"]= $this->input->post("constract_drawing4ftext");
            $data["constract_drawing5f"]= $this->input->post("constract_drawing5ftext");
            $data["constract_drawing6f"]= $this->input->post("constract_drawing6ftext");
            $data["constract_drawing7f"]= $this->input->post("constract_drawing7ftext");
            $data["constract_drawing8f"]= $this->input->post("constract_drawing8ftext");
            $data["constract_drawing9f"]= $this->input->post("constract_drawing9ftext");
            $data["constract_drawing10f"]= $this->input->post("constract_drawing10ftext");

            $data["date_insert_drawing1f"] = date('Y-m-d');
            $data["date_insert_drawing2f"] = date('Y-m-d');
            $data["date_insert_drawing3f"] = date('Y-m-d');
            $data["date_insert_drawing4f"] = date('Y-m-d');
            $data["date_insert_drawing5f"] = date('Y-m-d');
            $data["date_insert_drawing6f"] = date('Y-m-d');
            $data["date_insert_drawing7f"] = date('Y-m-d');
            $data["date_insert_drawing8f"] = date('Y-m-d');
            $data["date_insert_drawing9f"] = date('Y-m-d');
            $data["date_insert_drawing10f"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1gtext")))
        // if($_FILES['constract_drawing1g']['size']!=0)
        {

            $data["check1g"] = $this->input->post("check1g");
            $data["desc_item1g"] = mb_convert_kana($this->input->post("desc_item1g"), "as");
            $data["check2g"] = $this->input->post("check2g");
            $data["desc_item2g"] = mb_convert_kana($this->input->post("desc_item2g"), "as");
            $data["check3g"] = $this->input->post("check3g");
            $data["desc_item3g"] = mb_convert_kana($this->input->post("desc_item3g"), "as");
            $data["check4g"] = $this->input->post("check4g");
            $data["desc_item4g"] = mb_convert_kana($this->input->post("desc_item4g"), "as");
            $data["check5g"] = $this->input->post("check5g");
            $data["desc_item5g"] = mb_convert_kana($this->input->post("desc_item5g"), "as");
            $data["check6g"] = $this->input->post("check6g");
            $data["desc_item6g"] = mb_convert_kana($this->input->post("desc_item6g"), "as");
            $data["check7g"] = $this->input->post("check7g");
            $data["desc_item7g"] = mb_convert_kana($this->input->post("desc_item7g"), "as");
            $data["check8g"] = $this->input->post("check8g");
            $data["desc_item8g"] = mb_convert_kana($this->input->post("desc_item8g"), "as");
            $data["check9g"] = $this->input->post("check9g");
            $data["desc_item9g"] = mb_convert_kana($this->input->post("desc_item9g"), "as");
            $data["check10g"] = $this->input->post("check10g");
            $data["desc_item10g"] = mb_convert_kana($this->input->post("desc_item10g"), "as");
            $data["category_name1g"] = "工事写真";
            // $data["type_item"] = "admin";

            $data["constract_drawing1g"]= $this->input->post("constract_drawing1gtext");
            $data["constract_drawing2g"]= $this->input->post("constract_drawing2gtext");
            $data["constract_drawing3g"]= $this->input->post("constract_drawing3gtext");
            $data["constract_drawing4g"]= $this->input->post("constract_drawing4gtext");
            $data["constract_drawing5g"]= $this->input->post("constract_drawing5gtext");
            $data["constract_drawing6g"]= $this->input->post("constract_drawing6gtext");
            $data["constract_drawing7g"]= $this->input->post("constract_drawing7gtext");
            $data["constract_drawing8g"]= $this->input->post("constract_drawing8gtext");
            $data["constract_drawing9g"]= $this->input->post("constract_drawing9gtext");
            $data["constract_drawing10g"]= $this->input->post("constract_drawing10gtext");

            $data["date_insert_drawing1g"] = date('Y-m-d');
            $data["date_insert_drawing2g"] = date('Y-m-d');
            $data["date_insert_drawing3g"] = date('Y-m-d');
            $data["date_insert_drawing4g"] = date('Y-m-d');
            $data["date_insert_drawing5g"] = date('Y-m-d');
            $data["date_insert_drawing6g"] = date('Y-m-d');
            $data["date_insert_drawing7g"] = date('Y-m-d');
            $data["date_insert_drawing8g"] = date('Y-m-d');
            $data["date_insert_drawing9g"] = date('Y-m-d');
            $data["date_insert_drawing10g"] = date('Y-m-d');
        }

        
        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
        $data = $this->security->xss_clean($data);
        $this->load->view('dashboard/constructor/preview_v',$data);
    }

    public function preview_data_ins()
    {

        // print_r($_POST);die();

        $data["cust_code"] = mb_convert_kana($this->input->post("cust_code"), "as");
        $data["cust_name"] = mb_convert_kana($this->input->post("cust_name"), "as");
        $data["kinds"] = mb_convert_kana($this->input->post("kinds"), "as");
        $data["const_no"] = mb_convert_kana($this->input->post("const_no"), "as");
        $data["remarks"] = mb_convert_kana($this->input->post("remarks"), "as");
        $data["contract_date"] = $this->input->post("contract_date");
        $data["construction_start_date"] = $this->input->post("construction_start_date");
        $data["upper_building_date"] = $this->input->post("upper_building_date");
        $data["completion_date"] = $this->input->post("completion_date");
        $data["delivery_date"] = $this->input->post("delivery_date");
        $data["amount_money"] = mb_convert_kana($this->input->post("amount_money"), "as");
        $data["construct_stat"] = $this->input->post("construct_stat");

        // $config['upload_path'] = APPPATH.'../uploads/files'; 
        // $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        // $config['max_size'] = '500000'; // in KlioBytes

        if(!empty($this->input->post("sales_staff1")))
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff1"), "as");
        }
        else
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff"), "as");
        }

        if(!empty($this->input->post("incharge_construction1")))
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction1"), "as");
        }
        else
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction"), "as");
        }

        if(!empty($this->input->post("incharge_coordinator1")))
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator1"), "as");
        }
        else
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator"), "as");
        }


        if(!empty($this->input->post("constract_drawing1blueatext")))
        // if($_FILES['constract_drawing1bluea']['size']!=0)
        {

            $data["check1bluea"] = $this->input->post("check1bluea");
            $data["desc_item1bluea"] = mb_convert_kana($this->input->post("desc_item1bluea"), "as");
            $data["check2bluea"] = $this->input->post("check2bluea");
            $data["desc_item2bluea"] = mb_convert_kana($this->input->post("desc_item2bluea"), "as");
            $data["check3bluea"] = $this->input->post("check3bluea");
            $data["desc_item3bluea"] = mb_convert_kana($this->input->post("desc_item3bluea"), "as");
            $data["check4bluea"] = $this->input->post("check4bluea");
            $data["desc_item4bluea"] = mb_convert_kana($this->input->post("desc_item4bluea"), "as");
            $data["check5bluea"] = $this->input->post("check5bluea");
            $data["desc_item5bluea"] = mb_convert_kana($this->input->post("desc_item5bluea"), "as");
            $data["check6bluea"] = $this->input->post("check6bluea");
            $data["desc_item6bluea"] = mb_convert_kana($this->input->post("desc_item6bluea"), "as");
            $data["check7bluea"] = $this->input->post("check7bluea");
            $data["desc_item7bluea"] = mb_convert_kana($this->input->post("desc_item7bluea"), "as");
            $data["check8bluea"] = $this->input->post("check8bluea");
            $data["desc_item8bluea"] = mb_convert_kana($this->input->post("desc_item8bluea"), "as");
            $data["check9bluea"] = $this->input->post("check9bluea");
            $data["desc_item9bluea"] = mb_convert_kana($this->input->post("desc_item9bluea"), "as");
            $data["check10bluea"] = $this->input->post("check10bluea");
            $data["desc_item10bluea"] = mb_convert_kana($this->input->post("desc_item10bluea"), "as");
            $data["category_name1bluea"] = "確認申請書一式";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1bluea")))
            {
                $data["co_owner1bluea"] = $this->input->post("co_owner1bluea");
            }
            else
            {
                $data["co_owner1bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluea")))
            {
                $data["co_owner2bluea"] = $this->input->post("co_owner2bluea");
            }
            else
            {
                $data["co_owner2bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluea")))
            {
                $data["co_owner3bluea"] = $this->input->post("co_owner3bluea");
            }
            else
            {
                $data["co_owner3bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluea")))
            {
                $data["co_owner4bluea"] = $this->input->post("co_owner4bluea");
            }
            else
            {
                $data["co_owner4bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluea")))
            {
                $data["co_owner5bluea"] = $this->input->post("co_owner5bluea");
            }
            else
            {
                $data["co_owner5bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluea")))
            {
                $data["co_owner6bluea"] = $this->input->post("co_owner6bluea");
            }
            else
            {
                $data["co_owner6bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluea")))
            {
                $data["co_owner7bluea"] = $this->input->post("co_owner7bluea");
            }
            else
            {
                $data["co_owner7bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluea")))
            {
                $data["co_owner8bluea"] = $this->input->post("co_owner8bluea");
            }
            else
            {
                $data["co_owner8bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluea")))
            {
                $data["co_owner9bluea"] = $this->input->post("co_owner9bluea");
            }
            else
            {
                $data["co_owner9bluea"] = '';
            }

            if (!empty($this->input->post("co_owner10bluea")))
            {
                $data["co_owner10bluea"] = $this->input->post("co_owner10bluea");
            }
            else
            {
                $data["co_owner10bluea"] = '';
            }


            $data["constract_drawing1bluea"]= $this->input->post("constract_drawing1blueatext");
            $data["constract_drawing2bluea"]= $this->input->post("constract_drawing2blueatext");
            $data["constract_drawing3bluea"]= $this->input->post("constract_drawing3blueatext");
            $data["constract_drawing4bluea"]= $this->input->post("constract_drawing4blueatext");
            $data["constract_drawing5bluea"]= $this->input->post("constract_drawing5blueatext");
            $data["constract_drawing6bluea"]= $this->input->post("constract_drawing6blueatext");
            $data["constract_drawing7bluea"]= $this->input->post("constract_drawing7blueatext");
            $data["constract_drawing8bluea"]= $this->input->post("constract_drawing8blueatext");
            $data["constract_drawing9bluea"]= $this->input->post("constract_drawing9blueatext");
            $data["constract_drawing10bluea"]= $this->input->post("constract_drawing10blueatext");

            $data["date_insert_drawing1bluea"] = date('Y-m-d');
            $data["date_insert_drawing2bluea"] = date('Y-m-d');
            $data["date_insert_drawing3bluea"] = date('Y-m-d');
            $data["date_insert_drawing4bluea"] = date('Y-m-d');
            $data["date_insert_drawing5bluea"] = date('Y-m-d');
            $data["date_insert_drawing6bluea"] = date('Y-m-d');
            $data["date_insert_drawing7bluea"] = date('Y-m-d');
            $data["date_insert_drawing8bluea"] = date('Y-m-d');
            $data["date_insert_drawing9bluea"] = date('Y-m-d');
            $data["date_insert_drawing10bluea"] = date('Y-m-d');

        }
        
        if(!empty($this->input->post("constract_drawing1bluebtext")))
        // if($_FILES['constract_drawing1blueb']['size']!=0)
        {

            $data["check1blueb"] = $this->input->post("check1blueb");
            $data["desc_item1blueb"] = mb_convert_kana($this->input->post("desc_item1blueb"), "as");
            $data["check2blueb"] = $this->input->post("check2blueb");
            $data["desc_item2blueb"] = mb_convert_kana($this->input->post("desc_item2blueb"), "as");
            $data["check3blueb"] = $this->input->post("check3blueb");
            $data["desc_item3blueb"] = mb_convert_kana($this->input->post("desc_item3blueb"), "as");
            $data["check4blueb"] = $this->input->post("check4blueb");
            $data["desc_item4blueb"] = mb_convert_kana($this->input->post("desc_item4blueb"), "as");
            $data["check5blueb"] = $this->input->post("check5blueb");
            $data["desc_item5blueb"] = mb_convert_kana($this->input->post("desc_item5blueb"), "as");
            $data["check6blueb"] = $this->input->post("check6blueb");
            $data["desc_item6blueb"] = mb_convert_kana($this->input->post("desc_item6blueb"), "as");
            $data["check7blueb"] = $this->input->post("check7blueb");
            $data["desc_item7blueb"] = mb_convert_kana($this->input->post("desc_item7blueb"), "as");
            $data["check8blueb"] = $this->input->post("check8blueb");
            $data["desc_item8blueb"] = mb_convert_kana($this->input->post("desc_item8blueb"), "as");
            $data["check9blueb"] = $this->input->post("check9blueb");
            $data["desc_item9blueb"] = mb_convert_kana($this->input->post("desc_item9blueb"), "as");
            $data["check10blueb"] = $this->input->post("check10blueb");
            $data["desc_item10blueb"] = mb_convert_kana($this->input->post("desc_item10blueb"), "as");
            $data["category_name1blueb"] = "工事工程表";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1blueb")))
            {
                $data["co_owner1blueb"] = $this->input->post("co_owner1blueb");
            }
            else
            {
                $data["co_owner1blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueb")))
            {
                $data["co_owner2blueb"] = $this->input->post("co_owner2blueb");
            }
            else
            {
                $data["co_owner2blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueb")))
            {
                $data["co_owner3blueb"] = $this->input->post("co_owner3blueb");
            }
            else
            {
                $data["co_owner3blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueb")))
            {
                $data["co_owner4blueb"] = $this->input->post("co_owner4blueb");
            }
            else
            {
                $data["co_owner4blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueb")))
            {
                $data["co_owner5blueb"] = $this->input->post("co_owner5blueb");
            }
            else
            {
                $data["co_owner5blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueb")))
            {
                $data["co_owner6blueb"] = $this->input->post("co_owner6blueb");
            }
            else
            {
                $data["co_owner6blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueb")))
            {
                $data["co_owner7blueb"] = $this->input->post("co_owner7blueb");
            }
            else
            {
                $data["co_owner7blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueb")))
            {
                $data["co_owner8blueb"] = $this->input->post("co_owner8blueb");
            }
            else
            {
                $data["co_owner8blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueb")))
            {
                $data["co_owner9blueb"] = $this->input->post("co_owner9blueb");
            }
            else
            {
                $data["co_owner9blueb"] = '';
            }

            if (!empty($this->input->post("co_owner10blueb")))
            {
                $data["co_owner10blueb"] = $this->input->post("co_owner10blueb");
            }
            else
            {
                $data["co_owner10blueb"] = '';
            }


            $data["constract_drawing1blueb"]= $this->input->post("constract_drawing1bluebtext");
            $data["constract_drawing2blueb"]= $this->input->post("constract_drawing2bluebtext");
            $data["constract_drawing3blueb"]= $this->input->post("constract_drawing3bluebtext");
            $data["constract_drawing4blueb"]= $this->input->post("constract_drawing4bluebtext");
            $data["constract_drawing5blueb"]= $this->input->post("constract_drawing5bluebtext");
            $data["constract_drawing6blueb"]= $this->input->post("constract_drawing6bluebtext");
            $data["constract_drawing7blueb"]= $this->input->post("constract_drawing7bluebtext");
            $data["constract_drawing8blueb"]= $this->input->post("constract_drawing8bluebtext");
            $data["constract_drawing9blueb"]= $this->input->post("constract_drawing9bluebtext");
            $data["constract_drawing10blueb"]= $this->input->post("constract_drawing10bluebtext");

            $data["date_insert_drawing1blueb"] = date('Y-m-d');
            $data["date_insert_drawing2blueb"] = date('Y-m-d');
            $data["date_insert_drawing3blueb"] = date('Y-m-d');
            $data["date_insert_drawing4blueb"] = date('Y-m-d');
            $data["date_insert_drawing5blueb"] = date('Y-m-d');
            $data["date_insert_drawing6blueb"] = date('Y-m-d');
            $data["date_insert_drawing7blueb"] = date('Y-m-d');
            $data["date_insert_drawing8blueb"] = date('Y-m-d');
            $data["date_insert_drawing9blueb"] = date('Y-m-d');
            $data["date_insert_drawing10blueb"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluectext")))
        // if($_FILES['constract_drawing1bluec']['size']!=0)
        {

            $data["check1bluec"] = $this->input->post("check1bluec");
            $data["desc_item1bluec"] = mb_convert_kana($this->input->post("desc_item1bluec"), "as");
            $data["check2bluec"] = $this->input->post("check2bluec");
            $data["desc_item2bluec"] = mb_convert_kana($this->input->post("desc_item2bluec"), "as");
            $data["check3bluec"] = $this->input->post("check3bluec");
            $data["desc_item3bluec"] = mb_convert_kana($this->input->post("desc_item3bluec"), "as");
            $data["check4bluec"] = $this->input->post("check4bluec");
            $data["desc_item4bluec"] = mb_convert_kana($this->input->post("desc_item4bluec"), "as");
            $data["check5bluec"] = $this->input->post("check5bluec");
            $data["desc_item5bluec"] = mb_convert_kana($this->input->post("desc_item5bluec"), "as");
            $data["check6bluec"] = $this->input->post("check6bluec");
            $data["desc_item6bluec"] = mb_convert_kana($this->input->post("desc_item6bluec"), "as");
            $data["check7bluec"] = $this->input->post("check7bluec");
            $data["desc_item7bluec"] = mb_convert_kana($this->input->post("desc_item7bluec"), "as");
            $data["check8bluec"] = $this->input->post("check8bluec");
            $data["desc_item8bluec"] = mb_convert_kana($this->input->post("desc_item8bluec"), "as");
            $data["check9bluec"] = $this->input->post("check9bluec");
            $data["desc_item9bluec"] = mb_convert_kana($this->input->post("desc_item9bluec"), "as");
            $data["check10bluec"] = $this->input->post("check10bluec");
            $data["desc_item10bluec"] = mb_convert_kana($this->input->post("desc_item10bluec"), "as");
            $data["category_name1bluec"] = "工事着工資料";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1bluec")))
            {
                $data["co_owner1bluec"] = $this->input->post("co_owner1bluec");
            }
            else
            {
                $data["co_owner1bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluec")))
            {
                $data["co_owner2bluec"] = $this->input->post("co_owner2bluec");
            }
            else
            {
                $data["co_owner2bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluec")))
            {
                $data["co_owner3bluec"] = $this->input->post("co_owner3bluec");
            }
            else
            {
                $data["co_owner3bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluec")))
            {
                $data["co_owner4bluec"] = $this->input->post("co_owner4bluec");
            }
            else
            {
                $data["co_owner4bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluec")))
            {
                $data["co_owner5bluec"] = $this->input->post("co_owner5bluec");
            }
            else
            {
                $data["co_owner5bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluec")))
            {
                $data["co_owner6bluec"] = $this->input->post("co_owner6bluec");
            }
            else
            {
                $data["co_owner6bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluec")))
            {
                $data["co_owner7bluec"] = $this->input->post("co_owner7bluec");
            }
            else
            {
                $data["co_owner7bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluec")))
            {
                $data["co_owner8bluec"] = $this->input->post("co_owner8bluec");
            }
            else
            {
                $data["co_owner8bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluec")))
            {
                $data["co_owner9bluec"] = $this->input->post("co_owner9bluec");
            }
            else
            {
                $data["co_owner9bluec"] = '';
            }

            if (!empty($this->input->post("co_owner10bluec")))
            {
                $data["co_owner10bluec"] = $this->input->post("co_owner10bluec");
            }
            else
            {
                $data["co_owner10bluec"] = '';
            }



            $data["constract_drawing1bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing2bluec"]= $this->input->post("constract_drawing2bluectext");
            $data["constract_drawing3bluec"]= $this->input->post("constract_drawing3bluectext");
            $data["constract_drawing4bluec"]= $this->input->post("constract_drawing4bluectext");
            $data["constract_drawing5bluec"]= $this->input->post("constract_drawing5bluectext");
            $data["constract_drawing6bluec"]= $this->input->post("constract_drawing6bluectext");
            $data["constract_drawing7bluec"]= $this->input->post("constract_drawing7bluectext");
            $data["constract_drawing8bluec"]= $this->input->post("constract_drawing8bluectext");
            $data["constract_drawing9bluec"]= $this->input->post("constract_drawing9bluectext");
            $data["constract_drawing10bluec"]= $this->input->post("constract_drawing10bluectext");


            $data["date_insert_drawing1bluec"] = date('Y-m-d');
            $data["date_insert_drawing2bluec"] = date('Y-m-d');
            $data["date_insert_drawing3bluec"] = date('Y-m-d');
            $data["date_insert_drawing4bluec"] = date('Y-m-d');
            $data["date_insert_drawing5bluec"] = date('Y-m-d');
            $data["date_insert_drawing6bluec"] = date('Y-m-d');
            $data["date_insert_drawing7bluec"] = date('Y-m-d');
            $data["date_insert_drawing8bluec"] = date('Y-m-d');
            $data["date_insert_drawing9bluec"] = date('Y-m-d');
            $data["date_insert_drawing10bluec"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluedtext")))
        // if($_FILES['constract_drawing1blued']['size']!=0)
        {


            $data["check1blued"] = $this->input->post("check1blued");
            $data["desc_item1blued"] = mb_convert_kana($this->input->post("desc_item1blued"), "as");
            $data["check2blued"] = $this->input->post("check2blued");
            $data["desc_item2blued"] = mb_convert_kana($this->input->post("desc_item2blued"), "as");
            $data["check3blued"] = $this->input->post("check3blued");
            $data["desc_item3blued"] = mb_convert_kana($this->input->post("desc_item3blued"), "as");
            $data["check4blued"] = $this->input->post("check4blued");
            $data["desc_item4blued"] = mb_convert_kana($this->input->post("desc_item4blued"), "as");
            $data["check5blued"] = $this->input->post("check5blued");
            $data["desc_item5blued"] = mb_convert_kana($this->input->post("desc_item5blued"), "as");
            $data["check6blued"] = $this->input->post("check6blued");
            $data["desc_item6blued"] = mb_convert_kana($this->input->post("desc_item6blued"), "as");
            $data["check7blued"] = $this->input->post("check7blued");
            $data["desc_item7blued"] = mb_convert_kana($this->input->post("desc_item7blued"), "as");
            $data["check8blued"] = $this->input->post("check8blued");
            $data["desc_item8blued"] = mb_convert_kana($this->input->post("desc_item8blued"), "as");
            $data["check9blued"] = $this->input->post("check9blued");
            $data["desc_item9blued"] = mb_convert_kana($this->input->post("desc_item9blued"), "as");
            $data["check10blued"] = $this->input->post("check10blued");
            $data["desc_item10blued"] = mb_convert_kana($this->input->post("desc_item10blued"), "as");
            $data["category_name1blued"] = "施工図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blued")))
            {
                $data["co_owner1blued"] = $this->input->post("co_owner1blued");
            }
            else
            {
                $data["co_owner1blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blued")))
            {
                $data["co_owner2blued"] = $this->input->post("co_owner2blued");
            }
            else
            {
                $data["co_owner2blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blued")))
            {
                $data["co_owner3blued"] = $this->input->post("co_owner3blued");
            }
            else
            {
                $data["co_owner3blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blued")))
            {
                $data["co_owner4blued"] = $this->input->post("co_owner4blued");
            }
            else
            {
                $data["co_owner4blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blued")))
            {
                $data["co_owner5blued"] = $this->input->post("co_owner5blued");
            }
            else
            {
                $data["co_owner5blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blued")))
            {
                $data["co_owner6blued"] = $this->input->post("co_owner6blued");
            }
            else
            {
                $data["co_owner6blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blued")))
            {
                $data["co_owner7blued"] = $this->input->post("co_owner7blued");
            }
            else
            {
                $data["co_owner7blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blued")))
            {
                $data["co_owner8blued"] = $this->input->post("co_owner8blued");
            }
            else
            {
                $data["co_owner8blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blued")))
            {
                $data["co_owner9blued"] = $this->input->post("co_owner9blued");
            }
            else
            {
                $data["co_owner9blued"] = '';
            }

            if (!empty($this->input->post("co_owner10blued")))
            {
                $data["co_owner10blued"] = $this->input->post("co_owner10blued");
            }
            else
            {
                $data["co_owner10blued"] = '';
            }


            $data["constract_drawing1blued"]= $this->input->post("constract_drawing1bluedtext");
            $data["constract_drawing2blued"]= $this->input->post("constract_drawing2bluedtext");
            $data["constract_drawing3blued"]= $this->input->post("constract_drawing3bluedtext");
            $data["constract_drawing4blued"]= $this->input->post("constract_drawing4bluedtext");
            $data["constract_drawing5blued"]= $this->input->post("constract_drawing5bluedtext");
            $data["constract_drawing6blued"]= $this->input->post("constract_drawing6bluedtext");
            $data["constract_drawing7blued"]= $this->input->post("constract_drawing7bluedtext");
            $data["constract_drawing8blued"]= $this->input->post("constract_drawing8bluedtext");
            $data["constract_drawing9blued"]= $this->input->post("constract_drawing9bluedtext");
            $data["constract_drawing10blued"]= $this->input->post("constract_drawing10bluedtext");


            $data["date_insert_drawing1blued"] = date('Y-m-d');
            $data["date_insert_drawing2blued"] = date('Y-m-d');
            $data["date_insert_drawing3blued"] = date('Y-m-d');
            $data["date_insert_drawing4blued"] = date('Y-m-d');
            $data["date_insert_drawing5blued"] = date('Y-m-d');
            $data["date_insert_drawing6blued"] = date('Y-m-d');
            $data["date_insert_drawing7blued"] = date('Y-m-d');
            $data["date_insert_drawing8blued"] = date('Y-m-d');
            $data["date_insert_drawing9blued"] = date('Y-m-d');
            $data["date_insert_drawing10blued"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueetext")))
        // if($_FILES['constract_drawing1bluee']['size']!=0)
        {

            $data["check1bluee"] = $this->input->post("check1bluee");
            $data["desc_item1bluee"] = mb_convert_kana($this->input->post("desc_item1bluee"), "as");
            $data["check2bluee"] = $this->input->post("check2bluee");
            $data["desc_item2bluee"] = mb_convert_kana($this->input->post("desc_item2bluee"), "as");
            $data["check3bluee"] = $this->input->post("check3bluee");
            $data["desc_item3bluee"] = mb_convert_kana($this->input->post("desc_item3bluee"), "as");
            $data["check4bluee"] = $this->input->post("check4bluee");
            $data["desc_item4bluee"] = mb_convert_kana($this->input->post("desc_item4bluee"), "as");
            $data["check5bluee"] = $this->input->post("check5bluee");
            $data["desc_item5bluee"] = mb_convert_kana($this->input->post("desc_item5bluee"), "as");
            $data["check6bluee"] = $this->input->post("check6bluee");
            $data["desc_item6bluee"] = mb_convert_kana($this->input->post("desc_item6bluee"), "as");
            $data["check7bluee"] = $this->input->post("check7bluee");
            $data["desc_item7bluee"] = mb_convert_kana($this->input->post("desc_item7bluee"), "as");
            $data["check8bluee"] = $this->input->post("check8bluee");
            $data["desc_item8bluee"] = mb_convert_kana($this->input->post("desc_item8bluee"), "as");
            $data["check9bluee"] = $this->input->post("check9bluee");
            $data["desc_item9bluee"] = mb_convert_kana($this->input->post("desc_item9bluee"), "as");
            $data["check10bluee"] = $this->input->post("check10bluee");
            $data["desc_item10bluee"] = mb_convert_kana($this->input->post("desc_item10bluee"), "as");
            $data["category_name1bluee"] = "変更図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluee")))
            {
                $data["co_owner1bluee"] = $this->input->post("co_owner1bluee");
            }
            else
            {
                $data["co_owner1bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluee")))
            {
                $data["co_owner2bluee"] = $this->input->post("co_owner2bluee");
            }
            else
            {
                $data["co_owner2bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluee")))
            {
                $data["co_owner3bluee"] = $this->input->post("co_owner3bluee");
            }
            else
            {
                $data["co_owner3bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluee")))
            {
                $data["co_owner4bluee"] = $this->input->post("co_owner4bluee");
            }
            else
            {
                $data["co_owner4bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluee")))
            {
                $data["co_owner5bluee"] = $this->input->post("co_owner5bluee");
            }
            else
            {
                $data["co_owner5bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluee")))
            {
                $data["co_owner6bluee"] = $this->input->post("co_owner6bluee");
            }
            else
            {
                $data["co_owner6bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluee")))
            {
                $data["co_owner7bluee"] = $this->input->post("co_owner7bluee");
            }
            else
            {
                $data["co_owner7bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluee")))
            {
                $data["co_owner8bluee"] = $this->input->post("co_owner8bluee");
            }
            else
            {
                $data["co_owner8bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluee")))
            {
                $data["co_owner9bluee"] = $this->input->post("co_owner9bluee");
            }
            else
            {
                $data["co_owner9bluee"] = '';
            }

            if (!empty($this->input->post("co_owner10bluee")))
            {
                $data["co_owner10bluee"] = $this->input->post("co_owner10bluee");
            }
            else
            {
                $data["co_owner10bluee"] = '';
            }


            $data["constract_drawing1bluee"]= $this->input->post("constract_drawing1blueetext");
            $data["constract_drawing2bluee"]= $this->input->post("constract_drawing2blueetext");
            $data["constract_drawing3bluee"]= $this->input->post("constract_drawing3blueetext");
            $data["constract_drawing4bluee"]= $this->input->post("constract_drawing4blueetext");
            $data["constract_drawing5bluee"]= $this->input->post("constract_drawing5blueetext");
            $data["constract_drawing6bluee"]= $this->input->post("constract_drawing6blueetext");
            $data["constract_drawing7bluee"]= $this->input->post("constract_drawing7blueetext");
            $data["constract_drawing8bluee"]= $this->input->post("constract_drawing8blueetext");
            $data["constract_drawing9bluee"]= $this->input->post("constract_drawing9blueetext");
            $data["constract_drawing10bluee"]= $this->input->post("constract_drawing10blueetext");


            $data["date_insert_drawing1bluee"] = date('Y-m-d');
            $data["date_insert_drawing2bluee"] = date('Y-m-d');
            $data["date_insert_drawing3bluee"] = date('Y-m-d');
            $data["date_insert_drawing4bluee"] = date('Y-m-d');
            $data["date_insert_drawing5bluee"] = date('Y-m-d');
            $data["date_insert_drawing6bluee"] = date('Y-m-d');
            $data["date_insert_drawing7bluee"] = date('Y-m-d');
            $data["date_insert_drawing8bluee"] = date('Y-m-d');
            $data["date_insert_drawing9bluee"] = date('Y-m-d');
            $data["date_insert_drawing10bluee"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueftext")))
        // if($_FILES['constract_drawing1bluef']['size']!=0)
        {


            $data["check1bluef"] = $this->input->post("check1bluef");
            $data["desc_item1bluef"] = mb_convert_kana($this->input->post("desc_item1bluef"), "as");
            $data["check2bluef"] = $this->input->post("check2bluef");
            $data["desc_item2bluef"] = mb_convert_kana($this->input->post("desc_item2bluef"), "as");
            $data["check3bluef"] = $this->input->post("check3bluef");
            $data["desc_item3bluef"] = mb_convert_kana($this->input->post("desc_item3bluef"), "as");
            $data["check4bluef"] = $this->input->post("check4bluef");
            $data["desc_item4bluef"] = mb_convert_kana($this->input->post("desc_item4bluef"), "as");
            $data["check5bluef"] = $this->input->post("check5bluef");
            $data["desc_item5bluef"] = mb_convert_kana($this->input->post("desc_item5bluef"), "as");
            $data["check6bluef"] = $this->input->post("check6bluef");
            $data["desc_item6bluef"] = mb_convert_kana($this->input->post("desc_item6bluef"), "as");
            $data["check7bluef"] = $this->input->post("check7bluef");
            $data["desc_item7bluef"] = mb_convert_kana($this->input->post("desc_item7bluef"), "as");
            $data["check8bluef"] = $this->input->post("check8bluef");
            $data["desc_item8bluef"] = mb_convert_kana($this->input->post("desc_item8bluef"), "as");
            $data["check9bluef"] = $this->input->post("check9bluef");
            $data["desc_item9bluef"] = mb_convert_kana($this->input->post("desc_item9bluef"), "as");
            $data["check10bluef"] = $this->input->post("check10bluef");
            $data["desc_item10bluef"] = mb_convert_kana($this->input->post("desc_item10bluef"), "as");
            $data["category_name1bluef"] = "設備プランシート";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluef")))
            {
                $data["co_owner1bluef"] = $this->input->post("co_owner1bluef");
            }
            else
            {
                $data["co_owner1bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluef")))
            {
                $data["co_owner2bluef"] = $this->input->post("co_owner2bluef");
            }
            else
            {
                $data["co_owner2bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluef")))
            {
                $data["co_owner3bluef"] = $this->input->post("co_owner3bluef");
            }
            else
            {
                $data["co_owner3bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluef")))
            {
                $data["co_owner4bluef"] = $this->input->post("co_owner4bluef");
            }
            else
            {
                $data["co_owner4bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluef")))
            {
                $data["co_owner5bluef"] = $this->input->post("co_owner5bluef");
            }
            else
            {
                $data["co_owner5bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluef")))
            {
                $data["co_owner6bluef"] = $this->input->post("co_owner6bluef");
            }
            else
            {
                $data["co_owner6bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluef")))
            {
                $data["co_owner7bluef"] = $this->input->post("co_owner7bluef");
            }
            else
            {
                $data["co_owner7bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluef")))
            {
                $data["co_owner8bluef"] = $this->input->post("co_owner8bluef");
            }
            else
            {
                $data["co_owner8bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluef")))
            {
                $data["co_owner9bluef"] = $this->input->post("co_owner9bluef");
            }
            else
            {
                $data["co_owner9bluef"] = '';
            }

            if (!empty($this->input->post("co_owner10bluef")))
            {
                $data["co_owner10bluef"] = $this->input->post("co_owner10bluef");
            }
            else
            {
                $data["co_owner10bluef"] = '';
            }


            $data["constract_drawing1bluef"]= $this->input->post("constract_drawing1blueftext");
            $data["constract_drawing2bluef"]= $this->input->post("constract_drawing2blueftext");
            $data["constract_drawing3bluef"]= $this->input->post("constract_drawing3blueftext");
            $data["constract_drawing4bluef"]= $this->input->post("constract_drawing4blueftext");
            $data["constract_drawing5bluef"]= $this->input->post("constract_drawing5blueftext");
            $data["constract_drawing6bluef"]= $this->input->post("constract_drawing6blueftext");
            $data["constract_drawing7bluef"]= $this->input->post("constract_drawing7blueftext");
            $data["constract_drawing8bluef"]= $this->input->post("constract_drawing8blueftext");
            $data["constract_drawing9bluef"]= $this->input->post("constract_drawing9blueftext");
            $data["constract_drawing10bluef"]= $this->input->post("constract_drawing10blueftext");


            $data["date_insert_drawing1bluef"] = date('Y-m-d');
            $data["date_insert_drawing2bluef"] = date('Y-m-d');
            $data["date_insert_drawing3bluef"] = date('Y-m-d');
            $data["date_insert_drawing4bluef"] = date('Y-m-d');
            $data["date_insert_drawing5bluef"] = date('Y-m-d');
            $data["date_insert_drawing6bluef"] = date('Y-m-d');
            $data["date_insert_drawing7bluef"] = date('Y-m-d');
            $data["date_insert_drawing8bluef"] = date('Y-m-d');
            $data["date_insert_drawing9bluef"] = date('Y-m-d');
            $data["date_insert_drawing10bluef"] = date('Y-m-d');
        }
         
        if(!empty($this->input->post("constract_drawing1bluegtext")))
        // if($_FILES['constract_drawing1blueg']['size']!=0)
        {


            $data["check1blueg"] = $this->input->post("check1blueg");
            $data["desc_item1blueg"] = mb_convert_kana($this->input->post("desc_item1blueg"), "as");
            $data["check2blueg"] = $this->input->post("check2blueg");
            $data["desc_item2blueg"] = mb_convert_kana($this->input->post("desc_item2blueg"), "as");
            $data["check3blueg"] = $this->input->post("check3blueg");
            $data["desc_item3blueg"] = mb_convert_kana($this->input->post("desc_item3blueg"), "as");
            $data["check4blueg"] = $this->input->post("check4blueg");
            $data["desc_item4blueg"] = mb_convert_kana($this->input->post("desc_item4blueg"), "as");
            $data["check5blueg"] = $this->input->post("check5blueg");
            $data["desc_item5blueg"] = mb_convert_kana($this->input->post("desc_item5blueg"), "as");
            $data["check6blueg"] = $this->input->post("check6blueg");
            $data["desc_item6blueg"] = mb_convert_kana($this->input->post("desc_item6blueg"), "as");
            $data["check7blueg"] = $this->input->post("check7blueg");
            $data["desc_item7blueg"] = mb_convert_kana($this->input->post("desc_item7blueg"), "as");
            $data["check8blueg"] = $this->input->post("check8blueg");
            $data["desc_item8blueg"] = mb_convert_kana($this->input->post("desc_item8blueg"), "as");
            $data["check9blueg"] = $this->input->post("check9blueg");
            $data["desc_item9blueg"] = mb_convert_kana($this->input->post("desc_item9blueg"), "as");
            $data["check10blueg"] = $this->input->post("check10blueg");
            $data["desc_item10blueg"] = mb_convert_kana($this->input->post("desc_item10blueg"), "as");
            $data["category_name1blueg"] = "その他";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blueg")))
            {
                $data["co_owner1blueg"] = $this->input->post("co_owner1blueg");
            }
            else
            {
                $data["co_owner1blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueg")))
            {
                $data["co_owner2blueg"] = $this->input->post("co_owner2blueg");
            }
            else
            {
                $data["co_owner2blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueg")))
            {
                $data["co_owner3blueg"] = $this->input->post("co_owner3blueg");
            }
            else
            {
                $data["co_owner3blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueg")))
            {
                $data["co_owner4blueg"] = $this->input->post("co_owner4blueg");
            }
            else
            {
                $data["co_owner4blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueg")))
            {
                $data["co_owner5blueg"] = $this->input->post("co_owner5blueg");
            }
            else
            {
                $data["co_owner5blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueg")))
            {
                $data["co_owner6blueg"] = $this->input->post("co_owner6blueg");
            }
            else
            {
                $data["co_owner6blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueg")))
            {
                $data["co_owner7blueg"] = $this->input->post("co_owner7blueg");
            }
            else
            {
                $data["co_owner7blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueg")))
            {
                $data["co_owner8blueg"] = $this->input->post("co_owner8blueg");
            }
            else
            {
                $data["co_owner8blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueg")))
            {
                $data["co_owner9blueg"] = $this->input->post("co_owner9blueg");
            }
            else
            {
                $data["co_owner9blueg"] = '';
            }

            if (!empty($this->input->post("co_owner10blueg")))
            {
                $data["co_owner10blueg"] = $this->input->post("co_owner10blueg");
            }
            else
            {
                $data["co_owner10blueg"] = '';
            }


            $data["constract_drawing1blueg"]= $this->input->post("constract_drawing1bluegtext");
            $data["constract_drawing2blueg"]= $this->input->post("constract_drawing2bluegtext");
            $data["constract_drawing3blueg"]= $this->input->post("constract_drawing3bluegtext");
            $data["constract_drawing4blueg"]= $this->input->post("constract_drawing4bluegtext");
            $data["constract_drawing5blueg"]= $this->input->post("constract_drawing5bluegtext");
            $data["constract_drawing6blueg"]= $this->input->post("constract_drawing6bluegtext");
            $data["constract_drawing7blueg"]= $this->input->post("constract_drawing7bluegtext");
            $data["constract_drawing8blueg"]= $this->input->post("constract_drawing8bluegtext");
            $data["constract_drawing9blueg"]= $this->input->post("constract_drawing9bluegtext");
            $data["constract_drawing10blueg"]= $this->input->post("constract_drawing10bluegtext");


            $data["date_insert_drawing1blueg"] = date('Y-m-d');
            $data["date_insert_drawing2blueg"] = date('Y-m-d');
            $data["date_insert_drawing3blueg"] = date('Y-m-d');
            $data["date_insert_drawing4blueg"] = date('Y-m-d');
            $data["date_insert_drawing5blueg"] = date('Y-m-d');
            $data["date_insert_drawing6blueg"] = date('Y-m-d');
            $data["date_insert_drawing7blueg"] = date('Y-m-d');
            $data["date_insert_drawing8blueg"] = date('Y-m-d');
            $data["date_insert_drawing9blueg"] = date('Y-m-d');
            $data["date_insert_drawing10blueg"] = date('Y-m-d');
        }

        //admin//

        if(!empty($this->input->post("constract_drawing1text")))
        // if($_FILES['constract_drawing1']['size']!=0)
        {

         
            $data["check1"] = $this->input->post("check1");
            $data["desc_item1"] = mb_convert_kana($this->input->post("desc_item1"), "as");
            $data["check2"] = $this->input->post("check2");
            $data["desc_item2"] = mb_convert_kana($this->input->post("desc_item2"), "as");
            $data["check3"] = $this->input->post("check3");
            $data["desc_item3"] = mb_convert_kana($this->input->post("desc_item3"), "as");
            $data["check4"] = $this->input->post("check4");
            $data["desc_item4"] = mb_convert_kana($this->input->post("desc_item4"), "as");
            $data["check5"] = $this->input->post("check5");
            $data["desc_item5"] = mb_convert_kana($this->input->post("desc_item5"), "as");
            $data["check6"] = $this->input->post("check6");
            $data["desc_item6"] = mb_convert_kana($this->input->post("desc_item6"), "as");
            $data["check7"] = $this->input->post("check7");
            $data["desc_item7"] = mb_convert_kana($this->input->post("desc_item7"), "as");
            $data["check8"] = $this->input->post("check8");
            $data["desc_item8"] = mb_convert_kana($this->input->post("desc_item8"), "as");
            $data["check9"] = $this->input->post("check9");
            $data["desc_item9"] = mb_convert_kana($this->input->post("desc_item9"), "as");
            $data["check10"] = $this->input->post("check10");
            $data["desc_item10"] = mb_convert_kana($this->input->post("desc_item10"), "as");
            $data["category_name"] = "請負契約書・土地契約書";
            // $data["type_item"] = "admin";


            $data["constract_drawing1"]= $this->input->post("constract_drawing1text");
            $data["constract_drawing2"]= $this->input->post("constract_drawing2text");
            $data["constract_drawing3"]= $this->input->post("constract_drawing3text");
            $data["constract_drawing4"]= $this->input->post("constract_drawing4text");
            $data["constract_drawing5"]= $this->input->post("constract_drawing5text");
            $data["constract_drawing6"]= $this->input->post("constract_drawing6text");
            $data["constract_drawing7"]= $this->input->post("constract_drawing7text");
            $data["constract_drawing8"]= $this->input->post("constract_drawing8text");
            $data["constract_drawing9"]= $this->input->post("constract_drawing9text");
            $data["constract_drawing10"]= $this->input->post("constract_drawing10text");


            $data["date_insert_drawing1"] = date('Y-m-d');
            $data["date_insert_drawing2"] = date('Y-m-d');
            $data["date_insert_drawing3"] = date('Y-m-d');
            $data["date_insert_drawing4"] = date('Y-m-d');
            $data["date_insert_drawing5"] = date('Y-m-d');
            $data["date_insert_drawing6"] = date('Y-m-d');
            $data["date_insert_drawing7"] = date('Y-m-d');
            $data["date_insert_drawing8"] = date('Y-m-d');
            $data["date_insert_drawing9"] = date('Y-m-d');
            $data["date_insert_drawing10"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1btext")))
        // if($_FILES['constract_drawing1b']['size']!=0)
        {

            $data["check1b"] = $this->input->post("check1b");
            $data["desc_item1b"] = mb_convert_kana($this->input->post("desc_item1b"), "as");
            $data["check2b"] = $this->input->post("check2b");
            $data["desc_item2b"] = mb_convert_kana($this->input->post("desc_item2b"), "as");
            $data["check3b"] = $this->input->post("check3b");
            $data["desc_item3b"] = mb_convert_kana($this->input->post("desc_item3b"), "as");
            $data["check4b"] = $this->input->post("check4b");
            $data["desc_item4b"] = mb_convert_kana($this->input->post("desc_item4b"), "as");
            $data["check5b"] = $this->input->post("check5b");
            $data["desc_item5b"] = mb_convert_kana($this->input->post("desc_item5b"), "as");
            $data["check6b"] = $this->input->post("check6b");
            $data["desc_item6b"] = mb_convert_kana($this->input->post("desc_item6b"), "as");
            $data["check7b"] = $this->input->post("check7b");
            $data["desc_item7b"] = mb_convert_kana($this->input->post("desc_item7b"), "as");
            $data["check8b"] = $this->input->post("check8b");
            $data["desc_item8b"] = mb_convert_kana($this->input->post("desc_item8b"), "as");
            $data["check9b"] = $this->input->post("check9b");
            $data["desc_item9b"] = mb_convert_kana($this->input->post("desc_item9b"), "as");
            $data["check10b"] = $this->input->post("check10b");
            $data["desc_item10b"] = mb_convert_kana($this->input->post("desc_item10b"), "as");
            $data["category_name1b"] = "長期優良住宅認定書・性能評価書";
            // $data["type_item"] = "admin";


            $data["constract_drawing1b"]= $this->input->post("constract_drawing1btext");
            $data["constract_drawing2b"]= $this->input->post("constract_drawing2btext");
            $data["constract_drawing3b"]= $this->input->post("constract_drawing3btext");
            $data["constract_drawing4b"]= $this->input->post("constract_drawing4btext");
            $data["constract_drawing5b"]= $this->input->post("constract_drawing5btext");
            $data["constract_drawing6b"]= $this->input->post("constract_drawing6btext");
            $data["constract_drawing7b"]= $this->input->post("constract_drawing7btext");
            $data["constract_drawing8b"]= $this->input->post("constract_drawing8btext");
            $data["constract_drawing9b"]= $this->input->post("constract_drawing9btext");
            $data["constract_drawing10b"]= $this->input->post("constract_drawing10btext");


            $data["date_insert_drawing1b"] = date('Y-m-d');
            $data["date_insert_drawing2b"] = date('Y-m-d');
            $data["date_insert_drawing3b"] = date('Y-m-d');
            $data["date_insert_drawing4b"] = date('Y-m-d');
            $data["date_insert_drawing5b"] = date('Y-m-d');
            $data["date_insert_drawing6b"] = date('Y-m-d');
            $data["date_insert_drawing7b"] = date('Y-m-d');
            $data["date_insert_drawing8b"] = date('Y-m-d');
            $data["date_insert_drawing9b"] = date('Y-m-d');
            $data["date_insert_drawing10b"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ctext")))
        // if($_FILES['constract_drawing1c']['size']!=0)
        {

            $data["check1c"] = $this->input->post("check1c");
            $data["desc_item1c"] = mb_convert_kana($this->input->post("desc_item1c"), "as");
            $data["check2c"] = $this->input->post("check2c");
            $data["desc_item2c"] = mb_convert_kana($this->input->post("desc_item2c"), "as");
            $data["check3c"] = $this->input->post("check3c");
            $data["desc_item3c"] = mb_convert_kana($this->input->post("desc_item3c"), "as");
            $data["check4c"] = $this->input->post("check4c");
            $data["desc_item4c"] = mb_convert_kana($this->input->post("desc_item4c"), "as");
            $data["check5c"] = $this->input->post("check5c");
            $data["desc_item5c"] = mb_convert_kana($this->input->post("desc_item5c"), "as");
            $data["check6c"] = $this->input->post("check6c");
            $data["desc_item6c"] = mb_convert_kana($this->input->post("desc_item6c"), "as");
            $data["check7c"] = $this->input->post("check7c");
            $data["desc_item7c"] = mb_convert_kana($this->input->post("desc_item7c"), "as");
            $data["check8c"] = $this->input->post("check8c");
            $data["desc_item8c"] = mb_convert_kana($this->input->post("desc_item8c"), "as");
            $data["check9c"] = $this->input->post("check9c");
            $data["desc_item9c"] = mb_convert_kana($this->input->post("desc_item9c"), "as");
            $data["check10c"] = $this->input->post("check10c");
            $data["desc_item10c"] = mb_convert_kana($this->input->post("desc_item10c"), "as");
            $data["category_name1c"] = "地盤調査・地盤改良";
            // $data["type_item"] = "admin";


            $data["constract_drawing1c"]= $this->input->post("constract_drawing1ctext");
            $data["constract_drawing2c"]= $this->input->post("constract_drawing2ctext");
            $data["constract_drawing3c"]= $this->input->post("constract_drawing3ctext");
            $data["constract_drawing4c"]= $this->input->post("constract_drawing4ctext");
            $data["constract_drawing5c"]= $this->input->post("constract_drawing5ctext");
            $data["constract_drawing6c"]= $this->input->post("constract_drawing6ctext");
            $data["constract_drawing7c"]= $this->input->post("constract_drawing7ctext");
            $data["constract_drawing8c"]= $this->input->post("constract_drawing8ctext");
            $data["constract_drawing9c"]= $this->input->post("constract_drawing9ctext");
            $data["constract_drawing10c"]= $this->input->post("constract_drawing10ctext");


            $data["date_insert_drawing1c"] = date('Y-m-d');
            $data["date_insert_drawing2c"] = date('Y-m-d');
            $data["date_insert_drawing3c"] = date('Y-m-d');
            $data["date_insert_drawing4c"] = date('Y-m-d');
            $data["date_insert_drawing5c"] = date('Y-m-d');
            $data["date_insert_drawing6c"] = date('Y-m-d');
            $data["date_insert_drawing7c"] = date('Y-m-d');
            $data["date_insert_drawing8c"] = date('Y-m-d');
            $data["date_insert_drawing9c"] = date('Y-m-d');
            $data["date_insert_drawing10c"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1dtext")))
        // if($_FILES['constract_drawing1d']['size']!=0)
        {


            $data["check1d"] = $this->input->post("check1d");
            $data["desc_item1d"] = mb_convert_kana($this->input->post("desc_item1d"), "as");
            $data["check2d"] = $this->input->post("check2d");
            $data["desc_item2d"] = mb_convert_kana($this->input->post("desc_item2d"), "as");
            $data["check3d"] = $this->input->post("check3d");
            $data["desc_item3d"] = mb_convert_kana($this->input->post("desc_item3d"), "as");
            $data["check4d"] = $this->input->post("check4d");
            $data["desc_item4d"] = mb_convert_kana($this->input->post("desc_item4d"), "as");
            $data["check5d"] = $this->input->post("check5d");
            $data["desc_item5d"] = mb_convert_kana($this->input->post("desc_item5d"), "as");
            $data["check6d"] = $this->input->post("check6d");
            $data["desc_item6d"] = mb_convert_kana($this->input->post("desc_item6d"), "as");
            $data["check7d"] = $this->input->post("check7d");
            $data["desc_item7d"] = mb_convert_kana($this->input->post("desc_item7d"), "as");
            $data["check8d"] = $this->input->post("check8d");
            $data["desc_item8d"] = mb_convert_kana($this->input->post("desc_item8d"), "as");
            $data["check9d"] = $this->input->post("check9d");
            $data["desc_item9d"] = mb_convert_kana($this->input->post("desc_item9d"), "as");
            $data["check10d"] = $this->input->post("check10d");
            $data["desc_item10d"] = mb_convert_kana($this->input->post("desc_item10d"), "as");
            $data["category_name1d"] = "点検履歴";
            // $data["type_item"] = "admin";


            $data["constract_drawing1d"]= $this->input->post("constract_drawing1dtext");
            $data["constract_drawing2d"]= $this->input->post("constract_drawing2dtext");
            $data["constract_drawing3d"]= $this->input->post("constract_drawing3dtext");
            $data["constract_drawing4d"]= $this->input->post("constract_drawing4dtext");
            $data["constract_drawing5d"]= $this->input->post("constract_drawing5dtext");
            $data["constract_drawing6d"]= $this->input->post("constract_drawing6dtext");
            $data["constract_drawing7d"]= $this->input->post("constract_drawing7dtext");
            $data["constract_drawing8d"]= $this->input->post("constract_drawing8dtext");
            $data["constract_drawing9d"]= $this->input->post("constract_drawing9dtext");
            $data["constract_drawing10d"]= $this->input->post("constract_drawing10dtext");


            $data["date_insert_drawing1d"] = date('Y-m-d');
            $data["date_insert_drawing2d"] = date('Y-m-d');
            $data["date_insert_drawing3d"] = date('Y-m-d');
            $data["date_insert_drawing4d"] = date('Y-m-d');
            $data["date_insert_drawing5d"] = date('Y-m-d');
            $data["date_insert_drawing6d"] = date('Y-m-d');
            $data["date_insert_drawing7d"] = date('Y-m-d');
            $data["date_insert_drawing8d"] = date('Y-m-d');
            $data["date_insert_drawing9d"] = date('Y-m-d');
            $data["date_insert_drawing10d"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1etext")))
        // if($_FILES['constract_drawing1e']['size']!=0)
        {

            $data["check1e"] = $this->input->post("check1e");
            $data["desc_item1e"] = mb_convert_kana($this->input->post("desc_item1e"), "as");
            $data["check2e"] = $this->input->post("check2e");
            $data["desc_item2e"] = mb_convert_kana($this->input->post("desc_item2e"), "as");
            $data["check3e"] = $this->input->post("check3e");
            $data["desc_item3e"] = mb_convert_kana($this->input->post("desc_item3e"), "as");
            $data["check4e"] = $this->input->post("check4e");
            $data["desc_item4e"] = mb_convert_kana($this->input->post("desc_item4e"), "as");
            $data["check5e"] = $this->input->post("check5e");
            $data["desc_item5e"] = mb_convert_kana($this->input->post("desc_item5e"), "as");
            $data["check6e"] = $this->input->post("check6e");
            $data["desc_item6e"] = mb_convert_kana($this->input->post("desc_item6e"), "as");
            $data["check7e"] = $this->input->post("check7e");
            $data["desc_item7e"] = mb_convert_kana($this->input->post("desc_item7e"), "as");
            $data["check8e"] = $this->input->post("check8e");
            $data["desc_item8e"] = mb_convert_kana($this->input->post("desc_item8e"), "as");
            $data["check9e"] = $this->input->post("check9e");
            $data["desc_item9e"] = mb_convert_kana($this->input->post("desc_item9e"), "as");
            $data["check10e"] = $this->input->post("check10e");
            $data["desc_item10e"] = mb_convert_kana($this->input->post("desc_item10e"), "as");
            $data["category_name1e"] = "三者引継ぎ合意書";
            // $data["type_item"] = "admin";
 

            $data["constract_drawing1e"]= $this->input->post("constract_drawing1etext");
            $data["constract_drawing2e"]= $this->input->post("constract_drawing2etext");
            $data["constract_drawing3e"]= $this->input->post("constract_drawing3etext");
            $data["constract_drawing4e"]= $this->input->post("constract_drawing4etext");
            $data["constract_drawing5e"]= $this->input->post("constract_drawing5etext");
            $data["constract_drawing6e"]= $this->input->post("constract_drawing6etext");
            $data["constract_drawing7e"]= $this->input->post("constract_drawing7etext");
            $data["constract_drawing8e"]= $this->input->post("constract_drawing8etext");
            $data["constract_drawing9e"]= $this->input->post("constract_drawing9etext");
            $data["constract_drawing10e"]= $this->input->post("constract_drawing10etext");

            $data["date_insert_drawing1e"] = date('Y-m-d');
            $data["date_insert_drawing2e"] = date('Y-m-d');
            $data["date_insert_drawing3e"] = date('Y-m-d');
            $data["date_insert_drawing4e"] = date('Y-m-d');
            $data["date_insert_drawing5e"] = date('Y-m-d');
            $data["date_insert_drawing6e"] = date('Y-m-d');
            $data["date_insert_drawing7e"] = date('Y-m-d');
            $data["date_insert_drawing8e"] = date('Y-m-d');
            $data["date_insert_drawing9e"] = date('Y-m-d');
            $data["date_insert_drawing10e"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ftext")))
        // if($_FILES['constract_drawing1f']['size']!=0)
        {

            $data["check1f"] = $this->input->post("check1f");
            $data["desc_item1f"] = mb_convert_kana($this->input->post("desc_item1f"), "as");
            $data["check2f"] = $this->input->post("check2f");
            $data["desc_item2f"] = mb_convert_kana($this->input->post("desc_item2f"), "as");
            $data["check3f"] = $this->input->post("check3f");
            $data["desc_item3f"] = mb_convert_kana($this->input->post("desc_item3f"), "as");
            $data["check4f"] = $this->input->post("check4f");
            $data["desc_item4f"] = mb_convert_kana($this->input->post("desc_item4f"), "as");
            $data["check5f"] = $this->input->post("check5f");
            $data["desc_item5f"] = mb_convert_kana($this->input->post("desc_item5f"), "as");
            $data["check6f"] = $this->input->post("check6f");
            $data["desc_item6f"] = mb_convert_kana($this->input->post("desc_item6f"), "as");
            $data["check7f"] = $this->input->post("check7f");
            $data["desc_item7f"] = mb_convert_kana($this->input->post("desc_item7f"), "as");
            $data["check8f"] = $this->input->post("check8f");
            $data["desc_item8f"] = mb_convert_kana($this->input->post("desc_item8f"), "as");
            $data["check9f"] = $this->input->post("check9f");
            $data["desc_item9f"] = mb_convert_kana($this->input->post("desc_item9f"), "as");
            $data["check10f"] = $this->input->post("check10f");
            $data["desc_item10f"] = mb_convert_kana($this->input->post("desc_item10f"), "as");
            $data["category_name1f"] = "住宅仕様書・プレカット資料";
            // $data["type_item"] = "admin";


            $data["constract_drawing1f"]= $this->input->post("constract_drawing1ftext");
            $data["constract_drawing2f"]= $this->input->post("constract_drawing2ftext");
            $data["constract_drawing3f"]= $this->input->post("constract_drawing3ftext");
            $data["constract_drawing4f"]= $this->input->post("constract_drawing4ftext");
            $data["constract_drawing5f"]= $this->input->post("constract_drawing5ftext");
            $data["constract_drawing6f"]= $this->input->post("constract_drawing6ftext");
            $data["constract_drawing7f"]= $this->input->post("constract_drawing7ftext");
            $data["constract_drawing8f"]= $this->input->post("constract_drawing8ftext");
            $data["constract_drawing9f"]= $this->input->post("constract_drawing9ftext");
            $data["constract_drawing10f"]= $this->input->post("constract_drawing10ftext");


            $data["date_insert_drawing1f"] = date('Y-m-d');
            $data["date_insert_drawing2f"] = date('Y-m-d');
            $data["date_insert_drawing3f"] = date('Y-m-d');
            $data["date_insert_drawing4f"] = date('Y-m-d');
            $data["date_insert_drawing5f"] = date('Y-m-d');
            $data["date_insert_drawing6f"] = date('Y-m-d');
            $data["date_insert_drawing7f"] = date('Y-m-d');
            $data["date_insert_drawing8f"] = date('Y-m-d');
            $data["date_insert_drawing9f"] = date('Y-m-d');
            $data["date_insert_drawing10f"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1gtext")))
        // if($_FILES['constract_drawing1g']['size']!=0)
        {

            $data["check1g"] = $this->input->post("check1g");
            $data["desc_item1g"] = mb_convert_kana($this->input->post("desc_item1g"), "as");
            $data["check2g"] = $this->input->post("check2g");
            $data["desc_item2g"] = mb_convert_kana($this->input->post("desc_item2g"), "as");
            $data["check3g"] = $this->input->post("check3g");
            $data["desc_item3g"] = mb_convert_kana($this->input->post("desc_item3g"), "as");
            $data["check4g"] = $this->input->post("check4g");
            $data["desc_item4g"] = mb_convert_kana($this->input->post("desc_item4g"), "as");
            $data["check5g"] = $this->input->post("check5g");
            $data["desc_item5g"] = mb_convert_kana($this->input->post("desc_item5g"), "as");
            $data["check6g"] = $this->input->post("check6g");
            $data["desc_item6g"] = mb_convert_kana($this->input->post("desc_item6g"), "as");
            $data["check7g"] = $this->input->post("check7g");
            $data["desc_item7g"] = mb_convert_kana($this->input->post("desc_item7g"), "as");
            $data["check8g"] = $this->input->post("check8g");
            $data["desc_item8g"] = mb_convert_kana($this->input->post("desc_item8g"), "as");
            $data["check9g"] = $this->input->post("check9g");
            $data["desc_item9g"] = mb_convert_kana($this->input->post("desc_item9g"), "as");
            $data["check10g"] = $this->input->post("check10g");
            $data["desc_item10g"] = mb_convert_kana($this->input->post("desc_item10g"), "as");
            $data["category_name1g"] = "工事写真";
            // $data["type_item"] = "admin";


            $data["constract_drawing1g"]= $this->input->post("constract_drawing1gtext");
            $data["constract_drawing2g"]= $this->input->post("constract_drawing2gtext");
            $data["constract_drawing3g"]= $this->input->post("constract_drawing3gtext");
            $data["constract_drawing4g"]= $this->input->post("constract_drawing4gtext");
            $data["constract_drawing5g"]= $this->input->post("constract_drawing5gtext");
            $data["constract_drawing6g"]= $this->input->post("constract_drawing6gtext");
            $data["constract_drawing7g"]= $this->input->post("constract_drawing7gtext");
            $data["constract_drawing8g"]= $this->input->post("constract_drawing8gtext");
            $data["constract_drawing9g"]= $this->input->post("constract_drawing9gtext");
            $data["constract_drawing10g"]= $this->input->post("constract_drawing10gtext");


            $data["date_insert_drawing1g"] = date('Y-m-d');
            $data["date_insert_drawing2g"] = date('Y-m-d');
            $data["date_insert_drawing3g"] = date('Y-m-d');
            $data["date_insert_drawing4g"] = date('Y-m-d');
            $data["date_insert_drawing5g"] = date('Y-m-d');
            $data["date_insert_drawing6g"] = date('Y-m-d');
            $data["date_insert_drawing7g"] = date('Y-m-d');
            $data["date_insert_drawing8g"] = date('Y-m-d');
            $data["date_insert_drawing9g"] = date('Y-m-d');
            $data["date_insert_drawing10g"] = date('Y-m-d');
        }

        
        // echo json_encode($data, JSON_UNESCAPED_UNICODE);

        $data = $this->security->xss_clean($data);

        $this->load->view('dashboard/constructor/preview_v',$data);
    }

    public function check_data()
    {
        if($this->input->post('edit_btn') == 'edit')
        {

            $data["cust_code"] = mb_convert_kana($this->input->post("cust_code"), "as");
            $data["cust_name"] = mb_convert_kana($this->input->post("cust_name"), "as");
            $data["kinds"] = mb_convert_kana($this->input->post("kinds"), "as");
            $data["const_no"] = mb_convert_kana($this->input->post("const_no"), "as");
            $data["remarks"] = mb_convert_kana($this->input->post("remarks"), "as");
            $data["contract_date"] = $this->input->post("contract_date");
            $data["construction_start_date"] = $this->input->post("construction_start_date");
            $data["upper_building_date"] = $this->input->post("upper_building_date");
            $data["completion_date"] = $this->input->post("completion_date");
            $data["delivery_date"] = $this->input->post("delivery_date");
            $data["amount_money"] = mb_convert_kana($this->input->post("amount_money"), "as");
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff"), "as");
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction"), "as");
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator"), "as");
            $data["construct_stat"] = $this->input->post("construct_stat");


            if(!empty($this->input->post("constract_drawing1bluea")))
            {
                $data["check1bluea"] = $this->input->post("check1bluea");
                $data["desc_item1bluea"] = mb_convert_kana($this->input->post("desc_item1bluea"), "as");
                $data["check2bluea"] = $this->input->post("check2bluea");
                $data["desc_item2bluea"] = mb_convert_kana($this->input->post("desc_item2bluea"), "as");
                $data["check3bluea"] = $this->input->post("check3bluea");
                $data["desc_item3bluea"] = mb_convert_kana($this->input->post("desc_item3bluea"), "as");
                $data["check4bluea"] = $this->input->post("check4bluea");
                $data["desc_item4bluea"] = mb_convert_kana($this->input->post("desc_item4bluea"), "as");
                $data["check5bluea"] = $this->input->post("check5bluea");
                $data["desc_item5bluea"] = mb_convert_kana($this->input->post("desc_item5bluea"), "as");
                $data["check6bluea"] = $this->input->post("check6bluea");
                $data["desc_item6bluea"] = mb_convert_kana($this->input->post("desc_item6bluea"), "as");
                $data["check7bluea"] = $this->input->post("check7bluea");
                $data["desc_item7bluea"] = mb_convert_kana($this->input->post("desc_item7bluea"), "as");
                $data["check8bluea"] = $this->input->post("check8bluea");
                $data["desc_item8bluea"] = mb_convert_kana($this->input->post("desc_item8bluea"), "as");
                $data["check9bluea"] = $this->input->post("check9bluea");
                $data["desc_item9bluea"] = mb_convert_kana($this->input->post("desc_item9bluea"), "as");
                $data["check10bluea"] = $this->input->post("check10bluea");
                $data["desc_item10bluea"] = mb_convert_kana($this->input->post("desc_item10bluea"), "as");
                $data["category_name1bluea"] = "確認申請書一式";
                $data["type_item"] = "user";

                // if (!empty($this->input->post("co_owner1bluea")))
                // {
                    $data["co_owner1bluea"] = $this->input->post("co_owner1bluea");
                // }
                // else
                // {
                //     $data["co_owner1bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner2bluea")))
                // {
                    $data["co_owner2bluea"] = $this->input->post("co_owner2bluea");
                // }
                // // else
                // // {
                // //     $data["co_owner2bluea"] = '';
                // // }
                
                // if (!empty($this->input->post("co_owner3bluea")))
                // {
                    $data["co_owner3bluea"] = $this->input->post("co_owner3bluea");
                // }
                // else
                // {
                //     $data["co_owner3bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner4bluea")))
                // {
                    $data["co_owner4bluea"] = $this->input->post("co_owner4bluea");
                // }
                // else
                // {
                //     $data["co_owner4bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner5bluea")))
                // {
                    $data["co_owner5bluea"] = $this->input->post("co_owner5bluea");
                // }
                // else
                // {
                //     $data["co_owner5bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner6bluea")))
                // {
                    $data["co_owner6bluea"] = $this->input->post("co_owner6bluea");
                // }
                // else
                // {
                //     $data["co_owner6bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner7bluea")))
                // {
                    $data["co_owner7bluea"] = $this->input->post("co_owner7bluea");
                // }
                // else
                // {
                //     $data["co_owner7bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner8bluea")))
                // {
                    $data["co_owner8bluea"] = $this->input->post("co_owner8bluea");
                // }
                // else
                // {
                //     $data["co_owner8bluea"] = '';
                // }
                
                // if (!empty($this->input->post("co_owner9bluea")))
                // {
                    $data["co_owner9bluea"] = $this->input->post("co_owner9bluea");
                // }
                // else
                // {
                //     $data["co_owner9bluea"] = '';
                // }

                // if (!empty($this->input->post("co_owner10bluea")))
                // {
                    $data["co_owner10bluea"] = $this->input->post("co_owner10bluea");
                // }
                // else
                // {
                //     $data["co_owner10bluea"] = '';
                // }

                $data["constract_drawing1bluea"]= $this->input->post("constract_drawing1bluea");
                $data["constract_drawing2bluea"]= $this->input->post("constract_drawing2bluea");
                $data["constract_drawing3bluea"]= $this->input->post("constract_drawing3bluea");
                $data["constract_drawing4bluea"]= $this->input->post("constract_drawing4bluea");
                $data["constract_drawing5bluea"]= $this->input->post("constract_drawing5bluea");
                $data["constract_drawing6bluea"]= $this->input->post("constract_drawing6bluea");
                $data["constract_drawing7bluea"]= $this->input->post("constract_drawing7bluea");
                $data["constract_drawing8bluea"]= $this->input->post("constract_drawing8bluea");
                $data["constract_drawing9bluea"]= $this->input->post("constract_drawing9bluea");
                $data["constract_drawing10bluea"]= $this->input->post("constract_drawing10bluea");

                $data["date_insert_drawing1bluea"] = date('Y-m-d');
                $data["date_insert_drawing2bluea"] = date('Y-m-d');
                $data["date_insert_drawing3bluea"] = date('Y-m-d');
                $data["date_insert_drawing4bluea"] = date('Y-m-d');
                $data["date_insert_drawing5bluea"] = date('Y-m-d');
                $data["date_insert_drawing6bluea"] = date('Y-m-d');
                $data["date_insert_drawing7bluea"] = date('Y-m-d');
                $data["date_insert_drawing8bluea"] = date('Y-m-d');
                $data["date_insert_drawing9bluea"] = date('Y-m-d');
                $data["date_insert_drawing10bluea"] = date('Y-m-d');

            }
            
            if(!empty($this->input->post("constract_drawing1blueb")))
            {

                $data["check1blueb"] = $this->input->post("check1blueb");
                $data["desc_item1blueb"] = mb_convert_kana($this->input->post("desc_item1blueb"), "as");
                $data["check2blueb"] = $this->input->post("check2blueb");
                $data["desc_item2blueb"] = mb_convert_kana($this->input->post("desc_item2blueb"), "as");
                $data["check3blueb"] = $this->input->post("check3blueb");
                $data["desc_item3blueb"] = mb_convert_kana($this->input->post("desc_item3blueb"), "as");
                $data["check4blueb"] = $this->input->post("check4blueb");
                $data["desc_item4blueb"] = mb_convert_kana($this->input->post("desc_item4blueb"), "as");
                $data["check5blueb"] = $this->input->post("check5blueb");
                $data["desc_item5blueb"] = mb_convert_kana($this->input->post("desc_item5blueb"), "as");
                $data["check6blueb"] = $this->input->post("check6blueb");
                $data["desc_item6blueb"] = mb_convert_kana($this->input->post("desc_item6blueb"), "as");
                $data["check7blueb"] = $this->input->post("check7blueb");
                $data["desc_item7blueb"] = mb_convert_kana($this->input->post("desc_item7blueb"), "as");
                $data["check8blueb"] = $this->input->post("check8blueb");
                $data["desc_item8blueb"] = mb_convert_kana($this->input->post("desc_item8blueb"), "as");
                $data["check9blueb"] = $this->input->post("check9blueb");
                $data["desc_item9blueb"] = mb_convert_kana($this->input->post("desc_item9blueb"), "as");
                $data["check10blueb"] = $this->input->post("check10blueb");
                $data["desc_item10blueb"] = mb_convert_kana($this->input->post("desc_item10blueb"), "as");
                $data["category_name1blueb"] = "工事工程表";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1blueb")))
                {
                    $data["co_owner1blueb"] = $this->input->post("co_owner1blueb");
                }
                else
                {
                    $data["co_owner1blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blueb")))
                {
                    $data["co_owner2blueb"] = $this->input->post("co_owner2blueb");
                }
                else
                {
                    $data["co_owner2blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blueb")))
                {
                    $data["co_owner3blueb"] = $this->input->post("co_owner3blueb");
                }
                else
                {
                    $data["co_owner3blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blueb")))
                {
                    $data["co_owner4blueb"] = $this->input->post("co_owner4blueb");
                }
                else
                {
                    $data["co_owner4blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blueb")))
                {
                    $data["co_owner5blueb"] = $this->input->post("co_owner5blueb");
                }
                else
                {
                    $data["co_owner5blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blueb")))
                {
                    $data["co_owner6blueb"] = $this->input->post("co_owner6blueb");
                }
                else
                {
                    $data["co_owner6blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blueb")))
                {
                    $data["co_owner7blueb"] = $this->input->post("co_owner7blueb");
                }
                else
                {
                    $data["co_owner7blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blueb")))
                {
                    $data["co_owner8blueb"] = $this->input->post("co_owner8blueb");
                }
                else
                {
                    $data["co_owner8blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blueb")))
                {
                    $data["co_owner9blueb"] = $this->input->post("co_owner9blueb");
                }
                else
                {
                    $data["co_owner9blueb"] = '';
                }

                if (!empty($this->input->post("co_owner10blueb")))
                {
                    $data["co_owner10blueb"] = $this->input->post("co_owner10blueb");
                }
                else
                {
                    $data["co_owner10blueb"] = '';
                }




                $data["constract_drawing1blueb"]= $this->input->post("constract_drawing1blueb");
                $data["constract_drawing2blueb"]= $this->input->post("constract_drawing2blueb");
                $data["constract_drawing3blueb"]= $this->input->post("constract_drawing3blueb");
                $data["constract_drawing4blueb"]= $this->input->post("constract_drawing4blueb");
                $data["constract_drawing5blueb"]= $this->input->post("constract_drawing5blueb");
                $data["constract_drawing6blueb"]= $this->input->post("constract_drawing6blueb");
                $data["constract_drawing7blueb"]= $this->input->post("constract_drawing7blueb");
                $data["constract_drawing8blueb"]= $this->input->post("constract_drawing8blueb");
                $data["constract_drawing9blueb"]= $this->input->post("constract_drawing9blueb");
                $data["constract_drawing10blueb"]= $this->input->post("constract_drawing10blueb");

                $data["date_insert_drawing1blueb"] = date('Y-m-d');
                $data["date_insert_drawing2blueb"] = date('Y-m-d');
                $data["date_insert_drawing3blueb"] = date('Y-m-d');
                $data["date_insert_drawing4blueb"] = date('Y-m-d');
                $data["date_insert_drawing5blueb"] = date('Y-m-d');
                $data["date_insert_drawing6blueb"] = date('Y-m-d');
                $data["date_insert_drawing7blueb"] = date('Y-m-d');
                $data["date_insert_drawing8blueb"] = date('Y-m-d');
                $data["date_insert_drawing9blueb"] = date('Y-m-d');
                $data["date_insert_drawing10blueb"] = date('Y-m-d');
            }
            
            if(!empty($this->input->post("constract_drawing1bluec")))
            {

                $data["check1bluec"] = $this->input->post("check1bluec");
                $data["desc_item1bluec"] = mb_convert_kana($this->input->post("desc_item1bluec"), "as");
                $data["check2bluec"] = $this->input->post("check2bluec");
                $data["desc_item2bluec"] = mb_convert_kana($this->input->post("desc_item2bluec"), "as");
                $data["check3bluec"] = $this->input->post("check3bluec");
                $data["desc_item3bluec"] = mb_convert_kana($this->input->post("desc_item3bluec"), "as");
                $data["check4bluec"] = $this->input->post("check4bluec");
                $data["desc_item4bluec"] = mb_convert_kana($this->input->post("desc_item4bluec"), "as");
                $data["check5bluec"] = $this->input->post("check5bluec");
                $data["desc_item5bluec"] = mb_convert_kana($this->input->post("desc_item5bluec"), "as");
                $data["check6bluec"] = $this->input->post("check6bluec");
                $data["desc_item6bluec"] = mb_convert_kana($this->input->post("desc_item6bluec"), "as");
                $data["check7bluec"] = $this->input->post("check7bluec");
                $data["desc_item7bluec"] = mb_convert_kana($this->input->post("desc_item7bluec"), "as");
                $data["check8bluec"] = $this->input->post("check8bluec");
                $data["desc_item8bluec"] = mb_convert_kana($this->input->post("desc_item8bluec"), "as");
                $data["check9bluec"] = $this->input->post("check9bluec");
                $data["desc_item9bluec"] = mb_convert_kana($this->input->post("desc_item9bluec"), "as");
                $data["check10bluec"] = $this->input->post("check10bluec");
                $data["desc_item10bluec"] = mb_convert_kana($this->input->post("desc_item10bluec"), "as");
                $data["category_name1bluec"] = "工事着工資料";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1bluec")))
                {
                    $data["co_owner1bluec"] = $this->input->post("co_owner1bluec");
                }
                else
                {
                    $data["co_owner1bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluec")))
                {
                    $data["co_owner2bluec"] = $this->input->post("co_owner2bluec");
                }
                else
                {
                    $data["co_owner2bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluec")))
                {
                    $data["co_owner3bluec"] = $this->input->post("co_owner3bluec");
                }
                else
                {
                    $data["co_owner3bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluec")))
                {
                    $data["co_owner4bluec"] = $this->input->post("co_owner4bluec");
                }
                else
                {
                    $data["co_owner4bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluec")))
                {
                    $data["co_owner5bluec"] = $this->input->post("co_owner5bluec");
                }
                else
                {
                    $data["co_owner5bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluec")))
                {
                    $data["co_owner6bluec"] = $this->input->post("co_owner6bluec");
                }
                else
                {
                    $data["co_owner6bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluec")))
                {
                    $data["co_owner7bluec"] = $this->input->post("co_owner7bluec");
                }
                else
                {
                    $data["co_owner7bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluec")))
                {
                    $data["co_owner8bluec"] = $this->input->post("co_owner8bluec");
                }
                else
                {
                    $data["co_owner8bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluec")))
                {
                    $data["co_owner9bluec"] = $this->input->post("co_owner9bluec");
                }
                else
                {
                    $data["co_owner9bluec"] = '';
                }

                if (!empty($this->input->post("co_owner10bluec")))
                {
                    $data["co_owner10bluec"] = $this->input->post("co_owner10bluec");
                }
                else
                {
                    $data["co_owner10bluec"] = '';
                }


                $data["constract_drawing1bluec"]= $this->input->post("constract_drawing1bluec");
                $data["constract_drawing2bluec"]= $this->input->post("constract_drawing2bluec");
                $data["constract_drawing3bluec"]= $this->input->post("constract_drawing3bluec");
                $data["constract_drawing4bluec"]= $this->input->post("constract_drawing4bluec");
                $data["constract_drawing5bluec"]= $this->input->post("constract_drawing5bluec");
                $data["constract_drawing6bluec"]= $this->input->post("constract_drawing6bluec");
                $data["constract_drawing7bluec"]= $this->input->post("constract_drawing7bluec");
                $data["constract_drawing8bluec"]= $this->input->post("constract_drawing8bluec");
                $data["constract_drawing9bluec"]= $this->input->post("constract_drawing9bluec");
                $data["constract_drawing10bluec"]= $this->input->post("constract_drawing10bluec");


                $data["date_insert_drawing1bluec"] = date('Y-m-d');
                $data["date_insert_drawing2bluec"] = date('Y-m-d');
                $data["date_insert_drawing3bluec"] = date('Y-m-d');
                $data["date_insert_drawing4bluec"] = date('Y-m-d');
                $data["date_insert_drawing5bluec"] = date('Y-m-d');
                $data["date_insert_drawing6bluec"] = date('Y-m-d');
                $data["date_insert_drawing7bluec"] = date('Y-m-d');
                $data["date_insert_drawing8bluec"] = date('Y-m-d');
                $data["date_insert_drawing9bluec"] = date('Y-m-d');
                $data["date_insert_drawing10bluec"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1blued")))
            {

                $data["check1blued"] = $this->input->post("check1blued");
                $data["desc_item1blued"] = mb_convert_kana($this->input->post("desc_item1blued"), "as");
                $data["check2blued"] = $this->input->post("check2blued");
                $data["desc_item2blued"] = mb_convert_kana($this->input->post("desc_item2blued"), "as");
                $data["check3blued"] = $this->input->post("check3blued");
                $data["desc_item3blued"] = mb_convert_kana($this->input->post("desc_item3blued"), "as");
                $data["check4blued"] = $this->input->post("check4blued");
                $data["desc_item4blued"] = mb_convert_kana($this->input->post("desc_item4blued"), "as");
                $data["check5blued"] = $this->input->post("check5blued");
                $data["desc_item5blued"] = mb_convert_kana($this->input->post("desc_item5blued"), "as");
                $data["check6blued"] = $this->input->post("check6blued");
                $data["desc_item6blued"] = mb_convert_kana($this->input->post("desc_item6blued"), "as");
                $data["check7blued"] = $this->input->post("check7blued");
                $data["desc_item7blued"] = mb_convert_kana($this->input->post("desc_item7blued"), "as");
                $data["check8blued"] = $this->input->post("check8blued");
                $data["desc_item8blued"] = mb_convert_kana($this->input->post("desc_item8blued"), "as");
                $data["check9blued"] = $this->input->post("check9blued");
                $data["desc_item9blued"] = mb_convert_kana($this->input->post("desc_item9blued"), "as");
                $data["check10blued"] = $this->input->post("check10blued");
                $data["desc_item10blued"] = mb_convert_kana($this->input->post("desc_item10blued"), "as");
                $data["category_name1blued"] = "施工図面";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1blued")))
                {
                    $data["co_owner1blued"] = $this->input->post("co_owner1blued");
                }
                else
                {
                    $data["co_owner1blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blued")))
                {
                    $data["co_owner2blued"] = $this->input->post("co_owner2blued");
                }
                else
                {
                    $data["co_owner2blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blued")))
                {
                    $data["co_owner3blued"] = $this->input->post("co_owner3blued");
                }
                else
                {
                    $data["co_owner3blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blued")))
                {
                    $data["co_owner4blued"] = $this->input->post("co_owner4blued");
                }
                else
                {
                    $data["co_owner4blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blued")))
                {
                    $data["co_owner5blued"] = $this->input->post("co_owner5blued");
                }
                else
                {
                    $data["co_owner5blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blued")))
                {
                    $data["co_owner6"] = $this->input->post("co_owner6blued");
                }
                else
                {
                    $data["co_owner6blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blued")))
                {
                    $data["co_owner7blued"] = $this->input->post("co_owner7blued");
                }
                else
                {
                    $data["co_owner7blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blued")))
                {
                    $data["co_owner8blued"] = $this->input->post("co_owner8blued");
                }
                else
                {
                    $data["co_owner8blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blued")))
                {
                    $data["co_owner9blued"] = $this->input->post("co_owner9blued");
                }
                else
                {
                    $data["co_owner9blued"] = '';
                }

                if (!empty($this->input->post("co_owner10blued")))
                {
                    $data["co_owner10blued"] = $this->input->post("co_owner10blued");
                }
                else
                {
                    $data["co_owner10blued"] = '';
                }




                $data["constract_drawing1blued"]= $this->input->post("constract_drawing1blued");
                $data["constract_drawing2blued"]= $this->input->post("constract_drawing2blued");
                $data["constract_drawing3blued"]= $this->input->post("constract_drawing3blued");
                $data["constract_drawing4blued"]= $this->input->post("constract_drawing4blued");
                $data["constract_drawing5blued"]= $this->input->post("constract_drawing5blued");
                $data["constract_drawing6blued"]= $this->input->post("constract_drawing6blued");
                $data["constract_drawing7blued"]= $this->input->post("constract_drawing7blued");
                $data["constract_drawing8blued"]= $this->input->post("constract_drawing8blued");
                $data["constract_drawing9blued"]= $this->input->post("constract_drawing9blued");
                $data["constract_drawing10blued"]= $this->input->post("constract_drawing10blued");


                $data["date_insert_drawing1blued"] = date('Y-m-d');
                $data["date_insert_drawing2blued"] = date('Y-m-d');
                $data["date_insert_drawing3blued"] = date('Y-m-d');
                $data["date_insert_drawing4blued"] = date('Y-m-d');
                $data["date_insert_drawing5blued"] = date('Y-m-d');
                $data["date_insert_drawing6blued"] = date('Y-m-d');
                $data["date_insert_drawing7blued"] = date('Y-m-d');
                $data["date_insert_drawing8blued"] = date('Y-m-d');
                $data["date_insert_drawing9blued"] = date('Y-m-d');
                $data["date_insert_drawing10blued"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1bluee")))
            {


                $data["check1bluee"] = $this->input->post("check1bluee");
                $data["desc_item1bluee"] = mb_convert_kana($this->input->post("desc_item1bluee"), "as");
                $data["check2bluee"] = $this->input->post("check2bluee");
                $data["desc_item2bluee"] = mb_convert_kana($this->input->post("desc_item2bluee"), "as");
                $data["check3bluee"] = $this->input->post("check3bluee");
                $data["desc_item3bluee"] = mb_convert_kana($this->input->post("desc_item3bluee"), "as");
                $data["check4bluee"] = $this->input->post("check4bluee");
                $data["desc_item4bluee"] = mb_convert_kana($this->input->post("desc_item4bluee"), "as");
                $data["check5bluee"] = $this->input->post("check5bluee");
                $data["desc_item5bluee"] = mb_convert_kana($this->input->post("desc_item5bluee"), "as");
                $data["check6bluee"] = $this->input->post("check6bluee");
                $data["desc_item6bluee"] = mb_convert_kana($this->input->post("desc_item6bluee"), "as");
                $data["check7bluee"] = $this->input->post("check7bluee");
                $data["desc_item7bluee"] = mb_convert_kana($this->input->post("desc_item7bluee"), "as");
                $data["check8bluee"] = $this->input->post("check8bluee");
                $data["desc_item8bluee"] = mb_convert_kana($this->input->post("desc_item8bluee"), "as");
                $data["check9bluee"] = $this->input->post("check9bluee");
                $data["desc_item9bluee"] = mb_convert_kana($this->input->post("desc_item9bluee"), "as");
                $data["check10bluee"] = $this->input->post("check10bluee");
                $data["desc_item10bluee"] = mb_convert_kana($this->input->post("desc_item10bluee"), "as");
                $data["category_name1bluee"] = "変更図面";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1bluee")))
                {
                    $data["co_owner1bluee"] = $this->input->post("co_owner1bluee");
                }
                else
                {
                    $data["co_owner1bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluee")))
                {
                    $data["co_owner2bluee"] = $this->input->post("co_owner2bluee");
                }
                else
                {
                    $data["co_owner2bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluee")))
                {
                    $data["co_owner3bluee"] = $this->input->post("co_owner3bluee");
                }
                else
                {
                    $data["co_owner3bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluee")))
                {
                    $data["co_owner4bluee"] = $this->input->post("co_owner4bluee");
                }
                else
                {
                    $data["co_owner4bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluee")))
                {
                    $data["co_owner5bluee"] = $this->input->post("co_owner5bluee");
                }
                else
                {
                    $data["co_owner5bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluee")))
                {
                    $data["co_owner6bluee"] = $this->input->post("co_owner6bluee");
                }
                else
                {
                    $data["co_owner6bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluee")))
                {
                    $data["co_owner7bluee"] = $this->input->post("co_owner7bluee");
                }
                else
                {
                    $data["co_owner7bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluee")))
                {
                    $data["co_owner8bluee"] = $this->input->post("co_owner8bluee");
                }
                else
                {
                    $data["co_owner8bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluee")))
                {
                    $data["co_owner9bluee"] = $this->input->post("co_owner9bluee");
                }
                else
                {
                    $data["co_owner9bluee"] = '';
                }

                if (!empty($this->input->post("co_owner10bluee")))
                {
                    $data["co_owner10bluee"] = $this->input->post("co_owner10bluee");
                }
                else
                {
                    $data["co_owner10bluee"] = '';
                }




                $data["constract_drawing1bluee"]= $this->input->post("constract_drawing1bluee");
                $data["constract_drawing2bluee"]= $this->input->post("constract_drawing2bluee");
                $data["constract_drawing3bluee"]= $this->input->post("constract_drawing3bluee");
                $data["constract_drawing4bluee"]= $this->input->post("constract_drawing4bluee");
                $data["constract_drawing5bluee"]= $this->input->post("constract_drawing5bluee");
                $data["constract_drawing6bluee"]= $this->input->post("constract_drawing6bluee");
                $data["constract_drawing7bluee"]= $this->input->post("constract_drawing7bluee");
                $data["constract_drawing8bluee"]= $this->input->post("constract_drawing8bluee");
                $data["constract_drawing9bluee"]= $this->input->post("constract_drawing9bluee");
                $data["constract_drawing10bluee"]= $this->input->post("constract_drawing10bluee");


                $data["date_insert_drawing1bluee"] = date('Y-m-d');
                $data["date_insert_drawing2bluee"] = date('Y-m-d');
                $data["date_insert_drawing3bluee"] = date('Y-m-d');
                $data["date_insert_drawing4bluee"] = date('Y-m-d');
                $data["date_insert_drawing5bluee"] = date('Y-m-d');
                $data["date_insert_drawing6bluee"] = date('Y-m-d');
                $data["date_insert_drawing7bluee"] = date('Y-m-d');
                $data["date_insert_drawing8bluee"] = date('Y-m-d');
                $data["date_insert_drawing9bluee"] = date('Y-m-d');
                $data["date_insert_drawing10bluee"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1bluef")))
            {

                $data["check1bluef"] = $this->input->post("check1bluef");
                $data["desc_item1bluef"] = mb_convert_kana($this->input->post("desc_item1bluef"), "as");
                $data["check2bluef"] = $this->input->post("check2bluef");
                $data["desc_item2bluef"] = mb_convert_kana($this->input->post("desc_item2bluef"), "as");
                $data["check3bluef"] = $this->input->post("check3bluef");
                $data["desc_item3bluef"] = mb_convert_kana($this->input->post("desc_item3bluef"), "as");
                $data["check4bluef"] = $this->input->post("check4bluef");
                $data["desc_item4bluef"] = mb_convert_kana($this->input->post("desc_item4bluef"), "as");
                $data["check5bluef"] = $this->input->post("check5bluef");
                $data["desc_item5bluef"] = mb_convert_kana($this->input->post("desc_item5bluef"), "as");
                $data["check6bluef"] = $this->input->post("check6bluef");
                $data["desc_item6bluef"] = mb_convert_kana($this->input->post("desc_item6bluef"), "as");
                $data["check7bluef"] = $this->input->post("check7bluef");
                $data["desc_item7bluef"] = mb_convert_kana($this->input->post("desc_item7bluef"), "as");
                $data["check8bluef"] = $this->input->post("check8bluef");
                $data["desc_item8bluef"] = mb_convert_kana($this->input->post("desc_item8bluef"), "as");
                $data["check9bluef"] = $this->input->post("check9bluef");
                $data["desc_item9bluef"] = mb_convert_kana($this->input->post("desc_item9bluef"), "as");
                $data["check10bluef"] = $this->input->post("check10bluef");
                $data["desc_item10bluef"] = mb_convert_kana($this->input->post("desc_item10bluef"), "as");
                $data["category_name1bluef"] = "設備プランシート";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1bluef")))
                {
                    $data["co_owner1bluef"] = $this->input->post("co_owner1bluef");
                }
                else
                {
                    $data["co_owner1bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluef")))
                {
                    $data["co_owner2bluef"] = $this->input->post("co_owner2bluef");
                }
                else
                {
                    $data["co_owner2bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluef")))
                {
                    $data["co_owner3bluef"] = $this->input->post("co_owner3bluef");
                }
                else
                {
                    $data["co_owner3bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluef")))
                {
                    $data["co_owner4bluef"] = $this->input->post("co_owner4bluef");
                }
                else
                {
                    $data["co_owner4bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluef")))
                {
                    $data["co_owner5bluef"] = $this->input->post("co_owner5bluef");
                }
                else
                {
                    $data["co_owner5bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluef")))
                {
                    $data["co_owner6bluef"] = $this->input->post("co_owner6bluef");
                }
                else
                {
                    $data["co_owner6bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluef")))
                {
                    $data["co_owner7bluef"] = $this->input->post("co_owner7bluef");
                }
                else
                {
                    $data["co_owner7bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluef")))
                {
                    $data["co_owner8bluef"] = $this->input->post("co_owner8bluef");
                }
                else
                {
                    $data["co_owner8bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluef")))
                {
                    $data["co_owner9bluef"] = $this->input->post("co_owner9bluef");
                }
                else
                {
                    $data["co_owner9bluef"] = '';
                }

                if (!empty($this->input->post("co_owner10bluef")))
                {
                    $data["co_owner10bluef"] = $this->input->post("co_owner10bluef");
                }
                else
                {
                    $data["co_owner10bluef"] = '';
                }


                $data["constract_drawing1bluef"]= $this->input->post("constract_drawing1bluef");
                $data["constract_drawing2bluef"]= $this->input->post("constract_drawing2bluef");
                $data["constract_drawing3bluef"]= $this->input->post("constract_drawing3bluef");
                $data["constract_drawing4bluef"]= $this->input->post("constract_drawing4bluef");
                $data["constract_drawing5bluef"]= $this->input->post("constract_drawing5bluef");
                $data["constract_drawing6bluef"]= $this->input->post("constract_drawing6bluef");
                $data["constract_drawing7bluef"]= $this->input->post("constract_drawing7bluef");
                $data["constract_drawing8bluef"]= $this->input->post("constract_drawing8bluef");
                $data["constract_drawing9bluef"]= $this->input->post("constract_drawing9bluef");
                $data["constract_drawing10bluef"]= $this->input->post("constract_drawing10bluef");


                $data["date_insert_drawing1bluef"] = date('Y-m-d');
                $data["date_insert_drawing2bluef"] = date('Y-m-d');
                $data["date_insert_drawing3bluef"] = date('Y-m-d');
                $data["date_insert_drawing4bluef"] = date('Y-m-d');
                $data["date_insert_drawing5bluef"] = date('Y-m-d');
                $data["date_insert_drawing6bluef"] = date('Y-m-d');
                $data["date_insert_drawing7bluef"] = date('Y-m-d');
                $data["date_insert_drawing8bluef"] = date('Y-m-d');
                $data["date_insert_drawing9bluef"] = date('Y-m-d');
                $data["date_insert_drawing10bluef"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1blueg")))
            {

                $data["check1blueg"] = $this->input->post("check1blueg");
                $data["desc_item1blueg"] = mb_convert_kana($this->input->post("desc_item1blueg"), "as");
                $data["check2blueg"] = $this->input->post("check2blueg");
                $data["desc_item2blueg"] = mb_convert_kana($this->input->post("desc_item2blueg"), "as");
                $data["check3blueg"] = $this->input->post("check3blueg");
                $data["desc_item3blueg"] = mb_convert_kana($this->input->post("desc_item3blueg"), "as");
                $data["check4blueg"] = $this->input->post("check4blueg");
                $data["desc_item4blueg"] = mb_convert_kana($this->input->post("desc_item4blueg"), "as");
                $data["check5blueg"] = $this->input->post("check5blueg");
                $data["desc_item5blueg"] = mb_convert_kana($this->input->post("desc_item5blueg"), "as");
                $data["check6blueg"] = $this->input->post("check6blueg");
                $data["desc_item6blueg"] = mb_convert_kana($this->input->post("desc_item6blueg"), "as");
                $data["check7blueg"] = $this->input->post("check7blueg");
                $data["desc_item7blueg"] = mb_convert_kana($this->input->post("desc_item7blueg"), "as");
                $data["check8blueg"] = $this->input->post("check8blueg");
                $data["desc_item8blueg"] = mb_convert_kana($this->input->post("desc_item8blueg"), "as");
                $data["check9blueg"] = $this->input->post("check9blueg");
                $data["desc_item9blueg"] = mb_convert_kana($this->input->post("desc_item9blueg"), "as");
                $data["check10blueg"] = $this->input->post("check10blueg");
                $data["desc_item10blueg"] = mb_convert_kana($this->input->post("desc_item10blueg"), "as");
                $data["category_name1blueg"] = "その他";
                $data["type_item"] = "user";

                if (!empty($this->input->post("co_owner1blueg")))
                {
                    $data["co_owner1blueg"] = $this->input->post("co_owner1blueg");
                }
                else
                {
                    $data["co_owner1blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blueg")))
                {
                    $data["co_owner2blueg"] = $this->input->post("co_owner2blueg");
                }
                else
                {
                    $data["co_owner2blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blueg")))
                {
                    $data["co_owner3blueg"] = $this->input->post("co_owner3blueg");
                }
                else
                {
                    $data["co_owner3blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blueg")))
                {
                    $data["co_owner4blueg"] = $this->input->post("co_owner4blueg");
                }
                else
                {
                    $data["co_owner4blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blueg")))
                {
                    $data["co_owner5blueg"] = $this->input->post("co_owner5blueg");
                }
                else
                {
                    $data["co_owner5blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blueg")))
                {
                    $data["co_owner6blueg"] = $this->input->post("co_owner6blueg");
                }
                else
                {
                    $data["co_owner6blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blueg")))
                {
                    $data["co_owner7blueg"] = $this->input->post("co_owner7blueg");
                }
                else
                {
                    $data["co_owner7blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blueg")))
                {
                    $data["co_owner8blueg"] = $this->input->post("co_owner8blueg");
                }
                else
                {
                    $data["co_owner8blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blueg")))
                {
                    $data["co_owner9blueg"] = $this->input->post("co_owner9blueg");
                }
                else
                {
                    $data["co_owner9blueg"] = '';
                }

                if (!empty($this->input->post("co_owner10blueg")))
                {
                    $data["co_owner10blueg"] = $this->input->post("co_owner10blueg");
                }
                else
                {
                    $data["co_owner10blueg"] = '';
                }


                $data["constract_drawing1blueg"]= $this->input->post("constract_drawing1blueg");
                $data["constract_drawing2blueg"]= $this->input->post("constract_drawing2blueg");
                $data["constract_drawing3blueg"]= $this->input->post("constract_drawing3blueg");
                $data["constract_drawing4blueg"]= $this->input->post("constract_drawing4blueg");
                $data["constract_drawing5blueg"]= $this->input->post("constract_drawing5blueg");
                $data["constract_drawing6blueg"]= $this->input->post("constract_drawing6blueg");
                $data["constract_drawing7blueg"]= $this->input->post("constract_drawing7blueg");
                $data["constract_drawing8blueg"]= $this->input->post("constract_drawing8blueg");
                $data["constract_drawing9blueg"]= $this->input->post("constract_drawing9blueg");
                $data["constract_drawing10blueg"]= $this->input->post("constract_drawing10blueg");


                $data["date_insert_drawing1blueg"] = date('Y-m-d');
                $data["date_insert_drawing2blueg"] = date('Y-m-d');
                $data["date_insert_drawing3blueg"] = date('Y-m-d');
                $data["date_insert_drawing4blueg"] = date('Y-m-d');
                $data["date_insert_drawing5blueg"] = date('Y-m-d');
                $data["date_insert_drawing6blueg"] = date('Y-m-d');
                $data["date_insert_drawing7blueg"] = date('Y-m-d');
                $data["date_insert_drawing8blueg"] = date('Y-m-d');
                $data["date_insert_drawing9blueg"] = date('Y-m-d');
                $data["date_insert_drawing10blueg"] = date('Y-m-d');
            }
            //admin//

            if(!empty($this->input->post("constract_drawing1")))
            {

                $data["check1"] = $this->input->post("check1");
                $data["desc_item1"] = mb_convert_kana($this->input->post("desc_item1"), "as");
                $data["check2"] = $this->input->post("check2");
                $data["desc_item2"] = mb_convert_kana($this->input->post("desc_item2"), "as");
                $data["check3"] = $this->input->post("check3");
                $data["desc_item3"] = mb_convert_kana($this->input->post("desc_item3"), "as");
                $data["check4"] = $this->input->post("check4");
                $data["desc_item4"] = mb_convert_kana($this->input->post("desc_item4"), "as");
                $data["check5"] = $this->input->post("check5");
                $data["desc_item5"] = mb_convert_kana($this->input->post("desc_item5"), "as");
                $data["check6"] = $this->input->post("check6");
                $data["desc_item6"] = mb_convert_kana($this->input->post("desc_item6"), "as");
                $data["check7"] = $this->input->post("check7");
                $data["desc_item7"] = mb_convert_kana($this->input->post("desc_item7"), "as");
                $data["check8"] = $this->input->post("check8");
                $data["desc_item8"] = mb_convert_kana($this->input->post("desc_item8"), "as");
                $data["check9"] = $this->input->post("check9");
                $data["desc_item9"] = mb_convert_kana($this->input->post("desc_item9"), "as");
                $data["check10"] = $this->input->post("check10");
                $data["desc_item10"] = mb_convert_kana($this->input->post("desc_item10"), "as");
                $data["category_name"] = "請負契約書・土地契約書";
                // $data["type_item"] = "admin";

                $data["constract_drawing1"]= $this->input->post("constract_drawing1");
                $data["constract_drawing2"]= $this->input->post("constract_drawing2");
                $data["constract_drawing3"]= $this->input->post("constract_drawing3");
                $data["constract_drawing4"]= $this->input->post("constract_drawing4");
                $data["constract_drawing5"]= $this->input->post("constract_drawing5");
                $data["constract_drawing6"]= $this->input->post("constract_drawing6");
                $data["constract_drawing7"]= $this->input->post("constract_drawing7");
                $data["constract_drawing8"]= $this->input->post("constract_drawing8");
                $data["constract_drawing9"]= $this->input->post("constract_drawing9");
                $data["constract_drawing10"]= $this->input->post("constract_drawing10");


                $data["date_insert_drawing1"] = date('Y-m-d');
                $data["date_insert_drawing2"] = date('Y-m-d');
                $data["date_insert_drawing3"] = date('Y-m-d');
                $data["date_insert_drawing4"] = date('Y-m-d');
                $data["date_insert_drawing5"] = date('Y-m-d');
                $data["date_insert_drawing6"] = date('Y-m-d');
                $data["date_insert_drawing7"] = date('Y-m-d');
                $data["date_insert_drawing8"] = date('Y-m-d');
                $data["date_insert_drawing9"] = date('Y-m-d');
                $data["date_insert_drawing10"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1b")))
            {


                $data["check1b"] = $this->input->post("check1b");
                $data["desc_item1b"] = mb_convert_kana($this->input->post("desc_item1b"), "as");
                $data["check2b"] = $this->input->post("check2b");
                $data["desc_item2b"] = mb_convert_kana($this->input->post("desc_item2b"), "as");
                $data["check3b"] = $this->input->post("check3b");
                $data["desc_item3b"] = mb_convert_kana($this->input->post("desc_item3b"), "as");
                $data["check4b"] = $this->input->post("check4b");
                $data["desc_item4b"] = mb_convert_kana($this->input->post("desc_item4b"), "as");
                $data["check5b"] = $this->input->post("check5b");
                $data["desc_item5b"] = mb_convert_kana($this->input->post("desc_item5b"), "as");
                $data["check6b"] = $this->input->post("check6b");
                $data["desc_item6b"] = mb_convert_kana($this->input->post("desc_item6b"), "as");
                $data["check7b"] = $this->input->post("check7b");
                $data["desc_item7b"] = mb_convert_kana($this->input->post("desc_item7b"), "as");
                $data["check8b"] = $this->input->post("check8b");
                $data["desc_item8b"] = mb_convert_kana($this->input->post("desc_item8b"), "as");
                $data["check9b"] = $this->input->post("check9b");
                $data["desc_item9b"] = mb_convert_kana($this->input->post("desc_item9b"), "as");
                $data["check10b"] = $this->input->post("check10b");
                $data["desc_item10b"] = mb_convert_kana($this->input->post("desc_item10b"), "as");
                $data["category_name1b"] = "長期優良住宅認定書・性能評価書";
                // $data["type_item"] = "admin";

                $data["constract_drawing1b"]= $this->input->post("constract_drawing1b");
                $data["constract_drawing2b"]= $this->input->post("constract_drawing2b");
                $data["constract_drawing3b"]= $this->input->post("constract_drawing3b");
                $data["constract_drawing4b"]= $this->input->post("constract_drawing4b");
                $data["constract_drawing5b"]= $this->input->post("constract_drawing5b");
                $data["constract_drawing6b"]= $this->input->post("constract_drawing6b");
                $data["constract_drawing7b"]= $this->input->post("constract_drawing7b");
                $data["constract_drawing8b"]= $this->input->post("constract_drawing8b");
                $data["constract_drawing9b"]= $this->input->post("constract_drawing9b");
                $data["constract_drawing10b"]= $this->input->post("constract_drawing10b");


                $data["date_insert_drawing1b"] = date('Y-m-d');
                $data["date_insert_drawing2b"] = date('Y-m-d');
                $data["date_insert_drawing3b"] = date('Y-m-d');
                $data["date_insert_drawing4b"] = date('Y-m-d');
                $data["date_insert_drawing5b"] = date('Y-m-d');
                $data["date_insert_drawing6b"] = date('Y-m-d');
                $data["date_insert_drawing7b"] = date('Y-m-d');
                $data["date_insert_drawing8b"] = date('Y-m-d');
                $data["date_insert_drawing9b"] = date('Y-m-d');
                $data["date_insert_drawing10b"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1c")))
            {

                $data["check1c"] = $this->input->post("check1c");
                $data["desc_item1c"] = mb_convert_kana($this->input->post("desc_item1c"), "as");
                $data["check2c"] = $this->input->post("check2c");
                $data["desc_item2c"] = mb_convert_kana($this->input->post("desc_item2c"), "as");
                $data["check3c"] = $this->input->post("check3c");
                $data["desc_item3c"] = mb_convert_kana($this->input->post("desc_item3c"), "as");
                $data["check4c"] = $this->input->post("check4c");
                $data["desc_item4c"] = mb_convert_kana($this->input->post("desc_item4c"), "as");
                $data["check5c"] = $this->input->post("check5c");
                $data["desc_item5c"] = mb_convert_kana($this->input->post("desc_item5c"), "as");
                $data["check6c"] = $this->input->post("check6c");
                $data["desc_item6c"] = mb_convert_kana($this->input->post("desc_item6c"), "as");
                $data["check7c"] = $this->input->post("check7c");
                $data["desc_item7c"] = mb_convert_kana($this->input->post("desc_item7c"), "as");
                $data["check8c"] = $this->input->post("check8c");
                $data["desc_item8c"] = mb_convert_kana($this->input->post("desc_item8c"), "as");
                $data["check9c"] = $this->input->post("check9c");
                $data["desc_item9c"] = mb_convert_kana($this->input->post("desc_item9c"), "as");
                $data["check10c"] = $this->input->post("check10c");
                $data["desc_item10c"] = mb_convert_kana($this->input->post("desc_item10c"), "as");
                $data["category_name1c"] = "地盤調査・地盤改良";
                // $data["type_item"] = "admin";

                $data["constract_drawing1c"]= $this->input->post("constract_drawing1c");
                $data["constract_drawing2c"]= $this->input->post("constract_drawing2c");
                $data["constract_drawing3c"]= $this->input->post("constract_drawing3c");
                $data["constract_drawing4c"]= $this->input->post("constract_drawing4c");
                $data["constract_drawing5c"]= $this->input->post("constract_drawing5c");
                $data["constract_drawing6c"]= $this->input->post("constract_drawing6c");
                $data["constract_drawing7c"]= $this->input->post("constract_drawing7c");
                $data["constract_drawing8c"]= $this->input->post("constract_drawing8c");
                $data["constract_drawing9c"]= $this->input->post("constract_drawing9c");
                $data["constract_drawing10c"]= $this->input->post("constract_drawing10c");


                $data["date_insert_drawing1c"] = date('Y-m-d');
                $data["date_insert_drawing2c"] = date('Y-m-d');
                $data["date_insert_drawing3c"] = date('Y-m-d');
                $data["date_insert_drawing4c"] = date('Y-m-d');
                $data["date_insert_drawing5c"] = date('Y-m-d');
                $data["date_insert_drawing6c"] = date('Y-m-d');
                $data["date_insert_drawing7c"] = date('Y-m-d');
                $data["date_insert_drawing8c"] = date('Y-m-d');
                $data["date_insert_drawing9c"] = date('Y-m-d');
                $data["date_insert_drawing10c"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1d")))
            {


                $data["check1d"] = $this->input->post("check1d");
                $data["desc_item1d"] = mb_convert_kana($this->input->post("desc_item1d"), "as");
                $data["check2d"] = $this->input->post("check2d");
                $data["desc_item2d"] = mb_convert_kana($this->input->post("desc_item2d"), "as");
                $data["check3d"] = $this->input->post("check3d");
                $data["desc_item3d"] = mb_convert_kana($this->input->post("desc_item3d"), "as");
                $data["check4d"] = $this->input->post("check4d");
                $data["desc_item4d"] = mb_convert_kana($this->input->post("desc_item4d"), "as");
                $data["check5d"] = $this->input->post("check5d");
                $data["desc_item5d"] = mb_convert_kana($this->input->post("desc_item5d"), "as");
                $data["check6d"] = $this->input->post("check6d");
                $data["desc_item6d"] = mb_convert_kana($this->input->post("desc_item6d"), "as");
                $data["check7d"] = $this->input->post("check7d");
                $data["desc_item7d"] = mb_convert_kana($this->input->post("desc_item7d"), "as");
                $data["check8d"] = $this->input->post("check8d");
                $data["desc_item8d"] = mb_convert_kana($this->input->post("desc_item8d"), "as");
                $data["check9d"] = $this->input->post("check9d");
                $data["desc_item9d"] = mb_convert_kana($this->input->post("desc_item9d"), "as");
                $data["check10d"] = $this->input->post("check10d");
                $data["desc_item10d"] = mb_convert_kana($this->input->post("desc_item10d"), "as");
                $data["category_name1d"] = "点検履歴";
                // $data["type_item"] = "admin";

                $data["constract_drawing1d"]= $this->input->post("constract_drawing1d");
                $data["constract_drawing2d"]= $this->input->post("constract_drawing2d");
                $data["constract_drawing3d"]= $this->input->post("constract_drawing3d");
                $data["constract_drawing4d"]= $this->input->post("constract_drawing4d");
                $data["constract_drawing5d"]= $this->input->post("constract_drawing5d");
                $data["constract_drawing6d"]= $this->input->post("constract_drawing6d");
                $data["constract_drawing7d"]= $this->input->post("constract_drawing7d");
                $data["constract_drawing8d"]= $this->input->post("constract_drawing8d");
                $data["constract_drawing9d"]= $this->input->post("constract_drawing9d");
                $data["constract_drawing10d"]= $this->input->post("constract_drawing10d");


                $data["date_insert_drawing1d"] = date('Y-m-d');
                $data["date_insert_drawing2d"] = date('Y-m-d');
                $data["date_insert_drawing3d"] = date('Y-m-d');
                $data["date_insert_drawing4d"] = date('Y-m-d');
                $data["date_insert_drawing5d"] = date('Y-m-d');
                $data["date_insert_drawing6d"] = date('Y-m-d');
                $data["date_insert_drawing7d"] = date('Y-m-d');
                $data["date_insert_drawing8d"] = date('Y-m-d');
                $data["date_insert_drawing9d"] = date('Y-m-d');
                $data["date_insert_drawing10d"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1e")))
            {


                $data["check1e"] = $this->input->post("check1e");
                $data["desc_item1e"] = mb_convert_kana($this->input->post("desc_item1e"), "as");
                $data["check2e"] = $this->input->post("check2e");
                $data["desc_item2e"] = mb_convert_kana($this->input->post("desc_item2e"), "as");
                $data["check3e"] = $this->input->post("check3e");
                $data["desc_item3e"] = mb_convert_kana($this->input->post("desc_item3e"), "as");
                $data["check4e"] = $this->input->post("check4e");
                $data["desc_item4e"] = mb_convert_kana($this->input->post("desc_item4e"), "as");
                $data["check5e"] = $this->input->post("check5e");
                $data["desc_item5e"] = mb_convert_kana($this->input->post("desc_item5e"), "as");
                $data["check6e"] = $this->input->post("check6e");
                $data["desc_item6e"] = mb_convert_kana($this->input->post("desc_item6e"), "as");
                $data["check7e"] = $this->input->post("check7e");
                $data["desc_item7e"] = mb_convert_kana($this->input->post("desc_item7e"), "as");
                $data["check8e"] = $this->input->post("check8e");
                $data["desc_item8e"] = mb_convert_kana($this->input->post("desc_item8e"), "as");
                $data["check9e"] = $this->input->post("check9e");
                $data["desc_item9e"] = mb_convert_kana($this->input->post("desc_item9e"), "as");
                $data["check10e"] = $this->input->post("check10e");
                $data["desc_item10e"] = mb_convert_kana($this->input->post("desc_item10e"), "as");
                $data["category_name1e"] = "三者引継ぎ合意書";
                // $data["type_item"] = "admin";

                $data["constract_drawing1e"]= $this->input->post("constract_drawing1e");
                $data["constract_drawing2e"]= $this->input->post("constract_drawing2e");
                $data["constract_drawing3e"]= $this->input->post("constract_drawing3e");
                $data["constract_drawing4e"]= $this->input->post("constract_drawing4e");
                $data["constract_drawing5e"]= $this->input->post("constract_drawing5e");
                $data["constract_drawing6e"]= $this->input->post("constract_drawing6e");
                $data["constract_drawing7e"]= $this->input->post("constract_drawing7e");
                $data["constract_drawing8e"]= $this->input->post("constract_drawing8e");
                $data["constract_drawing9e"]= $this->input->post("constract_drawing9e");
                $data["constract_drawing10e"]= $this->input->post("constract_drawing10e");


                $data["date_insert_drawing1e"] = date('Y-m-d');
                $data["date_insert_drawing2e"] = date('Y-m-d');
                $data["date_insert_drawing3e"] = date('Y-m-d');
                $data["date_insert_drawing4e"] = date('Y-m-d');
                $data["date_insert_drawing5e"] = date('Y-m-d');
                $data["date_insert_drawing6e"] = date('Y-m-d');
                $data["date_insert_drawing7e"] = date('Y-m-d');
                $data["date_insert_drawing8e"] = date('Y-m-d');
                $data["date_insert_drawing9e"] = date('Y-m-d');
                $data["date_insert_drawing10e"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1f")))
            {


                $data["check1f"] = $this->input->post("check1f");
                $data["desc_item1f"] = mb_convert_kana($this->input->post("desc_item1f"), "as");
                $data["check2f"] = $this->input->post("check2f");
                $data["desc_item2f"] = mb_convert_kana($this->input->post("desc_item2f"), "as");
                $data["check3f"] = $this->input->post("check3f");
                $data["desc_item3f"] = mb_convert_kana($this->input->post("desc_item3f"), "as");
                $data["check4f"] = $this->input->post("check4f");
                $data["desc_item4f"] = mb_convert_kana($this->input->post("desc_item4f"), "as");
                $data["check5f"] = $this->input->post("check5f");
                $data["desc_item5f"] = mb_convert_kana($this->input->post("desc_item5f"), "as");
                $data["check6f"] = $this->input->post("check6f");
                $data["desc_item6f"] = mb_convert_kana($this->input->post("desc_item6f"), "as");
                $data["check7f"] = $this->input->post("check7f");
                $data["desc_item7f"] = mb_convert_kana($this->input->post("desc_item7f"), "as");
                $data["check8f"] = $this->input->post("check8f");
                $data["desc_item8f"] = mb_convert_kana($this->input->post("desc_item8f"), "as");
                $data["check9f"] = $this->input->post("check9f");
                $data["desc_item9f"] = mb_convert_kana($this->input->post("desc_item9f"), "as");
                $data["check10f"] = $this->input->post("check10f");
                $data["desc_item10f"] = mb_convert_kana($this->input->post("desc_item10f"), "as");
                $data["category_name1f"] = "住宅仕様書・プレカット資料";
                // $data["type_item"] = "admin";

                $data["constract_drawing1f"]= $this->input->post("constract_drawing1f");
                $data["constract_drawing2f"]= $this->input->post("constract_drawing2f");
                $data["constract_drawing3f"]= $this->input->post("constract_drawing3f");
                $data["constract_drawing4f"]= $this->input->post("constract_drawing4f");
                $data["constract_drawing5f"]= $this->input->post("constract_drawing5f");
                $data["constract_drawing6f"]= $this->input->post("constract_drawing6f");
                $data["constract_drawing7f"]= $this->input->post("constract_drawing7f");
                $data["constract_drawing8f"]= $this->input->post("constract_drawing8f");
                $data["constract_drawing9f"]= $this->input->post("constract_drawing9f");
                $data["constract_drawing10f"]= $this->input->post("constract_drawing10f");


                $data["date_insert_drawing1f"] = date('Y-m-d');
                $data["date_insert_drawing2f"] = date('Y-m-d');
                $data["date_insert_drawing3f"] = date('Y-m-d');
                $data["date_insert_drawing4f"] = date('Y-m-d');
                $data["date_insert_drawing5f"] = date('Y-m-d');
                $data["date_insert_drawing6f"] = date('Y-m-d');
                $data["date_insert_drawing7f"] = date('Y-m-d');
                $data["date_insert_drawing8f"] = date('Y-m-d');
                $data["date_insert_drawing9f"] = date('Y-m-d');
                $data["date_insert_drawing10f"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1g")))
            {

                $data["check1g"] = $this->input->post("check1g");
                $data["desc_item1g"] = mb_convert_kana($this->input->post("desc_item1g"), "as");
                $data["check2g"] = $this->input->post("check2g");
                $data["desc_item2g"] = mb_convert_kana($this->input->post("desc_item2g"), "as");
                $data["check3g"] = $this->input->post("check3g");
                $data["desc_item3g"] = mb_convert_kana($this->input->post("desc_item3g"), "as");
                $data["check4g"] = $this->input->post("check4g");
                $data["desc_item4g"] = mb_convert_kana($this->input->post("desc_item4g"), "as");
                $data["check5g"] = $this->input->post("check5g");
                $data["desc_item5g"] = mb_convert_kana($this->input->post("desc_item5g"), "as");
                $data["check6g"] = $this->input->post("check6g");
                $data["desc_item6g"] = mb_convert_kana($this->input->post("desc_item6g"), "as");
                $data["check7g"] = $this->input->post("check7g");
                $data["desc_item7g"] = mb_convert_kana($this->input->post("desc_item7g"), "as");
                $data["check8g"] = $this->input->post("check8g");
                $data["desc_item8g"] = mb_convert_kana($this->input->post("desc_item8g"), "as");
                $data["check9g"] = $this->input->post("check9g");
                $data["desc_item9g"] = mb_convert_kana($this->input->post("desc_item9g"), "as");
                $data["check10g"] = $this->input->post("check10g");
                $data["desc_item10g"] = mb_convert_kana($this->input->post("desc_item10g"), "as");
                $data["category_name1g"] = "工事写真";
                // $data["type_item"] = "admin";

                $data["constract_drawing1g"]= $this->input->post("constract_drawing1g");
                $data["constract_drawing2g"]= $this->input->post("constract_drawing2g");
                $data["constract_drawing3g"]= $this->input->post("constract_drawing3g");
                $data["constract_drawing4g"]= $this->input->post("constract_drawing4g");
                $data["constract_drawing5g"]= $this->input->post("constract_drawing5g");
                $data["constract_drawing6g"]= $this->input->post("constract_drawing6g");
                $data["constract_drawing7g"]= $this->input->post("constract_drawing7g");
                $data["constract_drawing8g"]= $this->input->post("constract_drawing8g");
                $data["constract_drawing9g"]= $this->input->post("constract_drawing9g");
                $data["constract_drawing10g"]= $this->input->post("constract_drawing10g");


                $data["date_insert_drawing1g"] = date('Y-m-d');
                $data["date_insert_drawing2g"] = date('Y-m-d');
                $data["date_insert_drawing3g"] = date('Y-m-d');
                $data["date_insert_drawing4g"] = date('Y-m-d');
                $data["date_insert_drawing5g"] = date('Y-m-d');
                $data["date_insert_drawing6g"] = date('Y-m-d');
                $data["date_insert_drawing7g"] = date('Y-m-d');
                $data["date_insert_drawing8g"] = date('Y-m-d');
                $data["date_insert_drawing9g"] = date('Y-m-d');
                $data["date_insert_drawing10g"] = date('Y-m-d');
            }

            $data = $this->security->xss_clean($data);

            $this->load->model('customer_model', "customer");
            $data["data_employ"] = $this->customer->fetch_employees_names();
            $data["data"] = $this->customer->fetch_affiliate_names();

            $this->load->view('dashboard/constructor/preview_ins_v',$data);

            // echo json_encode($data, JSON_UNESCAPED_UNICODE);

        }
        else
        {
            $data["cust_code"] = mb_convert_kana($this->input->post("cust_code"), "as");
            $data["cust_name"] = mb_convert_kana($this->input->post("cust_name"), "as");
            $data["kinds"] = mb_convert_kana($this->input->post("kinds"), "as");
            $data["const_no"] = mb_convert_kana($this->input->post("const_no"), "as");
            $data["remarks"] = mb_convert_kana($this->input->post("remarks"), "as");
            $data["contract_date"] = $this->input->post("contract_date");
            $data["construction_start_date"] = $this->input->post("construction_start_date");
            $data["upper_building_date"] = $this->input->post("upper_building_date");
            $data["completion_date"] = $this->input->post("completion_date");
            $data["delivery_date"] = $this->input->post("delivery_date");
            $data["amount_money"] = mb_convert_kana($this->input->post("amount_money"), "as");
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff"), "as");
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction"), "as");
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator"), "as");
            $data["date_insert"] = date('Y-m-d H:i:s');
            $data["construct_stat"] = $this->input->post("construct_stat");
            
            // $config['upload_path'] = APPPATH.'../uploads/files'; 
            // $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            // $config['max_size'] = '500000'; // in KlioBytes
    
    
            if(!empty($this->input->post("constract_drawing1bluea")))
            {
    
                $data["check1bluea"] = $this->input->post("check1bluea");
                $data["desc_item1bluea"] = mb_convert_kana($this->input->post("desc_item1bluea"), "as");
                $data["check2bluea"] = $this->input->post("check2bluea");
                $data["desc_item2bluea"] = mb_convert_kana($this->input->post("desc_item2bluea"), "as");
                $data["check3bluea"] = $this->input->post("check3bluea");
                $data["desc_item3bluea"] = mb_convert_kana($this->input->post("desc_item3bluea"), "as");
                $data["check4bluea"] = $this->input->post("check4bluea");
                $data["desc_item4bluea"] = mb_convert_kana($this->input->post("desc_item4bluea"), "as");
                $data["check5bluea"] = $this->input->post("check5bluea");
                $data["desc_item5bluea"] = mb_convert_kana($this->input->post("desc_item5bluea"), "as");
                $data["check6bluea"] = $this->input->post("check6bluea");
                $data["desc_item6bluea"] = mb_convert_kana($this->input->post("desc_item6bluea"), "as");
                $data["check7bluea"] = $this->input->post("check7bluea");
                $data["desc_item7bluea"] = mb_convert_kana($this->input->post("desc_item7bluea"), "as");
                $data["check8bluea"] = $this->input->post("check8bluea");
                $data["desc_item8bluea"] = mb_convert_kana($this->input->post("desc_item8bluea"), "as");
                $data["check9bluea"] = $this->input->post("check9bluea");
                $data["desc_item9bluea"] = mb_convert_kana($this->input->post("desc_item9bluea"), "as");
                $data["check10bluea"] = $this->input->post("check10bluea");
                $data["desc_item10bluea"] = mb_convert_kana($this->input->post("desc_item10bluea"), "as");
                $data["category_name1bluea"] = "確認申請書一式";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1bluea")))
                {
                    $data["co_owner1bluea"] = $this->input->post("co_owner1bluea");
                }
                else
                {
                    $data["co_owner1bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluea")))
                {
                    $data["co_owner2bluea"] = $this->input->post("co_owner2bluea");
                }
                else
                {
                    $data["co_owner2bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluea")))
                {
                    $data["co_owner3bluea"] = $this->input->post("co_owner3bluea");
                }
                else
                {
                    $data["co_owner3bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluea")))
                {
                    $data["co_owner4bluea"] = $this->input->post("co_owner4bluea");
                }
                else
                {
                    $data["co_owner4bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluea")))
                {
                    $data["co_owner5bluea"] = $this->input->post("co_owner5bluea");
                }
                else
                {
                    $data["co_owner5bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluea")))
                {
                    $data["co_owner6bluea"] = $this->input->post("co_owner6bluea");
                }
                else
                {
                    $data["co_owner6bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluea")))
                {
                    $data["co_owner7bluea"] = $this->input->post("co_owner7bluea");
                }
                else
                {
                    $data["co_owner7bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluea")))
                {
                    $data["co_owner8bluea"] = $this->input->post("co_owner8bluea");
                }
                else
                {
                    $data["co_owner8bluea"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluea")))
                {
                    $data["co_owner9bluea"] = $this->input->post("co_owner9bluea");
                }
                else
                {
                    $data["co_owner9bluea"] = '';
                }
    
                if (!empty($this->input->post("co_owner10bluea")))
                {
                    $data["co_owner10bluea"] = $this->input->post("co_owner10bluea");
                }
                else
                {
                    $data["co_owner10bluea"] = '';
                }
    
    
    
                $data["constract_drawing1bluea"]= $this->input->post("constract_drawing1bluea");
                $data["constract_drawing2bluea"]= $this->input->post("constract_drawing2bluea");
                $data["constract_drawing3bluea"]= $this->input->post("constract_drawing3bluea");
                $data["constract_drawing4bluea"]= $this->input->post("constract_drawing4bluea");
                $data["constract_drawing5bluea"]= $this->input->post("constract_drawing5bluea");
                $data["constract_drawing6bluea"]= $this->input->post("constract_drawing6bluea");
                $data["constract_drawing7bluea"]= $this->input->post("constract_drawing7bluea");
                $data["constract_drawing8bluea"]= $this->input->post("constract_drawing8bluea");
                $data["constract_drawing9bluea"]= $this->input->post("constract_drawing9bluea");
                $data["constract_drawing10bluea"]= $this->input->post("constract_drawing10bluea");

    
                $data["date_insert_drawing1bluea"] = date('Y-m-d');
                $data["date_insert_drawing2bluea"] = date('Y-m-d');
                $data["date_insert_drawing3bluea"] = date('Y-m-d');
                $data["date_insert_drawing4bluea"] = date('Y-m-d');
                $data["date_insert_drawing5bluea"] = date('Y-m-d');
                $data["date_insert_drawing6bluea"] = date('Y-m-d');
                $data["date_insert_drawing7bluea"] = date('Y-m-d');
                $data["date_insert_drawing8bluea"] = date('Y-m-d');
                $data["date_insert_drawing9bluea"] = date('Y-m-d');
                $data["date_insert_drawing10bluea"] = date('Y-m-d');
    
            }
            
            if(!empty($this->input->post("constract_drawing1blueb")))
            {
    
    
                $data["check1blueb"] = $this->input->post("check1blueb");
                $data["desc_item1blueb"] = mb_convert_kana($this->input->post("desc_item1blueb"), "as");
                $data["check2blueb"] = $this->input->post("check2blueb");
                $data["desc_item2blueb"] = mb_convert_kana($this->input->post("desc_item2blueb"), "as");
                $data["check3blueb"] = $this->input->post("check3blueb");
                $data["desc_item3blueb"] = mb_convert_kana($this->input->post("desc_item3blueb"), "as");
                $data["check4blueb"] = $this->input->post("check4blueb");
                $data["desc_item4blueb"] = mb_convert_kana($this->input->post("desc_item4blueb"), "as");
                $data["check5blueb"] = $this->input->post("check5blueb");
                $data["desc_item5blueb"] = mb_convert_kana($this->input->post("desc_item5blueb"), "as");
                $data["check6blueb"] = $this->input->post("check6blueb");
                $data["desc_item6blueb"] = mb_convert_kana($this->input->post("desc_item6blueb"), "as");
                $data["check7blueb"] = $this->input->post("check7blueb");
                $data["desc_item7blueb"] = mb_convert_kana($this->input->post("desc_item7blueb"), "as");
                $data["check8blueb"] = $this->input->post("check8blueb");
                $data["desc_item8blueb"] = mb_convert_kana($this->input->post("desc_item8blueb"), "as");
                $data["check9blueb"] = $this->input->post("check9blueb");
                $data["desc_item9blueb"] = mb_convert_kana($this->input->post("desc_item9blueb"), "as");
                $data["check10blueb"] = $this->input->post("check10blueb");
                $data["desc_item10blueb"] = mb_convert_kana($this->input->post("desc_item10blueb"), "as");
                $data["category_name1blueb"] = "工事工程表";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1blueb")))
                {
                    $data["co_owner1blueb"] = $this->input->post("co_owner1blueb");
                }
                else
                {
                    $data["co_owner1blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blueb")))
                {
                    $data["co_owner2blueb"] = $this->input->post("co_owner2blueb");
                }
                else
                {
                    $data["co_owner2blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blueb")))
                {
                    $data["co_owner3blueb"] = $this->input->post("co_owner3blueb");
                }
                else
                {
                    $data["co_owner3blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blueb")))
                {
                    $data["co_owner4blueb"] = $this->input->post("co_owner4blueb");
                }
                else
                {
                    $data["co_owner4blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blueb")))
                {
                    $data["co_owner5blueb"] = $this->input->post("co_owner5blueb");
                }
                else
                {
                    $data["co_owner5blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blueb")))
                {
                    $data["co_owner6blueb"] = $this->input->post("co_owner6blueb");
                }
                else
                {
                    $data["co_owner6blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blueb")))
                {
                    $data["co_owner7blueb"] = $this->input->post("co_owner7blueb");
                }
                else
                {
                    $data["co_owner7blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blueb")))
                {
                    $data["co_owner8blueb"] = $this->input->post("co_owner8blueb");
                }
                else
                {
                    $data["co_owner8blueb"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blueb")))
                {
                    $data["co_owner9blueb"] = $this->input->post("co_owner9blueb");
                }
                else
                {
                    $data["co_owner9blueb"] = '';
                }
    
                if (!empty($this->input->post("co_owner10blueb")))
                {
                    $data["co_owner10blueb"] = $this->input->post("co_owner10blueb");
                }
                else
                {
                    $data["co_owner10blueb"] = '';
                }
    
         
                $data["constract_drawing1blueb"]= $this->input->post("constract_drawing1blueb");
                $data["constract_drawing2blueb"]= $this->input->post("constract_drawing2blueb");
                $data["constract_drawing3blueb"]= $this->input->post("constract_drawing3blueb");
                $data["constract_drawing4blueb"]= $this->input->post("constract_drawing4blueb");
                $data["constract_drawing5blueb"]= $this->input->post("constract_drawing5blueb");
                $data["constract_drawing6blueb"]= $this->input->post("constract_drawing6blueb");
                $data["constract_drawing7blueb"]= $this->input->post("constract_drawing7blueb");
                $data["constract_drawing8blueb"]= $this->input->post("constract_drawing8blueb");
                $data["constract_drawing9blueb"]= $this->input->post("constract_drawing9blueb");
                $data["constract_drawing10blueb"]= $this->input->post("constract_drawing10blueb");

    
                $data["date_insert_drawing1blueb"] = date('Y-m-d');
                $data["date_insert_drawing2blueb"] = date('Y-m-d');
                $data["date_insert_drawing3blueb"] = date('Y-m-d');
                $data["date_insert_drawing4blueb"] = date('Y-m-d');
                $data["date_insert_drawing5blueb"] = date('Y-m-d');
                $data["date_insert_drawing6blueb"] = date('Y-m-d');
                $data["date_insert_drawing7blueb"] = date('Y-m-d');
                $data["date_insert_drawing8blueb"] = date('Y-m-d');
                $data["date_insert_drawing9blueb"] = date('Y-m-d');
                $data["date_insert_drawing10blueb"] = date('Y-m-d');
            }
            
            if(!empty($this->input->post("constract_drawing1bluec")))
            {
       
    
                $data["check1bluec"] = $this->input->post("check1bluec");
                $data["desc_item1bluec"] = mb_convert_kana($this->input->post("desc_item1bluec"), "as");
                $data["check2bluec"] = $this->input->post("check2bluec");
                $data["desc_item2bluec"] = mb_convert_kana($this->input->post("desc_item2bluec"), "as");
                $data["check3bluec"] = $this->input->post("check3bluec");
                $data["desc_item3bluec"] = mb_convert_kana($this->input->post("desc_item3bluec"), "as");
                $data["check4bluec"] = $this->input->post("check4bluec");
                $data["desc_item4bluec"] = mb_convert_kana($this->input->post("desc_item4bluec"), "as");
                $data["check5bluec"] = $this->input->post("check5bluec");
                $data["desc_item5bluec"] = mb_convert_kana($this->input->post("desc_item5bluec"), "as");
                $data["check6bluec"] = $this->input->post("check6bluec");
                $data["desc_item6bluec"] = mb_convert_kana($this->input->post("desc_item6bluec"), "as");
                $data["check7bluec"] = $this->input->post("check7bluec");
                $data["desc_item7bluec"] = mb_convert_kana($this->input->post("desc_item7bluec"), "as");
                $data["check8bluec"] = $this->input->post("check8bluec");
                $data["desc_item8bluec"] = mb_convert_kana($this->input->post("desc_item8bluec"), "as");
                $data["check9bluec"] = $this->input->post("check9bluec");
                $data["desc_item9bluec"] = mb_convert_kana($this->input->post("desc_item9bluec"), "as");
                $data["check10bluec"] = $this->input->post("check10bluec");
                $data["desc_item10bluec"] = mb_convert_kana($this->input->post("desc_item10bluec"), "as");
                $data["category_name1bluec"] = "工事着工資料";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1bluec")))
                {
                    $data["co_owner1bluec"] = $this->input->post("co_owner1bluec");
                }
                else
                {
                    $data["co_owner1bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluec")))
                {
                    $data["co_owner2bluec"] = $this->input->post("co_owner2bluec");
                }
                else
                {
                    $data["co_owner2bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluec")))
                {
                    $data["co_owner3bluec"] = $this->input->post("co_owner3bluec");
                }
                else
                {
                    $data["co_owner3bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluec")))
                {
                    $data["co_owner4bluec"] = $this->input->post("co_owner4bluec");
                }
                else
                {
                    $data["co_owner4bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluec")))
                {
                    $data["co_owner5bluec"] = $this->input->post("co_owner5bluec");
                }
                else
                {
                    $data["co_owner5bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluec")))
                {
                    $data["co_owner6bluec"] = $this->input->post("co_owner6bluec");
                }
                else
                {
                    $data["co_owner6bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluec")))
                {
                    $data["co_owner7bluec"] = $this->input->post("co_owner7bluec");
                }
                else
                {
                    $data["co_owner7bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluec")))
                {
                    $data["co_owner8bluec"] = $this->input->post("co_owner8bluec");
                }
                else
                {
                    $data["co_owner8bluec"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluec")))
                {
                    $data["co_owner9bluec"] = $this->input->post("co_owner9bluec");
                }
                else
                {
                    $data["co_owner9bluec"] = '';
                }
    
                if (!empty($this->input->post("co_owner10bluec")))
                {
                    $data["co_owner10bluec"] = $this->input->post("co_owner10bluec");
                }
                else
                {
                    $data["co_owner10bluec"] = '';
                }
      
        
                $data["constract_drawing1bluec"]= $this->input->post("constract_drawing1bluec");
                $data["constract_drawing2bluec"]= $this->input->post("constract_drawing2bluec");
                $data["constract_drawing3bluec"]= $this->input->post("constract_drawing3bluec");
                $data["constract_drawing4bluec"]= $this->input->post("constract_drawing4bluec");
                $data["constract_drawing5bluec"]= $this->input->post("constract_drawing5bluec");
                $data["constract_drawing6bluec"]= $this->input->post("constract_drawing6bluec");
                $data["constract_drawing7bluec"]= $this->input->post("constract_drawing7bluec");
                $data["constract_drawing8bluec"]= $this->input->post("constract_drawing8bluec");
                $data["constract_drawing9bluec"]= $this->input->post("constract_drawing9bluec");
                $data["constract_drawing10bluec"]= $this->input->post("constract_drawing10bluec");

    
                $data["date_insert_drawing1bluec"] = date('Y-m-d');
                $data["date_insert_drawing2bluec"] = date('Y-m-d');
                $data["date_insert_drawing3bluec"] = date('Y-m-d');
                $data["date_insert_drawing4bluec"] = date('Y-m-d');
                $data["date_insert_drawing5bluec"] = date('Y-m-d');
                $data["date_insert_drawing6bluec"] = date('Y-m-d');
                $data["date_insert_drawing7bluec"] = date('Y-m-d');
                $data["date_insert_drawing8bluec"] = date('Y-m-d');
                $data["date_insert_drawing9bluec"] = date('Y-m-d');
                $data["date_insert_drawing10bluec"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1blued")))
            {
    
                $data["check1blued"] = $this->input->post("check1blued");
                $data["desc_item1blued"] = mb_convert_kana($this->input->post("desc_item1blued"), "as");
                $data["check2blued"] = $this->input->post("check2blued");
                $data["desc_item2blued"] = mb_convert_kana($this->input->post("desc_item2blued"), "as");
                $data["check3blued"] = $this->input->post("check3blued");
                $data["desc_item3blued"] = mb_convert_kana($this->input->post("desc_item3blued"), "as");
                $data["check4blued"] = $this->input->post("check4blued");
                $data["desc_item4blued"] = mb_convert_kana($this->input->post("desc_item4blued"), "as");
                $data["check5blued"] = $this->input->post("check5blued");
                $data["desc_item5blued"] = mb_convert_kana($this->input->post("desc_item5blued"), "as");
                $data["check6blued"] = $this->input->post("check6blued");
                $data["desc_item6blued"] = mb_convert_kana($this->input->post("desc_item6blued"), "as");
                $data["check7blued"] = $this->input->post("check7blued");
                $data["desc_item7blued"] = mb_convert_kana($this->input->post("desc_item7blued"), "as");
                $data["check8blued"] = $this->input->post("check8blued");
                $data["desc_item8blued"] = mb_convert_kana($this->input->post("desc_item8blued"), "as");
                $data["check9blued"] = $this->input->post("check9blued");
                $data["desc_item9blued"] = mb_convert_kana($this->input->post("desc_item9blued"), "as");
                $data["check10blued"] = $this->input->post("check10blued");
                $data["desc_item10blued"] = mb_convert_kana($this->input->post("desc_item10blued"), "as");
                $data["category_name1blued"] = "施工図面";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1blued")))
                {
                    $data["co_owner1blued"] = $this->input->post("co_owner1blued");
                }
                else
                {
                    $data["co_owner1blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blued")))
                {
                    $data["co_owner2blued"] = $this->input->post("co_owner2blued");
                }
                else
                {
                    $data["co_owner2blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blued")))
                {
                    $data["co_owner3blued"] = $this->input->post("co_owner3blued");
                }
                else
                {
                    $data["co_owner3blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blued")))
                {
                    $data["co_owner4blued"] = $this->input->post("co_owner4blued");
                }
                else
                {
                    $data["co_owner4blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blued")))
                {
                    $data["co_owner5blued"] = $this->input->post("co_owner5blued");
                }
                else
                {
                    $data["co_owner5blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blued")))
                {
                    $data["co_owner6blued"] = $this->input->post("co_owner6blued");
                }
                else
                {
                    $data["co_owner6blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blued")))
                {
                    $data["co_owner7blued"] = $this->input->post("co_owner7blued");
                }
                else
                {
                    $data["co_owner7blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blued")))
                {
                    $data["co_owner8blued"] = $this->input->post("co_owner8blued");
                }
                else
                {
                    $data["co_owner8blued"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blued")))
                {
                    $data["co_owner9blued"] = $this->input->post("co_owner9blued");
                }
                else
                {
                    $data["co_owner9blued"] = '';
                }
    
                if (!empty($this->input->post("co_owner10blued")))
                {
                    $data["co_owner10blued"] = $this->input->post("co_owner10blued");
                }
                else
                {
                    $data["co_owner10blued"] = '';
                }
    
    
                $data["constract_drawing1blued"]= $this->input->post("constract_drawing1blued");
                $data["constract_drawing2blued"]= $this->input->post("constract_drawing2blued");
                $data["constract_drawing3blued"]= $this->input->post("constract_drawing3blued");
                $data["constract_drawing4blued"]= $this->input->post("constract_drawing4blued");
                $data["constract_drawing5blued"]= $this->input->post("constract_drawing5blued");
                $data["constract_drawing6blued"]= $this->input->post("constract_drawing6blued");
                $data["constract_drawing7blued"]= $this->input->post("constract_drawing7blued");
                $data["constract_drawing8blued"]= $this->input->post("constract_drawing8blued");
                $data["constract_drawing9blued"]= $this->input->post("constract_drawing9blued");
                $data["constract_drawing10blued"]= $this->input->post("constract_drawing10blued");

    
                $data["date_insert_drawing1blued"] = date('Y-m-d');
                $data["date_insert_drawing2blued"] = date('Y-m-d');
                $data["date_insert_drawing3blued"] = date('Y-m-d');
                $data["date_insert_drawing4blued"] = date('Y-m-d');
                $data["date_insert_drawing5blued"] = date('Y-m-d');
                $data["date_insert_drawing6blued"] = date('Y-m-d');
                $data["date_insert_drawing7blued"] = date('Y-m-d');
                $data["date_insert_drawing8blued"] = date('Y-m-d');
                $data["date_insert_drawing9blued"] = date('Y-m-d');
                $data["date_insert_drawing10blued"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1bluee")))
            {
    
    
                $data["check1bluee"] = $this->input->post("check1bluee");
                $data["desc_item1bluee"] = mb_convert_kana($this->input->post("desc_item1bluee"), "as");
                $data["check2bluee"] = $this->input->post("check2bluee");
                $data["desc_item2bluee"] = mb_convert_kana($this->input->post("desc_item2bluee"), "as");
                $data["check3bluee"] = $this->input->post("check3bluee");
                $data["desc_item3bluee"] = mb_convert_kana($this->input->post("desc_item3bluee"), "as");
                $data["check4bluee"] = $this->input->post("check4bluee");
                $data["desc_item4bluee"] = mb_convert_kana($this->input->post("desc_item4bluee"), "as");
                $data["check5bluee"] = $this->input->post("check5bluee");
                $data["desc_item5bluee"] = mb_convert_kana($this->input->post("desc_item5bluee"), "as");
                $data["check6bluee"] = $this->input->post("check6bluee");
                $data["desc_item6bluee"] = mb_convert_kana($this->input->post("desc_item6bluee"), "as");
                $data["check7bluee"] = $this->input->post("check7bluee");
                $data["desc_item7bluee"] = mb_convert_kana($this->input->post("desc_item7bluee"), "as");
                $data["check8bluee"] = $this->input->post("check8bluee");
                $data["desc_item8bluee"] = mb_convert_kana($this->input->post("desc_item8bluee"), "as");
                $data["check9bluee"] = $this->input->post("check9bluee");
                $data["desc_item9bluee"] = mb_convert_kana($this->input->post("desc_item9bluee"), "as");
                $data["check10bluee"] = $this->input->post("check10bluee");
                $data["desc_item10bluee"] = mb_convert_kana($this->input->post("desc_item10bluee"), "as");
                $data["category_name1bluee"] = "変更図面";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1bluee")))
                {
                    $data["co_owner1bluee"] = $this->input->post("co_owner1bluee");
                }
                else
                {
                    $data["co_owner1bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluee")))
                {
                    $data["co_owner2bluee"] = $this->input->post("co_owner2bluee");
                }
                else
                {
                    $data["co_owner2bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluee")))
                {
                    $data["co_owner3bluee"] = $this->input->post("co_owner3bluee");
                }
                else
                {
                    $data["co_owner3bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluee")))
                {
                    $data["co_owner4bluee"] = $this->input->post("co_owner4bluee");
                }
                else
                {
                    $data["co_owner4bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluee")))
                {
                    $data["co_owner5bluee"] = $this->input->post("co_owner5bluee");
                }
                else
                {
                    $data["co_owner5bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluee")))
                {
                    $data["co_owner6bluee"] = $this->input->post("co_owner6bluee");
                }
                else
                {
                    $data["co_owner6bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluee")))
                {
                    $data["co_owner7bluee"] = $this->input->post("co_owner7bluee");
                }
                else
                {
                    $data["co_owner7bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluee")))
                {
                    $data["co_owner8bluee"] = $this->input->post("co_owner8bluee");
                }
                else
                {
                    $data["co_owner8bluee"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluee")))
                {
                    $data["co_owner9bluee"] = $this->input->post("co_owner9bluee");
                }
                else
                {
                    $data["co_owner9bluee"] = '';
                }
    
                if (!empty($this->input->post("co_owner10bluee")))
                {
                    $data["co_owner10bluee"] = $this->input->post("co_owner10bluee");
                }
                else
                {
                    $data["co_owner10bluee"] = '';
                }
    
    
                $data["constract_drawing1bluee"]= $this->input->post("constract_drawing1bluee");
                $data["constract_drawing2bluee"]= $this->input->post("constract_drawing2bluee");
                $data["constract_drawing3bluee"]= $this->input->post("constract_drawing3bluee");
                $data["constract_drawing4bluee"]= $this->input->post("constract_drawing4bluee");
                $data["constract_drawing5bluee"]= $this->input->post("constract_drawing5bluee");
                $data["constract_drawing6bluee"]= $this->input->post("constract_drawing6bluee");
                $data["constract_drawing7bluee"]= $this->input->post("constract_drawing7bluee");
                $data["constract_drawing8bluee"]= $this->input->post("constract_drawing8bluee");
                $data["constract_drawing9bluee"]= $this->input->post("constract_drawing9bluee");
                $data["constract_drawing10bluee"]= $this->input->post("constract_drawing10bluee");
 
    
                $data["date_insert_drawing1bluee"] = date('Y-m-d');
                $data["date_insert_drawing2bluee"] = date('Y-m-d');
                $data["date_insert_drawing3bluee"] = date('Y-m-d');
                $data["date_insert_drawing4bluee"] = date('Y-m-d');
                $data["date_insert_drawing5bluee"] = date('Y-m-d');
                $data["date_insert_drawing6bluee"] = date('Y-m-d');
                $data["date_insert_drawing7bluee"] = date('Y-m-d');
                $data["date_insert_drawing8bluee"] = date('Y-m-d');
                $data["date_insert_drawing9bluee"] = date('Y-m-d');
                $data["date_insert_drawing10bluee"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1bluef")))
            {
    
                $data["check1bluef"] = $this->input->post("check1bluef");
                $data["desc_item1bluef"] = mb_convert_kana($this->input->post("desc_item1bluef"), "as");
                $data["check2bluef"] = $this->input->post("check2bluef");
                $data["desc_item2bluef"] = mb_convert_kana($this->input->post("desc_item2bluef"), "as");
                $data["check3bluef"] = $this->input->post("check3bluef");
                $data["desc_item3bluef"] = mb_convert_kana($this->input->post("desc_item3bluef"), "as");
                $data["check4bluef"] = $this->input->post("check4bluef");
                $data["desc_item4bluef"] = mb_convert_kana($this->input->post("desc_item4bluef"), "as");
                $data["check5bluef"] = $this->input->post("check5bluef");
                $data["desc_item5bluef"] = mb_convert_kana($this->input->post("desc_item5bluef"), "as");
                $data["check6bluef"] = $this->input->post("check6bluef");
                $data["desc_item6bluef"] = mb_convert_kana($this->input->post("desc_item6bluef"), "as");
                $data["check7bluef"] = $this->input->post("check7bluef");
                $data["desc_item7bluef"] = mb_convert_kana($this->input->post("desc_item7bluef"), "as");
                $data["check8bluef"] = $this->input->post("check8bluef");
                $data["desc_item8bluef"] = mb_convert_kana($this->input->post("desc_item8bluef"), "as");
                $data["check9bluef"] = $this->input->post("check9bluef");
                $data["desc_item9bluef"] = mb_convert_kana($this->input->post("desc_item9bluef"), "as");
                $data["check10bluef"] = $this->input->post("check10bluef");
                $data["desc_item10bluef"] = mb_convert_kana($this->input->post("desc_item10bluef"), "as");
                $data["category_name1bluef"] = "設備プランシート";
                $data["type_item"] = "user";

    
                if (!empty($this->input->post("co_owner1bluef")))
                {
                    $data["co_owner1bluef"] = $this->input->post("co_owner1bluef");
                }
                else
                {
                    $data["co_owner1bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner2bluef")))
                {
                    $data["co_owner2bluef"] = $this->input->post("co_owner2bluef");
                }
                else
                {
                    $data["co_owner2bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner3bluef")))
                {
                    $data["co_owner3bluef"] = $this->input->post("co_owner3bluef");
                }
                else
                {
                    $data["co_owner3bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner4bluef")))
                {
                    $data["co_owner4bluef"] = $this->input->post("co_owner4bluef");
                }
                else
                {
                    $data["co_owner4bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner5bluef")))
                {
                    $data["co_owner5bluef"] = $this->input->post("co_owner5bluef");
                }
                else
                {
                    $data["co_owner5bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner6bluef")))
                {
                    $data["co_owner6bluef"] = $this->input->post("co_owner6bluef");
                }
                else
                {
                    $data["co_owner6bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner7bluef")))
                {
                    $data["co_owner7bluef"] = $this->input->post("co_owner7bluef");
                }
                else
                {
                    $data["co_owner7bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner8bluef")))
                {
                    $data["co_owner8bluef"] = $this->input->post("co_owner8bluef");
                }
                else
                {
                    $data["co_owner8bluef"] = '';
                }
                
                if (!empty($this->input->post("co_owner9bluef")))
                {
                    $data["co_owner9bluef"] = $this->input->post("co_owner9bluef");
                }
                else
                {
                    $data["co_owner9bluef"] = '';
                }
    
                if (!empty($this->input->post("co_owner10bluef")))
                {
                    $data["co_owner10bluef"] = $this->input->post("co_owner10bluef");
                }
                else
                {
                    $data["co_owner10bluef"] = '';
                }
    
    
                $data["constract_drawing1bluef"]= $this->input->post("constract_drawing1bluef");
                $data["constract_drawing2bluef"]= $this->input->post("constract_drawing2bluef");
                $data["constract_drawing3bluef"]= $this->input->post("constract_drawing3bluef");
                $data["constract_drawing4bluef"]= $this->input->post("constract_drawing4bluef");
                $data["constract_drawing5bluef"]= $this->input->post("constract_drawing5bluef");
                $data["constract_drawing6bluef"]= $this->input->post("constract_drawing6bluef");
                $data["constract_drawing7bluef"]= $this->input->post("constract_drawing7bluef");
                $data["constract_drawing8bluef"]= $this->input->post("constract_drawing8bluef");
                $data["constract_drawing9bluef"]= $this->input->post("constract_drawing9bluef");
                $data["constract_drawing10bluef"]= $this->input->post("constract_drawing10bluef");

    
                $data["date_insert_drawing1bluef"] = date('Y-m-d');
                $data["date_insert_drawing2bluef"] = date('Y-m-d');
                $data["date_insert_drawing3bluef"] = date('Y-m-d');
                $data["date_insert_drawing4bluef"] = date('Y-m-d');
                $data["date_insert_drawing5bluef"] = date('Y-m-d');
                $data["date_insert_drawing6bluef"] = date('Y-m-d');
                $data["date_insert_drawing7bluef"] = date('Y-m-d');
                $data["date_insert_drawing8bluef"] = date('Y-m-d');
                $data["date_insert_drawing9bluef"] = date('Y-m-d');
                $data["date_insert_drawing10bluef"] = date('Y-m-d');
            }

            if(!empty($this->input->post("constract_drawing1blueg")))
            {
    
                $data["check1blueg"] = $this->input->post("check1blueg");
                $data["desc_item1blueg"] = mb_convert_kana($this->input->post("desc_item1blueg"), "as");
                $data["check2blueg"] = $this->input->post("check2blueg");
                $data["desc_item2blueg"] = mb_convert_kana($this->input->post("desc_item2blueg"), "as");
                $data["check3blueg"] = $this->input->post("check3blueg");
                $data["desc_item3blueg"] = mb_convert_kana($this->input->post("desc_item3blueg"), "as");
                $data["check4blueg"] = $this->input->post("check4blueg");
                $data["desc_item4blueg"] = mb_convert_kana($this->input->post("desc_item4blueg"), "as");
                $data["check5blueg"] = $this->input->post("check5blueg");
                $data["desc_item5blueg"] = mb_convert_kana($this->input->post("desc_item5blueg"), "as");
                $data["check6blueg"] = $this->input->post("check6blueg");
                $data["desc_item6blueg"] = mb_convert_kana($this->input->post("desc_item6blueg"), "as");
                $data["check7blueg"] = $this->input->post("check7blueg");
                $data["desc_item7blueg"] = mb_convert_kana($this->input->post("desc_item7blueg"), "as");
                $data["check8blueg"] = $this->input->post("check8blueg");
                $data["desc_item8blueg"] = mb_convert_kana($this->input->post("desc_item8blueg"), "as");
                $data["check9blueg"] = $this->input->post("check9blueg");
                $data["desc_item9blueg"] = mb_convert_kana($this->input->post("desc_item9blueg"), "as");
                $data["check10blueg"] = $this->input->post("check10blueg");
                $data["desc_item10blueg"] = mb_convert_kana($this->input->post("desc_item10blueg"), "as");
                $data["category_name1blueg"] = "その他";
                $data["type_item"] = "user";
    
                if (!empty($this->input->post("co_owner1blueg")))
                {
                    $data["co_owner1blueg"] = $this->input->post("co_owner1blueg");
                }
                else
                {
                    $data["co_owner1blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner2blueg")))
                {
                    $data["co_owner2blueg"] = $this->input->post("co_owner2blueg");
                }
                else
                {
                    $data["co_owner2blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner3blueg")))
                {
                    $data["co_owner3blueg"] = $this->input->post("co_owner3blueg");
                }
                else
                {
                    $data["co_owner3blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner4blueg")))
                {
                    $data["co_owner4blueg"] = $this->input->post("co_owner4blueg");
                }
                else
                {
                    $data["co_owner4blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner5blueg")))
                {
                    $data["co_owner5blueg"] = $this->input->post("co_owner5blueg");
                }
                else
                {
                    $data["co_owner5blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner6blueg")))
                {
                    $data["co_owner6blueg"] = $this->input->post("co_owner6blueg");
                }
                else
                {
                    $data["co_owner6blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner7blueg")))
                {
                    $data["co_owner7blueg"] = $this->input->post("co_owner7blueg");
                }
                else
                {
                    $data["co_owner7blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner8blueg")))
                {
                    $data["co_owner8blueg"] = $this->input->post("co_owner8blueg");
                }
                else
                {
                    $data["co_owner8blueg"] = '';
                }
                
                if (!empty($this->input->post("co_owner9blueg")))
                {
                    $data["co_owner9blueg"] = $this->input->post("co_owner9blueg");
                }
                else
                {
                    $data["co_owner9blueg"] = '';
                }
    
                if (!empty($this->input->post("co_owner10blueg")))
                {
                    $data["co_owner10blueg"] = $this->input->post("co_owner10blueg");
                }
                else
                {
                    $data["co_owner10blueg"] = '';
                }
    
                $data["constract_drawing1blueg"]= $this->input->post("constract_drawing1blueg");
                $data["constract_drawing2blueg"]= $this->input->post("constract_drawing2blueg");
                $data["constract_drawing3blueg"]= $this->input->post("constract_drawing3blueg");
                $data["constract_drawing4blueg"]= $this->input->post("constract_drawing4blueg");
                $data["constract_drawing5blueg"]= $this->input->post("constract_drawing5blueg");
                $data["constract_drawing6blueg"]= $this->input->post("constract_drawing6blueg");
                $data["constract_drawing7blueg"]= $this->input->post("constract_drawing7blueg");
                $data["constract_drawing8blueg"]= $this->input->post("constract_drawing8blueg");
                $data["constract_drawing9blueg"]= $this->input->post("constract_drawing9blueg");
                $data["constract_drawing10blueg"]= $this->input->post("constract_drawing10blueg");

    
                $data["date_insert_drawing1blueg"] = date('Y-m-d');
                $data["date_insert_drawing2blueg"] = date('Y-m-d');
                $data["date_insert_drawing3blueg"] = date('Y-m-d');
                $data["date_insert_drawing4blueg"] = date('Y-m-d');
                $data["date_insert_drawing5blueg"] = date('Y-m-d');
                $data["date_insert_drawing6blueg"] = date('Y-m-d');
                $data["date_insert_drawing7blueg"] = date('Y-m-d');
                $data["date_insert_drawing8blueg"] = date('Y-m-d');
                $data["date_insert_drawing9blueg"] = date('Y-m-d');
                $data["date_insert_drawing10blueg"] = date('Y-m-d');
            }
    
            //admin//
    
            if(!empty($this->input->post("constract_drawing1")))
            {
    
                $data["check1"] = $this->input->post("check1");
                $data["desc_item1"] = mb_convert_kana($this->input->post("desc_item1"), "as");
                $data["check2"] = $this->input->post("check2");
                $data["desc_item2"] = mb_convert_kana($this->input->post("desc_item2"), "as");
                $data["check3"] = $this->input->post("check3");
                $data["desc_item3"] = mb_convert_kana($this->input->post("desc_item3"), "as");
                $data["check4"] = $this->input->post("check4");
                $data["desc_item4"] = mb_convert_kana($this->input->post("desc_item4"), "as");
                $data["check5"] = $this->input->post("check5");
                $data["desc_item5"] = mb_convert_kana($this->input->post("desc_item5"), "as");
                $data["check6"] = $this->input->post("check6");
                $data["desc_item6"] = mb_convert_kana($this->input->post("desc_item6"), "as");
                $data["check7"] = $this->input->post("check7");
                $data["desc_item7"] = mb_convert_kana($this->input->post("desc_item7"), "as");
                $data["check8"] = $this->input->post("check8");
                $data["desc_item8"] = mb_convert_kana($this->input->post("desc_item8"), "as");
                $data["check9"] = $this->input->post("check9");
                $data["desc_item9"] = mb_convert_kana($this->input->post("desc_item9"), "as");
                $data["check10"] = $this->input->post("check10");
                $data["desc_item10"] = mb_convert_kana($this->input->post("desc_item10"), "as");
                $data["category_name"] = "請負契約書・土地契約書";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1"]= $this->input->post("constract_drawing1");
                $data["constract_drawing2"]= $this->input->post("constract_drawing2");
                $data["constract_drawing3"]= $this->input->post("constract_drawing3");
                $data["constract_drawing4"]= $this->input->post("constract_drawing4");
                $data["constract_drawing5"]= $this->input->post("constract_drawing5");
                $data["constract_drawing6"]= $this->input->post("constract_drawing6");
                $data["constract_drawing7"]= $this->input->post("constract_drawing7");
                $data["constract_drawing8"]= $this->input->post("constract_drawing8");
                $data["constract_drawing9"]= $this->input->post("constract_drawing9");
                $data["constract_drawing10"]= $this->input->post("constract_drawing10");

    
                $data["date_insert_drawing1"] = date('Y-m-d');
                $data["date_insert_drawing2"] = date('Y-m-d');
                $data["date_insert_drawing3"] = date('Y-m-d');
                $data["date_insert_drawing4"] = date('Y-m-d');
                $data["date_insert_drawing5"] = date('Y-m-d');
                $data["date_insert_drawing6"] = date('Y-m-d');
                $data["date_insert_drawing7"] = date('Y-m-d');
                $data["date_insert_drawing8"] = date('Y-m-d');
                $data["date_insert_drawing9"] = date('Y-m-d');
                $data["date_insert_drawing10"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1b")))
            {
    
                $data["check1b"] = $this->input->post("check1b");
                $data["desc_item1b"] = mb_convert_kana($this->input->post("desc_item1b"), "as");
                $data["check2b"] = $this->input->post("check2b");
                $data["desc_item2b"] = mb_convert_kana($this->input->post("desc_item2b"), "as");
                $data["check3b"] = $this->input->post("check3b");
                $data["desc_item3b"] = mb_convert_kana($this->input->post("desc_item3b"), "as");
                $data["check4b"] = $this->input->post("check4b");
                $data["desc_item4b"] = mb_convert_kana($this->input->post("desc_item4b"), "as");
                $data["check5b"] = $this->input->post("check5b");
                $data["desc_item5b"] = mb_convert_kana($this->input->post("desc_item5b"), "as");
                $data["check6b"] = $this->input->post("check6b");
                $data["desc_item6b"] = mb_convert_kana($this->input->post("desc_item6b"), "as");
                $data["check7b"] = $this->input->post("check7b");
                $data["desc_item7b"] = mb_convert_kana($this->input->post("desc_item7b"), "as");
                $data["check8b"] = $this->input->post("check8b");
                $data["desc_item8b"] = mb_convert_kana($this->input->post("desc_item8b"), "as");
                $data["check9b"] = $this->input->post("check9b");
                $data["desc_item9b"] = mb_convert_kana($this->input->post("desc_item9b"), "as");
                $data["check10b"] = $this->input->post("check10b");
                $data["desc_item10b"] = mb_convert_kana($this->input->post("desc_item10b"), "as");
                $data["category_name1b"] = "長期優良住宅認定書・性能評価書";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1b"]= $this->input->post("constract_drawing1b");
                $data["constract_drawing2b"]= $this->input->post("constract_drawing2b");
                $data["constract_drawing3b"]= $this->input->post("constract_drawing3b");
                $data["constract_drawing4b"]= $this->input->post("constract_drawing4b");
                $data["constract_drawing5b"]= $this->input->post("constract_drawing5b");
                $data["constract_drawing6b"]= $this->input->post("constract_drawing6b");
                $data["constract_drawing7b"]= $this->input->post("constract_drawing7b");
                $data["constract_drawing8b"]= $this->input->post("constract_drawing8b");
                $data["constract_drawing9b"]= $this->input->post("constract_drawing9b");
                $data["constract_drawing10b"]= $this->input->post("constract_drawing10b");

    
                $data["date_insert_drawing1b"] = date('Y-m-d');
                $data["date_insert_drawing2b"] = date('Y-m-d');
                $data["date_insert_drawing3b"] = date('Y-m-d');
                $data["date_insert_drawing4b"] = date('Y-m-d');
                $data["date_insert_drawing5b"] = date('Y-m-d');
                $data["date_insert_drawing6b"] = date('Y-m-d');
                $data["date_insert_drawing7b"] = date('Y-m-d');
                $data["date_insert_drawing8b"] = date('Y-m-d');
                $data["date_insert_drawing9b"] = date('Y-m-d');
                $data["date_insert_drawing10b"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1c")))
            {
    
                $data["check1c"] = $this->input->post("check1c");
                $data["desc_item1c"] = mb_convert_kana($this->input->post("desc_item1c"), "as");
                $data["check2c"] = $this->input->post("check2c");
                $data["desc_item2c"] = mb_convert_kana($this->input->post("desc_item2c"), "as");
                $data["check3c"] = $this->input->post("check3c");
                $data["desc_item3c"] = mb_convert_kana($this->input->post("desc_item3c"), "as");
                $data["check4c"] = $this->input->post("check4c");
                $data["desc_item4c"] = mb_convert_kana($this->input->post("desc_item4c"), "as");
                $data["check5c"] = $this->input->post("check5c");
                $data["desc_item5c"] = mb_convert_kana($this->input->post("desc_item5c"), "as");
                $data["check6c"] = $this->input->post("check6c");
                $data["desc_item6c"] = mb_convert_kana($this->input->post("desc_item6c"), "as");
                $data["check7c"] = $this->input->post("check7c");
                $data["desc_item7c"] = mb_convert_kana($this->input->post("desc_item7c"), "as");
                $data["check8c"] = $this->input->post("check8c");
                $data["desc_item8c"] = mb_convert_kana($this->input->post("desc_item8c"), "as");
                $data["check9c"] = $this->input->post("check9c");
                $data["desc_item9c"] = mb_convert_kana($this->input->post("desc_item9c"), "as");
                $data["check10c"] = $this->input->post("check10c");
                $data["desc_item10c"] = mb_convert_kana($this->input->post("desc_item10c"), "as");
                $data["category_name1c"] = "地盤調査・地盤改良";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1c"]= $this->input->post("constract_drawing1c");
                $data["constract_drawing2c"]= $this->input->post("constract_drawing2c");
                $data["constract_drawing3c"]= $this->input->post("constract_drawing3c");
                $data["constract_drawing4c"]= $this->input->post("constract_drawing4c");
                $data["constract_drawing5c"]= $this->input->post("constract_drawing5c");
                $data["constract_drawing6c"]= $this->input->post("constract_drawing6c");
                $data["constract_drawing7c"]= $this->input->post("constract_drawing7c");
                $data["constract_drawing8c"]= $this->input->post("constract_drawing8c");
                $data["constract_drawing9c"]= $this->input->post("constract_drawing9c");
                $data["constract_drawing10c"]= $this->input->post("constract_drawing10c");

    
                $data["date_insert_drawing1c"] = date('Y-m-d');
                $data["date_insert_drawing2c"] = date('Y-m-d');
                $data["date_insert_drawing3c"] = date('Y-m-d');
                $data["date_insert_drawing4c"] = date('Y-m-d');
                $data["date_insert_drawing5c"] = date('Y-m-d');
                $data["date_insert_drawing6c"] = date('Y-m-d');
                $data["date_insert_drawing7c"] = date('Y-m-d');
                $data["date_insert_drawing8c"] = date('Y-m-d');
                $data["date_insert_drawing9c"] = date('Y-m-d');
                $data["date_insert_drawing10c"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1d")))
            {
    
                $data["check1d"] = $this->input->post("check1d");
                $data["desc_item1d"] = mb_convert_kana($this->input->post("desc_item1d"), "as");
                $data["check2d"] = $this->input->post("check2d");
                $data["desc_item2d"] = mb_convert_kana($this->input->post("desc_item2d"), "as");
                $data["check3d"] = $this->input->post("check3d");
                $data["desc_item3d"] = mb_convert_kana($this->input->post("desc_item3d"), "as");
                $data["check4d"] = $this->input->post("check4d");
                $data["desc_item4d"] = mb_convert_kana($this->input->post("desc_item4d"), "as");
                $data["check5d"] = $this->input->post("check5d");
                $data["desc_item5d"] = mb_convert_kana($this->input->post("desc_item5d"), "as");
                $data["check6d"] = $this->input->post("check6d");
                $data["desc_item6d"] = mb_convert_kana($this->input->post("desc_item6d"), "as");
                $data["check7d"] = $this->input->post("check7d");
                $data["desc_item7d"] = mb_convert_kana($this->input->post("desc_item7d"), "as");
                $data["check8d"] = $this->input->post("check8d");
                $data["desc_item8d"] = mb_convert_kana($this->input->post("desc_item8d"), "as");
                $data["check9d"] = $this->input->post("check9d");
                $data["desc_item9d"] = mb_convert_kana($this->input->post("desc_item9d"), "as");
                $data["check10d"] = $this->input->post("check10d");
                $data["desc_item10d"] = mb_convert_kana($this->input->post("desc_item10d"), "as");
                $data["category_name1d"] = "点検履歴";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1d"]= $this->input->post("constract_drawing1d");
                $data["constract_drawing2d"]= $this->input->post("constract_drawing2d");
                $data["constract_drawing3d"]= $this->input->post("constract_drawing3d");
                $data["constract_drawing4d"]= $this->input->post("constract_drawing4d");
                $data["constract_drawing5d"]= $this->input->post("constract_drawing5d");
                $data["constract_drawing6d"]= $this->input->post("constract_drawing6d");
                $data["constract_drawing7d"]= $this->input->post("constract_drawing7d");
                $data["constract_drawing8d"]= $this->input->post("constract_drawing8d");
                $data["constract_drawing9d"]= $this->input->post("constract_drawing9d");
                $data["constract_drawing10d"]= $this->input->post("constract_drawing10d");

    
                $data["date_insert_drawing1d"] = date('Y-m-d');
                $data["date_insert_drawing2d"] = date('Y-m-d');
                $data["date_insert_drawing3d"] = date('Y-m-d');
                $data["date_insert_drawing4d"] = date('Y-m-d');
                $data["date_insert_drawing5d"] = date('Y-m-d');
                $data["date_insert_drawing6d"] = date('Y-m-d');
                $data["date_insert_drawing7d"] = date('Y-m-d');
                $data["date_insert_drawing8d"] = date('Y-m-d');
                $data["date_insert_drawing9d"] = date('Y-m-d');
                $data["date_insert_drawing10d"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1e")))
            {
    
    
                $data["check1e"] = $this->input->post("check1e");
                $data["desc_item1e"] = mb_convert_kana($this->input->post("desc_item1e"), "as");
                $data["check2e"] = $this->input->post("check2e");
                $data["desc_item2e"] = mb_convert_kana($this->input->post("desc_item2e"), "as");
                $data["check3e"] = $this->input->post("check3e");
                $data["desc_item3e"] = mb_convert_kana($this->input->post("desc_item3e"), "as");
                $data["check4e"] = $this->input->post("check4e");
                $data["desc_item4e"] = mb_convert_kana($this->input->post("desc_item4e"), "as");
                $data["check5e"] = $this->input->post("check5e");
                $data["desc_item5e"] = mb_convert_kana($this->input->post("desc_item5e"), "as");
                $data["check6e"] = $this->input->post("check6e");
                $data["desc_item6e"] = mb_convert_kana($this->input->post("desc_item6e"), "as");
                $data["check7e"] = $this->input->post("check7e");
                $data["desc_item7e"] = mb_convert_kana($this->input->post("desc_item7e"), "as");
                $data["check8e"] = $this->input->post("check8e");
                $data["desc_item8e"] = mb_convert_kana($this->input->post("desc_item8e"), "as");
                $data["check9e"] = $this->input->post("check9e");
                $data["desc_item9e"] = mb_convert_kana($this->input->post("desc_item9e"), "as");
                $data["check10e"] = $this->input->post("check10e");
                $data["desc_item10e"] = mb_convert_kana($this->input->post("desc_item10e"), "as");
                $data["category_name1e"] = "三者引継ぎ合意書";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1e"]= $this->input->post("constract_drawing1e");
                $data["constract_drawing2e"]= $this->input->post("constract_drawing2e");
                $data["constract_drawing3e"]= $this->input->post("constract_drawing3e");
                $data["constract_drawing4e"]= $this->input->post("constract_drawing4e");
                $data["constract_drawing5e"]= $this->input->post("constract_drawing5e");
                $data["constract_drawing6e"]= $this->input->post("constract_drawing6e");
                $data["constract_drawing7e"]= $this->input->post("constract_drawing7e");
                $data["constract_drawing8e"]= $this->input->post("constract_drawing8e");
                $data["constract_drawing9e"]= $this->input->post("constract_drawing9e");
                $data["constract_drawing10e"]= $this->input->post("constract_drawing10e");

    
                $data["date_insert_drawing1e"] = date('Y-m-d');
                $data["date_insert_drawing2e"] = date('Y-m-d');
                $data["date_insert_drawing3e"] = date('Y-m-d');
                $data["date_insert_drawing4e"] = date('Y-m-d');
                $data["date_insert_drawing5e"] = date('Y-m-d');
                $data["date_insert_drawing6e"] = date('Y-m-d');
                $data["date_insert_drawing7e"] = date('Y-m-d');
                $data["date_insert_drawing8e"] = date('Y-m-d');
                $data["date_insert_drawing9e"] = date('Y-m-d');
                $data["date_insert_drawing10e"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1f")))
            {
    
                $data["check1f"] = $this->input->post("check1f");
                $data["desc_item1f"] = mb_convert_kana($this->input->post("desc_item1f"), "as");
                $data["check2f"] = $this->input->post("check2f");
                $data["desc_item2f"] = mb_convert_kana($this->input->post("desc_item2f"), "as");
                $data["check3f"] = $this->input->post("check3f");
                $data["desc_item3f"] = mb_convert_kana($this->input->post("desc_item3f"), "as");
                $data["check4f"] = $this->input->post("check4f");
                $data["desc_item4f"] = mb_convert_kana($this->input->post("desc_item4f"), "as");
                $data["check5f"] = $this->input->post("check5f");
                $data["desc_item5f"] = mb_convert_kana($this->input->post("desc_item5f"), "as");
                $data["check6f"] = $this->input->post("check6f");
                $data["desc_item6f"] = mb_convert_kana($this->input->post("desc_item6f"), "as");
                $data["check7f"] = $this->input->post("check7f");
                $data["desc_item7f"] = mb_convert_kana($this->input->post("desc_item7f"), "as");
                $data["check8f"] = $this->input->post("check8f");
                $data["desc_item8f"] = mb_convert_kana($this->input->post("desc_item8f"), "as");
                $data["check9f"] = $this->input->post("check9f");
                $data["desc_item9f"] = mb_convert_kana($this->input->post("desc_item9f"), "as");
                $data["check10f"] = $this->input->post("check10f");
                $data["desc_item10f"] = mb_convert_kana($this->input->post("desc_item10f"), "as");
                $data["category_name1f"] = "住宅仕様書・プレカット資料";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1f"]= $this->input->post("constract_drawing1f");
                $data["constract_drawing2f"]= $this->input->post("constract_drawing2f");
                $data["constract_drawing3f"]= $this->input->post("constract_drawing3f");
                $data["constract_drawing4f"]= $this->input->post("constract_drawing4f");
                $data["constract_drawing5f"]= $this->input->post("constract_drawing5f");
                $data["constract_drawing6f"]= $this->input->post("constract_drawing6f");
                $data["constract_drawing7f"]= $this->input->post("constract_drawing7f");
                $data["constract_drawing8f"]= $this->input->post("constract_drawing8f");
                $data["constract_drawing9f"]= $this->input->post("constract_drawing9f");
                $data["constract_drawing10f"]= $this->input->post("constract_drawing10f");

    
                $data["date_insert_drawing1f"] = date('Y-m-d');
                $data["date_insert_drawing2f"] = date('Y-m-d');
                $data["date_insert_drawing3f"] = date('Y-m-d');
                $data["date_insert_drawing4f"] = date('Y-m-d');
                $data["date_insert_drawing5f"] = date('Y-m-d');
                $data["date_insert_drawing6f"] = date('Y-m-d');
                $data["date_insert_drawing7f"] = date('Y-m-d');
                $data["date_insert_drawing8f"] = date('Y-m-d');
                $data["date_insert_drawing9f"] = date('Y-m-d');
                $data["date_insert_drawing10f"] = date('Y-m-d');
            }
    
            if(!empty($this->input->post("constract_drawing1g")))
            {
    
                $data["check1g"] = $this->input->post("check1g");
                $data["desc_item1g"] = mb_convert_kana($this->input->post("desc_item1g"), "as");
                $data["check2g"] = $this->input->post("check2g");
                $data["desc_item2g"] = mb_convert_kana($this->input->post("desc_item2g"), "as");
                $data["check3g"] = $this->input->post("check3g");
                $data["desc_item3g"] = mb_convert_kana($this->input->post("desc_item3g"), "as");
                $data["check4g"] = $this->input->post("check4g");
                $data["desc_item4g"] = mb_convert_kana($this->input->post("desc_item4g"), "as");
                $data["check5g"] = $this->input->post("check5g");
                $data["desc_item5g"] = mb_convert_kana($this->input->post("desc_item5g"), "as");
                $data["check6g"] = $this->input->post("check6g");
                $data["desc_item6g"] = mb_convert_kana($this->input->post("desc_item6g"), "as");
                $data["check7g"] = $this->input->post("check7g");
                $data["desc_item7g"] = mb_convert_kana($this->input->post("desc_item7g"), "as");
                $data["check8g"] = $this->input->post("check8g");
                $data["desc_item8g"] = mb_convert_kana($this->input->post("desc_item8g"), "as");
                $data["check9g"] = $this->input->post("check9g");
                $data["desc_item9g"] = mb_convert_kana($this->input->post("desc_item9g"), "as");
                $data["check10g"] = $this->input->post("check10g");
                $data["desc_item10g"] = mb_convert_kana($this->input->post("desc_item10g"), "as");
                $data["category_name1g"] = "工事写真";
                // $data["type_item"] = "admin";
    
                $data["constract_drawing1g"]= $this->input->post("constract_drawing1g");
                $data["constract_drawing2g"]= $this->input->post("constract_drawing2g");
                $data["constract_drawing3g"]= $this->input->post("constract_drawing3g");
                $data["constract_drawing4g"]= $this->input->post("constract_drawing4g");
                $data["constract_drawing5g"]= $this->input->post("constract_drawing5g");
                $data["constract_drawing6g"]= $this->input->post("constract_drawing6g");
                $data["constract_drawing7g"]= $this->input->post("constract_drawing7g");
                $data["constract_drawing8g"]= $this->input->post("constract_drawing8g");
                $data["constract_drawing9g"]= $this->input->post("constract_drawing9g");
                $data["constract_drawing10g"]= $this->input->post("constract_drawing10g");

    
                $data["date_insert_drawing1g"] = date('Y-m-d');
                $data["date_insert_drawing2g"] = date('Y-m-d');
                $data["date_insert_drawing3g"] = date('Y-m-d');
                $data["date_insert_drawing4g"] = date('Y-m-d');
                $data["date_insert_drawing5g"] = date('Y-m-d');
                $data["date_insert_drawing6g"] = date('Y-m-d');
                $data["date_insert_drawing7g"] = date('Y-m-d');
                $data["date_insert_drawing8g"] = date('Y-m-d');
                $data["date_insert_drawing9g"] = date('Y-m-d');
                $data["date_insert_drawing10g"] = date('Y-m-d');
            }

            $data = $this->security->xss_clean($data);

            // echo json_encode($data, JSON_UNESCAPED_UNICODE);die();

            $this->load->model('customer_model',"customer");
            $insert = $this->customer->insert_constructor_new($data);
            if($insert)
            {
                $this->record_action("add_constructor","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/constructor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'ファイル種が正しくありません');
                $this->load->view('dashboard/constructor/insert_v',$data);
            }
        }
    }


    //-------------------------------------------------------------------------//


    public function edit_customer($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }


    //---------------------------------------NEW-----------------------------------------//


    public function edit_customers($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_customers_new($id)[0];
        $data["dataks"] = $this->customer->fetch_employees_names();
        $data["for_bank"] = $this->customer->fetch_finances();
        $this->load->view('dashboard/customers/edit_v',$data);
    }

    public function edit_client($id)
    {
        $this->record_action("edit_client","view");
        $this->load->model('customer_model', "customer");
        $data["dataks"] = $this->customer->fetch_employees_names();
        $data["data"] = $this->customer->fetch_one_client($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }

    public function edit_employees($id)
    {
        $this->record_action("edit_employees","view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["datar"] = $this->user->fetch_one_username($id)[0];
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $this->load->view('dashboard/employees/edit_v',$data);
    }


    public function edit_contractor($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];
        $data["weeee"] = $this->customer->fetch_affiliate_names();
        $data["data_user"] = $this->user->fetch_one_user_by($data["data"]);
        $this->load->view('dashboard/contractor/edit_v',$data);

        // echo json_encode($data["data"],JSON_UNESCAPED_UNICODE);
    }

    public function edit_construction($id)
    {
        $this->record_action("edit_construction", "view");
        $this->load->model('customer_model', "customer");

        $slebew = $this->customer->fetch_category_name($id)[0];


        $data["data_employ"] = $this->customer->fetch_employees_names();
        $data["affiliate"] = $this->customer->fetch_affiliate_names();
        $data["data"] = $this->customer->fetch_one_construction_newer($id,$slebew->category_name)[0];
        $this->load->view('dashboard/constructor/edit_v',$data);

        // echo json_encode($data["data"],JSON_UNESCAPED_UNICODE);
        // print_r($slebew->category_name);
    }


    //-----------------------------------------------------------------------//


    public function update_customer($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            redirect("dashboard/customer_list");
        }

        $data = $this->security->xss_clean($data);
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customer($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'OB顧客の編集が失敗いたしました。');
            redirect("dashboard/customer_list");
        }
    }

    //---------------------------------------NEW-----------------------------------------//

    public function update_customers($id)
    {
        // print_r($_POST);die();
        $data = [
            // "gid" => $this->input->post("gid"),
            "cust_code" => $this->input->post("cust_code"),
            "氏名" => $this->input->post("name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "生年月日" => $this->input->post("birthday"),
            "age" => $this->input->post("age"),
            "共同住宅" => $this->input->post("building_name"),
            "電　　話" => $this->input->post("phone_number"),
            "種別" => $this->input->post("kinds"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("calendar"),
            "メールアドレス" => $this->input->post("email"),
            "備考" => $this->input->post("marks"),
            "氏名_かな" => $this->input->post("name_kana"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "長期優良住宅" => $this->input->post("longterm"),
            "設備10年保証加入" => $this->input->post("warranty"),
            "引渡日" => $this->input->post("delivery_date"),
            "定期点検項目_3ヶ月" => $this->input->post("inspection_time1"),
            "定期点検項目_6ヶ月" => $this->input->post("inspection_time2"),
            "定期点検項目_1年" => $this->input->post("inspection_time3"),
            "定期点検項目_2年" => $this->input->post("inspection_time4"),
            "定期点検項目_3年" => $this->input->post("inspection_time5"),
            "定期点検項目_5年" => $this->input->post("inspection_time6"),
            "定期点検項目_10年" => $this->input->post("inspection_time7"),
            "借入金融機関" => $this->input->post("bank"),
            "火災保険" => $this->input->post("insurance_company"),
            "火災保険備考" => $this->input->post("insurance"),
            "グリーン化事業補助金" => $this->input->post("subsidy"),
            "地盤改良工事" => $this->input->post("ground_improve"),
            "感謝祭" => $this->input->post("thanksgiving"),
            "イベントDM" => $this->input->post("dm")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/customers_list");
        // }
        if(!empty($this->input->post("sales_staff1")))
        {
            $data["営業担当"] = $this->input->post("sales_staff1");
        }
        else
        {
            $data["営業担当"] = $this->input->post("sales_staff");
        }

        if(!empty($this->input->post("pic_construction1")))
        {
            $data["工務担当"] = $this->input->post("pic_construction1");
        }
        else
        {
            $data["工務担当"] = $this->input->post("pic_construction");
        }


        if(!empty($this->input->post("incharge_coordinator1")))
        {
            $data["コーデ担当"] = $this->input->post("incharge_coordinator1");
        }
        else
        {
            $data["コーデ担当"] = $this->input->post("incharge_coordinator");
        }

        $data = $this->security->xss_clean($data);

        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customers_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_customers/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', 'COB顧客の編集が失敗いたしました。');
            $this->load->view('dashboard/customers/insert_v',$data);
            // redirect("dashboard/customers_list");
        }
    }
    
    public function update_client($id)
    {
        $data = [
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "birth_date" => $this->input->post("birth_date"),
            "age" => $this->input->post("age"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "土地　有無" => $this->input->post("with_or_without"),
            "ＤＭ" => $this->input->post("dm"),
            "備考" => $this->input->post("remarks"),
            "氏名_かな" => $this->input->post("full_name_kana"),
            "メールアドレス" => $this->input->post("email")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/client_list");
        // }
        if(!empty($this->input->post("first_time_change1")))
        {
            $data["初回担当"] = $this->input->post("first_time_change1");
        }
        else
        {
            $data["初回担当"] = $this->input->post("first_time_change");
        }

        if(!empty($this->input->post("pic_person1")))
        {
            $data["担当"] = $this->input->post("pic_person1");
        }
        else
        {
            $data["担当"] = $this->input->post("pic_person");
        }

        $data = $this->security->xss_clean($data);
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->update_client_new($data,$id);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_client/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', '営業中顧客の編集が失敗いたしました。');
            $this->load->view('dashboard/customer/edit_v',$data);
        }
    }

    public function update_employees($id)
    {
        $data["社員コード"] = $this->input->post("employee_code");
        $data["姓"] = $this->input->post("last_name");
        $data["名"] = $this->input->post("first_name");
        $data["せい"] = $this->input->post("last_furigana");
        $data["めい"] = $this->input->post("first_furigana");
        $data["保有資格1"] = $this->input->post("qualification_1");
        $data["保有資格2"] = $this->input->post("qualification_2");
        $data["保有資格3"] = $this->input->post("qualification_3");
        $data["保有資格4"] = $this->input->post("qualification_4");
        $data["保有資格5"] = $this->input->post("qualification_5");
        $data["入社日"] = $this->input->post("hire_date");
        $data["郵便番号1"] = $this->input->post("zip_code");
        $data["住所1"] = $this->input->post("address_line");
        $data["住所1番地"] = $this->input->post("address");
        $data["電話番号"] = $this->input->post("telephone_number");
        $data["携帯番号"] = $this->input->post("cellphone_number");
        $data["email"] = $this->input->post("email");
        $data["username"] = $this->input->post("username");
        $data["password"] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
        $data["view_password"] = $this->input->post("password");
        $data["privilage"] = $this->input->post("privilage");


        $config['upload_path'] = APPPATH.'../uploads/employee'; 
        $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        $config['max_size'] = '500000'; // in KlioBytes

        if($_FILES['sertificate_1']['size']!=0)
        {
            $new_names1 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_1"));
            $config['file_name'] = $new_names1;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('sertificate_1'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                $uploadData1 = $this->upload->data();
                $filename1 = $uploadData1['file_name'];
                $data["sertificate_1"]= $filename1;
            }
        }

        if($_FILES['sertificate_2']['size']!=0)
        {
            $new_names2 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_2"));
            $config['file_name'] = $new_names2;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('sertificate_2'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                $uploadData2 = $this->upload->data();
                $filename2 = $uploadData2['file_name'];
                $data["sertificate_2"]= $filename2;
            }
        }

        if($_FILES['sertificate_3']['size']!=0)
        {
            $new_names3 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_3"));
            $config['file_name'] = $new_names3;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('sertificate_3'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                $uploadData3 = $this->upload->data();
                $filename3 = $uploadData3['file_name'];
                $data["sertificate_3"]= $filename3;
            }
        }

        if($_FILES['sertificate_4']['size']!=0)
        {
            $new_names4 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_4"));
            $config['file_name'] = $new_names4;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('sertificate_4'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                $uploadData4 = $this->upload->data();
                $filename4 = $uploadData4['file_name'];
                $data["sertificate_4"]= $filename4;
            }
        }

        if($_FILES['sertificate_5']['size']!=0)
        {
            $new_names5 = str_replace(array(";",",","@","!","#",":","(",")"),"",$this->input->post("qualification_5"));
            $config['file_name'] = $new_names5;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('sertificate_5'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                $uploadData5 = $this->upload->data();
                $filename5 = $uploadData5['file_name'];
                $data["sertificate_5"]= $filename5;
            }
        }

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);

        $data = $this->security->xss_clean($data);

        $this->load->model('users_model',"user");
        $this->load->model('customer_model', "customer");
        $updated = $this->user->update_userpass($data);
        $update = $this->customer->update_employees_new($data,$id);

        if($update && $updated)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_employees/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', '社員の編集が失敗いたしました。');
            // redirect("dashboard/employees_list");
            redirect('dashboard/edit_client',$id);
        }
    }

    public function update_contractor($id)
    {
        // print_r($_POST);die();
        if(!empty($this->input->post("kinds")) && empty($this->input->post('username')))
        {
            // print_r($_POST);die();
            $data = [
                "種別" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name")),
                "連名" => $this->input->post("joint_name"),
                "〒" => $this->input->post("zip_code"),
                "都道府県" => $this->input->post("prefecture"),
                "住　　　所" => $this->input->post("address"),
                "住所2" => $this->input->post("address2"),
                "電　　話" => $this->input->post("telephone"),
                "invoice" => "T".$this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status")
            ];
            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data["b"] = $this->input->post("latitude");
                $data["l"] = $this->input->post("longitude");
                          
                $config2['upload_path'] = APPPATH.'../uploads/contractor'; 
                $config2['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
                $config2['max_size'] = '50000'; // in KlioBytes
                
                if($_FILES['admission']['size']!=0)
                {
                    $new_names = str_replace(array(";",",","@","!","#",":","(",")"),"",$_FILES['admission']['name']);
                    $config2['file_name'] = $new_names;
                    $this->upload->initialize($config2);
                    if ( ! $this->upload->do_upload('admission'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
                        $data["admission"]= $filename;
                    }
                }

                // print_r($data);

                $data = $this->security->xss_clean($data);

                $this->load->model('customer_model', "customer");
                $update = $this->customer->update_contractor_new($data,$id);
                if($update)
                {
                    $this->record_action("edit_customer","execute");
                    $this->session->set_flashdata('success', '更新しました');
                    redirect("dashboard/view_contractor/".$id);
                }
                else
                {
                    $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                    $this->load->view('dashboard/contractor/edit_v',$data);
                }
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                $this->load->view('dashboard/contractor/edit_v',$data);
            }
        }
        elseif(empty($this->input->post("kinds")) && !empty($this->input->post('username')))
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data['view_password'] = $this->input->post('password');
            $data['access'] = '3';
            $data['affilliate_from'] = $this->input->post('name');
            $data['date_regist'] = date('Y-m-d');
            
            $data = $this->security->xss_clean($data);

            $this->load->model('users_model',"user");
            $insert = $this->user->update_user_aff($data);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                $this->load->view('dashboard/contractor/edit_v',$data);
            }
        }
        elseif(!empty($this->input->post("kinds")) && !empty($this->input->post('username')))
        {
            $data = [
                "種別" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => str_replace(array(";",",","@","!","#",":"),"",$this->input->post("name")),
                "連名" => $this->input->post("joint_name"),
                "〒" => $this->input->post("zip_code"),
                "都道府県" => $this->input->post("prefecture"),
                "住　　　所" => $this->input->post("address"),
                "住所2" => $this->input->post("address2"),
                "電　　話" => $this->input->post("telephone"),
                "invoice" => $this->input->post("invoice"),
                "pic" => $this->input->post("pic"),
                "phone" => $this->input->post("phone"),
                "fax" => $this->input->post("fax"),
                "email" => $this->input->post("email"),
                "pic_nd" => $this->input->post("pic_nd"),
                "phone_nd" => $this->input->post("phone_nd"),
                "email_nd" => $this->input->post("email_nd"),
                "project_stat" => $this->input->post("project_stat"),
                "status" => $this->input->post("status")
            ];
            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data["b"] = $this->input->post("latitude");
                $data["l"] = $this->input->post("longitude");

                $config2['upload_path'] = APPPATH.'../uploads/contractor'; 
                $config2['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
                $config2['max_size'] = '50000'; // in KlioBytes

                if($_FILES['admission']['size']!=0)
                {
                    $new_names = str_replace(array(";",",","@","!","#",":","(",")"),"",$_FILES['admission']['name']);
                    $config2['file_name'] = $new_names;
                    $this->upload->initialize($config2);
                    if ( ! $this->upload->do_upload('admission'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData = $this->upload->data();
                        $filename = $uploadData['file_name'];
                        $data["admission"]= $filename;
                    }
                }

                $data2['username'] = $this->input->post('username');
                $data2['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
                $data2['view_password'] = $this->input->post('password');
                $data2['access'] = '3';
                $data2['affilliate_from'] = $this->input->post('name');
                $data2['date_regist'] = date('Y-m-d');


                $data = $this->security->xss_clean($data);
                $data2 = $this->security->xss_clean($data2);
                
                $this->load->model('users_model',"user");
                $insert = $this->user->update_user_aff($data2);

                $datar["data"] = [
                    "gid" => $this->input->post("contractor_code"),
                    "種別" => $this->input->post("kinds"),
                    "no" => $this->input->post("no"),
                    "name" => $this->input->post("name"),
                    "連名" => $this->input->post("joint_name"),
                    "〒" => $this->input->post("zip_code"),
                    "都道府県" => $this->input->post("prefecture"),
                    "住　　　所" => $this->input->post("address"),
                    "住所2" => $this->input->post("address2"),
                    "電　　話" => $this->input->post("telephone"),
                    "invoice" => "T".$this->input->post("invoice"),
                    "pic" => $this->input->post("pic"),
                    "phone" => $this->input->post("phone"),
                    "fax" => $this->input->post("fax"),
                    "email" => $this->input->post("email"),
                    "pic_nd" => $this->input->post("pic_nd"),
                    "phone_nd" => $this->input->post("phone_nd"),
                    "email_nd" => $this->input->post("email_nd"),
                    "project_stat" => $this->input->post("project_stat"),
                    "status" => $this->input->post("status")
                ];


                $this->load->model('customer_model',"customer");
                $insert = $this->customer->update_contractor_new($data,$id);
                if($insert)
                {
                    $this->record_action("add_contractorr","execute");
                    $this->session->set_flashdata('success', '更新しました');
                    redirect("dashboard/contractor_list");
                }
                else
                {
                    $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                    $this->load->view('dashboard/contractor/edit_v',$datar);
                }
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                $this->load->view('dashboard/contractor/edit_v',$data);
            }

        }
        else
        {
            $this->session->set_flashdata('error', 'Something was Error.');
            redirect("dashboard/contractor_list");
        }

       
    }

    public function update_construction($id)
    {

        $data["cust_code"] = mb_convert_kana($this->input->post("cust_code"), "as");
        $data["cust_name"] = mb_convert_kana($this->input->post("cust_name"), "as");
        $data["kinds"] = mb_convert_kana($this->input->post("kinds"), "as");
        $data["const_no"] = mb_convert_kana($this->input->post("const_no"), "as");
        $data["remarks"] = mb_convert_kana($this->input->post("remarks"), "as");
        $data["contract_date"] = $this->input->post("contract_date");
        $data["construction_start_date"] = $this->input->post("construction_start_date");
        $data["upper_building_date"] = $this->input->post("upper_building_date");
        $data["completion_date"] = $this->input->post("completion_date");
        $data["delivery_date"] = $this->input->post("delivery_date");
        $data["amount_money"] = mb_convert_kana($this->input->post("amount_money"), "as");
        $data["construct_stat"] = $this->input->post("construct_stat");
        
        if(!empty($this->input->post("sales_staff1")))
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff1"), "as");
        }
        else
        {
            $data["sales_staff"] = mb_convert_kana($this->input->post("sales_staff"), "as");
        }

        if(!empty($this->input->post("incharge_construction1")))
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction1"), "as");
        }
        else
        {
            $data["incharge_construction"] = mb_convert_kana($this->input->post("incharge_construction"), "as");
        }

        if(!empty($this->input->post("incharge_coordinator1")))
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator1"), "as");
        }
        else
        {
            $data["incharge_coordinator"] = mb_convert_kana($this->input->post("incharge_coordinator"), "as");
        }

        // $config['upload_path'] = APPPATH.'../uploads/files'; 
        // $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        // $config['max_size'] = '500000'; // in KlioBytes

        if(!empty($this->input->post("constract_drawing1blueatext")) || !empty($this->input->post("constract_drawing2blueatext")) || !empty($this->input->post("constract_drawing3blueatext")) || !empty($this->input->post("constract_drawing4blueatext")) || !empty($this->input->post("constract_drawing5blueatext")) || !empty($this->input->post("constract_drawing6blueatext")) || !empty($this->input->post("constract_drawing7blueatext")) || !empty($this->input->post("constract_drawing8blueatext")) || !empty($this->input->post("constract_drawing9blueatext")) || !empty($this->input->post("constract_drawing10blueatext")) || !empty($this->input->post("desc_item1bluea")) || !empty($this->input->post("co_owner1bluea")) || !empty($this->input->post("desc_item2bluea")) || !empty($this->input->post("co_owner2bluea")) || !empty($this->input->post("desc_item3bluea")) || !empty($this->input->post("co_owner3bluea")) || !empty($this->input->post("desc_item4bluea")) || !empty($this->input->post("co_owner4bluea")) || !empty($this->input->post("desc_item5bluea")) || !empty($this->input->post("co_owner5bluea")) || !empty($this->input->post("desc_item6bluea")) || !empty($this->input->post("co_owner6bluea")) || !empty($this->input->post("desc_item7bluea")) || !empty($this->input->post("co_owner7bluea")) || !empty($this->input->post("desc_item8bluea")) || !empty($this->input->post("co_owner8bluea")) || !empty($this->input->post("desc_item9bluea")) || !empty($this->input->post("co_owner9bluea")) || !empty($this->input->post("desc_item10bluea")) || !empty($this->input->post("co_owner10bluea")))
        {

            $data["check1bluea"] = $this->input->post("check1bluea");
            $data["desc_item1bluea"] = mb_convert_kana($this->input->post("desc_item1bluea"), "as");
            $data["check2bluea"] = $this->input->post("check2bluea");
            $data["desc_item2bluea"] = mb_convert_kana($this->input->post("desc_item2bluea"), "as");
            $data["check3bluea"] = $this->input->post("check3bluea");
            $data["desc_item3bluea"] = mb_convert_kana($this->input->post("desc_item3bluea"), "as");
            $data["check4bluea"] = $this->input->post("check4bluea");
            $data["desc_item4bluea"] = mb_convert_kana($this->input->post("desc_item4bluea"), "as");
            $data["check5bluea"] = $this->input->post("check5bluea");
            $data["desc_item5bluea"] = mb_convert_kana($this->input->post("desc_item5bluea"), "as");
            $data["check6bluea"] = $this->input->post("check6bluea");
            $data["desc_item6bluea"] = mb_convert_kana($this->input->post("desc_item6bluea"), "as");
            $data["check7bluea"] = $this->input->post("check7bluea");
            $data["desc_item7bluea"] = mb_convert_kana($this->input->post("desc_item7bluea"), "as");
            $data["check8bluea"] = $this->input->post("check8bluea");
            $data["desc_item8bluea"] = mb_convert_kana($this->input->post("desc_item8bluea"), "as");
            $data["check9bluea"] = $this->input->post("check9bluea");
            $data["desc_item9bluea"] = mb_convert_kana($this->input->post("desc_item9bluea"), "as");
            $data["check10bluea"] = $this->input->post("check10bluea");
            $data["desc_item10bluea"] = mb_convert_kana($this->input->post("desc_item10bluea"), "as");
            $data["category_name1bluea"] = "確認申請書一式";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluea")))
            {
                $data["co_owner1bluea"] = implode(";", $this->input->post("co_owner1bluea"));
            }
            else
            {
                $data["co_owner1bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluea")))
            {
                $data["co_owner2bluea"] = implode(";", $this->input->post("co_owner2bluea"));
            }
            else
            {
                $data["co_owner2bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluea")))
            {
                $data["co_owner3bluea"] = implode(";", $this->input->post("co_owner3bluea"));
            }
            else
            {
                $data["co_owner3bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluea")))
            {
                $data["co_owner4bluea"] = implode(";", $this->input->post("co_owner4bluea"));
            }
            else
            {
                $data["co_owner4bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluea")))
            {
                $data["co_owner5bluea"] = implode(";", $this->input->post("co_owner5bluea"));
            }
            else
            {
                $data["co_owner5bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluea")))
            {
                $data["co_owner6bluea"] = implode(";", $this->input->post("co_owner6bluea"));
            }
            else
            {
                $data["co_owner6bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluea")))
            {
                $data["co_owner7bluea"] = implode(";", $this->input->post("co_owner7bluea"));
            }
            else
            {
                $data["co_owner7bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluea")))
            {
                $data["co_owner8bluea"] = implode(";", $this->input->post("co_owner8bluea"));
            }
            else
            {
                $data["co_owner8bluea"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluea")))
            {
                $data["co_owner9bluea"] = implode(";", $this->input->post("co_owner9bluea"));
            }
            else
            {
                $data["co_owner9bluea"] = '';
            }

            if (!empty($this->input->post("co_owner10bluea")))
            {
                $data["co_owner10bluea"] = implode(";", $this->input->post("co_owner10bluea"));
            }
            else
            {
                $data["co_owner10bluea"] = '';
            }



            $data["constract_drawing1bluea"]= $this->input->post("constract_drawing1blueatext");
            $data["constract_drawing2bluea"]= $this->input->post("constract_drawing2blueatext");
            $data["constract_drawing3bluea"]= $this->input->post("constract_drawing3blueatext");
            $data["constract_drawing4bluea"]= $this->input->post("constract_drawing4blueatext");
            $data["constract_drawing5bluea"]= $this->input->post("constract_drawing5blueatext");
            $data["constract_drawing6bluea"]= $this->input->post("constract_drawing6blueatext");
            $data["constract_drawing7bluea"]= $this->input->post("constract_drawing7blueatext");
            $data["constract_drawing8bluea"]= $this->input->post("constract_drawing8blueatext");
            $data["constract_drawing9bluea"]= $this->input->post("constract_drawing9blueatext");
            $data["constract_drawing10bluea"]= $this->input->post("constract_drawing10blueatext");


            $data["date_insert_drawing1bluea"] = date('Y-m-d');
            $data["date_insert_drawing2bluea"] = date('Y-m-d');
            $data["date_insert_drawing3bluea"] = date('Y-m-d');
            $data["date_insert_drawing4bluea"] = date('Y-m-d');
            $data["date_insert_drawing5bluea"] = date('Y-m-d');
            $data["date_insert_drawing6bluea"] = date('Y-m-d');
            $data["date_insert_drawing7bluea"] = date('Y-m-d');
            $data["date_insert_drawing8bluea"] = date('Y-m-d');
            $data["date_insert_drawing9bluea"] = date('Y-m-d');
            $data["date_insert_drawing10bluea"] = date('Y-m-d');

        }
        
        if(!empty($this->input->post("constract_drawing1bluebtext")) || !empty($this->input->post("constract_drawing2bluebtext")) || !empty($this->input->post("constract_drawing3bluebtext")) || !empty($this->input->post("constract_drawing4bluebtext")) || !empty($this->input->post("constract_drawing5bluebtext")) || !empty($this->input->post("constract_drawing6bluebtext")) || !empty($this->input->post("constract_drawing7bluebtext")) || !empty($this->input->post("constract_drawing8bluebtext")) || !empty($this->input->post("constract_drawing9bluebtext")) || !empty($this->input->post("constract_drawing10bluebtext")) || !empty($this->input->post("desc_item1blueb")) || !empty($this->input->post("co_owner1blueb")) || !empty($this->input->post("desc_item2blueb")) || !empty($this->input->post("co_owner2blueb")) || !empty($this->input->post("desc_item3blueb")) || !empty($this->input->post("co_owner3blueb")) || !empty($this->input->post("desc_item4blueb")) || !empty($this->input->post("co_owner4blueb")) || !empty($this->input->post("desc_item5blueb")) || !empty($this->input->post("co_owner5blueb")) || !empty($this->input->post("desc_item6blueb")) || !empty($this->input->post("co_owner6blueb")) || !empty($this->input->post("desc_item7blueb")) || !empty($this->input->post("co_owner7blueb")) || !empty($this->input->post("desc_item8blueb")) || !empty($this->input->post("co_owner8blueb")) || !empty($this->input->post("desc_item9blueb")) || !empty($this->input->post("co_owner9blueb")) || !empty($this->input->post("desc_item10blueb")) || !empty($this->input->post("co_owner10blueb")))
        {


            $data["check1blueb"] = $this->input->post("check1blueb");
            $data["desc_item1blueb"] = mb_convert_kana($this->input->post("desc_item1blueb"), "as");
            $data["check2blueb"] = $this->input->post("check2blueb");
            $data["desc_item2blueb"] = mb_convert_kana($this->input->post("desc_item2blueb"), "as");
            $data["check3blueb"] = $this->input->post("check3blueb");
            $data["desc_item3blueb"] = mb_convert_kana($this->input->post("desc_item3blueb"), "as");
            $data["check4blueb"] = $this->input->post("check4blueb");
            $data["desc_item4blueb"] = mb_convert_kana($this->input->post("desc_item4blueb"), "as");
            $data["check5blueb"] = $this->input->post("check5blueb");
            $data["desc_item5blueb"] = mb_convert_kana($this->input->post("desc_item5blueb"), "as");
            $data["check6blueb"] = $this->input->post("check6blueb");
            $data["desc_item6blueb"] = mb_convert_kana($this->input->post("desc_item6blueb"), "as");
            $data["check7blueb"] = $this->input->post("check7blueb");
            $data["desc_item7blueb"] = mb_convert_kana($this->input->post("desc_item7blueb"), "as");
            $data["check8blueb"] = $this->input->post("check8blueb");
            $data["desc_item8blueb"] = mb_convert_kana($this->input->post("desc_item8blueb"), "as");
            $data["check9blueb"] = $this->input->post("check9blueb");
            $data["desc_item9blueb"] = mb_convert_kana($this->input->post("desc_item9blueb"), "as");
            $data["check10blueb"] = $this->input->post("check10blueb");
            $data["desc_item10blueb"] = mb_convert_kana($this->input->post("desc_item10blueb"), "as");
            $data["category_name1blueb"] = "工事工程表";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blueb")))
            {
                $data["co_owner1blueb"] = implode(";", $this->input->post("co_owner1blueb"));
            }
            else
            {
                $data["co_owner1blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueb")))
            {
                $data["co_owner2blueb"] = implode(";", $this->input->post("co_owner2blueb"));
            }
            else
            {
                $data["co_owner2blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueb")))
            {
                $data["co_owner3blueb"] = implode(";", $this->input->post("co_owner3blueb"));
            }
            else
            {
                $data["co_owner3blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueb")))
            {
                $data["co_owner4blueb"] = implode(";", $this->input->post("co_owner4blueb"));
            }
            else
            {
                $data["co_owner4blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueb")))
            {
                $data["co_owner5blueb"] = implode(";", $this->input->post("co_owner5blueb"));
            }
            else
            {
                $data["co_owner5blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueb")))
            {
                $data["co_owner6blueb"] = implode(";", $this->input->post("co_owner6blueb"));
            }
            else
            {
                $data["co_owner6blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueb")))
            {
                $data["co_owner7blueb"] = implode(";", $this->input->post("co_owner7blueb"));
            }
            else
            {
                $data["co_owner7blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueb")))
            {
                $data["co_owner8blueb"] = implode(";", $this->input->post("co_owner8blueb"));
            }
            else
            {
                $data["co_owner8blueb"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueb")))
            {
                $data["co_owner9blueb"] = implode(";", $this->input->post("co_owner9blueb"));
            }
            else
            {
                $data["co_owner9blueb"] = '';
            }

            if (!empty($this->input->post("co_owner10blueb")))
            {
                $data["co_owner10blueb"] = implode(";", $this->input->post("co_owner10blueb"));
            }
            else
            {
                $data["co_owner10blueb"] = '';
            }



            $data["constract_drawing1blueb"]= $this->input->post("constract_drawing1bluebtext");
            $data["constract_drawing2blueb"]= $this->input->post("constract_drawing2bluebtext");
            $data["constract_drawing3blueb"]= $this->input->post("constract_drawing3bluebtext");
            $data["constract_drawing4blueb"]= $this->input->post("constract_drawing4bluebtext");
            $data["constract_drawing5blueb"]= $this->input->post("constract_drawing5bluebtext");
            $data["constract_drawing6blueb"]= $this->input->post("constract_drawing6bluebtext");
            $data["constract_drawing7blueb"]= $this->input->post("constract_drawing7bluebtext");
            $data["constract_drawing8blueb"]= $this->input->post("constract_drawing8bluebtext");
            $data["constract_drawing9blueb"]= $this->input->post("constract_drawing9bluebtext");
            $data["constract_drawing10blueb"]= $this->input->post("constract_drawing10bluebtext");


            $data["date_insert_drawing1blueb"] = date('Y-m-d');
            $data["date_insert_drawing2blueb"] = date('Y-m-d');
            $data["date_insert_drawing3blueb"] = date('Y-m-d');
            $data["date_insert_drawing4blueb"] = date('Y-m-d');
            $data["date_insert_drawing5blueb"] = date('Y-m-d');
            $data["date_insert_drawing6blueb"] = date('Y-m-d');
            $data["date_insert_drawing7blueb"] = date('Y-m-d');
            $data["date_insert_drawing8blueb"] = date('Y-m-d');
            $data["date_insert_drawing9blueb"] = date('Y-m-d');
            $data["date_insert_drawing10blueb"] = date('Y-m-d');
        }
        
        if(!empty($this->input->post("constract_drawing1bluectext")) || !empty($this->input->post("constract_drawing2bluectext")) || !empty($this->input->post("constract_drawing3bluectext")) || !empty($this->input->post("constract_drawing4bluectext")) || !empty($this->input->post("constract_drawing5bluectext")) || !empty($this->input->post("constract_drawing6bluectext")) || !empty($this->input->post("constract_drawing7bluectext")) || !empty($this->input->post("constract_drawing8bluectext")) || !empty($this->input->post("constract_drawing9bluectext")) || !empty($this->input->post("constract_drawing10bluectext")) || !empty($this->input->post("desc_item1bluec")) || !empty($this->input->post("co_owner1bluec")) || !empty($this->input->post("desc_item2bluec")) || !empty($this->input->post("co_owner2bluec")) || !empty($this->input->post("desc_item3bluec")) || !empty($this->input->post("co_owner3bluec")) || !empty($this->input->post("desc_item4bluec")) || !empty($this->input->post("co_owner4bluec")) || !empty($this->input->post("desc_item5bluec")) || !empty($this->input->post("co_owner5bluec")) || !empty($this->input->post("desc_item6bluec")) || !empty($this->input->post("co_owner6bluec")) || !empty($this->input->post("desc_item7bluec")) || !empty($this->input->post("co_owner7bluec")) || !empty($this->input->post("desc_item8bluec")) || !empty($this->input->post("co_owner8bluec")) || !empty($this->input->post("desc_item9bluec")) || !empty($this->input->post("co_owner9bluec")) || !empty($this->input->post("desc_item10bluec")) || !empty($this->input->post("co_owner10bluec")))
        {
   

            $data["check1bluec"] = $this->input->post("check1bluec");
            $data["desc_item1bluec"] = mb_convert_kana($this->input->post("desc_item1bluec"), "as");
            $data["check2bluec"] = $this->input->post("check2bluec");
            $data["desc_item2bluec"] = mb_convert_kana($this->input->post("desc_item2bluec"), "as");
            $data["check3bluec"] = $this->input->post("check3bluec");
            $data["desc_item3bluec"] = mb_convert_kana($this->input->post("desc_item3bluec"), "as");
            $data["check4bluec"] = $this->input->post("check4bluec");
            $data["desc_item4bluec"] = mb_convert_kana($this->input->post("desc_item4bluec"), "as");
            $data["check5bluec"] = $this->input->post("check5bluec");
            $data["desc_item5bluec"] = mb_convert_kana($this->input->post("desc_item5bluec"), "as");
            $data["check6bluec"] = $this->input->post("check6bluec");
            $data["desc_item6bluec"] = mb_convert_kana($this->input->post("desc_item6bluec"), "as");
            $data["check7bluec"] = $this->input->post("check7bluec");
            $data["desc_item7bluec"] = mb_convert_kana($this->input->post("desc_item7bluec"), "as");
            $data["check8bluec"] = $this->input->post("check8bluec");
            $data["desc_item8bluec"] = mb_convert_kana($this->input->post("desc_item8bluec"), "as");
            $data["check9bluec"] = $this->input->post("check9bluec");
            $data["desc_item9bluec"] = mb_convert_kana($this->input->post("desc_item9bluec"), "as");
            $data["check10bluec"] = $this->input->post("check10bluec");
            $data["desc_item10bluec"] = mb_convert_kana($this->input->post("desc_item10bluec"), "as");
            $data["category_name1bluec"] = "工事着工資料";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluec")))
            {
                $data["co_owner1bluec"] = implode(";", $this->input->post("co_owner1bluec"));
            }
            else
            {
                $data["co_owner1bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluec")))
            {
                $data["co_owner2bluec"] = implode(";", $this->input->post("co_owner2bluec"));
            }
            else
            {
                $data["co_owner2bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluec")))
            {
                $data["co_owner3bluec"] = implode(";", $this->input->post("co_owner3bluec"));
            }
            else
            {
                $data["co_owner3bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluec")))
            {
                $data["co_owner4bluec"] = implode(";", $this->input->post("co_owner4bluec"));
            }
            else
            {
                $data["co_owner4bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluec")))
            {
                $data["co_owner5bluec"] = implode(";", $this->input->post("co_owner5bluec"));
            }
            else
            {
                $data["co_owner5bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluec")))
            {
                $data["co_owner6bluec"] = implode(";", $this->input->post("co_owner6bluec"));
            }
            else
            {
                $data["co_owner6bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluec")))
            {
                $data["co_owner7bluec"] = implode(";", $this->input->post("co_owner7bluec"));
            }
            else
            {
                $data["co_owner7bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluec")))
            {
                $data["co_owner8bluec"] = implode(";", $this->input->post("co_owner8bluec"));
            }
            else
            {
                $data["co_owner8bluec"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluec")))
            {
                $data["co_owner9bluec"] = implode(";", $this->input->post("co_owner9bluec"));
            }
            else
            {
                $data["co_owner9bluec"] = '';
            }

            if (!empty($this->input->post("co_owner10bluec")))
            {
                $data["co_owner10bluec"] = implode(";", $this->input->post("co_owner10bluec"));
            }
            else
            {
                $data["co_owner10bluec"] = '';
            }




            $data["constract_drawing1bluec"]= $this->input->post("constract_drawing1bluectext");
            $data["constract_drawing2bluec"]= $this->input->post("constract_drawing2bluectext");
            $data["constract_drawing3bluec"]= $this->input->post("constract_drawing3bluectext");
            $data["constract_drawing4bluec"]= $this->input->post("constract_drawing4bluectext");
            $data["constract_drawing5bluec"]= $this->input->post("constract_drawing5bluectext");
            $data["constract_drawing6bluec"]= $this->input->post("constract_drawing6bluectext");
            $data["constract_drawing7bluec"]= $this->input->post("constract_drawing7bluectext");
            $data["constract_drawing8bluec"]= $this->input->post("constract_drawing8bluectext");
            $data["constract_drawing9bluec"]= $this->input->post("constract_drawing9bluectext");
            $data["constract_drawing10bluec"]= $this->input->post("constract_drawing10bluectext");


            $data["date_insert_drawing1bluec"] = date('Y-m-d');
            $data["date_insert_drawing2bluec"] = date('Y-m-d');
            $data["date_insert_drawing3bluec"] = date('Y-m-d');
            $data["date_insert_drawing4bluec"] = date('Y-m-d');
            $data["date_insert_drawing5bluec"] = date('Y-m-d');
            $data["date_insert_drawing6bluec"] = date('Y-m-d');
            $data["date_insert_drawing7bluec"] = date('Y-m-d');
            $data["date_insert_drawing8bluec"] = date('Y-m-d');
            $data["date_insert_drawing9bluec"] = date('Y-m-d');
            $data["date_insert_drawing10bluec"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluedtext")) || !empty($this->input->post("constract_drawing2bluedtext")) || !empty($this->input->post("constract_drawing3bluedtext")) || !empty($this->input->post("constract_drawing4bluedtext")) || !empty($this->input->post("constract_drawing5bluedtext")) || !empty($this->input->post("constract_drawing6bluedtext")) || !empty($this->input->post("constract_drawing7bluedtext")) || !empty($this->input->post("constract_drawing8bluedtext")) || !empty($this->input->post("constract_drawing9bluedtext")) || !empty($this->input->post("constract_drawing10bluedtext")) || !empty($this->input->post("desc_item1blued")) || !empty($this->input->post("co_owner1blued")) || !empty($this->input->post("desc_item2blued")) || !empty($this->input->post("co_owner2blued")) || !empty($this->input->post("desc_item3blued")) || !empty($this->input->post("co_owner3blued")) || !empty($this->input->post("desc_item4blued")) || !empty($this->input->post("co_owner4blued")) || !empty($this->input->post("desc_item5blued")) || !empty($this->input->post("co_owner5blued")) || !empty($this->input->post("desc_item6blued")) || !empty($this->input->post("co_owner6blued")) || !empty($this->input->post("desc_item7blued")) || !empty($this->input->post("co_owner7blued")) || !empty($this->input->post("desc_item8blued")) || !empty($this->input->post("co_owner8blued")) || !empty($this->input->post("desc_item9blued")) || !empty($this->input->post("co_owner9blued")) || !empty($this->input->post("desc_item10blued")) || !empty($this->input->post("co_owner10blued")))
        {

            $data["check1blued"] = $this->input->post("check1blued");
            $data["desc_item1blued"] = mb_convert_kana($this->input->post("desc_item1blued"), "as");
            $data["check2blued"] = $this->input->post("check2blued");
            $data["desc_item2blued"] = mb_convert_kana($this->input->post("desc_item2blued"), "as");
            $data["check3blued"] = $this->input->post("check3blued");
            $data["desc_item3blued"] = mb_convert_kana($this->input->post("desc_item3blued"), "as");
            $data["check4blued"] = $this->input->post("check4blued");
            $data["desc_item4blued"] = mb_convert_kana($this->input->post("desc_item4blued"), "as");
            $data["check5blued"] = $this->input->post("check5blued");
            $data["desc_item5blued"] = mb_convert_kana($this->input->post("desc_item5blued"), "as");
            $data["check6blued"] = $this->input->post("check6blued");
            $data["desc_item6blued"] = mb_convert_kana($this->input->post("desc_item6blued"), "as");
            $data["check7blued"] = $this->input->post("check7blued");
            $data["desc_item7blued"] = mb_convert_kana($this->input->post("desc_item7blued"), "as");
            $data["check8blued"] = $this->input->post("check8blued");
            $data["desc_item8blued"] = mb_convert_kana($this->input->post("desc_item8blued"), "as");
            $data["check9blued"] = $this->input->post("check9blued");
            $data["desc_item9blued"] = mb_convert_kana($this->input->post("desc_item9blued"), "as");
            $data["check10blued"] = $this->input->post("check10blued");
            $data["desc_item10blued"] = mb_convert_kana($this->input->post("desc_item10blued"), "as");
            $data["category_name1blued"] = "施工図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blued")))
            {
                $data["co_owner1blued"] = implode(";", $this->input->post("co_owner1blued"));
            }
            else
            {
                $data["co_owner1blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blued")))
            {
                $data["co_owner2blued"] = implode(";", $this->input->post("co_owner2blued"));
            }
            else
            {
                $data["co_owner2blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blued")))
            {
                $data["co_owner3blued"] = implode(";", $this->input->post("co_owner3blued"));
            }
            else
            {
                $data["co_owner3blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blued")))
            {
                $data["co_owner4blued"] = implode(";", $this->input->post("co_owner4blued"));
            }
            else
            {
                $data["co_owner4blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blued")))
            {
                $data["co_owner5blued"] = implode(";", $this->input->post("co_owner5blued"));
            }
            else
            {
                $data["co_owner5blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blued")))
            {
                $data["co_owner6blued"] = implode(";", $this->input->post("co_owner6blued"));
            }
            else
            {
                $data["co_owner6blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blued")))
            {
                $data["co_owner7blued"] = implode(";", $this->input->post("co_owner7blued"));
            }
            else
            {
                $data["co_owner7blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blued")))
            {
                $data["co_owner8blued"] = implode(";", $this->input->post("co_owner8blued"));
            }
            else
            {
                $data["co_owner8blued"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blued")))
            {
                $data["co_owner9blued"] = implode(";", $this->input->post("co_owner9blued"));
            }
            else
            {
                $data["co_owner9blued"] = '';
            }

            if (!empty($this->input->post("co_owner10blued")))
            {
                $data["co_owner10blued"] = implode(";", $this->input->post("co_owner10blued"));
            }
            else
            {
                $data["co_owner10blued"] = '';
            }


            $data["constract_drawing1blued"]= $this->input->post("constract_drawing1bluedtext");
            $data["constract_drawing2blued"]= $this->input->post("constract_drawing2bluedtext");
            $data["constract_drawing3blued"]= $this->input->post("constract_drawing3bluedtext");
            $data["constract_drawing4blued"]= $this->input->post("constract_drawing4bluedtext");
            $data["constract_drawing5blued"]= $this->input->post("constract_drawing5bluedtext");
            $data["constract_drawing6blued"]= $this->input->post("constract_drawing6bluedtext");
            $data["constract_drawing7blued"]= $this->input->post("constract_drawing7bluedtext");
            $data["constract_drawing8blued"]= $this->input->post("constract_drawing8bluedtext");
            $data["constract_drawing9blued"]= $this->input->post("constract_drawing9bluedtext");
            $data["constract_drawing10blued"]= $this->input->post("constract_drawing10bluedtext");


            $data["date_insert_drawing1blued"] = date('Y-m-d');
            $data["date_insert_drawing2blued"] = date('Y-m-d');
            $data["date_insert_drawing3blued"] = date('Y-m-d');
            $data["date_insert_drawing4blued"] = date('Y-m-d');
            $data["date_insert_drawing5blued"] = date('Y-m-d');
            $data["date_insert_drawing6blued"] = date('Y-m-d');
            $data["date_insert_drawing7blued"] = date('Y-m-d');
            $data["date_insert_drawing8blued"] = date('Y-m-d');
            $data["date_insert_drawing9blued"] = date('Y-m-d');
            $data["date_insert_drawing10blued"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueetext")) || !empty($this->input->post("constract_drawing2blueetext")) || !empty($this->input->post("constract_drawing3blueetext")) || !empty($this->input->post("constract_drawing4blueetext")) || !empty($this->input->post("constract_drawing5blueetext")) || !empty($this->input->post("constract_drawing6blueetext")) || !empty($this->input->post("constract_drawing7blueetext")) || !empty($this->input->post("constract_drawing8blueetext")) || !empty($this->input->post("constract_drawing9blueetext")) || !empty($this->input->post("constract_drawing10blueetext")) || !empty($this->input->post("desc_item1bluee")) || !empty($this->input->post("co_owner1bluee")) || !empty($this->input->post("desc_item2bluee")) || !empty($this->input->post("co_owner2bluee")) || !empty($this->input->post("desc_item3bluee")) || !empty($this->input->post("co_owner3bluee")) || !empty($this->input->post("desc_item4bluee")) || !empty($this->input->post("co_owner4bluee")) || !empty($this->input->post("desc_item5bluee")) || !empty($this->input->post("co_owner5bluee")) || !empty($this->input->post("desc_item6bluee")) || !empty($this->input->post("co_owner6bluee")) || !empty($this->input->post("desc_item7bluee")) || !empty($this->input->post("co_owner7bluee")) || !empty($this->input->post("desc_item8bluee")) || !empty($this->input->post("co_owner8bluee")) || !empty($this->input->post("desc_item9bluee")) || !empty($this->input->post("co_owner9bluee")) || !empty($this->input->post("desc_item10bluee")) || !empty($this->input->post("co_owner10bluee")))
        {


            $data["check1bluee"] = $this->input->post("check1bluee");
            $data["desc_item1bluee"] = mb_convert_kana($this->input->post("desc_item1bluee"), "as");
            $data["check2bluee"] = $this->input->post("check2bluee");
            $data["desc_item2bluee"] = mb_convert_kana($this->input->post("desc_item2bluee"), "as");
            $data["check3bluee"] = $this->input->post("check3bluee");
            $data["desc_item3bluee"] = mb_convert_kana($this->input->post("desc_item3bluee"), "as");
            $data["check4bluee"] = $this->input->post("check4bluee");
            $data["desc_item4bluee"] = mb_convert_kana($this->input->post("desc_item4bluee"), "as");
            $data["check5bluee"] = $this->input->post("check5bluee");
            $data["desc_item5bluee"] = mb_convert_kana($this->input->post("desc_item5bluee"), "as");
            $data["check6bluee"] = $this->input->post("check6bluee");
            $data["desc_item6bluee"] = mb_convert_kana($this->input->post("desc_item6bluee"), "as");
            $data["check7bluee"] = $this->input->post("check7bluee");
            $data["desc_item7bluee"] = mb_convert_kana($this->input->post("desc_item7bluee"), "as");
            $data["check8bluee"] = $this->input->post("check8bluee");
            $data["desc_item8bluee"] = mb_convert_kana($this->input->post("desc_item8bluee"), "as");
            $data["check9bluee"] = $this->input->post("check9bluee");
            $data["desc_item9bluee"] = mb_convert_kana($this->input->post("desc_item9bluee"), "as");
            $data["check10bluee"] = $this->input->post("check10bluee");
            $data["desc_item10bluee"] = mb_convert_kana($this->input->post("desc_item10bluee"), "as");
            $data["category_name1bluee"] = "変更図面";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1bluee")))
            {
                $data["co_owner1bluee"] = implode(";", $this->input->post("co_owner1bluee"));
            }
            else
            {
                $data["co_owner1bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluee")))
            {
                $data["co_owner2bluee"] = implode(";", $this->input->post("co_owner2bluee"));
            }
            else
            {
                $data["co_owner2bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluee")))
            {
                $data["co_owner3bluee"] = implode(";", $this->input->post("co_owner3bluee"));
            }
            else
            {
                $data["co_owner3bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluee")))
            {
                $data["co_owner4bluee"] = implode(";", $this->input->post("co_owner4bluee"));
            }
            else
            {
                $data["co_owner4bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluee")))
            {
                $data["co_owner5bluee"] = implode(";", $this->input->post("co_owner5bluee"));
            }
            else
            {
                $data["co_owner5bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluee")))
            {
                $data["co_owner6bluee"] = implode(";", $this->input->post("co_owner6bluee"));
            }
            else
            {
                $data["co_owner6bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluee")))
            {
                $data["co_owner7bluee"] = implode(";", $this->input->post("co_owner7bluee"));
            }
            else
            {
                $data["co_owner7bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluee")))
            {
                $data["co_owner8bluee"] = implode(";", $this->input->post("co_owner8bluee"));
            }
            else
            {
                $data["co_owner8bluee"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluee")))
            {
                $data["co_owner9bluee"] = implode(";", $this->input->post("co_owner9bluee"));
            }
            else
            {
                $data["co_owner9bluee"] = '';
            }

            if (!empty($this->input->post("co_owner10bluee")))
            {
                $data["co_owner10bluee"] = implode(";", $this->input->post("co_owner10bluee"));
            }
            else
            {
                $data["co_owner10bluee"] = '';
            }

            $data["constract_drawing1bluee"]= $this->input->post("constract_drawing1blueetext");
            $data["constract_drawing2bluee"]= $this->input->post("constract_drawing2blueetext");
            $data["constract_drawing3bluee"]= $this->input->post("constract_drawing3blueetext");
            $data["constract_drawing4bluee"]= $this->input->post("constract_drawing4blueetext");
            $data["constract_drawing5bluee"]= $this->input->post("constract_drawing5blueetext");
            $data["constract_drawing6bluee"]= $this->input->post("constract_drawing6blueetext");
            $data["constract_drawing7bluee"]= $this->input->post("constract_drawing7blueetext");
            $data["constract_drawing8bluee"]= $this->input->post("constract_drawing8blueetext");
            $data["constract_drawing9bluee"]= $this->input->post("constract_drawing9blueetext");
            $data["constract_drawing10bluee"]= $this->input->post("constract_drawing10blueetext");


            $data["date_insert_drawing1bluee"] = date('Y-m-d');
            $data["date_insert_drawing2bluee"] = date('Y-m-d');
            $data["date_insert_drawing3bluee"] = date('Y-m-d');
            $data["date_insert_drawing4bluee"] = date('Y-m-d');
            $data["date_insert_drawing5bluee"] = date('Y-m-d');
            $data["date_insert_drawing6bluee"] = date('Y-m-d');
            $data["date_insert_drawing7bluee"] = date('Y-m-d');
            $data["date_insert_drawing8bluee"] = date('Y-m-d');
            $data["date_insert_drawing9bluee"] = date('Y-m-d');
            $data["date_insert_drawing10bluee"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1blueftext")) || !empty($this->input->post("constract_drawing2blueftext")) || !empty($this->input->post("constract_drawing3blueftext")) || !empty($this->input->post("constract_drawing4blueftext")) || !empty($this->input->post("constract_drawing5blueftext")) || !empty($this->input->post("constract_drawing6blueftext")) || !empty($this->input->post("constract_drawing7blueftext")) || !empty($this->input->post("constract_drawing8blueftext")) || !empty($this->input->post("constract_drawing9blueftext")) || !empty($this->input->post("constract_drawing10blueftext")) || !empty($this->input->post("desc_item1bluef")) || !empty($this->input->post("co_owner1bluef")) || !empty($this->input->post("desc_item2bluef")) || !empty($this->input->post("co_owner2bluef")) || !empty($this->input->post("desc_item3bluef")) || !empty($this->input->post("co_owner3bluef")) || !empty($this->input->post("desc_item4bluef")) || !empty($this->input->post("co_owner4bluef")) || !empty($this->input->post("desc_item5bluef")) || !empty($this->input->post("co_owner5bluef")) || !empty($this->input->post("desc_item6bluef")) || !empty($this->input->post("co_owner6bluef")) || !empty($this->input->post("desc_item7bluef")) || !empty($this->input->post("co_owner7bluef")) || !empty($this->input->post("desc_item8bluef")) || !empty($this->input->post("co_owner8bluef")) || !empty($this->input->post("desc_item9bluef")) || !empty($this->input->post("co_owner9bluef")) || !empty($this->input->post("desc_item10bluef")) || !empty($this->input->post("co_owner10bluef")))
        {

            $data["check1bluef"] = $this->input->post("check1bluef");
            $data["desc_item1bluef"] = mb_convert_kana($this->input->post("desc_item1bluef"), "as");
            $data["check2bluef"] = $this->input->post("check2bluef");
            $data["desc_item2bluef"] = mb_convert_kana($this->input->post("desc_item2bluef"), "as");
            $data["check3bluef"] = $this->input->post("check3bluef");
            $data["desc_item3bluef"] = mb_convert_kana($this->input->post("desc_item3bluef"), "as");
            $data["check4bluef"] = $this->input->post("check4bluef");
            $data["desc_item4bluef"] = mb_convert_kana($this->input->post("desc_item4bluef"), "as");
            $data["check5bluef"] = $this->input->post("check5bluef");
            $data["desc_item5bluef"] = mb_convert_kana($this->input->post("desc_item5bluef"), "as");
            $data["check6bluef"] = $this->input->post("check6bluef");
            $data["desc_item6bluef"] = mb_convert_kana($this->input->post("desc_item6bluef"), "as");
            $data["check7bluef"] = $this->input->post("check7bluef");
            $data["desc_item7bluef"] = mb_convert_kana($this->input->post("desc_item7bluef"), "as");
            $data["check8bluef"] = $this->input->post("check8bluef");
            $data["desc_item8bluef"] = mb_convert_kana($this->input->post("desc_item8bluef"), "as");
            $data["check9bluef"] = $this->input->post("check9bluef");
            $data["desc_item9bluef"] = mb_convert_kana($this->input->post("desc_item9bluef"), "as");
            $data["check10bluef"] = $this->input->post("check10bluef");
            $data["desc_item10bluef"] = mb_convert_kana($this->input->post("desc_item10bluef"), "as");
            $data["category_name1bluef"] = "設備プランシート";
            $data["type_item"] = "user";


            if (!empty($this->input->post("co_owner1bluef")))
            {
                $data["co_owner1bluef"] = implode(";", $this->input->post("co_owner1bluef"));
            }
            else
            {
                $data["co_owner1bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner2bluef")))
            {
                $data["co_owner2bluef"] = implode(";", $this->input->post("co_owner2bluef"));
            }
            else
            {
                $data["co_owner2bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner3bluef")))
            {
                $data["co_owner3bluef"] = implode(";", $this->input->post("co_owner3bluef"));
            }
            else
            {
                $data["co_owner3bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner4bluef")))
            {
                $data["co_owner4bluef"] = implode(";", $this->input->post("co_owner4bluef"));
            }
            else
            {
                $data["co_owner4bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner5bluef")))
            {
                $data["co_owner5bluef"] = implode(";", $this->input->post("co_owner5bluef"));
            }
            else
            {
                $data["co_owner5bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner6bluef")))
            {
                $data["co_owner6bluef"] = implode(";", $this->input->post("co_owner6bluef"));
            }
            else
            {
                $data["co_owner6bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner7bluef")))
            {
                $data["co_owner7bluef"] = implode(";", $this->input->post("co_owner7bluef"));
            }
            else
            {
                $data["co_owner7bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner8bluef")))
            {
                $data["co_owner8bluef"] = implode(";", $this->input->post("co_owner8bluef"));
            }
            else
            {
                $data["co_owner8bluef"] = '';
            }
            
            if (!empty($this->input->post("co_owner9bluef")))
            {
                $data["co_owner9bluef"] = implode(";", $this->input->post("co_owner9bluef"));
            }
            else
            {
                $data["co_owner9bluef"] = '';
            }

            if (!empty($this->input->post("co_owner10bluef")))
            {
                $data["co_owner10bluef"] = implode(";", $this->input->post("co_owner10bluef"));
            }
            else
            {
                $data["co_owner10bluef"] = '';
            }


            $data["constract_drawing1bluef"]= $this->input->post("constract_drawing1blueftext");
            $data["constract_drawing2bluef"]= $this->input->post("constract_drawing2blueftext");
            $data["constract_drawing3bluef"]= $this->input->post("constract_drawing3blueftext");
            $data["constract_drawing4bluef"]= $this->input->post("constract_drawing4blueftext");
            $data["constract_drawing5bluef"]= $this->input->post("constract_drawing5blueftext");
            $data["constract_drawing6bluef"]= $this->input->post("constract_drawing6blueftext");
            $data["constract_drawing7bluef"]= $this->input->post("constract_drawing7blueftext");
            $data["constract_drawing8bluef"]= $this->input->post("constract_drawing8blueftext");
            $data["constract_drawing9bluef"]= $this->input->post("constract_drawing9blueftext");
            $data["constract_drawing10bluef"]= $this->input->post("constract_drawing10blueftext");


            $data["date_insert_drawing1bluef"] = date('Y-m-d');
            $data["date_insert_drawing2bluef"] = date('Y-m-d');
            $data["date_insert_drawing3bluef"] = date('Y-m-d');
            $data["date_insert_drawing4bluef"] = date('Y-m-d');
            $data["date_insert_drawing5bluef"] = date('Y-m-d');
            $data["date_insert_drawing6bluef"] = date('Y-m-d');
            $data["date_insert_drawing7bluef"] = date('Y-m-d');
            $data["date_insert_drawing8bluef"] = date('Y-m-d');
            $data["date_insert_drawing9bluef"] = date('Y-m-d');
            $data["date_insert_drawing10bluef"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1bluegtext")) || !empty($this->input->post("constract_drawing2bluegtext")) || !empty($this->input->post("constract_drawing3bluegtext")) || !empty($this->input->post("constract_drawing4bluegtext")) || !empty($this->input->post("constract_drawing5bluegtext")) || !empty($this->input->post("constract_drawing6bluegtext")) || !empty($this->input->post("constract_drawing7bluegtext")) || !empty($this->input->post("constract_drawing8bluegtext")) || !empty($this->input->post("constract_drawing9bluegtext")) || !empty($this->input->post("constract_drawing10bluegtext")) || !empty($this->input->post("desc_item1blueg")) || !empty($this->input->post("co_owner1blueg")) || !empty($this->input->post("desc_item2blueg")) || !empty($this->input->post("co_owner2blueg")) || !empty($this->input->post("desc_item3blueg")) || !empty($this->input->post("co_owner3blueg")) || !empty($this->input->post("desc_item4blueg")) || !empty($this->input->post("co_owner4blueg")) || !empty($this->input->post("desc_item5blueg")) || !empty($this->input->post("co_owner5blueg")) || !empty($this->input->post("desc_item6blueg")) || !empty($this->input->post("co_owner6blueg")) || !empty($this->input->post("desc_item7blueg")) || !empty($this->input->post("co_owner7blueg")) || !empty($this->input->post("desc_item8blueg")) || !empty($this->input->post("co_owner8blueg")) || !empty($this->input->post("desc_item9blueg")) || !empty($this->input->post("co_owner9blueg")) || !empty($this->input->post("desc_item10blueg")) || !empty($this->input->post("co_owner10blueg")))
        {

            $data["check1blueg"] = $this->input->post("check1blueg");
            $data["desc_item1blueg"] = mb_convert_kana($this->input->post("desc_item1blueg"), "as");
            $data["check2blueg"] = $this->input->post("check2blueg");
            $data["desc_item2blueg"] = mb_convert_kana($this->input->post("desc_item2blueg"), "as");
            $data["check3blueg"] = $this->input->post("check3blueg");
            $data["desc_item3blueg"] = mb_convert_kana($this->input->post("desc_item3blueg"), "as");
            $data["check4blueg"] = $this->input->post("check4blueg");
            $data["desc_item4blueg"] = mb_convert_kana($this->input->post("desc_item4blueg"), "as");
            $data["check5blueg"] = $this->input->post("check5blueg");
            $data["desc_item5blueg"] = mb_convert_kana($this->input->post("desc_item5blueg"), "as");
            $data["check6blueg"] = $this->input->post("check6blueg");
            $data["desc_item6blueg"] = mb_convert_kana($this->input->post("desc_item6blueg"), "as");
            $data["check7blueg"] = $this->input->post("check7blueg");
            $data["desc_item7blueg"] = mb_convert_kana($this->input->post("desc_item7blueg"), "as");
            $data["check8blueg"] = $this->input->post("check8blueg");
            $data["desc_item8blueg"] = mb_convert_kana($this->input->post("desc_item8blueg"), "as");
            $data["check9blueg"] = $this->input->post("check9blueg");
            $data["desc_item9blueg"] = mb_convert_kana($this->input->post("desc_item9blueg"), "as");
            $data["check10blueg"] = $this->input->post("check10blueg");
            $data["desc_item10blueg"] = mb_convert_kana($this->input->post("desc_item10blueg"), "as");
            $data["category_name1blueg"] = "その他";
            $data["type_item"] = "user";

            if (!empty($this->input->post("co_owner1blueg")))
            {
                $data["co_owner1blueg"] = implode(";", $this->input->post("co_owner1blueg"));
            }
            else
            {
                $data["co_owner1blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner2blueg")))
            {
                $data["co_owner2blueg"] = implode(";", $this->input->post("co_owner2blueg"));
            }
            else
            {
                $data["co_owner2blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner3blueg")))
            {
                $data["co_owner3blueg"] = implode(";", $this->input->post("co_owner3blueg"));
            }
            else
            {
                $data["co_owner3blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner4blueg")))
            {
                $data["co_owner4blueg"] = implode(";", $this->input->post("co_owner4blueg"));
            }
            else
            {
                $data["co_owner4blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner5blueg")))
            {
                $data["co_owner5blueg"] = implode(";", $this->input->post("co_owner5blueg"));
            }
            else
            {
                $data["co_owner5blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner6blueg")))
            {
                $data["co_owner6blueg"] = implode(";", $this->input->post("co_owner6blueg"));
            }
            else
            {
                $data["co_owner6blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner7blueg")))
            {
                $data["co_owner7blueg"] = implode(";", $this->input->post("co_owner7blueg"));
            }
            else
            {
                $data["co_owner7blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner8blueg")))
            {
                $data["co_owner8blueg"] = implode(";", $this->input->post("co_owner8blueg"));
            }
            else
            {
                $data["co_owner8blueg"] = '';
            }
            
            if (!empty($this->input->post("co_owner9blueg")))
            {
                $data["co_owner9blueg"] = implode(";", $this->input->post("co_owner9blueg"));
            }
            else
            {
                $data["co_owner9blueg"] = '';
            }

            if (!empty($this->input->post("co_owner10blueg")))
            {
                $data["co_owner10blueg"] = implode(";", $this->input->post("co_owner10blueg"));
            }
            else
            {
                $data["co_owner10blueg"] = '';
            }

            $data["constract_drawing1blueg"]= $this->input->post("constract_drawing1bluegtext");
            $data["constract_drawing2blueg"]= $this->input->post("constract_drawing2bluegtext");
            $data["constract_drawing3blueg"]= $this->input->post("constract_drawing3bluegtext");
            $data["constract_drawing4blueg"]= $this->input->post("constract_drawing4bluegtext");
            $data["constract_drawing5blueg"]= $this->input->post("constract_drawing5bluegtext");
            $data["constract_drawing6blueg"]= $this->input->post("constract_drawing6bluegtext");
            $data["constract_drawing7blueg"]= $this->input->post("constract_drawing7bluegtext");
            $data["constract_drawing8blueg"]= $this->input->post("constract_drawing8bluegtext");
            $data["constract_drawing9blueg"]= $this->input->post("constract_drawing9bluegtext");
            $data["constract_drawing10blueg"]= $this->input->post("constract_drawing10bluegtext");


            $data["date_insert_drawing1blueg"] = date('Y-m-d');
            $data["date_insert_drawing2blueg"] = date('Y-m-d');
            $data["date_insert_drawing3blueg"] = date('Y-m-d');
            $data["date_insert_drawing4blueg"] = date('Y-m-d');
            $data["date_insert_drawing5blueg"] = date('Y-m-d');
            $data["date_insert_drawing6blueg"] = date('Y-m-d');
            $data["date_insert_drawing7blueg"] = date('Y-m-d');
            $data["date_insert_drawing8blueg"] = date('Y-m-d');
            $data["date_insert_drawing9blueg"] = date('Y-m-d');
            $data["date_insert_drawing10blueg"] = date('Y-m-d');
        }

        //admin//

        if(!empty($this->input->post("constract_drawing1text")) || !empty($this->input->post("constract_drawing2text")) || !empty($this->input->post("constract_drawing3text")) || !empty($this->input->post("constract_drawing4text")) || !empty($this->input->post("constract_drawing5text")) || !empty($this->input->post("constract_drawing6text")) || !empty($this->input->post("constract_drawing7text")) || !empty($this->input->post("constract_drawing8text")) || !empty($this->input->post("constract_drawing9text")) || !empty($this->input->post("constract_drawing10text")) || !empty($this->input->post("desc_item1")) || !empty($this->input->post("desc_item2")) || !empty($this->input->post("desc_item3")) || !empty($this->input->post("desc_item4")) || !empty($this->input->post("desc_item5")) || !empty($this->input->post("desc_item6")) || !empty($this->input->post("desc_item7")) || !empty($this->input->post("desc_item8")) || !empty($this->input->post("desc_item9")) || !empty($this->input->post("desc_item10")))
        {

            $data["check1"] = $this->input->post("check1");
            $data["desc_item1"] = mb_convert_kana($this->input->post("desc_item1"), "as");
            $data["check2"] = $this->input->post("check2");
            $data["desc_item2"] = mb_convert_kana($this->input->post("desc_item2"), "as");
            $data["check3"] = $this->input->post("check3");
            $data["desc_item3"] = mb_convert_kana($this->input->post("desc_item3"), "as");
            $data["check4"] = $this->input->post("check4");
            $data["desc_item4"] = mb_convert_kana($this->input->post("desc_item4"), "as");
            $data["check5"] = $this->input->post("check5");
            $data["desc_item5"] = mb_convert_kana($this->input->post("desc_item5"), "as");
            $data["check6"] = $this->input->post("check6");
            $data["desc_item6"] = mb_convert_kana($this->input->post("desc_item6"), "as");
            $data["check7"] = $this->input->post("check7");
            $data["desc_item7"] = mb_convert_kana($this->input->post("desc_item7"), "as");
            $data["check8"] = $this->input->post("check8");
            $data["desc_item8"] = mb_convert_kana($this->input->post("desc_item8"), "as");
            $data["check9"] = $this->input->post("check9");
            $data["desc_item9"] = mb_convert_kana($this->input->post("desc_item9"), "as");
            $data["check10"] = $this->input->post("check10");
            $data["desc_item10"] = mb_convert_kana($this->input->post("desc_item10"), "as");
            $data["category_name"] = "請負契約書・土地契約書";
            // $data["type_item"] = "admin";

            $data["constract_drawing1"]= $this->input->post("constract_drawing1text");
            $data["constract_drawing2"]= $this->input->post("constract_drawing2text");
            $data["constract_drawing3"]= $this->input->post("constract_drawing3text");
            $data["constract_drawing4"]= $this->input->post("constract_drawing4text");
            $data["constract_drawing5"]= $this->input->post("constract_drawing5text");
            $data["constract_drawing6"]= $this->input->post("constract_drawing6text");
            $data["constract_drawing7"]= $this->input->post("constract_drawing7text");
            $data["constract_drawing8"]= $this->input->post("constract_drawing8text");
            $data["constract_drawing9"]= $this->input->post("constract_drawing9text");
            $data["constract_drawing10"]= $this->input->post("constract_drawing10text");


            $data["date_insert_drawing1"] = date('Y-m-d');
            $data["date_insert_drawing2"] = date('Y-m-d');
            $data["date_insert_drawing3"] = date('Y-m-d');
            $data["date_insert_drawing4"] = date('Y-m-d');
            $data["date_insert_drawing5"] = date('Y-m-d');
            $data["date_insert_drawing6"] = date('Y-m-d');
            $data["date_insert_drawing7"] = date('Y-m-d');
            $data["date_insert_drawing8"] = date('Y-m-d');
            $data["date_insert_drawing9"] = date('Y-m-d');
            $data["date_insert_drawing10"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1btext")) || !empty($this->input->post("constract_drawing2btext")) || !empty($this->input->post("constract_drawing3btext")) || !empty($this->input->post("constract_drawing4btext")) || !empty($this->input->post("constract_drawing5btext")) || !empty($this->input->post("constract_drawing6btext")) || !empty($this->input->post("constract_drawing7btext")) || !empty($this->input->post("constract_drawing8btext")) || !empty($this->input->post("constract_drawing9btext")) || !empty($this->input->post("constract_drawing10btext")) || !empty($this->input->post("desc_item1b")) || !empty($this->input->post("desc_item2b")) || !empty($this->input->post("desc_item3b")) || !empty($this->input->post("desc_item4b")) || !empty($this->input->post("desc_item5b")) || !empty($this->input->post("desc_item6b")) || !empty($this->input->post("desc_item7b")) || !empty($this->input->post("desc_item8b")) || !empty($this->input->post("desc_item9b")) || !empty($this->input->post("desc_item10b")))
        {

            $data["check1b"] = $this->input->post("check1b");
            $data["desc_item1b"] = mb_convert_kana($this->input->post("desc_item1b"), "as");
            $data["check2b"] = $this->input->post("check2b");
            $data["desc_item2b"] = mb_convert_kana($this->input->post("desc_item2b"), "as");
            $data["check3b"] = $this->input->post("check3b");
            $data["desc_item3b"] = mb_convert_kana($this->input->post("desc_item3b"), "as");
            $data["check4b"] = $this->input->post("check4b");
            $data["desc_item4b"] = mb_convert_kana($this->input->post("desc_item4b"), "as");
            $data["check5b"] = $this->input->post("check5b");
            $data["desc_item5b"] = mb_convert_kana($this->input->post("desc_item5b"), "as");
            $data["check6b"] = $this->input->post("check6b");
            $data["desc_item6b"] = mb_convert_kana($this->input->post("desc_item6b"), "as");
            $data["check7b"] = $this->input->post("check7b");
            $data["desc_item7b"] = mb_convert_kana($this->input->post("desc_item7b"), "as");
            $data["check8b"] = $this->input->post("check8b");
            $data["desc_item8b"] = mb_convert_kana($this->input->post("desc_item8b"), "as");
            $data["check9b"] = $this->input->post("check9b");
            $data["desc_item9b"] = mb_convert_kana($this->input->post("desc_item9b"), "as");
            $data["check10b"] = $this->input->post("check10b");
            $data["desc_item10b"] = mb_convert_kana($this->input->post("desc_item10b"), "as");
            $data["category_name1b"] = "長期優良住宅認定書・性能評価書";
            // $data["type_item"] = "admin";

            $data["constract_drawing1b"]= $this->input->post("constract_drawing1btext");
            $data["constract_drawing2b"]= $this->input->post("constract_drawing2btext");
            $data["constract_drawing3b"]= $this->input->post("constract_drawing3btext");
            $data["constract_drawing4b"]= $this->input->post("constract_drawing4btext");
            $data["constract_drawing5b"]= $this->input->post("constract_drawing5btext");
            $data["constract_drawing6b"]= $this->input->post("constract_drawing6btext");
            $data["constract_drawing7b"]= $this->input->post("constract_drawing7btext");
            $data["constract_drawing8b"]= $this->input->post("constract_drawing8btext");
            $data["constract_drawing9b"]= $this->input->post("constract_drawing9btext");
            $data["constract_drawing10b"]= $this->input->post("constract_drawing10btext");


            $data["date_insert_drawing1b"] = date('Y-m-d');
            $data["date_insert_drawing2b"] = date('Y-m-d');
            $data["date_insert_drawing3b"] = date('Y-m-d');
            $data["date_insert_drawing4b"] = date('Y-m-d');
            $data["date_insert_drawing5b"] = date('Y-m-d');
            $data["date_insert_drawing6b"] = date('Y-m-d');
            $data["date_insert_drawing7b"] = date('Y-m-d');
            $data["date_insert_drawing8b"] = date('Y-m-d');
            $data["date_insert_drawing9b"] = date('Y-m-d');
            $data["date_insert_drawing10b"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ctext")) || !empty($this->input->post("constract_drawing2ctext")) || !empty($this->input->post("constract_drawing3ctext")) || !empty($this->input->post("constract_drawing4ctext")) || !empty($this->input->post("constract_drawing5ctext")) || !empty($this->input->post("constract_drawing6ctext")) || !empty($this->input->post("constract_drawing7ctext")) || !empty($this->input->post("constract_drawing8ctext")) || !empty($this->input->post("constract_drawing9ctext")) || !empty($this->input->post("constract_drawing10ctext")) || !empty($this->input->post("desc_item1c")) || !empty($this->input->post("desc_item2c")) || !empty($this->input->post("desc_item3c")) || !empty($this->input->post("desc_item4c")) || !empty($this->input->post("desc_item5c")) || !empty($this->input->post("desc_item6c")) || !empty($this->input->post("desc_item7c")) || !empty($this->input->post("desc_item8c")) || !empty($this->input->post("desc_item9c")) || !empty($this->input->post("desc_item10c")))
        {

            $data["check1c"] = $this->input->post("check1c");
            $data["desc_item1c"] = mb_convert_kana($this->input->post("desc_item1c"), "as");
            $data["check2c"] = $this->input->post("check2c");
            $data["desc_item2c"] = mb_convert_kana($this->input->post("desc_item2c"), "as");
            $data["check3c"] = $this->input->post("check3c");
            $data["desc_item3c"] = mb_convert_kana($this->input->post("desc_item3c"), "as");
            $data["check4c"] = $this->input->post("check4c");
            $data["desc_item4c"] = mb_convert_kana($this->input->post("desc_item4c"), "as");
            $data["check5c"] = $this->input->post("check5c");
            $data["desc_item5c"] = mb_convert_kana($this->input->post("desc_item5c"), "as");
            $data["check6c"] = $this->input->post("check6c");
            $data["desc_item6c"] = mb_convert_kana($this->input->post("desc_item6c"), "as");
            $data["check7c"] = $this->input->post("check7c");
            $data["desc_item7c"] = mb_convert_kana($this->input->post("desc_item7c"), "as");
            $data["check8c"] = $this->input->post("check8c");
            $data["desc_item8c"] = mb_convert_kana($this->input->post("desc_item8c"), "as");
            $data["check9c"] = $this->input->post("check9c");
            $data["desc_item9c"] = mb_convert_kana($this->input->post("desc_item9c"), "as");
            $data["check10c"] = $this->input->post("check10c");
            $data["desc_item10c"] = mb_convert_kana($this->input->post("desc_item10c"), "as");
            $data["category_name1c"] = "地盤調査・地盤改良";
            // $data["type_item"] = "admin";

            $data["constract_drawing1c"]= $this->input->post("constract_drawing1ctext");
            $data["constract_drawing2c"]= $this->input->post("constract_drawing2ctext");
            $data["constract_drawing3c"]= $this->input->post("constract_drawing3ctext");
            $data["constract_drawing4c"]= $this->input->post("constract_drawing4ctext");
            $data["constract_drawing5c"]= $this->input->post("constract_drawing5ctext");
            $data["constract_drawing6c"]= $this->input->post("constract_drawing6ctext");
            $data["constract_drawing7c"]= $this->input->post("constract_drawing7ctext");
            $data["constract_drawing8c"]= $this->input->post("constract_drawing8ctext");
            $data["constract_drawing9c"]= $this->input->post("constract_drawing9ctext");
            $data["constract_drawing10c"]= $this->input->post("constract_drawing10ctext");


            $data["date_insert_drawing1c"] = date('Y-m-d');
            $data["date_insert_drawing2c"] = date('Y-m-d');
            $data["date_insert_drawing3c"] = date('Y-m-d');
            $data["date_insert_drawing4c"] = date('Y-m-d');
            $data["date_insert_drawing5c"] = date('Y-m-d');
            $data["date_insert_drawing6c"] = date('Y-m-d');
            $data["date_insert_drawing7c"] = date('Y-m-d');
            $data["date_insert_drawing8c"] = date('Y-m-d');
            $data["date_insert_drawing9c"] = date('Y-m-d');
            $data["date_insert_drawing10c"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1dtext")) || !empty($this->input->post("constract_drawing2dtext")) || !empty($this->input->post("constract_drawing3dtext")) || !empty($this->input->post("constract_drawing4dtext")) || !empty($this->input->post("constract_drawing5dtext")) || !empty($this->input->post("constract_drawing6dtext")) || !empty($this->input->post("constract_drawing7dtext")) || !empty($this->input->post("constract_drawing8dtext")) || !empty($this->input->post("constract_drawing9dtext")) || !empty($this->input->post("constract_drawing10dtext")) || !empty($this->input->post("desc_item1d")) || !empty($this->input->post("desc_item2d")) || !empty($this->input->post("desc_item3d")) || !empty($this->input->post("desc_item4d")) || !empty($this->input->post("desc_item5d")) || !empty($this->input->post("desc_item6d")) || !empty($this->input->post("desc_item7d")) || !empty($this->input->post("desc_item8d")) || !empty($this->input->post("desc_item9d")) || !empty($this->input->post("desc_item10d")))
        {

            $data["check1d"] = $this->input->post("check1d");
            $data["desc_item1d"] = mb_convert_kana($this->input->post("desc_item1d"), "as");
            $data["check2d"] = $this->input->post("check2d");
            $data["desc_item2d"] = mb_convert_kana($this->input->post("desc_item2d"), "as");
            $data["check3d"] = $this->input->post("check3d");
            $data["desc_item3d"] = mb_convert_kana($this->input->post("desc_item3d"), "as");
            $data["check4d"] = $this->input->post("check4d");
            $data["desc_item4d"] = mb_convert_kana($this->input->post("desc_item4d"), "as");
            $data["check5d"] = $this->input->post("check5d");
            $data["desc_item5d"] = mb_convert_kana($this->input->post("desc_item5d"), "as");
            $data["check6d"] = $this->input->post("check6d");
            $data["desc_item6d"] = mb_convert_kana($this->input->post("desc_item6d"), "as");
            $data["check7d"] = $this->input->post("check7d");
            $data["desc_item7d"] = mb_convert_kana($this->input->post("desc_item7d"), "as");
            $data["check8d"] = $this->input->post("check8d");
            $data["desc_item8d"] = mb_convert_kana($this->input->post("desc_item8d"), "as");
            $data["check9d"] = $this->input->post("check9d");
            $data["desc_item9d"] = mb_convert_kana($this->input->post("desc_item9d"), "as");
            $data["check10d"] = $this->input->post("check10d");
            $data["desc_item10d"] = mb_convert_kana($this->input->post("desc_item10d"), "as");
            $data["category_name1d"] = "点検履歴";
            // $data["type_item"] = "admin";

            $data["constract_drawing1d"]= $this->input->post("constract_drawing1dtext");
            $data["constract_drawing2d"]= $this->input->post("constract_drawing2dtext");
            $data["constract_drawing3d"]= $this->input->post("constract_drawing3dtext");
            $data["constract_drawing4d"]= $this->input->post("constract_drawing4dtext");
            $data["constract_drawing5d"]= $this->input->post("constract_drawing5dtext");
            $data["constract_drawing6d"]= $this->input->post("constract_drawing6dtext");
            $data["constract_drawing7d"]= $this->input->post("constract_drawing7dtext");
            $data["constract_drawing8d"]= $this->input->post("constract_drawing8dtext");
            $data["constract_drawing9d"]= $this->input->post("constract_drawing9dtext");
            $data["constract_drawing10d"]= $this->input->post("constract_drawing10dtext");


            $data["date_insert_drawing1d"] = date('Y-m-d');
            $data["date_insert_drawing2d"] = date('Y-m-d');
            $data["date_insert_drawing3d"] = date('Y-m-d');
            $data["date_insert_drawing4d"] = date('Y-m-d');
            $data["date_insert_drawing5d"] = date('Y-m-d');
            $data["date_insert_drawing6d"] = date('Y-m-d');
            $data["date_insert_drawing7d"] = date('Y-m-d');
            $data["date_insert_drawing8d"] = date('Y-m-d');
            $data["date_insert_drawing9d"] = date('Y-m-d');
            $data["date_insert_drawing10d"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1etext")) || !empty($this->input->post("constract_drawing3etext")) || !empty($this->input->post("constract_drawing4etext")) || !empty($this->input->post("constract_drawing4etext")) || !empty($this->input->post("constract_drawing5etext")) || !empty($this->input->post("constract_drawing6etext")) || !empty($this->input->post("constract_drawing7etext")) || !empty($this->input->post("constract_drawing8etext")) || !empty($this->input->post("constract_drawing9etext")) || !empty($this->input->post("constract_drawing10etext")) || !empty($this->input->post("desc_item1e")) || !empty($this->input->post("desc_item2e")) || !empty($this->input->post("desc_item3e")) || !empty($this->input->post("desc_item4e")) || !empty($this->input->post("desc_item5e")) || !empty($this->input->post("desc_item6e")) || !empty($this->input->post("desc_item7e")) || !empty($this->input->post("desc_item8e")) || !empty($this->input->post("desc_item9e")) || !empty($this->input->post("desc_item10e")))
        {


            $data["check1e"] = $this->input->post("check1e");
            $data["desc_item1e"] = mb_convert_kana($this->input->post("desc_item1e"), "as");
            $data["check2e"] = $this->input->post("check2e");
            $data["desc_item2e"] = mb_convert_kana($this->input->post("desc_item2e"), "as");
            $data["check3e"] = $this->input->post("check3e");
            $data["desc_item3e"] = mb_convert_kana($this->input->post("desc_item3e"), "as");
            $data["check4e"] = $this->input->post("check4e");
            $data["desc_item4e"] = mb_convert_kana($this->input->post("desc_item4e"), "as");
            $data["check5e"] = $this->input->post("check5e");
            $data["desc_item5e"] = mb_convert_kana($this->input->post("desc_item5e"), "as");
            $data["check6e"] = $this->input->post("check6e");
            $data["desc_item6e"] = mb_convert_kana($this->input->post("desc_item6e"), "as");
            $data["check7e"] = $this->input->post("check7e");
            $data["desc_item7e"] = mb_convert_kana($this->input->post("desc_item7e"), "as");
            $data["check8e"] = $this->input->post("check8e");
            $data["desc_item8e"] = mb_convert_kana($this->input->post("desc_item8e"), "as");
            $data["check9e"] = $this->input->post("check9e");
            $data["desc_item9e"] = mb_convert_kana($this->input->post("desc_item9e"), "as");
            $data["check10e"] = $this->input->post("check10e");
            $data["desc_item10e"] = mb_convert_kana($this->input->post("desc_item10e"), "as");
            $data["category_name1e"] = "三者引継ぎ合意書";
            // $data["type_item"] = "admin";

            $data["constract_drawing1e"]= $this->input->post("constract_drawing1etext");
            $data["constract_drawing2e"]= $this->input->post("constract_drawing2etext");
            $data["constract_drawing3e"]= $this->input->post("constract_drawing3etext");
            $data["constract_drawing4e"]= $this->input->post("constract_drawing4etext");
            $data["constract_drawing5e"]= $this->input->post("constract_drawing5etext");
            $data["constract_drawing6e"]= $this->input->post("constract_drawing6etext");
            $data["constract_drawing7e"]= $this->input->post("constract_drawing7etext");
            $data["constract_drawing8e"]= $this->input->post("constract_drawing8etext");
            $data["constract_drawing9e"]= $this->input->post("constract_drawing9etext");
            $data["constract_drawing10e"]= $this->input->post("constract_drawing10etext");


            $data["date_insert_drawing1e"] = date('Y-m-d');
            $data["date_insert_drawing2e"] = date('Y-m-d');
            $data["date_insert_drawing3e"] = date('Y-m-d');
            $data["date_insert_drawing4e"] = date('Y-m-d');
            $data["date_insert_drawing5e"] = date('Y-m-d');
            $data["date_insert_drawing6e"] = date('Y-m-d');
            $data["date_insert_drawing7e"] = date('Y-m-d');
            $data["date_insert_drawing8e"] = date('Y-m-d');
            $data["date_insert_drawing9e"] = date('Y-m-d');
            $data["date_insert_drawing10e"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1ftext")) || !empty($this->input->post("constract_drawing2ftext")) || !empty($this->input->post("constract_drawing3ftext")) || !empty($this->input->post("constract_drawing4ftext")) || !empty($this->input->post("constract_drawing5ftext")) || !empty($this->input->post("constract_drawing6ftext")) || !empty($this->input->post("constract_drawing7ftext")) || !empty($this->input->post("constract_drawing8ftext")) || !empty($this->input->post("constract_drawing9ftext")) || !empty($this->input->post("constract_drawing10ftext")) || !empty($this->input->post("desc_item1f")) || !empty($this->input->post("desc_item2f")) || !empty($this->input->post("desc_item3f")) || !empty($this->input->post("desc_item4f")) || !empty($this->input->post("desc_item5f")) || !empty($this->input->post("desc_item6f")) || !empty($this->input->post("desc_item7f")) || !empty($this->input->post("desc_item8f")) || !empty($this->input->post("desc_item9f")) || !empty($this->input->post("desc_item10f")))
        {

            $data["check1f"] = $this->input->post("check1f");
            $data["desc_item1f"] = mb_convert_kana($this->input->post("desc_item1f"), "as");
            $data["check2f"] = $this->input->post("check2f");
            $data["desc_item2f"] = mb_convert_kana($this->input->post("desc_item2f"), "as");
            $data["check3f"] = $this->input->post("check3f");
            $data["desc_item3f"] = mb_convert_kana($this->input->post("desc_item3f"), "as");
            $data["check4f"] = $this->input->post("check4f");
            $data["desc_item4f"] = mb_convert_kana($this->input->post("desc_item4f"), "as");
            $data["check5f"] = $this->input->post("check5f");
            $data["desc_item5f"] = mb_convert_kana($this->input->post("desc_item5f"), "as");
            $data["check6f"] = $this->input->post("check6f");
            $data["desc_item6f"] = mb_convert_kana($this->input->post("desc_item6f"), "as");
            $data["check7f"] = $this->input->post("check7f");
            $data["desc_item7f"] = mb_convert_kana($this->input->post("desc_item7f"), "as");
            $data["check8f"] = $this->input->post("check8f");
            $data["desc_item8f"] = mb_convert_kana($this->input->post("desc_item8f"), "as");
            $data["check9f"] = $this->input->post("check9f");
            $data["desc_item9f"] = mb_convert_kana($this->input->post("desc_item9f"), "as");
            $data["check10f"] = $this->input->post("check10f");
            $data["desc_item10f"] = mb_convert_kana($this->input->post("desc_item10f"), "as");
            $data["category_name1f"] = "住宅仕様書・プレカット資料";
            // $data["type_item"] = "admin";

            $data["constract_drawing1f"]= $this->input->post("constract_drawing1ftext");
            $data["constract_drawing2f"]= $this->input->post("constract_drawing2ftext");
            $data["constract_drawing3f"]= $this->input->post("constract_drawing3ftext");
            $data["constract_drawing4f"]= $this->input->post("constract_drawing4ftext");
            $data["constract_drawing5f"]= $this->input->post("constract_drawing5ftext");
            $data["constract_drawing6f"]= $this->input->post("constract_drawing6ftext");
            $data["constract_drawing7f"]= $this->input->post("constract_drawing7ftext");
            $data["constract_drawing8f"]= $this->input->post("constract_drawing8ftext");
            $data["constract_drawing9f"]= $this->input->post("constract_drawing9ftext");
            $data["constract_drawing10f"]= $this->input->post("constract_drawing10ftext");


            $data["date_insert_drawing1f"] = date('Y-m-d');
            $data["date_insert_drawing2f"] = date('Y-m-d');
            $data["date_insert_drawing3f"] = date('Y-m-d');
            $data["date_insert_drawing4f"] = date('Y-m-d');
            $data["date_insert_drawing5f"] = date('Y-m-d');
            $data["date_insert_drawing6f"] = date('Y-m-d');
            $data["date_insert_drawing7f"] = date('Y-m-d');
            $data["date_insert_drawing8f"] = date('Y-m-d');
            $data["date_insert_drawing9f"] = date('Y-m-d');
            $data["date_insert_drawing10f"] = date('Y-m-d');
        }

        if(!empty($this->input->post("constract_drawing1gtext")) || !empty($this->input->post("constract_drawing2gtext")) || !empty($this->input->post("constract_drawing3gtext")) || !empty($this->input->post("constract_drawing4gtext")) || !empty($this->input->post("constract_drawing5gtext")) || !empty($this->input->post("constract_drawing6gtext")) ||!empty($this->input->post("constract_drawing7gtext")) || !empty($this->input->post("constract_drawing8gtext")) || !empty($this->input->post("constract_drawing9gtext")) || !empty($this->input->post("constract_drawing10gtext")) || !empty($this->input->post("desc_item1g")) || !empty($this->input->post("desc_item2g")) || !empty($this->input->post("desc_item3g")) || !empty($this->input->post("desc_item4g")) || !empty($this->input->post("desc_item5g")) || !empty($this->input->post("desc_item6g")) || !empty($this->input->post("desc_item7g")) || !empty($this->input->post("desc_item8g")) || !empty($this->input->post("desc_item9g")) || !empty($this->input->post("desc_item10g")))
        {

            $data["check1g"] = $this->input->post("check1g");
            $data["desc_item1g"] = mb_convert_kana($this->input->post("desc_item1g"), "as");
            $data["check2g"] = $this->input->post("check2g");
            $data["desc_item2g"] = mb_convert_kana($this->input->post("desc_item2g"), "as");
            $data["check3g"] = $this->input->post("check3g");
            $data["desc_item3g"] = mb_convert_kana($this->input->post("desc_item3g"), "as");
            $data["check4g"] = $this->input->post("check4g");
            $data["desc_item4g"] = mb_convert_kana($this->input->post("desc_item4g"), "as");
            $data["check5g"] = $this->input->post("check5g");
            $data["desc_item5g"] = mb_convert_kana($this->input->post("desc_item5g"), "as");
            $data["check6g"] = $this->input->post("check6g");
            $data["desc_item6g"] = mb_convert_kana($this->input->post("desc_item6g"), "as");
            $data["check7g"] = $this->input->post("check7g");
            $data["desc_item7g"] = mb_convert_kana($this->input->post("desc_item7g"), "as");
            $data["check8g"] = $this->input->post("check8g");
            $data["desc_item8g"] = mb_convert_kana($this->input->post("desc_item8g"), "as");
            $data["check9g"] = $this->input->post("check9g");
            $data["desc_item9g"] = mb_convert_kana($this->input->post("desc_item9g"), "as");
            $data["check10g"] = $this->input->post("check10g");
            $data["desc_item10g"] = mb_convert_kana($this->input->post("desc_item10g"), "as");
            $data["category_name1g"] = "工事写真";
            // $data["type_item"] = "admin";

            $data["constract_drawing1g"]= $this->input->post("constract_drawing1gtext");
            $data["constract_drawing2g"]= $this->input->post("constract_drawing2gtext");
            $data["constract_drawing3g"]= $this->input->post("constract_drawing3gtext");
            $data["constract_drawing4g"]= $this->input->post("constract_drawing4gtext");
            $data["constract_drawing5g"]= $this->input->post("constract_drawing5gtext");
            $data["constract_drawing6g"]= $this->input->post("constract_drawing6gtext");
            $data["constract_drawing7g"]= $this->input->post("constract_drawing7gtext");
            $data["constract_drawing8g"]= $this->input->post("constract_drawing8gtext");
            $data["constract_drawing9g"]= $this->input->post("constract_drawing9gtext");
            $data["constract_drawing10g"]= $this->input->post("constract_drawing10gtext");


            $data["date_insert_drawing1g"] = date('Y-m-d');
            $data["date_insert_drawing2g"] = date('Y-m-d');
            $data["date_insert_drawing3g"] = date('Y-m-d');
            $data["date_insert_drawing4g"] = date('Y-m-d');
            $data["date_insert_drawing5g"] = date('Y-m-d');
            $data["date_insert_drawing6g"] = date('Y-m-d');
            $data["date_insert_drawing7g"] = date('Y-m-d');
            $data["date_insert_drawing8g"] = date('Y-m-d');
            $data["date_insert_drawing9g"] = date('Y-m-d');
            $data["date_insert_drawing10g"] = date('Y-m-d');
        }
        

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);die();

        $data = $this->security->xss_clean($data);

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->update_constructor_new($data,$id);
        if($insert)
        {
            $this->record_action("add_constructor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/constructor_list");
        }
        else
        {
            $this->session->set_flashdata('error', '工事の編集が失敗いたしました。');
            redirect("dashboard/edit_construction/",$id);
        }
    }

    //-----------------------------------------------------------------------//

    public function delete_customer($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'OB顧客の削除が失敗いたしました。');
            redirect("dashboard/customer_list");
        }
    }


    //---------------------------------------NEW-----------------------------------------//

    public function delete_customers($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_customers_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer delete was failed.');
            redirect("dashboard/customers_list");
        }
    }

    public function delete_client($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_client_new($id);
        if($delete)
        {
            
            $this->record_action("delete_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', '営業中顧客の削除が失敗いたしました。');
            redirect("dashboard/client_list");
        }
    }

    public function delete_employees($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_employee_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', '社員の削除が失敗いたしました。');
            redirect("dashboard/employees_list");
        }
    }

    public function delete_contractor($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_contractor_new($id);
        if($delete)
        {
            
            $this->record_action("delete_contractor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', '業者の削除が失敗いたしました。');
            redirect("dashboard/contractor_list");
        }
    }

    public function delete_contractor_all($id)
    {
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $delete = $this->customer->delete_one_contractor_new($id);
        $delete = $this->user->delete_one_user($id);
        if($delete)
        {
            
            $this->record_action("delete_contractor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Contractor delete was failed.');
            redirect("dashboard/contractor_list");
        }
    }

    public function delete_construction($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_construction_new($id);
        if($delete)
        {
            
            $this->record_action("delete_contractor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/constructor_list");
        }
        else
        {
            $this->session->set_flashdata('error', '工事の削除が失敗いたしました。');
            redirect("dashboard/constructor_list");
        }
    }
    
    

    //-----------------------------------------------------------------------//

    public function inspection_list()
    {
        
        $this->record_action("inspection_list","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_all();
        $this->load->view('dashboard/inspection/list_v',$data);
    }

    public function add_inspection()
    {
        
        $this->record_action("add_inspection","view");
        $this->load->view('dashboard/inspection/insert_v'); 
    }

    public function insert_inspection()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model',"inspection");
        $insert = $this->inspection->insert_inspection($data);
        if($insert)
        {
            
            $this->record_action("add_inspection","execute");
            $this->session->set_flashdata('success', 'Inpection insertion was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inpection insertion was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function edit_inspection($id)
    {
        
        $this->record_action("edit_inspection","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_one($id)[0];
        $this->load->view('dashboard/inspection/edit_v',$data);
    }

    public function update_inspection($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model', "inspection");
        $update = $this->inspection->update_inspection($data,$id);
        if($update)
        {
            
            $this->record_action("edit_inspection", "execute");
            $this->session->set_flashdata('success', 'Inspection update was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection update was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function delete_inspection($id)
    {
        $this->load->model('inspection_model', "inspection");
        $delete = $this->inspection->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_inspection","execute");
            $this->session->set_flashdata('success', 'Inspection delete was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection delete was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function partner_list()
    {
        
        $this->record_action("partner_list","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_all();
        $this->load->view('dashboard/partner/list_v',$data);
    }

    public function add_partner()
    {
        
        $this->record_action("add_partner","view");
        $this->load->view('dashboard/partner/insert_v');
    }

    public function insert_partner()
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $insert = $this->partner->insert_partner($data);
        if($insert)
        {
            
            $this->record_action("add_partner","execute");
            $this->session->set_flashdata('success', 'Partner insertion was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner insertion was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function edit_partner($id)
    {
        
        $this->record_action("edit_partner","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_one($id)[0];
        $this->load->view('dashboard/partner/edit_v',$data);
    }

    public function update_partner($id)
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $update = $this->partner->update_partner($data,$id);
        if($update)
        {
            
            $this->record_action("edit_partner", "execute");
            $this->session->set_flashdata('success', 'Partner update was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner update was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function delete_partner($id)
    {
        $this->load->model('partner_model', "partner");
        $delete = $this->partner->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_partner","execute");
            $this->session->set_flashdata('success', 'Delete Partner was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Partner was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function public_works_list()
    {
        
        $this->record_action("public_work_list","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_all();
        $this->load->view('dashboard/public_works/list_v',$data);
    }

    public function add_public_works()
    {
        
        $this->record_action("add_public_work","view");
        $this->load->view('dashboard/public_works/insert_v');
    }

    public function insert_public_works()
    {
        // print_r($_FILES);die();
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        
        $config['upload_path'] = FCPATH.'uploads/'; 
        // print_r($config['upload_path']);
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '500000'; // in KlioBytes
        $this->upload->initialize($config);
        if($_FILES['document_image']['name']!="")
        {
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }
        else
        {
            $data['document_image']= null;
        }
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        $this->load->model('public_works_model',"public_works");
        $insert = $this->public_works->insert_public_works($data);
        if($insert)
        {
            
            $this->record_action("add_public_work","execute");
            $this->session->set_flashdata('success', 'Public Works insertion was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Works insertion was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function edit_public_works($id)
    {
        
        $this->record_action("edit_public_work","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_one($id)[0];
        $this->load->view('dashboard/public_works/edit_v',$data);
    }

    public function update_public_works($id)
    {
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        
        if($_FILES['document_image']["name"] != "" && $_FILES['document_image']["size"] > 0)
        {
            $config['upload_path'] = FCPATH.'uploads/'; 
            // print_r($config['upload_path']);
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '500000'; // in KlioBytes
            $this->upload->initialize($config);
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }

        $this->load->model('public_works_model',"public_works");
        $update = $this->public_works->update_public_works($data,$id);
        if($update)
        {
            
            $this->record_action("edit_public_work","execute");
            $this->session->set_flashdata('success', 'Public Work update was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Work update was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function delete_public_works($id)
    {
        $this->load->model('public_works_model', "public_works");
        $delete = $this->public_works->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_public_work","execute");
            $this->session->set_flashdata('success', 'Delete Public Work was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Public Work was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function job_master_list()
    {
        
        $this->record_action("job_master_list","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_all();
        $this->load->view('dashboard/job_master/list_v',$data);
    }

    public function add_job_master()
    {
        
        $this->record_action("add_job_master","view");
        $this->load->view('dashboard/job_master/insert_v');
    }

    public function insert_job_master()
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $insert = $this->job_master->insert_job_master($data);
        if($insert)
        {
            
            $this->record_action("add_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master insertion was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master insertion was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function edit_job_master($id)
    {
        
        $this->record_action("edit_job_master","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_one($id)[0];
        $this->load->view('dashboard/job_master/edit_v',$data);
    }

    public function update_job_master($id)
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $update = $this->job_master->update_job_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master update was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master update was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function delete_job_master($id)
    {
        $this->load->model('job_master_model', "job_master");
        $delete = $this->job_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_job_master","execute");
            $this->session->set_flashdata('success', 'Delete Job Master was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Job Master was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function assignment_master_list()
    {
        
        $this->record_action("assignment_master_list","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_all();
        $this->load->view('dashboard/assignment_master/list_v',$data);
    }

    public function add_assignment_master()
    {
        
        $this->record_action("add_assignment_master","view");
        $this->load->view('dashboard/assignment_master/insert_v');
    }

    public function insert_assignment_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $insert = $this->assignment_master->insert_assignment_master($data);
        if($insert)
        {
            
            $this->record_action("add_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master insertion was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master insertion was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function edit_assignment_master($id)
    {
        
        $this->record_action("edit_assignment_master","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_one($id)[0];
        $this->load->view('dashboard/assignment_master/edit_v',$data);
    }

    public function update_assignment_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $update = $this->assignment_master->update_assignment_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master update was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master update was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function delete_assignment_master($id)
    {
        $this->load->model('assignment_master_model', "assignment_master");
        $delete = $this->assignment_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master Master was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master Master was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function employee_master_list()
    {
        
        $this->record_action("employee_master_list","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_all();
        $this->load->view('dashboard/employee_master/list_v',$data);
    }

    public function add_employee_master()
    {
        
        $this->record_action("add_employee_master","view");
        $this->load->view('dashboard/employee_master/insert_v');
    }

    public function insert_employee_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $insert = $this->employee_master->insert_employee_master($data);
        if($insert)
        {
            
            $this->record_action("add_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master insertion was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master insertion was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function edit_employee_master($id)
    {
        
        $this->record_action("edit_employee_master","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_one($id)[0];
        $this->load->view('dashboard/employee_master/edit_v',$data);
    }

    public function update_employee_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $update = $this->employee_master->update_employee_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Employee Master update was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master update was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function delete_employee_master($id)
    {
        $this->load->model('employee_master_model', "employee_master");
        $delete = $this->employee_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master Master was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master Master was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function search_data()
    {
        $data["table_data"] = $this->input->post("table");
        $data["氏名"] = $this->input->post("full_name");
        $data["連名"] = $this->input->post("full_name_kana");
        
        if($this->input->post("table") == 'harada_client')
        {
            $data["住所"] = $this->input->post("address");
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $data["住　　　所"] = $this->input->post("address");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["kinds"] = $this->input->post("kinds");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["営業担当"] = $this->input->post("sales_staff");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["工務担当"] = $this->input->post("pic_construction");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["コーデ担当"] = $this->input->post("coordinator");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["感謝祭"] = $this->input->post("thanksgiving");
        }
        
        if($this->input->post("table") == 'harada_client')
        {
           $data["ＤＭ"] = $this->input->post("dm");
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["イベントDM"] = $this->input->post("dm");
        }


        if($this->input->post("table") == 'harada_client')
        {
           $data["age"] = $this->input->post("age");
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["age"] = $this->input->post("age");
        }
        
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["長期優良住宅"] = $this->input->post("longterm");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["設備10年保証加入"] = $this->input->post("warranty");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["グリーン化事業補助金"] = $this->input->post("subsidy");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["地盤改良工事"] = $this->input->post("ground_improve");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["借入金融機関"] = $this->input->post("bank");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["火災保険"] = $this->input->post("insurance_company");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["引渡日_1"] = $this->input->post("date_from");
           $data["引渡日_2"] = $this->input->post("date_to");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["生年月日_1"] = $this->input->post("birthdate_from");
           $data["生年月日_2"] = $this->input->post("birthdate_to");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["inspection_from"] = $this->input->post("inspection_from");
           $data["inspection_to"] = $this->input->post("inspection_to");
        }

        if($this->input->post("table") == 'harada_client')
        {
            $data["土地　有無"] = $this->input->post("with_or_without");
        }

        if($this->input->post("table") == 'harada_client')
        {
            $data["初回接点"] = $this->input->post("first_contact");
        }





        $this->load->model('customer_model',"customer");

        if($this->input->post("table") == 'harada_client') {
            $insert["data"] = $this->customer->get_datas_client($data);
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $insert["data"] = $this->customer->get_datas_client_ob($data);
        }

        
        if($this->input->post("table") == 'harada_client')
        {
            $this->load->view('dashboard/result/client',$insert);
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $this->load->view('dashboard/result/client_ob',$insert);
        }

        // echo json_encode($insert, JSON_UNESCAPED_UNICODE);
        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function search_construction()
    {
        $this->record_action("search_construct", "view");
        $data["selector_search_construction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/search_v',$data);
    }

    public function search_data_construct()
    {
        // redirect('dashboard/underconstruc');
        $data['cust_code'] = $this->input->post('cust_code');
        $data['cust_name'] = $this->input->post('cust_name');
        $data['kinds'] = $this->input->post('kinds');
        $data['from_contract_date'] = $this->input->post('from_contract_date');
        $data['to_contract_date'] = $this->input->post('to_contract_date');
        $data['from_construction_start_date'] = $this->input->post('from_construction_start_date');
        $data['to_construction_start_date'] = $this->input->post('to_construction_start_date');
        $data['from_upper_building_date'] = $this->input->post('from_upper_building_date');
        $data['to_upper_building_date'] = $this->input->post('to_upper_building_date');
        $data['from_completion_date'] = $this->input->post('from_completion_date');
        $data['to_completion_date'] = $this->input->post('to_completion_date');
        $data['from_delivery_date'] = $this->input->post('from_delivery_date');
        $data['to_delivery_date'] = $this->input->post('to_delivery_date');

        $this->load->model('customer_model',"customer");
        $wehe['data'] = $this->customer->get_data_construct($data);

        $this->load->view('dashboard/constructor/result_v',$wehe);
        // echo json_encode($wehe, JSON_UNESCAPED_UNICODE);
    }

    function get_autocomplete(){
        $this->load->model('customer_model',"customer");

        if (isset($_GET['term'])) {
            $result = $this->customer->search_id($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $arr_result[] = array(
                        'label'         => $row->cust_code,
                        'description'   => $row->氏名,
                        'another'       => $row->種別,
                 );
                    echo json_encode($arr_result,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    public function add_user($id)
    {
        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names_by($id);
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/user/insert_v',$data);
    }

    public function edit_user($id)
    {
        $this->record_action("update_user", "view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["affiliate"] = $this->customer->fetch_affiliate_names();
        $data["data"] = $this->user->fetch_one_user($id)[0];
        $this->load->view('dashboard/user/update_v',$data);
    }

    public function insert_user()
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]', array('required' => 'パスワードを再入力してください。'));
        $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('dashboard/user/insert_v',$data);
        }
        else
        {
                $this->load->view('formsuccess');
        }
    }

    public function preview_datas()
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]', array('required' => 'パスワードを再入力してください。'));
        // $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        $this->form_validation->set_message('matches','パスワードが間違っています。再度入力してください。');

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        // $data = $this->security->xss_clean($data);

        if ($this->form_validation->run() == FALSE)
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $this->load->view('dashboard/user/insert_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['passconf'] = $this->input->post('passconf');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->load->view('dashboard/user/preview_v',$data);
        }
    }


    public function check_datas()
    {
        if($this->input->post('edit_btn') == 'edit')
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['passconf'] = $this->input->post('passconf');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->record_action("add_construction", "view");
            $this->load->model('customer_model', "customer");
            $data["data"] = $this->customer->fetch_affiliate_names();
            $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

            $this->load->view('dashboard/user/preview_ins_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            // $data['passconf'] = $this->input->post('passconf');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $data['date_regist'] = date('Y-m-d');
            $data['access'] = '3';
            
            $this->load->model('users_model',"user");

            $data = $this->security->xss_clean($data);
            $insert = $this->user->insert_user_new($data);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/list_user");
            }
            else
            {
                $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                redirect("dashboard/list_user");
            }
        }
    }

    public function list_user()
    {
        $this->record_action("user_list", "view");
        $this->load->model('users_model', "user");
        $data["data"] = $this->user->fetch_all_user();
        $data["selector_list_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/user/list_v',$data);
    }


    public function preview_user($id)
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        // $this->form_validation->set_message('matches','パスワードが間違っています。再度入力してください。');

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["datak"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        // $data = $this->security->xss_clean($data);

        if ($this->form_validation->run() == FALSE)
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $this->load->view('dashboard/user/update_err_v',$data);
        }
        else
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->load->view('dashboard/user/preview_user_v',$data);
        }
    }

    public function check_data_user($id)
    {
        if($this->input->post('edit_btn') == 'edit')
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->record_action("add_construction", "view");
            $this->load->model('customer_model', "customer");
            $data["data"] = $this->customer->fetch_affiliate_names();
            $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

            // $data = $this->security->xss_clean($data);

            $this->load->view('dashboard/user/preview_user_ins_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $data['date_regist'] = date('Y-m-d');
            
            $this->load->model('users_model',"user");
            $data = $this->security->xss_clean($data);
            $insert = $this->user->update_user($data,$id);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'ユーザー名が既に存在しています');
                redirect("dashboard/contractor_list");
            }
        }
    }


    public function copy_client($id) {
        // $this->record_action("construction_list", "view");
        $this->load->model('customer_model', "customer");
        $client_data = $this->customer->get_data_client($id)[0];

        $data = [
                "gid"=>$client_data->gid,
                "b"=>$client_data->b,
                "l"=>$client_data->l,
                "氏名"=>$client_data->氏名,
                "連名"=>$client_data->連名,
                "〒"=>$client_data->〒,
                "birth_date" => $client_data->birth_date,
                "age" => $client_data->age,
                "都道府県"=>$client_data->都道府県,
                "住所"=>$client_data->住所,
                "共同住宅"=>$client_data->共同住宅,
                "電話"=>$client_data->電話,
                "奥様名前" => $client_data->奥様名前,
                "奥様名前_かな" => $client_data->奥様名前_かな,
                "奥様生年月日" => $client_data->奥様生年月日,
                "お子様1名前" => $client_data->お子様1名前,
                "お子様1名前_かな" => $client_data->お子様1名前_かな,
                "お子様1生年月日" => $client_data->お子様1生年月日,
                "お子様2名前" => $client_data->お子様2名前,
                "お子様2名前_かな" => $client_data->お子様2名前_かな,
                "お子様2生年月日" => $client_data->お子様2生年月日,
                "お子様3名前" => $client_data->お子様3名前,
                "お子様3名前_かな" => $client_data->お子様3名前_かな,
                "お子様3生年月日" => $client_data->お子様3生年月日,
                "種別"=>$client_data->種別,
                "初回接点"=>$client_data->初回接点,
                "初回年月日"=>$client_data->初回年月日,
                "営業担当"=>$client_data->初回担当,
                "担当"=>$client_data->担当,
                "土地　有無"=>$client_data->土地　有無,
                "訪問"=>$client_data->訪問,
                "ＤＭ"=>$client_data->ＤＭ,
                "備考"=>$client_data->備考,
                "geom"=>$client_data->geom,
                "氏名_かな"=>$client_data->氏名_かな,
                "メールアドレス"=>$client_data->メールアドレス
        ];

        $geez = $this->customer->input_new_client_data($data);

        if($geez)
        {
            $this->record_action("copy_clien","execute");
            $this->session->set_flashdata('success', '更新しました');
            $this->customer->delete_one_client_new($client_data->gid);
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', '氏名は既に存在しています');
            redirect("dashboard/client_list");
        }

        // echo json_encode($geez, JSON_UNESCAPED_UNICODE);
    }






























    //-------------------------------------------------------------------------TEST SECTION----------------------------------------------------------------//
    


    public function underconstruc()
    {
        $this->load->view('dashboard/under/list_v');

        // $data = [
        //     "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
        //     "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
        //     "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
        //     "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
        //     "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
        //     "contract_date" => $this->input->post("contract_date"),
        //     "construction_start_date" => $this->input->post("construction_start_date"),
        //     "upper_building_date" => $this->input->post("upper_building_date"),
        //     "completion_date" => $this->input->post("completion_date"),
        //     "delivery_date" => $this->input->post("delivery_date"),
        //     "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
        //     "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
        //     "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
        //     "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
        //     "check1" => $this->input->post("check1"),
        //     "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
        //     "check2" => $this->input->post("check2"),
        //     "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
        //     "check3" => $this->input->post("check3"),
        //     "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
        //     "check4" => $this->input->post("check4"),
        //     "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
        //     "check5" => $this->input->post("check5"),
        //     "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
        //     "check6" => $this->input->post("check6"),
        //     "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
        //     "check7" => $this->input->post("check7"),
        //     "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
        //     "check8" => $this->input->post("check8"),
        //     "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
        //     "check9" => $this->input->post("check9"),
        //     "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
        // ];

        // if (!empty($this->input->post("co_owner1")))
        // {
        //     $data["co_owner1"] = implode(";",$this->input->post("co_owner1"));
        // }
        // else
        // {
        //     $data["co_owner1"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner2")))
        // {
        //     $data["co_owner2"] = implode(";",$this->input->post("co_owner2"));
        // }
        // else
        // {
        //     $data["co_owner2"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner3")))
        // {
        //     $data["co_owner3"] = implode(";",$this->input->post("co_owner3"));
        // }
        // else
        // {
        //     $data["co_owner3"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner4")))
        // {
        //     $data["co_owner4"] = implode(";",$this->input->post("co_owner4"));
        // }
        // else
        // {
        //     $data["co_owner4"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner5")))
        // {
        //     $data["co_owner5"] = implode(";",$this->input->post("co_owner5"));
        // }
        // else
        // {
        //     $data["co_owner5"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner6")))
        // {
        //     $data["co_owner6"] = implode(";",$this->input->post("co_owner6"));
        // }
        // else
        // {
        //     $data["co_owner6"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner7")))
        // {
        //     $data["co_owner7"] = implode(";",$this->input->post("co_owner6"));
        // }
        // else
        // {
        //     $data["co_owner7"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner8")))
        // {
        //     $data["co_owner8"] = implode(";",$this->input->post("co_owner8"));
        // }
        // else
        // {
        //     $data["co_owner8"] = '';
        // }
        
        // if (!empty($this->input->post("co_owner9")))
        // {
        //     $data["co_owner9"] = implode(";",$this->input->post("co_owner9"));
        // }
        // else
        // {
        //     $data["co_owner9"] = '';
        // }

        // if(isset($_FILES)) {

        //     $config['upload_path'] = APPPATH.'../uploads/files'; 
        //     $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        //     $config['max_size'] = '500000'; // in KlioBytes

        //     if(!empty($_FILES['contract']['size']) && empty($_FILES['file_replace']['size']))
        //     {
        //         $new_name1 = time()."_contract";
        //         $config['file_name'] = $new_name1;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('contract'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["contract"]= $filename1;
        //             $data["date_insert_contract1"] = date('Y-m-d');
        //         }
        //     }

        //     if(!empty($_FILES['file_replace']['size']) && empty($_FILES['contract']['size']))
        //     {
        //         $new_name1 = time()."_contract";
        //         $config['file_name'] = $new_name1;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_contract()[0];
        //             $date_insert = $this->customer->get_last_date_contract()[0];

        //             $data_history_contract = $this->customer->get_last_historycontract()[0];
        //             $date_history_insert = $this->customer->get_last_historydate_contract()[0];



        //             if(empty($data_history_contract->contract_history))
        //             {
        //                 $combine = $data_contract->contract;
        //             }
        //             else
        //             {
        //                 $combine = $data_history_contract->contract_history.';'.$data_contract->contract;
        //             }


        //             if(empty($date_history_insert->date_insert_history1))
        //             {
        //                 $date_combine = $date_insert->date_insert_contract1;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_insert->date_insert_history1.';'.$date_insert->date_insert_contract1;
        //             }
                    

        //             $data["contract"]= $filename1;
        //             $data["date_insert_contract1"] = date('Y-m-d');
        //             $data["contract_history"] = $combine;
        //             $data["date_insert_history1"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing1']['size']) && empty($_FILES['file_replace1']['size']))
        //     {
        //         $new_names1 = time()."_contract_drawing1";
        //         $config['file_name'] = $new_names1;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing1'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing1"]= $filename1;
        //             $data["date_insert_drawing1"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace1']['size']) && empty($_FILES['constract_drawing1']['size']))
        //     {
        //         $new_names1 = time()."_contract_drawing1";
        //         $config['file_name'] = $new_names1;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace1'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing1()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing1()[0];

        //             $data_contract_drawing1_history = $this->customer->get_last_historydrawing1()[0];
        //             $date_history_drawing1 = $this->customer->get_last_historydate_drawing1()[0];



        //             if(empty($data_contract_drawing1_history->contract_drawing1_history))
        //             {
        //                 $combine = $data_contract->constract_drawing1;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing1_history->contract_drawing1_history.';'.$data_contract->constract_drawing1;
        //             }


        //             if(empty($date_history_drawing1->date_insert_history2))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing1;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing1->date_insert_history2.';'.$date_insert->date_insert_drawing1;
        //             }
                    

        //             $data["constract_drawing1"]= $filename1;
        //             $data["date_insert_drawing1"] = date('Y-m-d');
        //             $data["contract_drawing1_history"] = $combine;
        //             $data["date_insert_history2"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing2']['size']) && empty($_FILES['file_replace2']['size']))
        //     {
        //         $new_names2 = time()."_contract_drawing2";
        //         $config['file_name'] = $new_names2;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing2'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing2"]= $filename1;
        //             $data["date_insert_drawing2"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace2']['size']) && empty($_FILES['constract_drawing2']['size']))
        //     {
        //         $new_names2 = time()."_contract_drawing2";
        //         $config['file_name'] = $new_names2;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace2'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing2()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing2()[0];

        //             $data_contract_drawing2_history = $this->customer->get_last_historydrawing2()[0];
        //             $date_history_drawing2 = $this->customer->get_last_historydate_drawing2()[0];



        //             if(empty($data_contract_drawing2_history->contract_drawing2_history))
        //             {
        //                 $combine = $data_contract->constract_drawing2;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing2_history->contract_drawing2_history.';'.$data_contract->constract_drawing2;
        //             }


        //             if(empty($date_history_drawing2->date_insert_history2))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing2;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing2->date_insert_history3.';'.$date_insert->date_insert_drawing2;
        //             }
                    

        //             $data["constract_drawing2"]= $filename1;
        //             $data["date_insert_drawing2"] = date('Y-m-d');
        //             $data["contract_drawing2_history"] = $combine;
        //             $data["date_insert_history3"] = $date_combine;
        //         }
        //     }


        //     if(!empty($_FILES['constract_drawing3']['size']) && empty($_FILES['file_replace3']['size']))
        //     {
        //         $new_names3 = time()."_contract_drawing3";
        //         $config['file_name'] = $new_names3;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing3'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing3"]= $filename1;
        //             $data["date_insert_drawing3"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace3']['size']) && empty($_FILES['constract_drawing3']['size']))
        //     {
        //         $new_names3 = time()."_contract_drawing3";
        //         $config['file_name'] = $new_names3;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace3'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing3()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing3()[0];

        //             $data_contract_drawing3_history = $this->customer->get_last_historydrawing3()[0];
        //             $date_history_drawing3 = $this->customer->get_last_historydate_drawing3()[0];



        //             if(empty($data_contract_drawing3_history->contract_drawing3_history))
        //             {
        //                 $combine = $data_contract->constract_drawing3;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing3_history->contract_drawing3_history.';'.$data_contract->constract_drawing3;
        //             }


        //             if(empty($date_history_drawing3->date_insert_history4))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing3;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing3->date_insert_history4.';'.$date_insert->date_insert_drawing3;
        //             }
                    

        //             $data["constract_drawing3"]= $filename1;
        //             $data["date_insert_drawing3"] = date('Y-m-d');
        //             $data["contract_drawing3_history"] = $combine;
        //             $data["date_insert_history4"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing4']['size']) && empty($_FILES['file_replace4']['size']))
        //     {
        //         $new_names4 = time()."_contract_drawing4";
        //         $config['file_name'] = $new_names4;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing4'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing4"]= $filename1;
        //             $data["date_insert_drawing4"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace4']['size']) && empty($_FILES['constract_drawing4']['size']))
        //     {
        //         $new_names4 = time()."_contract_drawing4";
        //         $config['file_name'] = $new_names4;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace4'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing4()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing4()[0];

        //             $data_contract_drawing4_history = $this->customer->get_last_historydrawing4()[0];
        //             $date_history_drawing4 = $this->customer->get_last_historydate_drawing4()[0];



        //             if(empty($data_contract_drawing4_history->contract_drawing4_history))
        //             {
        //                 $combine = $data_contract->constract_drawing4;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing4_history->contract_drawing4_history.';'.$data_contract->constract_drawing4;
        //             }


        //             if(empty($date_history_drawing4->date_insert_history5))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing4;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing4->date_insert_history5.';'.$date_insert->date_insert_drawing4;
        //             }
                    

        //             $data["constract_drawing4"]= $filename1;
        //             $data["date_insert_drawing4"] = date('Y-m-d');
        //             $data["contract_drawing4_history"] = $combine;
        //             $data["date_insert_history5"] = $date_combine;
        //         }
        //     }


        //     if(!empty($_FILES['constract_drawing5']['size']) && empty($_FILES['file_replace5']['size']))
        //     {
        //         $new_names5 = time()."_contract_drawing5";
        //         $config['file_name'] = $new_names5;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing5'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing5"]= $filename1;
        //             $data["date_insert_drawing5"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace5']['size']) && empty($_FILES['constract_drawing5']['size']))
        //     {
        //         $new_names5 = time()."_contract_drawing5";
        //         $config['file_name'] = $new_names5;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace5'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing5()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing5()[0];

        //             $data_contract_drawing5_history = $this->customer->get_last_historydrawing5()[0];
        //             $date_history_drawing5 = $this->customer->get_last_historydate_drawing5()[0];



        //             if(empty($data_contract_drawing5_history->contract_drawing5_history))
        //             {
        //                 $combine = $data_contract->constract_drawing5;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing5_history->contract_drawing5_history.';'.$data_contract->constract_drawing5;
        //             }


        //             if(empty($date_history_drawing5->date_insert_history6))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing5;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing5->date_insert_history6.';'.$date_insert->date_insert_drawing5;
        //             }
                    

        //             $data["constract_drawing5"]= $filename1;
        //             $data["date_insert_drawing5"] = date('Y-m-d');
        //             $data["contract_drawing5_history"] = $combine;
        //             $data["date_insert_history6"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing6']['size']) && empty($_FILES['file_replace6']['size']))
        //     {
        //         $new_names6 = time()."_contract_drawing6";
        //         $config['file_name'] = $new_names6;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing6'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing6"]= $filename1;
        //             $data["date_insert_drawing6"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace6']['size']) && empty($_FILES['constract_drawing6']['size']))
        //     {
        //         $new_names6 = time()."_contract_drawing6";
        //         $config['file_name'] = $new_names6;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace6'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing6()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing6()[0];

        //             $data_contract_drawing6_history = $this->customer->get_last_historydrawing6()[0];
        //             $date_history_drawing6 = $this->customer->get_last_historydate_drawing6()[0];



        //             if(empty($data_contract_drawing6_history->contract_drawing6_history))
        //             {
        //                 $combine = $data_contract->constract_drawing6;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing6_history->contract_drawing6_history.';'.$data_contract->constract_drawing6;
        //             }


        //             if(empty($date_history_drawing6->date_insert_history7))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing6;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing6->date_insert_history7.';'.$date_insert->date_insert_drawing6;
        //             }
                    

        //             $data["constract_drawing6"]= $filename1;
        //             $data["date_insert_drawing6"] = date('Y-m-d');
        //             $data["contract_drawing6_history"] = $combine;
        //             $data["date_insert_history7"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing7']['size']) && empty($_FILES['file_replace7']['size']))
        //     {
        //         $new_names7 = time()."_contract_drawing7";
        //         $config['file_name'] = $new_names7;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing7'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing7"]= $filename1;
        //             $data["date_insert_drawing7"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace7']['size']) && empty($_FILES['constract_drawing7']['size']))
        //     {
        //         $new_names7 = time()."_contract_drawing7";
        //         $config['file_name'] = $new_names7;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace7'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing7()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing7()[0];

        //             $data_contract_drawing7_history = $this->customer->get_last_historydrawing7()[0];
        //             $date_history_drawing7 = $this->customer->get_last_historydate_drawing7()[0];



        //             if(empty($data_contract_drawing7_history->contract_drawing7_history))
        //             {
        //                 $combine = $data_contract->constract_drawing7;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing7_history->contract_drawing7_history.';'.$data_contract->constract_drawing7;
        //             }


        //             if(empty($date_history_drawing7->date_insert_history8))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing7;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing7->date_insert_history8.';'.$date_insert->date_insert_drawing7;
        //             }
                    

        //             $data["constract_drawing7"]= $filename1;
        //             $data["date_insert_drawing7"] = date('Y-m-d');
        //             $data["contract_drawing7_history"] = $combine;
        //             $data["date_insert_history8"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing8']['size']) && empty($_FILES['file_replace8']['size']))
        //     {
        //         $new_names8 = time()."_contract_drawing8";
        //         $config['file_name'] = $new_names8;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing8'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing8"]= $filename1;
        //             $data["date_insert_drawing8"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace8']['size']) && empty($_FILES['constract_drawing8']['size']))
        //     {
        //         $new_names8 = time()."_contract_drawing8";
        //         $config['file_name'] = $new_names8;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace8'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing8()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing8()[0];

        //             $data_contract_drawing8_history = $this->customer->get_last_historydrawing8()[0];
        //             $date_history_drawing8 = $this->customer->get_last_historydate_drawing8()[0];



        //             if(empty($data_contract_drawing8_history->contract_drawing8_history))
        //             {
        //                 $combine = $data_contract->constract_drawing8;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing8_history->contract_drawing8_history.';'.$data_contract->constract_drawing8;
        //             }


        //             if(empty($date_history_drawing8->date_insert_history9))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing8;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing7->date_insert_history9.';'.$date_insert->date_insert_drawing8;
        //             }
                    

        //             $data["constract_drawing8"]= $filename1;
        //             $data["date_insert_drawing8"] = date('Y-m-d');
        //             $data["contract_drawing8_history"] = $combine;
        //             $data["date_insert_history9"] = $date_combine;
        //         }
        //     }

        //     if(!empty($_FILES['constract_drawing9']['size']) && empty($_FILES['file_replace8']['size']))
        //     {
        //         $new_names9 = time()."_contract_drawing9";
        //         $config['file_name'] = $new_names9;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('constract_drawing9'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];
        //             $data["constract_drawing9"]= $filename1;
        //             $data["date_insert_drawing9"] = date('Y-m-d');
        //         }
        //     }

            
        //     if(!empty($_FILES['file_replace9']['size']) && empty($_FILES['constract_drawing9']['size']))
        //     {
        //         $new_names9 = time()."_contract_drawing9";
        //         $config['file_name'] = $new_names9;
        //         $this->upload->initialize($config);
        //         if ( ! $this->upload->do_upload('file_replace9'))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             $uploadData1 = $this->upload->data();
        //             $filename1 = $uploadData1['file_name'];

        //             $this->load->model('customer_model', "customer");
        //             $data_contract = $this->customer->get_last_constract_drawing9()[0];
        //             $date_insert = $this->customer->get_last_date_constract_drawing9()[0];

        //             $data_contract_drawing9_history = $this->customer->get_last_historydrawing9()[0];
        //             $date_history_drawing9 = $this->customer->get_last_historydate_drawing9()[0];



        //             if(empty($data_contract_drawing9_history->contract_drawing9_history))
        //             {
        //                 $combine = $data_contract->constract_drawing9;
        //             }
        //             else
        //             {
        //                 $combine = $data_contract_drawing9_history->contract_drawing8_history.';'.$data_contract->constract_drawing9;
        //             }


        //             if(empty($date_history_drawing9->date_insert_history10))
        //             {
        //                 $date_combine = $date_insert->date_insert_drawing9;
        //             }
        //             else
        //             {
        //                 $date_combine = $date_history_drawing9->date_insert_history10.';'.$date_insert->date_insert_drawing9;
        //             }
                    

        //             $data["constract_drawing9"]= $filename1;
        //             $data["date_insert_drawing9"] = date('Y-m-d');
        //             $data["contract_drawing9_history"] = $combine;
        //             $data["date_insert_history10"] = $date_combine;
        //         }
        //     }

        // }
    }


    public function test_date()
    {
        echo date('Y-m-d');
    }

    public function view_files_contract($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("login");
        }
        else 
        {
            $path = FCPATH."uploads/contractor/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_cust($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("login");
        }
        else 
        {
            $path = FCPATH."uploads/customer/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_employ($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("login");
        }
        else 
        {
            $path = FCPATH."uploads/employee/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files($file_name)
    {
        $this->load->helper('file');
        if(!isset($_SESSION["username"])) {
            session_destroy();
            show_404();
        }
        else 
        {
            $path = FCPATH."uploads/files/".urldecode($file_name); // get file name
            //get the file extension
            $info = new SplFileInfo($path);
            // var_dump($info->getExtension());
    
            switch ($info->getExtension()) {
                // case 'pdf':
                // case 'png':
                case 'pdf':
                    $contentDisposition = 'inline';
                    break;
                default:
                $contentDisposition = 'attachment';
            }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function view_files_images($file_name)
    {
        if(!isset($_SESSION["username"])) {
            session_destroy();
            redirect("login");
        }
        else 
        {
            $path = FCPATH."uploads/images/".urldecode($file_name); // get file name
            //get the file extension
            $info = mime_content_type($path);
            // var_dump($info->getExtension());
    
            // switch ($info->getExtension()) {
            //     // case 'pdf':
            //     // case 'png':
            //     case 'pdf':
            //         $contentDisposition = 'inline';
            //         break;
            //     default:
            //     $contentDisposition = 'attachment';
            // }

    
            if (file_exists($path)) {

                $fp = fopen($path, "r") ;

                header("Cache-Control: maxage=1");
                header("Pragma: public");
                header("Content-type: ".$info);
                header("Content-Disposition: inline; filename=".urldecode($file_name)."");
                header("Content-Description: PHP Generated Data");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length:' . filesize($path));
                ob_clean();
                flush();
                while (!feof($fp)) {
                    $buff = fread($fp, 1024);
                    print $buff;
                }
                exit;
            }
        }
    }

    public function delete_files() {

        $id = $this->input->post('id');
        $table_name = $this->input->post('table_name');
        $name_file = $this->input->post('name_file');
        // $category = $this->input->post('category');

        $path = APPPATH.'../uploads/files/'.urldecode($name_file);
        unlink($path);

        $this->load->model('customer_model',"customer");
        $boom = $this->customer->update_post($id,$table_name,$category);
        if($boom)
        {
            echo "success";
        }
        else
        {
            echo "something wrong";
        }
        
        // redirect('dashboard/edit_construction/'.$id,'refresh');
        // print_r($boom);
    }


    public function test_unicode()
    {
        $file_name = '2F93FC18-0206-45B4-8E46-C831A9C3ACD6.jpeg';
        $path = FCPATH."uploads/images/".urldecode($file_name); // get file name
        //get the file extension
        $info = mime_content_type($path);


        echo $info;
    }

    public function upload_afiles($file)
    {
        $allowed = array('pdf','csv','xls','xlsx','docx','doc','ppt','pptx');
        $filename = $_FILES[$file]['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!in_array($ext, $allowed)) {
            echo 'error';
        }
        else
        {
            $config['upload_path'] = APPPATH.'../uploads/files'; 
            $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            $config['max_size'] = '500000'; 

            $new_names1 = str_replace(array(";",",","@","!","#",":","(",")"),"",$_FILES[$file]['name']);
            $config['file_name'] = $new_names1;
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload($file))
            {
                echo $this->upload->display_errors();
                die();
            }
            else
            {
                $uploadData1 = $this->upload->data();
                $filename1 = $uploadData1['file_name'];

                // echo '更新しました - '.$file.' - '.$filename1;
                // echo '更新しました';
                echo $filename1;
            }
        }
    }

    public function test_post()
    {
        echo $_FILES['constract_drawing1']['name'];
    }


    public function date_conversion()
    {
        $formatter = new IntlDateFormatter(
            'ja_JP@calendar=japanese',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            'Europe/Madrid',
            IntlDateFormatter::TRADITIONAL
        );
        $ts = $formatter->parse('令和3年7月10日');

    
        print_r($ts, date('Y-m-d', $ts));
    }



    public function logout()
    {
        
        $this->record_action("logout","execute");
        session_destroy();
        redirect("login");
    }
}
