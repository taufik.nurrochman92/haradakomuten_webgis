<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$this->load->view('login/login_v');
	}

    public function check_login()
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $this->load->model('users_model',"users");
        $user = $this->users->check_username($username);
        // print_r($user);die();
        if(count($user) == 1)
        {
            if(password_verify($password,$user[0]->password))
            {
                $array = array(
                    'username' => $username,
                );
                switch ($user[0]->access) {
                    case 1:
                        $array["access"] = "super_admin";
                        $redirect = "dashboard";
                        break;
                    case 2:
                        $array["access"] = "member";
                        $redirect = "member";
                        break;
                    case 3:
                        $array["access"] = "user";
                        $redirect = "user";
                        break;
                    case 4:
                        $array["access"] = "dummy1";
                        $redirect = "dummy1";
                        break;
                    case 5:
                        $array["access"] = "dummy2";
                        $redirect = "dummy2";
                        break;
                }
                
                $this->session->set_userdata( $array );
                // track login
                $this->load->model('login_logs_model',"login_logs");
                $this->login_logs->insert_log($array);
                redirect($redirect);
            }
            else 
            {
                $this->session->set_flashdata('error', 'Password is wrong!');
                redirect("login");
            }
        }
        else 
        {
            $this->session->set_flashdata('error', 'Username is not found!');
            redirect("login");
        }
    }
}
