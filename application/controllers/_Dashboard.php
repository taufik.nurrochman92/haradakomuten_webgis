<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION["username"]))
        {
            redirect("login");
        }
    }

    public function record_action($menu, $action)
    {
        $logs = 
        [
            "username" => $_SESSION["username"],
            "menu" => $menu,
            "action" => $action,
            "access" => $_SESSION["access"],
        ];
        $this->load->model('menu_logs_model',"menu_logs");
        $this->menu_logs->insert_log($logs);
    }

	// public function index()
	// {
    //     $this->record_action("dashboard", "view");
    //     $this->load->model('menu_logs_model',"menu_logs");
    //     $total = $this->menu_logs->count_all();
    //     $data = 
    //     [
    //         "dashboard" => $this->menu_logs->count_menu("dashboard")->total,
    //         "customer" => $this->menu_logs->count_menu_like("customer")->total,
    //         "customer_list" => $this->menu_logs->count_menu("customer_list")->total,
    //         "add_customer" => $this->menu_logs->count_menu("add_customer")->total,
    //         "add_customer_view" => $this->menu_logs->count_menu_action("add_customer","view")->total,
    //         "add_customer_execute" => $this->menu_logs->count_menu_action("add_customer","execute")->total,
    //         "edit_customer" => $this->menu_logs->count_menu("edit_customer")->total,
    //         "edit_customer_view" => $this->menu_logs->count_menu_action("edit_customer","view")->total,
    //         "edit_customer_execute" => $this->menu_logs->count_menu_action("edit_customer","execute")->total,
    //         "delete_customer" => $this->menu_logs->count_menu("delete_customer")->total,
    //         "inspection" => $this->menu_logs->count_menu_like("inspection")->total,
    //         "inspection_list" => $this->menu_logs->count_menu("inspection_list")->total,
    //         "add_inspection" => $this->menu_logs->count_menu("add_inspection")->total,
    //         "add_inspection_view" => $this->menu_logs->count_menu_action("add_inspection","view")->total,
    //         "add_inspection_execute" => $this->menu_logs->count_menu_action("add_inspection","execute")->total,
    //         "edit_inspection" => $this->menu_logs->count_menu("edit_inspection")->total,
    //         "edit_inspection_view" => $this->menu_logs->count_menu_action("edit_inspection","view")->total,
    //         "edit_inspection_execute" => $this->menu_logs->count_menu_action("edit_inspection","execute")->total,
    //         "delete_inspection" => $this->menu_logs->count_menu("delete_inspection")->total,
    //         "partner" => $this->menu_logs->count_menu_like("partner")->total,
    //         "partner_list" => $this->menu_logs->count_menu("partner_list")->total,
    //         "add_partner" => $this->menu_logs->count_menu("add_partner")->total,
    //         "add_partner_view" => $this->menu_logs->count_menu_action("add_partner","view")->total,
    //         "add_partner_execute" => $this->menu_logs->count_menu_action("add_partner","execute")->total,
    //         "edit_partner" => $this->menu_logs->count_menu("edit_partner")->total,
    //         "edit_partner_view" => $this->menu_logs->count_menu_action("edit_partner","view")->total,
    //         "edit_partner_execute" => $this->menu_logs->count_menu_action("edit_partner","execute")->total,
    //         "delete_partner" => $this->menu_logs->count_menu("delete_partner")->total,
    //         "public_work" => $this->menu_logs->count_menu_like("public_work")->total,
    //         "public_work_list" => $this->menu_logs->count_menu("public_work_list")->total,
    //         "add_public_work" => $this->menu_logs->count_menu("add_public_work")->total,
    //         "add_public_work_view" => $this->menu_logs->count_menu_action("add_public_work","view")->total,
    //         "add_public_work_execute" => $this->menu_logs->count_menu_action("add_public_work","execute")->total,
    //         "edit_public_work" => $this->menu_logs->count_menu("edit_public_work")->total,
    //         "edit_public_work_view" => $this->menu_logs->count_menu_action("edit_public_work","view")->total,
    //         "edit_public_work_execute" => $this->menu_logs->count_menu_action("edit_public_work","execute")->total,
    //         "delete_public_work" => $this->menu_logs->count_menu("delete_public_work")->total,
    //         "job_master" => $this->menu_logs->count_menu_like("job_master")->total,
    //         "job_master_list" => $this->menu_logs->count_menu("job_master_list")->total,
    //         "add_job_master" => $this->menu_logs->count_menu("add_job_master")->total,
    //         "add_job_master_view" => $this->menu_logs->count_menu_action("add_job_master","view")->total,
    //         "add_job_master_execute" => $this->menu_logs->count_menu_action("add_job_master","execute")->total,
    //         "edit_job_master" => $this->menu_logs->count_menu("edit_job_master")->total,
    //         "edit_job_master_view" => $this->menu_logs->count_menu_action("edit_job_master","view")->total,
    //         "edit_job_master_execute" => $this->menu_logs->count_menu_action("edit_job_master","execute")->total,
    //         "delete_job_master" => $this->menu_logs->count_menu("delete_job_master")->total,
    //         "assignment_master" => $this->menu_logs->count_menu_like("assignment_master")->total,
    //         "assignment_master_list" => $this->menu_logs->count_menu("assignment_master_list")->total,
    //         "add_assignment_master" => $this->menu_logs->count_menu("add_assignment_master")->total,
    //         "add_assignment_master_view" => $this->menu_logs->count_menu_action("add_assignment_master","view")->total,
    //         "add_assignment_master_execute" => $this->menu_logs->count_menu_action("add_assignment_master","execute")->total,
    //         "edit_assignment_master" => $this->menu_logs->count_menu("edit_assignment_master")->total,
    //         "edit_assignment_master_view" => $this->menu_logs->count_menu_action("edit_assignment_master","view")->total,
    //         "edit_assignment_master_execute" => $this->menu_logs->count_menu_action("edit_assignment_master","execute")->total,
    //         "delete_assignment_master" => $this->menu_logs->count_menu("delete_assignment_master")->total,
    //         "employee_master" => $this->menu_logs->count_menu_like("employee_master")->total,
    //         "employee_master_list" => $this->menu_logs->count_menu("employee_master_list")->total,
    //         "add_employee_master" => $this->menu_logs->count_menu("add_employee_master")->total,
    //         "add_employee_master_view" => $this->menu_logs->count_menu_action("add_employee_master","view")->total,
    //         "add_employee_master_execute" => $this->menu_logs->count_menu_action("add_employee_master","execute")->total,
    //         "edit_employee_master" => $this->menu_logs->count_menu("edit_employee_master")->total,
    //         "edit_employee_master_view" => $this->menu_logs->count_menu_action("edit_employee_master","view")->total,
    //         "edit_employee_master_execute" => $this->menu_logs->count_menu_action("edit_employee_master","execute")->total,
    //         "delete_employee_master" => $this->menu_logs->count_menu("delete_employee_master")->total,
    //     ];
    //     $this->load->view('dashboard/dashboard_v',$data);
	// }

    public function index()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        // $data["data"] = $this->customer->fetch_all_customers_new();
        $data["data"] = $this->customer->fetch_all_client();
        $this->load->view('dashboard/customer/list_v',$data);
    }

    public function customer_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all();
        $this->load->view('dashboard/customer/list_v',$data);
    }


    //---------------------------------------NEW------------------------------------------//

    public function customers_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $this->load->view('dashboard/customers/list_v',$data);
    }

    public function view_customers($id)
    {
        $this->record_action("customer_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_new($id)[0];
        $this->load->view('dashboard/customers/view_v',$data);
    }


    public function client_list()
    {
        
        $this->record_action("client_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_client();
        $this->load->view('dashboard/customer/list_v',$data);
    }


    public function view_client($id)
    {
        $this->record_action("client_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_client_new($id)[0];
        $this->load->view('dashboard/customer/view_v',$data);
    }


    public function employees_list()
    {
        
        $this->record_action("employee_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_employees_new();
        $this->load->view('dashboard/employees/list_v',$data);
    }

    public function view_employees($id)
    {
        $this->record_action("employee_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $this->load->view('dashboard/employees/view_v',$data);
    }

    public function contractor_list()
    {
        
        $this->record_action("contractor_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_contractor_new();
        $this->load->view('dashboard/contractor/list_v',$data);
    }

    public function view_contractor($id)
    {
        $this->record_action("contractor_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];
        $this->load->view('dashboard/contractor/view_v',$data);
    }


    public function email()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $this->load->view('dashboard/email/list_v',$data);
    }


    //-----------------------------------------------------------------------//

    public function add_customer()
    {
        $this->record_action("add_customer", "view");
        $this->load->view('dashboard/customer/insert_v');
    }


    //---------------------------------------NEW-----------------------------------------//

    public function add_customers()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_id"] = $this->customer->get_last_id_inspect()[0];
        $this->load->view('dashboard/customers/insert_v',$data);
    }

    public function add_client()
    {
        $this->record_action("add_client", "view");
        $this->load->model('customer_model', "customer");
        $data["for_id"] = $this->customer->get_last_id_client()[0];
        $this->load->view('dashboard/customer/insert_v',$data);
    }


    public function add_employees()
    {
        $this->record_action("add_customer", "view");
        $this->load->view('dashboard/employees/insert_v');
    }

    public function add_contractor()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["for_id"] = $this->customer->get_last_id_affiliate()[0];
        $this->load->view('dashboard/contractor/insert_v',$data);
    }

    //-----------------------------------------------------------------------//


    public function insert_customer()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard");
        }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_customer($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', 'Customer insertion was successful.');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer insertion was failed.');
            redirect("dashboard/customer_list");
        }
    }


    //---------------------------------------NEW-----------------------------------------//


    public function insert_customers()
    {
        $data = [
            "gid" => $this->input->post("gid"),
            // "customer_code" => $this->input->post("customer_code"),
            "sei" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "last_furigana" => $this->input->post("last_furigana"),
            "first_furigana" => $this->input->post("first_furigana"),
            "zip_code1" => $this->input->post("zip_code1"),
            "住所" => $this->input->post("address_1"),
            "address_line1" => $this->input->post("address_line1"),
            "電話番号" => $this->input->post("phone_number1"),
            "zip_code2" => $this->input->post("zip_code2"),
            "address_line2" => $this->input->post("address_line2"),
            "住所_2" => $this->input->post("address_2"),
            "電話番号_2" => $this->input->post("phone_number2"),
            "cell_phone" => $this->input->post("cell_phone"),
            "メールアドレス" => $this->input->post("email"),
            "誕生日" => $this->input->post("birthday"),
            "fb_account" => $this->input->post("fb_account"),
            "twitter_account" => $this->input->post("twitter_account"),
            "line_account" => $this->input->post("line_account"),
            "instagram_account" => $this->input->post("instagram_account"),
            "thanksgiving" => $this->input->post("thanksgiving"),
            "dm" => $this->input->post("dm"),
            "introducer" => $this->input->post("introducer"),
            // "mid_and_year_end_gift" => $this->input->post("mid_and_year_end_gift"),
            "完了検査" => $this->input->post("contract_date"),
            "引渡" => $this->input->post("delivery_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "construction_end_date" => $this->input->post("construction_end_date"),
            "blog_url" => $this->input->post("blog_url"),
            "sales_staff" => $this->input->post("sales_staff"),
            "pic_construction" => $this->input->post("pic_construction"),
            "coordinator" => $this->input->post("coordinator"),
            "種別" => $this->input->post("kinds"),
            "ten_year_warranty" => $this->input->post("ten_year_warranty"),
            "thermo_hygrometer" => $this->input->post("thermo_hygrometer"),
            "solar_power" => $this->input->post("solar_power"),
            "long_term" => $this->input->post("long_term")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/customers_list");
        }
        if (!empty($this->input->post("mid_and_year_end_gift")))
        {
            $data["mid_and_year_end_gift"] = implode(",",$this->input->post("mid_and_year_end_gift"));
        }
        else
        {
            $data["mid_and_year_end_gift"] = "";
        } 

        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['contract']['size']!=0)
        {
            $new_name = time()."_contract";
            $config['file_name'] = $new_name;

            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('contract'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["contract"]= $filename;
            }
        }

        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['drawing']['size']!=0)
        {
            $new_name = time()."_drawing";
            $config['file_name'] = $new_name;

            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('drawing'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["drawing"]= $filename;
            }
        }

        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['photo']['size']!=0)
        {
            $new_name = time()."_photo";
            $config['file_name'] = $new_name;

            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('photo'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["photo"]= $filename;
            }
        }

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_customers_new($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', 'Customer insertion was successful.');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer insertion was failed.');
            redirect("dashboard/customers_list");
        }
    }

    public function insert_client()
    {
        $data = [
            "gid" => $this->input->post("gid"),
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "初回担当" => $this->input->post("first_time_change"),
            "担当" => $this->input->post("pic_person"),
            "土地　有無" => $this->input->post("with_or_without"),
            "訪問" => $this->input->post("visit"),
            "ＤＭ" => $this->input->post("dm"),
            "ランク" => $this->input->post("rank"),
            "備考" => $this->input->post("remarks")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/client_list");
        }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_client_new($data);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', 'Client insertion was successful.');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Client insertion was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function insert_employees()
    {
        $data = [
            "社員コード" => $this->input->post("employee_code"),
            "姓" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "せい" => $this->input->post("last_furigana"),
            "めい" => $this->input->post("first_furigana"),
            "保有資格1" => $this->input->post("qualification_1"),
            "保有資格2" => $this->input->post("qualification_2"),
            "保有資格3" => $this->input->post("qualification_3"),
            "保有資格4" => $this->input->post("qualification_4"),
            "保有資格5" => $this->input->post("qualification_5"),
            "入社日" => $this->input->post("hire_date"),
            "郵便番号1" => $this->input->post("zip_code"),
            "住所1" => $this->input->post("address_line"),
            "住所1番地" => $this->input->post("address"),
            "電話番号" => $this->input->post("telephone_number"),
            "携帯番号" => $this->input->post("cellphone_number")
        ];
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_employees_new($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', 'Employees insertion was successful.');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees insertion was failed.');
            redirect("dashboard/employees_list");
        }
    }


    public function insert_contractor()
    {
        $data = [
            "gid" => $this->input->post("contractor_code"),
            "種別" => $this->input->post("kinds"),
            "no" => $this->input->post("no"),
            "name" => $this->input->post("name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "住所2" => $this->input->post("address2"),
            "電　　話" => $this->input->post("telephone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/contractor_list");
        }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_contractor_new($data);
        if($insert)
        {
            $this->record_action("add_contractorr","execute");
            $this->session->set_flashdata('success', 'Contractor insertion was successful.');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Contractor insertion was failed.');
            redirect("dashboard/contractor_list");
        }
    }


    //-----------------------------------------------------------------------//



    public function edit_customer($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }


    //---------------------------------------NEW-----------------------------------------//


    public function edit_customers($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_customers_new($id)[0];
        $data["dataks"] = $this->customer->fetch_employees_names();
        $this->load->view('dashboard/customers/edit_v',$data);
    }

    public function edit_client($id)
    {
        $this->record_action("edit_client","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_client($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }

    public function edit_employees($id)
    {
        $this->record_action("edit_employees","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $this->load->view('dashboard/employees/edit_v',$data);
    }


    public function edit_contractor($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];
        $this->load->view('dashboard/contractor/edit_v',$data);
    }



    //-----------------------------------------------------------------------//


    public function update_customer($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/customer_list");
        }
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customer($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', 'Customer update was successful.');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer update was failed.');
            redirect("dashboard/customer_list");
        }
    }

    //---------------------------------------NEW-----------------------------------------//

    public function update_customers($id)
    {
        // print_r($_POST);die();
        $data = [
            // "customer_code" => $this->input->post("customer_code"),
            "sei" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "last_furigana" => $this->input->post("last_furigana"),
            "first_furigana" => $this->input->post("first_furigana"),
            "zip_code1" => $this->input->post("zip_code1"),
            "住所" => $this->input->post("address_1"),
            "address_line1" => $this->input->post("address_line1"),
            "電話番号" => $this->input->post("phone_number1"),
            "zip_code2" => $this->input->post("zip_code2"),
            "address_line2" => $this->input->post("address_line2"),
            "住所_2" => $this->input->post("address_2"),
            "電話番号_2" => $this->input->post("phone_number2"),
            "cell_phone" => $this->input->post("cell_phone"),
            "メールアドレス" => $this->input->post("email"),
            "誕生日" => $this->input->post("birthday"),
            "fb_account" => $this->input->post("fb_account"),
            "twitter_account" => $this->input->post("twitter_account"),
            "line_account" => $this->input->post("line_account"),
            "instagram_account" => $this->input->post("instagram_account"),
            "thanksgiving" => $this->input->post("thanksgiving"),
            "dm" => $this->input->post("dm"),
            "introducer" => $this->input->post("introducer"),
            // "mid_and_year_end_gift" => $this->input->post("mid_and_year_end_gift"),
            "完了検査" => $this->input->post("contract_date"),
            "引渡" => $this->input->post("delivery_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "construction_end_date" => $this->input->post("construction_end_date"),
            "blog_url" => $this->input->post("blog_url"),
            "sales_staff" => $this->input->post("sales_staff"),
            "pic_construction" => $this->input->post("pic_construction"),
            "coordinator" => $this->input->post("coordinator"),
            "種別" => $this->input->post("kinds"),
            "ten_year_warranty" => $this->input->post("ten_year_warranty"),
            "thermo_hygrometer" => $this->input->post("thermo_hygrometer"),
            "solar_power" => $this->input->post("solar_power"),
            "long_term" => $this->input->post("long_term")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/customers_list");
        }
        if (!empty($this->input->post("mid_and_year_end_gift")))
        {
            $data["mid_and_year_end_gift"] = implode(",",$this->input->post("mid_and_year_end_gift"));
        }
        else
        {
            $data["mid_and_year_end_gift"] = "";
        } 
        $fileName = $this->input->post('contract');
        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['contract']['size']!=0)
        {
            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('contract'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $fileName = $uploadData['file_name'];
                $data["contract"] = $fileName;
            }
        }

        $fileName = $this->input->post('drawing');
        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['drawing']['size']!=0)
        {
            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('drawing'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $fileName = $uploadData['file_name'];
                $data["drawing"] = $fileName;
            }
        }

        $fileName = $this->input->post('photo');
        $config['upload_path'] = './uploads/customer'; 
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $config['quality']= '60%';
        if($_FILES['photo']['size']!=0)
        {
            $this->load->library('upload',$config); 
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('photo'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $fileName = $uploadData['file_name'];
                $data["photo"] = $fileName;
            }
        }


        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customers_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', 'Customer update was successful.');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer update was failed.');
            redirect("dashboard/customers_list");
        }
    }
    
    public function update_client($id)
    {
        $data = [
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "初回担当" => $this->input->post("first_time_change"),
            "担当" => $this->input->post("pic_person"),
            "土地　有無" => $this->input->post("with_or_without"),
            "訪問" => $this->input->post("visit"),
            "ＤＭ" => $this->input->post("dm"),
            "ランク" => $this->input->post("rank"),
            "備考" => $this->input->post("remarks")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/client_list");
        }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->update_client_new($data,$id);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', 'Client insertion was successful.');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Client insertion was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function update_employees($id)
    {
        // print_r($_POST);die();
        $data = [
            "社員コード" => $this->input->post("employee_code"),
            "姓" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "せい" => $this->input->post("last_furigana"),
            "めい" => $this->input->post("first_furigana"),
            "保有資格1" => $this->input->post("qualification_1"),
            "保有資格2" => $this->input->post("qualification_2"),
            "保有資格3" => $this->input->post("qualification_3"),
            "保有資格4" => $this->input->post("qualification_4"),
            "保有資格5" => $this->input->post("qualification_5"),
            "入社日" => $this->input->post("hire_date"),
            "郵便番号1" => $this->input->post("zip_code"),
            "住所1" => $this->input->post("address_line"),
            "住所1番地" => $this->input->post("address"),
            "電話番号" => $this->input->post("telephone_number"),
            "携帯番号" => $this->input->post("cellphone_number")
        ];
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_employees_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', 'Employees update was successful.');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees update was failed.');
            redirect("dashboard/employees_list");
        }
    }

    public function update_contractor($id)
    {
        // print_r($_POST);die();
        $data = [
            "種別" => $this->input->post("kinds"),
            "no" => $this->input->post("no"),
            "name" => $this->input->post("name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "住所2" => $this->input->post("address2"),
            "電　　話" => $this->input->post("telephone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/contractor_list");
        }
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_contractor_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', 'Contractor update was successful.');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Contractor update was failed.');
            redirect("dashboard/contractor_list");
        }
    }

    //-----------------------------------------------------------------------//

    public function delete_customer($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', 'Customer delete was successful.');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer delete was failed.');
            redirect("dashboard/customer_list");
        }
    }


    //---------------------------------------NEW-----------------------------------------//

    public function delete_customers($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_customers_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', 'Customer delete was successful.');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer delete was failed.');
            redirect("dashboard/customers_list");
        }
    }

    public function delete_client($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_client_new($id);
        if($delete)
        {
            
            $this->record_action("delete_client","execute");
            $this->session->set_flashdata('success', 'Client delete was successful.');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Client delete was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function delete_employees($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_employee_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', 'Employees delete was successful.');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees delete was failed.');
            redirect("dashboard/employees_list");
        }
    }

    public function delete_contractor($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_contractor_new($id);
        if($delete)
        {
            
            $this->record_action("delete_contractor","execute");
            $this->session->set_flashdata('success', 'Contractor delete was successful.');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Contractor delete was failed.');
            redirect("dashboard/contractor_list");
        }
    }

    //-----------------------------------------------------------------------//

    public function inspection_list()
    {
        
        $this->record_action("inspection_list","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_all();
        $this->load->view('dashboard/inspection/list_v',$data);
    }

    public function add_inspection()
    {
        
        $this->record_action("add_inspection","view");
        $this->load->view('dashboard/inspection/insert_v'); 
    }

    public function insert_inspection()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model',"inspection");
        $insert = $this->inspection->insert_inspection($data);
        if($insert)
        {
            
            $this->record_action("add_inspection","execute");
            $this->session->set_flashdata('success', 'Inpection insertion was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inpection insertion was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function edit_inspection($id)
    {
        
        $this->record_action("edit_inspection","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_one($id)[0];
        $this->load->view('dashboard/inspection/edit_v',$data);
    }

    public function update_inspection($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model', "inspection");
        $update = $this->inspection->update_inspection($data,$id);
        if($update)
        {
            
            $this->record_action("edit_inspection", "execute");
            $this->session->set_flashdata('success', 'Inspection update was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection update was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function delete_inspection($id)
    {
        $this->load->model('inspection_model', "inspection");
        $delete = $this->inspection->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_inspection","execute");
            $this->session->set_flashdata('success', 'Inspection delete was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection delete was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function partner_list()
    {
        
        $this->record_action("partner_list","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_all();
        $this->load->view('dashboard/partner/list_v',$data);
    }

    public function add_partner()
    {
        
        $this->record_action("add_partner","view");
        $this->load->view('dashboard/partner/insert_v');
    }

    public function insert_partner()
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $insert = $this->partner->insert_partner($data);
        if($insert)
        {
            
            $this->record_action("add_partner","execute");
            $this->session->set_flashdata('success', 'Partner insertion was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner insertion was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function edit_partner($id)
    {
        
        $this->record_action("edit_partner","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_one($id)[0];
        $this->load->view('dashboard/partner/edit_v',$data);
    }

    public function update_partner($id)
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $update = $this->partner->update_partner($data,$id);
        if($update)
        {
            
            $this->record_action("edit_partner", "execute");
            $this->session->set_flashdata('success', 'Partner update was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner update was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function delete_partner($id)
    {
        $this->load->model('partner_model', "partner");
        $delete = $this->partner->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_partner","execute");
            $this->session->set_flashdata('success', 'Delete Partner was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Partner was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function public_works_list()
    {
        
        $this->record_action("public_work_list","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_all();
        $this->load->view('dashboard/public_works/list_v',$data);
    }

    public function add_public_works()
    {
        
        $this->record_action("add_public_work","view");
        $this->load->view('dashboard/public_works/insert_v');
    }

    public function insert_public_works()
    {
        // print_r($_FILES);die();
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        
        $config['upload_path'] = FCPATH.'uploads/'; 
        // print_r($config['upload_path']);
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $this->upload->initialize($config);
        if($_FILES['document_image']['name']!="")
        {
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->load->library('upload',$config); 
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }
        else
        {
            $data['document_image']= null;
        }
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        $this->load->model('public_works_model',"public_works");
        $insert = $this->public_works->insert_public_works($data);
        if($insert)
        {
            
            $this->record_action("add_public_work","execute");
            $this->session->set_flashdata('success', 'Public Works insertion was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Works insertion was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function edit_public_works($id)
    {
        
        $this->record_action("edit_public_work","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_one($id)[0];
        $this->load->view('dashboard/public_works/edit_v',$data);
    }

    public function update_public_works($id)
    {
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        
        if($_FILES['document_image']["name"] != "" && $_FILES['document_image']["size"] > 0)
        {
            $config['upload_path'] = FCPATH.'uploads/'; 
            // print_r($config['upload_path']);
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '50000'; // in KlioBytes
            $this->upload->initialize($config);
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->load->library('upload',$config); 
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }

        $this->load->model('public_works_model',"public_works");
        $update = $this->public_works->update_public_works($data,$id);
        if($update)
        {
            
            $this->record_action("edit_public_work","execute");
            $this->session->set_flashdata('success', 'Public Work update was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Work update was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function delete_public_works($id)
    {
        $this->load->model('public_works_model', "public_works");
        $delete = $this->public_works->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_public_work","execute");
            $this->session->set_flashdata('success', 'Delete Public Work was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Public Work was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function job_master_list()
    {
        
        $this->record_action("job_master_list","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_all();
        $this->load->view('dashboard/job_master/list_v',$data);
    }

    public function add_job_master()
    {
        
        $this->record_action("add_job_master","view");
        $this->load->view('dashboard/job_master/insert_v');
    }

    public function insert_job_master()
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $insert = $this->job_master->insert_job_master($data);
        if($insert)
        {
            
            $this->record_action("add_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master insertion was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master insertion was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function edit_job_master($id)
    {
        
        $this->record_action("edit_job_master","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_one($id)[0];
        $this->load->view('dashboard/job_master/edit_v',$data);
    }

    public function update_job_master($id)
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $update = $this->job_master->update_job_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master update was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master update was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function delete_job_master($id)
    {
        $this->load->model('job_master_model', "job_master");
        $delete = $this->job_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_job_master","execute");
            $this->session->set_flashdata('success', 'Delete Job Master was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Job Master was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function assignment_master_list()
    {
        
        $this->record_action("assignment_master_list","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_all();
        $this->load->view('dashboard/assignment_master/list_v',$data);
    }

    public function add_assignment_master()
    {
        
        $this->record_action("add_assignment_master","view");
        $this->load->view('dashboard/assignment_master/insert_v');
    }

    public function insert_assignment_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $insert = $this->assignment_master->insert_assignment_master($data);
        if($insert)
        {
            
            $this->record_action("add_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master insertion was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master insertion was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function edit_assignment_master($id)
    {
        
        $this->record_action("edit_assignment_master","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_one($id)[0];
        $this->load->view('dashboard/assignment_master/edit_v',$data);
    }

    public function update_assignment_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $update = $this->assignment_master->update_assignment_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master update was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master update was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function delete_assignment_master($id)
    {
        $this->load->model('assignment_master_model', "assignment_master");
        $delete = $this->assignment_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master Master was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master Master was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function employee_master_list()
    {
        
        $this->record_action("employee_master_list","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_all();
        $this->load->view('dashboard/employee_master/list_v',$data);
    }

    public function add_employee_master()
    {
        
        $this->record_action("add_employee_master","view");
        $this->load->view('dashboard/employee_master/insert_v');
    }

    public function insert_employee_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $insert = $this->employee_master->insert_employee_master($data);
        if($insert)
        {
            
            $this->record_action("add_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master insertion was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master insertion was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function edit_employee_master($id)
    {
        
        $this->record_action("edit_employee_master","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_one($id)[0];
        $this->load->view('dashboard/employee_master/edit_v',$data);
    }

    public function update_employee_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $update = $this->employee_master->update_employee_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Employee Master update was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master update was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function delete_employee_master($id)
    {
        $this->load->model('employee_master_model', "employee_master");
        $delete = $this->employee_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master Master was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master Master was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function logout()
    {
        
        $this->record_action("logout","execute");
        session_destroy();
        redirect("login");
    }
}
