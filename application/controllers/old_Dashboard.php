<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION["username"]))
        {
            redirect("login");
        }
    }

    public function record_action($menu, $action)
    {
        $logs = 
        [
            "username" => $_SESSION["username"],
            "menu" => $menu,
            "action" => $action,
            "access" => $_SESSION["access"],
        ];
        $this->load->model('menu_logs_model',"menu_logs");
        $this->menu_logs->insert_log($logs);
    }

	// public function index()
	// {
    //     $this->record_action("dashboard", "view");
    //     $this->load->model('menu_logs_model',"menu_logs");
    //     $total = $this->menu_logs->count_all();
    //     $data = 
    //     [
    //         "dashboard" => $this->menu_logs->count_menu("dashboard")->total,
    //         "customer" => $this->menu_logs->count_menu_like("customer")->total,
    //         "customer_list" => $this->menu_logs->count_menu("customer_list")->total,
    //         "add_customer" => $this->menu_logs->count_menu("add_customer")->total,
    //         "add_customer_view" => $this->menu_logs->count_menu_action("add_customer","view")->total,
    //         "add_customer_execute" => $this->menu_logs->count_menu_action("add_customer","execute")->total,
    //         "edit_customer" => $this->menu_logs->count_menu("edit_customer")->total,
    //         "edit_customer_view" => $this->menu_logs->count_menu_action("edit_customer","view")->total,
    //         "edit_customer_execute" => $this->menu_logs->count_menu_action("edit_customer","execute")->total,
    //         "delete_customer" => $this->menu_logs->count_menu("delete_customer")->total,
    //         "inspection" => $this->menu_logs->count_menu_like("inspection")->total,
    //         "inspection_list" => $this->menu_logs->count_menu("inspection_list")->total,
    //         "add_inspection" => $this->menu_logs->count_menu("add_inspection")->total,
    //         "add_inspection_view" => $this->menu_logs->count_menu_action("add_inspection","view")->total,
    //         "add_inspection_execute" => $this->menu_logs->count_menu_action("add_inspection","execute")->total,
    //         "edit_inspection" => $this->menu_logs->count_menu("edit_inspection")->total,
    //         "edit_inspection_view" => $this->menu_logs->count_menu_action("edit_inspection","view")->total,
    //         "edit_inspection_execute" => $this->menu_logs->count_menu_action("edit_inspection","execute")->total,
    //         "delete_inspection" => $this->menu_logs->count_menu("delete_inspection")->total,
    //         "partner" => $this->menu_logs->count_menu_like("partner")->total,
    //         "partner_list" => $this->menu_logs->count_menu("partner_list")->total,
    //         "add_partner" => $this->menu_logs->count_menu("add_partner")->total,
    //         "add_partner_view" => $this->menu_logs->count_menu_action("add_partner","view")->total,
    //         "add_partner_execute" => $this->menu_logs->count_menu_action("add_partner","execute")->total,
    //         "edit_partner" => $this->menu_logs->count_menu("edit_partner")->total,
    //         "edit_partner_view" => $this->menu_logs->count_menu_action("edit_partner","view")->total,
    //         "edit_partner_execute" => $this->menu_logs->count_menu_action("edit_partner","execute")->total,
    //         "delete_partner" => $this->menu_logs->count_menu("delete_partner")->total,
    //         "public_work" => $this->menu_logs->count_menu_like("public_work")->total,
    //         "public_work_list" => $this->menu_logs->count_menu("public_work_list")->total,
    //         "add_public_work" => $this->menu_logs->count_menu("add_public_work")->total,
    //         "add_public_work_view" => $this->menu_logs->count_menu_action("add_public_work","view")->total,
    //         "add_public_work_execute" => $this->menu_logs->count_menu_action("add_public_work","execute")->total,
    //         "edit_public_work" => $this->menu_logs->count_menu("edit_public_work")->total,
    //         "edit_public_work_view" => $this->menu_logs->count_menu_action("edit_public_work","view")->total,
    //         "edit_public_work_execute" => $this->menu_logs->count_menu_action("edit_public_work","execute")->total,
    //         "delete_public_work" => $this->menu_logs->count_menu("delete_public_work")->total,
    //         "job_master" => $this->menu_logs->count_menu_like("job_master")->total,
    //         "job_master_list" => $this->menu_logs->count_menu("job_master_list")->total,
    //         "add_job_master" => $this->menu_logs->count_menu("add_job_master")->total,
    //         "add_job_master_view" => $this->menu_logs->count_menu_action("add_job_master","view")->total,
    //         "add_job_master_execute" => $this->menu_logs->count_menu_action("add_job_master","execute")->total,
    //         "edit_job_master" => $this->menu_logs->count_menu("edit_job_master")->total,
    //         "edit_job_master_view" => $this->menu_logs->count_menu_action("edit_job_master","view")->total,
    //         "edit_job_master_execute" => $this->menu_logs->count_menu_action("edit_job_master","execute")->total,
    //         "delete_job_master" => $this->menu_logs->count_menu("delete_job_master")->total,
    //         "assignment_master" => $this->menu_logs->count_menu_like("assignment_master")->total,
    //         "assignment_master_list" => $this->menu_logs->count_menu("assignment_master_list")->total,
    //         "add_assignment_master" => $this->menu_logs->count_menu("add_assignment_master")->total,
    //         "add_assignment_master_view" => $this->menu_logs->count_menu_action("add_assignment_master","view")->total,
    //         "add_assignment_master_execute" => $this->menu_logs->count_menu_action("add_assignment_master","execute")->total,
    //         "edit_assignment_master" => $this->menu_logs->count_menu("edit_assignment_master")->total,
    //         "edit_assignment_master_view" => $this->menu_logs->count_menu_action("edit_assignment_master","view")->total,
    //         "edit_assignment_master_execute" => $this->menu_logs->count_menu_action("edit_assignment_master","execute")->total,
    //         "delete_assignment_master" => $this->menu_logs->count_menu("delete_assignment_master")->total,
    //         "employee_master" => $this->menu_logs->count_menu_like("employee_master")->total,
    //         "employee_master_list" => $this->menu_logs->count_menu("employee_master_list")->total,
    //         "add_employee_master" => $this->menu_logs->count_menu("add_employee_master")->total,
    //         "add_employee_master_view" => $this->menu_logs->count_menu_action("add_employee_master","view")->total,
    //         "add_employee_master_execute" => $this->menu_logs->count_menu_action("add_employee_master","execute")->total,
    //         "edit_employee_master" => $this->menu_logs->count_menu("edit_employee_master")->total,
    //         "edit_employee_master_view" => $this->menu_logs->count_menu_action("edit_employee_master","view")->total,
    //         "edit_employee_master_execute" => $this->menu_logs->count_menu_action("edit_employee_master","execute")->total,
    //         "delete_employee_master" => $this->menu_logs->count_menu("delete_employee_master")->total,
    //     ];
    //     $this->load->view('dashboard/dashboard_v',$data);
	// }

    public function index()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_bank"] = $this->customer->fetch_finances();
        $this->load->view('dashboard/dashboard_v',$data);
    }

    public function customer_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all();
        $data["selector"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/list_v',$data);
    }


    //---------------------------------------NEW------------------------------------------//

    public function customers_list()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $data["selector_customers"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customers/list_v',$data);
    }

    public function view_customers($id)
    {
        $this->record_action("customer_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_new($id)[0];
        $this->load->view('dashboard/customers/view_v',$data);
    }


    public function client_list()
    {
        
        $this->record_action("client_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_client();
        $data["selector_client"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/list_v',$data);
    }


    public function view_client($id)
    {
        $this->record_action("client_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_client_new($id)[0];
        $this->load->view('dashboard/customer/view_v',$data);
    }


    public function employees_list()
    {
        
        $this->record_action("employee_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_employees_new();
        $data["selector_employees"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/employees/list_v',$data);
    }

    public function view_employees($id)
    {
        $this->record_action("employee_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $this->load->view('dashboard/employees/view_v',$data);
    }

    public function contractor_list()
    {
        
        $this->record_action("contractor_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_contractor_new();
        $data["selector_contractor"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/contractor/list_v',$data);
    }

    public function view_contractor($id)
    {
        $this->record_action("contractor_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];

    
        $this->load->model('users_model', "user");
        $name = $this->user->get_name_affiliate($id)[0];

        $data["data_affiliate"] = $this->user->fetch_all_user_by($name);
        $this->load->view('dashboard/contractor/view_v',$data);

        // echo json_encode($name, JSON_UNESCAPED_UNICODE);
    }


    public function email()
    {
        
        $this->record_action("customer_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_customers_new();
        $this->load->view('dashboard/email/list_v',$data);
    }


    public function constructor_list()
    {
        
        $this->record_action("construction_list", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_all_construction_new();
        $data["selector_costruction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/list_v',$data);
    }

    public function view_construction($id)
    {
        $this->record_action("construction_view", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_construction_new($id)[0];
        $this->load->view('dashboard/constructor/view_v',$data);
    }


    //-----------------------------------------------------------------------//

    public function add_customer()
    {
        $this->record_action("add_customer", "view");
        $this->load->view('dashboard/customer/insert_v');
    }


    //---------------------------------------NEW-----------------------------------------//

    public function add_customers()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_employees_names();
        $data["for_id"] = $this->customer->get_last_id_inspect()[0];
        $data["for_bank"] = $this->customer->fetch_finances();
        $data["selector_add_customers"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customers/insert_v',$data);
    }

    public function add_client()
    {
        $this->record_action("add_client", "view");
        $this->load->model('customer_model', "customer");
        $data["for_id"] = $this->customer->get_last_id_client()[0];
        $data["selector_add_client"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/customer/insert_v',$data);
    }


    public function add_employees()
    {
        $this->record_action("add_customer", "view");
        $data["selector_add_employees"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/employees/insert_v',$data);
    }

    public function add_contractor()
    {
        $this->record_action("add_customer", "view");
        $this->load->model('customer_model', "customer");
        $data["for_id"] = $this->customer->get_last_id_affiliate()[0];
        // $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_contractor"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/contractor/insert_v',$data);
    }

    public function add_construction()
    {
        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data_employ"] = $this->customer->fetch_employees_names();
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_construction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/insert_v',$data);
    }


    //-----------------------------------------------------------------------//


    public function insert_customer()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            redirect("dashboard");
        }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_customer($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer insertion was failed.');
            redirect("dashboard/customer_list");
        }
    }


    //---------------------------------------NEW-----------------------------------------//


    public function insert_customers()
    {
        $data = [
            "gid" => $this->input->post("gid"),
            "cust_code" => $this->input->post("cust_code"),
            "氏名" => $this->input->post("name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "共同住宅" => $this->input->post("building_name"),
            "電　　話" => $this->input->post("phone_number"),
            "種別" => $this->input->post("kinds"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("calendar"),
            "メールアドレス" => $this->input->post("email"),
            "生年月日" => $this->input->post("birthday"),
            "営業担当" => $this->input->post("sales_staff"),
            "工務担当" => $this->input->post("pic_construction"),
            "備考" => $this->input->post("marks"),
            "氏名_かな" => $this->input->post("name_kana"),
            "コーデ担当" => $this->input->post("coordinator"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "長期優良住宅" => $this->input->post("longterm"),
            "設備10年保証加入" => $this->input->post("warranty"),
            "引渡日" => $this->input->post("delivery_date"),
            "定期点検項目_3ヶ月" => $this->input->post("inspection_time1"),
            "定期点検項目_6ヶ月" => $this->input->post("inspection_time2"),
            "定期点検項目_1年" => $this->input->post("inspection_time3"),
            "定期点検項目_2年" => $this->input->post("inspection_time4"),
            "定期点検項目_3年" => $this->input->post("inspection_time5"),
            "定期点検項目_5年" => $this->input->post("inspection_time6"),
            "定期点検項目_10年" => $this->input->post("inspection_time7"),
            "借入金融機関" => $this->input->post("bank"),
            "火災保険" => $this->input->post("insurance_company"),
            "火災保険備考" => $this->input->post("insurance"),
            "グリーン化事業補助金" => $this->input->post("subsidy"),
            "地盤改良工事" => $this->input->post("ground_improve"),
            "感謝祭" => $this->input->post("thanksgiving"),
            "イベントDM" => $this->input->post("dm")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            redirect("dashboard/customers_list");
        }

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_customers_new($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer insertion was failed.');
            redirect("dashboard/customers_list");
        }
    }

    public function insert_client()
    {
        $data = [
            "gid" => $this->input->post("gid"),
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "初回担当" => $this->input->post("first_time_change"),
            "担当" => $this->input->post("pic_person"),
            "土地　有無" => $this->input->post("with_or_without"),
            "訪問" => $this->input->post("visit"),
            "ＤＭ" => $this->input->post("dm"),
            "ランク" => $this->input->post("rank"),
            "備考" => $this->input->post("remarks"),
            "氏名_かな" => $this->input->post("full_name_kana")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/client_list");
        // }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_client_new($data);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Client insertion was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function insert_employees()
    {
        $data = [
            "社員コード" => $this->input->post("employee_code"),
            "姓" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "せい" => $this->input->post("last_furigana"),
            "めい" => $this->input->post("first_furigana"),
            "保有資格1" => $this->input->post("qualification_1"),
            "保有資格2" => $this->input->post("qualification_2"),
            "保有資格3" => $this->input->post("qualification_3"),
            "保有資格4" => $this->input->post("qualification_4"),
            "保有資格5" => $this->input->post("qualification_5"),
            "入社日" => $this->input->post("hire_date"),
            "郵便番号1" => $this->input->post("zip_code"),
            "住所1" => $this->input->post("address_line"),
            "住所1番地" => $this->input->post("address"),
            "電話番号" => $this->input->post("telephone_number"),
            "携帯番号" => $this->input->post("cellphone_number")
        ];
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_employees_new($data);
        if($insert)
        {
            $this->record_action("add_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees insertion was failed.');
            redirect("dashboard/employees_list");
        }
    }


    public function insert_contractor()
    {
        if(!empty($this->input->post("kinds")) && empty($this->input->post('username')))
        {
            $data = [
                "gid" => $this->input->post("contractor_code"),
                "種別" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => $this->input->post("name"),
                "〒" => $this->input->post("zip_code"),
                "都道府県" => $this->input->post("prefecture"),
                "住　　　所" => $this->input->post("address"),
                "住所2" => $this->input->post("address2"),
                "電　　話" => $this->input->post("telephone")
            ];
            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data["b"] = $this->input->post("latitude");
                $data["l"] = $this->input->post("longitude");
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                redirect("dashboard/contractor_list");
            }


            $this->load->model('customer_model',"customer");
            $insert = $this->customer->insert_contractor_new($data);
            if($insert)
            {
                $this->record_action("add_contractorr","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'Contractor insertion was failed.');
                redirect("dashboard/contractor_list");
            }
        }
        elseif(empty($this->input->post("kinds")) && !empty($this->input->post('username')))
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('name');
            $data['date_regist'] = date('Y-m-d');
            
            $this->load->model('users_model',"user");
            $insert = $this->user->update_user($data,$id);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'User insertion was failed.');
                redirect("dashboard/contractor_list");
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Something was Error.');
            redirect("dashboard/contractor_list");
        }
        
    }


    public function insert_constructor()
    {
        // redirect("dashboard/underconstruc");
        $data = [
            "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
            "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
            "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
            "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
            "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
            "contract_date" => $this->input->post("contract_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "upper_building_date" => $this->input->post("upper_building_date"),
            "completion_date" => $this->input->post("completion_date"),
            "delivery_date" => $this->input->post("delivery_date"),
            "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
            "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
            "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
            "contract" => mb_convert_kana($this->input->post("contract"), "as"),
            "constract_drawing1" => mb_convert_kana($this->input->post("constract_drawing1"), "as"),
            "check1" => mb_convert_kana($this->input->post("check1"), "as"),
            "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
            "constract_drawing2" => mb_convert_kana($this->input->post("constract_drawing2"), "as"),
            "check2" => mb_convert_kana($this->input->post("check2"), "as"),
            "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
            "constract_drawing3" => mb_convert_kana($this->input->post("constract_drawing3"), "as"),
            "check3" => mb_convert_kana($this->input->post("check3"), "as"),
            "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
            "constract_drawing4" => mb_convert_kana($this->input->post("constract_drawing4"), "as"),
            "check4" => mb_convert_kana($this->input->post("check4"), "as"),
            "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
            "constract_drawing5" => mb_convert_kana($this->input->post("constract_drawing5"), "as"),
            "check5" => mb_convert_kana($this->input->post("check5"), "as"),
            "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
            "constract_drawing6" => mb_convert_kana($this->input->post("constract_drawing6"), "as"),
            "check6" => mb_convert_kana($this->input->post("check6"), "as"),
            "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
            "constract_drawing7" => mb_convert_kana($this->input->post("constract_drawing7"), "as"),
            "check7" => mb_convert_kana($this->input->post("check7"), "as"),
            "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
            "constract_drawing8" => mb_convert_kana($this->input->post("constract_drawing8"), "as"),
            "check8" => mb_convert_kana($this->input->post("check8"), "as"),
            "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
            "constract_drawing9" => mb_convert_kana($this->input->post("constract_drawing9"), "as"),
            "check9" => mb_convert_kana($this->input->post("check9"), "as"),
            "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
        ];

        if (!empty($this->input->post("co_owner1")))
        {
            $data["co_owner1"] = implode(";",mb_convert_kana($this->input->post("co_owner1"), "as"));
        }
        else
        {
            $data["co_owner1"] = "";
        }
        
        if (!empty($this->input->post("co_owner2")))
        {
            $data["co_owner2"] = implode(";",mb_convert_kana($this->input->post("co_owner"), "as"));
        }
        else
        {
            $data["co_owner2"] = "";
        }
        
        if (!empty($this->input->post("co_owner3")))
        {
            $data["co_owner3"] = implode(";",mb_convert_kana($this->input->post("co_owner3"), "as"));
        }
        else
        {
            $data["co_owner3"] = "";
        }
        
        if (!empty($this->input->post("co_owner4")))
        {
            $data["co_owner4"] = implode(";",mb_convert_kana($this->input->post("co_owner4"), "as"));
        }
        else
        {
            $data["co_owner4"] = "";
        }
        
        if (!empty($this->input->post("co_owner5")))
        {
            $data["co_owner5"] = implode(";",mb_convert_kana($this->input->post("co_owner5"), "as"));
        }
        else
        {
            $data["co_owner5"] = "";
        }
        
        if (!empty($this->input->post("co_owner6")))
        {
            $data["co_owner6"] = implode(";",mb_convert_kana($this->input->post("co_owner6"), "as"));
        }
        else
        {
            $data["co_owner6"] = "";
        }
        
        if (!empty($this->input->post("co_owner7")))
        {
            $data["co_owner7"] = implode(";",mb_convert_kana($this->input->post("co_owner7"), "as"));
        }
        else
        {
            $data["co_owner7"] = "";
        }
        
        if (!empty($this->input->post("co_owner8")))
        {
            $data["co_owner8"] = implode(";",mb_convert_kana($this->input->post("co_owner8"), "as"));
        }
        else
        {
            $data["co_owner8"] = "";
        }
        
        if (!empty($this->input->post("co_owner9")))
        {
            $data["co_owner9"] = implode(";",mb_convert_kana($this->input->post("co_owner9"), "as"));
        }
        else
        {
            $data["co_owner9"] = "";
        }

        $config['upload_path'] = APPPATH.'../uploads/files'; 
        $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        $config['max_size'] = '50000'; // in KlioBytes

        for ($i=1; $i<= 9 ; $i++) {
            if($_FILES['constract_drawing'.$i]['name'] != "")
            {
                $new_name = time()."_contract_drawing".$i;
                $config['file_name'] = $new_name;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing'.$i))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    //file is uploaded successfully
                    //now get the file uploaded data 
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $data['constract_drawing'.$i]= $filename;
                }
            }
        } 

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->insert_constructor_new($data);
        if($insert)
        {
            $this->record_action("add_constructor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/constructor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'constructor insertion was failed.');
            redirect("dashboard/constructor_list");
        }
    }


    //-----------------------------------------------------------------------//

    public function preview_data()
    {
        $data = [
            "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
            "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
            "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
            "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
            "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
            "contract_date" => $this->input->post("contract_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "upper_building_date" => $this->input->post("upper_building_date"),
            "completion_date" => $this->input->post("completion_date"),
            "delivery_date" => $this->input->post("delivery_date"),
            "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
            "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
            "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
            "check1" => $this->input->post("check1"),
            "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
            "check2" => $this->input->post("check2"),
            "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
            "check3" => $this->input->post("check3"),
            "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
            "check4" => $this->input->post("check4"),
            "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
            "check5" => $this->input->post("check5"),
            "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
            "check6" => $this->input->post("check6"),
            "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
            "check7" => $this->input->post("check7"),
            "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
            "check8" => $this->input->post("check8"),
            "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
            "check9" => $this->input->post("check9"),
            "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
        ];

        if (!empty($this->input->post("co_owner1")))
        {
            $data["co_owner1"] = $this->input->post("co_owner1");
        }
        else
        {
            $data["co_owner1"] = "";
        }
        
        if (!empty($this->input->post("co_owner2")))
        {
            $data["co_owner2"] = $this->input->post("co_owner2");
        }
        else
        {
            $data["co_owner2"] = "";
        }
        
        if (!empty($this->input->post("co_owner3")))
        {
            $data["co_owner3"] = $this->input->post("co_owner3");
        }
        else
        {
            $data["co_owner3"] = "";
        }
        
        if (!empty($this->input->post("co_owner4")))
        {
            $data["co_owner4"] = $this->input->post("co_owner4");
        }
        else
        {
            $data["co_owner4"] = "";
        }
        
        if (!empty($this->input->post("co_owner5")))
        {
            $data["co_owner5"] = $this->input->post("co_owner5");
        }
        else
        {
            $data["co_owner5"] = "";
        }
        
        if (!empty($this->input->post("co_owner6")))
        {
            $data["co_owner6"] = $this->input->post("co_owner6");
        }
        else
        {
            $data["co_owner6"] = "";
        }
        
        if (!empty($this->input->post("co_owner7")))
        {
            $data["co_owner7"] = $this->input->post("co_owner7");
        }
        else
        {
            $data["co_owner7"] = "";
        }
        
        if (!empty($this->input->post("co_owner8")))
        {
            $data["co_owner8"] = $this->input->post("co_owner8");
        }
        else
        {
            $data["co_owner8"] = "";
        }
        
        if (!empty($this->input->post("co_owner9")))
        {
            $data["co_owner9"] = $this->input->post("co_owner9");
        }
        else
        {
            $data["co_owner9"] = "";
        }

        $config['upload_path'] = APPPATH.'../uploads/files'; 
        $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
        $config['max_size'] = '50000'; // in KlioBytes

        // for ($i=1; $i<= 9 ; $i++) {
        //     if($_FILES['constract_drawing'.$i]['name'] != "")
        //     {
        //         $this->load->library('upload',$config); 
        //         if ( ! $this->upload->do_upload('constract_drawing'.$i))
        //         {
        //             print_r($this->upload->display_errors());
        //             die();
        //         }
        //         else
        //         {
        //             //file is uploaded successfully
        //             //now get the file uploaded data 
        //             $uploadData = $this->upload->data();
        //             $filename = $uploadData['file_name'];
        //             $data['constract_drawing'.$i]= $filename;
        //         }
        //     }
        // } 

        if(isset($_FILES)) {

            if($_FILES['contract']['size']!=0)
            {
                $new_name1 = time()."_contract";
                $config['file_name'] = $new_name1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('contract'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["contract"]= $filename1;
                }
            }
            else
            {
                $data["contract"]= $this->input->post("contract");
            }

            if($_FILES['constract_drawing1']['size']!=0)
            {
                $new_names1 = time()."_contract_drawing1";
                $config['file_name'] = $new_names1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing1'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing1"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing1"]= $this->input->post("constract_drawing1");
            }

            if($_FILES['constract_drawing2']['size']!=0)
            {
                $new_names2 = time()."_contract_drawing2";
                $config['file_name'] = $new_names2;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing2'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing2"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing2"]= $this->input->post("constract_drawing2");
            }

            if($_FILES['constract_drawing3']['size']!=0)
            {
                $new_names3 = time()."_contract_drawing3";
                $config['file_name'] = $new_names3;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing3'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing3"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing3"]= $this->input->post("constract_drawing3");
            }

            if($_FILES['constract_drawing4']['size']!=0)
            {
                $new_names4 = time()."_contract_drawing4";
                $config['file_name'] = $new_names4;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing4'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing4"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing4"]= $this->input->post("constract_drawing4");
            }

            if($_FILES['constract_drawing5']['size']!=0)
            {
                $new_names5 = time()."_contract_drawing5";
                $config['file_name'] = $new_names5;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing5'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing5"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing5"]= $this->input->post("constract_drawing5");
            }

            if($_FILES['constract_drawing6']['size']!=0)
            {
                $new_names6 = time()."_contract_drawing6";
                $config['file_name'] = $new_names6;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing6'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing6"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing6"]= $this->input->post("constract_drawing6");
            }

            if($_FILES['constract_drawing7']['size']!=0)
            {
                $new_names7 = time()."_contract_drawing7";
                $config['file_name'] = $new_names7;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing7'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing7"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing7"]= $this->input->post("constract_drawing7");
            }

            if($_FILES['constract_drawing8']['size']!=0)
            {
                $new_names8 = time()."_contract_drawing8";
                $config['file_name'] = $new_names8;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing8'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing8"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing8"]= $this->input->post("constract_drawing8");
            }

            
            if($_FILES['constract_drawing9']['size']!=0)
            {
                $new_names9 = time()."_contract_drawing9";
                $config['file_name'] = $new_names9;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing9'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing9"]= $filename1;
                }
            }
            else
            {
                $data["constract_drawing9"]= $this->input->post("constract_drawing9");
            }

        }

        $data["date_insert_contract1"] = date('Y-m-d');
        $data["date_insert_drawing1"] = date('Y-m-d');
        $data["date_insert_drawing2"] = date('Y-m-d');
        $data["date_insert_drawing3"] = date('Y-m-d');
        $data["date_insert_drawing4"] = date('Y-m-d');
        $data["date_insert_drawing5"] = date('Y-m-d');
        $data["date_insert_drawing6"] = date('Y-m-d');
        $data["date_insert_drawing7"] = date('Y-m-d');
        $data["date_insert_drawing8"] = date('Y-m-d');
        $data["date_insert_drawing9"] = date('Y-m-d');

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);
        $this->load->view('dashboard/constructor/preview_v',$data);
    }



    public function check_data()
    {
        if($this->input->post('edit_btn') == 'edit')
        {
            $data = [
                "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
                "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
                "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
                "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
                "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
                "contract_date" => $this->input->post("contract_date"),
                "construction_start_date" => $this->input->post("construction_start_date"),
                "upper_building_date" => $this->input->post("upper_building_date"),
                "completion_date" => $this->input->post("completion_date"),
                "delivery_date" => $this->input->post("delivery_date"),
                "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
                "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
                "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
                "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
                "check1" => $this->input->post("check1"),
                "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
                "check2" => $this->input->post("check2"),
                "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
                "check3" => $this->input->post("check3"),
                "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
                "check4" => $this->input->post("check4"),
                "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
                "check5" => $this->input->post("check5"),
                "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
                "check6" => $this->input->post("check6"),
                "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
                "check7" => $this->input->post("check7"),
                "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
                "check8" => $this->input->post("check8"),
                "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
                "check9" => $this->input->post("check9"),
                "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
            ];
    
            if (!empty($this->input->post("co_owner1")))
            {
                $data["co_owner1"] = $this->input->post("co_owner1");
            }
            else
            {
                $data["co_owner1"] = "";
            }
            
            if (!empty($this->input->post("co_owner2")))
            {
                $data["co_owner2"] = $this->input->post("co_owner2");
            }
            else
            {
                $data["co_owner2"] = "";
            }
            
            if (!empty($this->input->post("co_owner3")))
            {
                $data["co_owner3"] = $this->input->post("co_owner3");
            }
            else
            {
                $data["co_owner3"] = "";
            }
            
            if (!empty($this->input->post("co_owner4")))
            {
                $data["co_owner4"] = $this->input->post("co_owner4");
            }
            else
            {
                $data["co_owner4"] = "";
            }
            
            if (!empty($this->input->post("co_owner5")))
            {
                $data["co_owner5"] = $this->input->post("co_owner5");
            }
            else
            {
                $data["co_owner5"] = "";
            }
            
            if (!empty($this->input->post("co_owner6")))
            {
                $data["co_owner6"] = $this->input->post("co_owner6");
            }
            else
            {
                $data["co_owner6"] = "";
            }
            
            if (!empty($this->input->post("co_owner7")))
            {
                $data["co_owner7"] = $this->input->post("co_owner7");
            }
            else
            {
                $data["co_owner7"] = "";
            }
            
            if (!empty($this->input->post("co_owner8")))
            {
                $data["co_owner8"] = $this->input->post("co_owner8");
            }
            else
            {
                $data["co_owner8"] = "";
            }
            
            if (!empty($this->input->post("co_owner9")))
            {
                $data["co_owner9"] = $this->input->post("co_owner9");
            }
            else
            {
                $data["co_owner9"] = "";
            }

            $data["contract"] = $this->input->post("contract");
            $data["constract_drawing1"]= $this->input->post("constract_drawing1");
            $data["constract_drawing2"]= $this->input->post("constract_drawing2");
            $data["constract_drawing3"]= $this->input->post("constract_drawing3");
            $data["constract_drawing4"]= $this->input->post("constract_drawing4");
            $data["constract_drawing5"]= $this->input->post("constract_drawing5");
            $data["constract_drawing6"]= $this->input->post("constract_drawing6");
            $data["constract_drawing7"]= $this->input->post("constract_drawing7");
            $data["constract_drawing8"]= $this->input->post("constract_drawing8");
            $data["constract_drawing9"]= $this->input->post("constract_drawing9");

            $data["date_insert_contract1"] = date('Y-m-d');
            $data["date_insert_drawing1"] = date('Y-m-d');
            $data["date_insert_drawing2"] = date('Y-m-d');
            $data["date_insert_drawing3"] = date('Y-m-d');
            $data["date_insert_drawing4"] = date('Y-m-d');
            $data["date_insert_drawing5"] = date('Y-m-d');
            $data["date_insert_drawing6"] = date('Y-m-d');
            $data["date_insert_drawing7"] = date('Y-m-d');
            $data["date_insert_drawing8"] = date('Y-m-d');
            $data["date_insert_drawing9"] = date('Y-m-d');

            $this->load->model('customer_model', "customer");
            $data["data_employ"] = $this->customer->fetch_employees_names();
            $data["data"] = $this->customer->fetch_affiliate_names();

            $this->load->view('dashboard/constructor/preview_ins_v',$data);

        }
        else
        {
            $data = [
                "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
                "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
                "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
                "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
                "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
                "contract_date" => $this->input->post("contract_date"),
                "construction_start_date" => $this->input->post("construction_start_date"),
                "upper_building_date" => $this->input->post("upper_building_date"),
                "completion_date" => $this->input->post("completion_date"),
                "delivery_date" => $this->input->post("delivery_date"),
                "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
                "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
                "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
                "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
                "check1" => $this->input->post("check1"),
                "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
                "check2" => $this->input->post("check2"),
                "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
                "check3" => $this->input->post("check3"),
                "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
                "check4" => $this->input->post("check4"),
                "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
                "check5" => $this->input->post("check5"),
                "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
                "check6" => $this->input->post("check6"),
                "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
                "check7" => $this->input->post("check7"),
                "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
                "check8" => $this->input->post("check8"),
                "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
                "check9" => $this->input->post("check9"),
                "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
            ];
    
            if (!empty($this->input->post("co_owner1")))
            {
                $data["co_owner1"] = $this->input->post("co_owner1");
            }
            else
            {
                $data["co_owner1"] = "";
            }
            
            if (!empty($this->input->post("co_owner2")))
            {
                $data["co_owner2"] = $this->input->post("co_owner2");
            }
            else
            {
                $data["co_owner2"] = "";
            }
            
            if (!empty($this->input->post("co_owner3")))
            {
                $data["co_owner3"] = $this->input->post("co_owner3");
            }
            else
            {
                $data["co_owner3"] = "";
            }
            
            if (!empty($this->input->post("co_owner4")))
            {
                $data["co_owner4"] = $this->input->post("co_owner4");
            }
            else
            {
                $data["co_owner4"] = "";
            }
            
            if (!empty($this->input->post("co_owner5")))
            {
                $data["co_owner5"] = $this->input->post("co_owner5");
            }
            else
            {
                $data["co_owner5"] = "";
            }
            
            if (!empty($this->input->post("co_owner6")))
            {
                $data["co_owner6"] = $this->input->post("co_owner6");
            }
            else
            {
                $data["co_owner6"] = "";
            }
            
            if (!empty($this->input->post("co_owner7")))
            {
                $data["co_owner7"] = $this->input->post("co_owner7");
            }
            else
            {
                $data["co_owner7"] = "";
            }
            
            if (!empty($this->input->post("co_owner8")))
            {
                $data["co_owner8"] = $this->input->post("co_owner8");
            }
            else
            {
                $data["co_owner8"] = "";
            }
            
            if (!empty($this->input->post("co_owner9")))
            {
                $data["co_owner9"] = $this->input->post("co_owner9");
            }
            else
            {
                $data["co_owner9"] = "";
            }

            if(isset($_FILES)) {
    
                $config['upload_path'] = APPPATH.'../uploads/files'; 
                $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
                $config['max_size'] = '50000'; // in KlioBytes

                if($_FILES['contract']['size']!=0)
                {
                    $new_name1 = time()."_contract";
                    $config['file_name'] = $new_name1;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('contract'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["contract"]= $filename1;
                        $data["date_insert_contract1"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["contract"]= $this->input->post("contract");
                }

                if($_FILES['constract_drawing1']['size']!=0)
                {
                    $new_names1 = time()."_contract_drawing1";
                    $config['file_name'] = $new_names1;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing1'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing1"]= $filename1;
                        $data["date_insert_drawing1"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing1"]= $this->input->post("constract_drawing1");
                }
    
                if($_FILES['constract_drawing2']['size']!=0)
                {
                    $new_names2 = time()."_contract_drawing2";
                    $config['file_name'] = $new_names2;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing2'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing2"]= $filename1;
                        $data["date_insert_drawing2"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing2"]= $this->input->post("constract_drawing2");
                }
    
                if($_FILES['constract_drawing3']['size']!=0)
                {
                    $new_names3 = time()."_contract_drawing3";
                    $config['file_name'] = $new_names3;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing3'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing3"]= $filename1;
                        $data["date_insert_drawing3"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing3"]= $this->input->post("constract_drawing3");
                }
    
                if($_FILES['constract_drawing4']['size']!=0)
                {
                    $new_names4 = time()."_contract_drawing4";
                    $config['file_name'] = $new_names4;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing4'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing4"]= $filename1;
                        $data["date_insert_drawing4"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing4"]= $this->input->post("constract_drawing4");
                }
    
                if($_FILES['constract_drawing5']['size']!=0)
                {
                    $new_names5 = time()."_contract_drawing5";
                    $config['file_name'] = $new_names5;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing5'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing5"]= $filename1;
                        $data["date_insert_drawing5"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing5"]= $this->input->post("constract_drawing5");
                }
    
                if($_FILES['constract_drawing6']['size']!=0)
                {
                    $new_names6 = time()."_contract_drawing6";
                    $config['file_name'] = $new_names6;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing6'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing6"]= $filename1;
                        $data["date_insert_drawing6"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing6"]= $this->input->post("constract_drawing6");
                }
    
                if($_FILES['constract_drawing7']['size']!=0)
                {
                    $new_names7 = time()."_contract_drawing7";
                    $config['file_name'] = $new_names7;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing7'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing7"]= $filename1;
                        $data["date_insert_drawing7"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing7"]= $this->input->post("constract_drawing7");
                }
    
                if($_FILES['constract_drawing8']['size']!=0)
                {
                    $new_names8 = time()."_contract_drawing8";
                    $config['file_name'] = $new_names8;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing8'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing8"]= $filename1;
                        $data["date_insert_drawing8"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing8"]= $this->input->post("constract_drawing8");
                }
    
                
                if($_FILES['constract_drawing9']['size']!=0)
                {
                    $new_names9 = time()."_contract_drawing9";
                    $config['file_name'] = $new_names9;
                    $this->load->library('upload',$config); 
                    if ( ! $this->upload->do_upload('constract_drawing9'))
                    {
                        print_r($this->upload->display_errors());
                        die();
                    }
                    else
                    {
                        $uploadData1 = $this->upload->data();
                        $filename1 = $uploadData1['file_name'];
                        $data["constract_drawing9"]= $filename1;
                        $data["date_insert_drawing9"] = date('Y-m-d');
                    }
                }
                else
                {
                    $data["constract_drawing9"]= $this->input->post("constract_drawing9");
                }
    
            }


            $this->load->model('customer_model',"customer");
            $insert = $this->customer->insert_constructor_new($data);
            if($insert)
            {
                $this->record_action("add_constructor","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/constructor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'constructor insertion was failed.');
                redirect("dashboard/constructor_list");
            }
        }
    }


    //-------------------------------------------------------------------------//


    public function edit_customer($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }


    //---------------------------------------NEW-----------------------------------------//


    public function edit_customers($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_customers_new($id)[0];
        $data["dataks"] = $this->customer->fetch_employees_names();
        $data["for_bank"] = $this->customer->fetch_finances();
        $this->load->view('dashboard/customers/edit_v',$data);
    }

    public function edit_client($id)
    {
        $this->record_action("edit_client","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_client($id)[0];
        $this->load->view('dashboard/customer/edit_v',$data);
    }

    public function edit_employees($id)
    {
        $this->record_action("edit_employees","view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_one_employee_new($id)[0];
        $this->load->view('dashboard/employees/edit_v',$data);
    }


    public function edit_contractor($id)
    {
        $this->record_action("edit_customer","view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["data"] = $this->customer->fetch_one_contractor_new($id)[0];
        $data["weeee"] = $this->customer->fetch_affiliate_names();
        $data["data_user"] = $this->user->fetch_one_user_by($data["data"]);
        $this->load->view('dashboard/contractor/edit_v',$data);

        // echo json_encode($data["data"],JSON_UNESCAPED_UNICODE);
    }

    public function edit_construction($id)
    {
        $this->record_action("edit_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data_employ"] = $this->customer->fetch_employees_names();
        $data["affiliate"] = $this->customer->fetch_affiliate_names();
        $data["data"] = $this->customer->fetch_one_construction_new($id)[0];
        $this->load->view('dashboard/constructor/edit_v',$data);
    }


    //-----------------------------------------------------------------------//


    public function update_customer($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "no" => $this->input->post("no"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => $this->input->post("postal_code"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("street"),"ksa"),
            "phone" => $this->input->post("phone")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            redirect("dashboard/customer_list");
        }
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customer($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer update was failed.');
            redirect("dashboard/customer_list");
        }
    }

    //---------------------------------------NEW-----------------------------------------//

    public function update_customers($id)
    {
        // print_r($_POST);die();
        $data = [
            // "gid" => $this->input->post("gid"),
            "cust_code" => $this->input->post("cust_code"),
            "氏名" => $this->input->post("name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住　　　所" => $this->input->post("address"),
            "共同住宅" => $this->input->post("building_name"),
            "電　　話" => $this->input->post("phone_number"),
            "種別" => $this->input->post("kinds"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("calendar"),
            "メールアドレス" => $this->input->post("email"),
            "生年月日" => $this->input->post("birthday"),
            "営業担当" => $this->input->post("sales_staff"),
            "工務担当" => $this->input->post("pic_construction"),
            "備考" => $this->input->post("marks"),
            "氏名_かな" => $this->input->post("name_kana"),
            "コーデ担当" => $this->input->post("coordinator"),
            "奥様名前" => $this->input->post("name_wife"),
            "奥様名前_かな" => $this->input->post("name_wife_kana"),
            "奥様生年月日" => $this->input->post("date_birth_wife"),
            "お子様1名前" => $this->input->post("name_child1"),
            "お子様1名前_かな" => $this->input->post("name_child1_kana"),
            "お子様1生年月日" => $this->input->post("birth_child1"),
            "お子様2名前" => $this->input->post("name_child2"),
            "お子様2名前_かな" => $this->input->post("name_child2_kana"),
            "お子様2生年月日" => $this->input->post("birth_child2"),
            "お子様3名前" => $this->input->post("name_child3"),
            "お子様3名前_かな" => $this->input->post("name_child3_kana"),
            "お子様3生年月日" => $this->input->post("birth_child3"),
            "長期優良住宅" => $this->input->post("longterm"),
            "設備10年保証加入" => $this->input->post("warranty"),
            "引渡日" => $this->input->post("delivery_date"),
            "定期点検項目_3ヶ月" => $this->input->post("inspection_time1"),
            "定期点検項目_6ヶ月" => $this->input->post("inspection_time2"),
            "定期点検項目_1年" => $this->input->post("inspection_time3"),
            "定期点検項目_2年" => $this->input->post("inspection_time4"),
            "定期点検項目_3年" => $this->input->post("inspection_time5"),
            "定期点検項目_5年" => $this->input->post("inspection_time6"),
            "定期点検項目_10年" => $this->input->post("inspection_time7"),
            "借入金融機関" => $this->input->post("bank"),
            "火災保険" => $this->input->post("insurance_company"),
            "火災保険備考" => $this->input->post("insurance"),
            "グリーン化事業補助金" => $this->input->post("subsidy"),
            "地盤改良工事" => $this->input->post("ground_improve"),
            "感謝祭" => $this->input->post("thanksgiving"),
            "イベントDM" => $this->input->post("dm")
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        }
        else 
        {
            print_r(is_numeric($this->input->post("latitude")));
            $this->session->set_flashdata('error', '緯度経度を登録してください');
            redirect("dashboard/customers_list");
        }

        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_customers_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_customers/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer update was failed.');
            redirect("dashboard/customers_list");
        }
    }
    
    public function update_client($id)
    {
        $data = [
            "氏名" => $this->input->post("full_name"),
            "連名" => $this->input->post("joint_name"),
            "〒" => $this->input->post("zip_code"),
            "都道府県" => $this->input->post("prefecture"),
            "住所" => $this->input->post("street"),
            "共同住宅" => $this->input->post("apart_name"),
            "電話" => $this->input->post("phone"),
            "種別" => $this->input->post("kind"),
            "初回接点" => $this->input->post("first_contact"),
            "初回年月日" => $this->input->post("first_date"),
            "初回担当" => $this->input->post("first_time_change"),
            "担当" => $this->input->post("pic_person"),
            "土地　有無" => $this->input->post("with_or_without"),
            "訪問" => $this->input->post("visit"),
            "ＤＭ" => $this->input->post("dm"),
            "ランク" => $this->input->post("rank"),
            "備考" => $this->input->post("remarks"),
            "氏名_かな" => $this->input->post("full_name_kana")
        ];
        // if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        // {
            $data["b"] = $this->input->post("latitude");
            $data["l"] = $this->input->post("longitude");
        // }
        // else 
        // {
        //     print_r(is_numeric($this->input->post("latitude")));
        //     $this->session->set_flashdata('error', '緯度経度を登録してください');
        //     redirect("dashboard/client_list");
        // }
        $this->load->model('customer_model',"customer");
        $insert = $this->customer->update_client_new($data,$id);
        if($insert)
        {
            $this->record_action("add_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_client/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', 'Client insertion was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function update_employees($id)
    {
        // print_r($_POST);die();
        $data = [
            "社員コード" => $this->input->post("employee_code"),
            "姓" => $this->input->post("last_name"),
            "名" => $this->input->post("first_name"),
            "せい" => $this->input->post("last_furigana"),
            "めい" => $this->input->post("first_furigana"),
            "保有資格1" => $this->input->post("qualification_1"),
            "保有資格2" => $this->input->post("qualification_2"),
            "保有資格3" => $this->input->post("qualification_3"),
            "保有資格4" => $this->input->post("qualification_4"),
            "保有資格5" => $this->input->post("qualification_5"),
            "入社日" => $this->input->post("hire_date"),
            "郵便番号1" => $this->input->post("zip_code"),
            "住所1" => $this->input->post("address_line"),
            "住所1番地" => $this->input->post("address"),
            "電話番号" => $this->input->post("telephone_number"),
            "携帯番号" => $this->input->post("cellphone_number")
        ];
        $this->load->model('customer_model', "customer");
        $update = $this->customer->update_employees_new($data,$id);
        if($update)
        {
            
            $this->record_action("edit_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/view_employees/".$id);
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees update was failed.');
            redirect("dashboard/employees_list");
        }
    }

    public function update_contractor($id)
    {
        if(!empty($this->input->post("kinds")) && empty($this->input->post('username')))
        {
            // print_r($_POST);die();
            $data = [
                "種別" => $this->input->post("kinds"),
                "no" => $this->input->post("no"),
                "name" => $this->input->post("name"),
                "〒" => $this->input->post("zip_code"),
                "都道府県" => $this->input->post("prefecture"),
                "住　　　所" => $this->input->post("address"),
                "住所2" => $this->input->post("address2"),
                "電　　話" => $this->input->post("telephone")
            ];
            if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
            {
                $data["b"] = $this->input->post("latitude");
                $data["l"] = $this->input->post("longitude");
            }
            else 
            {
                print_r(is_numeric($this->input->post("latitude")));
                $this->session->set_flashdata('error', '緯度経度を登録してください');
                redirect("dashboard/contractor_list");
            }
            $this->load->model('customer_model', "customer");
            $update = $this->customer->update_contractor_new($data,$id);
            if($update)
            {
                
                $this->record_action("edit_customer","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/view_contractor/".$id);
            }
            else
            {
                $this->session->set_flashdata('error', 'Contractor update was failed.');
                redirect("dashboard/contractor_list");
            }
        }
        elseif(empty($this->input->post("kinds")) && !empty($this->input->post('username')))
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('name');
            $data['date_regist'] = date('Y-m-d');
            
            $this->load->model('users_model',"user");
            $insert = $this->user->update_user($data,$id);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'User insertion was failed.');
                redirect("dashboard/contractor_list");
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Something was Error.');
            redirect("dashboard/contractor_list");
        }

       
    }

    public function update_construction($id)
    {
        $data = [
            "cust_code" => mb_convert_kana($this->input->post("cust_code"), "as"),
            "cust_name" => mb_convert_kana($this->input->post("cust_name"), "as"),
            "kinds" => mb_convert_kana($this->input->post("kinds"), "as"),
            "const_no" => mb_convert_kana($this->input->post("const_no"), "as"),
            "remarks" => mb_convert_kana($this->input->post("remarks"), "as"),
            "contract_date" => $this->input->post("contract_date"),
            "construction_start_date" => $this->input->post("construction_start_date"),
            "upper_building_date" => $this->input->post("upper_building_date"),
            "completion_date" => $this->input->post("completion_date"),
            "delivery_date" => $this->input->post("delivery_date"),
            "amount_money" => mb_convert_kana($this->input->post("amount_money"), "as"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"), "as"),
            "incharge_construction" => mb_convert_kana($this->input->post("incharge_construction"), "as"),
            "incharge_coordinator" => mb_convert_kana($this->input->post("incharge_coordinator"), "as"),
            "check1" => $this->input->post("check1"),
            "desc_item1" => mb_convert_kana($this->input->post("desc_item1"), "as"),
            "check2" => $this->input->post("check2"),
            "desc_item2" => mb_convert_kana($this->input->post("desc_item2"), "as"),
            "check3" => $this->input->post("check3"),
            "desc_item3" => mb_convert_kana($this->input->post("desc_item3"), "as"),
            "check4" => $this->input->post("check4"),
            "desc_item4" => mb_convert_kana($this->input->post("desc_item4"), "as"),
            "check5" => $this->input->post("check5"),
            "desc_item5" => mb_convert_kana($this->input->post("desc_item5"), "as"),
            "check6" => $this->input->post("check6"),
            "desc_item6" => mb_convert_kana($this->input->post("desc_item6"), "as"),
            "check7" => $this->input->post("check7"),
            "desc_item7" => mb_convert_kana($this->input->post("desc_item7"), "as"),
            "check8" => $this->input->post("check8"),
            "desc_item8" => mb_convert_kana($this->input->post("desc_item8"), "as"),
            "check9" => $this->input->post("check9"),
            "desc_item9" => mb_convert_kana($this->input->post("desc_item9"), "as")
        ];

        if (!empty($this->input->post("co_owner1")))
        {
            $data["co_owner1"] = implode(";",$this->input->post("co_owner1"));
        }
        else
        {
            $data["co_owner1"] = "";
        }
        
        if (!empty($this->input->post("co_owner2")))
        {
            $data["co_owner2"] = implode(";",$this->input->post("co_owner2"));
        }
        else
        {
            $data["co_owner2"] = "";
        }
        
        if (!empty($this->input->post("co_owner3")))
        {
            $data["co_owner3"] = implode(";",$this->input->post("co_owner3"));
        }
        else
        {
            $data["co_owner3"] = "";
        }
        
        if (!empty($this->input->post("co_owner4")))
        {
            $data["co_owner4"] = implode(";",$this->input->post("co_owner4"));
        }
        else
        {
            $data["co_owner4"] = "";
        }
        
        if (!empty($this->input->post("co_owner5")))
        {
            $data["co_owner5"] = implode(";",$this->input->post("co_owner5"));
        }
        else
        {
            $data["co_owner5"] = "";
        }
        
        if (!empty($this->input->post("co_owner6")))
        {
            $data["co_owner6"] = implode(";",$this->input->post("co_owner6"));
        }
        else
        {
            $data["co_owner6"] = "";
        }
        
        if (!empty($this->input->post("co_owner7")))
        {
            $data["co_owner7"] = implode(";",$this->input->post("co_owner6"));
        }
        else
        {
            $data["co_owner7"] = "";
        }
        
        if (!empty($this->input->post("co_owner8")))
        {
            $data["co_owner8"] = implode(";",$this->input->post("co_owner8"));
        }
        else
        {
            $data["co_owner8"] = "";
        }
        
        if (!empty($this->input->post("co_owner9")))
        {
            $data["co_owner9"] = implode(";",$this->input->post("co_owner9"));
        }
        else
        {
            $data["co_owner9"] = "";
        }

        if(isset($_FILES)) {

            $config['upload_path'] = APPPATH.'../uploads/files'; 
            $config['allowed_types'] = 'pdf|csv|xls|xlsx|docx|doc|ppt|pptx';
            $config['max_size'] = '50000'; // in KlioBytes

            if(!empty($_FILES['contract']['size']) && empty($_FILES['file_replace']['size']))
            {
                $new_name1 = time()."_contract";
                $config['file_name'] = $new_name1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('contract'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["contract"]= $filename1;
                    $data["date_insert_contract1"] = date('Y-m-d');
                }
            }

            if(!empty($_FILES['file_replace']['size']) && empty($_FILES['contract']['size']))
            {
                $new_name1 = time()."_contract";
                $config['file_name'] = $new_name1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_contract()[0];
                    $date_insert = $this->customer->get_last_date_contract()[0];

                    $data_history_contract = $this->customer->get_last_historycontract()[0];
                    $date_history_insert = $this->customer->get_last_historydate_contract()[0];



                    if(empty($data_history_contract->contract_history))
                    {
                        $combine = $data_contract->contract;
                    }
                    else
                    {
                        $combine = $data_history_contract->contract_history.';'.$data_contract->contract;
                    }


                    if(empty($date_history_insert->date_insert_history1))
                    {
                        $date_combine = $date_insert->date_insert_contract1;
                    }
                    else
                    {
                        $date_combine = $date_history_insert->date_insert_history1.';'.$date_insert->date_insert_contract1;
                    }
                    

                    $data["contract"]= $filename1;
                    $data["date_insert_contract1"] = date('Y-m-d');
                    $data["contract_history"] = $combine;
                    $data["date_insert_history1"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing1']['size']) && empty($_FILES['file_replace1']['size']))
            {
                $new_names1 = time()."_contract_drawing1";
                $config['file_name'] = $new_names1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing1'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing1"]= $filename1;
                    $data["date_insert_drawing1"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace1']['size']) && empty($_FILES['constract_drawing1']['size']))
            {
                $new_names1 = time()."_contract_drawing1";
                $config['file_name'] = $new_names1;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace1'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing1()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing1()[0];

                    $data_contract_drawing1_history = $this->customer->get_last_historydrawing1()[0];
                    $date_history_drawing1 = $this->customer->get_last_historydate_drawing1()[0];



                    if(empty($data_contract_drawing1_history->contract_drawing1_history))
                    {
                        $combine = $data_contract->constract_drawing1;
                    }
                    else
                    {
                        $combine = $data_contract_drawing1_history->contract_drawing1_history.';'.$data_contract->constract_drawing1;
                    }


                    if(empty($date_history_drawing1->date_insert_history2))
                    {
                        $date_combine = $date_insert->date_insert_drawing1;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing1->date_insert_history2.';'.$date_insert->date_insert_drawing1;
                    }
                    

                    $data["constract_drawing1"]= $filename1;
                    $data["date_insert_drawing1"] = date('Y-m-d');
                    $data["contract_drawing1_history"] = $combine;
                    $data["date_insert_history2"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing2']['size']) && empty($_FILES['file_replace2']['size']))
            {
                $new_names2 = time()."_contract_drawing2";
                $config['file_name'] = $new_names2;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing2'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing2"]= $filename1;
                    $data["date_insert_drawing2"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace2']['size']) && empty($_FILES['constract_drawing2']['size']))
            {
                $new_names2 = time()."_contract_drawing2";
                $config['file_name'] = $new_names2;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace2'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing2()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing2()[0];

                    $data_contract_drawing2_history = $this->customer->get_last_historydrawing2()[0];
                    $date_history_drawing2 = $this->customer->get_last_historydate_drawing2()[0];



                    if(empty($data_contract_drawing2_history->contract_drawing2_history))
                    {
                        $combine = $data_contract->constract_drawing2;
                    }
                    else
                    {
                        $combine = $data_contract_drawing2_history->contract_drawing2_history.';'.$data_contract->constract_drawing2;
                    }


                    if(empty($date_history_drawing2->date_insert_history2))
                    {
                        $date_combine = $date_insert->date_insert_drawing2;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing2->date_insert_history3.';'.$date_insert->date_insert_drawing2;
                    }
                    

                    $data["constract_drawing2"]= $filename1;
                    $data["date_insert_drawing2"] = date('Y-m-d');
                    $data["contract_drawing2_history"] = $combine;
                    $data["date_insert_history3"] = $date_combine;
                }
            }


            if(!empty($_FILES['constract_drawing3']['size']) && empty($_FILES['file_replace3']['size']))
            {
                $new_names3 = time()."_contract_drawing3";
                $config['file_name'] = $new_names3;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing3'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing3"]= $filename1;
                    $data["date_insert_drawing3"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace3']['size']) && empty($_FILES['constract_drawing3']['size']))
            {
                $new_names3 = time()."_contract_drawing3";
                $config['file_name'] = $new_names3;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace3'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing3()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing3()[0];

                    $data_contract_drawing3_history = $this->customer->get_last_historydrawing3()[0];
                    $date_history_drawing3 = $this->customer->get_last_historydate_drawing3()[0];



                    if(empty($data_contract_drawing3_history->contract_drawing3_history))
                    {
                        $combine = $data_contract->constract_drawing3;
                    }
                    else
                    {
                        $combine = $data_contract_drawing3_history->contract_drawing3_history.';'.$data_contract->constract_drawing3;
                    }


                    if(empty($date_history_drawing3->date_insert_history4))
                    {
                        $date_combine = $date_insert->date_insert_drawing3;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing3->date_insert_history4.';'.$date_insert->date_insert_drawing3;
                    }
                    

                    $data["constract_drawing3"]= $filename1;
                    $data["date_insert_drawing3"] = date('Y-m-d');
                    $data["contract_drawing3_history"] = $combine;
                    $data["date_insert_history4"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing4']['size']) && empty($_FILES['file_replace4']['size']))
            {
                $new_names4 = time()."_contract_drawing4";
                $config['file_name'] = $new_names4;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing4'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing4"]= $filename1;
                    $data["date_insert_drawing4"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace4']['size']) && empty($_FILES['constract_drawing4']['size']))
            {
                $new_names4 = time()."_contract_drawing4";
                $config['file_name'] = $new_names4;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace4'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing4()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing4()[0];

                    $data_contract_drawing4_history = $this->customer->get_last_historydrawing4()[0];
                    $date_history_drawing4 = $this->customer->get_last_historydate_drawing4()[0];



                    if(empty($data_contract_drawing4_history->contract_drawing4_history))
                    {
                        $combine = $data_contract->constract_drawing4;
                    }
                    else
                    {
                        $combine = $data_contract_drawing4_history->contract_drawing4_history.';'.$data_contract->constract_drawing4;
                    }


                    if(empty($date_history_drawing4->date_insert_history5))
                    {
                        $date_combine = $date_insert->date_insert_drawing4;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing4->date_insert_history5.';'.$date_insert->date_insert_drawing4;
                    }
                    

                    $data["constract_drawing4"]= $filename1;
                    $data["date_insert_drawing4"] = date('Y-m-d');
                    $data["contract_drawing4_history"] = $combine;
                    $data["date_insert_history5"] = $date_combine;
                }
            }


            if(!empty($_FILES['constract_drawing5']['size']) && empty($_FILES['file_replace5']['size']))
            {
                $new_names5 = time()."_contract_drawing5";
                $config['file_name'] = $new_names5;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing5'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing5"]= $filename1;
                    $data["date_insert_drawing5"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace5']['size']) && empty($_FILES['constract_drawing5']['size']))
            {
                $new_names5 = time()."_contract_drawing5";
                $config['file_name'] = $new_names5;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace5'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing5()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing5()[0];

                    $data_contract_drawing5_history = $this->customer->get_last_historydrawing5()[0];
                    $date_history_drawing5 = $this->customer->get_last_historydate_drawing5()[0];



                    if(empty($data_contract_drawing5_history->contract_drawing5_history))
                    {
                        $combine = $data_contract->constract_drawing5;
                    }
                    else
                    {
                        $combine = $data_contract_drawing5_history->contract_drawing5_history.';'.$data_contract->constract_drawing5;
                    }


                    if(empty($date_history_drawing5->date_insert_history6))
                    {
                        $date_combine = $date_insert->date_insert_drawing5;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing5->date_insert_history6.';'.$date_insert->date_insert_drawing5;
                    }
                    

                    $data["constract_drawing5"]= $filename1;
                    $data["date_insert_drawing5"] = date('Y-m-d');
                    $data["contract_drawing5_history"] = $combine;
                    $data["date_insert_history6"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing6']['size']) && empty($_FILES['file_replace6']['size']))
            {
                $new_names6 = time()."_contract_drawing6";
                $config['file_name'] = $new_names6;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing6'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing6"]= $filename1;
                    $data["date_insert_drawing6"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace6']['size']) && empty($_FILES['constract_drawing6']['size']))
            {
                $new_names6 = time()."_contract_drawing6";
                $config['file_name'] = $new_names6;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace6'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing6()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing6()[0];

                    $data_contract_drawing6_history = $this->customer->get_last_historydrawing6()[0];
                    $date_history_drawing6 = $this->customer->get_last_historydate_drawing6()[0];



                    if(empty($data_contract_drawing6_history->contract_drawing6_history))
                    {
                        $combine = $data_contract->constract_drawing6;
                    }
                    else
                    {
                        $combine = $data_contract_drawing6_history->contract_drawing6_history.';'.$data_contract->constract_drawing6;
                    }


                    if(empty($date_history_drawing6->date_insert_history7))
                    {
                        $date_combine = $date_insert->date_insert_drawing6;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing6->date_insert_history7.';'.$date_insert->date_insert_drawing6;
                    }
                    

                    $data["constract_drawing6"]= $filename1;
                    $data["date_insert_drawing6"] = date('Y-m-d');
                    $data["contract_drawing6_history"] = $combine;
                    $data["date_insert_history7"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing7']['size']) && empty($_FILES['file_replace7']['size']))
            {
                $new_names7 = time()."_contract_drawing7";
                $config['file_name'] = $new_names7;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing7'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing7"]= $filename1;
                    $data["date_insert_drawing7"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace7']['size']) && empty($_FILES['constract_drawing7']['size']))
            {
                $new_names7 = time()."_contract_drawing7";
                $config['file_name'] = $new_names7;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace7'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing7()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing7()[0];

                    $data_contract_drawing7_history = $this->customer->get_last_historydrawing7()[0];
                    $date_history_drawing7 = $this->customer->get_last_historydate_drawing7()[0];



                    if(empty($data_contract_drawing7_history->contract_drawing7_history))
                    {
                        $combine = $data_contract->constract_drawing7;
                    }
                    else
                    {
                        $combine = $data_contract_drawing7_history->contract_drawing7_history.';'.$data_contract->constract_drawing7;
                    }


                    if(empty($date_history_drawing7->date_insert_history8))
                    {
                        $date_combine = $date_insert->date_insert_drawing7;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing7->date_insert_history8.';'.$date_insert->date_insert_drawing7;
                    }
                    

                    $data["constract_drawing7"]= $filename1;
                    $data["date_insert_drawing7"] = date('Y-m-d');
                    $data["contract_drawing7_history"] = $combine;
                    $data["date_insert_history8"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing8']['size']) && empty($_FILES['file_replace8']['size']))
            {
                $new_names8 = time()."_contract_drawing8";
                $config['file_name'] = $new_names8;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing8'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing8"]= $filename1;
                    $data["date_insert_drawing8"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace8']['size']) && empty($_FILES['constract_drawing8']['size']))
            {
                $new_names8 = time()."_contract_drawing8";
                $config['file_name'] = $new_names8;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace8'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing8()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing8()[0];

                    $data_contract_drawing8_history = $this->customer->get_last_historydrawing8()[0];
                    $date_history_drawing8 = $this->customer->get_last_historydate_drawing8()[0];



                    if(empty($data_contract_drawing8_history->contract_drawing8_history))
                    {
                        $combine = $data_contract->constract_drawing8;
                    }
                    else
                    {
                        $combine = $data_contract_drawing8_history->contract_drawing8_history.';'.$data_contract->constract_drawing8;
                    }


                    if(empty($date_history_drawing8->date_insert_history9))
                    {
                        $date_combine = $date_insert->date_insert_drawing8;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing7->date_insert_history9.';'.$date_insert->date_insert_drawing8;
                    }
                    

                    $data["constract_drawing8"]= $filename1;
                    $data["date_insert_drawing8"] = date('Y-m-d');
                    $data["contract_drawing8_history"] = $combine;
                    $data["date_insert_history9"] = $date_combine;
                }
            }

            if(!empty($_FILES['constract_drawing9']['size']) && empty($_FILES['file_replace8']['size']))
            {
                $new_names9 = time()."_contract_drawing9";
                $config['file_name'] = $new_names9;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('constract_drawing9'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];
                    $data["constract_drawing9"]= $filename1;
                    $data["date_insert_drawing9"] = date('Y-m-d');
                }
            }

            
            if(!empty($_FILES['file_replace9']['size']) && empty($_FILES['constract_drawing9']['size']))
            {
                $new_names9 = time()."_contract_drawing9";
                $config['file_name'] = $new_names9;
                $this->load->library('upload',$config); 
                if ( ! $this->upload->do_upload('file_replace9'))
                {
                    print_r($this->upload->display_errors());
                    die();
                }
                else
                {
                    $uploadData1 = $this->upload->data();
                    $filename1 = $uploadData1['file_name'];

                    $this->load->model('customer_model', "customer");
                    $data_contract = $this->customer->get_last_constract_drawing9()[0];
                    $date_insert = $this->customer->get_last_date_constract_drawing9()[0];

                    $data_contract_drawing9_history = $this->customer->get_last_historydrawing9()[0];
                    $date_history_drawing9 = $this->customer->get_last_historydate_drawing9()[0];



                    if(empty($data_contract_drawing9_history->contract_drawing9_history))
                    {
                        $combine = $data_contract->constract_drawing9;
                    }
                    else
                    {
                        $combine = $data_contract_drawing9_history->contract_drawing8_history.';'.$data_contract->constract_drawing9;
                    }


                    if(empty($date_history_drawing9->date_insert_history10))
                    {
                        $date_combine = $date_insert->date_insert_drawing9;
                    }
                    else
                    {
                        $date_combine = $date_history_drawing9->date_insert_history10.';'.$date_insert->date_insert_drawing9;
                    }
                    

                    $data["constract_drawing9"]= $filename1;
                    $data["date_insert_drawing9"] = date('Y-m-d');
                    $data["contract_drawing9_history"] = $combine;
                    $data["date_insert_history10"] = $date_combine;
                }
            }

        }







            // if($_FILES['contract']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('contract'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["contract"]= $filename1;
            //         $data["date_insert_contract1"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing1']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing1'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing1"]= $filename1;
            //         $data["date_insert_drawing1"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing2']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing2'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing2"]= $filename1;
            //         $data["date_insert_drawing2"] = date('Y-m-d');
                    
            //     }
            // }

            // if($_FILES['constract_drawing3']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing3'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing3"]= $filename1;
            //         $data["date_insert_drawing3"] = date('Y-m-d');

            //     }
            // }

            // if($_FILES['constract_drawing4']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing4'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing4"]= $filename1;
            //         $data["date_insert_drawing4"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing5']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing5'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing5"]= $filename1;
            //         $data["date_insert_drawing5"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing6']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing6'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing6"]= $filename1;
            //         $data["date_insert_drawing6"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing7']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing7'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing7"]= $filename1;
            //         $data["date_insert_drawing7"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['constract_drawing8']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing8'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing8"]= $filename1;
            //         $data["date_insert_drawing8"] = date('Y-m-d');
            //     }
            // }
            
            // if($_FILES['constract_drawing9']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('constract_drawing9'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["constract_drawing9"]= $filename1;
            //         $data["date_insert_drawing9"] = date('Y-m-d');
            //     }
            // }

            // if($_FILES['file_replace']['size']!=0)
            // {
            //     $this->load->library('upload',$config); 
            //     if ( ! $this->upload->do_upload('file_replace'))
            //     {
            //         print_r($this->upload->display_errors());
            //         die();
            //     }
            //     else
            //     {
            //         $uploadData1 = $this->upload->data();
            //         $filename1 = $uploadData1['file_name'];
            //         $data["file_replace"]= $filename1;
            //         $data["date_insert_drawing9"] = date('Y-m-d');
            //     }
            // }

        
        

        // echo json_encode($data, JSON_UNESCAPED_UNICODE);

        $this->load->model('customer_model',"customer");
        $insert = $this->customer->update_constructor_new($data,$id);
        if($insert)
        {
            $this->record_action("add_constructor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/constructor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'constructor update was failed.');
            redirect("dashboard/constructor_list");
        }
    }

    //-----------------------------------------------------------------------//

    public function delete_customer($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customer_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer delete was failed.');
            redirect("dashboard/customer_list");
        }
    }


    //---------------------------------------NEW-----------------------------------------//

    public function delete_customers($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_customers_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/customers_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Customer delete was failed.');
            redirect("dashboard/customers_list");
        }
    }

    public function delete_client($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_client_new($id);
        if($delete)
        {
            
            $this->record_action("delete_client","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/client_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Client delete was failed.');
            redirect("dashboard/client_list");
        }
    }

    public function delete_employees($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_employee_new($id);
        if($delete)
        {
            
            $this->record_action("delete_customer","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/employees_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employees delete was failed.');
            redirect("dashboard/employees_list");
        }
    }

    public function delete_contractor($id)
    {
        $this->load->model('customer_model', "customer");
        $delete = $this->customer->delete_one_contractor_new($id);
        if($delete)
        {
            
            $this->record_action("delete_contractor","execute");
            $this->session->set_flashdata('success', '更新しました');
            redirect("dashboard/contractor_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Contractor delete was failed.');
            redirect("dashboard/contractor_list");
        }
    }

    //-----------------------------------------------------------------------//

    public function inspection_list()
    {
        
        $this->record_action("inspection_list","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_all();
        $this->load->view('dashboard/inspection/list_v',$data);
    }

    public function add_inspection()
    {
        
        $this->record_action("add_inspection","view");
        $this->load->view('dashboard/inspection/insert_v'); 
    }

    public function insert_inspection()
    {
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model',"inspection");
        $insert = $this->inspection->insert_inspection($data);
        if($insert)
        {
            
            $this->record_action("add_inspection","execute");
            $this->session->set_flashdata('success', 'Inpection insertion was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inpection insertion was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function edit_inspection($id)
    {
        
        $this->record_action("edit_inspection","view");
        $this->load->model('inspection_model', "inspection");
        $data["data"] = $this->inspection->fetch_one($id)[0];
        $this->load->view('dashboard/inspection/edit_v',$data);
    }

    public function update_inspection($id)
    {
        // print_r($_POST);die();
        $data = [
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
            "family_name" => mb_convert_kana($this->input->post("family_name"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "sama" => mb_convert_kana($this->input->post("sama"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "inspection_date" => date("Y-m-d", strtotime($this->input->post("inspection_date"))),
            "delivery_date" => date("Y-m-d", strtotime($this->input->post("delivery_date"))),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/inspection_list");
        }
        $this->load->model('inspection_model', "inspection");
        $update = $this->inspection->update_inspection($data,$id);
        if($update)
        {
            
            $this->record_action("edit_inspection", "execute");
            $this->session->set_flashdata('success', 'Inspection update was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection update was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function delete_inspection($id)
    {
        $this->load->model('inspection_model', "inspection");
        $delete = $this->inspection->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_inspection","execute");
            $this->session->set_flashdata('success', 'Inspection delete was successful.');
            redirect("dashboard/inspection_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Inspection delete was failed.');
            redirect("dashboard/inspection_list");
        }
    }

    public function partner_list()
    {
        
        $this->record_action("partner_list","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_all();
        $this->load->view('dashboard/partner/list_v',$data);
    }

    public function add_partner()
    {
        
        $this->record_action("add_partner","view");
        $this->load->view('dashboard/partner/insert_v');
    }

    public function insert_partner()
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $insert = $this->partner->insert_partner($data);
        if($insert)
        {
            
            $this->record_action("add_partner","execute");
            $this->session->set_flashdata('success', 'Partner insertion was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner insertion was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function edit_partner($id)
    {
        
        $this->record_action("edit_partner","view");
        $this->load->model('partner_model', "partner");
        $data["data"] = $this->partner->fetch_one($id)[0];
        $this->load->view('dashboard/partner/edit_v',$data);
    }

    public function update_partner($id)
    {
        $data = [
            "number" => mb_convert_kana($this->input->post("number"),"ksa"),
            "name" => mb_convert_kana($this->input->post("name"),"ksa"),
            "federation" => mb_convert_kana($this->input->post("federation"),"ksa"),
            "postal_code" => mb_convert_kana($this->input->post("postal_code"),"ksa"),
            "prefecture" => mb_convert_kana($this->input->post("prefecture"),"ksa"),
            "address" => mb_convert_kana($this->input->post("address"),"ksa"),
            "phone" => $this->input->post("phone"),
            "type" => mb_convert_kana($this->input->post("type"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/partner_list");
        }
        $this->load->model('partner_model',"partner");
        $update = $this->partner->update_partner($data,$id);
        if($update)
        {
            
            $this->record_action("edit_partner", "execute");
            $this->session->set_flashdata('success', 'Partner update was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Partner update was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function delete_partner($id)
    {
        $this->load->model('partner_model', "partner");
        $delete = $this->partner->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_partner","execute");
            $this->session->set_flashdata('success', 'Delete Partner was successful.');
            redirect("dashboard/partner_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Partner was failed.');
            redirect("dashboard/partner_list");
        }
    }

    public function public_works_list()
    {
        
        $this->record_action("public_work_list","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_all();
        $this->load->view('dashboard/public_works/list_v',$data);
    }

    public function add_public_works()
    {
        
        $this->record_action("add_public_work","view");
        $this->load->view('dashboard/public_works/insert_v');
    }

    public function insert_public_works()
    {
        // print_r($_FILES);die();
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        
        $config['upload_path'] = FCPATH.'uploads/'; 
        // print_r($config['upload_path']);
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = '50000'; // in KlioBytes
        $this->upload->initialize($config);
        if($_FILES['document_image']['name']!="")
        {
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->load->library('upload',$config); 
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }
        else
        {
            $data['document_image']= null;
        }
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        $this->load->model('public_works_model',"public_works");
        $insert = $this->public_works->insert_public_works($data);
        if($insert)
        {
            
            $this->record_action("add_public_work","execute");
            $this->session->set_flashdata('success', 'Public Works insertion was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Works insertion was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function edit_public_works($id)
    {
        
        $this->record_action("edit_public_work","view");
        $this->load->model('public_works_model', "public_works");
        $data["data"] = $this->public_works->fetch_one($id)[0];
        $this->load->view('dashboard/public_works/edit_v',$data);
    }

    public function update_public_works($id)
    {
        $data = [
            "reference_number" => mb_convert_kana($this->input->post("reference_number"),"ksa"),
            "ordering_party" => mb_convert_kana($this->input->post("ordering_party"),"ksa"),
            "work_type" => mb_convert_kana($this->input->post("work_type"),"ksa"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_site" => mb_convert_kana($this->input->post("construction_site"),"ksa"),
            "route_name" => mb_convert_kana($this->input->post("route_name"),"ksa"),
            "route_number" => mb_convert_kana($this->input->post("route_number"),"ksa"),
            "year" => mb_convert_kana($this->input->post("year"),"ksa"),
            "contract_date" => date("Y-m-d", strtotime($this->input->post("contract_date"))),
            "first_contract_price" => $this->input->post("first_contract_price"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "final_contract_price" => $this->input->post("final_contract_price"),
            "construction_name" => mb_convert_kana($this->input->post("construction_name"),"ksa"),
            "construction_detail" => mb_convert_kana($this->input->post("construction_detail"),"ksa"),
            "construction_result" => mb_convert_kana($this->input->post("construction_result"),"ksa"),
            "lead_engineer" => mb_convert_kana($this->input->post("lead_engineer"),"ksa"),
            "agent" => mb_convert_kana($this->input->post("agent"),"ksa"),
            "construction_staff" => mb_convert_kana($this->input->post("construction_staff"),"ksa"),
            "requester" => mb_convert_kana($this->input->post("requester"),"ksa"),
            "sales_staff" => mb_convert_kana($this->input->post("sales_staff"),"ksa"),
        ];
        if(is_numeric($this->input->post("latitude")) && is_numeric($this->input->post("longitude")))
        {
            $data["latitude"] = $this->input->post("latitude");
            $data["longitude"] = $this->input->post("longitude");
        }
        else 
        {
            $this->session->set_flashdata('error', 'Longitude / Latitude invalid');
            redirect("dashboard/public_works_list");
        }
        
        if($_FILES['document_image']["name"] != "" && $_FILES['document_image']["size"] > 0)
        {
            $config['upload_path'] = FCPATH.'uploads/'; 
            // print_r($config['upload_path']);
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '50000'; // in KlioBytes
            $this->upload->initialize($config);
            $new_name = time()."_document_image";
            $config['file_name'] = $new_name;
            $this->load->library('upload',$config); 
            if ( ! $this->upload->do_upload('document_image'))
            {
                print_r($this->upload->display_errors());
                die();
            }
            else
            {
                //file is uploaded successfully
                //now get the file uploaded data 
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $data["document_image"]= $filename;
            }
        }

        $this->load->model('public_works_model',"public_works");
        $update = $this->public_works->update_public_works($data,$id);
        if($update)
        {
            
            $this->record_action("edit_public_work","execute");
            $this->session->set_flashdata('success', 'Public Work update was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Public Work update was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function delete_public_works($id)
    {
        $this->load->model('public_works_model', "public_works");
        $delete = $this->public_works->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_public_work","execute");
            $this->session->set_flashdata('success', 'Delete Public Work was successful.');
            redirect("dashboard/public_works_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Public Work was failed.');
            redirect("dashboard/public_works_list");
        }
    }

    public function job_master_list()
    {
        
        $this->record_action("job_master_list","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_all();
        $this->load->view('dashboard/job_master/list_v',$data);
    }

    public function add_job_master()
    {
        
        $this->record_action("add_job_master","view");
        $this->load->view('dashboard/job_master/insert_v');
    }

    public function insert_job_master()
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $insert = $this->job_master->insert_job_master($data);
        if($insert)
        {
            
            $this->record_action("add_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master insertion was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master insertion was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function edit_job_master($id)
    {
        
        $this->record_action("edit_job_master","view");
        $this->load->model('job_master_model', "job_master");
        $data["data"] = $this->job_master->fetch_one($id)[0];
        $this->load->view('dashboard/job_master/edit_v',$data);
    }

    public function update_job_master($id)
    {
        $data = [
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
            "job_rank1" => mb_convert_kana($this->input->post("job_rank1"),"ksa"),
            "job_rank2" => mb_convert_kana($this->input->post("job_rank2"),"ksa"),
            "job_name" => mb_convert_kana($this->input->post("job_name"),"ksa"),
        ];
        $this->load->model('job_master_model',"job_master");
        $update = $this->job_master->update_job_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_job_master","execute");
            $this->session->set_flashdata('success', 'Job Master update was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Job Master update was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function delete_job_master($id)
    {
        $this->load->model('job_master_model', "job_master");
        $delete = $this->job_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_job_master","execute");
            $this->session->set_flashdata('success', 'Delete Job Master was successful.');
            redirect("dashboard/job_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Delete Job Master was failed.');
            redirect("dashboard/job_master_list");
        }
    }

    public function assignment_master_list()
    {
        
        $this->record_action("assignment_master_list","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_all();
        $this->load->view('dashboard/assignment_master/list_v',$data);
    }

    public function add_assignment_master()
    {
        
        $this->record_action("add_assignment_master","view");
        $this->load->view('dashboard/assignment_master/insert_v');
    }

    public function insert_assignment_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $insert = $this->assignment_master->insert_assignment_master($data);
        if($insert)
        {
            
            $this->record_action("add_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master insertion was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master insertion was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function edit_assignment_master($id)
    {
        
        $this->record_action("edit_assignment_master","view");
        $this->load->model('assignment_master_model', "assignment_master");
        $data["data"] = $this->assignment_master->fetch_one($id)[0];
        $this->load->view('dashboard/assignment_master/edit_v',$data);
    }

    public function update_assignment_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('assignment_master_model',"assignment_master");
        $update = $this->assignment_master->update_assignment_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master update was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master update was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function delete_assignment_master($id)
    {
        $this->load->model('assignment_master_model', "assignment_master");
        $delete = $this->assignment_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_assignment_master","execute");
            $this->session->set_flashdata('success', 'Assignment Master Master was successful.');
            redirect("dashboard/assignment_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Assignment Master Master was failed.');
            redirect("dashboard/assignment_master_list");
        }
    }

    public function employee_master_list()
    {
        
        $this->record_action("employee_master_list","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_all();
        $this->load->view('dashboard/employee_master/list_v',$data);
    }

    public function add_employee_master()
    {
        
        $this->record_action("add_employee_master","view");
        $this->load->view('dashboard/employee_master/insert_v');
    }

    public function insert_employee_master()
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $insert = $this->employee_master->insert_employee_master($data);
        if($insert)
        {
            
            $this->record_action("add_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master insertion was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master insertion was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function edit_employee_master($id)
    {
        
        $this->record_action("edit_employee_master","view");
        $this->load->model('employee_master_model', "employee_master");
        $data["data"] = $this->employee_master->fetch_one($id)[0];
        $this->load->view('dashboard/employee_master/edit_v',$data);
    }

    public function update_employee_master($id)
    {
        $data = [
            "employee_code" => mb_convert_kana($this->input->post("employee_code"),"ksa"),
            "start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
            "end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
            "department_code" => mb_convert_kana($this->input->post("department_code"),"ksa"),
            "job_code" => mb_convert_kana($this->input->post("job_code"),"ksa"),
        ];
        $this->load->model('employee_master_model',"employee_master");
        $update = $this->employee_master->update_employee_master($data,$id);
        if($update)
        {
            
            $this->record_action("edit_assignment_master","execute");
            $this->session->set_flashdata('success', 'Employee Master update was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master update was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function delete_employee_master($id)
    {
        $this->load->model('employee_master_model', "employee_master");
        $delete = $this->employee_master->delete_one($id);
        if($delete)
        {
            
            $this->record_action("delete_employee_master","execute");
            $this->session->set_flashdata('success', 'Employee Master Master was successful.');
            redirect("dashboard/employee_master_list");
        }
        else
        {
            $this->session->set_flashdata('error', 'Employee Master Master was failed.');
            redirect("dashboard/employee_master_list");
        }
    }

    public function search_data()
    {
        $data["table_data"] = $this->input->post("table");
        $data["full_name"] = $this->input->post("full_name");
        $data["full_name_kana"] = $this->input->post("full_name_kana");
        
        if($this->input->post("table") == 'harada_client')
        {
            $data["住所"] = $this->input->post("address");
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $data["住　　　所"] = $this->input->post("address");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["kinds"] = $this->input->post("kinds");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["営業担当"] = $this->input->post("sales_staff");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["工務担当"] = $this->input->post("pic_construction");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["コーデ担当"] = $this->input->post("coordinator");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["感謝祭"] = $this->input->post("thanksgiving");
        }
        
        if($this->input->post("table") == 'harada_client')
        {
           $data["ＤＭ"] = $this->input->post("dm");
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["イベントDM"] = $this->input->post("dm");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["長期優良住宅"] = $this->input->post("longterm");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["設備10年保証加入"] = $this->input->post("warranty");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["グリーン化事業補助金"] = $this->input->post("subsidy");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["地盤改良工事"] = $this->input->post("ground_improve");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["借入金融機関"] = $this->input->post("bank");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["火災保険"] = $this->input->post("insurance_company");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["引渡日_1"] = $this->input->post("date_from");
           $data["引渡日_2"] = $this->input->post("date_to");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["生年月日_1"] = $this->input->post("birthdate_from");
           $data["生年月日_2"] = $this->input->post("birthdate_to");
        }
        
        if($this->input->post("table") == 'harada_client_ob')
        {
           $data["inspection_from"] = $this->input->post("inspection_from");
           $data["inspection_to"] = $this->input->post("inspection_to");
        }

        $this->load->model('customer_model',"customer");

        if($this->input->post("table") == 'harada_client') {
            $insert["data"] = $this->customer->get_datas_client($data);
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $insert["data"] = $this->customer->get_datas_client_ob($data);
        }
        
        if($this->input->post("table") == 'harada_client')
        {
            $this->load->view('dashboard/result/client',$insert);
        }

        if($this->input->post("table") == 'harada_client_ob')
        {
            $this->load->view('dashboard/result/client_ob',$insert);
        }

        // echo json_encode($insert, JSON_UNESCAPED_UNICODE);
    }

    public function search_construction()
    {
        $this->record_action("search_construct", "view");
        $data["selector_search_construction"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/constructor/search_v',$data);
    }

    public function search_data_construct()
    {
        // redirect('dashboard/underconstruc');
        $data['cust_code'] = $this->input->post('cust_code');
        $data['cust_name'] = $this->input->post('cust_name');
        $data['kinds'] = $this->input->post('kinds');
        $data['from_contract_date'] = $this->input->post('from_contract_date');
        $data['to_contract_date'] = $this->input->post('to_contract_date');
        $data['from_construction_start_date'] = $this->input->post('from_construction_start_date');
        $data['to_construction_start_date'] = $this->input->post('to_construction_start_date');
        $data['from_upper_building_date'] = $this->input->post('from_upper_building_date');
        $data['to_upper_building_date'] = $this->input->post('to_upper_building_date');
        $data['from_completion_date'] = $this->input->post('from_completion_date');
        $data['to_completion_date'] = $this->input->post('to_completion_date');
        $data['from_delivery_date'] = $this->input->post('from_delivery_date');
        $data['to_delivery_date'] = $this->input->post('to_delivery_date');

        $this->load->model('customer_model',"customer");
        $wehe['data'] = $this->customer->get_data_construct($data);

        $this->load->view('dashboard/constructor/result_v',$wehe);
        // echo json_encode($wehe, JSON_UNESCAPED_UNICODE);
    }

    function get_autocomplete(){
        $this->load->model('customer_model',"customer");

        if (isset($_GET['term'])) {
            $result = $this->customer->search_id($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row)
                    $arr_result[] = array(
                        'label'         => $row->cust_code,
                        'description'   => $row->氏名,
                 );
                    echo json_encode($arr_result,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    public function add_user()
    {
        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/user/insert_v',$data);
    }

    public function edit_user($id)
    {
        $this->record_action("update_user", "view");
        $this->load->model('customer_model', "customer");
        $this->load->model('users_model', "user");
        $data["affiliate"] = $this->customer->fetch_affiliate_names();
        $data["data"] = $this->user->fetch_one_user($id)[0];
        $this->load->view('dashboard/user/update_v',$data);
    }

    public function insert_user()
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]', array('required' => 'パスワードを再入力してください。'));
        $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('dashboard/user/insert_v',$data);
        }
        else
        {
                $this->load->view('formsuccess');
        }
    }

    public function preview_datas()
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]', array('required' => 'パスワードを再入力してください。'));
        $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        $this->form_validation->set_message('matches','パスワードが間違っています。再度入力してください。');

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["data"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        if ($this->form_validation->run() == FALSE)
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $this->load->view('dashboard/user/insert_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['passconf'] = $this->input->post('passconf');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->load->view('dashboard/user/preview_v',$data);
        }
    }


    public function check_datas()
    {
        if($this->input->post('edit_btn') == 'edit')
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['passconf'] = $this->input->post('passconf');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->record_action("add_construction", "view");
            $this->load->model('customer_model', "customer");
            $data["data"] = $this->customer->fetch_affiliate_names();
            $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

            $this->load->view('dashboard/user/preview_ins_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            // $data['passconf'] = $this->input->post('passconf');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $data['date_regist'] = date('Y-m-d');
            $data['access'] = '2';
            
            $this->load->model('users_model',"user");
            $insert = $this->user->insert_user_new($data);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/list_user");
            }
            else
            {
                $this->session->set_flashdata('error', 'User insertion was failed.');
                redirect("dashboard/list_user");
            }
        }
    }

    public function list_user()
    {
        $this->record_action("user_list", "view");
        $this->load->model('users_model', "user");
        $data["data"] = $this->user->fetch_all_user();
        $data["selector_list_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";
        $this->load->view('dashboard/user/list_v',$data);
    }


    public function preview_user($id)
    {
        $this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'ユーザー名を入力してください。'));
        $this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'パスワードを入力してください。'));
        $this->form_validation->set_rules('affilliate_from', 'Affilliate', 'required', array('required' => '業者を選択してください。'));

        // $this->form_validation->set_message('matches','パスワードが間違っています。再度入力してください。');

        $this->record_action("add_construction", "view");
        $this->load->model('customer_model', "customer");
        $data["datak"] = $this->customer->fetch_affiliate_names();
        $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

        if ($this->form_validation->run() == FALSE)
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $this->load->view('dashboard/user/update_err_v',$data);
        }
        else
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->load->view('dashboard/user/preview_user_v',$data);
        }
    }

    public function check_data_user($id)
    {
        if($this->input->post('edit_btn') == 'edit')
        {
            $data['id'] = $id;
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->input->post('password');
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');

            $this->record_action("add_construction", "view");
            $this->load->model('customer_model', "customer");
            $data["data"] = $this->customer->fetch_affiliate_names();
            $data["selector_add_affiliate"] = "<i class='fas fa-angle-right' style='padding-left: 20px;'></i>";

            $this->load->view('dashboard/user/preview_user_ins_v',$data);
        }
        else
        {
            $data['username'] = $this->input->post('username');
            $data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT);
            $data['view_password'] = $this->input->post('password');
            $data['affilliate_from'] = $this->input->post('affilliate_from');
            $data['date_regist'] = date('Y-m-d');
            
            $this->load->model('users_model',"user");
            $insert = $this->user->update_user($data,$id);
            if($insert)
            {
                $this->record_action("add_user","execute");
                $this->session->set_flashdata('success', '更新しました');
                redirect("dashboard/contractor_list");
            }
            else
            {
                $this->session->set_flashdata('error', 'User insertion was failed.');
                redirect("dashboard/contractor_list");
            }
        }
    }


    public function underconstruc()
    {
        $this->load->view('dashboard/under/list_v');
    }


    public function test_date()
    {
        echo date('Y-m-d');
    }


    public function logout()
    {
        
        $this->record_action("logout","execute");
        session_destroy();
        redirect("login");
    }
}
