<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->config->set_item('language', 'japanese');
    }

    
    public function index()
	{
		$this->load->view('user/login_v');
	}

    public function check_login()
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $this->load->model('users_model',"users");
        $user = $this->users->check_username_affiliate($username);
        // print_r($user);die();
        if(count($user) == 1)
        {
            if(password_verify($password,$user[0]->password))
            {
                $array = array(
                    'username' => $username,
                    'login_time' => date('Y-m-d H:i:s')
                );
                switch ($user[0]->access) {
                    // case 2:
                    //     $array["access"] = "member";
                    //     $redirect = "user/dashboard";
                    //     break;
                    case 3:
                        $array["access"] = "user";
                        $redirect = "user/dashboard";
                        break;
                }

                $_SESSION['affilate_name'] = $user[0]->affilliate_from;
                
                $this->session->set_userdata( $array );
                // track login
                $this->load->model('login_logs_model',"login_logs");
                $this->login_logs->insert_log($array);
                //update login
                // echo json_encode($array);
                $this->login_logs->update_date($array);
                redirect($redirect);
            }
            else 
            {
                $this->session->set_flashdata('error', 'Password is wrong!');
                redirect("user_login");
            }
        }
        else 
        {
            $this->session->set_flashdata('error', 'Username is not found!');
            redirect("user_login");
        }
    }


}