<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#b1e799">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">協力会社一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Dashboard / 協力会社一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body">
                <div class="card-body table-responsive">
                      <table class="table table-striped table-inverse" id="data_customer">
                        <thead class="thead-inverse">
                          <tr>
                            <th>GID</th>
                            <th>緯度</th>
                            <th>経度</th>
                            <th>番号</th>
                            <th>名称</th>
                            <th>連盟</th>
                            <th>郵便番号</th>
                            <th>都道府県</th>
                            <th>住所</th>
                            <th>電話</th>
                            <th>種別</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                            <tr>
                              <td scope="row"><?=$value->gid?></td>
                              <td><?=$value->latitude?></td>
                              <td><?=$value->longitude?></td>
                              <td><?=$value->number?></td>
                              <td><?=$value->name?></td>
                              <td><?=$value->federation?></td>
                              <td><?=$value->postal_code?></td>
                              <td><?=$value->prefecture?></td>
                              <td><?=$value->address?></td>
                              <td><?=$value->phone?></td>
                              <td><?=$value->type?></td>
                              <td>
                              <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_partner/<?=$value->gid?>" role="button"><i class="fa fa-edit"></i></a>
                              <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_partner/<?=$value->gid?>" role="button"><i class="fa fa-trash"></i></a>
                              </td>
                            </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="12">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                          </tbody>
                      </table>
                  </div>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  $('.delete').confirm({
  title:'',
  content: "物件情報を削除されますが、よろしいですか。",
  buttons: {
    削除: {
      btnClass: 'btn-blue',
      action: function(){
          location.href = this.$target.attr('href');
        }
      },
      キャンセル: function(){

      }
  }
});
</script>
</body>
</html>