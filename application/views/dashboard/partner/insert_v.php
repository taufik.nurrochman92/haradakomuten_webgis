<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#b1e799">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規追加協力会社</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規追加協力会社</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_partner" method="post">
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="number">番号</label>
                          <input type="text" class="form-control col-md-6" name="number" id="number" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="family_name">名称</label>
                          <input type="text" class="form-control col-md-6" name="name" id="name" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="federation">連盟</label>
                          <input type="text" class="form-control col-md-6" name="federation" id="federation" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="postal_code">郵便番号</label>
                          <input type="text" class="form-control col-md-6" name="postal_code" id="postal_code" aria-describedby="helpId" placeholder="例 : 4328051" onkeyup="AjaxZip3.zip2addr(this,'','prefecture','address');">
                        </div>
                        <div class="form-group">
                          <label for="prefecture">都道府県</label>
                          <input type="text" class="form-control col-md-6" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="address">住所</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="phone">電話</label>
                          <input type="text" class="form-control col-md-6" name="phone" id="phone" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="type">種別</label>
                          <input type="text" class="form-control col-md-6" name="type" id="type" aria-describedby="helpId" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>