<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">工事情報</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">管理ページ / 工事情報</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <form action="<?=base_url()?>dashboard/check_data" method="post" enctype="multipart/form-data">

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="col-md-6">
                      <label for="cust_code">顧客コード</label>
                      <p>
                        <?=$cust_code?>
                      </p>
                      <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId"
                        placeholder="" value="<?php if(!empty($cust_code)){ echo $cust_code; }?>" hidden>
                    </div>
                    <div class="col-md-6">
                      <label for="cust_name">顧客名</label>
                      <p>
                        <?=$cust_name?>
                      </p>
                      <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId"
                        placeholder="" value="<?php if(!empty($cust_name)){ echo $cust_name; }?>" hidden>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group col-md-6 dropdowns" style="margin-bottom: 0;">
                      <label for="kinds">種別</label>
                      <p>
                        <?=$kinds?>
                      </p>
                      <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId"
                        placeholder="" value="<?php if(!empty($kinds)){ echo $kinds; }?>" hidden>
                    </div>
                    <div class="col-md-6" style="padding: 0;">
                      <div class="form-row">
                        <div class="col-md-12">
                          <label for="const_no">工事No</label>
                          <p>
                            <?=$const_no?>
                          </p>
                          <input type="text" class="form-control" name="const_no" id="const_no"
                            aria-describedby="helpId" placeholder=""
                            value="<?php if(!empty($const_no)){ echo $const_no; }?>" hidden>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <label for="remarks">備考</label>
                  <p class="mb30">
                    <?=$remarks?>
                  </p>
                  <input type="text" class="form-control col-md-12" name="remarks" id="remarks" aria-describedby="helpId"
                    placeholder="" value="<?php if(!empty($remarks)){ echo $remarks; }?>" hidden>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="contract_date">契約日</label>
                      <p>
                        <?=$contract_date?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="contract_date" id="contract_date"
                        aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($contract_date)){ echo $contract_date; }?>" hidden>
                    </div>
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="construction_start_date">着工日</label>
                      <p>
                        <?=$construction_start_date?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="construction_start_date"
                        id="construction_start_date" aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($construction_start_date)){ echo $construction_start_date; }?>" hidden>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="upper_building_date">上棟日</label>
                      <p>
                        <?=$upper_building_date?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="upper_building_date"
                        id="upper_building_date" aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($upper_building_date)){ echo $upper_building_date; }?>" hidden>
                    </div>
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="completion_date">竣工日</label>
                      <p>
                        <?=$completion_date?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="completion_date" id="completion_date"
                        aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($completion_date)){ echo $completion_date; }?>" hidden>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="delivery_date">引渡日</label>
                      <p>
                        <?=$delivery_date?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="delivery_date" id="delivery_date"
                        aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($delivery_date)){ echo $delivery_date; }?>" hidden>
                    </div>
                    <div class="form-group col-md-6" style="margin-bottom:0;">
                      <label for="amount_money">金額</label>
                      <p>
                        <?=$amount_money?>
                      </p>
                      <input type="text" class="form-control number" name="amount_money" id="amount_money"
                        aria-describedby="helpId" placeholder="円" style="text-align: right;"
                        value="<?php if(!empty($amount_money)){ echo $amount_money; }?>" hidden>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group dropdowns col-md-6" style="margin-bottom:0;">
                      <label for="sales_staff">営業担当</label>
                      <p>
                        <?=$sales_staff?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="sales_staff" id="sales_staff"
                        aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($sales_staff)){ echo $sales_staff; }?>" hidden>
                    </div>
                    <div class="form-group dropdowns col-md-6" style="margin-bottom:0;">
                      <label for="incharge_construction">工務担当</label>
                      <p>
                        <?=$incharge_construction?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="incharge_construction"
                        id="incharge_construction" aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($incharge_construction)){ echo $incharge_construction; }?>" hidden>
                    </div>
                  </div>
                </div>


                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">
                  <div class="form-row">
                    <div class="form-group dropdowns col-md-6" style="margin-bottom:0;">
                      <label for="incharge_coordinator">コーデ担当</label>
                      <p>
                        <?=$incharge_coordinator?>
                      </p>
                      <input type="text" class="form-control col-md-12" name="incharge_coordinator"
                        id="incharge_coordinator" aria-describedby="helpId" placeholder=""
                        value="<?php if(!empty($incharge_coordinator)){ echo $incharge_coordinator; }?>" hidden>
                    </div>
                  </div>
                </div>

                <div class="col-xl-8 col-lg-12 col-md-12 mb-col" style="padding: 0;">

                  <div id="accordion" class="customAccordion">
                      
                      <!-- CATEGORY 1 -->
                      <div class="card">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                              <div class="leftSide">
                                <span>1</span>
                                <p>請負契約書・土地契約書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column1">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1" name="constract_drawing1"
                                    value="<?php if(!empty($constract_drawing1)){ echo $constract_drawing1; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1)) { echo $date_insert_drawing1;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1"
                                  value="<?php if(!empty($constract_drawing1)) { echo $date_insert_drawing1;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1">コメント</label>
                                      <p>
                                        <?=$desc_item1?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1)){ echo $desc_item1; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2" name="constract_drawing2"
                                    value="<?php if(!empty($constract_drawing2)){ echo $constract_drawing2; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2)) { echo $date_insert_drawing2;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2"
                                  value="<?php if(!empty($constract_drawing2)) { echo $date_insert_drawing2;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2">コメント</label>
                                      <p>
                                        <?=$desc_item2?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2)){ echo $desc_item2; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3" name="constract_drawing3"
                                    value="<?php if(!empty($constract_drawing3)){ echo $constract_drawing3; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3)) { echo $date_insert_drawing3;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3"
                                  value="<?php if(!empty($constract_drawing3)) { echo $date_insert_drawing3;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3">コメント</label>
                                      <p>
                                        <?=$desc_item3?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3)){ echo $desc_item3; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4" name="constract_drawing4"
                                    value="<?php if(!empty($constract_drawing4)){ echo $constract_drawing4; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4)) { echo $date_insert_drawing4;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4"
                                  value="<?php if(!empty($constract_drawing4)) { echo $date_insert_drawing4;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4">コメント</label>
                                      <p>
                                        <?=$desc_item4?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4)){ echo $desc_item4; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5" name="constract_drawing5"
                                    value="<?php if(!empty($constract_drawing5)){ echo $constract_drawing5; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5)) { echo $date_insert_drawing5;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5"
                                  value="<?php if(!empty($constract_drawing5)) { echo $date_insert_drawing5;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5">コメント</label>
                                      <p>
                                        <?=$desc_item5?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5)){ echo $desc_item5; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6" name="constract_drawing6"
                                    value="<?php if(!empty($constract_drawing6)){ echo $constract_drawing6; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6)) { echo $date_insert_drawing6;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6"
                                  value="<?php if(!empty($constract_drawing6)) { echo $date_insert_drawing6;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6">コメント</label>
                                      <p>
                                        <?=$desc_item6?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6)){ echo $desc_item6; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7" name="constract_drawing7"
                                    value="<?php if(!empty($constract_drawing7)){ echo $constract_drawing7; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7)) { echo $date_insert_drawing7;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7"
                                  value="<?php if(!empty($constract_drawing7)) { echo $date_insert_drawing7;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7">コメント</label>
                                      <p>
                                        <?=$desc_item7?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7)){ echo $desc_item7; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8" name="constract_drawing8"
                                    value="<?php if(!empty($constract_drawing8)){ echo $constract_drawing8; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8)) { echo $date_insert_drawing8;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8"
                                  value="<?php if(!empty($constract_drawing8)) { echo $date_insert_drawing8;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8">コメント</label>
                                      <p>
                                        <?=$desc_item8?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8)){ echo $desc_item8; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9" name="constract_drawing9"
                                    value="<?php if(!empty($constract_drawing9)){ echo $constract_drawing9; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9)) { echo $date_insert_drawing9;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9"
                                  value="<?php if(!empty($constract_drawing9)) { echo $date_insert_drawing9;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9">コメント</label>
                                      <p>
                                        <?=$desc_item9?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9)){ echo $desc_item9; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10" name="constract_drawing10"
                                    value="<?php if(!empty($constract_drawing10)){ echo $constract_drawing10; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10)) { echo $date_insert_drawing10;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10"
                                  value="<?php if(!empty($constract_drawing10)) { echo $date_insert_drawing10;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10">コメント</label>
                                      <p>
                                        <?=$desc_item10?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10)){ echo $desc_item10; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 2 -->
                      <div class="card">
                        <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                              <div class="leftSide">
                                <span>2</span>
                                <p>長期優良住宅認定書・性能評価書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column2">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1b" name="constract_drawing1b"
                                    value="<?php if(!empty($constract_drawing1b)){ echo $constract_drawing1b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1b)) { echo $date_insert_drawing1b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1b"
                                  value="<?php if(!empty($constract_drawing1b)) { echo $date_insert_drawing1b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1b">コメント</label>
                                      <p>
                                        <?=$desc_item1b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1b)){ echo $desc_item1b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2b" name="constract_drawing2b"
                                    value="<?php if(!empty($constract_drawing2b)){ echo $constract_drawing2b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2b)) { echo $date_insert_drawing2b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2b"
                                  value="<?php if(!empty($constract_drawing2b)) { echo $date_insert_drawing2b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2b">コメント</label>
                                      <p>
                                        <?=$desc_item2b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2b)){ echo $desc_item2b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3b" name="constract_drawing3b"
                                    value="<?php if(!empty($constract_drawing3b)){ echo $constract_drawing3b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3b)) { echo $date_insert_drawing3b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3b"
                                  value="<?php if(!empty($constract_drawing3b)) { echo $date_insert_drawing3b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3b">コメント</label>
                                      <p>
                                        <?=$desc_item3b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3b)){ echo $desc_item3b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4b" name="constract_drawing4b"
                                    value="<?php if(!empty($constract_drawing4b)){ echo $constract_drawing4b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4b)) { echo $date_insert_drawing4b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4b"
                                  value="<?php if(!empty($constract_drawing4b)) { echo $date_insert_drawing4b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4b">コメント</label>
                                      <p>
                                        <?=$desc_item4b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4b)){ echo $desc_item4b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5b" name="constract_drawing5b"
                                    value="<?php if(!empty($constract_drawing5b)){ echo $constract_drawing5b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5b)) { echo $date_insert_drawing5b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5b"
                                  value="<?php if(!empty($constract_drawing5b)) { echo $date_insert_drawing5b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5b">コメント</label>
                                      <p>
                                        <?=$desc_item5b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5b)){ echo $desc_item5b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6b" name="constract_drawing6b"
                                    value="<?php if(!empty($constract_drawing6b)){ echo $constract_drawing6b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6b)) { echo $date_insert_drawing6b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6b"
                                  value="<?php if(!empty($constract_drawing6b)) { echo $date_insert_drawing6b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6b">コメント</label>
                                      <p>
                                        <?=$desc_item6b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6b)){ echo $desc_item6b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7b" name="constract_drawing7b"
                                    value="<?php if(!empty($constract_drawing7b)){ echo $constract_drawing7b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7b)) { echo $date_insert_drawing7b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7b"
                                  value="<?php if(!empty($constract_drawing7b)) { echo $date_insert_drawing7b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7b">コメント</label>
                                      <p>
                                        <?=$desc_item7b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7b)){ echo $desc_item7b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8b" name="constract_drawing8b"
                                    value="<?php if(!empty($constract_drawing8b)){ echo $constract_drawing8b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8b)) { echo $date_insert_drawing8b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8b"
                                  value="<?php if(!empty($constract_drawing8b)) { echo $date_insert_drawing8b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8b">コメント</label>
                                      <p>
                                        <?=$desc_item8b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8b)){ echo $desc_item8b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9b" name="constract_drawing9b"
                                    value="<?php if(!empty($constract_drawing9b)){ echo $constract_drawing9b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9b)) { echo $date_insert_drawing9b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9b"
                                  value="<?php if(!empty($constract_drawing9b)) { echo $date_insert_drawing9b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9b">コメント</label>
                                      <p>
                                        <?=$desc_item9b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9b)){ echo $desc_item9b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10b?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10b" name="constract_drawing10b"
                                    value="<?php if(!empty($constract_drawing10b)){ echo $constract_drawing10b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10b)) { echo $date_insert_drawing10b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10b"
                                  value="<?php if(!empty($constract_drawing10b)) { echo $date_insert_drawing10b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10b">コメント</label>
                                      <p>
                                        <?=$desc_item10b?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10b)){ echo $desc_item10b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10b)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 3 -->
                      <div class="card">
                        <div class="card-header" id="headingThree">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                              <div class="leftSide">
                                <span>3</span>
                                <p>地盤調査・地盤改良</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column3">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1c" name="constract_drawing1c"
                                    value="<?php if(!empty($constract_drawing1c)){ echo $constract_drawing1c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1c)) { echo $date_insert_drawing1c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1c"
                                  value="<?php if(!empty($constract_drawing1c)) { echo $date_insert_drawing1c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1c">コメント</label>
                                      <p>
                                        <?=$desc_item1c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1c)){ echo $desc_item1c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2c" name="constract_drawing2c"
                                    value="<?php if(!empty($constract_drawing2c)){ echo $constract_drawing2c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2c)) { echo $date_insert_drawing2c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2c"
                                  value="<?php if(!empty($constract_drawing2c)) { echo $date_insert_drawing2c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2c">コメント</label>
                                      <p>
                                        <?=$desc_item2c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2c)){ echo $desc_item2c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3c" name="constract_drawing3c"
                                    value="<?php if(!empty($constract_drawing3c)){ echo $constract_drawing3c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3c)) { echo $date_insert_drawing3c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3c"
                                  value="<?php if(!empty($constract_drawing3c)) { echo $date_insert_drawing3c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3c">コメント</label>
                                      <p>
                                        <?=$desc_item3c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3c)){ echo $desc_item3c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4c" name="constract_drawing4c"
                                    value="<?php if(!empty($constract_drawing4c)){ echo $constract_drawing4c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4c)) { echo $date_insert_drawing4c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4c"
                                  value="<?php if(!empty($constract_drawing4c)) { echo $date_insert_drawing4c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4c">コメント</label>
                                      <p>
                                        <?=$desc_item4c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4c)){ echo $desc_item4c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5c" name="constract_drawing5c"
                                    value="<?php if(!empty($constract_drawing5c)){ echo $constract_drawing5c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5c)) { echo $date_insert_drawing5c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5c"
                                  value="<?php if(!empty($constract_drawing5c)) { echo $date_insert_drawing5c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5c">コメント</label>
                                      <p>
                                        <?=$desc_item5c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5c)){ echo $desc_item5c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6c" name="constract_drawing6c"
                                    value="<?php if(!empty($constract_drawing6c)){ echo $constract_drawing6c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6c)) { echo $date_insert_drawing6c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6c"
                                  value="<?php if(!empty($constract_drawing6c)) { echo $date_insert_drawing6c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6c">コメント</label>
                                      <p>
                                        <?=$desc_item6c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6c)){ echo $desc_item6c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7c" name="constract_drawing7c"
                                    value="<?php if(!empty($constract_drawing7c)){ echo $constract_drawing7c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7c)) { echo $date_insert_drawing7c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7c"
                                  value="<?php if(!empty($constract_drawing7c)) { echo $date_insert_drawing7c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7c">コメント</label>
                                      <p>
                                        <?=$desc_item7c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7c)){ echo $desc_item7c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8c" name="constract_drawing8c"
                                    value="<?php if(!empty($constract_drawing8c)){ echo $constract_drawing8c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8c)) { echo $date_insert_drawing8c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8c"
                                  value="<?php if(!empty($constract_drawing8c)) { echo $date_insert_drawing8c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8c">コメント</label>
                                      <p>
                                        <?=$desc_item8c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8c)){ echo $desc_item8c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9c" name="constract_drawing9c"
                                    value="<?php if(!empty($constract_drawing9c)){ echo $constract_drawing9c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9c)) { echo $date_insert_drawing9c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9c"
                                  value="<?php if(!empty($constract_drawing9c)) { echo $date_insert_drawing9c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9c">コメント</label>
                                      <p>
                                        <?=$desc_item9c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9c)){ echo $desc_item9c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10c?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10c" name="constract_drawing10c"
                                    value="<?php if(!empty($constract_drawing10c)){ echo $constract_drawing10c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10c)) { echo $date_insert_drawing10c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10c"
                                  value="<?php if(!empty($constract_drawing10c)) { echo $date_insert_drawing10c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10c">コメント</label>
                                      <p>
                                        <?=$desc_item10c?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10c)){ echo $desc_item10c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10c)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>   
                      
                      <!-- CATEORY 4 -->
                      <div class="card">
                        <div class="card-header" id="headingFour">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                              <div class="leftSide">
                                <span>4</span>
                                <p>点検履歴</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column4">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1d" name="constract_drawing1d"
                                    value="<?php if(!empty($constract_drawing1d)){ echo $constract_drawing1d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1d)) { echo $date_insert_drawing1d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1d"
                                  value="<?php if(!empty($constract_drawing1d)) { echo $date_insert_drawing1d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1d">コメント</label>
                                      <p>
                                        <?=$desc_item1d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1d)){ echo $desc_item1d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2d" name="constract_drawing2d"
                                    value="<?php if(!empty($constract_drawing2d)){ echo $constract_drawing2d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2d)) { echo $date_insert_drawing2d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2d"
                                  value="<?php if(!empty($constract_drawing2d)) { echo $date_insert_drawing2d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2d">コメント</label>
                                      <p>
                                        <?=$desc_item2d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2d)){ echo $desc_item2d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3d" name="constract_drawing3d"
                                    value="<?php if(!empty($constract_drawing3d)){ echo $constract_drawing3d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3d)) { echo $date_insert_drawing3d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3d"
                                  value="<?php if(!empty($constract_drawing3d)) { echo $date_insert_drawing3d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3d">コメント</label>
                                      <p>
                                        <?=$desc_item3d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3d)){ echo $desc_item3d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4d" name="constract_drawing4d"
                                    value="<?php if(!empty($constract_drawing4d)){ echo $constract_drawing4d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4d)) { echo $date_insert_drawing4d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4d"
                                  value="<?php if(!empty($constract_drawing4d)) { echo $date_insert_drawing4d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4d">コメント</label>
                                      <p>
                                        <?=$desc_item4d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4d)){ echo $desc_item4d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5d" name="constract_drawing5d"
                                    value="<?php if(!empty($constract_drawing5d)){ echo $constract_drawing5d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5d)) { echo $date_insert_drawing5d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5d"
                                  value="<?php if(!empty($constract_drawing5d)) { echo $date_insert_drawing5d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5d">コメント</label>
                                      <p>
                                        <?=$desc_item5d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5d)){ echo $desc_item5d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6d" name="constract_drawing6d"
                                    value="<?php if(!empty($constract_drawing6d)){ echo $constract_drawing6d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6d)) { echo $date_insert_drawing6d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6d"
                                  value="<?php if(!empty($constract_drawing6d)) { echo $date_insert_drawing6d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6d">コメント</label>
                                      <p>
                                        <?=$desc_item6d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6d)){ echo $desc_item6d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7d" name="constract_drawing7d"
                                    value="<?php if(!empty($constract_drawing7d)){ echo $constract_drawing7d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7d)) { echo $date_insert_drawing7d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7d"
                                  value="<?php if(!empty($constract_drawing7d)) { echo $date_insert_drawing7d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7d">コメント</label>
                                      <p>
                                        <?=$desc_item7d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7d)){ echo $desc_item7d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8d" name="constract_drawing8d"
                                    value="<?php if(!empty($constract_drawing8d)){ echo $constract_drawing8d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8d)) { echo $date_insert_drawing8d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8d"
                                  value="<?php if(!empty($constract_drawing8d)) { echo $date_insert_drawing8d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8d">コメント</label>
                                      <p>
                                        <?=$desc_item8d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8d)){ echo $desc_item8d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9d" name="constract_drawing9d"
                                    value="<?php if(!empty($constract_drawing9d)){ echo $constract_drawing9d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9d)) { echo $date_insert_drawing9d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9d"
                                  value="<?php if(!empty($constract_drawing9d)) { echo $date_insert_drawing9d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9d">コメント</label>
                                      <p>
                                        <?=$desc_item9d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9d)){ echo $desc_item9d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10d?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10d" name="constract_drawing10d"
                                    value="<?php if(!empty($constract_drawing10d)){ echo $constract_drawing10d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10d)) { echo $date_insert_drawing10d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10d"
                                  value="<?php if(!empty($constract_drawing10d)) { echo $date_insert_drawing10d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10d">コメント</label>
                                      <p>
                                        <?=$desc_item10d?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10d)){ echo $desc_item10d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10d)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>   

                      <!-- CATEGORY 5 --> 
                      <div class="card">
                        <div class="card-header" id="headingFive">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                              <div class="leftSide">
                                <span>5</span>
                                <p>三者引継ぎ合意書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column5">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1e" name="constract_drawing1e"
                                    value="<?php if(!empty($constract_drawing1e)){ echo $constract_drawing1e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1e)) { echo $date_insert_drawing1e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1e"
                                  value="<?php if(!empty($constract_drawing1e)) { echo $date_insert_drawing1e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1e">コメント</label>
                                      <p>
                                        <?=$desc_item1e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1e)){ echo $desc_item1e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2e" name="constract_drawing2e"
                                    value="<?php if(!empty($constract_drawing2e)){ echo $constract_drawing2e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2e)) { echo $date_insert_drawing2e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2e"
                                  value="<?php if(!empty($constract_drawing2e)) { echo $date_insert_drawing2e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2e">コメント</label>
                                      <p>
                                        <?=$desc_item2e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2e)){ echo $desc_item2e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3e" name="constract_drawing3e"
                                    value="<?php if(!empty($constract_drawing3e)){ echo $constract_drawing3e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3e)) { echo $date_insert_drawing3e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3e"
                                  value="<?php if(!empty($constract_drawing3e)) { echo $date_insert_drawing3e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3e">コメント</label>
                                      <p>
                                        <?=$desc_item3e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3e)){ echo $desc_item3e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4e" name="constract_drawing4e"
                                    value="<?php if(!empty($constract_drawing4e)){ echo $constract_drawing4e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4e)) { echo $date_insert_drawing4e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4e"
                                  value="<?php if(!empty($constract_drawing4e)) { echo $date_insert_drawing4e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4e">コメント</label>
                                      <p>
                                        <?=$desc_item4e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4e)){ echo $desc_item4e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5e" name="constract_drawing5e"
                                    value="<?php if(!empty($constract_drawing5e)){ echo $constract_drawing5e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5e)) { echo $date_insert_drawing5e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5e"
                                  value="<?php if(!empty($constract_drawing5e)) { echo $date_insert_drawing5e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5e">コメント</label>
                                      <p>
                                        <?=$desc_item5e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5e)){ echo $desc_item5e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6e" name="constract_drawing6e"
                                    value="<?php if(!empty($constract_drawing6e)){ echo $constract_drawing6e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6e)) { echo $date_insert_drawing6e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6e"
                                  value="<?php if(!empty($constract_drawing6e)) { echo $date_insert_drawing6e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6e">コメント</label>
                                      <p>
                                        <?=$desc_item6e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6e)){ echo $desc_item6e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7e" name="constract_drawing7e"
                                    value="<?php if(!empty($constract_drawing7e)){ echo $constract_drawing7e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7e)) { echo $date_insert_drawing7e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7e"
                                  value="<?php if(!empty($constract_drawing7e)) { echo $date_insert_drawing7e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7e">コメント</label>
                                      <p>
                                        <?=$desc_item7e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7e)){ echo $desc_item7e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8e" name="constract_drawing8e"
                                    value="<?php if(!empty($constract_drawing8e)){ echo $constract_drawing8e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8e)) { echo $date_insert_drawing8e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8e"
                                  value="<?php if(!empty($constract_drawing8e)) { echo $date_insert_drawing8e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8e">コメント</label>
                                      <p>
                                        <?=$desc_item8e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8e)){ echo $desc_item8e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9e" name="constract_drawing9e"
                                    value="<?php if(!empty($constract_drawing9e)){ echo $constract_drawing9e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9e)) { echo $date_insert_drawing9e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9e"
                                  value="<?php if(!empty($constract_drawing9e)) { echo $date_insert_drawing9e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9e">コメント</label>
                                      <p>
                                        <?=$desc_item9e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9e)){ echo $desc_item9e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10e?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10e" name="constract_drawing10e"
                                    value="<?php if(!empty($constract_drawing10e)){ echo $constract_drawing10e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10e)) { echo $date_insert_drawing10e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10e"
                                  value="<?php if(!empty($constract_drawing10e)) { echo $date_insert_drawing10e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10e">コメント</label>
                                      <p>
                                        <?=$desc_item10e?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10e)){ echo $desc_item10e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10e)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 6 -->
                      <div class="card">
                        <div class="card-header" id="headingSix">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                              <div class="leftSide">
                                <span>6</span>
                                <p>住宅仕様書・プレカット資料 </p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column6">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1f" name="constract_drawing1f"
                                    value="<?php if(!empty($constract_drawing1f)){ echo $constract_drawing1f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1f)) { echo $date_insert_drawing1f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1f"
                                  value="<?php if(!empty($constract_drawing1f)) { echo $date_insert_drawing1f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1f">コメント</label>
                                      <p>
                                        <?=$desc_item1f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1f)){ echo $desc_item1f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2f" name="constract_drawing2f"
                                    value="<?php if(!empty($constract_drawing2f)){ echo $constract_drawing2f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2f)) { echo $date_insert_drawing2f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2f"
                                  value="<?php if(!empty($constract_drawing2f)) { echo $date_insert_drawing2f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2f">コメント</label>
                                      <p>
                                        <?=$desc_item2f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2f)){ echo $desc_item2f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3f" name="constract_drawing3f"
                                    value="<?php if(!empty($constract_drawing3f)){ echo $constract_drawing3f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3f)) { echo $date_insert_drawing3f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3f"
                                  value="<?php if(!empty($constract_drawing3f)) { echo $date_insert_drawing3f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3f">コメント</label>
                                      <p>
                                        <?=$desc_item3f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3f)){ echo $desc_item3f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4f" name="constract_drawing4f"
                                    value="<?php if(!empty($constract_drawing4f)){ echo $constract_drawing4f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4f)) { echo $date_insert_drawing4f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4f"
                                  value="<?php if(!empty($constract_drawing4f)) { echo $date_insert_drawing4f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4f">コメント</label>
                                      <p>
                                        <?=$desc_item4f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4f)){ echo $desc_item4f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5f" name="constract_drawing5f"
                                    value="<?php if(!empty($constract_drawing5f)){ echo $constract_drawing5f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5f)) { echo $date_insert_drawing5f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5f"
                                  value="<?php if(!empty($constract_drawing5f)) { echo $date_insert_drawing5f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5f">コメント</label>
                                      <p>
                                        <?=$desc_item5f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5f)){ echo $desc_item5f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6f" name="constract_drawing6f"
                                    value="<?php if(!empty($constract_drawing6f)){ echo $constract_drawing6f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6f)) { echo $date_insert_drawing6f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6f"
                                  value="<?php if(!empty($constract_drawing6f)) { echo $date_insert_drawing6f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6f">コメント</label>
                                      <p>
                                        <?=$desc_item6f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6f)){ echo $desc_item6f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7f" name="constract_drawing7f"
                                    value="<?php if(!empty($constract_drawing7f)){ echo $constract_drawing7f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7f)) { echo $date_insert_drawing7f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7f"
                                  value="<?php if(!empty($constract_drawing7f)) { echo $date_insert_drawing7f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7f">コメント</label>
                                      <p>
                                        <?=$desc_item7f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7f)){ echo $desc_item7f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8f" name="constract_drawing8f"
                                    value="<?php if(!empty($constract_drawing8f)){ echo $constract_drawing8f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8f)) { echo $date_insert_drawing8f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8f"
                                  value="<?php if(!empty($constract_drawing8f)) { echo $date_insert_drawing8f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8f">コメント</label>
                                      <p>
                                        <?=$desc_item8f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8f)){ echo $desc_item8f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9f" name="constract_drawing9f"
                                    value="<?php if(!empty($constract_drawing9f)){ echo $constract_drawing9f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9f)) { echo $date_insert_drawing9f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9f"
                                  value="<?php if(!empty($constract_drawing9f)) { echo $date_insert_drawing9f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9f">コメント</label>
                                      <p>
                                        <?=$desc_item9f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9f)){ echo $desc_item9f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10f?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10f" name="constract_drawing10f"
                                    value="<?php if(!empty($constract_drawing10f)){ echo $constract_drawing10f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10f)) { echo $date_insert_drawing10f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10f"
                                  value="<?php if(!empty($constract_drawing10f)) { echo $date_insert_drawing10f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10f">コメント</label>
                                      <p>
                                        <?=$desc_item10f?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10f)){ echo $desc_item10f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10f)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 7 --> 
                      <div class="card">
                        <div class="card-header" id="headingSeven">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                              <div class="leftSide">
                                <span>7</span>
                                <p>工事写真 </p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column7">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1g" name="constract_drawing1g"
                                    value="<?php if(!empty($constract_drawing1g)){ echo $constract_drawing1g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1g)) { echo $date_insert_drawing1g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1g"
                                  value="<?php if(!empty($constract_drawing1g)) { echo $date_insert_drawing1g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1g">コメント</label>
                                      <p>
                                        <?=$desc_item1g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1g)){ echo $desc_item1g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2g" name="constract_drawing2g"
                                    value="<?php if(!empty($constract_drawing2g)){ echo $constract_drawing2g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2g)) { echo $date_insert_drawing2g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2g"
                                  value="<?php if(!empty($constract_drawing2g)) { echo $date_insert_drawing2g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2g">コメント</label>
                                      <p>
                                        <?=$desc_item2g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2g)){ echo $desc_item2g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3g" name="constract_drawing3g"
                                    value="<?php if(!empty($constract_drawing3g)){ echo $constract_drawing3g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3g)) { echo $date_insert_drawing3g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3g"
                                  value="<?php if(!empty($constract_drawing3g)) { echo $date_insert_drawing3g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3g">コメント</label>
                                      <p>
                                        <?=$desc_item3g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3g)){ echo $desc_item3g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4g" name="constract_drawing4g"
                                    value="<?php if(!empty($constract_drawing4g)){ echo $constract_drawing4g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4g)) { echo $date_insert_drawing4g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4g"
                                  value="<?php if(!empty($constract_drawing4g)) { echo $date_insert_drawing4g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4g">コメント</label>
                                      <p>
                                        <?=$desc_item4g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4g)){ echo $desc_item4g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5g" name="constract_drawing5g"
                                    value="<?php if(!empty($constract_drawing5g)){ echo $constract_drawing5g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5g)) { echo $date_insert_drawing5g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5g"
                                  value="<?php if(!empty($constract_drawing5g)) { echo $date_insert_drawing5g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5g">コメント</label>
                                      <p>
                                        <?=$desc_item5g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5g)){ echo $desc_item5g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6g" name="constract_drawing6g"
                                    value="<?php if(!empty($constract_drawing6g)){ echo $constract_drawing6g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6g)) { echo $date_insert_drawing6g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6g"
                                  value="<?php if(!empty($constract_drawing6g)) { echo $date_insert_drawing6g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6g">コメント</label>
                                      <p>
                                        <?=$desc_item6g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6g)){ echo $desc_item6g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7g" name="constract_drawing7g"
                                    value="<?php if(!empty($constract_drawing7g)){ echo $constract_drawing7g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7g)) { echo $date_insert_drawing7g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7g"
                                  value="<?php if(!empty($constract_drawing7g)) { echo $date_insert_drawing7g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7g">コメント</label>
                                      <p>
                                        <?=$desc_item7g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7g)){ echo $desc_item7g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8g" name="constract_drawing8g"
                                    value="<?php if(!empty($constract_drawing8g)){ echo $constract_drawing8g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8g)) { echo $date_insert_drawing8g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8g"
                                  value="<?php if(!empty($constract_drawing8g)) { echo $date_insert_drawing8g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8g">コメント</label>
                                      <p>
                                        <?=$desc_item8g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8g)){ echo $desc_item8g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9g" name="constract_drawing9g"
                                    value="<?php if(!empty($constract_drawing9g)){ echo $constract_drawing9g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9g)) { echo $date_insert_drawing9g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9g"
                                  value="<?php if(!empty($constract_drawing9g)) { echo $date_insert_drawing9g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9g">コメント</label>
                                      <p>
                                        <?=$desc_item9g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9g)){ echo $desc_item9g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10g?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10g" name="constract_drawing10g"
                                    value="<?php if(!empty($constract_drawing10g)){ echo $constract_drawing10g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10g)) { echo $date_insert_drawing10g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10g"
                                  value="<?php if(!empty($constract_drawing10g)) { echo $date_insert_drawing10g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10g">コメント</label>
                                      <p>
                                        <?=$desc_item10g?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10g)){ echo $desc_item10g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10g)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 1BLUE --> 
                      <div class="card blueCard">
                        <div class="card-header" id="headingOneBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                              <div class="leftSide">
                                <span>1</span>
                                <p>確認申請書一式 </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column1blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluea" name="constract_drawing1bluea"
                                    value="<?php if(!empty($constract_drawing1bluea)){ echo $constract_drawing1bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1bluea)) { echo $date_insert_drawing1bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluea"
                                  value="<?php if(!empty($constract_drawing1bluea)) { echo $date_insert_drawing1bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1bluea)){ echo implode(";",$co_owner1bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner1bluea" name="co_owner1bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluea">コメント</label>
                                      <p>
                                        <?=$desc_item1bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1bluea)){ echo $desc_item1bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluea" name="constract_drawing2bluea"
                                    value="<?php if(!empty($constract_drawing2bluea)){ echo $constract_drawing2bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2bluea)) { echo $date_insert_drawing2bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluea"
                                  value="<?php if(!empty($constract_drawing2bluea)) { echo $date_insert_drawing2bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2bluea)){ echo implode(";",$co_owner2bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner2bluea" name="co_owner2bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluea">コメント</label>
                                      <p>
                                        <?=$desc_item2bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2bluea)){ echo $desc_item2bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluea" name="constract_drawing3bluea"
                                    value="<?php if(!empty($constract_drawing3bluea)){ echo $constract_drawing3bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3bluea)) { echo $date_insert_drawing3bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluea"
                                  value="<?php if(!empty($constract_drawing3bluea)) { echo $date_insert_drawing3bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3bluea)){ echo implode(";",$co_owner3bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner3bluea" name="co_owner3bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluea">コメント</label>
                                      <p>
                                        <?=$desc_item3bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3bluea)){ echo $desc_item3bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluea" name="constract_drawing4bluea"
                                    value="<?php if(!empty($constract_drawing4bluea)){ echo $constract_drawing4bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4bluea)) { echo $date_insert_drawing4bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluea"
                                  value="<?php if(!empty($constract_drawing4bluea)) { echo $date_insert_drawing4bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4bluea)){ echo implode(";",$co_owner4bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner4bluea" name="co_owner4bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluea">コメント</label>
                                      <p>
                                        <?=$desc_item4bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4bluea)){ echo $desc_item4bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluea" name="constract_drawing5bluea"
                                    value="<?php if(!empty($constract_drawing5bluea)){ echo $constract_drawing5bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5bluea)) { echo $date_insert_drawing5bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluea"
                                  value="<?php if(!empty($constract_drawing5bluea)) { echo $date_insert_drawing5bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5bluea)){ echo implode(";",$co_owner5bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner5bluea" name="co_owner5bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluea">コメント</label>
                                      <p>
                                        <?=$desc_item5bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5bluea)){ echo $desc_item5bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluea" name="constract_drawing6bluea"
                                    value="<?php if(!empty($constract_drawing6bluea)){ echo $constract_drawing6bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6bluea)) { echo $date_insert_drawing6bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluea"
                                  value="<?php if(!empty($constract_drawing6bluea)) { echo $date_insert_drawing6bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6bluea)){ echo implode(";",$co_owner6bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner6bluea" name="co_owner6bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluea">コメント</label>
                                      <p>
                                        <?=$desc_item6bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6bluea)){ echo $desc_item6bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluea" name="constract_drawing7bluea"
                                    value="<?php if(!empty($constract_drawing7bluea)){ echo $constract_drawing7bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7bluea)) { echo $date_insert_drawing7bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluea"
                                  value="<?php if(!empty($constract_drawing7bluea)) { echo $date_insert_drawing7bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7bluea)){ echo implode(";",$co_owner7bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner7bluea" name="co_owner7bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluea">コメント</label>
                                      <p>
                                        <?=$desc_item7bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7bluea)){ echo $desc_item7bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluea" name="constract_drawing8bluea"
                                    value="<?php if(!empty($constract_drawing8bluea)){ echo $constract_drawing8bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8bluea)) { echo $date_insert_drawing8bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluea"
                                  value="<?php if(!empty($constract_drawing8bluea)) { echo $date_insert_drawing8bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8bluea)){ echo implode(";",$co_owner8bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner8bluea" name="co_owner8bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluea">コメント</label>
                                      <p>
                                        <?=$desc_item8bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8bluea)){ echo $desc_item8bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluea" name="constract_drawing9bluea"
                                    value="<?php if(!empty($constract_drawing9bluea)){ echo $constract_drawing9bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9bluea)) { echo $date_insert_drawing9bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluea"
                                  value="<?php if(!empty($constract_drawing9bluea)) { echo $date_insert_drawing9bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9bluea)){ echo implode(";",$co_owner9bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner9bluea" name="co_owner9bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluea">コメント</label>
                                      <p>
                                        <?=$desc_item9bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9bluea)){ echo $desc_item9bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10bluea?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluea" name="constract_drawing10bluea"
                                    value="<?php if(!empty($constract_drawing10bluea)){ echo $constract_drawing10bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10bluea)) { echo $date_insert_drawing10bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluea"
                                  value="<?php if(!empty($constract_drawing10bluea)) { echo $date_insert_drawing10bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10bluea)){ echo implode(";",$co_owner10bluea);}?>
                                    </p>
                                    <input type="text" id="co_owner10bluea" name="co_owner10bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluea">コメント</label>
                                      <p>
                                        <?=$desc_item10bluea?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10bluea)){ echo $desc_item10bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 2BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingTwoBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                              <div class="leftSide">
                                <span>2</span>
                                <p>工事工程表 </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column2blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1blueb" name="constract_drawing1blueb"
                                    value="<?php if(!empty($constract_drawing1blueb)){ echo $constract_drawing1blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1blueb)) { echo $date_insert_drawing1blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1blueb"
                                  value="<?php if(!empty($constract_drawing1blueb)) { echo $date_insert_drawing1blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1blueb)){ echo implode(";",$co_owner1blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner1blueb" name="co_owner1blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1blueb">コメント</label>
                                      <p>
                                        <?=$desc_item1blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1blueb)){ echo $desc_item1blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2blueb" name="constract_drawing2blueb"
                                    value="<?php if(!empty($constract_drawing2blueb)){ echo $constract_drawing2blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2blueb)) { echo $date_insert_drawing2blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2blueb"
                                  value="<?php if(!empty($constract_drawing2blueb)) { echo $date_insert_drawing2blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2blueb)){ echo implode(";",$co_owner2blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner2blueb" name="co_owner2blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2blueb">コメント</label>
                                      <p>
                                        <?=$desc_item2blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2blueb)){ echo $desc_item2blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3blueb" name="constract_drawing3blueb"
                                    value="<?php if(!empty($constract_drawing3blueb)){ echo $constract_drawing3blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3blueb)) { echo $date_insert_drawing3blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3blueb"
                                  value="<?php if(!empty($constract_drawing3blueb)) { echo $date_insert_drawing3blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3blueb)){ echo implode(";",$co_owner3blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner3blueb" name="co_owner3blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3blueb">コメント</label>
                                      <p>
                                        <?=$desc_item3blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3blueb)){ echo $desc_item3blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4blueb" name="constract_drawing4blueb"
                                    value="<?php if(!empty($constract_drawing4blueb)){ echo $constract_drawing4blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4blueb)) { echo $date_insert_drawing4blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4blueb"
                                  value="<?php if(!empty($constract_drawing4blueb)) { echo $date_insert_drawing4blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4blueb)){ echo implode(";",$co_owner4blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner4blueb" name="co_owner4blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4blueb">コメント</label>
                                      <p>
                                        <?=$desc_item4blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4blueb)){ echo $desc_item4blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5blueb" name="constract_drawing5blueb"
                                    value="<?php if(!empty($constract_drawing5blueb)){ echo $constract_drawing5blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5blueb)) { echo $date_insert_drawing5blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5blueb"
                                  value="<?php if(!empty($constract_drawing5blueb)) { echo $date_insert_drawing5blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5blueb)){ echo implode(";",$co_owner5blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner5blueb" name="co_owner5blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5blueb">コメント</label>
                                      <p>
                                        <?=$desc_item5blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5blueb)){ echo $desc_item5blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6blueb" name="constract_drawing6blueb"
                                    value="<?php if(!empty($constract_drawing6blueb)){ echo $constract_drawing6blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6blueb)) { echo $date_insert_drawing6blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6blueb"
                                  value="<?php if(!empty($constract_drawing6blueb)) { echo $date_insert_drawing6blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6blueb)){ echo implode(";",$co_owner6blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner6blueb" name="co_owner6blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6blueb">コメント</label>
                                      <p>
                                        <?=$desc_item6blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6blueb)){ echo $desc_item6blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7blueb" name="constract_drawing7blueb"
                                    value="<?php if(!empty($constract_drawing7blueb)){ echo $constract_drawing7blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7blueb)) { echo $date_insert_drawing7blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7blueb"
                                  value="<?php if(!empty($constract_drawing7blueb)) { echo $date_insert_drawing7blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7blueb)){ echo implode(";",$co_owner7blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner7blueb" name="co_owner7blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7blueb">コメント</label>
                                      <p>
                                        <?=$desc_item7blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7blueb)){ echo $desc_item7blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8blueb" name="constract_drawing8blueb"
                                    value="<?php if(!empty($constract_drawing8blueb)){ echo $constract_drawing8blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8blueb)) { echo $date_insert_drawing8blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8blueb"
                                  value="<?php if(!empty($constract_drawing8blueb)) { echo $date_insert_drawing8blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8blueb)){ echo implode(";",$co_owner8blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner8blueb" name="co_owner8blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8blueb">コメント</label>
                                      <p>
                                        <?=$desc_item8blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8blueb)){ echo $desc_item8blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9blueb" name="constract_drawing9blueb"
                                    value="<?php if(!empty($constract_drawing9blueb)){ echo $constract_drawing9blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9blueb)) { echo $date_insert_drawing9blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9blueb"
                                  value="<?php if(!empty($constract_drawing9blueb)) { echo $date_insert_drawing9blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9blueb)){ echo implode(";",$co_owner9blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner9blueb" name="co_owner9blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9blueb">コメント</label>
                                      <p>
                                        <?=$desc_item9blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9blueb)){ echo $desc_item9blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10blueb?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10blueb" name="constract_drawing10blueb"
                                    value="<?php if(!empty($constract_drawing10blueb)){ echo $constract_drawing10blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10blueb)) { echo $date_insert_drawing10blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10blueb"
                                  value="<?php if(!empty($constract_drawing10blueb)) { echo $date_insert_drawing10blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10blueb)){ echo implode(";",$co_owner10blueb);}?>
                                    </p>
                                    <input type="text" id="co_owner10blueb" name="co_owner10blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10blueb">コメント</label>
                                      <p>
                                        <?=$desc_item10blueb?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10blueb)){ echo $desc_item10blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 3 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingThreeBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                              <div class="leftSide">
                                <span>3</span>
                                <p>工事着工資料  </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column3blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluec" name="constract_drawing1bluec"
                                    value="<?php if(!empty($constract_drawing1bluec)){ echo $constract_drawing1bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1bluec)) { echo $date_insert_drawing1bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluec"
                                  value="<?php if(!empty($constract_drawing1bluec)) { echo $date_insert_drawing1bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1bluec)){ echo implode(";",$co_owner1bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner1bluec" name="co_owner1bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluec">コメント</label>
                                      <p>
                                        <?=$desc_item1bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1bluec)){ echo $desc_item1bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluec" name="constract_drawing2bluec"
                                    value="<?php if(!empty($constract_drawing2bluec)){ echo $constract_drawing2bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2bluec)) { echo $date_insert_drawing2bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluec"
                                  value="<?php if(!empty($constract_drawing2bluec)) { echo $date_insert_drawing2bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2bluec)){ echo implode(";",$co_owner2bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner2bluec" name="co_owner2bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluec">コメント</label>
                                      <p>
                                        <?=$desc_item2bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2bluec)){ echo $desc_item2bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluec" name="constract_drawing3bluec"
                                    value="<?php if(!empty($constract_drawing3bluec)){ echo $constract_drawing3bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3bluec)) { echo $date_insert_drawing3bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluec"
                                  value="<?php if(!empty($constract_drawing3bluec)) { echo $date_insert_drawing3bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3bluec)){ echo implode(";",$co_owner3bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner3bluec" name="co_owner3bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluec">コメント</label>
                                      <p>
                                        <?=$desc_item3bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3bluec)){ echo $desc_item3bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluec" name="constract_drawing4bluec"
                                    value="<?php if(!empty($constract_drawing4bluec)){ echo $constract_drawing4bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4bluec)) { echo $date_insert_drawing4bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluec"
                                  value="<?php if(!empty($constract_drawing4bluec)) { echo $date_insert_drawing4bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4bluec)){ echo implode(";",$co_owner4bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner4bluec" name="co_owner4bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluec">コメント</label>
                                      <p>
                                        <?=$desc_item4bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4bluec)){ echo $desc_item4bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluec" name="constract_drawing5bluec"
                                    value="<?php if(!empty($constract_drawing5bluec)){ echo $constract_drawing5bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5bluec)) { echo $date_insert_drawing5bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluec"
                                  value="<?php if(!empty($constract_drawing5bluec)) { echo $date_insert_drawing5bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5bluec)){ echo implode(";",$co_owner5bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner5bluec" name="co_owner5bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluec">コメント</label>
                                      <p>
                                        <?=$desc_item5bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5bluec)){ echo $desc_item5bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluec" name="constract_drawing6bluec"
                                    value="<?php if(!empty($constract_drawing6bluec)){ echo $constract_drawing6bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6bluec)) { echo $date_insert_drawing6bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluec"
                                  value="<?php if(!empty($constract_drawing6bluec)) { echo $date_insert_drawing6bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6bluec)){ echo implode(";",$co_owner6bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner6bluec" name="co_owner6bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluec">コメント</label>
                                      <p>
                                        <?=$desc_item6bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6bluec)){ echo $desc_item6bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluec" name="constract_drawing7bluec"
                                    value="<?php if(!empty($constract_drawing7bluec)){ echo $constract_drawing7bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7bluec)) { echo $date_insert_drawing7bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluec"
                                  value="<?php if(!empty($constract_drawing7bluec)) { echo $date_insert_drawing7bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7bluec)){ echo implode(";",$co_owner7bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner7bluec" name="co_owner7bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluec">コメント</label>
                                      <p>
                                        <?=$desc_item7bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7bluec)){ echo $desc_item7bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluec" name="constract_drawing8bluec"
                                    value="<?php if(!empty($constract_drawing8bluec)){ echo $constract_drawing8bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8bluec)) { echo $date_insert_drawing8bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluec"
                                  value="<?php if(!empty($constract_drawing8bluec)) { echo $date_insert_drawing8bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8bluec)){ echo implode(";",$co_owner8bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner8bluec" name="co_owner8bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluec">コメント</label>
                                      <p>
                                        <?=$desc_item8bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8bluec)){ echo $desc_item8bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluec" name="constract_drawing9bluec"
                                    value="<?php if(!empty($constract_drawing9bluec)){ echo $constract_drawing9bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9bluec)) { echo $date_insert_drawing9bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluec"
                                  value="<?php if(!empty($constract_drawing9bluec)) { echo $date_insert_drawing9bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9bluec)){ echo implode(";",$co_owner9bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner9bluec" name="co_owner9bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluec">コメント</label>
                                      <p>
                                        <?=$desc_item9bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9bluec)){ echo $desc_item9bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10bluec?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluec" name="constract_drawing10bluec"
                                    value="<?php if(!empty($constract_drawing10bluec)){ echo $constract_drawing10bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10bluec)) { echo $date_insert_drawing10bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluec"
                                  value="<?php if(!empty($constract_drawing10bluec)) { echo $date_insert_drawing10bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10bluec)){ echo implode(";",$co_owner10bluec);}?>
                                    </p>
                                    <input type="text" id="co_owner10bluec" name="co_owner10bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluec">コメント</label>
                                      <p>
                                        <?=$desc_item10bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10bluec)){ echo $desc_item10bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 4 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingFourBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                              <div class="leftSide">
                                <span>4</span>
                                <p>工事着工資料</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column4blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1blued" name="constract_drawing1blued"
                                    value="<?php if(!empty($constract_drawing1blued)){ echo $constract_drawing1blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1blued)) { echo $date_insert_drawing1blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1blued"
                                  value="<?php if(!empty($constract_drawing1blued)) { echo $date_insert_drawing1blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1blued)){ echo implode(";",$co_owner1blued);}?>
                                    </p>
                                    <input type="text" id="co_owner1blued" name="co_owner1blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1blued">コメント</label>
                                      <p>
                                        <?=$desc_item1blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1blued)){ echo $desc_item1blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2blued" name="constract_drawing2blued"
                                    value="<?php if(!empty($constract_drawing2blued)){ echo $constract_drawing2blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2blued)) { echo $date_insert_drawing2blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2blued"
                                  value="<?php if(!empty($constract_drawing2blued)) { echo $date_insert_drawing2blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2blued)){ echo implode(";",$co_owner2blued);}?>
                                    </p>
                                    <input type="text" id="co_owner2blued" name="co_owner2blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2blued">コメント</label>
                                      <p>
                                        <?=$desc_item2blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2blued)){ echo $desc_item2blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3blued" name="constract_drawing3blued"
                                    value="<?php if(!empty($constract_drawing3blued)){ echo $constract_drawing3blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3blued)) { echo $date_insert_drawing3blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3blued"
                                  value="<?php if(!empty($constract_drawing3blued)) { echo $date_insert_drawing3blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3blued)){ echo implode(";",$co_owner3blued);}?>
                                    </p>
                                    <input type="text" id="co_owner3blued" name="co_owner3blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3blued">コメント</label>
                                      <p>
                                        <?=$desc_item3blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3blued)){ echo $desc_item3blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4blued" name="constract_drawing4blued"
                                    value="<?php if(!empty($constract_drawing4blued)){ echo $constract_drawing4blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4blued)) { echo $date_insert_drawing4blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4blued"
                                  value="<?php if(!empty($constract_drawing4blued)) { echo $date_insert_drawing4blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4blued)){ echo implode(";",$co_owner4blued);}?>
                                    </p>
                                    <input type="text" id="co_owner4blued" name="co_owner4blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4blued">コメント</label>
                                      <p>
                                        <?=$desc_item4blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4blued)){ echo $desc_item4blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5blued" name="constract_drawing5blued"
                                    value="<?php if(!empty($constract_drawing5blued)){ echo $constract_drawing5blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5blued)) { echo $date_insert_drawing5blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5blued"
                                  value="<?php if(!empty($constract_drawing5blued)) { echo $date_insert_drawing5blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5blued)){ echo implode(";",$co_owner5blued);}?>
                                    </p>
                                    <input type="text" id="co_owner5blued" name="co_owner5blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5blued">コメント</label>
                                      <p>
                                        <?=$desc_item5blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5blued)){ echo $desc_item5blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6blued" name="constract_drawing6blued"
                                    value="<?php if(!empty($constract_drawing6blued)){ echo $constract_drawing6blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6blued)) { echo $date_insert_drawing6blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6blued"
                                  value="<?php if(!empty($constract_drawing6blued)) { echo $date_insert_drawing6blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6blued)){ echo implode(";",$co_owner6blued);}?>
                                    </p>
                                    <input type="text" id="co_owner6blued" name="co_owner6blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6blued">コメント</label>
                                      <p>
                                        <?=$desc_item6blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6blued)){ echo $desc_item6blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7blued" name="constract_drawing7blued"
                                    value="<?php if(!empty($constract_drawing7blued)){ echo $constract_drawing7blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7blued)) { echo $date_insert_drawing7blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7blued"
                                  value="<?php if(!empty($constract_drawing7blued)) { echo $date_insert_drawing7blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7blued)){ echo implode(";",$co_owner7blued);}?>
                                    </p>
                                    <input type="text" id="co_owner7blued" name="co_owner7blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7blued">コメント</label>
                                      <p>
                                        <?=$desc_item7blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7blued)){ echo $desc_item7blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8blued" name="constract_drawing8blued"
                                    value="<?php if(!empty($constract_drawing8blued)){ echo $constract_drawing8blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8blued)) { echo $date_insert_drawing8blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8blued"
                                  value="<?php if(!empty($constract_drawing8blued)) { echo $date_insert_drawing8blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8blued)){ echo implode(";",$co_owner8blued);}?>
                                    </p>
                                    <input type="text" id="co_owner8blued" name="co_owner8blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8blued">コメント</label>
                                      <p>
                                        <?=$desc_item8blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8blued)){ echo $desc_item8blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9blued" name="constract_drawing9blued"
                                    value="<?php if(!empty($constract_drawing9blued)){ echo $constract_drawing9blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9blued)) { echo $date_insert_drawing9blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9blued"
                                  value="<?php if(!empty($constract_drawing9blued)) { echo $date_insert_drawing9blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9blued)){ echo implode(";",$co_owner9blued);}?>
                                    </p>
                                    <input type="text" id="co_owner9blued" name="co_owner9blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9blued">コメント</label>
                                      <p>
                                        <?=$desc_item9blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9blued)){ echo $desc_item9blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10blued?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10blued" name="constract_drawing10blued"
                                    value="<?php if(!empty($constract_drawing10blued)){ echo $constract_drawing10blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10blued)) { echo $date_insert_drawing10blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10blued"
                                  value="<?php if(!empty($constract_drawing10blued)) { echo $date_insert_drawing10blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10blued)){ echo implode(";",$co_owner10blued);}?>
                                    </p>
                                    <input type="text" id="co_owner10blued" name="co_owner10blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10blued">コメント</label>
                                      <p>
                                        <?=$desc_item10blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10blued)){ echo $desc_item10blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 5 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingFiveBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                              <div class="leftSide">
                                <span>5</span>
                                <p>変更図面</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column5blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluee" name="constract_drawing1bluee"
                                    value="<?php if(!empty($constract_drawing1bluee)){ echo $constract_drawing1bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1bluee)) { echo $date_insert_drawing1bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluee"
                                  value="<?php if(!empty($constract_drawing1bluee)) { echo $date_insert_drawing1bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1bluee)){ echo implode(";",$co_owner1bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner1bluee" name="co_owner1bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluee">コメント</label>
                                      <p>
                                        <?=$desc_item1bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1bluee)){ echo $desc_item1bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluee" name="constract_drawing2bluee"
                                    value="<?php if(!empty($constract_drawing2bluee)){ echo $constract_drawing2bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2bluee)) { echo $date_insert_drawing2bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluee"
                                  value="<?php if(!empty($constract_drawing2bluee)) { echo $date_insert_drawing2bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2bluee)){ echo implode(";",$co_owner2bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner2bluee" name="co_owner2bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluee">コメント</label>
                                      <p>
                                        <?=$desc_item2bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2bluee)){ echo $desc_item2bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluee" name="constract_drawing3bluee"
                                    value="<?php if(!empty($constract_drawing3bluee)){ echo $constract_drawing3bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3bluee)) { echo $date_insert_drawing3bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluee"
                                  value="<?php if(!empty($constract_drawing3bluee)) { echo $date_insert_drawing3bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3bluee)){ echo implode(";",$co_owner3bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner3bluee" name="co_owner3bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluee">コメント</label>
                                      <p>
                                        <?=$desc_item3bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3bluee)){ echo $desc_item3bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluee" name="constract_drawing4bluee"
                                    value="<?php if(!empty($constract_drawing4bluee)){ echo $constract_drawing4bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4bluee)) { echo $date_insert_drawing4bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluee"
                                  value="<?php if(!empty($constract_drawing4bluee)) { echo $date_insert_drawing4bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4bluee)){ echo implode(";",$co_owner4bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner4bluee" name="co_owner4bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluee">コメント</label>
                                      <p>
                                        <?=$desc_item4bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4bluee)){ echo $desc_item4bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluee" name="constract_drawing5bluee"
                                    value="<?php if(!empty($constract_drawing5bluee)){ echo $constract_drawing5bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5bluee)) { echo $date_insert_drawing5bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluee"
                                  value="<?php if(!empty($constract_drawing5bluee)) { echo $date_insert_drawing5bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5bluee)){ echo implode(";",$co_owner5bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner5bluee" name="co_owner5bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluee">コメント</label>
                                      <p>
                                        <?=$desc_item5bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5bluee)){ echo $desc_item5bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluee" name="constract_drawing6bluee"
                                    value="<?php if(!empty($constract_drawing6bluee)){ echo $constract_drawing6bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6bluee)) { echo $date_insert_drawing6bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluee"
                                  value="<?php if(!empty($constract_drawing6bluee)) { echo $date_insert_drawing6bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6bluee)){ echo implode(";",$co_owner6bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner6bluee" name="co_owner6bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluee">コメント</label>
                                      <p>
                                        <?=$desc_item6bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6bluee)){ echo $desc_item6bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluee" name="constract_drawing7bluee"
                                    value="<?php if(!empty($constract_drawing7bluee)){ echo $constract_drawing7bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7bluee)) { echo $date_insert_drawing7bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluee"
                                  value="<?php if(!empty($constract_drawing7bluee)) { echo $date_insert_drawing7bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7bluee)){ echo implode(";",$co_owner7bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner7bluee" name="co_owner7bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluee">コメント</label>
                                      <p>
                                        <?=$desc_item7bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7bluee)){ echo $desc_item7bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluee" name="constract_drawing8bluee"
                                    value="<?php if(!empty($constract_drawing8bluee)){ echo $constract_drawing8bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8bluee)) { echo $date_insert_drawing8bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluee"
                                  value="<?php if(!empty($constract_drawing8bluee)) { echo $date_insert_drawing8bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8bluee)){ echo implode(";",$co_owner8bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner8bluee" name="co_owner8bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluee">コメント</label>
                                      <p>
                                        <?=$desc_item8bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8bluee)){ echo $desc_item8bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluee" name="constract_drawing9bluee"
                                    value="<?php if(!empty($constract_drawing9bluee)){ echo $constract_drawing9bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9bluee)) { echo $date_insert_drawing9bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluee"
                                  value="<?php if(!empty($constract_drawing9bluee)) { echo $date_insert_drawing9bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9bluee)){ echo implode(";",$co_owner9bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner9bluee" name="co_owner9bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluee">コメント</label>
                                      <p>
                                        <?=$desc_item9bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9bluee)){ echo $desc_item9bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10bluee?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluee" name="constract_drawing10bluee"
                                    value="<?php if(!empty($constract_drawing10bluee)){ echo $constract_drawing10bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10bluee)) { echo $date_insert_drawing10bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluee"
                                  value="<?php if(!empty($constract_drawing10bluee)) { echo $date_insert_drawing10bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10bluee)){ echo implode(";",$co_owner10bluee);}?>
                                    </p>
                                    <input type="text" id="co_owner10bluee" name="co_owner10bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluee">コメント</label>
                                      <p>
                                        <?=$desc_item10bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10bluee)){ echo $desc_item10bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 6 BLUE --> 
                      <div class="card blueCard">
                        <div class="card-header" id="headingSixBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                              <div class="leftSide">
                                <span>6</span>
                                <p>設備プランシート</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse" id="column6blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing1bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluef" name="constract_drawing1bluef"
                                    value="<?php if(!empty($constract_drawing1bluef)){ echo $constract_drawing1bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing1bluef)) { echo $date_insert_drawing1bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluef"
                                  value="<?php if(!empty($constract_drawing1bluef)) { echo $date_insert_drawing1bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner1bluef)){ echo implode(";",$co_owner1bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner1bluef" name="co_owner1bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check1bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluef">コメント</label>
                                      <p>
                                        <?=$desc_item1bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item1bluef)){ echo $desc_item1bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing2bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluef" name="constract_drawing2bluef"
                                    value="<?php if(!empty($constract_drawing2bluef)){ echo $constract_drawing2bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing2bluef)) { echo $date_insert_drawing2bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluef"
                                  value="<?php if(!empty($constract_drawing2bluef)) { echo $date_insert_drawing2bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner2bluef)){ echo implode(";",$co_owner2bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner2bluef" name="co_owner2bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check2bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluef">コメント</label>
                                      <p>
                                        <?=$desc_item2bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item2bluef)){ echo $desc_item2bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing3bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluef" name="constract_drawing3bluef"
                                    value="<?php if(!empty($constract_drawing3bluef)){ echo $constract_drawing3bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing3bluef)) { echo $date_insert_drawing3bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluef"
                                  value="<?php if(!empty($constract_drawing3bluef)) { echo $date_insert_drawing3bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner3bluef)){ echo implode(";",$co_owner3bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner3bluef" name="co_owner3bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check3bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluef">コメント</label>
                                      <p>
                                        <?=$desc_item3bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item3bluef)){ echo $desc_item3bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing4bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluef" name="constract_drawing4bluef"
                                    value="<?php if(!empty($constract_drawing4bluef)){ echo $constract_drawing4bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing4bluef)) { echo $date_insert_drawing4bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluef"
                                  value="<?php if(!empty($constract_drawing4bluef)) { echo $date_insert_drawing4bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner4bluef)){ echo implode(";",$co_owner4bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner4bluef" name="co_owner4bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check4bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluef">コメント</label>
                                      <p>
                                        <?=$desc_item4bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item4bluef)){ echo $desc_item4bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing5bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluef" name="constract_drawing5bluef"
                                    value="<?php if(!empty($constract_drawing5bluef)){ echo $constract_drawing5bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing5bluef)) { echo $date_insert_drawing5bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluef"
                                  value="<?php if(!empty($constract_drawing5bluef)) { echo $date_insert_drawing5bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner5bluef)){ echo implode(";",$co_owner5bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner5bluef" name="co_owner5bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check5bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluef">コメント</label>
                                      <p>
                                        <?=$desc_item5bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item5bluef)){ echo $desc_item5bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing6bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluef" name="constract_drawing6bluef"
                                    value="<?php if(!empty($constract_drawing6bluef)){ echo $constract_drawing6bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing6bluef)) { echo $date_insert_drawing6bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluef"
                                  value="<?php if(!empty($constract_drawing6bluef)) { echo $date_insert_drawing6bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner6bluef)){ echo implode(";",$co_owner6bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner6bluef" name="co_owner6bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check6bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluef">コメント</label>
                                      <p>
                                        <?=$desc_item6bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item6bluef)){ echo $desc_item6bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing7bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluef" name="constract_drawing7bluef"
                                    value="<?php if(!empty($constract_drawing7bluef)){ echo $constract_drawing7bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing7bluef)) { echo $date_insert_drawing7bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluef"
                                  value="<?php if(!empty($constract_drawing7bluef)) { echo $date_insert_drawing7bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner7bluef)){ echo implode(";",$co_owner7bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner7bluef" name="co_owner7bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check7bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluef">コメント</label>
                                      <p>
                                        <?=$desc_item7bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item7bluef)){ echo $desc_item7bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing8bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluef" name="constract_drawing8bluef"
                                    value="<?php if(!empty($constract_drawing8bluef)){ echo $constract_drawing8bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing8bluef)) { echo $date_insert_drawing8bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluef"
                                  value="<?php if(!empty($constract_drawing8bluef)) { echo $date_insert_drawing8bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner8bluef)){ echo implode(";",$co_owner8bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner8bluef" name="co_owner8bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check8bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluef">コメント</label>
                                      <p>
                                        <?=$desc_item8bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item8bluef)){ echo $desc_item8bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing9bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluef" name="constract_drawing9bluef"
                                    value="<?php if(!empty($constract_drawing9bluef)){ echo $constract_drawing9bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing9bluef)) { echo $date_insert_drawing9bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluef"
                                  value="<?php if(!empty($constract_drawing9bluef)) { echo $date_insert_drawing9bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner9bluef)){ echo implode(";",$co_owner9bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner9bluef" name="co_owner9bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check9bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluef">コメント</label>
                                      <p>
                                        <?=$desc_item9bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item9bluef)){ echo $desc_item9bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><?=$constract_drawing10bluef?></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluef" name="constract_drawing10bluef"
                                    value="<?php if(!empty($constract_drawing10bluef)){ echo $constract_drawing10bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($constract_drawing10bluef)) { echo $date_insert_drawing10bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluef"
                                  value="<?php if(!empty($constract_drawing10bluef)) { echo $date_insert_drawing10bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($co_owner10bluef)){ echo implode(";",$co_owner10bluef);}?>
                                    </p>
                                    <input type="text" id="co_owner10bluef" name="co_owner10bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($check10bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluef">コメント</label>
                                      <p>
                                        <?=$desc_item10bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($desc_item10bluef)){ echo $desc_item10bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


                  </div>

                </div>

                <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                        <input name="edit_button" id="" class="btn btn-primary" value="変更する"> -->
                  <br><br>
                <button name="" id="" class="btn custom-btn hvr-sink" type="submit">追加</button>
                <button name="edit_btn" value="edit" type="" id="" class="btn custom-btn white hvr-sink">変更する</button>
                <br><br>
              </form>
            </div>
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<?php $this->load->view('dashboard/dashboard_footer');?>
</body>

</html>