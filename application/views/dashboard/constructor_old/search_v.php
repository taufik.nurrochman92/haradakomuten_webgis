<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事検索</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事検索</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/search_data_construct" method="post" enctype="multipart/form-data">
                        
                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds">
                                <option selected disabled value="">選択してください</option>
                                <option value="SOB">SWEET HOME</option>
                                <option value="ビリーブの家">ビリーブの家</option>
                                <option value="リフォーム工事">リフォーム工事</option>
                                <option value="メンテナンス工事">メンテナンス工事</option>
                                <option value="新築工事">新築工事</option>
                                <option value="OB">OB</option>
                                <option value="その他">その他</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="from_contract_date" id="from_contract_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="">　</label>
                              <input type="date" class="form-control col-md-12" name="to_contract_date" id="to_contract_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="from_construction_start_date" id="from_construction_start_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="">　</label>
                              <input type="date" class="form-control col-md-12" name="to_construction_start_date" id="to_construction_start_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="from_upper_building_date" id="from_upper_building_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="">　</label>
                              <input type="date" class="form-control col-md-12" name="to_upper_building_date" id="to_upper_building_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="from_completion_date" id="from_completion_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                                <label for="">　</label>
                              <input type="date" class="form-control col-md-12" name="to_completion_date" id="to_completion_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="from_delivery_date" id="from_delivery_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="">　</label>
                              <input type="date" class="form-control col-md-12" name="to_delivery_date" id="to_delivery_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <br><br>
                        <button name="" id="" class="btn custom-btn hvr-sink" type="submit">検索する</button>
                        <br><br>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
  $('select').selectpicker();
</script>
</body>
</html>