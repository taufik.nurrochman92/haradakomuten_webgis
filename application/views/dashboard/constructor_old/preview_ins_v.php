<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_data" method="post" enctype="multipart/form-data">
                        
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" value="<?php if(!empty($cust_code)){ echo $cust_code; }?>" >
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($cust_name)){ echo $cust_name; }?>" >
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder="" value="<?php if(!empty($kinds)){ echo $kinds; }?>" > -->
                              <select class="form-control" name="kinds">
                                <option <?php if ($kinds == "") {echo "selected"; } ?> disabled value="">ご選択ください</option>
                                <option <?php if ($kinds == "SOB") {echo "selected"; } ?> value="SOB">Sweet Home</option>
                                <option <?php if ($kinds == "BOB") {echo "selected"; } ?> value="BOB">ビリーブの家</option>
                                <option <?php if ($kinds == "小工事") {echo "selected"; } ?> value="小工事">小工事</option>
                                <option <?php if ($kinds == "建設中") {echo "selected"; } ?> value="建設中">建設中</option>
                                <option <?php if ($kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option <?php if ($kinds == "材料販売") {echo "selected"; } ?> value="材料販売">材料販売</option>
                                <option <?php if ($kinds == "その他") {echo "selected"; } ?> value="その他">その他</option>
                                <option <?php if ($kinds == "NL") {echo "selected"; } ?> value="NL">NL</option>
                              </select>
                            </div>
                            <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No</label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="" value="<?php if(!empty($const_no)){ echo $const_no; }?>" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="remarks">備考</label>
                          <input type="text" class="form-control col-md-6" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" value="<?php if(!empty($remarks)){ echo $remarks; }?>" >
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($contract_date)){ echo $contract_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($construction_start_date)){ echo $construction_start_date; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($upper_building_date)){ echo $upper_building_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($completion_date)){ echo $completion_date; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($delivery_date)){ echo $delivery_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;" value="<?php if(!empty($amount_money)){ echo $amount_money; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <select class="form-control" name="sales_staff">
                                <option <?php if ($sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($sales_staff == "$sales_staff") {echo "selected"; } ?> value="<?=$sales_staff?>"><?=$sales_staff?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <select class="form-control" name="incharge_construction">
                                <option <?php if ($incharge_construction == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($incharge_construction == "$incharge_construction") {echo "selected"; } ?> value="<?=$incharge_construction?>"><?=$incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <select class="form-control" name="incharge_coordinator">
                                <option <?php if ($incharge_coordinator == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($incharge_coordinator == "$incharge_coordinator") {echo "selected"; } ?> value="<?=$incharge_coordinator?>"><?=$incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:25px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="contract">契約書</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($contract)) : ?>
                                    <input type="text" name="contract" value="<?= $contract ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="contract" name="contract" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $contract ?>">
                                    <label class="custom-file-label" for="pdf1" id="pdf1"><?php if($contract!="") echo $contract; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_contract1" value="<?php if(!empty($contract)) { echo $date_insert_contract1;}?>" hidden>
                                </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing1">契約図面 1</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing1)) : ?>
                                    <input type="text" name="constract_drawing1" value="<?= $constract_drawing1 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1 ?>">
                                    <label class="custom-file-label" for="pdf2" id="pdf2"><?php if($constract_drawing1!="") echo $constract_drawing1; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing1" value="<?php if(!empty($constract_drawing1)) { echo $date_insert_drawing1;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner1">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner1[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner1);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check1">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check1" id="check1" <?php if(!empty($check1)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item1">コメント</label>
                              <p><?=$desc_item1?></p>
                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1)){ echo $desc_item1; }?>">
                          </div>
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing2">契約図面 2</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing2)) : ?>
                                    <input type="text" name="constract_drawing2" value="<?= $constract_drawing2 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2 ?>">
                                    <label class="custom-file-label" for="pdf3" id="pdf3"><?php if($constract_drawing2!="") echo $constract_drawing2; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing2" value="<?php if(!empty($constract_drawing1)) { echo $date_insert_drawing2;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner2">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner2[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner2);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check2">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check2" id="check2"  <?php if(!empty($check2)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item2">コメント</label>
                              <p><?=$desc_item2?></p>
                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2)){ echo $desc_item2; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing3">契約図面 3</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing3)) : ?>
                                    <input type="text" name="constract_drawing3" value="<?= $constract_drawing3 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3 ?>">
                                    <label class="custom-file-label" for="pdf4" id="pdf4"><?php if($constract_drawing3!="") echo $constract_drawing3; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing3" value="<?php if(!empty($constract_drawing1)) { echo $date_insert_drawing3;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner3">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner3[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner3);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check3">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check3" id="check3" <?php if(!empty($check3)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item3">コメント</label>
                              <p><?=$desc_item3?></p>
                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3)){ echo $desc_item3; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing4">契約図面 4</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing4)) : ?>
                                    <input type="text" name="constract_drawing4" value="<?= $constract_drawing4 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4 ?>">
                                    <label class="custom-file-label" for="pdf5" id="pdf5"><?php if($constract_drawing4!="") echo $constract_drawing4; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing4" value="<?php if(!empty($constract_drawing1)) { echo $date_insert_drawing4;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner4">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner4[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner4);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check4">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check4" id="check4"<?php if(!empty($check4)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item4">コメント</label>
                              <p><?=$desc_item4?></p>
                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4)){ echo $desc_item4; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing5">契約図面 5</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing5)) : ?>
                                    <input type="text" name="constract_drawing5" value="<?= $constract_drawing5 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5 ?>">
                                    <label class="custom-file-label" for="pdf6" id="pdf6"><?php if($constract_drawing5!="") echo $constract_drawing5; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing5)) { echo $date_insert_drawing5;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner5">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner5[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner5);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check5">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check5" id="check5" <?php if(!empty($check5)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item5">コメント</label>
                              <p><?=$desc_item5?></p>
                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5)){ echo $desc_item5; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing6">契約図面 6</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing6)) : ?>
                                    <input type="text" name="constract_drawing6" value="<?= $constract_drawing6 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6 ?>">
                                    <label class="custom-file-label" for="pdf7" id="pdf7"><?php if($constract_drawing6!="") echo $constract_drawing6; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing6)) { echo $date_insert_drawing6;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner6">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner6[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner6);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check6">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check6" id="check6" <?php if(!empty($check6)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item6">コメント</label>
                              <p><?=$desc_item6?></p>
                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6)){ echo $desc_item6; }?>" hidden>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing7">契約図面 7</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing7)) : ?>
                                    <input type="text" name="constract_drawing7" value="<?= $constract_drawing7 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7 ?>">
                                    <label class="custom-file-label" for="pdf8" id="pdf8"><?php if($constract_drawing7!="") echo $constract_drawing7; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing7)) { echo $date_insert_drawing7;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner7">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner7[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner7);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check7">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check7" id="check7" <?php if(!empty($check7)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item7">コメント</label>
                              <p><?=$desc_item7?></p>
                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7)){ echo $desc_item7; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing8">契約図面 8</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing8)) : ?>
                                    <input type="text" name="constract_drawing8" value="<?= $constract_drawing8 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8 ?>">
                                    <label class="custom-file-label" for="pdf9" id="pdf9"><?php if($constract_drawing8!="") echo $constract_drawing8; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing8)) { echo $date_insert_drawing8;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner8">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner8[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner8);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check8">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check8" id="check8" <?php if(!empty($check8)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item8">コメント</label>
                              <p><?=$desc_item8?></p>
                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8)){ echo $desc_item8; }?>">
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;border-bottom: 2px solid;">
                          <div class="form-group">
                            <label for="constract_drawing9">契約図面 9</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <?php if (!empty($constract_drawing9)) : ?>
                                    <input type="text" name="constract_drawing9" value="<?= $constract_drawing9 ?>" hidden>
                                    <?php endif;?>
                                    <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9 ?>">
                                    <label class="custom-file-label" for="pdf10" id="pdf10"><?php if($constract_drawing9!="") echo $constract_drawing9; else echo"ファイルを選択";?></label>
                                    <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing9)) { echo $date_insert_drawing9;}?>" hidden>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="co_owner9">共有業者</label>
                              <select class="form-control selectpicker" name="co_owner9[]" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                              <?php
                                $selected_array = explode(';',$co_owner9);
                                foreach($data as $list){
                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                }  
                              ?>
                              </select>
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="check9">施主様共有</label>
                              <input type="checkbox" class="col-md-3" name="check9" id="check9" <?php if(!empty($check9)) { echo 'checked';}?> aria-describedby="helpId" placeholder="" value="1">
                              <span>共有する</span>
                            </div>
                          </div>
                          <div class="form-group col-md-12" style="margin-bottom:15px;">
                              <label for="desc_item9">コメント</label>
                              <p><?=$desc_item9?></p>
                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9)){ echo $desc_item9; }?>">
                          </div>
                        </div>
                        <button name="" id="" class="btn hvr-sink" type="submit">追加</button>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
  $('select').selectpicker();

  $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
</script>
</body>
</html>