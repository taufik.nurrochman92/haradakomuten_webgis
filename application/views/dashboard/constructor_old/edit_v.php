<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 工事情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 工事情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_construction/<?=$data->construct_id?>" method="post" enctype="multipart/form-data">
                        
                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" value="<?=$data->cust_code?>">
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="" value="<?=$data->cust_name?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds">
                                <option <?php if ($data->kinds == "") {echo "selected"; } ?> disabled value="">ご選択ください</option>
                                <option <?php if ($data->kinds == "SOB") {echo "selected"; } ?> value="SOB">Sweet Home</option>
                                <option <?php if ($data->kinds == "BOB") {echo "selected"; } ?> value="BOB">ビリーブの家</option>
                                <option <?php if ($data->kinds == "小工事") {echo "selected"; } ?> value="小工事">小工事</option>
                                <option <?php if ($data->kinds == "建設中") {echo "selected"; } ?> value="建設中">建設中</option>
                                <option <?php if ($data->kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option <?php if ($data->kinds == "材料販売") {echo "selected"; } ?> value="材料販売">材料販売</option>
                                <option <?php if ($data->kinds == "その他") {echo "selected"; } ?> value="その他">その他</option>
                                <option <?php if ($data->kinds == "NL") {echo "selected"; } ?> value="NL">NL</option>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No</label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="" value="<?=$data->const_no?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-6 mb30" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"><?=$data->remarks?></textarea>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->contract_date)) { echo isset($data->contract_date) ? set_value('contract_date', date('Y-m-d', strtotime($data->contract_date))) : set_value('contract_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->construction_start_date)) { echo isset($data->construction_start_date) ? set_value('construction_start_date', date('Y-m-d', strtotime($data->construction_start_date))) : set_value('construction_start_date'); };?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->upper_building_date)) { echo isset($data->upper_building_date) ? set_value('upper_building_date', date('Y-m-d', strtotime($data->upper_building_date))) : set_value('upper_building_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->completion_date)) { echo isset($data->completion_date) ? set_value('completion_date', date('Y-m-d', strtotime($data->completion_date))) : set_value('completion_date'); };?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->delivery_date)) { echo isset($data->delivery_date) ? set_value('delivery_date', date('Y-m-d', strtotime($data->delivery_date))) : set_value('delivery_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;" value="<?=$data->amount_money?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">共有業者</label>
                              <select class="form-control" name="sales_staff">
                                <option <?php if ($data->sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->sales_staff == "$data->sales_staff") {echo "selected"; } ?> value="<?=$data->sales_staff?>"><?=$data->sales_staff?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <select class="form-control" name="incharge_construction">
                                <option <?php if ($data->incharge_construction == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->incharge_construction == "$data->incharge_construction") {echo "selected"; } ?> value="<?=$data->incharge_construction?>"><?=$data->incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <select class="form-control" name="sales_staff">
                                <option <?php if ($data->sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->sales_staff == "$data->sales_staff") {echo "selected"; } ?> value="<?=$data->sales_staff?>"><?=$data->sales_staff?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div id="accordion" class="customAccordion">

                            <!-- CATEGORY 1 -->
                            <div class="card">

                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>請負契約書・土地契約書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1" id="pdf1"><?php if($data->constract_drawing1!="") echo $data->constract_drawing1; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1" name="check1" aria-describedby="helpId" <?php if(!empty($data->check1)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2" id="pdf2"><?php if($data->constract_drawing2!="") echo $data->constract_drawing2; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2" name="check2" aria-describedby="helpId" <?php if(!empty($data->check2)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3" id="pdf3"><?php if($data->constract_drawing3!="") echo $data->constract_drawing3; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3" name="check3" aria-describedby="helpId" <?php if(!empty($data->check3)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4" id="pdf4"><?php if($data->constract_drawing4!="") echo $data->constract_drawing4; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4" name="check4" aria-describedby="helpId" <?php if(!empty($data->check4)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5" id="pdf5"><?php if($data->constract_drawing5!="") echo $data->constract_drawing5; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5 class="subTitle">施主様共有</h5>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5" name="check5" aria-describedby="helpId" <?php if(!empty($data->check5)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6" id="pdf6"><?php if($data->constract_drawing6!="") echo $data->constract_drawing6; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6 class="subTitle">施主様共有</h6>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6" name="check6" aria-describedby="helpId" <?php if(!empty($data->check6)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7" id="pdf7"><?php if($data->constract_drawing7!="") echo $data->constract_drawing7; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7 class="subTitle">施主様共有</h7>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7" name="check7" aria-describedby="helpId" <?php if(!empty($data->check7)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8" id="pdf8"><?php if($data->constract_drawing8!="") echo $data->constract_drawing8; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8 class="subTitle">施主様共有</h8>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8" name="check8" aria-describedby="helpId" <?php if(!empty($data->check8)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9" id="pdf9"><?php if($data->constract_drawing9!="") echo $data->constract_drawing9; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9 class="subTitle">施主様共有</h9>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9" name="check9" aria-describedby="helpId" <?php if(!empty($data->check9)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10" id="pdf10"><?php if($data->constract_drawing10!="") echo $data->constract_drawing10; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10" name="constract_drawing10" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10 class="subTitle">施主様共有</h10>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10" name="check10" aria-describedby="helpId" <?php if(!empty($data->check10)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 2 --> 
                            <div class="card">

                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>長期優良住宅認定書・性能評価書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1b" id="pdf1b"><?php if($data->constract_drawing1b!="") echo $data->constract_drawing1b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1b" name="constract_drawing1b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1b" name="check1b" aria-describedby="helpId" <?php if(!empty($data->check1b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2b" id="pdf2b"><?php if($data->constract_drawing2b!="") echo $data->constract_drawing2b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2b" name="constract_drawing2b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2b" name="check2b" aria-describedby="helpId" <?php if(!empty($data->check2b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3b" id="pdf3b"><?php if($data->constract_drawing3b!="") echo $data->constract_drawing3b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3b" name="constract_drawing3b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3b" name="check3b" aria-describedby="helpId" <?php if(!empty($data->check3b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4b" id="pdf4b"><?php if($data->constract_drawing4b!="") echo $data->constract_drawing4b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4b" name="constract_drawing4b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4b" name="check4b" aria-describedby="helpId" <?php if(!empty($data->check4b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5b" id="pdf5b"><?php if($data->constract_drawing5b!="") echo $data->constract_drawing5b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5b" name="constract_drawing5b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5b class="subTitle">施主様共有</h5b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5b" name="check5b" aria-describedby="helpId" <?php if(!empty($data->check5b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6b" id="pdf6b"><?php if($data->constract_drawing6b!="") echo $data->constract_drawing6b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6b" name="constract_drawing6b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6b class="subTitle">施主様共有</h6b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6b" name="check6b" aria-describedby="helpId" <?php if(!empty($data->check6b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7b" id="pdf7b"><?php if($data->constract_drawing7b!="") echo $data->constract_drawing7b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7b" name="constract_drawing7b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7b class="subTitle">施主様共有</h7b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7b" name="check7b" aria-describedby="helpId" <?php if(!empty($data->check7b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8b" id="pdf8b"><?php if($data->constract_drawing8b!="") echo $data->constract_drawing8b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8b" name="constract_drawing8b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8b class="subTitle">施主様共有</h8b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8b" name="check8b" aria-describedby="helpId" <?php if(!empty($data->check8b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9b" id="pdf9b"><?php if($data->constract_drawing9b!="") echo $data->constract_drawing9b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9b" name="constract_drawing9b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9b class="subTitle">施主様共有</h9b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9b" name="check9b" aria-describedby="helpId" <?php if(!empty($data->check9b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10b" id="pdf10b"><?php if($data->constract_drawing10b!="") echo $data->constract_drawing10b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10b" name="constract_drawing10b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10b class="subTitle">施主様共有</h10b>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10b" name="check10b" aria-describedby="helpId" <?php if(!empty($data->check10b)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10b">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10b?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 3 --> 
                            <div class="card">

                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>地盤調査・地盤改良</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1c" id="pdf1c"><?php if($data->constract_drawing1c!="") echo $data->constract_drawing1c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1c" name="constract_drawing1c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1c" name="check1c" aria-describedby="helpId" <?php if(!empty($data->check1c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2c" id="pdf2c"><?php if($data->constract_drawing2c!="") echo $data->constract_drawing2c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2c" name="constract_drawing2c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2c" name="check2c" aria-describedby="helpId" <?php if(!empty($data->check2c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3c" id="pdf3c"><?php if($data->constract_drawing3c!="") echo $data->constract_drawing3c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3c" name="constract_drawing3c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3c" name="check3c" aria-describedby="helpId" <?php if(!empty($data->check3c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4c" id="pdf4c"><?php if($data->constract_drawing4c!="") echo $data->constract_drawing4c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4c" name="constract_drawing4c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4c" name="check4c" aria-describedby="helpId" <?php if(!empty($data->check4c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5c" id="pdf5c"><?php if($data->constract_drawing5c!="") echo $data->constract_drawing5c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5c" name="constract_drawing5c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5c class="subTitle">施主様共有</h5c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5c" name="check5c" aria-describedby="helpId" <?php if(!empty($data->check5c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6c" id="pdf6c"><?php if($data->constract_drawing6c!="") echo $data->constract_drawing6c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6c" name="constract_drawing6c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6c class="subTitle">施主様共有</h6c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6c" name="check6c" aria-describedby="helpId" <?php if(!empty($data->check6c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7c" id="pdf7c"><?php if($data->constract_drawing7c!="") echo $data->constract_drawing7c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7c" name="constract_drawing7c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7c class="subTitle">施主様共有</h7c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7c" name="check7c" aria-describedby="helpId" <?php if(!empty($data->check7c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8c" id="pdf8c"><?php if($data->constract_drawing8c!="") echo $data->constract_drawing8c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8c" name="constract_drawing8c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8c class="subTitle">施主様共有</h8c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8c" name="check8c" aria-describedby="helpId" <?php if(!empty($data->check8c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9c" id="pdf9c"><?php if($data->constract_drawing9c!="") echo $data->constract_drawing9c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9c" name="constract_drawing9c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9c class="subTitle">施主様共有</h9c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9c" name="check9c" aria-describedby="helpId" <?php if(!empty($data->check9c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10c" id="pdf10c"><?php if($data->constract_drawing10c!="") echo $data->constract_drawing10c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10c" name="constract_drawing10c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10c class="subTitle">施主様共有</h10c>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10c" name="check10c" aria-describedby="helpId" <?php if(!empty($data->check10c)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10c">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10c?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 4 --> 
                            <div class="card">

                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>点検履歴</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1d" id="pdf1d"><?php if($data->constract_drawing1d!="") echo $data->constract_drawing1d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1d" name="constract_drawing1d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1d" name="check1d" aria-describedby="helpId" <?php if(!empty($data->check1d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2d" id="pdf2d"><?php if($data->constract_drawing2d!="") echo $data->constract_drawing2d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2d" name="constract_drawing2d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2d" name="check2d" aria-describedby="helpId" <?php if(!empty($data->check2d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3d" id="pdf3d"><?php if($data->constract_drawing3d!="") echo $data->constract_drawing3d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3d" name="constract_drawing3d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3d" name="check3d" aria-describedby="helpId" <?php if(!empty($data->check3d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4d" id="pdf4d"><?php if($data->constract_drawing4d!="") echo $data->constract_drawing4d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4d" name="constract_drawing4d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4d" name="check4d" aria-describedby="helpId" <?php if(!empty($data->check4d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5d" id="pdf5d"><?php if($data->constract_drawing5d!="") echo $data->constract_drawing5d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5d" name="constract_drawing5d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5d class="subTitle">施主様共有</h5d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5d" name="check5d" aria-describedby="helpId" <?php if(!empty($data->check5d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6d" id="pdf6d"><?php if($data->constract_drawing6d!="") echo $data->constract_drawing6d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6d" name="constract_drawing6d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6d class="subTitle">施主様共有</h6d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6d" name="check6d" aria-describedby="helpId" <?php if(!empty($data->check6d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7d" id="pdf7d"><?php if($data->constract_drawing7d!="") echo $data->constract_drawing7d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7d" name="constract_drawing7d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7d class="subTitle">施主様共有</h7d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7d" name="check7d" aria-describedby="helpId" <?php if(!empty($data->check7d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8d" id="pdf8d"><?php if($data->constract_drawing8d!="") echo $data->constract_drawing8d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8d" name="constract_drawing8d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8d class="subTitle">施主様共有</h8d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8d" name="check8d" aria-describedby="helpId" <?php if(!empty($data->check8d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9d" id="pdf9d"><?php if($data->constract_drawing9d!="") echo $data->constract_drawing9d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9d" name="constract_drawing9d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9d class="subTitle">施主様共有</h9d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9d" name="check9d" aria-describedby="helpId" <?php if(!empty($data->check9d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10d" id="pdf10d"><?php if($data->constract_drawing10d!="") echo $data->constract_drawing10d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10d" name="constract_drawing10d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10d class="subTitle">施主様共有</h10d>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10d" name="check10d" aria-describedby="helpId" <?php if(!empty($data->check10d)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10d">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10d?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 5 --> 
                            <div class="card">

                              <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>三者引継ぎ合意書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1e" id="pdf1e"><?php if($data->constract_drawing1e!="") echo $data->constract_drawing1e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1e" name="constract_drawing1e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1e" name="check1e" aria-describedby="helpId" <?php if(!empty($data->check1e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2e" id="pdf2e"><?php if($data->constract_drawing2e!="") echo $data->constract_drawing2e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2e" name="constract_drawing2e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2e" name="check2e" aria-describedby="helpId" <?php if(!empty($data->check2e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3e" id="pdf3e"><?php if($data->constract_drawing3e!="") echo $data->constract_drawing3e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3e" name="constract_drawing3e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3e" name="check3e" aria-describedby="helpId" <?php if(!empty($data->check3e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4e" id="pdf4e"><?php if($data->constract_drawing4e!="") echo $data->constract_drawing4e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4e" name="constract_drawing4e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4e" name="check4e" aria-describedby="helpId" <?php if(!empty($data->check4e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5e" id="pdf5e"><?php if($data->constract_drawing5e!="") echo $data->constract_drawing5e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5e" name="constract_drawing5e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5e class="subTitle">施主様共有</h5e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5e" name="check5e" aria-describedby="helpId" <?php if(!empty($data->check5e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6e" id="pdf6e"><?php if($data->constract_drawing6e!="") echo $data->constract_drawing6e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6e" name="constract_drawing6e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6e class="subTitle">施主様共有</h6e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6e" name="check6e" aria-describedby="helpId" <?php if(!empty($data->check6e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7e" id="pdf7e"><?php if($data->constract_drawing7e!="") echo $data->constract_drawing7e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7e" name="constract_drawing7e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7e class="subTitle">施主様共有</h7e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7e" name="check7e" aria-describedby="helpId" <?php if(!empty($data->check7e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8e" id="pdf8e"><?php if($data->constract_drawing8e!="") echo $data->constract_drawing8e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8e" name="constract_drawing8e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8e class="subTitle">施主様共有</h8e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8e" name="check8e" aria-describedby="helpId" <?php if(!empty($data->check8e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9e" id="pdf9e"><?php if($data->constract_drawing9e!="") echo $data->constract_drawing9e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9e" name="constract_drawing9e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9e class="subTitle">施主様共有</h9e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9e" name="check9e" aria-describedby="helpId" <?php if(!empty($data->check9e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10e" id="pdf10e"><?php if($data->constract_drawing10e!="") echo $data->constract_drawing10e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10e" name="constract_drawing10e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10e class="subTitle">施主様共有</h10e>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10e" name="check10e" aria-describedby="helpId" <?php if(!empty($data->check10e)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10e">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10e?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 6 --> 
                            <div class="card">

                              <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>住宅仕様書・プレカット資料 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1f" id="pdf1f"><?php if($data->constract_drawing1f!="") echo $data->constract_drawing1f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1f" name="constract_drawing1f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1f" name="check1f" aria-describedby="helpId" <?php if(!empty($data->check1f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2f" id="pdf2f"><?php if($data->constract_drawing2f!="") echo $data->constract_drawing2f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2f" name="constract_drawing2f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2f" name="check2f" aria-describedby="helpId" <?php if(!empty($data->check2f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3f" id="pdf3f"><?php if($data->constract_drawing3f!="") echo $data->constract_drawing3f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3f" name="constract_drawing3f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3f" name="check3f" aria-describedby="helpId" <?php if(!empty($data->check3f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4f" id="pdf4f"><?php if($data->constract_drawing4f!="") echo $data->constract_drawing4f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4f" name="constract_drawing4f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4f" name="check4f" aria-describedby="helpId" <?php if(!empty($data->check4f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5f" id="pdf5f"><?php if($data->constract_drawing5f!="") echo $data->constract_drawing5f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5f" name="constract_drawing5f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5f class="subTitle">施主様共有</h5f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5f" name="check5f" aria-describedby="helpId" <?php if(!empty($data->check5f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6f" id="pdf6f"><?php if($data->constract_drawing6f!="") echo $data->constract_drawing6f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6f" name="constract_drawing6f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6f class="subTitle">施主様共有</h6f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6f" name="check6f" aria-describedby="helpId" <?php if(!empty($data->check6f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7f" id="pdf7f"><?php if($data->constract_drawing7f!="") echo $data->constract_drawing7f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7f" name="constract_drawing7f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7f class="subTitle">施主様共有</h7f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7f" name="check7f" aria-describedby="helpId" <?php if(!empty($data->check7f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8f" id="pdf8f"><?php if($data->constract_drawing8f!="") echo $data->constract_drawing8f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8f" name="constract_drawing8f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8f class="subTitle">施主様共有</h8f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8f" name="check8f" aria-describedby="helpId" <?php if(!empty($data->check8f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9f" id="pdf9f"><?php if($data->constract_drawing9f!="") echo $data->constract_drawing9f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9f" name="constract_drawing9f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9f class="subTitle">施主様共有</h9f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9f" name="check9f" aria-describedby="helpId" <?php if(!empty($data->check9f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10f" id="pdf10f"><?php if($data->constract_drawing10f!="") echo $data->constract_drawing10f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10f" name="constract_drawing10f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10f class="subTitle">施主様共有</h10f>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10f" name="check10f" aria-describedby="helpId" <?php if(!empty($data->check10f)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10f">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10f?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 7 --> 
                            <div class="card">

                              <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>工事写真 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column7">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1g" id="pdf1g"><?php if($data->constract_drawing1g!="") echo $data->constract_drawing1g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1g" name="constract_drawing1g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1g" name="check1g" aria-describedby="helpId" <?php if(!empty($data->check1g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2g" id="pdf2g"><?php if($data->constract_drawing2g!="") echo $data->constract_drawing2g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2g" name="constract_drawing2g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2g" name="check2g" aria-describedby="helpId" <?php if(!empty($data->check2g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3g" id="pdf3g"><?php if($data->constract_drawing3g!="") echo $data->constract_drawing3g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3g" name="constract_drawing3g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3g" name="check3g" aria-describedby="helpId" <?php if(!empty($data->check3g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4g" id="pdf4g"><?php if($data->constract_drawing4g!="") echo $data->constract_drawing4g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4g" name="constract_drawing4g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4g" name="check4g" aria-describedby="helpId" <?php if(!empty($data->check4g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5g" id="pdf5g"><?php if($data->constract_drawing5g!="") echo $data->constract_drawing5g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5g" name="constract_drawing5g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h5g class="subTitle">施主様共有</h5g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5g" name="check5g" aria-describedby="helpId" <?php if(!empty($data->check5g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6g" id="pdf6g"><?php if($data->constract_drawing6g!="") echo $data->constract_drawing6g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6g" name="constract_drawing6g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h6g class="subTitle">施主様共有</h6g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6g" name="check6g" aria-describedby="helpId" <?php if(!empty($data->check6g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7g" id="pdf7g"><?php if($data->constract_drawing7g!="") echo $data->constract_drawing7g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7g" name="constract_drawing7g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h7g class="subTitle">施主様共有</h7g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7g" name="check7g" aria-describedby="helpId" <?php if(!empty($data->check7g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8g" id="pdf8g"><?php if($data->constract_drawing8g!="") echo $data->constract_drawing8g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8g" name="constract_drawing8g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h8g class="subTitle">施主様共有</h8g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8g" name="check8g" aria-describedby="helpId" <?php if(!empty($data->check8g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9g" id="pdf9g"><?php if($data->constract_drawing9g!="") echo $data->constract_drawing9g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9g" name="constract_drawing9g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h9g class="subTitle">施主様共有</h9g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9g" name="check9g" aria-describedby="helpId" <?php if(!empty($data->check9g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10g" id="pdf10g"><?php if($data->constract_drawing10g!="") echo $data->constract_drawing10g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10g" name="constract_drawing10g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h10g class="subTitle">施主様共有</h10g>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10g" name="check10g" aria-describedby="helpId" <?php if(!empty($data->check10g)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10g">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10g?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 1 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingOneBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>確認申請書一式 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluea" id="pdf1bluea"><?php if($data->constract_drawing1bluea!="") echo $data->constract_drawing1bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluea" name="constract_drawing1bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluea" name="check1bluea" aria-describedby="helpId" <?php if(!empty($data->check1bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluea" id="pdf2bluea"><?php if($data->constract_drawing2bluea!="") echo $data->constract_drawing2bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluea" name="constract_drawing2bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluea" name="check2bluea" aria-describedby="helpId" <?php if(!empty($data->check2bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluea" id="pdf3bluea"><?php if($data->constract_drawing3bluea!="") echo $data->constract_drawing3bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluea" name="constract_drawing3bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluea" name="check3bluea" aria-describedby="helpId" <?php if(!empty($data->check3bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluea" id="pdf4bluea"><?php if($data->constract_drawing4bluea!="") echo $data->constract_drawing4bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluea" name="constract_drawing4bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluea" name="check4bluea" aria-describedby="helpId" <?php if(!empty($data->check4bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluea" id="pdf5bluea"><?php if($data->constract_drawing5bluea!="") echo $data->constract_drawing5bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluea" name="constract_drawing5bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluea" name="check5bluea" aria-describedby="helpId" <?php if(!empty($data->check5bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluea" id="pdf6bluea"><?php if($data->constract_drawing6bluea!="") echo $data->constract_drawing6bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluea" name="constract_drawing6bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluea" name="check6bluea" aria-describedby="helpId" <?php if(!empty($data->check6bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluea" id="pdf7bluea"><?php if($data->constract_drawing7bluea!="") echo $data->constract_drawing7bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluea" name="constract_drawing7bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluea" name="check7bluea" aria-describedby="helpId" <?php if(!empty($data->check7bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluea" id="pdf8bluea"><?php if($data->constract_drawing8bluea!="") echo $data->constract_drawing8bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluea" name="constract_drawing8bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluea" name="check8bluea" aria-describedby="helpId" <?php if(!empty($data->check8bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluea" id="pdf9bluea"><?php if($data->constract_drawing9bluea!="") echo $data->constract_drawing9bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluea" name="constract_drawing9bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluea" name="check9bluea" aria-describedby="helpId" <?php if(!empty($data->check9bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluea" id="pdf10bluea"><?php if($data->constract_drawing10bluea!="") echo $data->constract_drawing10bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluea" name="constract_drawing10bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10bluea);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluea" name="check10bluea" aria-describedby="helpId" <?php if(!empty($data->check10bluea)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10bluea">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 2 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingTwoBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>工事工程表 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1blueb" id="pdf1blueb"><?php if($data->constract_drawing1blueb!="") echo $data->constract_drawing1blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1blueb" name="constract_drawing1blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueb" name="check1blueb" aria-describedby="helpId" <?php if(!empty($data->check1blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2blueb" id="pdf2blueb"><?php if($data->constract_drawing2blueb!="") echo $data->constract_drawing2blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2blueb" name="constract_drawing2blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueb" name="check2blueb" aria-describedby="helpId" <?php if(!empty($data->check2blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3blueb" id="pdf3blueb"><?php if($data->constract_drawing3blueb!="") echo $data->constract_drawing3blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3blueb" name="constract_drawing3blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueb" name="check3blueb" aria-describedby="helpId" <?php if(!empty($data->check3blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4blueb" id="pdf4blueb"><?php if($data->constract_drawing4blueb!="") echo $data->constract_drawing4blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4blueb" name="constract_drawing4blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueb" name="check4blueb" aria-describedby="helpId" <?php if(!empty($data->check4blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5blueb" id="pdf5blueb"><?php if($data->constract_drawing5blueb!="") echo $data->constract_drawing5blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5blueb" name="constract_drawing5blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueb" name="check5blueb" aria-describedby="helpId" <?php if(!empty($data->check5blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6blueb" id="pdf6blueb"><?php if($data->constract_drawing6blueb!="") echo $data->constract_drawing6blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6blueb" name="constract_drawing6blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueb" name="check6blueb" aria-describedby="helpId" <?php if(!empty($data->check6blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7blueb" id="pdf7blueb"><?php if($data->constract_drawing7blueb!="") echo $data->constract_drawing7blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7blueb" name="constract_drawing7blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueb" name="check7blueb" aria-describedby="helpId" <?php if(!empty($data->check7blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8blueb" id="pdf8blueb"><?php if($data->constract_drawing8blueb!="") echo $data->constract_drawing8blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8blueb" name="constract_drawing8blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueb" name="check8blueb" aria-describedby="helpId" <?php if(!empty($data->check8blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9blueb" id="pdf9blueb"><?php if($data->constract_drawing9blueb!="") echo $data->constract_drawing9blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9blueb" name="constract_drawing9blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueb" name="check9blueb" aria-describedby="helpId" <?php if(!empty($data->check9blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10blueb" id="pdf10blueb"><?php if($data->constract_drawing10blueb!="") echo $data->constract_drawing10blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10blueb" name="constract_drawing10blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10blueb);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueb" name="check10blueb" aria-describedby="helpId" <?php if(!empty($data->check10blueb)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10blueb">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 3 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingThreeBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>工事着工資料  </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluec" id="pdf1bluec"><?php if($data->constract_drawing1bluec!="") echo $data->constract_drawing1bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluec" name="constract_drawing1bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluec" name="check1bluec" aria-describedby="helpId" <?php if(!empty($data->check1bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluec" id="pdf2bluec"><?php if($data->constract_drawing2bluec!="") echo $data->constract_drawing2bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluec" name="constract_drawing2bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluec" name="check2bluec" aria-describedby="helpId" <?php if(!empty($data->check2bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluec" id="pdf3bluec"><?php if($data->constract_drawing3bluec!="") echo $data->constract_drawing3bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluec" name="constract_drawing3bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluec" name="check3bluec" aria-describedby="helpId" <?php if(!empty($data->check3bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluec" id="pdf4bluec"><?php if($data->constract_drawing4bluec!="") echo $data->constract_drawing4bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluec" name="constract_drawing4bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluec" name="check4bluec" aria-describedby="helpId" <?php if(!empty($data->check4bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluec" id="pdf5bluec"><?php if($data->constract_drawing5bluec!="") echo $data->constract_drawing5bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluec" name="constract_drawing5bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluec" name="check5bluec" aria-describedby="helpId" <?php if(!empty($data->check5bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluec" id="pdf6bluec"><?php if($data->constract_drawing6bluec!="") echo $data->constract_drawing6bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluec" name="constract_drawing6bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluec" name="check6bluec" aria-describedby="helpId" <?php if(!empty($data->check6bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluec" id="pdf7bluec"><?php if($data->constract_drawing7bluec!="") echo $data->constract_drawing7bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluec" name="constract_drawing7bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluec" name="check7bluec" aria-describedby="helpId" <?php if(!empty($data->check7bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluec" id="pdf8bluec"><?php if($data->constract_drawing8bluec!="") echo $data->constract_drawing8bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluec" name="constract_drawing8bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluec" name="check8bluec" aria-describedby="helpId" <?php if(!empty($data->check8bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluec" id="pdf9bluec"><?php if($data->constract_drawing9bluec!="") echo $data->constract_drawing9bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluec" name="constract_drawing9bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluec" name="check9bluec" aria-describedby="helpId" <?php if(!empty($data->check9bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluec" id="pdf10bluec"><?php if($data->constract_drawing10bluec!="") echo $data->constract_drawing10bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluec" name="constract_drawing10bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10bluec);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluec" name="check10bluec" aria-describedby="helpId" <?php if(!empty($data->check10bluec)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10bluec">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 4 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingFourBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>工事着工資料</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1blued" id="pdf1blued"><?php if($data->constract_drawing1blued!="") echo $data->constract_drawing1blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1blued" name="constract_drawing1blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blued" name="check1blued" aria-describedby="helpId" <?php if(!empty($data->check1blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2blued" id="pdf2blued"><?php if($data->constract_drawing2blued!="") echo $data->constract_drawing2blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2blued" name="constract_drawing2blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blued" name="check2blued" aria-describedby="helpId" <?php if(!empty($data->check2blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3blued" id="pdf3blued"><?php if($data->constract_drawing3blued!="") echo $data->constract_drawing3blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3blued" name="constract_drawing3blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blued" name="check3blued" aria-describedby="helpId" <?php if(!empty($data->check3blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4blued" id="pdf4blued"><?php if($data->constract_drawing4blued!="") echo $data->constract_drawing4blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4blued" name="constract_drawing4blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blued" name="check4blued" aria-describedby="helpId" <?php if(!empty($data->check4blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5blued" id="pdf5blued"><?php if($data->constract_drawing5blued!="") echo $data->constract_drawing5blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5blued" name="constract_drawing5blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blued" name="check5blued" aria-describedby="helpId" <?php if(!empty($data->check5blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6blued" id="pdf6blued"><?php if($data->constract_drawing6blued!="") echo $data->constract_drawing6blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6blued" name="constract_drawing6blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blued" name="check6blued" aria-describedby="helpId" <?php if(!empty($data->check6blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7blued" id="pdf7blued"><?php if($data->constract_drawing7blued!="") echo $data->constract_drawing7blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7blued" name="constract_drawing7blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blued" name="check7blued" aria-describedby="helpId" <?php if(!empty($data->check7blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8blued" id="pdf8blued"><?php if($data->constract_drawing8blued!="") echo $data->constract_drawing8blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8blued" name="constract_drawing8blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blued" name="check8blued" aria-describedby="helpId" <?php if(!empty($data->check8blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9blued" id="pdf9blued"><?php if($data->constract_drawing9blued!="") echo $data->constract_drawing9blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9blued" name="constract_drawing9blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blued" name="check9blued" aria-describedby="helpId" <?php if(!empty($data->check9blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10blued" id="pdf10blued"><?php if($data->constract_drawing10blued!="") echo $data->constract_drawing10blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10blued" name="constract_drawing10blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10blued);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blued" name="check10blued" aria-describedby="helpId" <?php if(!empty($data->check10blued)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10blued">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 5 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingFiveBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>変更図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluee" id="pdf1bluee"><?php if($data->constract_drawing1bluee!="") echo $data->constract_drawing1bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluee" name="constract_drawing1bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluee" name="check1bluee" aria-describedby="helpId" <?php if(!empty($data->check1bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluee" id="pdf2bluee"><?php if($data->constract_drawing2bluee!="") echo $data->constract_drawing2bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluee" name="constract_drawing2bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluee" name="check2bluee" aria-describedby="helpId" <?php if(!empty($data->check2bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluee" id="pdf3bluee"><?php if($data->constract_drawing3bluee!="") echo $data->constract_drawing3bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluee" name="constract_drawing3bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluee" name="check3bluee" aria-describedby="helpId" <?php if(!empty($data->check3bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluee" id="pdf4bluee"><?php if($data->constract_drawing4bluee!="") echo $data->constract_drawing4bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluee" name="constract_drawing4bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluee" name="check4bluee" aria-describedby="helpId" <?php if(!empty($data->check4bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluee" id="pdf5bluee"><?php if($data->constract_drawing5bluee!="") echo $data->constract_drawing5bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluee" name="constract_drawing5bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluee" name="check5bluee" aria-describedby="helpId" <?php if(!empty($data->check5bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluee" id="pdf6bluee"><?php if($data->constract_drawing6bluee!="") echo $data->constract_drawing6bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluee" name="constract_drawing6bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluee" name="check6bluee" aria-describedby="helpId" <?php if(!empty($data->check6bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluee" id="pdf7bluee"><?php if($data->constract_drawing7bluee!="") echo $data->constract_drawing7bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluee" name="constract_drawing7bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluee" name="check7bluee" aria-describedby="helpId" <?php if(!empty($data->check7bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluee" id="pdf8bluee"><?php if($data->constract_drawing8bluee!="") echo $data->constract_drawing8bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluee" name="constract_drawing8bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluee" name="check8bluee" aria-describedby="helpId" <?php if(!empty($data->check8bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluee" id="pdf9bluee"><?php if($data->constract_drawing9bluee!="") echo $data->constract_drawing9bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluee" name="constract_drawing9bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluee" name="check9bluee" aria-describedby="helpId" <?php if(!empty($data->check9bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluee" id="pdf10bluee"><?php if($data->constract_drawing10bluee!="") echo $data->constract_drawing10bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluee" name="constract_drawing10bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10bluee);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluee" name="check10bluee" aria-describedby="helpId" <?php if(!empty($data->check10bluee)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10bluee">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 6 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingSixBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>設備プランシート</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluef" id="pdf1bluef"><?php if($data->constract_drawing1bluef!="") echo $data->constract_drawing1bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluef" name="constract_drawing1bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner1bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluef" name="check1bluef" aria-describedby="helpId" <?php if(!empty($data->check1bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check1bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item1bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluef" id="pdf2bluef"><?php if($data->constract_drawing2bluef!="") echo $data->constract_drawing2bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluef" name="constract_drawing2bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner2bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluef" name="check2bluef" aria-describedby="helpId" <?php if(!empty($data->check2bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check2bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item2bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluef" id="pdf3bluef"><?php if($data->constract_drawing3bluef!="") echo $data->constract_drawing3bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluef" name="constract_drawing3bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner3bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluef" name="check3bluef" aria-describedby="helpId" <?php if(!empty($data->check3bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check3bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item3bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluef" id="pdf4bluef"><?php if($data->constract_drawing4bluef!="") echo $data->constract_drawing4bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluef" name="constract_drawing4bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner4bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluef" name="check4bluef" aria-describedby="helpId" <?php if(!empty($data->check4bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check4bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item4bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluef" id="pdf5bluef"><?php if($data->constract_drawing5bluef!="") echo $data->constract_drawing5bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluef" name="constract_drawing5bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner5bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluef" name="check5bluef" aria-describedby="helpId" <?php if(!empty($data->check5bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check5bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item5bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluef" id="pdf6bluef"><?php if($data->constract_drawing6bluef!="") echo $data->constract_drawing6bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluef" name="constract_drawing6bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner6bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluef" name="check6bluef" aria-describedby="helpId" <?php if(!empty($data->check6bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check6bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item6bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluef" id="pdf7bluef"><?php if($data->constract_drawing7bluef!="") echo $data->constract_drawing7bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluef" name="constract_drawing7bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner7bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluef" name="check7bluef" aria-describedby="helpId" <?php if(!empty($data->check7bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check7bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item7bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluef" id="pdf8bluef"><?php if($data->constract_drawing8bluef!="") echo $data->constract_drawing8bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluef" name="constract_drawing8bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner8bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluef" name="check8bluef" aria-describedby="helpId" <?php if(!empty($data->check8bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check8bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item8bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluef" id="pdf9bluef"><?php if($data->constract_drawing9bluef!="") echo $data->constract_drawing9bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluef" name="constract_drawing9bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner9bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluef" name="check9bluef" aria-describedby="helpId" <?php if(!empty($data->check9bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check9bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item9bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluef" id="pdf10bluef"><?php if($data->constract_drawing10bluef!="") echo $data->constract_drawing10bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluef" name="constract_drawing10bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <div class="addition">
                                                <button type="button">追加</button>
                                              </div>
                                              <div class="delete">
                                                <button type="button">削除</button>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php
                                              $selected_array = explode(';',$data->co_owner10bluef);
                                              foreach($affiliate as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluef" name="check10bluef" aria-describedby="helpId" <?php if(!empty($data->check10bluef)) { echo 'checked';}?>
                                            <label class="form-check-label" for="check10bluef">共有する</label>

                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId" placeholder="" value="<?=$data->desc_item10bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                          </div>
                        </div>
                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->

                        <button name="" id="" class="btn hvr-sink" type="submit">追加</button>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>

  $(document).ready(function(){
  
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
          $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
        }
    });

  });

  $("#cust_code").keydown(function(e) {
    var EnterKeyPressed = verifyKeyPressed(e, 13);
    if (EnterKeyPressed === true) {
        //here is where i want to disable the text box
        // var that = this;
        // that.disabled = true;
        
        // $('#cust_name').fadeOut(500, function() {
        //     //i want to re enable it here
        //     that.disabled = false;    
        //   });
        document.getElementById('cust_name').disabled = true; 
        
      }
  });

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });

  $('select').selectpicker();

  // $(document).ready(function() {
  //     $('.selectpicker').select2();
  // });

  $('#contract').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files').innerText = cleanFileName;
});

  // $('#contract').on('change',function(){
  //     var fileName = $(this).val();
  //     var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
  //     $(this).next('#pdf1').html(cleanFileName);
  // });


  $('#constract_drawing1').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1').innerText = cleanFileName;
});
$('#constract_drawing2').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2').innerText = cleanFileName;
});
$('#constract_drawing3').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3').innerText = cleanFileName;
});
$('#constract_drawing4').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4').innerText = cleanFileName;
});
$('#constract_drawing5').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5').innerText = cleanFileName;
});
$('#constract_drawing6').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6').innerText = cleanFileName;
});
$('#constract_drawing7').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7').innerText = cleanFileName;
});
$('#constract_drawing8').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8').innerText = cleanFileName;
});
$('#constract_drawing9').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9').innerText = cleanFileName;
});
$('#constract_drawing10').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10').innerText = cleanFileName;
});

$('#constract_drawing1b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1b').innerText = cleanFileName;
});
$('#constract_drawing2b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2b').innerText = cleanFileName;
});
$('#constract_drawing3b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3b').innerText = cleanFileName;
});
$('#constract_drawing4b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4b').innerText = cleanFileName;
});
$('#constract_drawing5b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5b').innerText = cleanFileName;
});
$('#constract_drawing6b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6b').innerText = cleanFileName;
});
$('#constract_drawing7b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7b').innerText = cleanFileName;
});
$('#constract_drawing8b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8b').innerText = cleanFileName;
});
$('#constract_drawing9b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9b').innerText = cleanFileName;
});
$('#constract_drawing10b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10b').innerText = cleanFileName;
});

$('#constract_drawing1c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1c').innerText = cleanFileName;
});
$('#constract_drawing2c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2c').innerText = cleanFileName;
});
$('#constract_drawing3c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3c').innerText = cleanFileName;
});
$('#constract_drawing4c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4c').innerText = cleanFileName;
});
$('#constract_drawing5c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5c').innerText = cleanFileName;
});
$('#constract_drawing6c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6c').innerText = cleanFileName;
});
$('#constract_drawing7c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7c').innerText = cleanFileName;
});
$('#constract_drawing8c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8c').innerText = cleanFileName;
});
$('#constract_drawing9c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9c').innerText = cleanFileName;
});
$('#constract_drawing10c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10c').innerText = cleanFileName;
});

$('#constract_drawing1d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1d').innerText = cleanFileName;
});
$('#constract_drawing2d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2d').innerText = cleanFileName;
});
$('#constract_drawing3d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3d').innerText = cleanFileName;
});
$('#constract_drawing4d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4d').innerText = cleanFileName;
});
$('#constract_drawing5d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5d').innerText = cleanFileName;
});
$('#constract_drawing6d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6d').innerText = cleanFileName;
});
$('#constract_drawing7d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7d').innerText = cleanFileName;
});
$('#constract_drawing8d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8d').innerText = cleanFileName;
});
$('#constract_drawing9d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9d').innerText = cleanFileName;
});
$('#constract_drawing10d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10d').innerText = cleanFileName;
});

$('#constract_drawing1e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1e').innerText = cleanFileName;
});
$('#constract_drawing2e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2e').innerText = cleanFileName;
});
$('#constract_drawing3e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3e').innerText = cleanFileName;
});
$('#constract_drawing4e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4e').innerText = cleanFileName;
});
$('#constract_drawing5e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5e').innerText = cleanFileName;
});
$('#constract_drawing6e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6e').innerText = cleanFileName;
});
$('#constract_drawing7e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7e').innerText = cleanFileName;
});
$('#constract_drawing8e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8e').innerText = cleanFileName;
});
$('#constract_drawing9e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9e').innerText = cleanFileName;
});
$('#constract_drawing10e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10e').innerText = cleanFileName;
});

$('#constract_drawing1f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1f').innerText = cleanFileName;
});
$('#constract_drawing2f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2f').innerText = cleanFileName;
});
$('#constract_drawing3f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3f').innerText = cleanFileName;
});
$('#constract_drawing4f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4f').innerText = cleanFileName;
});
$('#constract_drawing5f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5f').innerText = cleanFileName;
});
$('#constract_drawing6f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6f').innerText = cleanFileName;
});
$('#constract_drawing7f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7f').innerText = cleanFileName;
});
$('#constract_drawing8f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8f').innerText = cleanFileName;
});
$('#constract_drawing9f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9f').innerText = cleanFileName;
});
$('#constract_drawing10f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10f').innerText = cleanFileName;
});

$('#constract_drawing1g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1g').innerText = cleanFileName;
});
$('#constract_drawing2g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2g').innerText = cleanFileName;
});
$('#constract_drawing3g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3g').innerText = cleanFileName;
});
$('#constract_drawing4g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4g').innerText = cleanFileName;
});
$('#constract_drawing5g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5g').innerText = cleanFileName;
});
$('#constract_drawing6g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6g').innerText = cleanFileName;
});
$('#constract_drawing7g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7g').innerText = cleanFileName;
});
$('#constract_drawing8g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8g').innerText = cleanFileName;
});
$('#constract_drawing9g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9g').innerText = cleanFileName;
});
$('#constract_drawing10g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10g').innerText = cleanFileName;
});

$('#constract_drawing1bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1bluea').innerText = cleanFileName;
});
$('#constract_drawing2bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2bluea').innerText = cleanFileName;
});
$('#constract_drawing3bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3bluea').innerText = cleanFileName;
});
$('#constract_drawing4bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4bluea').innerText = cleanFileName;
});
$('#constract_drawing5bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5bluea').innerText = cleanFileName;
});
$('#constract_drawing6bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6bluea').innerText = cleanFileName;
});
$('#constract_drawing7bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7bluea').innerText = cleanFileName;
});
$('#constract_drawing8bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8bluea').innerText = cleanFileName;
});
$('#constract_drawing9bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9bluea').innerText = cleanFileName;
});
$('#constract_drawing10bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10bluea').innerText = cleanFileName;
});

$('#constract_drawing1blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1blueb').innerText = cleanFileName;
});
$('#constract_drawing2blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2blueb').innerText = cleanFileName;
});
$('#constract_drawing3blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3blueb').innerText = cleanFileName;
});
$('#constract_drawing4blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4blueb').innerText = cleanFileName;
});
$('#constract_drawing5blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5blueb').innerText = cleanFileName;
});
$('#constract_drawing6blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6blueb').innerText = cleanFileName;
});
$('#constract_drawing7blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7blueb').innerText = cleanFileName;
});
$('#constract_drawing8blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8blueb').innerText = cleanFileName;
});
$('#constract_drawing9blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9blueb').innerText = cleanFileName;
});
$('#constract_drawing10blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10blueb').innerText = cleanFileName;
});

$('#constract_drawing1bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1bluec').innerText = cleanFileName;
});
$('#constract_drawing2bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2bluec').innerText = cleanFileName;
});
$('#constract_drawing3bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3bluec').innerText = cleanFileName;
});
$('#constract_drawing4bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4bluec').innerText = cleanFileName;
});
$('#constract_drawing5bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5bluec').innerText = cleanFileName;
});
$('#constract_drawing6bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6bluec').innerText = cleanFileName;
});
$('#constract_drawing7bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7bluec').innerText = cleanFileName;
});
$('#constract_drawing8bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8bluec').innerText = cleanFileName;
});
$('#constract_drawing9bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9bluec').innerText = cleanFileName;
});
$('#constract_drawing10bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10bluec').innerText = cleanFileName;
});

$('#constract_drawing1blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1blued').innerText = cleanFileName;
});
$('#constract_drawing2blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2blued').innerText = cleanFileName;
});
$('#constract_drawing3blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3blued').innerText = cleanFileName;
});
$('#constract_drawing4blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4blued').innerText = cleanFileName;
});
$('#constract_drawing5blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5blued').innerText = cleanFileName;
});
$('#constract_drawing6blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6blued').innerText = cleanFileName;
});
$('#constract_drawing7blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7blued').innerText = cleanFileName;
});
$('#constract_drawing8blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8blued').innerText = cleanFileName;
});
$('#constract_drawing9blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9blued').innerText = cleanFileName;
});
$('#constract_drawing10blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10blued').innerText = cleanFileName;
});

$('#constract_drawing1bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1bluee').innerText = cleanFileName;
});
$('#constract_drawing2bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2bluee').innerText = cleanFileName;
});
$('#constract_drawing3bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3bluee').innerText = cleanFileName;
});
$('#constract_drawing4bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4bluee').innerText = cleanFileName;
});
$('#constract_drawing5bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5bluee').innerText = cleanFileName;
});
$('#constract_drawing6bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6bluee').innerText = cleanFileName;
});
$('#constract_drawing7bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7bluee').innerText = cleanFileName;
});
$('#constract_drawing8bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8bluee').innerText = cleanFileName;
});
$('#constract_drawing9bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9bluee').innerText = cleanFileName;
});
$('#constract_drawing10bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10bluee').innerText = cleanFileName;
});

$('#constract_drawing1bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf1bluef').innerText = cleanFileName;
});
$('#constract_drawing2bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf2bluef').innerText = cleanFileName;
});
$('#constract_drawing3bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf3bluef').innerText = cleanFileName;
});
$('#constract_drawing4bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf4bluef').innerText = cleanFileName;
});
$('#constract_drawing5bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf5bluef').innerText = cleanFileName;
});
$('#constract_drawing6bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf6bluef').innerText = cleanFileName;
});
$('#constract_drawing7bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf7bluef').innerText = cleanFileName;
});
$('#constract_drawing8bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf8bluef').innerText = cleanFileName;
});
$('#constract_drawing9bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf9bluef').innerText = cleanFileName;
});
$('#constract_drawing10bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf10bluef').innerText = cleanFileName;
});


    // function handler(e){
    //   var input = document.getElementById('input_delivery_date').value;
    //   var input2 = document.getElementById('input_delivery_date').value;
    //   var input3 = document.getElementById('input_delivery_date').value;
    //   var input4 = document.getElementById('input_delivery_date').value;
    //   var input5 = document.getElementById('input_delivery_date').value;
    //   var input6 = document.getElementById('input_delivery_date').value;
    //   var input7 = document.getElementById('input_delivery_date').value;

    //   var manDate = moment(input).local();
    //   var manDate2 = moment(input2).local();
    //   var manDate3 = moment(input3).local();
    //   var manDate4 = moment(input4).local();
    //   var manDate5 = moment(input5).local();
    //   var manDate6 = moment(input6).local();
    //   var manDate7 = moment(input7).local();

    //   document.getElementById('inspection_time1').value = manDate.add(3, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time2').value = manDate2.add(6, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time3').value = manDate3.add(1, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time4').value = manDate4.add(2, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time5').value = manDate5.add(3, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time6').value = manDate6.add(5, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time7').value = manDate7.add(10, 'y').format('YYYY-MM-DD'); 
    // }
    $(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
</script>
<script>

$(document).ready(function() {
    $('#btn-example-file-reset1').on('click', function(e) {
        var $el = $('#contract');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1').innerText = "ファイルを選択";
    });
});

$(document).ready(function() {
    $('#btn-example-file-reset2').on('click', function(e) {
        var $el = $('#constract_drawing1');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset3').on('click', function(e) {
        var $el = $('#constract_drawing2');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset4').on('click', function(e) {
        var $el = $('#constract_drawing3');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset5').on('click', function(e) {
        var $el = $('#constract_drawing4');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset6').on('click', function(e) {
        var $el = $('#constract_drawing5');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset7').on('click', function(e) {
        var $el = $('#constract_drawing6');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset8').on('click', function(e) {
        var $el = $('#constract_drawing7');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset9').on('click', function(e) {
        var $el = $('#constract_drawing8');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9').innerText = "ファイルを選択";
    });
});
$(document).ready(function() {
    $('#btn-example-file-reset10').on('click', function(e) {
        var $el = $('#constract_drawing9');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10').innerText = "ファイルを選択";
    });
});


  document.getElementById('btn-example-file-replace1').addEventListener('click', openDialog);
  function openDialog() {
      document.getElementById('fileid').click();
  }

  document.getElementById('btn-example-file-replace2').addEventListener('click', openDialog1);
  function openDialog1() {
      document.getElementById('fileid1').click();
  }

  document.getElementById('btn-example-file-replace3').addEventListener('click', openDialog2);
  function openDialog2() {
      document.getElementById('fileid2').click();
  }

  document.getElementById('btn-example-file-replace4').addEventListener('click', openDialog3);
  function openDialog3() {
      document.getElementById('fileid3').click();
  }

  document.getElementById('btn-example-file-replace5').addEventListener('click', openDialog4);
  function openDialog4() {
      document.getElementById('fileid4').click();
  }

  document.getElementById('btn-example-file-replace6').addEventListener('click', openDialog5);
  function openDialog5() {
      document.getElementById('fileid5').click();
  }

  document.getElementById('btn-example-file-replace7').addEventListener('click', openDialog6);
  function openDialog6() {
      document.getElementById('fileid6').click();
  }

  document.getElementById('btn-example-file-replace8').addEventListener('click', openDialog7);
  function openDialog7() {
      document.getElementById('fileid7').click();
  }

  document.getElementById('btn-example-file-replace9').addEventListener('click', openDialog8);
  function openDialog8() {
      document.getElementById('fileid8').click();
  }

  document.getElementById('btn-example-file-replace10').addEventListener('click', openDialog9);
  function openDialog9() {
      document.getElementById('fileid9').click();
  }
</script>
</body>
</html>