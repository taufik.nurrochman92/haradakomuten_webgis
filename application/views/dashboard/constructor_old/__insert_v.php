<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_data" method="post" enctype="multipart/form-data">
                        
                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds">
                                <option selected disabled value="">選択してください</option>
                                <option value="SOB">SWEET HOME</option>
                                <option value="ビリーブの家">ビリーブの家</option>
                                <option value="リフォーム工事">リフォーム工事</option>
                                <option value="メンテナンス工事">メンテナンス工事</option>
                                <option value="新築工事">新築工事</option>
                                <option value="OB">OB</option>
                                <option value="その他">その他</option>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No</label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-12" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"></textarea>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <select class="form-control" name="sales_staff">
                                <option value="">選択してください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <select class="form-control" name="incharge_construction">
                                <option value="">選択してください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <select class="form-control" name="incharge_coordinator">
                                <option value="">選択してください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label class="label-title" for="contract">契約書</label>
                            <div class="input-group">
                                <h4 class="subTitle">書類</h4>
                                <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="contract" name="contract" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                  <label class="custom-file-label" for="pdf1" id="pdf1">ファイル選択</label>
                                </div>
                                <p id="files">ファイルが選択されていません。</p>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing1" class="label-title">契約図面 1</label>
                            <div class="flex-content">
                              <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                  </div>
                                  <p id="files1">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner1">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner1[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check1" name="check1" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check1">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item1">コメント</label>
                                      <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing2" class="label-title">契約図面 2</label>

                            <div class="flex-content">
                              <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                  </div>
                                  <p id="files2">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner2">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner2[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check2" name="check2" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check2">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item2">コメント</label>
                                      <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing3" class="label-title">契約図面 3</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf3" id="pdf3">ファイルを選択</label>
                                  </div>
                                  <p id="files3">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner3">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner3[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check3" name="check3" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check3">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item3">コメント</label>
                                      <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing4" class="label-title">契約図面 4</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf4" id="pdf4">ファイルを選択</label>
                                  </div>
                                  <p id="files4">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner4">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner4[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check4" name="check4" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check4">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item4">コメント</label>
                                      <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing5" class="label-title">契約図面 5</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf5" id="pdf5">ファイルを選択</label>
                                  </div>
                                  <p id="files5">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner5">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner5[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check5" name="check5" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check5">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item5">コメント</label>
                                      <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing6" class="label-title">契約図面 6</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf6" id="pdf6">ファイルを選択</label>
                                  </div>
                                  <p id="files6">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner6">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner6[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check6" name="check6" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check6">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item6">コメント</label>
                                      <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing7" class="label-title">契約図面 7</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf7" id="pdf7">ファイルを選択</label>
                                  </div>
                                  <p id="files7">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner7">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner7[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check7" name="check7" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check7">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item7">コメント</label>
                                      <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing8" class="label-title">契約図面 8</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf8" id="pdf8">ファイルを選択</label>
                                  </div>
                                  <p id="files8">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner8">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner8[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check8" name="check8" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check8">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item8">コメント</label>
                                      <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 col-md-12" style="padding: 0;margin-bottom:0;border-bottom: 1px solid #CED4DA;">
                          <div class="form-group flex">
                            <label for="constract_drawing9" class="label-title">契約図面 9</label>
                            <div class="flex-content">
                            <div class="input-group">
                                  <h4 class="subTitle">書類</h4>
                                  <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                      <label class="custom-file-label" for="pdf9" id="pdf9">ファイルを選択</label>
                                  </div>
                                  <p id="files9">ファイルが選択されていません。</p>
                              </div>
                              <div class="second-input">
                                <div class="dropdown-content">
                                  <label class="subTitle" for="co_owner9">共有業者</label>
                                  <select class="form-control selectpicker" name="co_owner9[]" multiple data-live-search="true" data-none-selected-text="選択してください">
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="checkbox-content">
                                  <h4 class="subTitle">施主様共有</h4>
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="check9" name="check9" aria-describedby="helpId" />
                                    <label class="form-check-label" for="check9">共有する</label>
                                  </div>
                                </div>
                              </div>
                              <div class="third-input">
                                  <div class="form-group">
                                      <label for="desc_item9">コメント</label>
                                      <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->
                        <br><br>
                        <button name="" id="" class="btn custom-btn hvr-sink" type="submit">登録する</button>
                        <br><br>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>

  $(document).ready(function(){
    
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
            $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
        }
    });

  });

  $("#cust_code").keydown(function(e) {
    var EnterKeyPressed = verifyKeyPressed(e, 13);
    if (EnterKeyPressed === true) {
        //here is where i want to disable the text box
        // var that = this;
        // that.disabled = true;
        
        // $('#cust_name').fadeOut(500, function() {
        //     //i want to re enable it here
        //     that.disabled = false;    
        //   });
        document.getElementById('cust_name').disabled = true; 
        
      }
  });

  function verifyKeyPressed(e, key) {
      if (e && ((e.which && e.which == key) || (e.keyCode && e.keyCode == key))) {
          return true;
      }
      else {
          return false;
      }
  }
  

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });

  $('select').selectpicker();

  // $(document).ready(function() {
  //     $('.selectpicker').select2();
  // });

$('#contract').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files').innerText = cleanFileName;
});

$('#constract_drawing1').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files1').innerText = cleanFileName;
});

$('#constract_drawing2').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files2').innerText = cleanFileName;
});

$('#constract_drawing3').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files3').innerText = cleanFileName;
});

$('#constract_drawing4').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files4').innerText = cleanFileName;
});

$('#constract_drawing5').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files5').innerText = cleanFileName;
});

$('#constract_drawing6').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files6').innerText = cleanFileName;
});

$('#constract_drawing7').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files7').innerText = cleanFileName;
});

$('#constract_drawing8').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files8').innerText = cleanFileName;
});

$('#constract_drawing9').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('files9').innerText = cleanFileName;
});

    // function handler(e){
    //   var input = document.getElementById('input_delivery_date').value;
    //   var input2 = document.getElementById('input_delivery_date').value;
    //   var input3 = document.getElementById('input_delivery_date').value;
    //   var input4 = document.getElementById('input_delivery_date').value;
    //   var input5 = document.getElementById('input_delivery_date').value;
    //   var input6 = document.getElementById('input_delivery_date').value;
    //   var input7 = document.getElementById('input_delivery_date').value;

    //   var manDate = moment(input).local();
    //   var manDate2 = moment(input2).local();
    //   var manDate3 = moment(input3).local();
    //   var manDate4 = moment(input4).local();
    //   var manDate5 = moment(input5).local();
    //   var manDate6 = moment(input6).local();
    //   var manDate7 = moment(input7).local();

    //   document.getElementById('inspection_time1').value = manDate.add(3, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time2').value = manDate2.add(6, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time3').value = manDate3.add(1, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time4').value = manDate4.add(2, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time5').value = manDate5.add(3, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time6').value = manDate6.add(5, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time7').value = manDate7.add(10, 'y').format('YYYY-MM-DD'); 
    // }

    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });
</script>
</body>
</html>