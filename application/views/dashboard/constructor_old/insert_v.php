<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事登録</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事登録</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_data" method="post" enctype="multipart/form-data">
                        
                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds">
                                <option selected disabled value="">ご選択ください</option>
                                <option value="SOB">Sweet Home</option>
                                <option value="BOB">ビリーブの家</option>
                                <option value="小工事">小工事</option>
                                <option value="建設中">建設中</option>
                                <option value="OB">OB</option>
                                <option value="材料販売">材料販売</option>
                                <option value="その他">その他</option>
                                <option value="NL">NL</option>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No</label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-12" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"></textarea>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <select class="form-control" name="sales_staff">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <select class="form-control" name="incharge_construction">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <select class="form-control" name="incharge_coordinator">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:0;">
                          <div id="accordion" class="customAccordion">
                            
                            <!-- CATEGORY 1 -->
                            <div class="card">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>請負契約書・土地契約書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1" name="check1" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2" name="check2" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3" name="check3" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4" name="check4" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5" name="check5" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6" name="check6" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7" name="check7" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8" name="check8" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9" name="check9" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10" name="constract_drawing10" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10" name="check10" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 2 -->
                            <div class="card">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>長期優良住宅認定書・性能評価書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1b" name="constract_drawing1b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1b" name="check1b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2b" name="constract_drawing2b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2b" name="check2b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3b" name="constract_drawing3b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3b" name="check3b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4b" name="constract_drawing4b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4b" name="check4b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5b" name="constract_drawing5b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5b" name="check5b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6b" name="constract_drawing6b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6b" name="check6b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7b" name="constract_drawing7b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7b" name="check7b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8b" name="constract_drawing8b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8b" name="check8b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9b" name="constract_drawing9b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9b" name="check9b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10b" name="constract_drawing10b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10b">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10b" name="check10b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10b">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 3 -->
                            <div class="card">
                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>地盤調査・地盤改良</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1c" name="constract_drawing1c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1c" name="check1c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2c" name="constract_drawing2c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2c" name="check2c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3c" name="constract_drawing3c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3c" name="check3c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4c" name="constract_drawing4c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4c" name="check4c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5c" name="constract_drawing5c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5c" name="check5c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6c" name="constract_drawing6c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6c" name="check6c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7c" name="constract_drawing7c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7c" name="check7c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8c" name="constract_drawing8c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8c" name="check8c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9c" name="constract_drawing9c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9c" name="check9c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10c" name="constract_drawing10c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10c">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10c" name="check10c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10c">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4 --> 
                            <div class="card">
                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>点検履歴</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1d" name="constract_drawing1d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1d" name="check1d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2d" name="constract_drawing2d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2d" name="check2d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3d" name="constract_drawing3d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3d" name="check3d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4d" name="constract_drawing4d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4d" name="check4d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5d" name="constract_drawing5d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5d" name="check5d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6d" name="constract_drawing6d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6d" name="check6d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7d" name="constract_drawing7d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7d" name="check7d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8d" name="constract_drawing8d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8d" name="check8d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9d" name="constract_drawing9d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9d" name="check9d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10d" name="constract_drawing10d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10d">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10d" name="check10d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10d">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5 -->
                            <div class="card">
                              <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>三者引継ぎ合意書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1e" name="constract_drawing1e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1e" name="check1e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2e" name="constract_drawing2e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2e" name="check2e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3e" name="constract_drawing3e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3e" name="check3e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4e" name="constract_drawing4e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4e" name="check4e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5e" name="constract_drawing5e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5e" name="check5e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6e" name="constract_drawing6e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6e" name="check6e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7e" name="constract_drawing7e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7e" name="check7e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8e" name="constract_drawing8e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8e" name="check8e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9e" name="constract_drawing9e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9e" name="check9e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10e" name="constract_drawing10e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10e">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10e" name="check10e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10e">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6 -->
                            <div class="card">
                              <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>住宅仕様書・プレカット資料 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1f" name="constract_drawing1f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1f" name="check1f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2f" name="constract_drawing2f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2f" name="check2f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3f" name="constract_drawing3f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3f" name="check3f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4f" name="constract_drawing4f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4f" name="check4f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5f" name="constract_drawing5f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5f" name="check5f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6f" name="constract_drawing6f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6f" name="check6f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7f" name="constract_drawing7f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7f" name="check7f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8f" name="constract_drawing8f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8f" name="check8f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9f" name="constract_drawing9f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9f" name="check9f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10f" name="constract_drawing10f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10f">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10f" name="check10f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10f">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 7 -->
                            <div class="card">
                              <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>工事写真 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column7">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1g" name="constract_drawing1g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1g" name="check1g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2g" name="constract_drawing2g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2g" name="check2g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3g" name="constract_drawing3g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3g" name="check3g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4g" name="constract_drawing4g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4g" name="check4g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5g" name="constract_drawing5g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5g" name="check5g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6g" name="constract_drawing6g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6g" name="check6g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7g" name="constract_drawing7g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7g" name="check7g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8g" name="constract_drawing8g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8g" name="check8g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9g" name="constract_drawing9g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9g" name="check9g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10g" name="constract_drawing10g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10g">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10g" name="check10g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10g">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 1BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingOneBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>確認申請書一式 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluea" name="constract_drawing1bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluea" name="check1bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluea" name="constract_drawing2bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluea" name="check2bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluea" name="constract_drawing3bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluea" name="check3bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluea" name="constract_drawing4bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluea" name="check4bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluea" name="constract_drawing5bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluea" name="check5bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluea" name="constract_drawing6bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluea" name="check6bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluea" name="constract_drawing7bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluea" name="check7bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluea" name="constract_drawing8bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluea" name="check8bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluea" name="constract_drawing9bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluea" name="check9bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluea" name="constract_drawing10bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluea">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluea" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluea" name="check10bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluea">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 2BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingTwoBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>工事工程表 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1blueb" name="constract_drawing1blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueb" name="check1blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2blueb" name="constract_drawing2blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueb" name="check2blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3blueb" name="constract_drawing3blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueb" name="check3blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4blueb" name="constract_drawing4blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueb" name="check4blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5blueb" name="constract_drawing5blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueb" name="check5blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6blueb" name="constract_drawing6blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueb" name="check6blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7blueb" name="constract_drawing7blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueb" name="check7blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8blueb" name="constract_drawing8blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueb" name="check8blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9blueb" name="constract_drawing9blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueb" name="check9blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10blueb" name="constract_drawing10blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10blueb">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10blueb" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueb" name="check10blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blueb">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 3BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingThreeBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>工事着工資料  </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluec" name="constract_drawing1bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluec" name="check1bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluec" name="constract_drawing2bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluec" name="check2bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluec" name="constract_drawing3bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluec" name="check3bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluec" name="constract_drawing4bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluec" name="check4bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluec" name="constract_drawing5bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluec" name="check5bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluec" name="constract_drawing6bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluec" name="check6bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluec" name="constract_drawing7bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluec" name="check7bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluec" name="constract_drawing8bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluec" name="check8bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluec" name="constract_drawing9bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluec" name="check9bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluec" name="constract_drawing10bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluec">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluec" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluec" name="check10bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluec">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingFourBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>工事着工資料</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1blued" name="constract_drawing1blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blued" name="check1blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2blued" name="constract_drawing2blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blued" name="check2blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3blued" name="constract_drawing3blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blued" name="check3blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4blued" name="constract_drawing4blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blued" name="check4blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5blued" name="constract_drawing5blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blued" name="check5blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6blued" name="constract_drawing6blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blued" name="check6blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7blued" name="constract_drawing7blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blued" name="check7blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8blued" name="constract_drawing8blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blued" name="check8blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9blued" name="constract_drawing9blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blued" name="check9blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10blued" name="constract_drawing10blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10blued">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blued">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10blued" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blued" name="check10blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blued">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingFiveBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>変更図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluee" name="constract_drawing1bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluee" name="check1bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluee" name="constract_drawing2bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluee" name="check2bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluee" name="constract_drawing3bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluee" name="check3bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluee" name="constract_drawing4bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluee" name="check4bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluee" name="constract_drawing5bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluee" name="check5bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluee" name="constract_drawing6bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluee" name="check6bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluee" name="constract_drawing7bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluee" name="check7bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluee" name="constract_drawing8bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluee" name="check8bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluee" name="constract_drawing9bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluee" name="check9bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluee" name="constract_drawing10bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluee">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluee" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluee" name="check10bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluee">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6BLUE -->
                            <div class="card blueCard">
                              <div class="card-header" id="headingSixBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>設備プランシート</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluef" name="constract_drawing1bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner1bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluef" name="check1bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluef" name="constract_drawing2bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner2bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluef" name="check2bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluef" name="constract_drawing3bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner3bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluef" name="check3bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluef" name="constract_drawing4bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner4bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluef" name="check4bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluef" name="constract_drawing5bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner5bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluef" name="check5bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluef" name="constract_drawing6bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner6bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluef" name="check6bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluef" name="constract_drawing7bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner7bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluef" name="check7bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluef" name="constract_drawing8bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner8bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluef" name="check8bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluef" name="constract_drawing9bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner9bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluef" name="check9bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluef" name="constract_drawing10bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluef">ファイルが選択されていません。</p>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                          <select class="form-control selectpicker" name="co_owner10bluef" multiple data-live-search="true" data-none-selected-text="ご選択ください">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluef" name="check10bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluef">共有する</label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>

                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->
                        <br><br>
                        <button name="" id="" class="btn custom-btn hvr-sink" type="submit">登録する</button>
                        <br><br>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>

  $(document).ready(function(){
    
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
            $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
        }
    });

  });

  $("#cust_code").keydown(function(e) {
    var EnterKeyPressed = verifyKeyPressed(e, 13);
    if (EnterKeyPressed === true) {
        //here is where i want to disable the text box
        // var that = this;
        // that.disabled = true;
        
        // $('#cust_name').fadeOut(500, function() {
        //     //i want to re enable it here
        //     that.disabled = false;    
        //   });
        document.getElementById('cust_name').disabled = true; 
        
      }
  });
  

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });

  $('select').selectpicker();

  // $(document).ready(function() {
  //     $('.selectpicker').select2();
  // });

$('#constract_drawing1').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1').innerText = cleanFileName;
});
$('#constract_drawing2').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2').innerText = cleanFileName;
});
$('#constract_drawing3').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3').innerText = cleanFileName;
});
$('#constract_drawing4').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4').innerText = cleanFileName;
});
$('#constract_drawing5').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5').innerText = cleanFileName;
});
$('#constract_drawing6').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6').innerText = cleanFileName;
});
$('#constract_drawing7').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7').innerText = cleanFileName;
});
$('#constract_drawing8').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8').innerText = cleanFileName;
});
$('#constract_drawing9').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9').innerText = cleanFileName;
});
$('#constract_drawing10').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10').innerText = cleanFileName;
});

$('#constract_drawing1b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1b').innerText = cleanFileName;
});
$('#constract_drawing2b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2b').innerText = cleanFileName;
});
$('#constract_drawing3b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3b').innerText = cleanFileName;
});
$('#constract_drawing4b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4b').innerText = cleanFileName;
});
$('#constract_drawing5b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5b').innerText = cleanFileName;
});
$('#constract_drawing6b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6b').innerText = cleanFileName;
});
$('#constract_drawing7b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7b').innerText = cleanFileName;
});
$('#constract_drawing8b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8b').innerText = cleanFileName;
});
$('#constract_drawing9b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9b').innerText = cleanFileName;
});
$('#constract_drawing10b').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10b').innerText = cleanFileName;
});

$('#constract_drawing1c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1c').innerText = cleanFileName;
});
$('#constract_drawing2c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2c').innerText = cleanFileName;
});
$('#constract_drawing3c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3c').innerText = cleanFileName;
});
$('#constract_drawing4c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4c').innerText = cleanFileName;
});
$('#constract_drawing5c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5c').innerText = cleanFileName;
});
$('#constract_drawing6c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6c').innerText = cleanFileName;
});
$('#constract_drawing7c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7c').innerText = cleanFileName;
});
$('#constract_drawing8c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8c').innerText = cleanFileName;
});
$('#constract_drawing9c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9c').innerText = cleanFileName;
});
$('#constract_drawing10c').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10c').innerText = cleanFileName;
});

$('#constract_drawing1d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1d').innerText = cleanFileName;
});
$('#constract_drawing2d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2d').innerText = cleanFileName;
});
$('#constract_drawing3d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3d').innerText = cleanFileName;
});
$('#constract_drawing4d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4d').innerText = cleanFileName;
});
$('#constract_drawing5d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5d').innerText = cleanFileName;
});
$('#constract_drawing6d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6d').innerText = cleanFileName;
});
$('#constract_drawing7d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7d').innerText = cleanFileName;
});
$('#constract_drawing8d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8d').innerText = cleanFileName;
});
$('#constract_drawing9d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9d').innerText = cleanFileName;
});
$('#constract_drawing10d').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10d').innerText = cleanFileName;
});

$('#constract_drawing1e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1e').innerText = cleanFileName;
});
$('#constract_drawing2e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2e').innerText = cleanFileName;
});
$('#constract_drawing3e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3e').innerText = cleanFileName;
});
$('#constract_drawing4e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4e').innerText = cleanFileName;
});
$('#constract_drawing5e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5e').innerText = cleanFileName;
});
$('#constract_drawing6e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6e').innerText = cleanFileName;
});
$('#constract_drawing7e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7e').innerText = cleanFileName;
});
$('#constract_drawing8e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8e').innerText = cleanFileName;
});
$('#constract_drawing9e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9e').innerText = cleanFileName;
});
$('#constract_drawing10e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10e').innerText = cleanFileName;
});

$('#constract_drawing1f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1f').innerText = cleanFileName;
});
$('#constract_drawing2f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2f').innerText = cleanFileName;
});
$('#constract_drawing3f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3f').innerText = cleanFileName;
});
$('#constract_drawing4f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4f').innerText = cleanFileName;
});
$('#constract_drawing5f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5f').innerText = cleanFileName;
});
$('#constract_drawing6f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6f').innerText = cleanFileName;
});
$('#constract_drawing7f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7f').innerText = cleanFileName;
});
$('#constract_drawing8f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8f').innerText = cleanFileName;
});
$('#constract_drawing9f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9f').innerText = cleanFileName;
});
$('#constract_drawing10f').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10f').innerText = cleanFileName;
});

$('#constract_drawing1g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1g').innerText = cleanFileName;
});
$('#constract_drawing2g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2g').innerText = cleanFileName;
});
$('#constract_drawing3g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3g').innerText = cleanFileName;
});
$('#constract_drawing4g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4g').innerText = cleanFileName;
});
$('#constract_drawing5g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5g').innerText = cleanFileName;
});
$('#constract_drawing6g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6g').innerText = cleanFileName;
});
$('#constract_drawing7g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7g').innerText = cleanFileName;
});
$('#constract_drawing8g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8g').innerText = cleanFileName;
});
$('#constract_drawing9g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9g').innerText = cleanFileName;
});
$('#constract_drawing10g').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10g').innerText = cleanFileName;
});

$('#constract_drawing1bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1bluea').innerText = cleanFileName;
});
$('#constract_drawing2bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2bluea').innerText = cleanFileName;
});
$('#constract_drawing3bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3bluea').innerText = cleanFileName;
});
$('#constract_drawing4bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4bluea').innerText = cleanFileName;
});
$('#constract_drawing5bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5bluea').innerText = cleanFileName;
});
$('#constract_drawing6bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6bluea').innerText = cleanFileName;
});
$('#constract_drawing7bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7bluea').innerText = cleanFileName;
});
$('#constract_drawing8bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8bluea').innerText = cleanFileName;
});
$('#constract_drawing9bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9bluea').innerText = cleanFileName;
});
$('#constract_drawing10bluea').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10bluea').innerText = cleanFileName;
});

$('#constract_drawing1blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1blueb').innerText = cleanFileName;
});
$('#constract_drawing2blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2blueb').innerText = cleanFileName;
});
$('#constract_drawing3blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3blueb').innerText = cleanFileName;
});
$('#constract_drawing4blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4blueb').innerText = cleanFileName;
});
$('#constract_drawing5blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5blueb').innerText = cleanFileName;
});
$('#constract_drawing6blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6blueb').innerText = cleanFileName;
});
$('#constract_drawing7blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7blueb').innerText = cleanFileName;
});
$('#constract_drawing8blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8blueb').innerText = cleanFileName;
});
$('#constract_drawing9blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9blueb').innerText = cleanFileName;
});
$('#constract_drawing10blueb').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10blueb').innerText = cleanFileName;
});

$('#constract_drawing1bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1bluec').innerText = cleanFileName;
});
$('#constract_drawing2bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2bluec').innerText = cleanFileName;
});
$('#constract_drawing3bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3bluec').innerText = cleanFileName;
});
$('#constract_drawing4bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4bluec').innerText = cleanFileName;
});
$('#constract_drawing5bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5bluec').innerText = cleanFileName;
});
$('#constract_drawing6bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6bluec').innerText = cleanFileName;
});
$('#constract_drawing7bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7bluec').innerText = cleanFileName;
});
$('#constract_drawing8bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8bluec').innerText = cleanFileName;
});
$('#constract_drawing9bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9bluec').innerText = cleanFileName;
});
$('#constract_drawing10bluec').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10bluec').innerText = cleanFileName;
});

$('#constract_drawing1blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1blued').innerText = cleanFileName;
});
$('#constract_drawing2blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2blued').innerText = cleanFileName;
});
$('#constract_drawing3blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3blued').innerText = cleanFileName;
});
$('#constract_drawing4blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4blued').innerText = cleanFileName;
});
$('#constract_drawing5blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5blued').innerText = cleanFileName;
});
$('#constract_drawing6blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6blued').innerText = cleanFileName;
});
$('#constract_drawing7blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7blued').innerText = cleanFileName;
});
$('#constract_drawing8blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8blued').innerText = cleanFileName;
});
$('#constract_drawing9blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9blued').innerText = cleanFileName;
});
$('#constract_drawing10blued').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10blued').innerText = cleanFileName;
});

$('#constract_drawing1bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1bluee').innerText = cleanFileName;
});
$('#constract_drawing2bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2bluee').innerText = cleanFileName;
});
$('#constract_drawing3bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3bluee').innerText = cleanFileName;
});
$('#constract_drawing4bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4bluee').innerText = cleanFileName;
});
$('#constract_drawing5bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5bluee').innerText = cleanFileName;
});
$('#constract_drawing6bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6bluee').innerText = cleanFileName;
});
$('#constract_drawing7bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7bluee').innerText = cleanFileName;
});
$('#constract_drawing8bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8bluee').innerText = cleanFileName;
});
$('#constract_drawing9bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9bluee').innerText = cleanFileName;
});
$('#constract_drawing10bluee').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10bluee').innerText = cleanFileName;
});

$('#constract_drawing1bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files1bluef').innerText = cleanFileName;
});
$('#constract_drawing2bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files2bluef').innerText = cleanFileName;
});
$('#constract_drawing3bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files3bluef').innerText = cleanFileName;
});
$('#constract_drawing4bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files4bluef').innerText = cleanFileName;
});
$('#constract_drawing5bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files5bluef').innerText = cleanFileName;
});
$('#constract_drawing6bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files6bluef').innerText = cleanFileName;
});
$('#constract_drawing7bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files7bluef').innerText = cleanFileName;
});
$('#constract_drawing8bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files8bluef').innerText = cleanFileName;
});
$('#constract_drawing9bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files9bluef').innerText = cleanFileName;
});
$('#constract_drawing10bluef').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files10bluef').innerText = cleanFileName;
});

    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });
</script>
</body>
</html>