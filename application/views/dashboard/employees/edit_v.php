<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
          <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_employees/<?=$data->社員コード?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <label for="employee_code">社員コード</label>
                          <input type="text" class="form-control col-md-6" name="employee_code" id="employee_code" aria-describedby="helpId" placeholder="" value="<?=$data->社員コード?>">
                        </div>
                        <div class="form-group">
                          <label for="last_name">氏名 (姓)</label>
                          <input type="text" class="form-control col-md-6" name="last_name" id="last_name" aria-describedby="helpId" placeholder="" value="<?=$data->姓?>">
                        </div>
                        <div class="form-group">
                          <label for="first_name">氏名 (名)</label>
                          <input type="text" class="form-control col-md-6" name="first_name" id="first_name" aria-describedby="helpId" placeholder="" value="<?=$data->名?>">
                        </div>
                        <div class="form-group">
                          <label for="last_furigana">氏名 (せい)</label>
                          <input type="text" class="form-control col-md-6" name="last_furigana" id="last_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->せい?>">
                        </div>
                        <div class="form-group">
                          <label for="first_furigana">氏名 (めい)</label>
                          <input type="text" class="form-control col-md-6" name="first_furigana" id="first_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->めい?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_1">保有資格1</label>
                          <input type="text" class="form-control col-md-6" name="qualification_1" id="qualification_1" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格1?>">
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sertificate_1" name="sertificate_1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf1" id="pdf1">ファイルを選択</label>
                              </div>
                              <p id="files1"><?php if($data->sertificate_1!="") echo $data->sertificate_1; else echo"ファイルが選択されていません。";?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification_2">保有資格2</label>
                          <input type="text" class="form-control col-md-6" name="qualification_2" id="qualification_2" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格2?>">
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sertificate_2" name="sertificate_2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                              </div>
                              <p id="files2"><?php if($data->sertificate_2!="") echo $data->sertificate_2; else echo"ファイルが選択されていません。";?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification_3">保有資格3</label>
                          <input type="text" class="form-control col-md-6" name="qualification_3" id="qualification_3" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格3?>">
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sertificate_3" name="sertificate_3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf3" id="pdf3">ファイルを選択</label>
                              </div>
                              <p id="files3"><?php if($data->sertificate_3!="") echo $data->sertificate_3; else echo"ファイルが選択されていません。";?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification_4">保有資格4</label>
                          <input type="text" class="form-control col-md-6" name="qualification_4" id="qualification_4" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格4?>">
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sertificate_4" name="sertificate_4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf4" id="pdf4">ファイルを選択</label>
                              </div>
                              <p id="files4"><?php if($data->sertificate_4!="") echo $data->sertificate_4; else echo"ファイルが選択されていません。";?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="qualification_5">保有資格5</label>
                          <input type="text" class="form-control col-md-6" name="qualification_5" id="qualification_5" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格5?>">
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sertificate_5" name="sertificate_5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf5" id="pdf5">ファイルを選択</label>
                              </div>
                              <p id="files5"><?php if($data->sertificate_5!="") echo $data->sertificate_5; else echo"ファイルが選択されていません。";?></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="hire_date">入社日</label>
                          <input type="date" class="form-control col-md-6" name="hire_date" id="hire_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->入社日)) { echo isset($data->入社日) ? set_value('hire_date', date('Y-m-d', strtotime($data->入社日))) : set_value('hire_date'); };?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code">郵便番号1</label>
                          <input type="text" class="form-control col-md-6" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="" value="<?=$data->郵便番号1?>">
                          
                        </div>
                        <div class="form-group">
                          <label for="address_line">住所1</label>
                          <input type="text" class="form-control col-md-6" name="address_line" id="address_line" aria-describedby="helpId" placeholder="" value="<?=$data->住所1?>">
                        </div>
                        <div class="form-group">
                          <label for="address">住所1 番地</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder="" value="<?=$data->住所1番地?>">
                        </div>
                        <div class="form-group">
                          <label for="telephone_number">電話番号</label>
                          <input type="text" class="form-control col-md-6" name="telephone_number" id="telephone_number" aria-describedby="helpId" placeholder="" value="<?=$data->電話番号?>">
                          
                        </div>
                        <div class="form-group">
                          <label for="cellphone_number">携帯番号</label>
                          <input type="text" class="form-control col-md-6" name="cellphone_number" id="cellphone_number" aria-describedby="helpId" placeholder="" value="<?=$data->携帯番号?>">
                          
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="text" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="card card-primary card-outline col-md-6">
                          <div class="card-header">
                            <h3 class="card-title">ID・パスワード一覧</h3>
                          </div> 
                          <div class="card-body">
                            <div class="form-group">
                              <label for="username">ユーザー名</label>
                              <input type="text" class="form-control col-md-6" name="username" id="username" aria-describedby="helpId" placeholder="" value="<?=$datar->username?>">
                            </div>
                            <div class="form-group">
                              <label for="password">パスワード</label>
                              <input type="text" class="form-control col-md-6" name="password" id="password" aria-describedby="helpId" placeholder="" value="<?=$datar->view_password?>">
                            </div>
                            <div class="form-group">
                              <label for="passconf">社員権限</label>
                              <div class="radbut" style="flex:1;">
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="privilage1" name="privilage" value="1" <?php if($datar->access == '1') echo 'checked';?> >
                                      <label for="privilage1" class="form-check-label">管理者</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="privilage2" name="privilage" value="2" <?php if($datar->access == '2') echo 'checked';?> >
                                      <label for="privilage2" class="form-check-label">編集者</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
  $('#sertificate_1').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files1').innerText = cleanFileName;
  });

  $('#sertificate_2').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files2').innerText = cleanFileName;
  });

  $('#sertificate_3').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files3').innerText = cleanFileName;
  });

  $('#sertificate_4').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files4').innerText = cleanFileName;
  });

  $('#sertificate_5').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files5').innerText = cleanFileName;
  });

  $('#employee_code').on('input', function() {
  var c = this.selectionStart,
      r = /[^a-z0-9]/gi,
      v = $(this).val();
  if(r.test(v)) {
    $(this).val(v.replace(r, ''));
    c--;
  }
  this.setSelectionRange(c, c);
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("hire_date").setAttribute("max", "9999-12-31");
</script>
</body>
</html>