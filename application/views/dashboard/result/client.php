<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
<link href="<?=base_url()?>assets/dashboard/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>assets/dashboard/js/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/datetime.js"></script>
<style>
#map { height: 540px; }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">検索結果</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 検索結果</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body table-responsive">
                <table class="table table-striped table-inverse" id="data_customer">
                      <thead class="thead-inverse">
                        <tr>
                          <th>ID</th>
                          <th>氏名</th>
                          <th>連名</th>
                          <th>年齢</th>
                          <th>〒</th>
                          <th>住所</th>
                          <th>建物名・部屋番号</th>
                          <th>電話</th>
                          <th>初回接点</th>
                          <th>初回担当</th>
                          <th>初回年月日</th>
                          <th>担当</th>
                          <th>詳細</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                          <tr>
                            <td><?=$value->gid?></td>
                            <td><?=$value->氏名?></td>
                            <td><?=$value->連名?></td>
                            <td><?php if(empty($value->age)) echo '年代不明'; else echo $value->age;?></td>
                            <td><?=$value->〒?></td>
                            <td><?=$value->住所?></td>
                            <td><?=$value->共同住宅?></td>
                            <td><?=$value->電話?></td>
                            <td><?=$value->初回接点?></td>
                            <td><?=$value->初回担当?></td>
                            <td><?=$value->初回年月日?></td>
                            <td><?php if($value->種別 == 'SOB'){ echo 'SWEET HOME';} else { echo $value->種別;}?></td>
                            <td>
                            <!-- <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_client/<?=$value->gid?>" role="button"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_client/<?=$value->gid?>" role="button"><i class="fa fa-trash"></i></a> -->
                            <a class="btn btn-info" href="<?=base_url()?>dashboard/view_client/<?=$value->gid?>" role="button"><i class="fa fa-eye"></i></a>
                            </td>
                          </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="7">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/cr-1.5.4/datatables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "columnDefs": [
        {   "targets": [0,2,3,4,5],
            "visible": false,
            "searchable": false
        }],
      "paging": true,
      "pageLength": 100,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/ja.json'
      },
      "dom": 'Bfrtip',
      "buttons": [
            {
                "extend": 'excel',
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ],
                    "modifier" : {
                        "order" : 'index', // 'current', 'applied','index', 'original'
                        "page" : 'all', // 'all', 'current'
                        "search" : 'none' // 'none', 'applied', 'removed'
                    }
                }
            },
            {
                "extend": 'pdf',
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ],
                    "modifier" : {
                        "order" : 'index', // 'current', 'applied','index', 'original'
                        "page" : 'all', // 'all', 'current'
                        "search" : 'none' // 'none', 'applied', 'removed'
                    }
                }
            },
            {
                "extend": 'csv',
                "exportOptions": {
                    "columns": [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ],
                    "modifier" : {
                        "order" : 'index', // 'current', 'applied','index', 'original'
                        "page" : 'all', // 'all', 'current'
                        "search" : 'none' // 'none', 'applied', 'removed'
                    }
                }
            },
      ],
    });

</script>
</body>
</html>