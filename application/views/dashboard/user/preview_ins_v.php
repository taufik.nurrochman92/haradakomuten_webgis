<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">ID・パスワード管理</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / ID・パスワード管理</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_datas" method="post">
                        <div class="form-group">
                          <label for="username">ユーザー名</label>
                          <input type="text" class="form-control col-md-6" name="username" id="username" aria-describedby="helpId" placeholder="" value="<?php if(!empty($username)){ echo $username; }?>">

                        </div>
                        <div class="form-group">
                          <label for="password">パスワード</label>
                          <input type="text" class="form-control col-md-6" name="password" id="password" aria-describedby="helpId" placeholder="" value="<?php if(!empty($password)){ echo $password; }?>">

                        </div>
                        <div class="form-group">
                          <label for="passconf">パスワードを再入力</label>
                          <input type="text" class="form-control col-md-6" name="passconf" id="passconf" aria-describedby="helpId" placeholder="" value="<?php if(!empty($passconf)){ echo $passconf; }?>">

                        </div>
                        <input type="hidden" class="form-control col-md-6" name="affilliate_from" id="affilliate_from" aria-describedby="helpId" placeholder="" value="<?php if(!empty($affilliate_from)){ echo $affilliate_from; }?>">
                        <!-- <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                                <label for="affilliate_from">業者名</label>
                                <select class="form-control" name="affilliate_from">
                                <option <?php if ($affilliate_from == "") {echo "selected"; } ?> value="">選択してください</option>
                                <option <?php if ($affilliate_from == "$affilliate_from") {echo "selected"; } ?> value="<?=$affilliate_from?>"><?=$affilliate_from?></option>
                                <?php 
                                if(!empty($data))
                                {
                                  foreach($data as $row)
                                  { 
                                    echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                            </div>
                        </div> -->
                        <button name="" id="" class="btn hvr-sink" type="submit">確認</button>
                        <!-- <button name="edit_btn" value="edit" type="" id="" class="btn white hvr-sink">変更する</button> -->
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>

    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });
</script>
</body>
</html>