<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  style="background-color:#ffcf89">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_inspection/<?=$data->gid?>" method="post">
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" value="<?=$data->latitude?>" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" value="<?=$data->longitude?>" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="type">種別</label>
                          <input type="text" class="form-control col-md-6" name="type" id="type" value="<?=$data->type?>" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="family_name">氏</label>
                          <input type="text" class="form-control col-md-6" name="family_name" id="family_name" aria-describedby="helpId" value="<?=$data->family_name?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="name">名</label>
                          <input type="text" class="form-control col-md-6" name="name" id="name" aria-describedby="helpId" value="<?=$data->name?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="sama">様</label>
                          <input type="text" class="form-control col-md-6" name="sama" id="sama" aria-describedby="helpId" value="<?=$data->sama?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="address">住所</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" value="<?=$data->address?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="phone">電話番号</label>
                          <input type="text" class="form-control col-md-6" name="phone" id="phone" aria-describedby="helpId" value="<?=$data->phone?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="inspection_date">完了検査日</label>
                          <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="inspection_date" id="inspection_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY" value="<?=date("m/d/Y",strtotime($data->inspection_date))?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="delivery_date">引渡し日</label>
                          <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="delivery_date" id="delivery_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY" value="<?=date("m/d/Y",strtotime($data->delivery_date))?>">
                          </div>
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>