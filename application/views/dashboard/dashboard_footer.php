<footer class="main-footer">
    <strong>Copyright &copy; <?=date("Y",strtotime("now"));?> <a href="https://haradakomuten.com/">原田工務店</a>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>assets/dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>assets/dashboard/plugins/moment/moment-with-locales.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>assets/dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dashboard/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>assets/dashboard/js/pages/dashboard.js"></script>
<!-- bs-custom-file-input -->
<script src="<?=base_url()?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dashboard/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dashboard/js/bootstrap-datepicker.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.min.js" integrity="sha512-6+P1bat5JJUm3ZeQKup1qbRitLnE7NE8z47htcKFs8LqH/XAbauzUfg1tGdXJKDJlecq9I/lTm9iCO/0uuho/w==" crossorigin="anonymous"></script> -->

<script type="text/javascript">
    // Set timeout variables.
    var timoutNow = 3600000; // Timeout of 30 mins - time in ms
    var logoutUrl = "<?php echo base_url('dashboard/logout') ?>"; // URL to logout page.

    var timeoutTimer;

    // Start timer
    function StartTimers() {
      timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
    }

    // Reset timer
    function ResetTimers() {
      clearTimeout(timeoutTimer);
      StartTimers();
    }

    // Logout user
    function IdleTimeout() {
      window.location = logoutUrl;
    }
 </script>
<script>
    var url = window.location;
    // for sidebar menu but not for treeview submenu
    $('ul.nav-sidebar a').filter(function() {
        return this.href == url;
    }).parent().siblings().removeClass('active').end().addClass('active');
    // for treeview which is like a submenu
    $('ul.nav-treeview a').filter(function() {
        return this.href == url;
    }).parentsUntil(".nav-sidebar > .nav-treeview").siblings().removeClass('active menu-open').end().addClass('active menu-open');
</script>
</body>
</html>
