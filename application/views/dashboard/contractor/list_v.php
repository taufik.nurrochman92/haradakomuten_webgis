<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
<link href="<?=base_url()?>assets/dashboard/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>assets/dashboard/js/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/datetime.js"></script>
<style>
#map { height: 540px; }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">業者一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 業者一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body table-responsive">
                    <hr>
                    <div id="suplier" style="text-align: right;">
                      <input type="checkbox" name="suplier_type" value="協力会会員" checked> 協力会会員　
                      <input type="checkbox" name="suplier_type" value="準会員" checked> 準会員　
                      <input type="checkbox" name="suplier_type" value="メーカー" checked> メーカー　
                      <input type="checkbox" name="suplier_type" value="" checked> 未設定
                    </div>
                    <hr>
                    <table class="table table-striped table-inverse" id="data_customer">
                      <thead class="thead-inverse">
                        <tr>
                          <th>ID</th>
                          <th>協力業者名</th>
                          <th>〒</th>
                          <th>都道府県</th>
                          <th>住 所</th>
                          <th>建物名・部屋番号</th>
                          <th>電 話</th>
                          <th>取引先種別</th>
                          <th>登録日時</th>
                          <th>詳細</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                          <tr>
                            <td><?=$value->gid?></td>
                            <td><?=$value->name?></td>
                            <td><?=$value->〒?></td>
                            <td><?=$value->都道府県?></td>
                            <td><?=$value->住　　　所?></td>
                            <td><?=$value->住所2?></td>
                            <td><?=$value->電　　話?></td>
                            <td><?=$value->status?></td>
                            <td><?=$value->date_insert?></td>
                            <td>
                            <!-- <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_contractor/<?=$value->id?>" role="button"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor/<?=$value->id?>" role="button"><i class="fa fa-trash"></i></a> -->
                            <a class="btn btn-info" href="<?=base_url()?>dashboard/view_contractor/<?=$value->id?>" role="button"><i class="fa fa-eye"></i></a>
                            </td>
                          </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="14">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- <div class="card">
              <div class="map-wrapper">
                <div id="map" style="margin-top:2%"></div>
                <div class="map-category">
                    <div class="_top">
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-1.png" alt="">
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-2.png" alt="">
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-3.png" alt="">
                        </a>
                    </div>

                    <div class="_bottom">
                        <p>担 当</p>

                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/user1.png" alt="">
                            <span>大井</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/user2.png" alt="">
                            <span>菅沼</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/user3.png" alt="">
                            <span>山下</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/user4.png" alt="">
                            <span>松元</span>
                        </a>
                    </div>
                </div>
              </div>
            </div> -->
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  var table = $('#data_customer').DataTable({
      "columnDefs": [
        {   "targets": [0],
            "visible": false,
            "searchable": false
        }],
      "paging": true,
      "pageLength": 100,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/ja.json'
      },
    });
    $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });

  $('input:checkbox').on('change', function () {
    //build a regex filter string with an or(|) condition
    var positions = $('input:checkbox[name="suplier_type"]:checked').map(function() {
      return '^' + this.value + '$';
    }).get().join('|');
    
    //filter in column 1, with an regex, no smart filtering, not case sensitive
    table.column(7).search(positions, true, false, false).draw(false);

  });
</script>
</body>
</html>