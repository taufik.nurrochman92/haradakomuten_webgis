<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規業者追加</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規業者追加</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('success')?></strong> 
              </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('error')?></strong> 
              </div>
            <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_contractor" method="post" enctype="multipart/form-data">
                    <input type="text" class="form-control col-md-6" name="contractor_code" id="contractor_code" aria-describedby="helpId" placeholder="" value="<?=$for_id->gid+1?>" hidden>
                        <div class="form-group">
                          <label for="kinds">種別</label>
                          <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder="" > -->
                          <select class="form-control col-md-6" name="kinds">
                            <option selected disabled value="">選択してください</option>
                            <option value="解体工事" <?php if($kinds == "解体工事") echo "selected"; ?> >解体工事</option>
                            <option value="造成工事" <?php if($kinds == "造成工事") echo "selected"; ?> >造成工事</option>
                            <option value="プレカット" <?php if($kinds == "プレカット") echo "selected"; ?> >プレカット</option>
                            <option value="基礎工事" <?php if($kinds == "基礎工事") echo "selected"; ?> >基礎工事</option>
                            <option value="建方工事" <?php if($kinds == "建方工事") echo "selected"; ?> >建方工事</option>
                            <option value="瓦工事" <?php if($kinds == "瓦工事") echo "selected"; ?> >瓦工事</option>
                            <option value="板金工事" <?php if($kinds == "板金工事") echo "selected"; ?> >板金工事</option>
                            <option value="外壁工事" <?php if($kinds == "外壁工事") echo "selected"; ?> >外壁工事</option>
                            <option value="大工工事" <?php if($kinds == "大工工事") echo "selected"; ?> >大工工事</option>
                            <option value="建具工事" <?php if($kinds == "建具工事") echo "selected"; ?> >建具工事</option>
                            <option value="鋼製建具" <?php if($kinds == "鋼製建具") echo "selected"; ?> >鋼製建具</option>
                            <option value="内装工事" <?php if($kinds == "内装工事") echo "selected"; ?> >内装工事</option>
                            <option value="給排水工事" <?php if($kinds == "給排水工事") echo "selected"; ?> >給排水工事</option>
                            <option value="電気工事" <?php if($kinds == "電気工事") echo "selected"; ?> >電気工事</option>
                            <option value="ガス工事" <?php if($kinds == "ガス工事") echo "selected"; ?> >ガス工事</option>
                            <option value="畳工事" <?php if($kinds == "畳工事") echo "selected"; ?> >畳工事</option>
                            <option value="浄化槽工事" <?php if($kinds == "浄化槽工事") echo "selected"; ?> >浄化槽工事</option>
                            <option value="太陽光設備工事" <?php if($kinds == "太陽光設備工事") echo "selected"; ?> >太陽光設備工事</option>
                            <option value="外構工事" <?php if($kinds == "外構工事") echo "selected"; ?> >外構工事</option>
                            <option value="流通店" <?php if($kinds == "流通店") echo "selected"; ?> >流通店</option>
                            <option value="設計事務所" <?php if($kinds == "設計事務所") echo "selected"; ?> >設計事務所</option>
                            <option value="地盤調査" <?php if($kinds == "地盤調査") echo "selected"; ?> >地盤調査</option>
                            <option value="防蟻工事" <?php if($kinds == "防蟻工事") echo "selected"; ?> >防蟻工事</option>
                            <option value="ハウスクリーニング" <?php if($kinds == "ハウスクリーニング") echo "selected"; ?> >ハウスクリーニング</option>
                            <option value="産廃処理" <?php if($kinds == "産廃処理") echo "selected"; ?> >産廃処理</option>
                            <option value="断熱工事" <?php if($kinds == "断熱工事") echo "selected"; ?> >断熱工事</option>
                            <option value="カーテン工事" <?php if($kinds == "カーテン工事") echo "selected"; ?> >カーテン工事</option>
                            <option value="地盤改良工事" <?php if($kinds == "地盤改良工事") echo "selected"; ?> >地盤改良工事</option>
                            <option value="仮設工事" <?php if($kinds == "仮設工事") echo "selected"; ?> >仮設工事</option>
                            <option value="補修工事" <?php if($kinds == "補修工事") echo "selected"; ?> >補修工事</option>
                            <option value="左官/タイル工事" <?php if($kinds == "左官/タイル工事") echo "selected"; ?> >左官/タイル工事</option>
                            <option value="製材/材木" <?php if($kinds == "製材/材木") echo "selected"; ?> >製材/材木</option>
                            <option value="レッカー" <?php if($kinds == "レッカー") echo "selected"; ?> >レッカー</option>
                            <option value="防水工事" <?php if($kinds == "防水工事") echo "selected"; ?> >防水工事</option>
                            <option value="塗装工事" <?php if($kinds == "塗装工事") echo "selected"; ?> >塗装工事</option>
                            <option value="メーカー" <?php if($kinds == "メーカー") echo "selected"; ?> >メーカー</option>
                            <option value="その他" <?php if($kinds == "その他") echo "selected"; ?> >その他</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" aria-describedby="helpId" placeholder="" value="<?php if(!empty($b)) echo $b; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" aria-describedby="helpId" placeholder="" value="<?php if(!empty($l)) echo $l; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="no">No.</label>
                          <input type="number" class="form-control col-md-6" name="no" id="no" aria-describedby="helpId" maxlength="5" placeholder="" value="<?php if(!empty($no)) echo $no; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="name">協力業者名</label>
                          <input type="text" class="form-control col-md-6" name="name" id="name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($name)) echo $name; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="joint_name">連名</label>
                          <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($joint_name)) echo $joint_name; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code">〒</label>
                          <input type="number" class="form-control col-md-6" name="zip_code" id="zip_code" onkeydown="return event.keyCode !== 69" onkeyup="AjaxZip3.zip2addr(this,'','prefecture','address');" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($zip_code)) echo $zip_code; else ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="prefecture">都道府県</label>
                          <input type="text" class="form-control col-md-6" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($prefecture)) echo $prefecture; else ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="address">住　所</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($address)) echo $address; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="address2">建物名・部屋番号</label>
                          <input type="text" class="form-control col-md-6" name="address2" id="address2" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($address2)) echo $address2; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="telephone">電　話</label>
                          <input type="text" class="form-control col-md-6" name="telephone" id="telephone" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($telephone)) echo $telephone; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="invoice">インボイス番号</label>
                          <input type="number" class="form-control col-md-6" name="invoice" id="invoice" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($invoice)) echo $invoice; else echo ''; ?>">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                        <div class="form-group">
                          <label for="pic">担当者名</label>
                          <input type="text" class="form-control col-md-6" name="pic" id="pic" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($pic)) echo $pic; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="phone">携帯番号</label>
                          <input type="number" class="form-control col-md-6" name="phone" id="phone" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($phone)) echo $phone; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="fax">FAX</label>
                          <input type="text" class="form-control col-md-6" name="fax" id="fax" maxlength="12" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($fax)) echo $fax; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="email" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="" oninvalid="this.setCustomValidity('メールアドレスを入力してください。')" oninput="this.setCustomValidity('')"  value="<?php if(!empty($email)) echo $email; else echo ''; ?>">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                        <div class="form-group">
                          <label for="pic_nd">担当者名2</label>
                          <input type="text" class="form-control col-md-6" name="pic_nd" id="pic_nd" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($pic_nd)) echo $pic_nd; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="phone_nd">携帯番号2</label>
                          <input type="number" class="form-control col-md-6" name="phone_nd" id="phone_nd" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($phone_nd)) echo $phone_nd; else echo ''; ?>">
                        </div>
                        <div class="form-group">
                          <label for="email_nd">メールアドレス2</label>
                          <input type="email" class="form-control col-md-6" name="email_nd" id="email_nd" aria-describedby="helpId" placeholder="" oninvalid="this.setCustomValidity('メールアドレスを入力してください。')" oninput="this.setCustomValidity('')"  value="<?php if(!empty($email_nd)) echo $email_nd; else echo ''; ?>">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                        <div class="form-group d-flex">
                          <div class="label">
                            <label for="project_stat">取引状態</label>
                          </div>
                          <div class="radbut" style="flex:1;">
                            <div class="col-sm-6">
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" id="project_stat1" name="project_stat" value="取引中"  <?php if($project_stat == "") echo "checked"; ?>>
                                  <label for="project_stat1" class="form-check-label">取引中</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" id="project_stat2" name="project_stat" value="取引中止中"  <?php if($project_stat =="取引中止中") echo "checked"; ?>>
                                  <label for="project_stat2" class="form-check-label">取引中止中</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="status">取引先種別</label>
                          <select class="form-control col-md-6" name="status">
                            <option selected disabled value="">選択してください</option>
                            <option value="協力会会員" <?php if($status == "協力会会員") echo "selected"; ?> >協力会会員</option>
                            <option value="準会員" <?php if($status == "準会員") echo "selected"; ?> >準会員</option>
                            <option value="メーカー" <?php if($status == "メーカー") echo "selected"; ?> >メーカー</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="files">新規入場</label>
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="admission" name="admission" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf1" id="pdf1">ファイルを選択</label>
                              </div>
                              <p id="files1">ファイルが選択されていません。</p>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                        <div class="card card-primary card-outline col-md-6" style="margin-top:70px;">
                          <div class="card-header">
                            <h3 class="card-title">ID・パスワード一覧</h3>
                          </div> 
                          <div class="card-body">
                            <div class="form-group">
                              <label for="username">ユーザー名</label>
                              <input type="text" class="form-control col-md-6" name="username" id="username" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($usernamerr)) echo $usernamerr; else echo ''; ?>">
                              <div class="err"><?php echo form_error('username'); ?></div>
                            </div>
                            <div class="form-group">
                              <label for="password">パスワード</label>
                              <input type="text" class="form-control col-md-6" name="password" id="password" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($view_passworddd)) echo $view_passworddd; else echo '';  ?>">
                              <div class="err"><?php echo form_error('password'); ?></div>
                            </div>
                            <div class="form-group">
                              <label for="passconf">パスワードを再入力</label>
                              <input type="text" class="form-control col-md-6" name="passconf" id="passconf" aria-describedby="helpId" placeholder=""  value="<?php if(!empty($view_passworddd)) echo $view_passworddd; else echo ''; ?>">
                              <div class="err"><?php echo form_error('passconf'); ?></div>
                            </div>
                            <!-- <div class="form-row">
                                <div class="form-group dropdowns">
                                  <label for="affilliate_from">業者名</label>
                                  <select class="form-control" name="affilliate_from">
                                    <option value="">選択してください</option>
                                    <?php 
                                    if(!empty($data))
                                    {
                                      foreach($data as $row)
                                      { 
                                        echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                      }
                                    }
                                    else
                                    {
                                      echo '<option value="">社員データなし</option>';
                                    }
                                    ?>
                                  </select>
                                  <div class="err"><?php echo form_error('affilliate_from'); ?></div>
                                </div>
                            </div> -->
                          </div>
                        </div>

                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
  $('#admission').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files1').innerText = cleanFileName;
  });

//   $('input[name="name"]').on('input', function() {
//   var c = this.selectionStart,
//       r = /[^a-z0-9]/gi,
//       v = $(this).val();
//   if(r.test(v)) {
//     $(this).val(v.replace(r, ''));
//     c--;
//   }
//   this.setSelectionRange(c, c);
// });

$(document).ready(function () {

  $("#fax").keyup(function (e) {
      var value = $("#fax").val();
      if (e.key.match(/[0-9]/) == null) {
          value = value.replace(e.key, "");
          $("#fax").val(value);
          return;
      }

      if (value.length == 3) {
          $("#fax").val(value + "-")
      }
      if (value.length == 7) {
          $("#fax").val(value + "-")
      }
  });
});

</script>
</body>
</html>