<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">業者詳細情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 業者詳細情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content view_custom">
      <div class="container-fluid">
        <!-- Main row -->
        <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          // $(".alert").alert();
        </script>
        <div class="row">
          <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-body">

                    <div class="button" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_contractor/<?=$data->id?>" role="button"><i class="fa fa-edit"></i></a>
                                <?php if(isset($data_affiliate[0]->username) === TRUE ) :?>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor_all/<?=$data->id?>/<?=$data_affiliate[0]->username?>" role="button"><i class="fa fa-trash"></i></a>
                                <?php else :?>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor/<?=$data->id?>" role="button"><i class="fa fa-trash"></i></a>
                                <?php endif;?>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="kinds">種別</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->種別?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="latitude">緯度</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->b?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="longitude">経度</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->l?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="no">No.</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->no?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="name">協力業者名</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->name?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="joint_name">連名</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->連名?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="zip_code">〒</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->〒?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="prefecture">都道府県</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->都道府県?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="address">住　所</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->住　　　所?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="address2">建物名・部屋番号</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->住所2?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">電　話</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->電　　話?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">インボイス番号</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->invoice?></p>
                            </div>
                        </div>
                    </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">担当者名</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->pic?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">携帯番号</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->phone?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">FAX</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->fax?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">メールアドレス</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->email?></p>
                            </div>
                        </div>
                    </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">担当者名2</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->pic_nd?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">携帯番号2</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->phone_nd?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">メールアドレス2</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->email_nd?></p>
                            </div>
                        </div>
                    </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">取引状態</label>
                            </div>
                            <div class="text-view">
                            <?php if($data->project_stat == '取引中' || empty($data->project_stat)) :?>
                            <p>取引中</p>
                            <?php else :?>
                            <p>取引中止中</p>
                            <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">取引先種別</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->status?></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="telephone">新規入場</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>dashboard/view_files_contract/<?=$data->admission?>"><?=$data->admission?></a></p>
                            </div>
                        </div>
                    </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                    <!-- <?php if($_SESSION["access"] == 'super_admin') : ?> -->
                    <div class="button" style="margin-top:20px;">
                        <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_contractor/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                    <!-- <?php endif;?> -->
                    <br>
                    <div class="card">
                    <div class="card-body table-responsive">
                        <table class="table table-striped table-inverse" id="data_customer">
                          <thead class="thead-inverse">
                            <tr>
                              <th>ユーザー名</th>
                              <th>パスワード</th>
                              <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                              if(count($data_affiliate) > 0):
                                foreach($data_affiliate as $key => $value):
                              ?>
                              <tr>
                                <td><?=$value->username?></td>
                                <td><?=$value->view_password?></td>
                                <td><a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_user/<?=$value->id?>" role="button"><i class="fa fa-edit"></i></a></td>
                              </tr>
                              <?php 
                                endforeach;
                              else:
                              ?>
                                <tr>
                                  <td colspan="3">
                                    <center><a class="btn btn-primary" href="<?=base_url()?>dashboard/add_user/<?=$data->gid?>" role="button"><i class="fa fa-plus"></i></a></center>
                                  </td>
                                </tr>
                                <?php
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </div>

                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
    $('.delete').confirm({
    title:'',
    content: "業者情報を削除します、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
</body>
</html>