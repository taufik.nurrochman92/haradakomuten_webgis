<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 協力業者</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 協力業者</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('success')?></strong> 
              </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('error')?></strong> 
              </div>
            <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_contractor/<?=$data->gid?>" method="post" enctype="multipart/form-data">
                        <!-- <div class="form-group">
                          <label for="contractor_code">業者コード</label>
                          <input type="text" class="form-control col-md-6" name="contractor_code" id="contractor_code" aria-describedby="helpId" placeholder="" value="<?=$data->gid?>">
                        </div> -->
                        <div class="form-group">
                          <label for="kinds">種別</label>
                          <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder="" value="<?=$data->種別?>"> -->
                          <select class="form-control col-md-6" name="kinds">
                            <option <?php if ($data->種別 == "") {echo "selected"; } ?> disabled value="">選択してください</option>
                            <option <?php if ($data->種別 == "解体工事") {echo "selected"; } ?> value="解体工事">解体工事</option>
                            <option <?php if ($data->種別 == "造成工事") {echo "selected"; } ?> value="造成工事">造成工事</option>
                            <option <?php if ($data->種別 == "プレカット") {echo "selected"; } ?> value="プレカット">プレカット</option>
                            <option <?php if ($data->種別 == "基礎工事") {echo "selected"; } ?> value="基礎工事">基礎工事</option>
                            <option <?php if ($data->種別 == "建方工事") {echo "selected"; } ?> value="建方工事">建方工事</option>
                            <option <?php if ($data->種別 == "瓦工事") {echo "selected"; } ?> value="瓦工事">瓦工事</option>
                            <option <?php if ($data->種別 == "板金工事") {echo "selected"; } ?> value="板金工事">板金工事</option>
                            <option <?php if ($data->種別 == "外壁工事") {echo "selected"; } ?> value="外壁工事">外壁工事</option>
                            <option <?php if ($data->種別 == "大工工事") {echo "selected"; } ?> value="大工工事">大工工事</option>
                            <option <?php if ($data->種別 == "建具工事") {echo "selected"; } ?> value="建具工事">建具工事</option>
                            <option <?php if ($data->種別 == "鋼製建具") {echo "selected"; } ?> value="鋼製建具">鋼製建具</option>
                            <option <?php if ($data->種別 == "内装工事") {echo "selected"; } ?> value="内装工事">内装工事</option>
                            <option <?php if ($data->種別 == "給排水工事") {echo "selected"; } ?> value="給排水工事">給排水工事</option>
                            <option <?php if ($data->種別 == "電気工事") {echo "selected"; } ?> value="電気工事">電気工事</option>
                            <option <?php if ($data->種別 == "ガス工事") {echo "selected"; } ?> value="ガス工事">ガス工事</option>
                            <option <?php if ($data->種別 == "畳工事") {echo "selected"; } ?> value="畳工事">畳工事</option>
                            <option <?php if ($data->種別 == "浄化槽工事") {echo "selected"; } ?> value="浄化槽工事">浄化槽工事</option>
                            <option <?php if ($data->種別 == "太陽光設備工事") {echo "selected"; } ?> value="太陽光設備工事">太陽光設備工事</option>
                            <option <?php if ($data->種別 == "外構工事") {echo "selected"; } ?> value="外構工事">外構工事</option>
                            <option <?php if ($data->種別 == "流通店") {echo "selected"; } ?> value="流通店">流通店</option>
                            <option <?php if ($data->種別 == "設計事務所") {echo "selected"; } ?> value="設計事務所">設計事務所</option>
                            <option <?php if ($data->種別 == "地盤調査") {echo "selected"; } ?> value="地盤調査">地盤調査</option>
                            <option <?php if ($data->種別 == "防蟻工事") {echo "selected"; } ?> value="防蟻工事">防蟻工事</option>
                            <option <?php if ($data->種別 == "ハウスクリーニング") {echo "selected"; } ?> value="ハウスクリーニング">ハウスクリーニング</option>
                            <option <?php if ($data->種別 == "産廃処理") {echo "selected"; } ?> value="産廃処理">産廃処理</option>
                            <option <?php if ($data->種別 == "断熱工事") {echo "selected"; } ?> value="断熱工事">断熱工事</option>
                            <option <?php if ($data->種別 == "カーテン工事") {echo "selected"; } ?> value="カーテン工事">カーテン工事</option>
                            <option <?php if ($data->種別 == "地盤改良工事") {echo "selected"; } ?> value="地盤改良工事">地盤改良工事</option>
                            <option <?php if ($data->種別 == "仮設工事") {echo "selected"; } ?> value="仮設工事">仮設工事</option>
                            <option <?php if ($data->種別 == "補修工事") {echo "selected"; } ?> value="補修工事">補修工事</option>
                            <option <?php if ($data->種別 == "左官/タイル工事") {echo "selected"; } ?> value="左官/タイル工事">左官/タイル工事</option>
                            <option <?php if ($data->種別 == "製材/材木") {echo "selected"; } ?> value="製材/材木">製材/材木</option>
                            <option <?php if ($data->種別 == "レッカー") {echo "selected"; } ?> value="レッカー">レッカー</option>
                            <option <?php if ($data->種別 == "防水工事") {echo "selected"; } ?> value="防水工事">防水工事</option>
                            <option <?php if ($data->種別 == "塗装工事") {echo "selected"; } ?> value="塗装工事">塗装工事</option>
                            <option <?php if ($data->種別 == "メーカー") {echo "selected"; } ?> value="メーカー">メーカー</option>
                            <option <?php if ($data->種別 == "その他") {echo "selected"; } ?> value="その他">その他</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" aria-describedby="helpId" placeholder="" value="<?=$data->b?>">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" aria-describedby="helpId" placeholder="" value="<?=$data->l?>">
                        </div>
                        <div class="form-group">
                          <label for="no">No.</label>
                          <input type="number" class="form-control col-md-6" name="no" id="no" aria-describedby="helpId" maxlength="5" placeholder="" value="<?=$data->no?>">
                        </div>
                        <div class="form-group">
                          <label for="name">協力業者名</label>
                          <input type="text" class="form-control col-md-6" name="name" id="name" aria-describedby="helpId" placeholder="" value="<?=$data->name?>">
                        </div>
                        <div class="form-group">
                          <label for="joint_name">連名</label>
                          <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name" aria-describedby="helpId" placeholder="" value="<?=$data->連名?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code">〒</label>
                          <input type="number" class="form-control col-md-6" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="" onkeydown="return event.keyCode !== 69"  onkeyup="AjaxZip3.zip2addr(this,'','prefecture','address');" value="<?=$data->〒?>">
                        </div>
                        <div class="form-group">
                          <label for="prefecture">都道府県</label>
                          <input type="text" class="form-control col-md-6" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder="" value="<?=$data->都道府県?>">
                        </div>
                        <div class="form-group">
                          <label for="address">住　所</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder="" value="<?=$data->住　　　所?>">
                        </div>
                        <div class="form-group">
                          <label for="address2">建物名・部屋番号</label>
                          <input type="text" class="form-control col-md-6" name="address2" id="address2" aria-describedby="helpId" placeholder="" value="<?=$data->住所2?>">
                        </div>
                        <div class="form-group">
                          <label for="telephone">電　話</label>
                          <input type="text" class="form-control col-md-6" name="telephone" id="telephone" aria-describedby="helpId" placeholder="" value="<?=$data->電　　話?>">
                        </div>
                        <div class="form-group">
                          <label for="invoice">インボイス番号</label>
                          <input type="number" class="form-control col-md-6" name="invoice" id="invoice" aria-describedby="helpId" placeholder="" value="<?=str_replace("T","",$data->invoice)?>">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                        <div class="form-group">
                          <label for="pic">担当者名</label>
                          <input type="text" class="form-control col-md-6" name="pic" id="pic" aria-describedby="helpId" placeholder="" value="<?=$data->pic?>">
                        </div>
                        <div class="form-group">
                          <label for="phone">携帯番号</label>
                          <input type="number" class="form-control col-md-6" name="phone" id="phone" aria-describedby="helpId" placeholder="" value="<?=$data->phone?>">
                        </div>
                        <div class="form-group">
                          <label for="fax">FAX</label>
                          <input type="text" class="form-control col-md-6" name="fax" id="fax" maxlength="12" aria-describedby="helpId" placeholder="" value="<?=$data->fax?>">
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="email" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="" value="<?=$data->email?>" oninvalid="this.setCustomValidity('メールアドレスを入力してください。')" oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                        <div class="form-group">
                          <label for="pic_nd">担当者名2</label>
                          <input type="text" class="form-control col-md-6" name="pic_nd" id="pic_nd" aria-describedby="helpId" placeholder="" value="<?=$data->pic_nd?>">
                        </div>
                        <div class="form-group">
                          <label for="phone_nd">携帯番号2</label>
                          <input type="number" class="form-control col-md-6" name="phone_nd" id="phone_nd" aria-describedby="helpId" placeholder="" value="<?=$data->phone_nd?>">
                        </div>
                        <div class="form-group">
                          <label for="email_nd">メールアドレス2</label>
                          <input type="email" class="form-control col-md-6" name="email_nd" id="email_nd" aria-describedby="helpId" placeholder="" value="<?=$data->email_nd?>" oninvalid="this.setCustomValidity('メールアドレスを入力してください。')" oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 1px dashed #000;display: block;"></span>
                        </div>
                        <div class="form-group d-flex">
                          <div class="label">
                            <label for="project_stat">取引状態</label>
                          </div>
                          <div class="radbut" style="flex:1;">
                            <div class="col-sm-6">
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" id="project_stat1" name="project_stat" value="取引中" <?php if($data->project_stat=='取引中' ) {echo "checked" ; }?>>
                                  <label for="project_stat1" class="form-check-label">取引中</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" id="project_stat2" name="project_stat" value="取引中止中" <?php if($data->project_stat=='取引中止中' ) {echo "checked" ; }?>>
                                  <label for="project_stat2" class="form-check-label">取引中止中</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="status">取引先種別</label>
                          <select class="form-control col-md-6" name="status">
                            <option <?php if ($data->status == "") {echo "selected"; } ?> disabled value="">選択してください</option>
                            <option <?php if ($data->status == "協力会会員") {echo "selected"; } ?> value="協力会会員">協力会会員</option>
                            <option <?php if ($data->status == "準会員") {echo "selected"; } ?> value="準会員">準会員</option>
                            <option <?php if ($data->status == "メーカー") {echo "selected"; } ?> value="メーカー">メーカー</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="files">新規入場</label>
                          <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="admission" name="admission" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                <label class="custom-file-label" for="pdf1" id="pdf1">ファイルなし</label>
                              </div>
                              <p id="files1"><a href="<?=base_url()?>uploads/contractor/<?=$data->admission?>"><?php if($data->admission!="") echo $data->admission; else echo"ファイルが選択されていません。";?></a></p>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <span style="border-bottom: 2px solid #000;display: block;"></span>
                        </div>
                        
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
  $('#admission').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files1').innerText = cleanFileName;
  });

//   $('input').on('input', function() {
//   var c = this.selectionStart,
//       r = /[^a-z0-9]/gi,
//       v = $(this).val();
//   if(r.test(v)) {
//     $(this).val(v.replace(r, ''));
//     c--;
//   }
//   this.setSelectionRange(c, c);
// });

$(document).ready(function () {

$("#fax").keyup(function (e) {
    var value = $("#fax").val();
    if (e.key.match(/[0-9]/) == null) {
        value = value.replace(e.key, "");
        $("#fax").val(value);
        return;
    }

    if (value.length == 3) {
        $("#fax").val(value + "-")
    }
    if (value.length == 7) {
        $("#fax").val(value + "-")
    }
});
});
</script>
</body>
</html>