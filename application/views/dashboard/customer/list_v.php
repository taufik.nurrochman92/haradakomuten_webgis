<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
<link href="<?=base_url()?>assets/dashboard/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>assets/dashboard/js/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/datetime.js"></script>
<style>
#map { height: 540px; }

table.dataTable th.hidden, table.dataTable td.hidden {
  display: none;
}

div.dataTables_length {
    padding-left: 2em;
    }
    div.dataTables_length,
    div.dataTables_filter {
        padding-top: 0.55em;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">営業中顧客一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 営業中顧客一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body table-responsive">
                    <form action="<?=base_url()?>dashboard/search_client_list" method="post" style="position: relative;">
                      <div class="form-inline">
                          <input id="full_name" title="full_name" placeholder="氏名" class="filter-input form-control form-control-sm m-2" type="text" name="full_name" <?php if(!empty($full_name)) echo 'value="'.$full_name.'"';?> >
                          <input id="street" title="street" placeholder="住所" class="filter-input form-control form-control-sm m-2" type="text" name="street" <?php if(!empty($street)) echo 'value="'.$street.'"';?> >
                          <input id="phone" title="phone" placeholder="電話番号" class="filter-input form-control form-control-sm m-2" type="text" name="phone" <?php if(!empty($phone)) echo 'value="'.$phone.'"';?> >
                          <select id="with_or_without" name="with_or_without" class="form-control form-control-sm m-2">
                            <option value="">土地</option>
                            <option value="あり" <?php if(!empty($with_or_without) && $with_or_without == 'あり') echo 'selected';?>>あり</option>
                            <option value="なし" <?php if(!empty($with_or_without) && $with_or_without == 'なし') echo 'selected';?>>なし</option>
                          </select>
                      </div>
                      <div class="form-inline">
                          <!-- <input type="text" class="filter-input form-control form-control-sm m-2" name="first_time_change" id="first_time_change" aria-describedby="helpId" placeholder="初回担当" <?php if(!empty($first_time_change)) echo 'value="'.$first_time_change.'"';?>>
                          <input type="text" class="filter-input form-control form-control-sm m-2" name="pic_person" id="pic_person1" aria-describedby="helpId" placeholder="担当" <?php if(!empty($pic_person)) echo 'value="'.$pic_person.'"';?>> -->

                          <select class="form-control form-control-sm m-2" name="first_time_change" id="first_time_change2">
                            <option value="">初回担当</option>
                            <?php if(!empty($first_time_change)) :?>
                            <option selected value="<?=$first_time_change?>"><?=$first_time_change?></option>
                            <?php 
                              endif;
                            if(!empty($datas))
                            {
                              foreach($datas as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>  

                          <select class="form-control form-control-sm m-2" name="pic_person" id="pic_person">
                            <option value="">担当</option>
                            <?php if(!empty($pic_person)) :?>
                            <option selected value="<?=$pic_person?>"><?=$pic_person?></option>
                            <?php 
                              endif;
                            if(!empty($datas))
                            {
                              foreach($datas as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>  
                          <input type="text" class="filter-input form-control form-control-sm m-2" id="min" name="min" placeholder="登録日" onfocus="(this.type='date')" onblur="(this.type='text')" <?php if(!empty($min)) echo 'value="'.$min.'"';?>>
                          ~
                          <input type="text" class="filter-input form-control form-control-sm m-2" id="max" name="max" placeholder="登録日" onfocus="(this.type='date')" onblur="(this.type='text')" <?php if(!empty($max)) echo 'value="'.$max.'"';?>>
                          <select id="dm" name="dm" class="form-control form-control-sm m-2">
                              <option value="">DM</option>
                              <option value="〇" <?php if(!empty($dm) && $dm == '〇') echo 'selected';?>>〇</option>
                              <option value="△" <?php if(!empty($dm) && $dm == '△') echo 'selected';?>>△</option>
                              <option value="X" <?php if(!empty($dm) && $dm == 'X') echo 'selected';?>>X</option>
                          </select>
                      </div>
                      <div class="form-inline" id="tableActions" style="position: absolute;right: 5px;bottom: 15px;">
                        <input name="" id="" class="dt-button buttons-csv buttons-html5" type="submit" value="検索" style="margin-right: 30px;">
                      </div>
                    </form>
                    <table class="table table-striped table-inverse" id="data_customer">
                      <thead class="thead-inverse">
                        <tr>
                          <th>ID</th>
                          <th>登録日時</th>
                          <th>氏名</th>
                          <th>住所</th>
                          <th>電話番号</th>
                          <th>初回担当</th>
                          <th>初回年月日</th>
                          <th>担当</th>
                          <th class="noExport">詳細</th>
                          <th class="hidden">奥様名前</th>
                          <th class="hidden">奥様名前 (かな)</th>
                          <th class="hidden">奥様生年月日</th>
                          <th class="hidden">お子様1名前</th>
                          <th class="hidden">お子様1名前 (かな)</th>
                          <th class="hidden">お子様1生年月日</th>
                          <th class="hidden">お子様2名前</th>
                          <th class="hidden">お子様2名前 (かな)</th>
                          <th class="hidden">お子様2生年月日</th>
                          <th class="hidden">お子様3名前</th>
                          <th class="hidden">お子様3名前 (かな)</th>
                          <th class="hidden">お子様3生年月日</th>
                          <th class="hidden">種別</th>
                          <th class="hidden">初回接点</th>
                          <th class="hidden">土地　有無</th>
                          <th class="hidden">DM</th>
                          <th class="hidden">メールアドレス</th>
                          <th class="hidden">備考</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):

                              // date_default_timezone_set('Asia/Tokyo');    

                              // $he = $value->初回年月日;
                              // $now = new DateTime($he);
          
          
                              // $formatter = new IntlDateFormatter('ja_JP@calendar=japanese', IntlDateFormatter::MEDIUM,
                              // IntlDateFormatter::MEDIUM, 'Asia/Tokyo', IntlDateFormatter::TRADITIONAL);

                              // $str = substr($formatter->format($now), 0, strrpos($formatter->format($now), ' '));

                          ?>
                          <tr>
                            <td><?=$value->gid?></td>
                            <td><?=$value->date_insert?></td>
                            <td><?=$value->氏名?><br>(<?=$value->氏名_かな?>)</td>
                            <td>〒<?=$value->〒?>　<?=$value->都道府県?>　<?=$value->住所?>　<?=$value->共同住宅?></td>
                            <td><?=$value->電話?></td>
                            <td><?=$value->初回担当?></td>
                            <td><?=$value->初回年月日?></td>                           
                            <td><?=$value->担当?></td>
                           
                            <td>
                            <!-- <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_client/<?=$value->gid?>" role="button"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_client/<?=$value->gid?>" role="button"><i class="fa fa-trash"></i></a> -->
                            <a class="btn btn-info" href="<?=base_url()?>dashboard/view_client/<?=$value->gid?>" role="button"><i class="fa fa-eye"></i></a>
                            </td>
                            <td class="hidden"><?=$value->奥様名前?></td>
                            <td class="hidden"><?=$value->奥様名前_かな?></td>
                            <td class="hidden"><?=$value->奥様生年月日?></td>
                            <td class="hidden"><?=$value->お子様1名前?></td>
                            <td class="hidden"><?=$value->お子様1名前_かな?></td>
                            <td class="hidden"><?=$value->お子様1生年月日?></td>
                            <td class="hidden"><?=$value->お子様2名前?></td>
                            <td class="hidden"><?=$value->お子様2名前_かな?></td>
                            <td class="hidden"><?=$value->お子様2生年月日?></td>
                            <td class="hidden"><?=$value->お子様3名前?></td>
                            <td class="hidden"><?=$value->お子様3名前_かな?></td>
                            <td class="hidden"><?=$value->お子様3生年月日?></td>
                            <td class="hidden"><?=$value->種別?></td>
                            <td class="hidden"><?=$value->初回接点?></td>
                            <td class="hidden"><?=$value->土地　有無?></td>
                            <td class="hidden"><?=$value->ＤＭ?></td>
                            <td class="hidden"><?=$value->メールアドレス?></td>
                            <td class="hidden"><?=$value->備考?></td>
                          </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="7">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.11.3/sorting/formatted-numbers.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.11.3/sorting/natural.js"></script>
<script>
  
  var table = $('#data_customer').DataTable({
      "columnDefs": [
          {   "targets": [0],
              "visible": false,
              "searchable": false
          }],
      "stateSave": true,
      "paging": true,
      "pageLength": 100,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "order": [[6, 'desc']],
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/ja.json'
      },
      "dom": "<'top'i>rt<'clear'>" + 
          "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    });

    var buttons = new $.fn.dataTable.Buttons(table, {
      buttons: [
        { 
          extend: 'csvHtml5',
          text: "CSV",
          charset: 'UTF-8',
          fieldBoundary: '"',
          fieldSeparator: ',',
          footer: true,
          bom: true ,
          exportOptions: {
              columns: "thead th:not(.noExport)",
              modifier : {
                  order : 'index', 
                  page : 'all', 
                  search : 'applied'
              }
          }
        }
      ]
  }).container().appendTo($('#tableActions'));

    $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });

</script>
</body>
</html>