<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 営業中顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 営業中顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('success')?></strong> 
              </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong><?=$this->session->flashdata('error')?></strong> 
              </div>
            <?php endif;?>
            <div class="card">
                <div class="card-body">
                <form action="<?=base_url()?>dashboard/update_client/<?=$data->gid?>" method="post" onsubmit="return validateForm();">
                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="latitude">緯度</label>
                      <input type="text" class="form-control" name="latitude" id="latitude" aria-describedby="helpId"
                        placeholder="" value="<?=$data->b?>">
                    </div>
                    <div class="col-md-6">
                      <label for="longitude">経度</label>
                      <input type="text" class="form-control" name="longitude" id="longitude" aria-describedby="helpId"
                        placeholder="" value="<?=$data->l?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="full_name">氏名 (漢字) <span class="red">※必須</span></label>
                  <input type="text" class="form-control col-md-6" name="full_name" id="full_name"
                    aria-describedby="helpId" placeholder="" value="<?=$data->氏名?>" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                </div>
                <div class="form-group">
                  <label for="full_name_kana">氏名（かな）</label>
                  <input type="text" class="form-control col-md-6" name="full_name_kana" id="full_name_kana"
                    aria-describedby="helpId" placeholder="" value="<?=$data->氏名_かな?>" oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                </div>
                <div class="form-group">
                  <label for="joint_name">連名</label>
                  <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name"
                    aria-describedby="helpId" placeholder="" value="<?=$data->連名?>">
                </div>
                <div class="form-group">
                  <label for="birth_date">生年月日</label>
                  <input type="date" class="form-control col-md-6" name="birth_date" id="birth_date"
                    aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->birth_date)) { echo isset($data->birth_date) ? set_value('birth_date', date('Y-m-d', strtotime($data->birth_date))) : set_value('birth_date'); };?>">
                </div>
                <div class="form-group">
                  <label for="age">年齢</label>
                  <input type="text" class="form-control col-md-6" name="age" id="age"
                    aria-describedby="helpId" placeholder="" value="<?=$data->age?>">
                </div>
                <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="zip_code">〒</label>
                      <input type="number" class="form-control col-md-6" name="zip_code" id="zip_code" onkeydown="return event.keyCode !== 69" onkeyup="AjaxZip3.zip2addr(this,'','prefecture','street');"
                        aria-describedby="helpId" placeholder="" value="<?=$data->〒?>">
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <label for="prefecture">都道府県</label>
                      <input type="text" class="form-control col-md-6" name="prefecture" id="prefecture"
                        aria-describedby="helpId" placeholder="" value="<?=$data->都道府県?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="street">住所</label>
                      <input type="text" class="form-control col-md-12" name="street" id="street"
                        aria-describedby="helpId" placeholder="" value="<?=$data->住所?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="apart_name">建物名・部屋番号</label>
                  <input type="text" class="form-control col-md-6" name="apart_name" id="apart_name"
                    aria-describedby="helpId" placeholder="" value="<?=$data->共同住宅?>">
                </div>
                <div class="form-group">
                  <label for="phone">電話番号</label>
                  <input type="text" class="form-control col-md-6" name="phone" id="phone" aria-describedby="helpId"
                    placeholder="" value="<?=$data->電話?>">
                </div>


                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_wife">奥様名前</label>
                      <input type="text" class="form-control" name="name_wife" id="name_wife" aria-describedby="helpId" placeholder="" value="<?=$data->奥様名前;?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_wife_kana">奥様名前 (かな)</label>
                      <input type="text" class="form-control" name="name_wife_kana" id="name_wife_kana" aria-describedby="helpId" placeholder="" value="<?=$data->奥様名前_かな;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="date_birth_wife">奥様生年月日</label>
                  <input type="date" class="form-control col-md-6" name="date_birth_wife" id="date_birth_wife" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->奥様生年月日)) { echo isset($data->奥様生年月日) ? set_value('date_birth_wife', date('Y-m-d', strtotime($data->奥様生年月日))) : set_value('date_birth_wife'); }; ?>">
                </div>

                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child1">お子様1名前</label>
                      <input type="text" class="form-control" name="name_child1" id="name_child1" aria-describedby="helpId" placeholder="" value="<?=$data->お子様1名前;?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child1_kana">お子様1名前 (かな)</label>
                      <input type="text" class="form-control" name="name_child1_kana" id="name_child1_kana" aria-describedby="helpId" placeholder="" value="<?=$data->お子様1名前_かな;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="birth_child1">お子様1生年月日</label>
                  <input type="date" class="form-control col-md-6" name="birth_child1" id="birth_child1" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->お子様1生年月日)) { echo isset($data->お子様1生年月日) ? set_value('birth_child1', date('Y-m-d', strtotime($data->お子様1生年月日))) : set_value('birth_child1'); }; ?>">
                </div>

                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child2">お子様2名前</label>
                      <input type="text" class="form-control" name="name_child2" id="name_child2" aria-describedby="helpId" placeholder="" value="<?=$data->お子様2名前;?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child2_kana">お子様2名前 (かな)</label>
                      <input type="text" class="form-control" name="name_child2_kana" id="name_child2_kana" aria-describedby="helpId" placeholder="" value="<?=$data->お子様2名前_かな;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="birth_child2">お子様2生年月日</label>
                  <input type="date" class="form-control col-md-6" name="birth_child2" id="birth_child2" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->お子様2生年月日)) { echo isset($data->お子様2生年月日) ? set_value('birth_child2', date('Y-m-d', strtotime($data->お子様2生年月日))) : set_value('birth_child2'); }; ?>">
                </div>

                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child3">お子様3名前</label>
                      <input type="text" class="form-control" name="name_child3" id="name_child3" aria-describedby="helpId" placeholder="" value="<?=$data->お子様3名前;?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                      <label for="name_child3_kana">お子様3名前 (かな)</label>
                      <input type="text" class="form-control" name="name_child3_kana" id="name_child3_kana" aria-describedby="helpId" placeholder="" value="<?=$data->お子様3名前_かな;?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="birth_child3">お子様3生年月日</label>
                  <input type="date" class="form-control col-md-6" name="birth_child3" id="birth_child3" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->お子様3生年月日)) { echo isset($data->お子様3生年月日) ? set_value('birth_child3', date('Y-m-d', strtotime($data->お子様3生年月日))) : set_value('birth_child3'); }; ?>">
                </div>


                
                <div class="form-group">
                  <label for="kind">種別</label>
                  <!-- <input type="text" class="form-control col-md-6" name="kind" id="kind" aria-describedby="helpId"
                    placeholder="" value="<?=$data->種別?>"> -->
                    <select class="form-control col-md-6" name="kind">
                      <option <?php if ($data->種別 == "") {echo "selected"; } ?> disabled value="">選択してください</option>
                      <option <?php if ($data->種別 == "SOB") {echo "selected"; } ?> value="SOB">SWEET HOME</option>
                      <option <?php if ($data->種別 == "ビリーブの家") {echo "selected"; } ?> value="ビリーブの家">ビリーブの家</option>
                      <option <?php if ($data->種別 == "リフォーム工事") {echo "selected"; } ?> value="リフォーム工事">リフォーム工事</option>
                      <option <?php if ($data->種別 == "メンテナンス工事") {echo "selected"; } ?> value="メンテナンス工事">メンテナンス工事</option>
                      <option <?php if ($data->種別 == "新築工事") {echo "selected"; } ?> value="新築工事">新築工事</option>
                      <option <?php if ($data->種別 == "OB") {echo "selected"; } ?> value="OB">OB</option>
                      <option <?php if ($data->種別 == "その他") {echo "selected"; } ?> value="その他">その他</option>
                      <option <?php if ($data->種別 == "削除") {echo "selected"; } ?> value="削除">削除</option>
                    </select>
                </div>
                <div class="form-group">
                  <label for="first_contact">初回接点</label>
                  <input type="text" class="form-control col-md-6" name="first_contact" id="first_contact"
                    aria-describedby="helpId" placeholder="" value="<?=$data->初回接点?>">
                </div>
                <div class="form-group">
                  <label for="first_date">初回年月日</label>
                  <input type="date" class="form-control col-md-6" name="first_date" id="first_date"
                    aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->初回年月日)) { echo isset($data->初回年月日) ? set_value('first_date', date('Y-m-d', strtotime($data->初回年月日))) : set_value('first_date'); };?>">
                </div>
                <div class="form-group">
                  <label for="first_time_change">初回担当</label>
                  <input type="text" class="form-control col-md-6" name="first_time_change1" id="first_time_change1"
                    aria-describedby="helpId" placeholder="" value="">
                  <select class="form-control col-md-6" name="first_time_change" id="first_time_change2">
                    <option <?php if ($data->初回担当 == "") {echo "selected"; } ?> value="">選択してください</option>
                    <option <?php if ($data->初回担当 == "$data->初回担当") {echo "selected"; } ?> value="<?=$data->初回担当?>"><?=$data->初回担当?></option>
                    <?php 
                    if(!empty($dataks))
                    {
                      foreach($dataks as $row)
                      { 
                        echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                      }
                    }
                    else
                    {
                      echo '<option value="">社員データなし</option>';
                    }
                    ?>
                  </select>
                  <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                  <span class="item-text">手入力に切り替えます</span>
                </div>
                <div class="form-group">
                  <label for="pic_person">担当</label>
                  <input type="text" class="form-control col-md-6" name="pic_person1" id="pic_person1"
                    aria-describedby="helpId" placeholder="" value="">
                  <select class="form-control col-md-6" name="pic_person" id="pic_person2">
                    <option <?php if ($data->担当 == "") {echo "selected"; } ?> value="">選択してください</option>
                    <option <?php if ($data->担当 == "$data->担当") {echo "selected"; } ?> value="<?=$data->担当?>"><?=$data->担当?></option>
                    <?php 
                    if(!empty($dataks))
                    {
                      foreach($dataks as $row)
                      { 
                        echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                      }
                    }
                    else
                    {
                      echo '<option value="">社員データなし</option>';
                    }
                    ?>
                  </select>
                  <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                  <span class="item-text">手入力に切り替えます</span>
                </div>
                <div class="form-group">
                  <label for="with_or_without">土地</label>
                  <!-- <input type="text" class="form-control col-md-6" name="with_or_without" id="with_or_without"
                    aria-describedby="helpId" placeholder="" value="<?=$data->土地　有無?>"> -->
                  <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="with_or_without1" name="with_or_without" value="あり" <?php if($data->土地　有無 == "あり"){ echo "checked";}?>>
                        <label for="with_or_without1" class="form-check-label">あり</label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="with_or_without2" name="with_or_without" value="なし" <?php if($data->土地　有無 == "なし"){ echo "checked";}?>>
                        <label for="with_or_without2" class="form-check-label">なし</label>
                    </div>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="visit">訪問</label>
                  <input type="text" class="form-control col-md-6" name="visit" id="visit" aria-describedby="helpId"
                    placeholder="" value="<?=$data->訪問?>">
                </div> -->
                <div class="form-group">
                  <label for="dm">DM</label>
                  <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                    placeholder="" value="<?=$data->ＤＭ?>"> -->
                  <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="dm1" name="dm" value="〇" <?php if($data->ＤＭ == "〇") { echo "checked";}?>>
                        <label for="dm1" class="form-check-label">〇</label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="dm2" name="dm" value="△" <?php if($data->ＤＭ == "△") { echo "checked";}?>>
                        <label for="dm2" class="form-check-label">△</label>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="dm3" name="dm" value="X" <?php if($data->ＤＭ == "X") { echo "checked";}?>>
                        <label for="dm3" class="form-check-label">X</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email">メールアドレス</label>
                  <input type="text" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="" value="<?=$data->メールアドレス?>">
                </div>
                <!-- <div class="form-group">
                  <label for="rank">ランク</label>
                  <input type="text" class="form-control col-md-6" name="rank" id="rank" aria-describedby="helpId"
                    placeholder="" value="<?=$data->ランク?>">
                </div> -->
                <div class="form-group">
                  <label for="remarks">備考</label>
                  <textarea type="text" class="form-control col-md-6" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" rows="5"><?=$data->備考;?></textarea>
                </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
  $("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$('#birth_date').change(function(){
  console.log("change");
  var dob = new Date(document.getElementById('birth_date').value);
  var today = new Date();
  var age = Math.floor((today-dob)/(365.25*24*60*60*1000));
  document.getElementById('age').value = age;
});

// $('#latitude').keyup(function(){
//     var val = $(this).val();
//     if(isNaN(val)){
//          val = val.replace(/[^0-9\.]/g,'');
//          if(val.split('.').length>2) 
//              val =val.replace(/\.+$/,"");
//     }
//     $(this).val(val); 
// });

// $('#longitude').keyup(function(){
//     var val = $(this).val();
//     if(isNaN(val)){
//          val = val.replace(/[^0-9\.]/g,'');
//          if(val.split('.').length>2) 
//              val =val.replace(/\.+$/,"");
//     }
//     $(this).val(val); 
// });

// validateForm = function () {
//     return checkName();
// }

// function checkName() {
//   var latitude = document.getElementById('latitude').value;
//   var longitude = document.getElementById('longitude').value;

//   var regLat = new RegExp("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}");
//   var regLang = new RegExp("^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$");
  
//   if( regLat.exec(latitude) ) 
//     {
//     //do nothing
//     } 
//     else 
//     {
//       alert('緯度の形式が違います');
//       return false;
//     }

//     if( regLang.exec(longitude) ) 
//     {
//     //do nothing
//     } else {
//       alert('経度の形式が違います');
//       return false;
//     }

// }

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("birth_date").setAttribute("max", "9999-12-31");
document.getElementById("date_birth_wife").setAttribute("max", "9999-12-31");
document.getElementById("birth_child1").setAttribute("max", "9999-12-31");
document.getElementById("birth_child2").setAttribute("max", "9999-12-31");
document.getElementById("birth_child3").setAttribute("max", "9999-12-31");
document.getElementById("first_date").setAttribute("max", "9999-12-31");


$("#first_time_change1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#first_time_change1").show();
        $("#first_time_change2").hide();
    } else {
        $("#first_time_change1").hide();
        $("#first_time_change2").show();
    }
});

$("#pic_person1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#pic_person1").show();
        $("#pic_person2").hide();
    } else {
        $("#pic_person1").hide();
        $("#pic_person2").show();
    }
});
</script>
</body>
</html>