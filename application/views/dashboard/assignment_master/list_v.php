<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">社員マスタ一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 社員マスタ一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-striped table-inverse" id="data_customer">
                      <thead class="thead-inverse">
                        <tr>
                          <th>メインキー社員コード</th>
                          <th>メインキー開始日</th>
                          <th>終了日</th>
                          <th>部署コード</th>
                          <th>役職コード</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                          <tr>
                            <td scope="row"><?=$value->employee_code?></td>
                            <td><?=$value->start_date?></td>
                            <td><?=$value->end_date?></td>
                            <td><?=$value->department_code?></td>
                            <td><?=$value->job_code?></td>
                            <td>
                            <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_assignment_master/<?=$value->employee_code?>" role="button"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_assignment_master/<?=$value->employee_code?>" role="button"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="10">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
</body>
</html>