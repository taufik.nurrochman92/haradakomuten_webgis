<?php $this->load->view('dashboard/dashboard_header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">情報検索</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">情報検索</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-8 offset-2">
            <div class="card">
              <div class="card-body">
                <!--START-->
                <!-- <p><?=$_SESSION["access"]?></p> -->
                <form action="<?=base_url()?>dashboard/search_data" method="post" enctype="multipart/form-data">
                  <div class="row offset-2">
                    <div class="col-md-6" style="margin-bottom:15px;">
                        <input type="radio" class="form-check-input" type="checkbox" id="table1" name="table" value="harada_client_ob" checked required>
                        <label for="table1" class="form-check-label">OB顧客</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" class="form-check-input" type="checkbox" id="table2" name="table" value="harada_client"  >
                        <label for="table2" class="form-check-label">営業中顧客</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6" style="margin-bottom:15px;">
                        <label for="full_name">氏名 (漢字)</label>
                        <input type="text" class="form-control" name="full_name" id="full_name"
                          aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="col-sm-6">
                        <label for="full_name_kana">氏名（かな）</label>
                        <input type="text" class="form-control" name="full_name_kana" id="full_name_kana"
                          aria-describedby="helpId" placeholder="">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-6" style="margin-bottom:15px;">
                        <label for="address">住所 </label>
                        <input type="text" class="form-control" name="address" id="address"
                          aria-describedby="helpId" placeholder="">
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="kinds">種別</label>
                        <select class="form-control" name="kinds">
                          <option selected value="">選択してください</option>
                          <option value="SOB">SWEET HOME</option>
                          <option value="ビリーブの家">ビリーブの家</option>
                          <option value="リフォーム工事">リフォーム工事</option>
                          <option value="メンテナンス工事">メンテナンス工事</option>
                          <option value="新築工事">新築工事</option>
                          <option value="OB">OB</option>
                          <option value="その他">その他</option>
                          <option value="削除">削除</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="sales_staff">営業担当</label>
                        <input type="text" class="form-control col-md-12" name="sales_staff1" id="sales_staff1" aria-describedby="helpId" placeholder="">
                        <select class="form-control" name="sales_staff" id="sales_staff2">
                          <option value="">選択してください</option>
                          <?php 
                          if(!empty($data))
                          {
                            foreach($data as $row)
                            { 
                              echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                            }
                          }
                          else
                          {
                            echo '<option value="">社員データなし</option>';
                          }
                          ?>
                        </select>
                        <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="pic_construction">工務担当</label>
                        <input type="text" class="form-control col-md-12" name="pic_construction1" id="pic_construction1" aria-describedby="helpId" placeholder="">
                        <select class="form-control" name="pic_construction" id="pic_construction2">
                          <option value="">選択してください</option>
                          <?php 
                          if(!empty($data))
                          {
                            foreach($data as $row)
                            { 
                              echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                            }
                          }
                          else
                          {
                            echo '<option value="">社員データなし</option>';
                          }
                          ?>
                        </select>
                        <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row border-bottom mb-3">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="coordinator">コーデ担当</label>
                          <input type="text" class="form-control col-md-12" name="incharge_coordinator1" id="incharge_coordinator1" aria-describedby="helpId" placeholder="">
                          <select class="form-control" name="coordinator" id="incharge_coordinator2">
                            <option value="">選択してください</option>
                            <?php 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                          <input class="coupon_question3" type="checkbox" name="coupon_question3" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="age">年齢</label>
                        <select class="form-control" name="age">
                          <option selected value="">選択してください</option>
                          <option value="20">20代</option>
                          <option value="30">30代</option>
                          <option value="40">40代</option>
                          <option value="50">50代</option>
                          <option value="60">60代</option>
                          <option value="70">70代</option>
                          <option value="80">80代</option>
                          <option value="年代不明">年代不明</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row mb-3 offset-2">
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="thanksgiving" name="thanksgiving" value="〇"  >
                          <label for="thanksgiving">感謝祭 〇</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="dm" name="dm" value="〇"  >
                          <label for="dm">その他DM 〇</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="dm" name="dm" value="△"  >
                          <label for="dm">その他DM △</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="dm" name="dm" value="×"  >
                          <label for="dm">その他DM ×</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="longterm" name="longterm" value="1"  >
                          <label for="longterm">長期優良住宅</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="warranty" name="warranty" value="1"  >
                          <label for="warranty">設備10年保証加入</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="subsidy" name="subsidy" value="1"  >
                          <label for="subsidy">グリーン化事業補助金</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="ground_improve" name="ground_improve" value="1"  >
                          <label for="ground_improve">地盤改良工事</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="kinds">土地の有無</label>
                        <select class="form-control" name="with_or_without">
                          <option selected disabled value="">選択してください</option>
                          <option value="あり">あり</option>
                          <option value="なし">なし</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom:15px;">
                        <label for="first_contact">初回接点</label>
                        <input type="text" class="form-control" name="first_contact" id="first_contact"
                          aria-describedby="helpId" placeholder="">
                    </div>
                  </div>
                  <div class="form-row mb-3">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="bank">借入金融機関</label>
                        <select class="form-control" name="bank">
                          <option value="">選択してください</option>
                          <?php 
                          if(!empty($for_bank))
                          {
                            foreach($for_bank as $row)
                            { 
                              echo '<option value="'.$row->金融機関名.'">'.$row->金融機関名.'</option>';
                            }
                          }
                          else
                          {
                            echo '<option value="">社員データなし</option>';
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group d-flex">
                        <div class="label">
                          <label for="insurance_company">火災保険</label>
                        </div>
                        <div class="radbut" style="flex:1;">
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company1" name="insurance_company" value="自社"  >
                                <label for="insurance_company1" class="form-check-label">自社</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company2" name="insurance_company" value="他社"  >
                                <label for="insurance_company2" class="form-check-label">他社</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="input_date" class="col-sm-2 col-form-label">引渡日</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_date1" name="date_from">
                      </div>
                      ~
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_date2" name="date_to">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="input_birthdate" class="col-sm-2 col-form-label">生年月日</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_birthdate1" name="birthdate_from">
                      </div>
                      ~
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_birthdate2" name="birthdate_to">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="input_inspection" class="col-sm-2 col-form-label">定期点検</label>
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_inspection1" name="inspection_from">
                      </div>
                      ~
                      <div class="col-sm-4">
                        <input type="date" class="form-control" id="input_inspection2" name="inspection_to">
                      </div>
                    </div>
                  <input name="" id="" class="btn btn-primary" type="submit" value="検索する">
                </form>
                <!--END-->
              </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script>
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd;
  }

  if (mm < 10) {
    mm = '0' + mm;
  } 
      
  today = yyyy + '-' + mm + '-' + dd;
  document.getElementById("input_date1").setAttribute("max", "9999-12-31");
  document.getElementById("input_date2").setAttribute("max", "9999-12-31");
  document.getElementById("input_birthdate1").setAttribute("max", "9999-12-31");
  document.getElementById("input_birthdate2").setAttribute("max", "9999-12-31");
  document.getElementById("input_inspection1").setAttribute("max", "9999-12-31");
  document.getElementById("input_inspection2").setAttribute("max", "9999-12-31");

$("#sales_staff1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#sales_staff1").show();
        $("#sales_staff2").hide();
    } else {
        $("#sales_staff1").hide();
        $("#sales_staff2").show();
    }
});

$("#pic_construction1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#pic_construction1").show();
        $("#pic_construction2").hide();
    } else {
        $("#pic_construction1").hide();
        $("#pic_construction2").show();
    }
});

$("#incharge_coordinator1").hide();
$(".coupon_question3").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_coordinator1").show();
        $("#incharge_coordinator2").hide();
    } else {
        $("#incharge_coordinator1").hide();
        $("#incharge_coordinator2").show();
    }
});
</script>
</body>
</html>