<?php $this->load->view('dashboard/dashboard_header');?>
<style>
    .my-custom-scrollbar {
        position: relative;
        height: 198px;
        overflow: auto;
        margin-bottom:20px;
    }
    .table-wrapper-scroll-y {
        display: block;
    }

    table.seratus {
        height: 100%;
    }
    
    .centering {
        display: flex;
        justify-content: center;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content view_custom">
      <div class="container-fluid">
        <!-- Main row -->
        <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
        <div class="row">
          <div class="col-xl-8 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="button" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                    <?php if(!empty($data->cust_code)) :?>
                      <div class="form-group">
                          <div class="flex-box">
                              <div class="label-view">
                                  <label for="cust_code">顧客コード</label>
                              </div>
                              <div class="text-view">
                                  <p>
                                      <?=$data->cust_code?>
                                  </p>
                              </div>
                          </div>
                      </div>
                    <?php endif;
                     if(!empty($data->cust_name)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="cust_name">顧客名</label>
                            </div>
                            <div class="text-view">
                              <p><?=$data->cust_name?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->kinds)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="kinds">種別</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->kinds?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->const_no)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="const_no">工事No</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->const_no?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->remarks)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="remarks">備考</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->remarks?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->contract_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="contract_date">契約日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->contract_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->construction_start_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="construction_start_date">着工日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->construction_start_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->upper_building_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="upper_building_date">上棟日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->upper_building_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->completion_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="completion_date">竣工日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->completion_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->delivery_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="delivery_date">引渡日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->delivery_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->amount_money)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="amount_money">金額</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->amount_money?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->sales_staff)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="sales_staff">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->sales_staff?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->incharge_construction)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="incharge_construction">工務担当</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->incharge_construction?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->incharge_coordinator)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="incharge_coordinator">コーデ担当</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->incharge_coordinator?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif; 
                     if(!empty($data->contract_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約書</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                    <?php

                                    $arrEmail1 = explode(';',$data->contract_history);
                                    $arrName1 = explode(';',$data->date_insert_history1);

                                    $arrGoal1;

                                    $arrLength1 = count($arrName1);

                                    //creating two-dimensional Array
                                    for($i = 0; $i < $arrLength1; $i++){ 
                                        $arrGoal1[$i] = array($arrName1[$i], $arrEmail1[$i]);
                                        //should look something like this ((name, email),(name, email)…)
                                    }

                                    $arrGoalLength1 = count($arrGoal1);

                                    //accessing Array
                                    //1. dimension
                                    for($i = 0; $i < $arrGoalLength1; $i++) :
                                    //2. dimension
                                        //Variables should be global (but aren't)
                                        $newName1 = $arrGoal1[$i][0];
                                        $newEmail1 = $arrGoal1[$i][1];

                                    ?>
                                    <tr>
                                        <td><?=$newName1 = $arrGoal1[$i][0]?></td>
                                        <td><?=$newEmail1 = $arrGoal1[$i][1]?></td>
                                    </tr>
                                    <?php 
                                    endfor;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->contract)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="contract">契約書</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->contract?>"><?=$data->contract?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->contract_drawing1_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 1</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

                                $arrEmail2 = explode(';',$data->contract_drawing1_history);
                                $arrName2 = explode(';',$data->date_insert_history2);

                                $arrGoal2;

                                $arrLength2 = count($arrName2);

                                //creating two-dimensional Array
                                for($i = 0; $i < $arrLength2; $i++){ 
                                    $arrGoal2[$i] = array($arrName2[$i], $arrEmail2[$i]);
                                    //should look something like this ((name, email),(name, email)…)
                                }

                                $arrGoalLength2 = count($arrGoal2);

                                //accessing Array
                                //1. dimension
                                for($i = 0; $i < $arrGoalLength2; $i++) :
                                //2. dimension
                                    //Variables should be global (but aren't)
                                    $newName2 = $arrGoal2[$i][0];
                                    $newEmail2 = $arrGoal2[$i][1];

                                ?>
                                <tr>
                                    <td><?=$newName2 = $arrGoal2[$i][0]?></td>
                                    <td><?=$newEmail2 = $arrGoal2[$i][1]?></td>
                                </tr>
                                <?php 
                                endfor;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                    endif;
                     if(!empty($data->constract_drawing1)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing1">契約図面 1</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing1?>"><?=$data->constract_drawing1?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner1)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner1">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner1?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check1)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check1">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check1)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item1)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item1">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item1?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing2_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 2</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail3 = explode(';',$data->contract_drawing2_history);
$arrName3 = explode(';',$data->date_insert_history3);

$arrGoal3;

$arrLength3 = count($arrName3);

//creating two-dimensional Array
for($i = 0; $i < $arrLength3; $i++){ 
    $arrGoal3[$i] = array($arrName3[$i], $arrEmail3[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength3 = count($arrGoal3);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength3; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName3 = $arrGoal3[$i][0];
    $newEmail3 = $arrGoal3[$i][1];

?>
<tr>
    <td><?=$newName3 = $arrGoal3[$i][0]?></td>
    <td><?=$newEmail3 = $arrGoal3[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing2)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing2">契約図面 2</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing2?>"><?=$data->constract_drawing2?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner2)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner2">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner2?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check2)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check2">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check2)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item2)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item2">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item2?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing3_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 3</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail4 = explode(';',$data->contract_drawing3_history);
$arrName4 = explode(';',$data->date_insert_history4);

$arrGoal4;

$arrLength4 = count($arrName4);

//creating two-dimensional Array
for($i = 0; $i < $arrLength4; $i++){ 
    $arrGoal4[$i] = array($arrName4[$i], $arrEmail4[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength4 = count($arrGoal4);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength4; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName4 = $arrGoal4[$i][0];
    $newEmail4 = $arrGoal4[$i][1];

?>
<tr>
    <td><?=$newName4 = $arrGoal4[$i][0]?></td>
    <td><?=$newEmail4 = $arrGoal4[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing3)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing3">契約図面 3</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing3?>"><?=$data->constract_drawing3?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner3)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner3">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner3?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check3)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check3">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check3)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item3)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item3">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item3?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing4_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 4</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail5 = explode(';',$data->contract_drawing4_history);
$arrName5 = explode(';',$data->date_insert_history5);

$arrGoal5;

$arrLength5 = count($arrName5);

//creating two-dimensional Array
for($i = 0; $i < $arrLength5; $i++){ 
    $arrGoal5[$i] = array($arrName5[$i], $arrEmail5[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength5 = count($arrGoal5);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength5; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName5 = $arrGoal5[$i][0];
    $newEmail5 = $arrGoal5[$i][1];

?>
<tr>
    <td><?=$newName5 = $arrGoal5[$i][0]?></td>
    <td><?=$newEmail5 = $arrGoal5[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing4)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing4">契約図面 4</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing4?>"><?=$data->constract_drawing4?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner4)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner4">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner4?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check4)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check4">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check4)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item4)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item4">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item4?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing5_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 5</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail6 = explode(';',$data->contract_drawing5_history);
$arrName6 = explode(';',$data->date_insert_history6);

$arrGoal6;

$arrLength6 = count($arrName6);

//creating two-dimensional Array
for($i = 0; $i < $arrLength6; $i++){ 
    $arrGoal6[$i] = array($arrName6[$i], $arrEmail6[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength6 = count($arrGoal6);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength6; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName6 = $arrGoal6[$i][0];
    $newEmail6 = $arrGoal6[$i][1];

?>
<tr>
    <td><?=$newName6 = $arrGoal6[$i][0]?></td>
    <td><?=$newEmail6 = $arrGoal6[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing5)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing5">契約図面 5</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing5?>"><?=$data->constract_drawing5?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner5)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner5">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner5?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check5)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check5">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check5)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item5)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item5">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item5?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing6_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 6</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail7 = explode(';',$data->contract_drawing6_history);
$arrName7 = explode(';',$data->date_insert_history7);

$arrGoal7;

$arrLength7 = count($arrName7);

//creating two-dimensional Array
for($i = 0; $i < $arrLength7; $i++){ 
    $arrGoal7[$i] = array($arrName7[$i], $arrEmail7[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength7 = count($arrGoal7);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength7; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName7 = $arrGoal7[$i][0];
    $newEmail7 = $arrGoal7[$i][1];

?>
<tr>
    <td><?=$newName7 = $arrGoal7[$i][0]?></td>
    <td><?=$newEmail7 = $arrGoal7[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing6)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing6">契約図面 6</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing6?>"><?=$data->constract_drawing6?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner6)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner6">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner6?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check6)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check6">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check6)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item6)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item6">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item6?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing7_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 7</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail8 = explode(';',$data->contract_drawing7_history);
$arrName8 = explode(';',$data->date_insert_history8);

$arrGoal8;

$arrLength8 = count($arrName8);

//creating two-dimensional Array
for($i = 0; $i < $arrLength8; $i++){ 
    $arrGoal8[$i] = array($arrName8[$i], $arrEmail8[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength8 = count($arrGoal8);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength8; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName8 = $arrGoal8[$i][0];
    $newEmail8 = $arrGoal8[$i][1];

?>
<tr>
    <td><?=$newName8 = $arrGoal8[$i][0]?></td>
    <td><?=$newEmail8 = $arrGoal8[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing7)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing7">契約図面 7</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing7?>"><?=$data->constract_drawing7?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner7)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner7">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner7?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check7)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check7">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check7)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item7)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item7">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item7?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing8_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 8</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail9 = explode(';',$data->contract_drawing8_history);
$arrName9 = explode(';',$data->date_insert_history9);

$arrGoal9;

$arrLength9 = count($arrName9);

//creating two-dimensional Array
for($i = 0; $i < $arrLength9; $i++){ 
    $arrGoal9[$i] = array($arrName9[$i], $arrEmail9[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength9 = count($arrGoal9);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength9; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName9 = $arrGoal9[$i][0];
    $newEmail9 = $arrGoal9[$i][1];

?>
<tr>
    <td><?=$newName9 = $arrGoal9[$i][0]?></td>
    <td><?=$newEmail9 = $arrGoal9[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing8)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing8">契約図面 8</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing8?>"><?=$data->constract_drawing8?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner8)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner8">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner8?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check8)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check8">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check8)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item8)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item8">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item8?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                    if(!empty($data->contract_drawing9_history)) :
                    ?>
                        <label class="centering">ファイル履歴 契約図面 9</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                <?php

$arrEmail = explode(';',$data->contract_drawing9_history);
$arrName = explode(';',$data->date_insert_history10);

$arrGoal;

$arrLength = count($arrName);

//creating two-dimensional Array
for($i = 0; $i < $arrLength; $i++){ 
    $arrGoal[$i] = array($arrName[$i], $arrEmail[$i]);
    //should look something like this ((name, email),(name, email)…)
}

$arrGoalLength = count($arrGoal);

//accessing Array
//1. dimension
for($i = 0; $i < $arrGoalLength; $i++) :
  //2. dimension
    //Variables should be global (but aren't)
    $newName = $arrGoal[$i][0];
    $newEmail = $arrGoal[$i][1];

?>
<tr>
    <td><?=$newName = $arrGoal[$i][0]?></td>
    <td><?=$newEmail = $arrGoal[$i][1]?></td>
</tr>
<?php 
endfor;
?>
                                </tbody>
                            </table>
                        </div>
                    <?php
                        endif;
                     if(!empty($data->constract_drawing9)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="constract_drawing9">契約図面 9</label>
                            </div>
                            <div class="text-view">
                            <p><a href="<?=base_url()?>uploads/files/<?=$data->constract_drawing9?>"><?=$data->constract_drawing9?></a></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->co_owner9)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="co_owner9">共有業者</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->co_owner9?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->check9)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="check9">施主様共有</label>
                            </div>
                            <div class="text-view">
                            <p><?php if(!empty($data->check9)){ echo '共有する';}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->desc_item9)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="desc_item9">コメント</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->desc_item9?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                    <div class="button" style="margin-top:20px;">
                      <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>