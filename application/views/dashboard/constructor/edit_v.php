<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事情報変更</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事情報変更</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_construction/<?=$data->construct_id?>" method="post">
                        <input type="text" id="category_euy" name="category_name" value="" hidden>
                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード <span class="red">※必須</span></label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" value="<?=$data->cust_code?>" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')"> 
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="" value="<?=$data->cust_name?>">
                              <!-- <input type="hidden" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="" value="<?=$data->cust_name?>"> -->
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別 <span class="red">※必須</span></label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds" required oninvalid="this.setCustomValidity('選択してください')" onchange="setCustomValidity('')">
                                <option <?php if ($data->kinds == "") {echo "selected"; } ?> disabled value="">ご選択ください</option>
                                <!-- <option  if ($data->kinds == "SOB") {echo "selected"; } ?> value="SOB">Sweet Home</option>
                                <option  if ($data->kinds == "BOB") {echo "selected"; } ?> value="BOB">ビリーブの家</option>
                                <option  if ($data->kinds == "小工事") {echo "selected"; } ?> value="小工事">小工事</option>
                                <option  if ($data->kinds == "建設中") {echo "selected"; } ?> value="建設中">建設中</option>
                                <option  if ($data->kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option  if ($data->kinds == "材料販売") {echo "selected"; } ?> value="材料販売">材料販売</option>
                                <option if ($data->kinds == "その他") {echo "selected"; } ?> value="その他">その他</option>
                                <option  if ($data->kinds == "NL") {echo "selected"; } ?> value="NL">NL</option> -->

                                <option <?php if ($data->kinds == "SOB") {echo "selected"; } ?> value="SOB">SWEET HOME</option>
                                <option <?php if ($data->kinds == "ビリーブの家") {echo "selected"; } ?> value="ビリーブの家">ビリーブの家</option>
                                <option <?php if ($data->kinds == "リフォーム工事") {echo "selected"; } ?> value="リフォーム工事">リフォーム工事</option>
                                <option <?php if ($data->kinds == "メンテナンス工事") {echo "selected"; } ?> value="メンテナンス工事">メンテナンス工事</option>
                                <option <?php if ($data->kinds == "新築工事") {echo "selected"; } ?> value="新築工事">新築工事</option>
                                <option <?php if ($data->kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option <?php if ($data->kinds == "その他") {echo "selected"; } ?> value="その他">その他</option>
                                <option <?php if ($data->kinds == "削除") {echo "selected"; } ?> value="削除">削除</option>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No <span class="red">※必須</span></label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="" value="<?=$data->const_no?>" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-6 mb30" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"><?=$data->remarks?></textarea>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->contract_date)) { echo isset($data->contract_date) ? set_value('contract_date', date('Y-m-d', strtotime($data->contract_date))) : set_value('contract_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->construction_start_date)) { echo isset($data->construction_start_date) ? set_value('construction_start_date', date('Y-m-d', strtotime($data->construction_start_date))) : set_value('construction_start_date'); };?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->upper_building_date)) { echo isset($data->upper_building_date) ? set_value('upper_building_date', date('Y-m-d', strtotime($data->upper_building_date))) : set_value('upper_building_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->completion_date)) { echo isset($data->completion_date) ? set_value('completion_date', date('Y-m-d', strtotime($data->completion_date))) : set_value('completion_date'); };?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder=""value="<?php if (strtotime($data->delivery_date)) { echo isset($data->delivery_date) ? set_value('delivery_date', date('Y-m-d', strtotime($data->delivery_date))) : set_value('delivery_date'); };?>">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;" value="<?=$data->amount_money?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <input type="text" class="form-control col-md-12" name="sales_staff1" id="sales_staff1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="sales_staff" id="sales_staff2">
                                <option <?php if ($data->sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->sales_staff == "$data->sales_staff") {echo "selected"; } ?> value="<?=$data->sales_staff?>"><?=$data->sales_staff?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_construction1" id="incharge_construction1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_construction" id="incharge_construction2">
                                <option <?php if ($data->incharge_construction == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->incharge_construction == "$data->incharge_construction") {echo "selected"; } ?> value="<?=$data->incharge_construction?>"><?=$data->incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_coordinator1" id="incharge_coordinator1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_coordinator" id="incharge_coordinator2">
                                <option <?php if ($data->incharge_coordinator == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($data->incharge_coordinator == "$data->incharge_coordinator") {echo "selected"; } ?> value="<?=$data->incharge_coordinator?>"><?=$data->incharge_coordinator?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question3" type="checkbox" name="coupon_question3" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="construct_stat">工事状態</label>
                              <div class="radbut" style="flex:1;">
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat1" name="construct_stat" value="工事中" <?php if ($data->construct_stat == "" || $data->construct_stat == "工事中") {echo "checked"; } ?> >
                                      <label for="construct_stat1" class="form-check-label">工事中</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat2" name="construct_stat" value="工事終了" <?php if ($data->construct_stat == "工事終了") {echo "checked"; } ?> >
                                      <label for="construct_stat2" class="form-check-label">工事終了</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div id="accordion" class="customAccordion">

                            <!-- CATEGORY 1 -->
                            <div class="card">

                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>請負契約書・土地契約書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name == "請負契約書・土地契約書"){echo "show";} ?>" id="column1">
                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1" id="pdf1"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing1!="") echo $data->constract_drawing1; else echo"ファイルなし";?></label>        
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing1text" name="constract_drawing1text" value="<?=$data->constract_drawing1?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1">削除</button>
                                              </div>
                                              <div id="loading1"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1" name="check1" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check1)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") { echo $data->desc_item1;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2" id="pdf2"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing2!="") echo $data->constract_drawing2; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing2text" name="constract_drawing2text" value="<?=$data->constract_drawing2?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2">削除</button>
                                              </div>
                                              <div id="loading2"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2" name="check2" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check2)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") {echo $data->desc_item2;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3" id="pdf3"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing3!="") echo $data->constract_drawing3; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing3text" name="constract_drawing3text" value="<?=$data->constract_drawing3?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3">削除</button>
                                              </div>
                                              <div id="loading3"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3" name="check3" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check3)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") { echo $data->desc_item3;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4" id="pdf4"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing4!="") echo $data->constract_drawing4; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing4text" name="constract_drawing4text" value="<?=$data->constract_drawing4?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4">削除</button>
                                              </div>
                                              <div id="loading4"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4" name="check4" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check4)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") {echo $data->desc_item4;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5" id="pdf5"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing5!="") echo $data->constract_drawing5; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing5text" name="constract_drawing5text" value="<?=$data->constract_drawing5?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5">削除</button>
                                              </div>
                                              <div id="loading5"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5 class="subTitle">施主様共有</h5> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5" name="check5" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check5)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") { echo $data->desc_item5;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6" id="pdf6"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing6!="") echo $data->constract_drawing6; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing6text" name="constract_drawing6text" value="<?=$data->constract_drawing6?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6">削除</button>
                                              </div>
                                              <div id="loading6"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6 class="subTitle">施主様共有</h6> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6" name="check6" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check6)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") { echo $data->desc_item6;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7" id="pdf7"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing7!="") echo $data->constract_drawing7; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing7text" name="constract_drawing7text" value="<?=$data->constract_drawing7?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7">削除</button>
                                              </div>
                                              <div id="loading7"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7 class="subTitle">施主様共有</h7> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7" name="check7" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check7)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") {echo $data->desc_item7;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8" id="pdf8"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing8!="") echo $data->constract_drawing8; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing8text" name="constract_drawing8text" value="<?=$data->constract_drawing8?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8">削除</button>
                                              </div>
                                              <div id="loading8"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8 class="subTitle">施主様共有</h8> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8" name="check8" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check8)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") {echo $data->desc_item8;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9" id="pdf9"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing9!="") echo $data->constract_drawing9; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing9text" name="constract_drawing9text" value="<?=$data->constract_drawing9?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9">削除</button>
                                              </div>
                                              <div id="loading9"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9 class="subTitle">施主様共有</h9> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9" name="check9" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check9)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item9;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10" id="pdf10"><?php if($data->category_name == "請負契約書・土地契約書" && $data->constract_drawing10!="") echo $data->constract_drawing10; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10" name="constract_drawing10" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="請負契約書・土地契約書">
                                                <input type="hidden" class="control" id="constract_drawing10text" name="constract_drawing10text" value="<?=$data->constract_drawing10?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10">削除</button>
                                              </div>
                                              <div id="loading10"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10 class="subTitle">施主様共有</h10> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10" name="check10" data-aum="請負契約書・土地契約書" aria-describedby="helpId" <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->check10)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10" id="desc_item10" data-aum="請負契約書・土地契約書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name == "請負契約書・土地契約書") {echo $data->desc_item10;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 2 --> 
                            <div class="card">

                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>長期優良住宅認定書・性能評価書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo "show";} ?>" id="column2">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1b" id="pdf1b"><?php if($data->constract_drawing1b!="") echo $data->constract_drawing1b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1b" name="constract_drawing1b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing1btext" name="constract_drawing1btext" value="<?=$data->constract_drawing1b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1b">削除</button>
                                              </div>
                                              <div id="loading1b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1b" name="check1b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check1b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->desc_item1b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2b" id="pdf2b"><?php if($data->constract_drawing2b!="") echo $data->constract_drawing2b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2b" name="constract_drawing2b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing2btext" name="constract_drawing2btext" value="<?=$data->constract_drawing2b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2b">削除</button>
                                              </div>
                                              <div id="loading2b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2b" name="check2b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check2b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") {echo $data->desc_item2b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3b" id="pdf3b"><?php if($data->constract_drawing3b!="") echo $data->constract_drawing3b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3b" name="constract_drawing3b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing3btext" name="constract_drawing3btext" value="<?=$data->constract_drawing3b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3b">削除</button>
                                              </div>
                                              <div id="loading3b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3b" name="check3b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check3b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->desc_item3b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4b" id="pdf4b"><?php if($data->constract_drawing4b!="") echo $data->constract_drawing4b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4b" name="constract_drawing4b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing4btext" name="constract_drawing4btext" value="<?=$data->constract_drawing4b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4b">削除</button>
                                              </div>
                                              <div id="loading4b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4b" name="check4b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check4b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") {echo $data->desc_item4b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5b" id="pdf5b"><?php if($data->constract_drawing5b!="") echo $data->constract_drawing5b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5b" name="constract_drawing5b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing5btext" name="constract_drawing5btext" value="<?=$data->constract_drawing5b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5b">削除</button>
                                              </div>
                                              <div id="loading5b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5b class="subTitle">施主様共有</h5b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5b" name="check5b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check5b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->desc_item5b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6b" id="pdf6b"><?php if($data->constract_drawing6b!="") echo $data->constract_drawing6b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6b" name="constract_drawing6b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing6btext" name="constract_drawing6btext" value="<?=$data->constract_drawing6b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6b">削除</button>
                                              </div>
                                              <div id="loading6b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6b class="subTitle">施主様共有</h6b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6b" name="check6b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check6b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->desc_item6b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7b" id="pdf7b"><?php if($data->constract_drawing7b!="") echo $data->constract_drawing7b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7b" name="constract_drawing7b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing7btext" name="constract_drawing7btext" value="<?=$data->constract_drawing7b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7b">削除</button>
                                              </div>
                                              <div id="loading7b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7b class="subTitle">施主様共有</h7b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7b" name="check7b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check7b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") {echo $data->desc_item7b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8b" id="pdf8b"><?php if($data->constract_drawing8b!="") echo $data->constract_drawing8b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8b" name="constract_drawing8b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing8btext" name="constract_drawing8btext" value="<?=$data->constract_drawing8b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8b">削除</button>
                                              </div>
                                              <div id="loading8b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8b class="subTitle">施主様共有</h8b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8b" name="check8b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check8b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") {echo $data->desc_item8b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9b" id="pdf9b"><?php if($data->constract_drawing9b!="") echo $data->constract_drawing9b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9b" name="constract_drawing9b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing9btext" name="constract_drawing9btext" value="<?=$data->constract_drawing9b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9b">削除</button>
                                              </div>
                                              <div id="loading9b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9b class="subTitle">施主様共有</h9b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9b" name="check9b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check9b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item9b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10b" id="pdf10b"><?php if($data->constract_drawing10b!="") echo $data->constract_drawing10b; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10b" name="constract_drawing10b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="長期優良住宅認定書・性能評価書">
                                                <input type="hidden" class="control" id="constract_drawing10btext" name="constract_drawing10btext" value="<?=$data->constract_drawing10b?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10b">削除</button>
                                              </div>
                                              <div id="loading10b"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10b class="subTitle">施主様共有</h10b> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10b" name="check10b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" <?php if(!empty($data->check10b)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10b">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" data-aum="長期優良住宅認定書・性能評価書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書") {echo $data->desc_item10b;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 3 --> 
                            <div class="card">

                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>地盤調査・地盤改良</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1c == "地盤調査・地盤改良"){echo "show";} ?>" id="column3">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1c" id="pdf1c"><?php if($data->constract_drawing1c!="") echo $data->constract_drawing1c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1c" name="constract_drawing1c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing1ctext" name="constract_drawing1ctext" value="<?=$data->constract_drawing1c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1c">削除</button>
                                              </div>
                                              <div id="loading1c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1c" name="check1c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check1c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") { echo $data->desc_item1c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2c" id="pdf2c"><?php if($data->constract_drawing2c!="") echo $data->constract_drawing2c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2c" name="constract_drawing2c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing2ctext" name="constract_drawing2ctext" value="<?=$data->constract_drawing2c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2c">削除</button>
                                              </div>
                                              <div id="loading2c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2c" name="check2c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check2c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") {echo $data->desc_item2c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3c" id="pdf3c"><?php if($data->constract_drawing3c!="") echo $data->constract_drawing3c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3c" name="constract_drawing3c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing3ctext" name="constract_drawing3ctext" value="<?=$data->constract_drawing3c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3c">削除</button>
                                              </div>
                                              <div id="loading3c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3c" name="check3c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check3c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") { echo $data->desc_item3c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4c" id="pdf4c"><?php if($data->constract_drawing4c!="") echo $data->constract_drawing4c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4c" name="constract_drawing4c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing4ctext" name="constract_drawing4ctext" value="<?=$data->constract_drawing4c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4c">削除</button>
                                              </div>
                                              <div id="loading4c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4c" name="check4c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check4c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") {echo $data->desc_item4c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5c" id="pdf5c"><?php if($data->constract_drawing5c!="") echo $data->constract_drawing5c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5c" name="constract_drawing5c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing5ctext" name="constract_drawing5ctext" value="<?=$data->constract_drawing5c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5c">削除</button>
                                              </div>
                                              <div id="loading5c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5c class="subTitle">施主様共有</h5c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5c" name="check5c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check5c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") { echo $data->desc_item5c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6c" id="pdf6c"><?php if($data->constract_drawing6c!="") echo $data->constract_drawing6c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6c" name="constract_drawing6c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing6ctext" name="constract_drawing6ctext" value="<?=$data->constract_drawing6c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6c">削除</button>
                                              </div>
                                              <div id="loading6c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6c class="subTitle">施主様共有</h6c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6c" name="check6c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check6c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") { echo $data->desc_item6c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7c" id="pdf7c"><?php if($data->constract_drawing7c!="") echo $data->constract_drawing7c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7c" name="constract_drawing7c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing7ctext" name="constract_drawing7ctext" value="<?=$data->constract_drawing7c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7c">削除</button>
                                              </div>
                                              <div id="loading7c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7c class="subTitle">施主様共有</h7c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7c" name="check7c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check7c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") {echo $data->desc_item7c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8c" id="pdf8c"><?php if($data->constract_drawing8c!="") echo $data->constract_drawing8c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8c" name="constract_drawing8c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing8ctext" name="constract_drawing8ctext" value="<?=$data->constract_drawing8c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8c">削除</button>
                                              </div>
                                              <div id="loading8c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8c class="subTitle">施主様共有</h8c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8c" name="check8c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check8c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") {echo $data->desc_item8c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9c" id="pdf9c"><?php if($data->constract_drawing9c!="") echo $data->constract_drawing9c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9c" name="constract_drawing9c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing9ctext" name="constract_drawing9ctext" value="<?=$data->constract_drawing9c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9c">削除</button>
                                              </div>
                                              <div id="loading9c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9c class="subTitle">施主様共有</h9c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9c" name="check9c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check9c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item9c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10c" id="pdf10c"><?php if($data->constract_drawing10c!="") echo $data->constract_drawing10c; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10c" name="constract_drawing10c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="地盤調査・地盤改良">
                                                <input type="hidden" class="control" id="constract_drawing10ctext" name="constract_drawing10ctext" value="<?=$data->constract_drawing10c?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10c">削除</button>
                                              </div>
                                              <div id="loading10c"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10c class="subTitle">施主様共有</h10c> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10c" name="check10c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" <?php if(!empty($data->check10c)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10c">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" data-aum="地盤調査・地盤改良" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1c == "地盤調査・地盤改良") {echo $data->desc_item10c;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 4 --> 
                            <div class="card">

                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>点検履歴</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1d == "点検履歴"){echo "show";} ?>" id="column4">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1d" id="pdf1d"><?php if($data->constract_drawing1d!="") echo $data->constract_drawing1d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1d" name="constract_drawing1d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing1dtext" name="constract_drawing1dtext" value="<?=$data->constract_drawing1d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1d">削除</button>
                                              </div>
                                              <div id="loading1d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1d" name="check1d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check1d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") { echo $data->desc_item1d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2d" id="pdf2d"><?php if($data->constract_drawing2d!="") echo $data->constract_drawing2d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2d" name="constract_drawing2d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing2dtext" name="constract_drawing2dtext" value="<?=$data->constract_drawing1d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2d">削除</button>
                                              </div>
                                              <div id="loading2d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2d" name="check2d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check2d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") {echo $data->desc_item2d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3d" id="pdf3d"><?php if($data->constract_drawing3d!="") echo $data->constract_drawing3d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3d" name="constract_drawing3d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing3dtext" name="constract_drawing3dtext" value="<?=$data->constract_drawing3d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3d">削除</button>
                                              </div>
                                              <div id="loading3d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3d" name="check3d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check3d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId" data-aum="点検履歴" placeholder="" value="<?php if($data->category_name1d == "点検履歴") { echo $data->desc_item3d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4d" id="pdf4d"><?php if($data->constract_drawing4d!="") echo $data->constract_drawing4d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4d" name="constract_drawing4d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing4dtext" name="constract_drawing4dtext" value="<?=$data->constract_drawing4d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4d">削除</button>
                                              </div>
                                              <div id="loading4d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4d" name="check4d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check4d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") {echo $data->desc_item4d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5d" id="pdf5d"><?php if($data->constract_drawing5d!="") echo $data->constract_drawing5d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5d" name="constract_drawing5d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing5dtext" name="constract_drawing5dtext" value="<?=$data->constract_drawing5d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5d">削除</button>
                                              </div>
                                              <div id="loading5d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5d class="subTitle">施主様共有</h5d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5d" name="check5d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check5d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") { echo $data->desc_item5d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6d" id="pdf6d"><?php if($data->constract_drawing6d!="") echo $data->constract_drawing6d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6d" name="constract_drawing6d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing6dtext" name="constract_drawing6dtext" value="<?=$data->constract_drawing6d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6d">削除</button>
                                              </div>
                                              <div id="loading6d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6d class="subTitle">施主様共有</h6d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6d" name="check6d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check6d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") { echo $data->desc_item6d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7d" id="pdf7d"><?php if($data->constract_drawing7d!="") echo $data->constract_drawing7d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7d" name="constract_drawing7d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing7dtext" name="constract_drawing7dtext" value="<?=$data->constract_drawing7d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7d">削除</button>
                                              </div>
                                              <div id="loading7d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7d class="subTitle">施主様共有</h7d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7d" name="check7d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check7d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId" data-aum="点検履歴" placeholder="" value="<?php if($data->category_name1d == "点検履歴") {echo $data->desc_item7d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8d" id="pdf8d"><?php if($data->constract_drawing8d!="") echo $data->constract_drawing8d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8d" name="constract_drawing8d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing8dtext" name="constract_drawing8dtext" value="<?=$data->constract_drawing8d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8d">削除</button>
                                              </div>
                                              <div id="loading8d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8d class="subTitle">施主様共有</h8d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8d" name="check8d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check8d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") {echo $data->desc_item8d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9d" id="pdf9d"><?php if($data->constract_drawing9d!="") echo $data->constract_drawing9d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9d" name="constract_drawing9d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing9dtext" name="constract_drawing9dtext" value="<?=$data->constract_drawing9d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9d">削除</button>
                                              </div>
                                              <div id="loading9d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9d class="subTitle">施主様共有</h9d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9d" name="check9d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check9d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item9d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10d" id="pdf10d"><?php if($data->constract_drawing10d!="") echo $data->constract_drawing10d; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10d" name="constract_drawing10d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="点検履歴">
                                                <input type="hidden" class="control" id="constract_drawing10dtext" name="constract_drawing10dtext" value="<?=$data->constract_drawing10d?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10d">削除</button>
                                              </div>
                                              <div id="loading10d"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10d class="subTitle">施主様共有</h10d> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10d" name="check10d" data-aum="点検履歴" aria-describedby="helpId" <?php if(!empty($data->check10d)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10d">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" data-aum="点検履歴" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1d == "点検履歴") {echo $data->desc_item10d;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 5 --> 
                            <div class="card">

                              <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>三者引継ぎ合意書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1e == "三者引継ぎ合意書"){echo "show";} ?>" id="column5">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1e" id="pdf1e"><?php if($data->constract_drawing1e!="") echo $data->constract_drawing1e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1e" name="constract_drawing1e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing1etext" name="constract_drawing1etext" value="<?=$data->constract_drawing1e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1e">削除</button>
                                              </div>
                                              <div id="loading1e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1e" name="check1e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check1e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1e" id="desc_item1e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") { echo $data->desc_item1e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2e" id="pdf2e"><?php if($data->constract_drawing2e!="") echo $data->constract_drawing2e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2e" name="constract_drawing2e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls"data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing2etext" name="constract_drawing2etext" value="<?=$data->constract_drawing2e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2e">削除</button>
                                              </div>
                                              <div id="loading2e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2e" name="check2e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check2e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2e" id="desc_item2e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") {echo $data->desc_item2e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3e" id="pdf3e"><?php if($data->constract_drawing3e!="") echo $data->constract_drawing3e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3e" name="constract_drawing3e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls"data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing3etext" name="constract_drawing3etext" value="<?=$data->constract_drawing3e?>"> 
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3e">削除</button>
                                              </div>
                                              <div id="loading3e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3e" name="check3e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check3e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3e" id="desc_item3e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") { echo $data->desc_item3e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4e" id="pdf4e"><?php if($data->constract_drawing4e!="") echo $data->constract_drawing4e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4e" name="constract_drawing4e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls"data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing4etext" name="constract_drawing4etext" value="<?=$data->constract_drawing4e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4e">削除</button>
                                              </div>
                                              <div id="loading4e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4e" name="check4e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check4e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4e" id="desc_item4e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") {echo $data->desc_item4e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5e" id="pdf5e"><?php if($data->constract_drawing5e!="") echo $data->constract_drawing5e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5e" name="constract_drawing5e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls"data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing5etext" name="constract_drawing5etext" value="<?=$data->constract_drawing5e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5e">削除</button>
                                              </div>
                                              <div id="loading5e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5e class="subTitle">施主様共有</h5e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5e" name="check5e"data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check5e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") { echo $data->desc_item5e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6e" id="pdf6e"><?php if($data->constract_drawing6e!="") echo $data->constract_drawing6e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6e" name="constract_drawing6e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing6etext" name="constract_drawing6etext" value="<?=$data->constract_drawing6e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6e">削除</button>
                                              </div>
                                              <div id="loading6e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6e class="subTitle">施主様共有</h6e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6e" name="check6e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check6e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId" data-aum="三者引継ぎ合意書" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") { echo $data->desc_item6e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7e" id="pdf7e"><?php if($data->constract_drawing7e!="") echo $data->constract_drawing7e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7e" name="constract_drawing7e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing7etext" name="constract_drawing7etext" value="<?=$data->constract_drawing7e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7e">削除</button>
                                              </div>
                                              <div id="loading7e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7e class="subTitle">施主様共有</h7e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7e" name="check7e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check7e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") {echo $data->desc_item7e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8e" id="pdf8e"><?php if($data->constract_drawing8e!="") echo $data->constract_drawing8e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8e" name="constract_drawing8e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing8etext" name="constract_drawing8etext" value="<?=$data->constract_drawing8e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8e">削除</button>
                                              </div>
                                              <div id="loading8e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8e class="subTitle">施主様共有</h8e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8e" name="check8e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check8e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") {echo $data->desc_item8e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9e" id="pdf9e"><?php if($data->constract_drawing9e!="") echo $data->constract_drawing9e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9e" name="constract_drawing9e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing9etext" name="constract_drawing9etext" value="<?=$data->constract_drawing9e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9e">削除</button>
                                              </div>
                                              <div id="loading9e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9e class="subTitle">施主様共有</h9e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9e" name="check9e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check9e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item9e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10e" id="pdf10e"><?php if($data->constract_drawing10e!="") echo $data->constract_drawing10e; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10e" name="constract_drawing10e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="三者引継ぎ合意書">
                                                <input type="hidden" class="control" id="constract_drawing10etext" name="constract_drawing10etext" value="<?=$data->constract_drawing10e?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10e">削除</button>
                                              </div>
                                              <div id="loading10e"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10e class="subTitle">施主様共有</h10e> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10e" name="check10e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" <?php if(!empty($data->check10e)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10e">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" data-aum="三者引継ぎ合意書" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1e == "三者引継ぎ合意書") {echo $data->desc_item10e;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 6 --> 
                            <div class="card">

                              <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>住宅仕様書・プレカット資料 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo "show";} ?>" id="column6">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1f" id="pdf1f"><?php if($data->constract_drawing1f!="") echo $data->constract_drawing1f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1f" name="constract_drawing1f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing1ftext" name="constract_drawing1ftext" value="<?=$data->constract_drawing1f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1f">削除</button>
                                              </div>
                                              <div id="loading1f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1f" name="check1f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check1f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->desc_item1f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2f" id="pdf2f"><?php if($data->constract_drawing2f!="") echo $data->constract_drawing2f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2f" name="constract_drawing2f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing2ftext" name="constract_drawing2ftext" value="<?=$data->constract_drawing2f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2f">削除</button>
                                              </div>
                                              <div id="loading2f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2f" name="check2f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check2f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") {echo $data->desc_item2f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3f" id="pdf3f"><?php if($data->constract_drawing3f!="") echo $data->constract_drawing3f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3f" name="constract_drawing3f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing3ftext" name="constract_drawing3ftext" value="<?=$data->constract_drawing3f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3f">削除</button>
                                              </div>
                                              <div id="loading3f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3f" name="check3f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check3f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->desc_item3f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4f" id="pdf4f"><?php if($data->constract_drawing4f!="") echo $data->constract_drawing4f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4f" name="constract_drawing4f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing4ftext" name="constract_drawing4ftext" value="<?=$data->constract_drawing4f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4f">削除</button>
                                              </div>
                                              <div id="loading4f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4f" name="check4f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check4f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") {echo $data->desc_item4f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5f" id="pdf5f"><?php if($data->constract_drawing5f!="") echo $data->constract_drawing5f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5f" name="constract_drawing5f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing5ftext" name="constract_drawing5ftext" value="<?=$data->constract_drawing5f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5f">削除</button>
                                              </div>
                                              <div id="loading5f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5f class="subTitle">施主様共有</h5f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5f" name="check5f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check5f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->desc_item5f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6f" id="pdf6f"><?php if($data->constract_drawing6f!="") echo $data->constract_drawing6f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6f" name="constract_drawing6f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing6ftext" name="constract_drawing6ftext" value="<?=$data->constract_drawing6f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6f">削除</button>
                                              </div>
                                              <div id="loading6f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6f class="subTitle">施主様共有</h6f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6f" name="check6f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check6f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->desc_item6f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7f" id="pdf7f"><?php if($data->constract_drawing7f!="") echo $data->constract_drawing7f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7f" name="constract_drawing7f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing7ftext" name="constract_drawing7ftext" value="<?=$data->constract_drawing7f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7f">削除</button>
                                              </div>
                                              <div id="loading7f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7f class="subTitle">施主様共有</h7f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7f" name="check7f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check7f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") {echo $data->desc_item7f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8f" id="pdf8f"><?php if($data->constract_drawing8f!="") echo $data->constract_drawing8f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8f" name="constract_drawing8f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing8ftext" name="constract_drawing8ftext" value="<?=$data->constract_drawing8f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8f">削除</button>
                                              </div>
                                              <div id="loading8f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8f class="subTitle">施主様共有</h8f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8f" name="check8f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check8f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") {echo $data->desc_item8f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9f" id="pdf9f"><?php if($data->constract_drawing9f!="") echo $data->constract_drawing9f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9f" name="constract_drawing9f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing9ftext" name="constract_drawing9ftext" value="<?=$data->constract_drawing9f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9f">削除</button>
                                              </div>
                                              <div id="loading9f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9f class="subTitle">施主様共有</h9f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9f" name="check9f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check9f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item9f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10f" id="pdf10f"><?php if($data->constract_drawing10f!="") echo $data->constract_drawing10f; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10f" name="constract_drawing10f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing10ftext" name="constract_drawing10ftext" value="<?=$data->constract_drawing10f?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10f">削除</button>
                                              </div>
                                              <div id="loading10f"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10f class="subTitle">施主様共有</h10f> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10f" name="check10f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check10f)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10f">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1f == "住宅仕様書・プレカット資料") {echo $data->desc_item10f;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 7 --> 
                            <div class="card">

                              <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>工事写真 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1g == "工事写真"){echo "show";} ?>" id="column7">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1g" id="pdf1g"><?php if($data->constract_drawing1g!="") echo $data->constract_drawing1g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1g" name="constract_drawing1g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing1gtext" name="constract_drawing1gtext" value="<?=$data->constract_drawing1g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1g">削除</button>
                                              </div>
                                              <div id="loading1g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1g" name="check1g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check1g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1g == "工事写真") { echo $data->desc_item1g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2g" id="pdf2g"><?php if($data->constract_drawing2g!="") echo $data->constract_drawing2g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2g" name="constract_drawing2g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing2gtext" name="constract_drawing2gtext" value="<?=$data->constract_drawing2g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2g">削除</button>
                                              </div>
                                              <div id="loading2g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2g" name="check2g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check2g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1g == "工事写真") {echo $data->desc_item2g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3g" id="pdf3g"><?php if($data->constract_drawing3g!="") echo $data->constract_drawing3g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3g" name="constract_drawing3g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing3gtext" name="constract_drawing3gtext" value="<?=$data->constract_drawing3g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3g">削除</button>
                                              </div>
                                              <div id="loading3g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3g" data-aum="住宅仕様書・プレカット資料" name="check3g" aria-describedby="helpId" <?php if(!empty($data->check3g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1g == "工事写真") { echo $data->desc_item3g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4g" id="pdf4g"><?php if($data->constract_drawing4g!="") echo $data->constract_drawing4g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4g" data-aum="住宅仕様書・プレカット資料" name="constract_drawing4g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <input type="hidden" class="control" id="constract_drawing4gtext" name="constract_drawing4gtext" value="<?=$data->constract_drawing4g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4g">削除</button>
                                              </div>
                                              <div id="loading4g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4g" name="check4g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check4g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1g == "工事写真") {echo $data->desc_item4g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5g" id="pdf5g"><?php if($data->constract_drawing5g!="") echo $data->constract_drawing5g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5g" name="constract_drawing5g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing5gtext" name="constract_drawing5gtext" value="<?=$data->constract_drawing5g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5g">削除</button>
                                              </div>
                                              <div id="loading5g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h5g class="subTitle">施主様共有</h5g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5g" name="check5g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check5g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1g == "工事写真") { echo $data->desc_item5g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6g" id="pdf6g"><?php if($data->constract_drawing6g!="") echo $data->constract_drawing6g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6g" name="constract_drawing6g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing6gtext" name="constract_drawing6gtext" value="<?=$data->constract_drawing6g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6g">削除</button>
                                              </div>
                                              <div id="loading6g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h6g class="subTitle">施主様共有</h6g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6g" name="check6g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check6g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1g == "工事写真") { echo $data->desc_item6g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7g" id="pdf7g"><?php if($data->constract_drawing7g!="") echo $data->constract_drawing7g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7g" name="constract_drawing7g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing7gtext" name="constract_drawing7gtext" value="<?=$data->constract_drawing7g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7g">削除</button>
                                              </div>
                                              <div id="loading7g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h7g class="subTitle">施主様共有</h7g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7g" name="check7g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check7g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1g == "工事写真") {echo $data->desc_item7g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8g" id="pdf8g"><?php if($data->constract_drawing8g!="") echo $data->constract_drawing8g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8g" name="constract_drawing8g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing8gtext" name="constract_drawing8gtext" value="<?=$data->constract_drawing8g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8g">削除</button>
                                              </div>
                                              <div id="loading8g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h8g class="subTitle">施主様共有</h8g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8g" name="check8g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check8g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId" data-aum="住宅仕様書・プレカット資料" placeholder="" value="<?php if($data->category_name1g == "工事写真") {echo $data->desc_item8g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9g" id="pdf9g"><?php if($data->constract_drawing9g!="") echo $data->constract_drawing9g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9g" name="constract_drawing9g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing9gtext" name="constract_drawing9gtext" value="<?=$data->constract_drawing9g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9g">削除</button>
                                              </div>
                                              <div id="loading9g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h9g class="subTitle">施主様共有</h9g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9g" name="check9g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check9g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1g == "工事写真"){ echo $data->desc_item9g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10g" id="pdf10g"><?php if($data->constract_drawing10g!="") echo $data->constract_drawing10g; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10g" name="constract_drawing10g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="住宅仕様書・プレカット資料">
                                                <input type="hidden" class="control" id="constract_drawing10gtext" name="constract_drawing10gtext" value="<?=$data->constract_drawing10g?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10g">削除</button>
                                              </div>
                                              <div id="loading10g"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h10g class="subTitle">施主様共有</h10g> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10g" name="check10g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" <?php if(!empty($data->check10g)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10g">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" data-aum="住宅仕様書・プレカット資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1g == "工事写真") {echo $data->desc_item10g;}?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </div>

                            <!-- CATEGORY 1 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingOneBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>確認申請書一式 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1bluea == "確認申請書一式"){echo "show";} ?>" id="column1blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluea" id="pdf1bluea"><?php if($data->constract_drawing1bluea!="") echo $data->constract_drawing1bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluea" name="constract_drawing1bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing1blueatext" name="constract_drawing1blueatext" value="<?=$data->constract_drawing1bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1bluea">削除</button>
                                              </div>
                                              <div id="loading1bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluea" name="co_owner1bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1bluea)) {
                                                $selected_array = explode(';',$data->co_owner1bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address" name="duplicate-address">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluea" name="check1bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check1bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluea" data-aum="確認申請書一式" id="desc_item1bluea" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item1bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluea" id="pdf2bluea"><?php if($data->constract_drawing2bluea!="") echo $data->constract_drawing2bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluea" name="constract_drawing2bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing2blueatext" name="constract_drawing2blueatext" value="<?=$data->constract_drawing2bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2bluea">削除</button>
                                              </div>
                                              <div id="loading2bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluea" name="co_owner2bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2bluea)) {
                                                $selected_array = explode(';',$data->co_owner2bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluea" name="check2bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if($data->category_name1bluea == "確認申請書一式" &&!empty($data->check2bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item2bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluea" id="pdf3bluea"><?php if($data->constract_drawing3bluea!="") echo $data->constract_drawing3bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluea" name="constract_drawing3bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing3blueatext" name="constract_drawing3blueatext" value="<?=$data->constract_drawing3bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3bluea">削除</button>
                                              </div>
                                              <div id="loading3bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluea" name="co_owner3bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3bluea)) {
                                                $selected_array = explode(';',$data->co_owner3bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluea" name="check3bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check3bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item3bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluea" id="pdf4bluea"><?php if($data->constract_drawing4bluea!="") echo $data->constract_drawing4bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluea" name="constract_drawing4bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing4blueatext" name="constract_drawing4blueatext" value="<?=$data->constract_drawing4bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4bluea">削除</button>
                                              </div>
                                              <div id="loading4bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluea" name="co_owner4bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4bluea)) {
                                                $selected_array = explode(';',$data->co_owner4bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluea" name="check4bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check4bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item4bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluea" id="pdf5bluea"><?php if($data->constract_drawing5bluea!="") echo $data->constract_drawing5bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluea" name="constract_drawing5bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing5blueatext" name="constract_drawing5blueatext" value="<?=$data->constract_drawing5bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5bluea">削除</button>
                                              </div>
                                              <div id="loading5bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluea" name="co_owner5bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5bluea)) {
                                                $selected_array = explode(';',$data->co_owner5bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluea" name="check5bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check5bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluea" data-aum="確認申請書一式" id="desc_item5bluea" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item5bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluea" id="pdf6bluea"><?php if($data->constract_drawing6bluea!="") echo $data->constract_drawing6bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluea" name="constract_drawing6bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing6blueatext" name="constract_drawing6blueatext" value="<?=$data->constract_drawing6bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6bluea">削除</button>
                                              </div>
                                              <div id="loading6bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluea" name="co_owner6bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6bluea)) {
                                                $selected_array = explode(';',$data->co_owner6bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluea" name="check6bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check6bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item6bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluea" id="pdf7bluea"><?php if($data->constract_drawing7bluea!="") echo $data->constract_drawing7bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluea" name="constract_drawing7bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing7blueatext" name="constract_drawing7blueatext" value="<?=$data->constract_drawing7bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7bluea">削除</button>
                                              </div>
                                              <div id="loading7bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluea" name="co_owner7bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7bluea)) {
                                                $selected_array = explode(';',$data->co_owner7bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluea" name="check7bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check7bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item7bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluea" id="pdf8bluea"><?php if($data->constract_drawing8bluea!="") echo $data->constract_drawing8bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluea" name="constract_drawing8bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing8blueatext" name="constract_drawing8blueatext" value="<?=$data->constract_drawing8bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8bluea">削除</button>
                                              </div>
                                              <div id="loading8bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluea" name="co_owner8bluea[]" multiple data-live-search="true" data-aum="確認申請書一式" data-none-selected-text="ご選択ください">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8bluea)) {
                                                $selected_array = explode(';',$data->co_owner8bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluea" name="check8bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check8bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item8bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluea" id="pdf9bluea"><?php if($data->constract_drawing9bluea!="") echo $data->constract_drawing9bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluea" name="constract_drawing9bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing9blueatext" name="constract_drawing9blueatext" value="<?=$data->constract_drawing9bluea?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9bluea">削除</button>
                                              </div>
                                              <div id="loading9bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluea" name="co_owner9bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9bluea)) {
                                                $selected_array = explode(';',$data->co_owner9bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluea" name="check9bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check9bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item9bluea;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluea" id="pdf10bluea"><?php if($data->constract_drawing10bluea!="") echo $data->constract_drawing10bluea; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluea" name="constract_drawing10bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="確認申請書一式">
                                                <input type="hidden" class="control" id="constract_drawing10blueatext" name="constract_drawing10blueatext" value="<?=$data->constract_drawing10bluea?>"> 
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10bluea">削除</button>
                                              </div>
                                              <div id="loading10bluea"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluea" name="co_owner10bluea[]" multiple style="width:100%;" data-aum="確認申請書一式">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10bluea)) {
                                                $selected_array = explode(';',$data->co_owner10bluea);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluea" name="check10bluea" data-aum="確認申請書一式" aria-describedby="helpId" <?php if(!empty($data->check10bluea)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10bluea">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" data-aum="確認申請書一式" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluea == "確認申請書一式") echo $data->desc_item10bluea?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 2 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingTwoBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>工事工程表 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1blueb == "工事工程表"){echo "show";} ?>" id="column2blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1blueb" id="pdf1blueb"><?php if($data->constract_drawing1blueb!="") echo $data->constract_drawing1blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1blueb" name="constract_drawing1blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing1bluebtext" name="constract_drawing1bluebtext" value="<?=$data->constract_drawing1blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1blueb">削除</button>
                                              </div>
                                              <div id="loading1blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueb" name="co_owner1blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1blueb)) {
                                                $selected_array = explode(';',$data->co_owner1blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address2" name="duplicate-address2">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueb" name="check1blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check1blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId" data-aum="工事工程表" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item1blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2blueb" id="pdf2blueb"><?php if($data->constract_drawing2blueb!="") echo $data->constract_drawing2blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2blueb" name="constract_drawing2blueb" data-aum="工事工程表" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                                <input type="hidden" class="control" id="constract_drawing2bluebtext" name="constract_drawing2bluebtext" value="<?=$data->constract_drawing2blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2blueb">削除</button>
                                              </div>
                                              <div id="loading2blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueb" name="co_owner2blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2blueb)) {
                                                $selected_array = explode(';',$data->co_owner2blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueb" name="check2blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if($data->category_name1blueb == "工事工程表" &&!empty($data->check2blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId" data-aum="工事工程表" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item2blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3blueb" id="pdf3blueb"><?php if($data->constract_drawing3blueb!="") echo $data->constract_drawing3blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3blueb" name="constract_drawing3blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing3bluebtext" name="constract_drawing3bluebtext" value="<?=$data->constract_drawing3blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3blueb">削除</button>
                                              </div>
                                              <div id="loading3blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueb" name="co_owner3blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3blueb)) {
                                                $selected_array = explode(';',$data->co_owner3blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }   
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueb" name="check3blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check3blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item3blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4blueb" id="pdf4blueb"><?php if($data->constract_drawing4blueb!="") echo $data->constract_drawing4blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4blueb" name="constract_drawing4blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing4bluebtext" name="constract_drawing4bluebtext" value="<?=$data->constract_drawing4blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4blueb">削除</button>
                                              </div>
                                              <div id="loading4blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueb" name="co_owner4blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4blueb)) {
                                                $selected_array = explode(';',$data->co_owner4blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueb" name="check4blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check4blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item4blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5blueb" id="pdf5blueb"><?php if($data->constract_drawing5blueb!="") echo $data->constract_drawing5blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5blueb" name="constract_drawing5blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing5bluebtext" name="constract_drawing5bluebtext" value="<?=$data->constract_drawing5blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5blueb">削除</button>
                                              </div>
                                              <div id="loading5blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueb" name="co_owner5blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5blueb)) {
                                                $selected_array = explode(';',$data->co_owner5blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueb" name="check5blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check5blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item5blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6blueb" id="pdf6blueb"><?php if($data->constract_drawing6blueb!="") echo $data->constract_drawing6blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6blueb" name="constract_drawing6blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing6bluebtext" name="constract_drawing6bluebtext" value="<?=$data->constract_drawing6blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6blueb">削除</button>
                                              </div>
                                              <div id="loading6blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueb" name="co_owner6blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6blueb)) {
                                                $selected_array = explode(';',$data->co_owner6blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueb" name="check6blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check6blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item6blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7blueb" id="pdf7blueb"><?php if($data->constract_drawing7blueb!="") echo $data->constract_drawing7blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7blueb" name="constract_drawing7blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing7bluebtext" name="constract_drawing7bluebtext" value="<?=$data->constract_drawing7blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7blueb">削除</button>
                                              </div>
                                              <div id="loading7blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueb" name="co_owner7blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7blueb)) {
                                                $selected_array = explode(';',$data->co_owner7blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueb" name="check7blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check7blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item7blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8blueb" id="pdf8blueb"><?php if($data->constract_drawing8blueb!="") echo $data->constract_drawing8blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8blueb" name="constract_drawing8blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing8bluebtext" name="constract_drawing8bluebtext" value="<?=$data->constract_drawing8blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8blueb">削除</button>
                                              </div>
                                              <div id="loading8blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueb" name="co_owner8blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8blueb)) {
                                                $selected_array = explode(';',$data->co_owner8blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueb" name="check8blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check8blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item8blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9blueb" id="pdf9blueb"><?php if($data->constract_drawing9blueb!="") echo $data->constract_drawing9blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9blueb" name="constract_drawing9blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing9bluebtext" name="constract_drawing9bluebtext" value="<?=$data->constract_drawing9blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9blueb">削除</button>
                                              </div>
                                              <div id="loading9blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueb" name="co_owner9blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9blueb)) {
                                                $selected_array = explode(';',$data->co_owner9blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueb" name="check9blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check9blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item9blueb;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10blueb" id="pdf10blueb"><?php if($data->constract_drawing10blueb!="") echo $data->constract_drawing10blueb; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10blueb" name="constract_drawing10blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事工程表">
                                                <input type="hidden" class="control" id="constract_drawing10bluebtext" name="constract_drawing10bluebtext" value="<?=$data->constract_drawing10blueb?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10blueb">削除</button>
                                              </div>
                                              <div id="loading10blueb"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueb" name="co_owner10blueb[]" multiple style="width:100%;" data-aum="工事工程表">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10blueb)) {
                                                $selected_array = explode(';',$data->co_owner10blueb);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueb" name="check10blueb" data-aum="工事工程表" aria-describedby="helpId" <?php if(!empty($data->check10blueb)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10blueb">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" data-aum="工事工程表" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueb == "工事工程表") echo $data->desc_item10blueb?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 3 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingThreeBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>工事着工資料  </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1bluec == "工事着工資料"){echo "show";} ?>" id="column3blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluec" id="pdf1bluec"><?php if($data->constract_drawing1bluec!="") echo $data->constract_drawing1bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluec" name="constract_drawing1bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing1bluectext" name="constract_drawing1bluectext" value="<?=$data->constract_drawing1bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1bluec">削除</button>
                                              </div>
                                              <div id="loading1bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluec" name="co_owner1bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1bluec)) {
                                                $selected_array = explode(';',$data->co_owner1bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address3" name="duplicate-address3">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluec" name="check1bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check1bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item1bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluec" id="pdf2bluec"><?php if($data->constract_drawing2bluec!="") echo $data->constract_drawing2bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluec" name="constract_drawing2bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing2bluectext" name="constract_drawing2bluectext" value="<?=$data->constract_drawing2bluec?>"> 
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2bluec">削除</button>
                                              </div>
                                              <div id="loading2bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluec" name="co_owner2bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2bluec)) {
                                                $selected_array = explode(';',$data->co_owner2bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluec" name="check2bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if($data->category_name1bluec == "工事着工資料" &&!empty($data->check2bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item2bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluec" id="pdf3bluec"><?php if($data->constract_drawing3bluec!="") echo $data->constract_drawing3bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluec" name="constract_drawing3bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing3bluectext" name="constract_drawing3bluectext" value="<?=$data->constract_drawing3bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3bluec">削除</button>
                                              </div>
                                              <div id="loading3bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluec" name="co_owner3bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3bluec)) {
                                                $selected_array = explode(';',$data->co_owner3bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluec" name="check3bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check3bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId" data-aum="工事着工資料" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item3bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluec" id="pdf4bluec"><?php if($data->constract_drawing4bluec!="") echo $data->constract_drawing4bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluec" name="constract_drawing4bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing4bluectext" name="constract_drawing4bluectext" value="<?=$data->constract_drawing4bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4bluec">削除</button>
                                              </div>
                                              <div id="loading4bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluec" name="co_owner4bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4bluec)) {
                                                $selected_array = explode(';',$data->co_owner4bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluec" name="check4bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check4bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item4bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluec" id="pdf5bluec"><?php if($data->constract_drawing5bluec!="") echo $data->constract_drawing5bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluec" name="constract_drawing5bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing5bluectext" name="constract_drawing5bluectext" value="<?=$data->constract_drawing5bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5bluec">削除</button>
                                              </div>
                                              <div id="loading5bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluec" name="co_owner5bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5bluec)) {
                                                $selected_array = explode(';',$data->co_owner5bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluec" name="check5bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check5bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item5bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluec" id="pdf6bluec"><?php if($data->constract_drawing6bluec!="") echo $data->constract_drawing6bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluec" name="constract_drawing6bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing6bluectext" name="constract_drawing6bluectext" value="<?=$data->constract_drawing6bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6bluec">削除</button>
                                              </div>
                                              <div id="loading6bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluec" name="co_owner6bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6bluec)) {
                                                $selected_array = explode(';',$data->co_owner6bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluec" name="check6bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check6bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item6bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluec" id="pdf7bluec"><?php if($data->constract_drawing7bluec!="") echo $data->constract_drawing7bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluec" name="constract_drawing7bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing7bluectext" name="constract_drawing7bluectext" value="<?=$data->constract_drawing7bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7bluec">削除</button>
                                              </div>
                                              <div id="loading7bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluec" name="co_owner7bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7bluec)) {
                                                $selected_array = explode(';',$data->co_owner7bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluec" name="check7bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check7bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item7bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluec" id="pdf8bluec"><?php if($data->constract_drawing8bluec!="") echo $data->constract_drawing8bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluec" name="constract_drawing8bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing8bluectext" name="constract_drawing8bluectext" value="<?=$data->constract_drawing8bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8bluec">削除</button>
                                              </div>
                                              <div id="loading8bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluec" name="co_owner8bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8bluec)) {
                                                $selected_array = explode(';',$data->co_owner8bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluec" name="check8bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check8bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item8bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluec" id="pdf9bluec"><?php if($data->constract_drawing9bluec!="") echo $data->constract_drawing9bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluec" name="constract_drawing9bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing9bluectext" name="constract_drawing9bluectext" value="<?=$data->constract_drawing9bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9bluec">削除</button>
                                              </div>
                                              <div id="loading9bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluec" name="co_owner9bluec[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9bluec)) {
                                                $selected_array = explode(';',$data->co_owner9bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluec" data-aum="工事着工資料" name="check9bluec" aria-describedby="helpId" <?php if(!empty($data->check9bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item9bluec;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluec" id="pdf10bluec"><?php if($data->constract_drawing10bluec!="") echo $data->constract_drawing10bluec; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluec" name="constract_drawing10bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="工事着工資料">
                                                <input type="hidden" class="control" id="constract_drawing10bluectext" name="constract_drawing10bluectext" value="<?=$data->constract_drawing10bluec?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10bluec">削除</button>
                                              </div>
                                              <div id="loading10bluec"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluec" name="co_owner10blue[]" multiple style="width:100%;" data-aum="工事着工資料">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10bluec)) {
                                                $selected_array = explode(';',$data->co_owner10bluec);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluec" name="check10bluec" data-aum="工事着工資料" aria-describedby="helpId" <?php if(!empty($data->check10bluec)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10bluec">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" data-aum="工事着工資料" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluec == "工事着工資料") echo $data->desc_item10bluec?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 4 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingFourBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>施工図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1blued == "施工図面"){echo "show";} ?>" id="column4blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1blued" id="pdf1blued"><?php if($data->constract_drawing1blued!="") echo $data->constract_drawing1blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1blued" name="constract_drawing1blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing1bluedtext" name="constract_drawing1bluedtext" value="<?=$data->constract_drawing1blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1blued">削除</button>
                                              </div>
                                              <div id="loading1blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blued" name="co_owner1blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1blued)) {
                                                $selected_array = explode(';',$data->co_owner1blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address4" name="duplicate-address4">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blued" name="check1blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check1blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item1blued?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2blued" id="pdf2blued"><?php if($data->constract_drawing2blued!="") echo $data->constract_drawing2blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2blued" name="constract_drawing2blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing2bluedtext" name="constract_drawing2bluedtext" value="<?=$data->constract_drawing2blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2blued">削除</button>
                                              </div>
                                              <div id="loading2blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blued" name="co_owner2blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2blued)) {
                                                $selected_array = explode(';',$data->co_owner2blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blued" name="check2blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check2blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item2blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3blued" id="pdf3blued"><?php if($data->constract_drawing3blued!="") echo $data->constract_drawing3blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3blued" name="constract_drawing3blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing3bluedtext" name="constract_drawing3bluedtext" value="<?=$data->constract_drawing3blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3blued">削除</button>
                                              </div>
                                              <div id="loading3blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blued" name="co_owner3blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3blued)) {
                                                $selected_array = explode(';',$data->co_owner3blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blued" name="check3blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check3blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item3blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4blued" id="pdf4blued"><?php if($data->constract_drawing4blued!="") echo $data->constract_drawing4blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4blued" name="constract_drawing4blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing4bluedtext" name="constract_drawing4bluedtext" value="<?=$data->constract_drawing4blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4blued">削除</button>
                                              </div>
                                              <div id="loading4blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blued" name="co_owner4blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4blued)) {
                                                $selected_array = explode(';',$data->co_owner4blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blued" name="check4blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check4blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item4blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5blued" id="pdf5blued"><?php if($data->constract_drawing5blued!="") echo $data->constract_drawing5blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5blued" name="constract_drawing5blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing5bluedtext" name="constract_drawing5bluedtext" value="<?=$data->constract_drawing5blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5blued">削除</button>
                                              </div>
                                              <div id="loading5blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blued" name="co_owner5blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5blued)) {
                                                $selected_array = explode(';',$data->co_owner5blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blued" name="check5blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check5blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item5blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6blued" id="pdf6blued"><?php if($data->constract_drawing6blued!="") echo $data->constract_drawing6blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6blued" name="constract_drawing6blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing6bluedtext" name="constract_drawing6bluedtext" value="<?=$data->constract_drawing6blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6blued">削除</button>
                                              </div>
                                              <div id="loading6blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blued" name="co_owner6blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6blued)) {
                                                $selected_array = explode(';',$data->co_owner6blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blued" name="check6blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check6blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item6blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7blued" id="pdf7blued"><?php if($data->constract_drawing7blued!="") echo $data->constract_drawing7blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7blued" name="constract_drawing7blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing7bluedtext" name="constract_drawing7bluedtext" value="<?=$data->constract_drawing7blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7blued">削除</button>
                                              </div>
                                              <div id="loading7blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blued" name="co_owner7blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7blued)) {
                                                $selected_array = explode(';',$data->co_owner7blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blued" name="check7blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check7blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item7blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8blued" id="pdf8blued"><?php if($data->constract_drawing8blued!="") echo $data->constract_drawing8blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8blued" name="constract_drawing8blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing8bluedtext" name="constract_drawing8bluedtext" value="<?=$data->constract_drawing8blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8blued">削除</button>
                                              </div>
                                              <div id="loading8blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blued" name="co_owner8blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8blued)) {
                                                $selected_array = explode(';',$data->co_owner8blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blued" name="check8blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check8blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item8blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9blued" id="pdf9blued"><?php if($data->constract_drawing9blued!="") echo $data->constract_drawing9blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9blued" name="constract_drawing9blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing9bluedtext" name="constract_drawing9bluedtext" value="<?=$data->constract_drawing9blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9blued">削除</button>
                                              </div>
                                              <div id="loading9blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blued" name="co_owner9blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9blued)) {
                                                $selected_array = explode(';',$data->co_owner9blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blued" name="check9blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check9blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item9blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10blued" id="pdf10blued"><?php if($data->constract_drawing10blued!="") echo $data->constract_drawing10blued; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10blued" name="constract_drawing10blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="施工図面">
                                                <input type="hidden" class="control" id="constract_drawing10bluedtext" name="constract_drawing10bluedtext" value="<?=$data->constract_drawing10blued?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10blued">削除</button>
                                              </div>
                                              <div id="loading10blued"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blued" name="co_owner10blued[]" multiple style="width:100%;" data-aum="施工図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10blued)) {
                                                $selected_array = explode(';',$data->co_owner10blued);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blued" name="check10blued" data-aum="施工図面" aria-describedby="helpId" <?php if(!empty($data->check10blued)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10blued">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" data-aum="施工図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blued == "施工図面") echo $data->desc_item10blued;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 5 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingFiveBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>変更図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1bluee == "変更図面"){echo "show";} ?>" id="column5blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluee" id="pdf1bluee"><?php if($data->constract_drawing1bluee!="") echo $data->constract_drawing1bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluee" name="constract_drawing1bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing1blueetext" name="constract_drawing1blueetext" value="<?=$data->constract_drawing1bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1bluee">削除</button>
                                              </div>
                                              <div id="loading1bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluee" name="co_owner1bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1bluee)) {
                                                $selected_array = explode(';',$data->co_owner1bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address5" name="duplicate-address5">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluee" name="check1bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check1bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item1bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluee" id="pdf2bluee"><?php if($data->constract_drawing2bluee!="") echo $data->constract_drawing2bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluee" name="constract_drawing2bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing2blueetext" name="constract_drawing2blueetext" value="<?=$data->constract_drawing2bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2bluee">削除</button>
                                              </div>
                                              <div id="loading2bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluee" name="co_owner2bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2bluee)) {
                                                $selected_array = explode(';',$data->co_owner2bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluee" name="check2bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check2bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item2bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluee" id="pdf3bluee"><?php if($data->constract_drawing3bluee!="") echo $data->constract_drawing3bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluee" name="constract_drawing3bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing3blueetext" name="constract_drawing3blueetext" value="<?=$data->constract_drawing3bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3bluee">削除</button>
                                              </div>
                                              <div id="loading3bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluee" name="co_owner3bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3bluee)) {
                                                $selected_array = explode(';',$data->co_owner3bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluee" name="check3bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check3bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item3bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluee" id="pdf4bluee"><?php if($data->constract_drawing4bluee!="") echo $data->constract_drawing4bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluee" name="constract_drawing4bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing4blueetext" name="constract_drawing4blueetext" value="<?=$data->constract_drawing4bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4bluee">削除</button>
                                              </div>
                                              <div id="loading4bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluee" name="co_owner4bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4bluee)) {
                                                $selected_array = explode(';',$data->co_owner4bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluee" name="check4bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check4bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item4bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluee" id="pdf5bluee"><?php if($data->constract_drawing5bluee!="") echo $data->constract_drawing5bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluee" name="constract_drawing5bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing5blueetext" name="constract_drawing5blueetext" value="<?=$data->constract_drawing5bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5bluee">削除</button>
                                              </div>
                                              <div id="loading5bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluee" name="co_owner5bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5bluee)) {
                                                $selected_array = explode(';',$data->co_owner5bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluee" name="check5bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check5bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item5bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluee" id="pdf6bluee"><?php if($data->constract_drawing6bluee!="") echo $data->constract_drawing6bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluee" name="constract_drawing6bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing6blueetext" name="constract_drawing6blueetext" value="<?=$data->constract_drawing6bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6bluee">削除</button>
                                              </div>
                                              <div id="loading6bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluee" name="co_owner6bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6bluee)) {
                                                $selected_array = explode(';',$data->co_owner6bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluee" name="check6bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check6bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item6bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluee" id="pdf7bluee"><?php if($data->constract_drawing7bluee!="") echo $data->constract_drawing7bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluee" name="constract_drawing7bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing7blueetext" name="constract_drawing7blueetext" value="<?=$data->constract_drawing7bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7bluee">削除</button>
                                              </div>
                                              <div id="loading7bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluee" name="co_owner7bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7bluee)) {
                                                $selected_array = explode(';',$data->co_owner7bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluee" name="check7bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check7bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item7bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluee" id="pdf8bluee"><?php if($data->constract_drawing8bluee!="") echo $data->constract_drawing8bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluee" name="constract_drawing8bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing8blueetext" name="constract_drawing8blueetext" value="<?=$data->constract_drawing8bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8bluee">削除</button>
                                              </div>
                                              <div id="loading8bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluee" name="co_owner8bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8bluee)) {
                                                $selected_array = explode(';',$data->co_owner8bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluee" name="check8bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check8bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item8bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluee" id="pdf9bluee"><?php if($data->constract_drawing9bluee!="") echo $data->constract_drawing9bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluee" name="constract_drawing9bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing9blueetext" name="constract_drawing9blueetext" value="<?=$data->constract_drawing9bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9bluee">削除</button>
                                              </div>
                                              <div id="loading9bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluee" name="co_owner9bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9bluee)) {
                                                $selected_array = explode(';',$data->co_owner9bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluee" name="check9bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check9bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item9bluee;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluee" id="pdf10bluee"><?php if($data->constract_drawing10bluee!="") echo $data->constract_drawing10bluee; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluee" name="constract_drawing10bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="変更図面">
                                                <input type="hidden" class="control" id="constract_drawing10blueetext" name="constract_drawing10blueetext" value="<?=$data->constract_drawing10bluee?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10bluee">削除</button>
                                              </div>
                                              <div id="loading10bluee"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluee" name="co_owner10bluee[]" multiple style="width:100%;" data-aum="変更図面">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10bluee)) {
                                                $selected_array = explode(';',$data->co_owner10bluee);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluee" name="check10bluee" data-aum="変更図面" aria-describedby="helpId" <?php if(!empty($data->check10bluee)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10bluee">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" data-aum="変更図面" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluee == "変更図面") echo $data->desc_item10bluee?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            <!-- CATEGORY 6 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingSixBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>設備プランシート</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1bluef == "設備プランシート"){echo "show";} ?>" id="column6blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1bluef" id="pdf1bluef"><?php if($data->constract_drawing1bluef!="") echo $data->constract_drawing1bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1bluef" name="constract_drawing1bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing1blueftext" name="constract_drawing1blueftext" value="<?=$data->constract_drawing1bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1bluef">削除</button>
                                              </div>
                                              <div id="loading1bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluef" name="co_owner1bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1bluef)) {
                                                $selected_array = explode(';',$data->co_owner1bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address6" name="duplicate-address6">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluef" name="check1bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check1bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item1bluef?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2bluef" id="pdf2bluef"><?php if($data->constract_drawing2bluef!="") echo $data->constract_drawing2bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2bluef" name="constract_drawing2bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing2blueftext" name="constract_drawing2blueftext" value="<?=$data->constract_drawing2bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2bluef">削除</button>
                                              </div>
                                              <div id="loading2bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluef" name="co_owner2bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2bluef)) {
                                                $selected_array = explode(';',$data->co_owner2bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluef" name="check2bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check2bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item2bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3bluef" id="pdf3bluef"><?php if($data->constract_drawing3bluef!="") echo $data->constract_drawing3bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3bluef" name="constract_drawing3bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing3blueftext" name="constract_drawing3blueftext" value="<?=$data->constract_drawing3bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3bluef">削除</button>
                                              </div>
                                              <div id="loading3bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluef" name="co_owner3bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3bluef)) {
                                                $selected_array = explode(';',$data->co_owner3bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluef" name="check3bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check3bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item3bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4bluef" id="pdf4bluef"><?php if($data->constract_drawing4bluef!="") echo $data->constract_drawing4bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4bluef" name="constract_drawing4bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing4blueftext" name="constract_drawing4blueftext" value="<?=$data->constract_drawing4bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4bluef">削除</button>
                                              </div>
                                              <div id="loading4bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluef" name="co_owner4bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4bluef)) {
                                                $selected_array = explode(';',$data->co_owner4bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluef" name="check4bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check4bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item4bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5bluef" id="pdf5bluef"><?php if($data->constract_drawing5bluef!="") echo $data->constract_drawing5bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5bluef" name="constract_drawing5bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing5blueftext" name="constract_drawing5blueftext" value="<?=$data->constract_drawing5bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5bluef">削除</button>
                                              </div>
                                              <div id="loading5bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluef" name="co_owner5bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5bluef)) {
                                                $selected_array = explode(';',$data->co_owner5bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluef" name="check5bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check5bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item5bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6bluef" id="pdf6bluef"><?php if($data->constract_drawing6bluef!="") echo $data->constract_drawing6bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6bluef" name="constract_drawing6bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing6blueftext" name="constract_drawing6blueftext" value="<?=$data->constract_drawing6bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6bluef">削除</button>
                                              </div>
                                              <div id="loading6bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluef" name="co_owner6bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6bluef)) {
                                                $selected_array = explode(';',$data->co_owner6bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluef" name="check6bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check6bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item6bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7bluef" id="pdf7bluef"><?php if($data->constract_drawing7bluef!="") echo $data->constract_drawing7bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7bluef" name="constract_drawing7bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing7blueftext" name="constract_drawing7blueftext" value="<?=$data->constract_drawing7bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7bluef">削除</button>
                                              </div>
                                              <div id="loading7bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluef" name="co_owner7bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7bluef)) {
                                                $selected_array = explode(';',$data->co_owner7bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluef" name="check7bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check7bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item7bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8bluef" id="pdf8bluef"><?php if($data->constract_drawing8bluef!="") echo $data->constract_drawing8bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8bluef" name="constract_drawing8bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing8blueftext" name="constract_drawing8blueftext" value="<?=$data->constract_drawing8bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8bluef">削除</button>
                                              </div>
                                              <div id="loading8bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluef" name="co_owner8bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8bluef)) {
                                                $selected_array = explode(';',$data->co_owner8bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluef" name="check8bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check8bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item8bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9bluef" id="pdf9bluef"><?php if($data->constract_drawing9bluef!="") echo $data->constract_drawing9bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9bluef" name="constract_drawing9bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing9blueftext" name="constract_drawing9blueftext" value="<?=$data->constract_drawing9bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9bluef">削除</button>
                                              </div>
                                              <div id="loading9bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluef" name="co_owner9bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9bluef)) {
                                                $selected_array = explode(';',$data->co_owner9bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluef" name="check9bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check9bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item9bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10bluef" id="pdf10bluef"><?php if($data->constract_drawing10bluef!="") echo $data->constract_drawing10bluef; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10bluef" name="constract_drawing10bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="設備プランシート">
                                                <input type="hidden" class="control" id="constract_drawing10blueftext" name="constract_drawing10blueftext" value="<?=$data->constract_drawing10bluef?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10bluef">削除</button>
                                              </div>
                                              <div id="loading10bluef"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluef" name="co_owner10bluef[]" multiple style="width:100%;" data-aum="設備プランシート">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10bluef)) {
                                                $selected_array = explode(';',$data->co_owner10bluef);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }  
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluef" name="check10bluef" data-aum="設備プランシート" aria-describedby="helpId" <?php if(!empty($data->check10bluef)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10bluef">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" data-aum="設備プランシート" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1bluef == "設備プランシート") echo $data->desc_item10bluef;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>

                            
                            <!-- CATEGORY 7 BLUE --> 
                            <div class="card blueCard">

                              <div class="card-header" id="headingSevenBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7blue" role="button" aria-expanded="false" aria-controls="column7blue">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>その他</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($data->category_name1blueg == "その他"){echo "show";} ?>" id="column7blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf1blueg" id="pdf1blueg"><?php if($data->constract_drawing1blueg!="") echo $data->constract_drawing1blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing1blueg" name="constract_drawing1blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing1bluegtext" name="constract_drawing1bluegtext" value="<?=$data->constract_drawing1blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete1blueg">削除</button>
                                              </div>
                                              <div id="loading1blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueg" name="co_owner1blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner1blueg)) {
                                                $selected_array = explode(';',$data->co_owner1blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              } 
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address7" name="duplicate-address7">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueg" name="check1blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check1blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check1blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueg" id="desc_item1blueg" aria-describedby="helpId" data-aum="その他" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item1blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>  

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf2blueg" id="pdf2blueg"><?php if($data->constract_drawing2blueg!="") echo $data->constract_drawing2blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing2blueg" name="constract_drawing2blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing2bluegtext" name="constract_drawing2bluegtext" value="<?=$data->constract_drawing2blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete2blueg">削除</button>
                                              </div>
                                              <div id="loading2blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueg" name="co_owner2blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner2blueg)) {
                                                $selected_array = explode(';',$data->co_owner2blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueg" name="check2blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check2blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check2blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueg" id="desc_item2blueg" aria-describedby="helpId" data-aum="その他" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item2blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf3blueg" id="pdf3blueg"><?php if($data->constract_drawing3blueg!="") echo $data->constract_drawing3blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing3blueg" name="constract_drawing3blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing3bluegtext" name="constract_drawing3bluegtext" value="<?=$data->constract_drawing3blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete3blueg">削除</button>
                                              </div>
                                              <div id="loading3blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueg" name="co_owner3blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner3blueg)) {
                                                $selected_array = explode(';',$data->co_owner3blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueg" name="check3blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check3blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check3blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueg" id="desc_item3blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item3blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf4blueg" id="pdf4blueg"><?php if($data->constract_drawing4blueg!="") echo $data->constract_drawing4blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing4blueg" name="constract_drawing4blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing4bluegtext" name="constract_drawing4bluegtext" value="<?=$data->constract_drawing4blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete4blueg">削除</button>
                                              </div>
                                              <div id="loading4blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueg" name="co_owner4blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner4blueg)) {
                                                $selected_array = explode(';',$data->co_owner4blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueg" name="check4blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check4blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check4blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueg" id="desc_item4blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item4blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf5blueg" id="pdf5blueg"><?php if($data->constract_drawing5blueg!="") echo $data->constract_drawing5blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing5blueg" name="constract_drawing5blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing5bluegtext" name="constract_drawing5bluegtext" value="<?=$data->constract_drawing5blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete5blueg">削除</button>
                                              </div>
                                              <div id="loading5blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueg" name="co_owner5blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner5blueg)) {
                                                $selected_array = explode(';',$data->co_owner5blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueg" name="check5blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check5blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check5blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueg" id="desc_item5blueg" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item5blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf6blueg" id="pdf6blueg"><?php if($data->constract_drawing6blueg!="") echo $data->constract_drawing6blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing6blueg" name="constract_drawing6blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing6bluegtext" name="constract_drawing6bluegtext" value="<?=$data->constract_drawing6blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete6blueg">削除</button>
                                              </div>
                                              <div id="loading6blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueg" name="co_owner6blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner6blueg)) {
                                                $selected_array = explode(';',$data->co_owner6blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueg" name="check6blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check6blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check6blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueg" id="desc_item6blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item6blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf7blueg" id="pdf7blueg"><?php if($data->constract_drawing7blueg!="") echo $data->constract_drawing7blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing7blueg" name="constract_drawing7blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing7bluegtext" name="constract_drawing7bluegtext" value="<?=$data->constract_drawing7blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete7blueg">削除</button>
                                              </div>
                                              <div id="loading7blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueg" name="co_owner7blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner7blueg)) {
                                                $selected_array = explode(';',$data->co_owner7blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueg" name="check7blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check7blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check7blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueg" id="desc_item7blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item7blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf8blueg" id="pdf8blueg"><?php if($data->constract_drawing8blueg!="") echo $data->constract_drawing8blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing8blueg" name="constract_drawing8blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing8bluegtext" name="constract_drawing8bluegtext" value="<?=$data->constract_drawing8blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete8blueg">削除</button>
                                              </div>
                                              <div id="loading8blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueg" name="co_owner8blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner8blueg)) {
                                                $selected_array = explode(';',$data->co_owner8blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueg" name="check8blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check8blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check8blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueg" id="desc_item8blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item8blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf9blueg" id="pdf9blueg"><?php if($data->constract_drawing9blueg!="") echo $data->constract_drawing9blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing9blueg" name="constract_drawing9blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing9bluegtext" name="constract_drawing9bluegtext" value="<?=$data->constract_drawing9blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete9blueg">削除</button>
                                              </div>
                                              <div id="loading9blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueg" name="co_owner9blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner9blueg)) {
                                                $selected_array = explode(';',$data->co_owner9blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueg" name="check9blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check9blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check9blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueg" id="desc_item9blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item9blueg;?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">

                                      <div class="input-group edit">
                                          <div class="pdf">
                                            <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                          </div>
                                          <div class="custom-file previewFile">
                                            <label for="pdf10blueg" id="pdf10blueg"><?php if($data->constract_drawing10blueg!="") echo $data->constract_drawing10blueg; else echo"ファイルなし";?></label>
                                            <div class="btnBoxEdit">
                                              <div class="change">
                                                <input type="file" class="custom-file-input" id="constract_drawing10blueg" name="constract_drawing10blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" data-aum="その他">
                                                <input type="hidden" class="control" id="constract_drawing10bluegtext" name="constract_drawing10bluegtext" value="<?=$data->constract_drawing10blueg?>">
                                                <div class="clickFile">変更</div>
                                              </div>
                                              <!-- <div class="addition">
                                                <button type="button">追加</button>
                                              </div> -->
                                              <div class="delete">
                                                <button type="button" id="delete10blueg">削除</button>
                                              </div>
                                              <div id="loading10blueg"></div>
                                            </div>
                                          </div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueg" name="co_owner10blueg[]" multiple style="width:100%;" data-aum="その他">
                                            <option></option>
                                            <?php
                                              if(!empty($data->co_owner10blueg)) {
                                                $selected_array = explode(';',$data->co_owner10blueg);
                                                foreach($affiliate as $list){
                                                $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                  echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                                }  
                                              }
                                              else
                                              {
                                                foreach($affiliate as $row)
                                                { 
                                                  echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                                }
                                              }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueg" name="check10blueg" data-aum="その他" aria-describedby="helpId" <?php if(!empty($data->check10blueg)) { echo 'checked';}?>>
                                            <label class="form-check-label" for="check10blueg">共有する</label>

                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueg" id="desc_item10blueg" data-aum="その他" aria-describedby="helpId" placeholder="" value="<?php if($data->category_name1blueg == "その他") echo $data->desc_item10blueg?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 

                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->

                        <button name="" id="myBtn" class="btn hvr-sink" type="submit">追加</button>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/js/select2.min.js"></script>
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->
<script>

  $(function(){  // document onready
      $('input[type=text]').click(function() {
        updateAum( $(this).attr('data-aum') );
      });

      $('.selectpicker').change(function() {
        updateAum( $(this).attr('data-aum') );
      });
  });

  function updateAum(content) {
      //$('#aum').html(content);
      $('#category_euy').val(content);
  }

  $(document).ready(function(){
  
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
          $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
        }
    });

  });

  // $("#cust_code").keydown(function(e) {
  //   var EnterKeyPressed = verifyKeyPressed(e, 13);
  //   if (EnterKeyPressed === true) {
  //       //here is where i want to disable the text box
  //       // var that = this;
  //       // that.disabled = true;
        
  //       // $('#cust_name').fadeOut(500, function() {
  //       //     //i want to re enable it here
  //       //     that.disabled = false;    
  //       //   });
  //       document.getElementById('cust_name').disabled = true; 
        
  //     }
  // });

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });

  // $('select').selectpicker();

  // $(document).ready(function() {
  //     $('.selectpicker').select2();
  // });

  $('#contract').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      //replace the "Choose a file" label
      // $(this).next('.custom-file-label').html(cleanFileName);
      document.getElementById('pdf').innerText = cleanFileName;
});

  // $('#contract').on('change',function(){
  //     var fileName = $(this).val();
  //     var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
  //     $(this).next('#pdf1').html(cleanFileName);
  // });
  var i = 1;
  for(let i=1;i<11;i++){

    $('#constract_drawing'+i).on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('pdf'+i).innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'b').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('pdf'+i+'b').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'c').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('pdf'+i+'c').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'d').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('pdf'+i+'d').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

  $('#constract_drawing'+i+'e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('pdf'+i+'e').innerText = cleanFileName;
  });

  }

  for(let i=1;i<11;i++){

$('#constract_drawing'+i+'f').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'f').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'g').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'g').innerText = cleanFileName;
});

}


for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluea').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'bluea').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blueb').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'blueb').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluec').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'bluec').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blued').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'blued').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluee').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'bluee').innerText = cleanFileName;
});

}


for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluef').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'bluef').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blueg').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('pdf'+i+'blueg').innerText = cleanFileName;
});

}
    // function handler(e){
    //   var input = document.getElementById('input_delivery_date').value;
    //   var input2 = document.getElementById('input_delivery_date').value;
    //   var input3 = document.getElementById('input_delivery_date').value;
    //   var input4 = document.getElementById('input_delivery_date').value;
    //   var input5 = document.getElementById('input_delivery_date').value;
    //   var input6 = document.getElementById('input_delivery_date').value;
    //   var input7 = document.getElementById('input_delivery_date').value;

    //   var manDate = moment(input).local();
    //   var manDate2 = moment(input2).local();
    //   var manDate3 = moment(input3).local();
    //   var manDate4 = moment(input4).local();
    //   var manDate5 = moment(input5).local();
    //   var manDate6 = moment(input6).local();
    //   var manDate7 = moment(input7).local();

    //   document.getElementById('inspection_time1').value = manDate.add(3, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time2').value = manDate2.add(6, 'M').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time3').value = manDate3.add(1, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time4').value = manDate4.add(2, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time5').value = manDate5.add(3, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time6').value = manDate6.add(5, 'y').format('YYYY-MM-DD'); 
    //   document.getElementById('inspection_time7').value = manDate7.add(10, 'y').format('YYYY-MM-DD'); 
    // }
//     $(document).ready(function() {
//   $(window).keydown(function(event){
//     if(event.keyCode == 13) {
//       event.preventDefault();
//       return false;
//     }
//   });
// });

$(document).on("keydown", ":input:not(textarea)", function(event) {
    return event.key != "Enter";
});
</script>
<script>


$(document).ready(function() {
    $('#delete1').on('click', function(e) {
        var $el = $('#constract_drawing1');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1').innerText = "ファイルを選択";
        document.getElementById('desc_item1').value = "";
        document.getElementById('constract_drawing1text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete2').on('click', function(e) {
        var $el = $('#constract_drawing2');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2').innerText = "ファイルを選択";
        document.getElementById('desc_item2').value = "";
        document.getElementById('constract_drawing2text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3').on('click', function(e) {
        var $el = $('#constract_drawing3');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3').innerText = "ファイルを選択";
        document.getElementById('desc_item3').value = "";
        document.getElementById('constract_drawing3text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4').on('click', function(e) {
        var $el = $('#constract_drawing4');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4').innerText = "ファイルを選択";
        document.getElementById('desc_item4').value = "";
        document.getElementById('constract_drawing4text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5').on('click', function(e) {
        var $el = $('#constract_drawing5');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5').innerText = "ファイルを選択";
        document.getElementById('desc_item5').value = "";
        document.getElementById('constract_drawing5text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6').on('click', function(e) {
        var $el = $('#constract_drawing6');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6').innerText = "ファイルを選択";
        document.getElementById('desc_item6').value = "";
        document.getElementById('constract_drawing6text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7').on('click', function(e) {
        var $el = $('#constract_drawing7');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7').innerText = "ファイルを選択";
        document.getElementById('desc_item7').value = "";
        document.getElementById('constract_drawing7text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8').on('click', function(e) {
        var $el = $('#constract_drawing8');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8').innerText = "ファイルを選択";
        document.getElementById('desc_item8').value = "";
        document.getElementById('constract_drawing8text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9').on('click', function(e) {
        var $el = $('#constract_drawing9');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9').innerText = "ファイルを選択";
        document.getElementById('desc_item9').value = "";
        document.getElementById('constract_drawing9text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10').on('click', function(e) {
        var $el = $('#constract_drawing10');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10').innerText = "ファイルを選択";
        document.getElementById('desc_item10').value = "";
        document.getElementById('constract_drawing10text').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10;?>';
        var category_name = 'category_name';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1b').on('click', function(e) {
        var $el = $('#constract_drawing1b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1b').innerText = "ファイルを選択";
        document.getElementById('desc_item1b').value = "";
        document.getElementById('constract_drawing1btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2b').on('click', function(e) {
        var $el = $('#constract_drawing2b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2b').innerText = "ファイルを選択";
        document.getElementById('desc_item2b').value = "";
        document.getElementById('constract_drawing2btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3b').on('click', function(e) {
        var $el = $('#constract_drawing3b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3b').innerText = "ファイルを選択";
        document.getElementById('desc_item3b').value = "";
        document.getElementById('constract_drawing3btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4b').on('click', function(e) {
        var $el = $('#constract_drawing4b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4b').innerText = "ファイルを選択";
        document.getElementById('desc_item4b').value = "";
        document.getElementById('constract_drawing4btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5b').on('click', function(e) {
        var $el = $('#constract_drawing5b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5b').innerText = "ファイルを選択";
        document.getElementById('desc_item5b').value = "";
        document.getElementById('constract_drawing5btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6b').on('click', function(e) {
        var $el = $('#constract_drawing6b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6b').innerText = "ファイルを選択";
        document.getElementById('desc_item6b').value = "";
        document.getElementById('constract_drawing6btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7b').on('click', function(e) {
        var $el = $('#constract_drawing7b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7b').innerText = "ファイルを選択";
        document.getElementById('desc_item7b').value = "";
        document.getElementById('constract_drawing7btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8b').on('click', function(e) {
        var $el = $('#constract_drawing8b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8b').innerText = "ファイルを選択";
        document.getElementById('desc_item8b').value = "";
        document.getElementById('constract_drawing8btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9b').on('click', function(e) {
        var $el = $('#constract_drawing9b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9b').innerText = "ファイルを選択";
        document.getElementById('desc_item9b').value = "";
        document.getElementById('constract_drawing9btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10b').on('click', function(e) {
        var $el = $('#constract_drawing10b');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10b').innerText = "ファイルを選択";
        document.getElementById('desc_item10b').value = "";
        document.getElementById('constract_drawing10btext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10b;?>';
        var category_name = 'category_name1b';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10b", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete1c').on('click', function(e) {
        var $el = $('#constract_drawing1c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1c').innerText = "ファイルを選択";
        document.getElementById('desc_item1c').value = "";
        document.getElementById('constract_drawing1ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2c').on('click', function(e) {
        var $el = $('#constract_drawing2c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2c').innerText = "ファイルを選択";
        document.getElementById('desc_item2c').value = "";
        document.getElementById('constract_drawing2ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3c').on('click', function(e) {
        var $el = $('#constract_drawing3c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3c').innerText = "ファイルを選択";
        document.getElementById('desc_item3c').value = "";
        document.getElementById('constract_drawing3ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4c').on('click', function(e) {
        var $el = $('#constract_drawing4c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4c').innerText = "ファイルを選択";
        document.getElementById('desc_item4c').value = "";
        document.getElementById('constract_drawing4ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5c').on('click', function(e) {
        var $el = $('#constract_drawing5c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5c').innerText = "ファイルを選択";
        document.getElementById('desc_item5c').value = "";
        document.getElementById('constract_drawing5ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6c').on('click', function(e) {
        var $el = $('#constract_drawing6c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6c').innerText = "ファイルを選択";
        document.getElementById('desc_item6c').value = "";
        document.getElementById('constract_drawing6ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7c').on('click', function(e) {
        var $el = $('#constract_drawing7c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7c').innerText = "ファイルを選択";
        document.getElementById('desc_item7c').value = "";
        document.getElementById('constract_drawing7ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8c').on('click', function(e) {
        var $el = $('#constract_drawing8c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8c').innerText = "ファイルを選択";
        document.getElementById('desc_item8c').value = "";
        document.getElementById('constract_drawing8ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9c').on('click', function(e) {
        var $el = $('#constract_drawing9c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9c').innerText = "ファイルを選択";
        document.getElementById('desc_item9c').value = "";
        document.getElementById('constract_drawing9ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10c').on('click', function(e) {
        var $el = $('#constract_drawing10c');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10c').innerText = "ファイルを選択";
        document.getElementById('desc_item10c').value = "";
        document.getElementById('constract_drawing10ctext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10c;?>';
        var category_name = 'category_name1c';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10c", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1d').on('click', function(e) {
        var $el = $('#constract_drawing1d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1d').innerText = "ファイルを選択";
        document.getElementById('desc_item1d').value = "";
        document.getElementById('constract_drawing1dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2d').on('click', function(e) {
        var $el = $('#constract_drawing2d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2d').innerText = "ファイルを選択";
        document.getElementById('desc_item2d').value = "";
        document.getElementById('constract_drawing2dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3d').on('click', function(e) {
        var $el = $('#constract_drawing3d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3d').innerText = "ファイルを選択";
        document.getElementById('desc_item3d').value = "";
        document.getElementById('constract_drawing3dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4d').on('click', function(e) {
        var $el = $('#constract_drawing4d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4d').innerText = "ファイルを選択";
        document.getElementById('desc_item4d').value = "";
        document.getElementById('constract_drawing4dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5d').on('click', function(e) {
        var $el = $('#constract_drawing5d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5d').innerText = "ファイルを選択";
        document.getElementById('desc_item5d').value = "";
        document.getElementById('constract_drawing5dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6d').on('click', function(e) {
        var $el = $('#constract_drawing6d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6d').innerText = "ファイルを選択";
        document.getElementById('desc_item6d').value = "";
        document.getElementById('constract_drawing6dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7d').on('click', function(e) {
        var $el = $('#constract_drawing7d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7d').innerText = "ファイルを選択";
        document.getElementById('desc_item7d').value = "";
        document.getElementById('constract_drawing7dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8d').on('click', function(e) {
        var $el = $('#constract_drawing8d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8d').innerText = "ファイルを選択";
        document.getElementById('desc_item8d').value = "";
        document.getElementById('constract_drawing8dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9d').on('click', function(e) {
        var $el = $('#constract_drawing9d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9d').innerText = "ファイルを選択";
        document.getElementById('desc_item9d').value = "";
        document.getElementById('constract_drawing9dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10d').on('click', function(e) {
        var $el = $('#constract_drawing10d');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10d').innerText = "ファイルを選択";
        document.getElementById('desc_item10d').value = "";
        document.getElementById('constract_drawing10dtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10d;?>';
        var category_name = 'category_name1d';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10d", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1e').on('click', function(e) {
        var $el = $('#constract_drawing1e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1e').innerText = "ファイルを選択";
        document.getElementById('desc_item1e').value = "";
        document.getElementById('constract_drawing1etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2e').on('click', function(e) {
        var $el = $('#constract_drawing2e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2e').innerText = "ファイルを選択";
        document.getElementById('desc_item2e').value = "";
        document.getElementById('constract_drawing2etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3e').on('click', function(e) {
        var $el = $('#constract_drawing3e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3e').innerText = "ファイルを選択";
        document.getElementById('desc_item3e').value = "";
        document.getElementById('constract_drawing3etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4e').on('click', function(e) {
        var $el = $('#constract_drawing4e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4e').innerText = "ファイルを選択";
        document.getElementById('desc_item4e').value = "";
        document.getElementById('constract_drawing4etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5e').on('click', function(e) {
        var $el = $('#constract_drawing5e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5e').innerText = "ファイルを選択";
        document.getElementById('desc_item5e').value = "";
        document.getElementById('constract_drawing5etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6e').on('click', function(e) {
        var $el = $('#constract_drawing6e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6e').innerText = "ファイルを選択";
        document.getElementById('desc_item6e').value = "";
        document.getElementById('constract_drawing6etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7e').on('click', function(e) {
        var $el = $('#constract_drawing7e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7e').innerText = "ファイルを選択";
        document.getElementById('desc_item7e').value = "";
        document.getElementById('constract_drawing7etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8e').on('click', function(e) {
        var $el = $('#constract_drawing8e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8e').innerText = "ファイルを選択";
        document.getElementById('desc_item8e').value = "";
        document.getElementById('constract_drawing8etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9e').on('click', function(e) {
        var $el = $('#constract_drawing9e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9e').innerText = "ファイルを選択";
        document.getElementById('desc_item9e').value = "";
        document.getElementById('constract_drawing9etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10e').on('click', function(e) {
        var $el = $('#constract_drawing10e');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10e').innerText = "ファイルを選択";
        document.getElementById('desc_item10e').value = "";
        document.getElementById('constract_drawing10etext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10e;?>';
        var category_name = 'category_name1e';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10e", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1f').on('click', function(e) {
        var $el = $('#constract_drawing1f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1f').innerText = "ファイルを選択";
        document.getElementById('desc_item1f').value = "";
        document.getElementById('constract_drawing1ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2f').on('click', function(e) {
        var $el = $('#constract_drawing2f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2f').innerText = "ファイルを選択";
        document.getElementById('desc_item2f').value = "";
        document.getElementById('constract_drawing2ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3f').on('click', function(e) {
        var $el = $('#constract_drawing3f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3f').innerText = "ファイルを選択";
        document.getElementById('desc_item3f').value = "";
        document.getElementById('constract_drawing3ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4f').on('click', function(e) {
        var $el = $('#constract_drawing4f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4f').innerText = "ファイルを選択";
        document.getElementById('desc_item4f').value = "";
        document.getElementById('constract_drawing4ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5f').on('click', function(e) {
        var $el = $('#constract_drawing5f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5f').innerText = "ファイルを選択";
        document.getElementById('desc_item5f').value = "";
        document.getElementById('constract_drawing5ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6f').on('click', function(e) {
        var $el = $('#constract_drawing6f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6f').innerText = "ファイルを選択";
        document.getElementById('desc_item6f').value = "";
        document.getElementById('constract_drawing6ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7f').on('click', function(e) {
        var $el = $('#constract_drawing7f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7f').innerText = "ファイルを選択";
        document.getElementById('desc_item7f').value = "";
        document.getElementById('constract_drawing7ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8f').on('click', function(e) {
        var $el = $('#constract_drawing8f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8f').innerText = "ファイルを選択";
        document.getElementById('desc_item8f').value = "";
        document.getElementById('constract_drawing8ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9f').on('click', function(e) {
        var $el = $('#constract_drawing9f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9f').innerText = "ファイルを選択";
        document.getElementById('desc_item9f').value = "";
        document.getElementById('constract_drawing9ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10f').on('click', function(e) {
        var $el = $('#constract_drawing10f');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10f').innerText = "ファイルを選択";
        document.getElementById('desc_item10f').value = "";
        document.getElementById('constract_drawing10ftext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10f;?>';
        var category_name = 'category_name1f';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10f", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1g').on('click', function(e) {
        var $el = $('#constract_drawing1g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1g').innerText = "ファイルを選択";
        document.getElementById('desc_item1g').value = "";
        document.getElementById('constract_drawing1gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2g').on('click', function(e) {
        var $el = $('#constract_drawing2g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2g').innerText = "ファイルを選択";
        document.getElementById('desc_item2g').value = "";
        document.getElementById('constract_drawing2gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3g').on('click', function(e) {
        var $el = $('#constract_drawing3g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3g').innerText = "ファイルを選択";
        document.getElementById('desc_item3g').value = "";
        document.getElementById('constract_drawing3gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4g').on('click', function(e) {
        var $el = $('#constract_drawing4g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4g').innerText = "ファイルを選択";
        document.getElementById('desc_item4g').value = "";
        document.getElementById('constract_drawing4gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5g').on('click', function(e) {
        var $el = $('#constract_drawing5g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5g').innerText = "ファイルを選択";
        document.getElementById('desc_item5g').value = "";
        document.getElementById('constract_drawing5gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6g').on('click', function(e) {
        var $el = $('#constract_drawing6g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6g').innerText = "ファイルを選択";
        document.getElementById('desc_item6g').value = "";
        document.getElementById('constract_drawing6gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7g').on('click', function(e) {
        var $el = $('#constract_drawing7g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7g').innerText = "ファイルを選択";
        document.getElementById('desc_item7g').value = "";
        document.getElementById('constract_drawing7gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8g').on('click', function(e) {
        var $el = $('#constract_drawing8g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8g').innerText = "ファイルを選択";
        document.getElementById('desc_item8g').value = "";
        document.getElementById('constract_drawing8gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9g').on('click', function(e) {
        var $el = $('#constract_drawing9g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9g').innerText = "ファイルを選択";
        document.getElementById('desc_item9g').value = "";
        document.getElementById('constract_drawing9gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10g').on('click', function(e) {
        var $el = $('#constract_drawing10g');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10g').innerText = "ファイルを選択";
        document.getElementById('desc_item10g').value = "";
        document.getElementById('constract_drawing10gtext').value = "";

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10g;?>';
        var category_name = 'category_name1g';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10g", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1bluea').on('click', function(e) {
        var $el = $('#constract_drawing1bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item1bluea').value = "";
        document.getElementById('constract_drawing1blueatext').value = "";
        $('#co_owner1bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2bluea').on('click', function(e) {
        var $el = $('#constract_drawing2bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item2bluea').value = "";
        document.getElementById('constract_drawing2blueatext').value = "";
        $('#co_owner2bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3bluea').on('click', function(e) {
        var $el = $('#constract_drawing3bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item3bluea').value = "";
        document.getElementById('constract_drawing3blueatext').value = "";
        $('#co_owner3bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4bluea').on('click', function(e) {
        var $el = $('#constract_drawing4bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item4bluea').value = "";
        document.getElementById('constract_drawing4blueatext').value = "";
        $('#co_owner4bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5bluea').on('click', function(e) {
        var $el = $('#constract_drawing5bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item5bluea').value = "";
        document.getElementById('constract_drawing5blueatext').value = "";
        $('#co_owner5bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6bluea').on('click', function(e) {
        var $el = $('#constract_drawing6bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item6bluea').value = "";
        document.getElementById('constract_drawing6blueatext').value = "";
        $('#co_owner6bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7bluea').on('click', function(e) {
        var $el = $('#constract_drawing7bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item7bluea').value = "";
        document.getElementById('constract_drawing7blueatext').value = "";
        $('#co_owner7bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8bluea').on('click', function(e) {
        var $el = $('#constract_drawing8bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item8bluea').value = "";
        document.getElementById('constract_drawing8blueatext').value = "";
        $('#co_owner8bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9bluea').on('click', function(e) {
        var $el = $('#constract_drawing9bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item9bluea').value = "";
        document.getElementById('constract_drawing9blueatext').value = "";
        $('#co_owner9bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10bluea').on('click', function(e) {
        var $el = $('#constract_drawing10bluea');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10bluea').innerText = "ファイルを選択";
        document.getElementById('desc_item10bluea').value = "";
        document.getElementById('constract_drawing10blueatext').value = "";
        $('#co_owner10bluea').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10bluea;?>';
        var category_name = 'category_name1bluea';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10bluea", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1blueb').on('click', function(e) {
        var $el = $('#constract_drawing1blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item1blueb').value = "";
        document.getElementById('constract_drawing1bluebtext').value = "";
        $('#co_owner1blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2blueb').on('click', function(e) {
        var $el = $('#constract_drawing2blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item2blueb').value = "";
        document.getElementById('constract_drawing2bluebtext').value = "";
        $('#co_owner2blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3blueb').on('click', function(e) {
        var $el = $('#constract_drawing3blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item3blueb').value = "";
        document.getElementById('constract_drawing3bluebtext').value = "";
        $('#co_owner3blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4blueb').on('click', function(e) {
        var $el = $('#constract_drawing4blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item4blueb').value = "";
        document.getElementById('constract_drawing4bluebtext').value = "";
        $('#co_owner4blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5blueb').on('click', function(e) {
        var $el = $('#constract_drawing5blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item5blueb').value = "";
        document.getElementById('constract_drawing5bluebtext').value = "";
        $('#co_owner5blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6blueb').on('click', function(e) {
        var $el = $('#constract_drawing6blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item6blueb').value = "";
        document.getElementById('constract_drawing6bluebtext').value = "";
        $('#co_owner6blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7blueb').on('click', function(e) {
        var $el = $('#constract_drawing7blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item7blueb').value = "";
        document.getElementById('constract_drawing7bluebtext').value = "";
        $('#co_owner7blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8blueb').on('click', function(e) {
        var $el = $('#constract_drawing8blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item8blueb').value = "";
        document.getElementById('constract_drawing8bluebtext').value = "";
        $('#co_owner8blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9blueb').on('click', function(e) {
        var $el = $('#constract_drawing9blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item9blueb').value = "";
        document.getElementById('constract_drawing9bluebtext').value = "";
        $('#co_owner9blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10blueb').on('click', function(e) {
        var $el = $('#constract_drawing10blueb');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10blueb').innerText = "ファイルを選択";
        document.getElementById('desc_item10blueb').value = "";
        document.getElementById('constract_drawing10bluebtext').value = "";
        $('#co_owner10blueb').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10blueb;?>';
        var category_name = 'category_name1blueb';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10blueb", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});


$(document).ready(function() {
    $('#delete1bluec').on('click', function(e) {
        var $el = $('#constract_drawing1bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item1bluec').value = "";
        document.getElementById('constract_drawing1bluectext').value = "";
        $('#co_owner1bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2bluec').on('click', function(e) {
        var $el = $('#constract_drawing2bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item2bluec').value = "";
        document.getElementById('constract_drawing2bluectext').value = "";
        $('#co_owner2bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3bluec').on('click', function(e) {
        var $el = $('#constract_drawing3bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item3bluec').value = "";
        document.getElementById('constract_drawing3bluectext').value = "";
        $('#co_owner3bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4bluec').on('click', function(e) {
        var $el = $('#constract_drawing4bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item4bluec').value = "";
        document.getElementById('constract_drawing4bluectext').value = "";
        $('#co_owner4bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5bluec').on('click', function(e) {
        var $el = $('#constract_drawing5bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item5bluec').value = "";
        document.getElementById('constract_drawing5bluectext').value = "";
        $('#co_owner5bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6bluec').on('click', function(e) {
        var $el = $('#constract_drawing6bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item6bluec').value = "";
        document.getElementById('constract_drawing6bluectext').value = "";
        $('#co_owner6bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7bluec').on('click', function(e) {
        var $el = $('#constract_drawing7bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item7bluec').value = "";
        document.getElementById('constract_drawing7bluectext').value = "";
        $('#co_owner7bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8bluec').on('click', function(e) {
        var $el = $('#constract_drawing8bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item8bluec').value = "";
        document.getElementById('constract_drawing8bluectext').value = "";
        $('#co_owner8bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9bluec').on('click', function(e) {
        var $el = $('#constract_drawing9bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item9bluec').value = "";
        document.getElementById('constract_drawing9bluectext').value = "";
        $('#co_owner9bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10bluec').on('click', function(e) {
        var $el = $('#constract_drawing10bluec');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10bluec').innerText = "ファイルを選択";
        document.getElementById('desc_item10bluec').value = "";
        document.getElementById('constract_drawing10bluectext').value = "";
        $('#co_owner10bluec').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10bluec;?>';
        var category_name = 'category_name1bluec';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10bluec", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete1blued').on('click', function(e) {
        var $el = $('#constract_drawing1blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1blued').innerText = "ファイルを選択";
        document.getElementById('desc_item1blued').value = "";
        document.getElementById('constract_drawing1bluedtext').value = "";
        $('#co_owner1blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2blued').on('click', function(e) {
        var $el = $('#constract_drawing2blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2blued').innerText = "ファイルを選択";
        document.getElementById('desc_item2blued').value = "";
        document.getElementById('constract_drawing2bluedtext').value = "";
        $('#co_owner2blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3blued').on('click', function(e) {
        var $el = $('#constract_drawing3blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3blued').innerText = "ファイルを選択";
        document.getElementById('desc_item3blued').value = "";
        document.getElementById('constract_drawing3bluedtext').value = "";
        $('#co_owner3blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4blued').on('click', function(e) {
        var $el = $('#constract_drawing4blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4blued').innerText = "ファイルを選択";
        document.getElementById('desc_item4blued').value = "";
        document.getElementById('constract_drawing4bluedtext').value = "";
        $('#co_owner4blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5blued').on('click', function(e) {
        var $el = $('#constract_drawing5blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5blued').innerText = "ファイルを選択";
        document.getElementById('desc_item5blued').value = "";
        document.getElementById('constract_drawing5bluedtext').value = "";
        $('#co_owner5blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6blued').on('click', function(e) {
        var $el = $('#constract_drawing6blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6blued').innerText = "ファイルを選択";
        document.getElementById('desc_item6blued').value = "";
        document.getElementById('constract_drawing6bluedtext').value = "";
        $('#co_owner6blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7blued').on('click', function(e) {
        var $el = $('#constract_drawing7blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7blued').innerText = "ファイルを選択";
        document.getElementById('desc_item7blued').value = "";
        document.getElementById('constract_drawing7bluedtext').value = "";
        $('#co_owner7blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8blued').on('click', function(e) {
        var $el = $('#constract_drawing8blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8blued').innerText = "ファイルを選択";
        document.getElementById('desc_item8blued').value = "";
        document.getElementById('constract_drawing8bluedtext').value = "";
        $('#co_owner8blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9blued').on('click', function(e) {
        var $el = $('#constract_drawing9blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9blued').innerText = "ファイルを選択";
        document.getElementById('desc_item9blued').value = "";
        document.getElementById('constract_drawing9bluedtext').value = "";
        $('#co_owner9blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10blued').on('click', function(e) {
        var $el = $('#constract_drawing10blued');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10blued').innerText = "ファイルを選択";
        document.getElementById('desc_item10blued').value = "";
        document.getElementById('constract_drawing10bluedtext').value = "";
        $('#co_owner10blued').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10blued;?>';
        var category_name = 'category_name1blued';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10blued", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete1bluee').on('click', function(e) {
        var $el = $('#constract_drawing1bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item1bluee').value = "";
        document.getElementById('constract_drawing1blueetext').value = "";
        $('#co_owner1bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2bluee').on('click', function(e) {
        var $el = $('#constract_drawing2bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item2bluee').value = "";
        document.getElementById('constract_drawing2blueetext').value = "";
        $('#co_owner2bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3bluee').on('click', function(e) {
        var $el = $('#constract_drawing3bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item3bluee').value = "";
        document.getElementById('constract_drawing3blueetext').value = "";
        $('#co_owner3bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4bluee').on('click', function(e) {
        var $el = $('#constract_drawing4bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item4bluee').value = "";
        document.getElementById('constract_drawing4blueetext').value = "";
        $('#co_owner4bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5bluee').on('click', function(e) {
        var $el = $('#constract_drawing5bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item5bluee').value = "";
        document.getElementById('constract_drawing5blueetext').value = "";
        $('#co_owner5bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6bluee').on('click', function(e) {
        var $el = $('#constract_drawing6bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item6bluee').value = "";
        document.getElementById('constract_drawing6blueetext').value = "";
        $('#co_owner6bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7bluee').on('click', function(e) {
        var $el = $('#constract_drawing7bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item7bluee').value = "";
        document.getElementById('constract_drawing7blueetext').value = "";
        $('#co_owner7bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8bluee').on('click', function(e) {
        var $el = $('#constract_drawing8bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item8bluee').value = "";
        document.getElementById('constract_drawing8blueetext').value = "";
        $('#co_owner8bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9bluee').on('click', function(e) {
        var $el = $('#constract_drawing9bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item9bluee').value = "";
        document.getElementById('constract_drawing9blueetext').value = "";
        $('#co_owner9bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10bluee').on('click', function(e) {
        var $el = $('#constract_drawing10bluee');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10bluee').innerText = "ファイルを選択";
        document.getElementById('desc_item10bluee').value = "";
        document.getElementById('constract_drawing10blueetext').value = "";
        $('#co_owner10bluee').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10bluee;?>';
        var category_name = 'category_name1bluee';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10bluee", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete1bluef').on('click', function(e) {
        var $el = $('#constract_drawing1bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item1bluef').value = "";
        document.getElementById('constract_drawing1blueftext').value = "";
        $('#co_owner1bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2bluef').on('click', function(e) {
        var $el = $('#constract_drawing2bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item2bluef').value = "";
        document.getElementById('constract_drawing2blueftext').value = "";
        $('#co_owner2bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3bluef').on('click', function(e) {
        var $el = $('#constract_drawing3bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item3bluef').value = "";
        document.getElementById('constract_drawing3blueftext').value = "";
        $('#co_owner3bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4bluef').on('click', function(e) {
        var $el = $('#constract_drawing4bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item4bluef').value = "";
        document.getElementById('constract_drawing4blueftext').value = "";
        $('#co_owner4bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5bluef').on('click', function(e) {
        var $el = $('#constract_drawing5bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item5bluef').value = "";
        document.getElementById('constract_drawing5blueftext').value = "";
        $('#co_owner5bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6bluef').on('click', function(e) {
        var $el = $('#constract_drawing6bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item6bluef').value = "";
        document.getElementById('constract_drawing6blueftext').value = "";
        $('#co_owner6bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7bluef').on('click', function(e) {
        var $el = $('#constract_drawing7bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item7bluef').value = "";
        document.getElementById('constract_drawing7blueftext').value = "";
        $('#co_owner7bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8bluef').on('click', function(e) {
        var $el = $('#constract_drawing8bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item8bluef').value = "";
        document.getElementById('constract_drawing8blueftext').value = "";
        $('#co_owner8bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9bluef').on('click', function(e) {
        var $el = $('#constract_drawing9bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item9bluef').value = "";
        document.getElementById('constract_drawing9blueftext').value = "";
        $('#co_owner9bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10bluef').on('click', function(e) {
        var $el = $('#constract_drawing10bluef');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10bluef').innerText = "ファイルを選択";
        document.getElementById('desc_item10bluef').value = "";
        document.getElementById('constract_drawing10blueftext').value = "";
        $('#co_owner10bluef').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10bluef;?>';
        var category_name = 'category_name1bluef';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10bluef", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

$(document).ready(function() {
    $('#delete1blueg').on('click', function(e) {
        var $el = $('#constract_drawing1blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf1blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item1blueg').value = "";
        document.getElementById('constract_drawing1bluegtext').value = "";
        $('#co_owner1blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing1blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing1blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete2blueg').on('click', function(e) {
        var $el = $('#constract_drawing2blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf2blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item2blueg').value = "";
        document.getElementById('constract_drawing2bluegtext').value = "";
        $('#co_owner2blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing2blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing2blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete3blueg').on('click', function(e) {
        var $el = $('#constract_drawing3blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf3blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item3blueg').value = "";
        document.getElementById('constract_drawing3bluegtext').value = "";
        $('#co_owner3blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing3blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing3blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete4blueg').on('click', function(e) {
        var $el = $('#constract_drawing4blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf4blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item4blueg').value = "";
        document.getElementById('constract_drawing4bluegtext').value = "";
        $('#co_owner4blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing4blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing4blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete5blueg').on('click', function(e) {
        var $el = $('#constract_drawing5blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf5blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item5blueg').value = "";
        document.getElementById('constract_drawing5bluegtext').value = "";
        $('#co_owner5blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing5blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing5blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete6blueg').on('click', function(e) {
        var $el = $('#constract_drawing6blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf6blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item6blueg').value = "";
        document.getElementById('constract_drawing6bluegtext').value = "";
        $('#co_owner6blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing6blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing6blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete7blueg').on('click', function(e) {
        var $el = $('#constract_drawing7blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf7blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item7blueg').value = "";
        document.getElementById('constract_drawing7bluegtext').value = "";
        $('#co_owner7blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing7blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing7blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete8blueg').on('click', function(e) {
        var $el = $('#constract_drawing8blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf8blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item8blueg').value = "";
        document.getElementById('constract_drawing8bluegtext').value = "";
        $('#co_owner8blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing8blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing8blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete9blueg').on('click', function(e) {
        var $el = $('#constract_drawing9blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf9blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item9blueg').value = "";
        document.getElementById('constract_drawing9bluegtext').value = "";
        $('#co_owner9blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing9blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing9blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});
$(document).ready(function() {
    $('#delete10blueg').on('click', function(e) {
        var $el = $('#constract_drawing10blueg');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        document.getElementById('pdf10blueg').innerText = "ファイルを選択";
        document.getElementById('desc_item10blueg').value = "";
        document.getElementById('constract_drawing10bluegtext').value = "";
        $('#co_owner10blueg').val('').trigger('change');

        var ajax_url = '<?php echo base_url();?>dashboard/delete_files/';
        var id_cons = '<?php echo $data->construct_id;?>';
        var name_file = '<?php echo $data->constract_drawing10blueg;?>';
        var category_name = 'category_name1blueg';
        e.preventDefault();
        $.ajax({
            url: ajax_url,
            type: "POST",
            dataType : "text",
            data : {"id" : id_cons, "table_name" : "constract_drawing10blueg", "name_file" : name_file, "category" : category_name},
            success: function (status) {
                console.log(status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});

</script>
<script>
$(document).ready(function() {

var i = 1;
for(let i=1;i<11;i++){
  $('#constract_drawing'+i).on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i, $('#constract_drawing'+i)[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i;
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i).html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i).html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'text').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i).html(result);
          var text = document.getElementById('pdf'+i).innerHTML;
          document.getElementById('constract_drawing'+i+'text').value = text;
          $('#loading'+i).hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'b').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'b', $('#constract_drawing'+i+'b')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'b';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'b').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'b').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'btext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'b').html(result);
          var text = document.getElementById('pdf'+i+'b').innerHTML;
          document.getElementById('constract_drawing'+i+'btext').value = text;
          $('#loading'+i+'b').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'c').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'c', $('#constract_drawing'+i+'c')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'c';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'c').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'c').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'ctext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'c').html(result);
          var text = document.getElementById('pdf'+i+'c').innerHTML;
          document.getElementById('constract_drawing'+i+'ctext').value = text;
          $('#loading'+i+'c').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'d').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'d', $('#constract_drawing'+i+'d')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'d';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'d').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'d').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'dtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'d').html(result);
          var text = document.getElementById('pdf'+i+'d').innerHTML;
          document.getElementById('constract_drawing'+i+'dtext').value = text;
          $('#loading'+i+'d').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'e').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'e', $('#constract_drawing'+i+'e')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'e';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'e').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'e').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'etext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'e').html(result);
          var text = document.getElementById('pdf'+i+'e').innerHTML;
          document.getElementById('constract_drawing'+i+'etext').value = text;
          $('#loading'+i+'e').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'f').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'f', $('#constract_drawing'+i+'f')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'f';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'f').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'f').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'ftext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'f').html(result);
          var text = document.getElementById('pdf'+i+'f').innerHTML;
          document.getElementById('constract_drawing'+i+'ftext').value = text;
          $('#loading'+i+'f').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'g').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'g', $('#constract_drawing'+i+'g')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'g';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'g').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'g').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'gtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'g').html(result);
          var text = document.getElementById('pdf'+i+'g').innerHTML;
          document.getElementById('constract_drawing'+i+'gtext').value = text;
          $('#loading'+i+'g').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluea').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluea', $('#constract_drawing'+i+'bluea')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluea';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluea').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'bluea').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueatext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'bluea').html(result);
          var text = document.getElementById('pdf'+i+'bluea').innerHTML;
          document.getElementById('constract_drawing'+i+'blueatext').value = text;
          $('#loading'+i+'bluea').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blueb').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blueb', $('#constract_drawing'+i+'blueb')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueb';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blueb').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'blueb').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluebtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'blueb').html(result);
          var text = document.getElementById('pdf'+i+'blueb').innerHTML;
          document.getElementById('constract_drawing'+i+'bluebtext').value = text;
          $('#loading'+i+'blueb').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluec').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluec', $('#constract_drawing'+i+'bluec')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluec';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluec').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'bluec').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluectext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'bluec').html(result);
          var text = document.getElementById('pdf'+i+'bluec').innerHTML;
          document.getElementById('constract_drawing'+i+'bluectext').value = text;
          $('#loading'+i+'bluec').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blued').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blued', $('#constract_drawing'+i+'blued')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blued';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blued').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
            $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'blued').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluedtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'blued').html(result);
          var text = document.getElementById('pdf'+i+'blued').innerHTML;
          document.getElementById('constract_drawing'+i+'bluedtext').value = text;
          $('#loading'+i+'blued').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluee').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluee', $('#constract_drawing'+i+'bluee')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluee';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluee').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'bluee').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueetext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'bluee').html(result);
          var text = document.getElementById('pdf'+i+'bluee').innerHTML;
          document.getElementById('constract_drawing'+i+'blueetext').value = text;
          $('#loading'+i+'bluee').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluef').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluef', $('#constract_drawing'+i+'bluef')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluef';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluef').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'bluef').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueftext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'bluef').html(result);
          var text = document.getElementById('pdf'+i+'bluef').innerHTML;
          document.getElementById('constract_drawing'+i+'blueftext').value = text;
          $('#loading'+i+'bluef').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blueg').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blueg', $('#constract_drawing'+i+'blueg')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueg';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blueg').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#pdf'+i+'blueg').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluegtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#pdf'+i+'blueg').html(result);
          var text = document.getElementById('pdf'+i+'blueg').innerHTML;
          document.getElementById('constract_drawing'+i+'bluegtext').value = text;
          $('#loading'+i+'blueg').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

});
</script>
<script>
  
  $('.js-example-responsive').select2({
    width: 'resolve', // need to override the changed default
    placeholder: 'ご選択ください',
    language: { noResults: () => "業者データがありません",},
    escapeMarkup: function (markup) {
        return markup;
    },
    allowClear: true,
    minimumResultsForSearch: -1,
  });



  
  $("#duplicate-address").change(function() {
    if ($("#duplicate-address:checked").length > 0) {
      bindGroups();
    } else {
      unbindGroups();
    }
  });

  $("#duplicate-address2").change(function() {
    if ($("#duplicate-address2:checked").length > 0) {
      bindGroups2();
    } else {
      unbindGroups2();
    }
  });

  $("#duplicate-address3").change(function() {
    if ($("#duplicate-address3:checked").length > 0) {
      bindGroups3();
    } else {
      unbindGroups3();
    }
  });


  $("#duplicate-address4").change(function() {
    if ($("#duplicate-address4:checked").length > 0) {
      bindGroups4();
    } else {
      unbindGroups4();
    }
  });

  $("#duplicate-address5").change(function() {
    if ($("#duplicate-address5:checked").length > 0) {
      bindGroups5();
    } else {
      unbindGroups5();
    }
  });

  $("#duplicate-address6").change(function() {
    if ($("#duplicate-address6:checked").length > 0) {
      bindGroups6();
    } else {
      unbindGroups6();
    }
  });

  $("#duplicate-address7").change(function() {
    if ($("#duplicate-address7:checked").length > 0) {
      bindGroups7();
    } else {
      unbindGroups7();
    }
  });



  var bindGroups = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluea').on("change.mychange", function() {
          $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");
      });
    }
  };

  var unbindGroups = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluea').val('').trigger('change');
    // }
  };


  var bindGroups2 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueb').on("change.mychange", function() {
          $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");
      });
    }
  };

  var unbindGroups2 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueb').val('').trigger('change');
    // }
  };


  var bindGroups3 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluec').on("change.mychange", function() {
          $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");
      });
    }
  };

  var unbindGroups3 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluec').val('').trigger('change');
    // }
  };


  var bindGroups4 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blued').on("change.mychange", function() {
          $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");
      });
    }
  };

  var unbindGroups4 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blued').val('').trigger('change');
    // }
  };

  var bindGroups5 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluee').on("change.mychange", function() {
          $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");
      });
    }
  };

  var unbindGroups5 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluee').val('').trigger('change');
    // }
  };

  var bindGroups6 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluef').on("change.mychange", function() {
          $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");
      });
    }
  };

  var unbindGroups6 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluef').val('').trigger('change');
    // }
  };


  var bindGroups7 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueg').on("change.mychange", function() {
          $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");
      });
    }
  };

  var unbindGroups7 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueg').val('').trigger('change');
    // }
  };

  var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("contract_date").setAttribute("max", "9999-12-31");
document.getElementById("construction_start_date").setAttribute("max", "9999-12-31");
document.getElementById("upper_building_date").setAttribute("max", "9999-12-31");
document.getElementById("completion_date").setAttribute("max", "9999-12-31");
document.getElementById("delivery_date").setAttribute("max", "9999-12-31");

$("#sales_staff1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#sales_staff1").show();
        $("#sales_staff2").hide();
    } else {
        $("#sales_staff1").hide();
        $("#sales_staff2").show();
    }
});

$("#incharge_construction1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_construction1").show();
        $("#incharge_construction2").hide();
    } else {
        $("#incharge_construction1").hide();
        $("#incharge_construction2").show();
    }
});

$("#incharge_coordinator1").hide();
$(".coupon_question3").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_coordinator1").show();
        $("#incharge_coordinator2").hide();
    } else {
        $("#incharge_coordinator1").hide();
        $("#incharge_coordinator2").show();
    }
});
</script>
</body>
</html>