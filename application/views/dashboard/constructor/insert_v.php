<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事登録</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事登録</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_data" method="post" >
                        
                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード <span class="red">※必須</span></label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="">
                              <input type="hidden" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別 <span class="red">※必須</span></label>
                              <!-- <input type="text" class="form-control" name="kinds" id="kinds" aria-describedby="helpId" placeholder="">
                              <input type="hidden" class="form-control" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <!-- <select class="form-control" name="kinds">
                                <option selected disabled value="">ご選択ください</option>
                                <option value="SOB">Sweet Home</option>
                                <option value="BOB">ビリーブの家</option>
                                <option value="小工事">小工事</option>
                                <option value="建設中">建設中</option>
                                <option value="OB">OB</option>
                                <option value="材料販売">材料販売</option>
                                <option value="その他">その他</option>
                                <option value="NL">NL</option> -->

                                <select class="form-control" name="kinds" required oninvalid="this.setCustomValidity('選択してください')" onchange="setCustomValidity('')">
                                  <option selected disabled value="">選択してください</option>
                                  <option value="SOB">SWEET HOME</option>
                                  <option value="ビリーブの家">ビリーブの家</option>
                                  <option value="リフォーム工事">リフォーム工事</option>
                                  <option value="メンテナンス工事">メンテナンス工事</option>
                                  <option value="新築工事">新築工事</option>
                                  <option value="OB">OB</option>
                                  <option value="その他">その他</option>
                                  <option value="削除">削除</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No <span class="red">※必須</span></label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-12" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"></textarea>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;">
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <input type="text" class="form-control col-md-12" name="sales_staff1" id="sales_staff1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="sales_staff" id="sales_staff2">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_construction1" id="incharge_construction1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_construction" id="incharge_construction2">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_coordinator1" id="incharge_coordinator1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_coordinator" id="incharge_coordinator2">
                                <option value="">ご選択ください</option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                  foreach($data_employ as $row)
                                  { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                  }
                                }
                                else
                                {
                                  echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question3" type="checkbox" name="coupon_question3" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="construct_stat">工事状態</label>
                              <div class="radbut" style="flex:1;">
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat1" name="construct_stat" value="工事中"  >
                                      <label for="construct_stat1" class="form-check-label">工事中</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat2" name="construct_stat" value="工事終了"  >
                                      <label for="construct_stat2" class="form-check-label">工事終了</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:0;">
                          <div id="accordion" class="customAccordion">
                            
                            <!-- CATEGORY 1 -->
                            <input type="text"  name="category_name" id="category_name" value="請負契約書・土地契約書" hidden>

                            <div class="card">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>請負契約書・土地契約書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1text" name="constract_drawing1text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1">ファイルが選択されていません。</p>
                                          <div id="loading1"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1" name="check1" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2text" name="constract_drawing2text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2">ファイルが選択されていません。</p>
                                          <div id="loading2"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2" name="check2" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3text" name="constract_drawing3text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3">ファイルが選択されていません。</p>
                                          <div id="loading3"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3" name="check3" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4text" name="constract_drawing4text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4">ファイルが選択されていません。</p>
                                          <div id="loading4"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4" name="check4" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5text" name="constract_drawing5text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5">ファイルが選択されていません。</p>
                                          <div id="loading5"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5" name="check5" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6text" name="constract_drawing6text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6">ファイルが選択されていません。</p>
                                          <div id="loading6"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6" name="check6" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7text" name="constract_drawing7text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7">ファイルが選択されていません。</p>
                                          <div id="loading7"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7" name="check7" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8text" name="constract_drawing8text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8">ファイルが選択されていません。</p>
                                          <div id="loading8"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8" name="check8" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9text" name="constract_drawing9text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9">ファイルが選択されていません。</p>
                                          <div id="loading9"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9" name="check9" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10" name="constract_drawing10" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10text" name="constract_drawing10text">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10">ファイルが選択されていません。</p>
                                          <div id="loading10"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10" name="check10" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 2 -->
                            <input type="text" name="category_name1b" id="category_name1b" value="長期優良住宅認定書・性能評価書" hidden>

                            <div class="card">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>長期優良住宅認定書・性能評価書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1b" name="constract_drawing1b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1btext" name="constract_drawing1btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1b">ファイルが選択されていません。</p>
                                          <div id="loading1b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1b" name="check1b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2b" name="constract_drawing2b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2btext" name="constract_drawing2btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2b">ファイルが選択されていません。</p>
                                          <div id="loading2b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2b" name="check2b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3b" name="constract_drawing3b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3btext" name="constract_drawing3btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3b">ファイルが選択されていません。</p>
                                          <div id="loading3b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3b" name="check3b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4b" name="constract_drawing4b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4btext" name="constract_drawing4btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4b">ファイルが選択されていません。</p>
                                          <div id="loading4b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4b" name="check4b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5b" name="constract_drawing5b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5btext" name="constract_drawing5btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5b">ファイルが選択されていません。</p>
                                          <div id="loading5b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5b" name="check5b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6b" name="constract_drawing6b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6btext" name="constract_drawing6btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6b">ファイルが選択されていません。</p>
                                          <div id="loading6b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6b" name="check6b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7b" name="constract_drawing7b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7btext" name="constract_drawing7btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7b">ファイルが選択されていません。</p>
                                          <div id="loading7b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7b" name="check7b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8b" name="constract_drawing8b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8btext" name="constract_drawing8btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8b">ファイルが選択されていません。</p>
                                          <div id="loading8b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8b" name="check8b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9b" name="constract_drawing9b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9btext" name="constract_drawing9btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9b">ファイルが選択されていません。</p>
                                          <div id="loading9b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9b" name="check9b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10b" name="constract_drawing10b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10btext" name="constract_drawing10btext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10b">ファイルが選択されていません。</p>
                                          <div id="loading10b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10b" name="check10b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 3 -->
                            <input type="text" name="category_name1c" id="category_name1c" value="地盤調査・地盤改良" hidden>

                            <div class="card">
                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>地盤調査・地盤改良</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1c" name="constract_drawing1c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1ctext" name="constract_drawing1ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1c">ファイルが選択されていません。</p>
                                          <div id="loading1c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1c" name="check1c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2c" name="constract_drawing2c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2ctext" name="constract_drawing2ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2c">ファイルが選択されていません。</p>
                                          <div id="loading1c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2c" name="check2c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3c" name="constract_drawing3c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3ctext" name="constract_drawing3ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3c">ファイルが選択されていません。</p>
                                          <div id="loading3c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3c" name="check3c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4c" name="constract_drawing4c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4ctext" name="constract_drawing4ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4c">ファイルが選択されていません。</p>
                                          <div id="loading4c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4c" name="check4c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5c" name="constract_drawing5c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5ctext" name="constract_drawing5ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5c">ファイルが選択されていません。</p>
                                          <div id="loading5c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5c" name="check5c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6c" name="constract_drawing6c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6ctext" name="constract_drawing6ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6c">ファイルが選択されていません。</p>
                                          <div id="loading6c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6c" name="check6c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7c" name="constract_drawing7c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7ctext" name="constract_drawing7ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7c">ファイルが選択されていません。</p>
                                          <div id="loading7c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7c" name="check7c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8c" name="constract_drawing8c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8ctext" name="constract_drawing8ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8c">ファイルが選択されていません。</p>
                                          <div id="loading8c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8c" name="check8c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9c" name="constract_drawing9c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9ctext" name="constract_drawing9ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9c">ファイルが選択されていません。</p>
                                          <div id="loading9c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9c" name="check9c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10c" name="constract_drawing10c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10ctext" name="constract_drawing10ctext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10c">ファイルが選択されていません。</p>
                                          <div id="loading10c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10c" name="check10c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4 --> 
                            <input type="text" name="category_name1d" id="category_name1d" value="点検履歴" hidden>

                            <div class="card">
                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>点検履歴</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1d" name="constract_drawing1d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1dtext" name="constract_drawing1dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1d">ファイルが選択されていません。</p>
                                          <div id="loading1d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1d" name="check1d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2d" name="constract_drawing2d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2dtext" name="constract_drawing2dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2d">ファイルが選択されていません。</p>
                                          <div id="loading2d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2d" name="check2d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3d" name="constract_drawing3d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3dtext" name="constract_drawing3dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3d">ファイルが選択されていません。</p>
                                          <div id="loading3d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3d" name="check3d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4d" name="constract_drawing4d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4dtext" name="constract_drawing4dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4d">ファイルが選択されていません。</p>
                                          <div id="loading4d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4d" name="check4d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5d" name="constract_drawing5d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5dtext" name="constract_drawing5dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5d">ファイルが選択されていません。</p>
                                          <div id="loading5d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5d" name="check5d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6d" name="constract_drawing6d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6dtext" name="constract_drawing6dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6d">ファイルが選択されていません。</p>
                                          <div id="loading6d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6d" name="check6d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7d" name="constract_drawing7d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7dtext" name="constract_drawing7dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7d">ファイルが選択されていません。</p>
                                          <div id="loading7d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7d" name="check7d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8d" name="constract_drawing8d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8dtext" name="constract_drawing8dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8d">ファイルが選択されていません。</p>
                                          <div id="loading8d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8d" name="check8d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9d" name="constract_drawing9d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9dtext" name="constract_drawing9dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9d">ファイルが選択されていません。</p>
                                          <div id="loading9d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9d" name="check9d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10d" name="constract_drawing10d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10dtext" name="constract_drawing10dtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10d">ファイルが選択されていません。</p>
                                          <div id="loading10d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10d" name="check10d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5 -->
                            <input type="text" name="category_name1e" id="category_name1e" value="三者引継ぎ合意書" hidden>

                            <div class="card">
                              <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>三者引継ぎ合意書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1e" name="constract_drawing1e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1etext" name="constract_drawing1etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1e">ファイルが選択されていません。</p>
                                          <div id="loading1e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1e" name="check1e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2e" name="constract_drawing2e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2etext" name="constract_drawing2etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2e">ファイルが選択されていません。</p>
                                          <div id="loading2e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2e" name="check2e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3e" name="constract_drawing3e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3etext" name="constract_drawing3etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3e">ファイルが選択されていません。</p>
                                          <div id="loading3e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3e" name="check3e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4e" name="constract_drawing4e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4etext" name="constract_drawing4etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4e">ファイルが選択されていません。</p>
                                          <div id="loading4e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4e" name="check4e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5e" name="constract_drawing5e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5etext" name="constract_drawing5etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5e">ファイルが選択されていません。</p>
                                          <div id="loading5e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5e" name="check5e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6e" name="constract_drawing6e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6etext" name="constract_drawing6etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6e">ファイルが選択されていません。</p>
                                          <div id="loading6e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6e" name="check6e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7e" name="constract_drawing7e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7etext" name="constract_drawing7etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7e">ファイルが選択されていません。</p>
                                          <div id="loading7e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7e" name="check7e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8e" name="constract_drawing8e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8etext" name="constract_drawing8etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8e">ファイルが選択されていません。</p>
                                          <div id="loading8e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8e" name="check8e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9e" name="constract_drawing9e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9etext" name="constract_drawing9etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9e">ファイルが選択されていません。</p>
                                          <div id="loading9e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9e" name="check9e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10e" name="constract_drawing10e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10etext" name="constract_drawing10etext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10e">ファイルが選択されていません。</p>
                                          <div id="loading10e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10e" name="check10e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6 -->
                            <input type="text" name="category_name1f" id="category_name1f" value="住宅仕様書・プレカット資料" hidden>

                            <div class="card">
                              <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>住宅仕様書・プレカット資料</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1f" name="constract_drawing1f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1ftext" name="constract_drawing1ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1f">ファイルが選択されていません。</p>
                                          <div id="loading1f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1f" name="check1f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2f" name="constract_drawing2f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2ftext" name="constract_drawing2ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2f">ファイルが選択されていません。</p>
                                          <div id="loading2f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2f" name="check2f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3f" name="constract_drawing3f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3ftext" name="constract_drawing3ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3f">ファイルが選択されていません。</p>
                                          <div id="loading3f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3f" name="check3f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4f" name="constract_drawing4f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4ftext" name="constract_drawing4ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4f">ファイルが選択されていません。</p>
                                          <div id="loading4f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4f" name="check4f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5f" name="constract_drawing5f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5ftext" name="constract_drawing5ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5f">ファイルが選択されていません。</p>
                                          <div id="loading5f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5f" name="check5f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6f" name="constract_drawing6f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6ftext" name="constract_drawing6ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6f">ファイルが選択されていません。</p>
                                          <div id="loading6f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6f" name="check6f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7f" name="constract_drawing7f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7ftext" name="constract_drawing7ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7f">ファイルが選択されていません。</p>
                                          <div id="loading7f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7f" name="check7f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8f" name="constract_drawing8f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8ftext" name="constract_drawing8ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8f">ファイルが選択されていません。</p>
                                          <div id="loading8f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8f" name="check8f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9f" name="constract_drawing9f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9ftext" name="constract_drawing9ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9f">ファイルが選択されていません。</p>
                                          <div id="loading9f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9f" name="check9f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10f" name="constract_drawing10f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10ftext" name="constract_drawing10ftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10f">ファイルが選択されていません。</p>
                                          <div id="loading10f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10f" name="check10f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 7 -->
                            <input type="text" name="category_name1g" id="category_name1g" value="工事写真" hidden>

                            <div class="card">
                              <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>工事写真</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column7">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1g" name="constract_drawing1g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1gtext" name="constract_drawing1gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1g">ファイルが選択されていません。</p>
                                          <div id="loading1g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1g" name="check1g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2g" name="constract_drawing2g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2gtext" name="constract_drawing2gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2g">ファイルが選択されていません。</p>
                                          <div id="loading2g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2g" name="check2g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3g" name="constract_drawing3g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3gtext" name="constract_drawing3gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3g">ファイルが選択されていません。</p>
                                          <div id="loading3g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3g" name="check3g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4g" name="constract_drawing4g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4gtext" name="constract_drawing4gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4g">ファイルが選択されていません。</p>
                                          <div id="loading4g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4g" name="check4g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5g" name="constract_drawing5g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5gtext" name="constract_drawing5gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5g">ファイルが選択されていません。</p>
                                          <div id="loading5g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5g" name="check5g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6g" name="constract_drawing6g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6gtext" name="constract_drawing6gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6g">ファイルが選択されていません。</p>
                                          <div id="loading6g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6g" name="check6g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7g" name="constract_drawing7g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7gtext" name="constract_drawing7gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7g">ファイルが選択されていません。</p>
                                          <div id="loading7g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7g" name="check7g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8g" name="constract_drawing8g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8gtext" name="constract_drawing8gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8g">ファイルが選択されていません。</p>
                                          <div id="loading8g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8g" name="check8g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9g" name="constract_drawing9g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9gtext" name="constract_drawing9gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9g">ファイルが選択されていません。</p>
                                          <div id="loading9g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9g" name="check9g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10g" name="constract_drawing10g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10gtext" name="constract_drawing10gtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10g">ファイルが選択されていません。</p>
                                          <div id="loading10g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10g" name="check10g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 1BLUE -->
                            <input type="text" name="category_name1bluea" id="category_name1bluea" value="確認申請書一式" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingOneBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>確認申請書一式 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column1blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluea" name="constract_drawing1bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1blueatext" name="constract_drawing1blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluea">ファイルが選択されていません。</p>
                                          <div id="loading1bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluea" name="co_owner1bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address" name="duplicate-address">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluea" name="check1bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluea" name="constract_drawing2bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2blueatext" name="constract_drawing2blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluea">ファイルが選択されていません。</p>
                                          <div id="loading2bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluea" name="co_owner2bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluea" name="check2bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluea" name="constract_drawing3bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3blueatext" name="constract_drawing3blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluea">ファイルが選択されていません。</p>
                                          <div id="loading3bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluea" name="co_owner3bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluea" name="check3bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluea" name="constract_drawing4bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4blueatext" name="constract_drawing4blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluea">ファイルが選択されていません。</p>
                                          <div id="loading4bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluea" name="co_owner4bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluea" name="check4bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluea" name="constract_drawing5bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5blueatext" name="constract_drawing5blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluea">ファイルが選択されていません。</p>
                                          <div id="loading5bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluea" name="co_owner5bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluea" name="check5bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluea" name="constract_drawing6bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6blueatext" name="constract_drawing6blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluea">ファイルが選択されていません。</p>
                                          <div id="loading6bluea"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluea" name="co_owner6bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluea" name="check6bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluea" name="constract_drawing7bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7blueatext" name="constract_drawing7blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluea">ファイルが選択されていません。</p>
                                          <div id="loading7bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluea" name="co_owner7bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluea" name="check7bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluea" name="constract_drawing8bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8blueatext" name="constract_drawing8blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluea">ファイルが選択されていません。</p>
                                          <div id="loading8bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluea" name="co_owner8bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluea" name="check8bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluea" name="constract_drawing9bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9blueatext" name="constract_drawing9blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluea">ファイルが選択されていません。</p>
                                          <div id="loading9bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluea" name="co_owner9bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluea" name="check9bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluea" name="constract_drawing10bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10blueatext" name="constract_drawing10blueatext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluea">ファイルが選択されていません。</p>
                                          <div id="loading10bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluea" name="co_owner10bluea[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluea" name="check10bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 2BLUE -->
                            <input type="text" name="category_name1blueb" id="category_name1blueb" value="工事工程表" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingTwoBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>工事工程表 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column2blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1blueb" name="constract_drawing1blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1bluebtext" name="constract_drawing1bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1blueb">ファイルが選択されていません。</p>
                                          <div id="loading1blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueb" name="co_owner1blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address2" name="duplicate-address2">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueb" name="check1blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2blueb" name="constract_drawing2blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2bluebtext" name="constract_drawing2bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2blueb">ファイルが選択されていません。</p>
                                          <div id="loading2blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueb" name="co_owner2blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueb" name="check2blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3blueb" name="constract_drawing3blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3bluebtext" name="constract_drawing3bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3blueb">ファイルが選択されていません。</p>
                                          <div id="loading3blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueb" name="co_owner3blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueb" name="check3blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4blueb" name="constract_drawing4blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4bluebtext" name="constract_drawing4bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4blueb">ファイルが選択されていません。</p>
                                          <div id="loading4blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueb" name="co_owner4blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueb" name="check4blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5blueb" name="constract_drawing5blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5bluebtext" name="constract_drawing5bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5blueb">ファイルが選択されていません。</p>
                                          <div id="loading5blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueb" name="co_owner5blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueb" name="check5blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6blueb" name="constract_drawing6blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6bluebtext" name="constract_drawing6bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6blueb">ファイルが選択されていません。</p>
                                          <div id="loading6blueb"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueb" name="co_owner6blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueb" name="check6blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7blueb" name="constract_drawing7blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7bluebtext" name="constract_drawing7bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7blueb">ファイルが選択されていません。</p>
                                          <div id="loading7blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueb" name="co_owner7blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueb" name="check7blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8blueb" name="constract_drawing8blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8bluebtext" name="constract_drawing8bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8blueb">ファイルが選択されていません。</p>
                                          <div id="loading8blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueb" name="co_owner8blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueb" name="check8blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9blueb" name="constract_drawing9blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9bluebtext" name="constract_drawing9bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9blueb">ファイルが選択されていません。</p>
                                          <div id="loading9blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueb" name="co_owner9blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueb" name="check9blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10blueb" name="constract_drawing10blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10bluebtext" name="constract_drawing10bluebtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10blueb">ファイルが選択されていません。</p>
                                          <div id="loading10blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueb" name="co_owner10blueb[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueb" name="check10blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 3BLUE -->
                            <input type="text" name="category_name1bluec" id="category_name1bluec" value="工事着工資料" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingThreeBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>工事着工資料  </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column3blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluec" name="constract_drawing1bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1bluectext" name="constract_drawing1bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluec">ファイルが選択されていません。</p>
                                          <div id="loading1bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluec" name="co_owner1bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address3" name="duplicate-address3">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluec" name="check1bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluec" name="constract_drawing2bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2bluectext" name="constract_drawing2bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluec">ファイルが選択されていません。</p>
                                          <div id="loading2bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluec" name="co_owner2bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluec" name="check2bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluec" name="constract_drawing3bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3bluectext" name="constract_drawing3bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluec">ファイルが選択されていません。</p>
                                          <div id="loading3bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluec" name="co_owner3bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluec" name="check3bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluec" name="constract_drawing4bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4bluectext" name="constract_drawing4bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluec">ファイルが選択されていません。</p>
                                          <div id="loading4bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluec" name="co_owner4bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluec" name="check4bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluec" name="constract_drawing5bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5bluectext" name="constract_drawing5bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluec">ファイルが選択されていません。</p>
                                          <div id="loading5bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluec" name="co_owner5bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluec" name="check5bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluec" name="constract_drawing6bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6bluectext" name="constract_drawing6bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluec">ファイルが選択されていません。</p>
                                          <div id="loading6bluec"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluec" name="co_owner6bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluec" name="check6bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluec" name="constract_drawing7bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7bluectext" name="constract_drawing7bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluec">ファイルが選択されていません。</p>
                                          <div id="loading7bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluec" name="co_owner7bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluec" name="check7bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluec" name="constract_drawing8bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8bluectext" name="constract_drawing8bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluec">ファイルが選択されていません。</p>
                                          <div id="loading8bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluec" name="co_owner8bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluec" name="check8bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluec" name="constract_drawing9bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9bluectext" name="constract_drawing9bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluec">ファイルが選択されていません。</p>
                                          <div id="loading9bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluec" name="co_owner9bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluec" name="check9bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluec" name="constract_drawing10bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10bluectext" name="constract_drawing10bluectext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluec">ファイルが選択されていません。</p>
                                          <div id="loading10bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluec" name="co_owner10bluec[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluec" name="check10bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4BLUE -->
                            <input type="text" name="category_name1blued" id="category_name1blued" value="施工図面" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingFourBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>施工図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column4blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1blued" name="constract_drawing1blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1bluedtext" name="constract_drawing1bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1blued">ファイルが選択されていません。</p>
                                          <div id="loading1blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blued" name="co_owner1blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address4" name="duplicate-address4">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blued" name="check1blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2blued" name="constract_drawing2blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2bluedtext" name="constract_drawing2bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2blued">ファイルが選択されていません。</p>
                                          <div id="loading2blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blued" name="co_owner2blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blued" name="check2blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3blued" name="constract_drawing3blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3bluedtext" name="constract_drawing3bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3blued">ファイルが選択されていません。</p>
                                          <div id="loading3blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blued" name="co_owner3blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blued" name="check3blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4blued" name="constract_drawing4blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4bluedtext" name="constract_drawing4bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4blued">ファイルが選択されていません。</p>
                                          <div id="loading4blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blued" name="co_owner4blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blued" name="check4blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5blued" name="constract_drawing5blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5bluedtext" name="constract_drawing5bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5blued">ファイルが選択されていません。</p>
                                          <div id="loading5blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blued" name="co_owner5blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blued" name="check5blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6blued" name="constract_drawing6blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6bluedtext" name="constract_drawing6bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6blued">ファイルが選択されていません。</p>
                                          <div id="loading6blued"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blued" name="co_owner6blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blued" name="check6blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7blued" name="constract_drawing7blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7bluedtext" name="constract_drawing7bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7blued">ファイルが選択されていません。</p>
                                          <div id="loading7blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blued" name="co_owner7blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blued" name="check7blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8blued" name="constract_drawing8blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8bluedtext" name="constract_drawing8bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8blued">ファイルが選択されていません。</p>
                                          <div id="loading8blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blued" name="co_owner8blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blued" name="check8blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9blued" name="constract_drawing9blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9bluedtext" name="constract_drawing9bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9blued">ファイルが選択されていません。</p>
                                          <div id="loading9blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blued" name="co_owner9blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blued" name="check9blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10blued" name="constract_drawing10blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10bluedtext" name="constract_drawing10bluedtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10blued">ファイルが選択されていません。</p>
                                          <div id="loading10blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blued" name="co_owner10blued[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blued" name="check10blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5BLUE -->
                            <input type="text" name="category_name1bluee" id="category_name1bluee" value="変更図面" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingFiveBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>変更図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column5blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluee" name="constract_drawing1bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1blueetext" name="constract_drawing1blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluee">ファイルが選択されていません。</p>
                                          <div id="loading1bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluee" name="co_owner1bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address5" name="duplicate-address5">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluee" name="check1bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluee" name="constract_drawing2bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2blueetext" name="constract_drawing2blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluee">ファイルが選択されていません。</p>
                                          <div id="loading2bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluee" name="co_owner2bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluee" name="check2bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluee" name="constract_drawing3bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3blueetext" name="constract_drawing3blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluee">ファイルが選択されていません。</p>
                                          <div id="loading3bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluee" name="co_owner3bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluee" name="check3bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluee" name="constract_drawing4bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4blueetext" name="constract_drawing4blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluee">ファイルが選択されていません。</p>
                                          <div id="loading4bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluee" name="co_owner4bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluee" name="check4bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluee" name="constract_drawing5bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5blueetext" name="constract_drawing5blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluee">ファイルが選択されていません。</p>
                                          <div id="loading5bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluee" name="co_owner5bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluee" name="check5bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluee" name="constract_drawing6bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6blueetext" name="constract_drawing6blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluee">ファイルが選択されていません。</p>
                                          <div id="loading6bluee"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluee" name="co_owner6bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluee" name="check6bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluee" name="constract_drawing7bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7blueetext" name="constract_drawing7blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluee">ファイルが選択されていません。</p>
                                          <div id="loading7bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluee" name="co_owner7bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluee" name="check7bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluee" name="constract_drawing8bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8blueetext" name="constract_drawing8blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluee">ファイルが選択されていません。</p>
                                          <div id="loading8bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluee" name="co_owner8bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluee" name="check8bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluee" name="constract_drawing9bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9blueetext" name="constract_drawing9blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluee">ファイルが選択されていません。</p>
                                          <div id="loading9bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluee" name="co_owner9bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluee" name="check9bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluee" name="constract_drawing10bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10blueetext" name="constract_drawing10blueetext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluee">ファイルが選択されていません。</p>
                                          <div id="loading10bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluee" name="co_owner10bluee[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluee" name="check10bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6BLUE -->
                            <input type="text" name="category_name1bluef" id="category_name1bluef" value="設備プランシート" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingSixBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>設備プランシート</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column6blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluef" name="constract_drawing1bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1blueftext" name="constract_drawing1blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1bluef">ファイルが選択されていません。</p>
                                          <div id="loading1bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluef" name="co_owner1bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address6" name="duplicate-address6">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1bluef" name="check1bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluef" name="constract_drawing2bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2blueftext" name="constract_drawing2blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2bluef">ファイルが選択されていません。</p>
                                          <div id="loading2bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluef" name="co_owner2bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2bluef" name="check2bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluef" name="constract_drawing3bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3blueftext" name="constract_drawing3blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3bluef">ファイルが選択されていません。</p>
                                          <div id="loading3bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluef" name="co_owner3bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3bluef" name="check3bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluef" name="constract_drawing4bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4blueftext" name="constract_drawing4blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4bluef">ファイルが選択されていません。</p>
                                          <div id="loading4bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluef" name="co_owner4bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4bluef" name="check4bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluef" name="constract_drawing5bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5blueftext" name="constract_drawing5blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5bluef">ファイルが選択されていません。</p>
                                          <div id="loading5bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluef" name="co_owner5bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5bluef" name="check5bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluef" name="constract_drawing6bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6blueftext" name="constract_drawing6blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6bluef">ファイルが選択されていません。</p>
                                          <div id="loading6bluef"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluef" name="co_owner6bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6bluef" name="check6bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluef" name="constract_drawing7bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7blueftext" name="constract_drawing7blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7bluef">ファイルが選択されていません。</p>
                                          <div id="loading7bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluef" name="co_owner7bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7bluef" name="check7bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluef" name="constract_drawing8bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8blueftext" name="constract_drawing8blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8bluef">ファイルが選択されていません。</p>
                                          <div id="loading8bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluef" name="co_owner8bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8bluef" name="check8bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluef" name="constract_drawing9bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9blueftext" name="constract_drawing9blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9bluef">ファイルが選択されていません。</p>
                                          <div id="loading9bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluef" name="co_owner9bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9bluef" name="check9bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluef" name="constract_drawing10bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10blueftext" name="constract_drawing10blueftext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10bluef">ファイルが選択されていません。</p>
                                          <div id="loading10bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluef" name="co_owner10bluef[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10bluef" name="check10bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 7BLUE -->
                            <input type="text" name="category_name1blueg" id="category_name1blueg" value="その他" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingSevenBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7blue" role="button" aria-expanded="false" aria-controls="column7blue">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>その他</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse" id="column7blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing1blueg" name="constract_drawing1blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing1bluegtext" name="constract_drawing1bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files1blueg">ファイルが選択されていません。</p>
                                          <div id="loading1blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueg" name="co_owner1blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address7" name="duplicate-address7">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check1blueg" name="check1blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueg" id="desc_item1blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing2blueg" name="constract_drawing2blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing2bluegtext" name="constract_drawing2bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files2blueg">ファイルが選択されていません。</p>
                                          <div id="loading2blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueg" name="co_owner2blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check2blueg" name="check2blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueg" id="desc_item2blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing3blueg" name="constract_drawing3blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing3bluegtext" name="constract_drawing3bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files3blueg">ファイルが選択されていません。</p>
                                          <div id="loading3blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueg" name="co_owner3blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check3blueg" name="check3blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueg" id="desc_item3blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing4blueg" name="constract_drawing4blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing4bluegtext" name="constract_drawing4bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files4blueg">ファイルが選択されていません。</p>
                                          <div id="loading4blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueg" name="co_owner4blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check4blueg" name="check4blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueg" id="desc_item4blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing5blueg" name="constract_drawing5blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing5bluegtext" name="constract_drawing5bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files5blueg">ファイルが選択されていません。</p>
                                          <div id="loading5blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueg" name="co_owner5blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check5blueg" name="check5blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueg" id="desc_item5blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing6blueg" name="constract_drawing6blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing6bluegtext" name="constract_drawing6bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files6blueg">ファイルが選択されていません。</p>
                                          <div id="loading6blueg"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueg" name="co_owner6blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check6blueg" name="check6blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueg" id="desc_item6blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing7blueg" name="constract_drawing7blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing7bluegtext" name="constract_drawing7bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files7blueg">ファイルが選択されていません。</p>
                                          <div id="loading7blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueg" name="co_owner7blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check7blueg" name="check7blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueg" id="desc_item7blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing8blueg" name="constract_drawing8blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing8bluegtext" name="constract_drawing8bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files8blueg">ファイルが選択されていません。</p>
                                          <div id="loading8blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueg" name="co_owner8blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check8blueg" name="check8blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueg" id="desc_item8blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing9blueg" name="constract_drawing9blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing9bluegtext" name="constract_drawing9bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files9blueg">ファイルが選択されていません。</p>
                                          <div id="loading9blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueg" name="co_owner9blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check9blueg" name="check9blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueg" id="desc_item9blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="constract_drawing10blueg" name="constract_drawing10blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls">
                                            <input type="hidden" class="control" id="constract_drawing10bluegtext" name="constract_drawing10bluegtext">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                          </div>
                                          <p id="files10blueg">ファイルが選択されていません。</p>
                                          <div id="loading10blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueg" name="co_owner10blueg[]" multiple style="width: 100%">
                                            <?php 
                                            if(!empty($data))
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                            else
                                            {
                                              echo '<option value="">社員データなし</option>';
                                            }
                                            ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4> -->
                                          <!-- <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" id="check10blueg" name="check10blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueg" id="desc_item10blueg" aria-describedby="helpId" placeholder="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>

                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->
                        <br><br>
                        <button name="" id="myBtn" class="btn custom-btn hvr-sink" type="submit">登録する</button>
                        <br><br>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?= base_url() ?>assets/dashboard/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->
<script>

  $(document).ready(function(){
    
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
            $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
            // $('[name="kinds"]').val(ui.item.another); 
        }
    });

  });

  $("#cust_code").keydown(function(e) {
    var myInput = document.getElementById("cust_code");
    if (myInput && myInput.value) {
      document.getElementById('cust_name').disabled = true; 
      // document.getElementById('kinds').disabled = false;
    }
  });
  

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });


  var i = 1;
  for(let i=1;i<11;i++){

    $('#constract_drawing'+i).on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i).innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'b').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'b').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'c').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'c').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

    $('#constract_drawing'+i+'d').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'d').innerText = cleanFileName;
    });

  }

  for(let i=1;i<11;i++){

  $('#constract_drawing'+i+'e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files'+i+'e').innerText = cleanFileName;
  });

  }

  for(let i=1;i<11;i++){

$('#constract_drawing'+i+'f').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'f').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'g').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'g').innerText = cleanFileName;
});

}


for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluea').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluea').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blueb').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blueb').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluec').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluec').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blued').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blued').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluee').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluee').innerText = cleanFileName;
});

}


for(let i=1;i<11;i++){

$('#constract_drawing'+i+'bluef').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluef').innerText = cleanFileName;
});

}

for(let i=1;i<11;i++){

$('#constract_drawing'+i+'blueg').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blueg').innerText = cleanFileName;
});

}



// $(document).ready(function() {
//   $(window).keydown(function(event){
//     if(event.keyCode == 13) {
//       event.preventDefault();
//       return false;
//     }
//   });
// });
$(document).on("keydown", ":input:not(textarea)", function(event) {
    return event.key != "Enter";
});
</script>
<script>
$(document).ready(function() {

  var i = 1;
  for(let i=1;i<11;i++){
    $('#constract_drawing'+i).on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i, $('#constract_drawing'+i)[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i;
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i).html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
            $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i).html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'text').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i).html(result);
            var text = document.getElementById('files'+i).innerHTML;
            document.getElementById('constract_drawing'+i+'text').value = text;
            $('#loading'+i).hide();
            $('.jconfirm').remove();

          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'b').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'b', $('#constract_drawing'+i+'b')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'b';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'b').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'b').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'btext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'b').html(result);
            var text = document.getElementById('files'+i+'b').innerHTML;
            document.getElementById('constract_drawing'+i+'btext').value = text;
            $('#loading'+i+'b').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'c').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'c', $('#constract_drawing'+i+'c')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'c';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'c').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'c').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'ctext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'c').html(result);
            var text = document.getElementById('files'+i+'c').innerHTML;
            document.getElementById('constract_drawing'+i+'ctext').value = text;
            $('#loading'+i+'c').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'d').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'d', $('#constract_drawing'+i+'d')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'d';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'d').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'d').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'dtext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'d').html(result);
            var text = document.getElementById('files'+i+'d').innerHTML;
            document.getElementById('constract_drawing'+i+'dtext').value = text;
            $('#loading'+i+'d').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'e').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'e', $('#constract_drawing'+i+'e')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'e';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'e').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'e').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'etext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'e').html(result);
            var text = document.getElementById('files'+i+'e').innerHTML;
            document.getElementById('constract_drawing'+i+'etext').value = text;
            $('#loading'+i+'e').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'f').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'f', $('#constract_drawing'+i+'f')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'f';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'f').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'f').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'ftext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'f').html(result);
            var text = document.getElementById('files'+i+'f').innerHTML;
            document.getElementById('constract_drawing'+i+'ftext').value = text;
            $('#loading'+i+'f').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'g').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'g', $('#constract_drawing'+i+'g')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'g';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'g').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'g').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'gtext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'g').html(result);
            var text = document.getElementById('files'+i+'g').innerHTML;
            document.getElementById('constract_drawing'+i+'gtext').value = text;
            $('#loading'+i+'g').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'bluea').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'bluea', $('#constract_drawing'+i+'bluea')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluea';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'bluea').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'bluea').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'blueatext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'bluea').html(result);
            var text = document.getElementById('files'+i+'bluea').innerHTML;
            document.getElementById('constract_drawing'+i+'blueatext').value = text;
            $('#loading'+i+'bluea').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'blueb').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'blueb', $('#constract_drawing'+i+'blueb')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueb';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'blueb').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'blueb').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'bluebtext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'blueb').html(result);
            var text = document.getElementById('files'+i+'blueb').innerHTML;
            document.getElementById('constract_drawing'+i+'bluebtext').value = text;
            $('#loading'+i+'blueb').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'bluec').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'bluec', $('#constract_drawing'+i+'bluec')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluec';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'bluec').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'bluec').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'bluectext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'bluec').html(result);
            var text = document.getElementById('files'+i+'bluec').innerHTML;
            document.getElementById('constract_drawing'+i+'bluectext').value = text;
            $('#loading'+i+'bluec').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'blued').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'blued', $('#constract_drawing'+i+'blued')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blued';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'blued').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'blued').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'bluedtext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'blued').html(result);
            var text = document.getElementById('files'+i+'blued').innerHTML;
            document.getElementById('constract_drawing'+i+'bluedtext').value = text;
            $('#loading'+i+'blued').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'bluee').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'bluee', $('#constract_drawing'+i+'bluee')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluee';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'bluee').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'bluee').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'blueetext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'bluee').html(result);
            var text = document.getElementById('files'+i+'bluee').innerHTML;
            document.getElementById('constract_drawing'+i+'blueetext').value = text;
            $('#loading'+i+'bluee').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'bluef').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'bluef', $('#constract_drawing'+i+'bluef')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluef';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'bluef').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'bluef').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'blueftext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'bluef').html(result);
            var text = document.getElementById('files'+i+'bluef').innerHTML;
            document.getElementById('constract_drawing'+i+'blueftext').value = text;
            $('#loading'+i+'bluef').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

  for(let i=1;i<11;i++){
    $('#constract_drawing'+i+'blueg').on('change', function() {
      var imageData = new FormData();
      imageData.append('constract_drawing'+i+'blueg', $('#constract_drawing'+i+'blueg')[0].files[0]);
      var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueg';
      //Make ajax call here:
      $.ajax({
        url: ajax_url,
        type: 'POST',
        processData: false, // important
        contentType: false, // important
        data: imageData,
        beforeSend: function() {    
            // $('#loading'+i+'blueg').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
        success: function(result) {
          if (result == 'error') {
            // invalid file format.
            console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
            $('#files'+i+'blueg').html("ファイルが選択されていません。");
            document.getElementById('constract_drawing'+i+'bluegtext').value = "";
            return false;
          } else {
            // console.log(result);
            $('#files'+i+'blueg').html(result);
            var text = document.getElementById('files'+i+'blueg').innerHTML;
            document.getElementById('constract_drawing'+i+'bluegtext').value = text;
            $('#loading'+i+'blueg').hide();
            $('.jconfirm').remove();
            // document.getElementById("myBtn").disabled = false;
          }
        },
        error: function(result) {
          console.error(result);
        }
      });
    });
  }

});
</script>
<script>
  
  $('.js-example-responsive').select2({
    width: 'resolve', // need to override the changed default
    placeholder: 'ご選択ください',
    language: { noResults: () => "業者データがありません",},
    escapeMarkup: function (markup) {
        return markup;
    }
  });



  
  $("#duplicate-address").change(function() {
    if ($("#duplicate-address:checked").length > 0) {
      bindGroups();
    } else {
      unbindGroups();
    }
  });

  $("#duplicate-address2").change(function() {
    if ($("#duplicate-address2:checked").length > 0) {
      bindGroups2();
    } else {
      unbindGroups2();
    }
  });

  $("#duplicate-address3").change(function() {
    if ($("#duplicate-address3:checked").length > 0) {
      bindGroups3();
    } else {
      unbindGroups3();
    }
  });


  $("#duplicate-address4").change(function() {
    if ($("#duplicate-address4:checked").length > 0) {
      bindGroups4();
    } else {
      unbindGroups4();
    }
  });

  $("#duplicate-address5").change(function() {
    if ($("#duplicate-address5:checked").length > 0) {
      bindGroups5();
    } else {
      unbindGroups5();
    }
  });

  $("#duplicate-address6").change(function() {
    if ($("#duplicate-address6:checked").length > 0) {
      bindGroups6();
    } else {
      unbindGroups6();
    }
  });

  $("#duplicate-address7").change(function() {
    if ($("#duplicate-address7:checked").length > 0) {
      bindGroups7();
    } else {
      unbindGroups7();
    }
  });



  var bindGroups = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluea').on("change.mychange", function() {
          $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");
      });
    }
  };

  var unbindGroups = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluea').off("change.mychange");
    // }
  };


  var bindGroups2 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueb').on("change.mychange", function() {
          $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");
      });
    }
  };

  var unbindGroups2 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueb').off("change.mychange");
    // }
  };


  var bindGroups3 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluec').on("change.mychange", function() {
          $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");
      });
    }
  };

  var unbindGroups3 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluec').off("change.mychange");
    // }
  };


  var bindGroups4 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blued').on("change.mychange", function() {
          $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");
      });
    }
  };

  var unbindGroups4 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blued').off("change.mychange");
    // }
  };

  var bindGroups5 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluee').on("change.mychange", function() {
          $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");
      });
    }
  };

  var unbindGroups5 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluee').off("change.mychange");
    // }
  };

  var bindGroups6 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluef').on("change.mychange", function() {
          $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");
      });
    }
  };

  var unbindGroups6 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluef').off("change.mychange");
    // }
  };


  var bindGroups7 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueg').on("change.mychange", function() {
          $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");
      });
    }
  };

  var unbindGroups7 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueg').off("change.mychange");
    // }
  };


var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("contract_date").setAttribute("max", "9999-12-31");
document.getElementById("construction_start_date").setAttribute("max", "9999-12-31");
document.getElementById("upper_building_date").setAttribute("max", "9999-12-31");
document.getElementById("completion_date").setAttribute("max", "9999-12-31");
document.getElementById("delivery_date").setAttribute("max", "9999-12-31");


$("#sales_staff1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#sales_staff1").show();
        $("#sales_staff2").hide();
    } else {
        $("#sales_staff1").hide();
        $("#sales_staff2").show();
    }
});

$("#incharge_construction1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_construction1").show();
        $("#incharge_construction2").hide();
    } else {
        $("#incharge_construction1").hide();
        $("#incharge_construction2").show();
    }
});

$("#incharge_coordinator1").hide();
$(".coupon_question3").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_coordinator1").show();
        $("#incharge_coordinator2").hide();
    } else {
        $("#incharge_coordinator1").hide();
        $("#incharge_coordinator2").show();
    }
});




</script>
</body>
</html>