<?php $this->load->view('dashboard/dashboard_header');?>
<style>
    .my-custom-scrollbar {
        position: relative;
        height: 198px;
        overflow: auto;
        margin-bottom:20px;
    }
    .table-wrapper-scroll-y {
        display: block;
    }

    table.seratus {
        height: 100%;
    }
    
    .centering {
        display: flex;
        justify-content: center;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content view_custom">
      <div class="container-fluid">
        <!-- Main row -->
        <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
        <div class="row">
          <div class="col-xl-8 col-lg-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="button" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-trash"></i></a>
                                <a class="btn btn-info" href="<?=base_url()?>dashboard/view_constructor_list/<?=$data->construct_id?>" role="button"><i class="fa fa-file"></i></a>
                    </div>
                    <?php if(!empty($data->cust_code)) :?>
                      <div class="form-group">
                          <div class="flex-box">
                              <div class="label-view">
                                  <label for="cust_code">顧客コード</label>
                              </div>
                              <div class="text-view">
                                  <p>
                                      <?=$data->cust_code?>
                                  </p>
                              </div>
                          </div>
                      </div>
                    <?php endif;
                     if(!empty($data->cust_name)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="cust_name">顧客名</label>
                            </div>
                            <div class="text-view">
                              <p><?=$data->cust_name?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->kinds)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="kinds">種別</label>
                            </div>
                            <div class="text-view">
                            <p><?php if($data->kinds == 'SOB'){ echo 'SWEET HOME';} else { echo $data->kinds;}?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->const_no)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="const_no">工事No</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->const_no?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->remarks)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="remarks">備考</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->remarks?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;

                      // date_default_timezone_set('Asia/Tokyo');    

                      // $formatter = new IntlDateFormatter('ja_JP@calendar=japanese', IntlDateFormatter::MEDIUM,
                      // IntlDateFormatter::MEDIUM, 'Asia/Tokyo', IntlDateFormatter::TRADITIONAL);

                      // $now = new DateTime($data->contract_date);
                      // $now2 = new DateTime($data->construction_start_date);
                      // $now3 = new DateTime($data->upper_building_date);
                      // $now4 = new DateTime($data->completion_date);
                      // $now5 = new DateTime($data->delivery_date);


                      // $str = substr($formatter->format($now), 0, strrpos($formatter->format($now), ' '));
                      // $str2 = substr($formatter->format($now2), 0, strrpos($formatter->format($now2), ' '));
                      // $str3 = substr($formatter->format($now3), 0, strrpos($formatter->format($now3), ' '));
                      // $str4 = substr($formatter->format($now4), 0, strrpos($formatter->format($now4), ' '));
                      // $str5 = substr($formatter->format($now5), 0, strrpos($formatter->format($now5), ' '));

                     if(!empty($data->contract_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="contract_date">契約日</label>
                            </div>
                            <div class="text-view">
                            <p><?php if($data->contract_date == '0000-00-00' || $data->contract_date == '') echo ''; else echo $str;?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->construction_start_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="construction_start_date">着工日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->construction_start_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->upper_building_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="upper_building_date">上棟日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->upper_building_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->completion_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="completion_date">竣工日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->completion_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->delivery_date)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="delivery_date">引渡日</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->delivery_date?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->amount_money)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="amount_money">金額</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->amount_money?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->sales_staff)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="sales_staff">営業担当</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->sales_staff?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->incharge_construction)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="incharge_construction">工務担当</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->incharge_construction?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;
                     if(!empty($data->incharge_coordinator)) :
                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="incharge_coordinator">コーデ担当</label>
                            </div>
                            <div class="text-view">
                            <p><?=$data->incharge_coordinator?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif;

                    ?>
                    <div class="form-group">
                        <div class="flex-box">
                            <div class="label-view">
                            <label for="construct_stat">工事状態</label>
                            </div>
                            <div class="text-view">
                            <?php if(empty($data->construct_stat)  || $data->construct_stat == '工事中') :?>
                              <p>工事中</p>
                            <?php else :?>
                              <p>工事終了</p>
                            <?php endif;?>
                            </div>
                        </div>
                    </div>


<div id="accordion" class="customAccordion">

                        <!-- <label class="centering">ファイル履歴 契約書</label>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered table-striped mb-0 seratus">
                                <tbody>
                                    <?php

                                    $arrEmail1 = explode(';',$data->contract_history);
                                    $arrName1 = explode(';',$data->date_insert_history1);

                                    $arrGoal1;

                                    $arrLength1 = count($arrName1);

                                    //creating two-dimensional Array
                                    for($i = 0; $i < $arrLength1; $i++){ 
                                        $arrGoal1[$i] = array($arrName1[$i], $arrEmail1[$i]);
                                        //should look something like this ((name, email),(name, email)…)
                                    }

                                    $arrGoalLength1 = count($arrGoal1);

                                    //accessing Array
                                    //1. dimension
                                    for($i = 0; $i < $arrGoalLength1; $i++) :
                                    //2. dimension
                                        //Variables should be global (but aren't)
                                        $newName1 = $arrGoal1[$i][0];
                                        $newEmail1 = $arrGoal1[$i][1];

                                    ?>
                                    <tr>
                                        <td>Category</td>
                                        <td>Number</td>
                                        <td><?=$newName1 = $arrGoal1[$i][0]?></td>
                                        <td><?=$newEmail1 = $arrGoal1[$i][1]?></td>
                                    </tr>
                                    <?php 
                                    endfor;
                                    ?>
                                </tbody>
                            </table>
                        </div> -->
                      
                      <!-- CATEGORY 1 -->
                      <div class="card">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                              <div class="leftSide">
                                <span>1</span>
                                <p>請負契約書・土地契約書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name == "請負契約書・土地契約書" && !empty($data->constract_drawing1)){echo "show";} ?>" id="column1">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing1;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing1;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1" name="constract_drawing1"
                                    value="<?php if(!empty($data->constract_drawing1) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing1; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing1;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1"
                                  value="<?php if(!empty($data->constract_drawing1) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing1;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item1;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item1; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing2;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing2;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2" name="constract_drawing2"
                                    value="<?php if(!empty($data->constract_drawing2) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing2; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing2;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2"
                                  value="<?php if(!empty($data->constract_drawing2) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing2;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item2;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item2; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing3;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing3;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3" name="constract_drawing3"
                                    value="<?php if(!empty($data->constract_drawing3) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing3; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing3;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3"
                                  value="<?php if(!empty($data->constract_drawing3) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing3;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item3;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item3; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing4;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing4;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4" name="constract_drawing4"
                                    value="<?php if(!empty($data->constract_drawing4) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing4; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing4;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4"
                                  value="<?php if(!empty($data->constract_drawing4) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing4;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item4;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item4; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing5;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing5;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5" name="constract_drawing5"
                                    value="<?php if(!empty($data->constract_drawing5) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing5; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing5;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5"
                                  value="<?php if(!empty($data->constract_drawing5) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing5;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item5;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item5; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing6;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing6;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6" name="constract_drawing6"
                                    value="<?php if(!empty($data->constract_drawing6) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing6; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing6;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6"
                                  value="<?php if(!empty($data->constract_drawing6) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing6;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item6;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item6; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing7;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing7;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7" name="constract_drawing7"
                                    value="<?php if(!empty($data->constract_drawing7) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing7; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing7;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7"
                                  value="<?php if(!empty($data->constract_drawing7) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing7;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item7;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item7; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing8;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing8;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8" name="constract_drawing8"
                                    value="<?php if(!empty($data->constract_drawing8) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing8; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing8;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8"
                                  value="<?php if(!empty($data->constract_drawing8) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing8;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item8;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item8; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing9;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing9;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9" name="constract_drawing9"
                                    value="<?php if(!empty($data->constract_drawing9) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing9; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing9;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9"
                                  value="<?php if(!empty($data->constract_drawing9) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing9;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item9;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item9; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing10;}?>"><?php if($data->category_name == "請負契約書・土地契約書"){echo $data->constract_drawing10;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10" name="constract_drawing10"
                                    value="<?php if(!empty($data->constract_drawing10) && $data->category_name == "請負契約書・土地契約書"){ echo $data->constract_drawing10; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing10;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10"
                                  value="<?php if(!empty($data->constract_drawing10) && $data->category_name == "請負契約書・土地契約書") { echo $data->date_insert_drawing10;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10">コメント</label>
                                      <p>
                                        <?php if($data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item10;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10) && $data->category_name == "請負契約書・土地契約書"){ echo $data->desc_item10; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10) && $data->category_name == "請負契約書・土地契約書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 2 -->
                      <div class="card">
                        <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                              <div class="leftSide">
                                <span>2</span>
                                <p>長期優良住宅認定書・性能評価書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書" && !empty($data->constract_drawing1b)){echo "show";} ?>" id="column2">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing1b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing1b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1b" name="constract_drawing1b"
                                    value="<?php if(!empty($data->constract_drawing1b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing1b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing1b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1b"
                                  value="<?php if(!empty($data->constract_drawing1b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing1b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item1b; }?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1b)){ echo $data->desc_item1b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing2b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing2b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2b" name="constract_drawing2b"
                                    value="<?php if(!empty($data->constract_drawing2b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing2b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing2b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2b"
                                  value="<?php if(!empty($data->constract_drawing2b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing2b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item2b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item2b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing3b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing3b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3b" name="constract_drawing3b"
                                    value="<?php if(!empty($data->constract_drawing3b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing3b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing3b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3b"
                                  value="<?php if(!empty($data->constract_drawing3b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing3b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item3;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item3b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing4b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing4b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4b" name="constract_drawing4b"
                                    value="<?php if(!empty($data->constract_drawing4b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing4b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing4b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4b"
                                  value="<?php if(!empty($data->constract_drawing4b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing4b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item4b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item4b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing5b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing5b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5b" name="constract_drawing5b"
                                    value="<?php if(!empty($data->constract_drawing5b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing5b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing5b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5b"
                                  value="<?php if(!empty($data->constract_drawing5b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing5b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item5b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item5b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing6b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing6b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6b" name="constract_drawing6b"
                                    value="<?php if(!empty($data->constract_drawing6b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing6b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing6b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6b"
                                  value="<?php if(!empty($data->constract_drawing6b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing6b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item6b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item6b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing7b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing7b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7b" name="constract_drawing7b"
                                    value="<?php if(!empty($data->constract_drawing7b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing7b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing7b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7b"
                                  value="<?php if(!empty($data->constract_drawing7b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing7b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item7b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item7b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing8b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing8b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8b" name="constract_drawing8b"
                                    value="<?php if(!empty($data->constract_drawing8b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing8b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing8b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8b"
                                  value="<?php if(!empty($data->constract_drawing8b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing8b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item8b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item8b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing9b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing9b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9b" name="constract_drawing9b"
                                    value="<?php if(!empty($data->constract_drawing9b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing9b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing9b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9b"
                                  value="<?php if(!empty($data->constract_drawing9b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing9b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item9b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item9b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing10b;}?>"><?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){echo $data->constract_drawing10b;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10b" name="constract_drawing10b"
                                    value="<?php if(!empty($data->constract_drawing10b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->constract_drawing10b; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing10b;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10b"
                                  value="<?php if(!empty($data->constract_drawing10b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") { echo $data->date_insert_drawing10b;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10b">コメント</label>
                                      <p>
                                        <?php if($data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item10b;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10b) && $data->category_name1b == "長期優良住宅認定書・性能評価書"){ echo $data->desc_item10b; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10b) && $data->category_name1b == "長期優良住宅認定書・性能評価書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 3 -->
                      <div class="card">
                        <div class="card-header" id="headingThree">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                              <div class="leftSide">
                                <span>3</span>
                                <p>地盤調査・地盤改良</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1c == "地盤調査・地盤改良" && !empty($data->constract_drawing1c)){echo "show";} ?>" id="column3">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing1c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing1c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1c" name="constract_drawing1c"
                                    value="<?php if(!empty($data->constract_drawing1c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing1c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing1c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1c"
                                  value="<?php if(!empty($data->constract_drawing1c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing1c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item1c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item1c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing2c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing2c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2c" name="constract_drawing2c"
                                    value="<?php if(!empty($data->constract_drawing2c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing2c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing2c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2c"
                                  value="<?php if(!empty($data->constract_drawing2c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing2c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item2c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item2c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing3c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing3c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3c" name="constract_drawing3c"
                                    value="<?php if(!empty($data->constract_drawing3c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing3c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing3c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3c"
                                  value="<?php if(!empty($data->constract_drawing3c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing3c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item3c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item3c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing4c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing4c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4c" name="constract_drawing4c"
                                    value="<?php if(!empty($data->constract_drawing4c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing4c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4c && $data->category_name1c == "地盤調査・地盤改良")) { echo $data->date_insert_drawing4c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4c"
                                  value="<?php if(!empty($data->constract_drawing4c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing4c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item4c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item4c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing5c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing5c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5c" name="constract_drawing5c"
                                    value="<?php if(!empty($data->constract_drawing5c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing5c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing5c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5c"
                                  value="<?php if(!empty($data->constract_drawing5c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing5c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item5c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item5c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing6c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing6c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6c" name="constract_drawing6c"
                                    value="<?php if(!empty($data->constract_drawing6c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing6c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing6c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6c"
                                  value="<?php if(!empty($data->constract_drawing6c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing6c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item6c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item6c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing7c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing7c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7c" name="constract_drawing7c"
                                    value="<?php if(!empty($data->constract_drawing7c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing7c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing7c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7c"
                                  value="<?php if(!empty($data->constract_drawing7c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing7c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item7c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item7c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing8c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing8c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8c" name="constract_drawing8c"
                                    value="<?php if(!empty($data->constract_drawing8c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing8c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing8c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8c"
                                  value="<?php if(!empty($data->constract_drawing8c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing8c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item8c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item8c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing9c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing9c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9c" name="constract_drawing9c"
                                    value="<?php if(!empty($data->constract_drawing9c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing9c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing9c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9c"
                                  value="<?php if(!empty($data->constract_drawing9c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing9c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item9c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item9c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9c) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing10c;}?>"><?php if($data->category_name1c == "地盤調査・地盤改良"){echo $data->constract_drawing10c;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10c" name="constract_drawing10c"
                                    value="<?php if(!empty($data->constract_drawing10c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->constract_drawing10c; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing10c;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10c"
                                  value="<?php if(!empty($data->constract_drawing10c) && $data->category_name1c == "地盤調査・地盤改良") { echo $data->date_insert_drawing10c;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10c">コメント</label>
                                      <p>
                                        <?php if($data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item10c;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10c) && $data->category_name1c == "地盤調査・地盤改良"){ echo $data->desc_item10c; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10) && $data->category_name1c == "地盤調査・地盤改良") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>   
                      
                      <!-- CATEORY 4 -->
                      <div class="card">
                        <div class="card-header" id="headingFour">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                              <div class="leftSide">
                                <span>4</span>
                                <p>点検履歴</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1d == "点検履歴" && !empty($data->constract_drawing1d)){echo "show";} ?>" id="column4">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing1d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing1d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1d" name="constract_drawing1d"
                                    value="<?php if(!empty($data->constract_drawing1d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing1d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing1d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1d"
                                  value="<?php if(!empty($data->constract_drawing1d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing1d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item1d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item1d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing2d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing2d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2d" name="constract_drawing2d"
                                    value="<?php if(!empty($data->constract_drawing2d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing2d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing2d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2d"
                                  value="<?php if(!empty($data->constract_drawing2d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing2d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item2d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item2d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing3d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing3d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3d" name="constract_drawing3d"
                                    value="<?php if(!empty($data->constract_drawing3d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing3d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing3d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3d"
                                  value="<?php if(!empty($data->constract_drawing3d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing3d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item3d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item3d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing4d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing4d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4d" name="constract_drawing4d"
                                    value="<?php if(!empty($data->constract_drawing4d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing4d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing4d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4d"
                                  value="<?php if(!empty($data->constract_drawing4d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing4d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item4d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item4d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing5d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing5d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5d" name="constract_drawing5d"
                                    value="<?php if(!empty($data->constract_drawing5d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing5d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing5d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5d"
                                  value="<?php if(!empty($data->constract_drawing5d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing5d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item5d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item5d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing6d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing6d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6d" name="constract_drawing6d"
                                    value="<?php if(!empty($data->constract_drawing6d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing6d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing6d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6d"
                                  value="<?php if(!empty($data->constract_drawing6d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing6d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item6d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item6d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing7d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing7d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7d" name="constract_drawing7d"
                                    value="<?php if(!empty($data->constract_drawing7d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing7d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing7d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7d"
                                  value="<?php if(!empty($data->constract_drawing7d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing7d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item7d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item7d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing8d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing8d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8d" name="constract_drawing8d"
                                    value="<?php if(!empty($data->constract_drawing8d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing8d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing8d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8d"
                                  value="<?php if(!empty($data->constract_drawing8d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing8d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item8d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item8d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing9d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing9d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9d" name="constract_drawing9d"
                                    value="<?php if(!empty($data->constract_drawing9d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing9d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing9d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9d"
                                  value="<?php if(!empty($data->constract_drawing9d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing9d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item9d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item9d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing10d;}?>"><?php if($data->category_name1d == "点検履歴"){echo $data->constract_drawing10d;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10d" name="constract_drawing10d"
                                    value="<?php if(!empty($data->constract_drawing10d) && $data->category_name1d == "点検履歴"){ echo $data->constract_drawing10d; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing10d;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10d"
                                  value="<?php if(!empty($data->constract_drawing10d) && $data->category_name1d == "点検履歴") { echo $data->date_insert_drawing10d;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10d">コメント</label>
                                      <p>
                                        <?php if($data->category_name1d == "点検履歴"){ echo $data->desc_item10d;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10d) && $data->category_name1d == "点検履歴"){ echo $data->desc_item10d; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10d) && $data->category_name1d == "点検履歴") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>   

                      <!-- CATEGORY 5 --> 
                      <div class="card">
                        <div class="card-header" id="headingFive">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                              <div class="leftSide">
                                <span>5</span>
                                <p>三者引継ぎ合意書</p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1e == "三者引継ぎ合意書" && !empty($data->constract_drawing1e)){echo "show";} ?>" id="column5">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing1e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing1e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1e" name="constract_drawing1e"
                                    value="<?php if(!empty($data->constract_drawing1e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing1e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing1e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1e"
                                  value="<?php if(!empty($data->constract_drawing1e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing1e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item1e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item1e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing2e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing2e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2e" name="constract_drawing2e"
                                    value="<?php if(!empty($data->constract_drawing2e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing2e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing2e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2e"
                                  value="<?php if(!empty($data->constract_drawing2e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing2e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item2e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item2e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing3e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing3e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3e" name="constract_drawing3e"
                                    value="<?php if(!empty($data->constract_drawing3e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing3e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing3e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3e"
                                  value="<?php if(!empty($data->constract_drawing3e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing3e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item3e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item3e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing4e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing4e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4e" name="constract_drawing4e"
                                    value="<?php if(!empty($data->constract_drawing4e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing4e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing4e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4e"
                                  value="<?php if(!empty($data->constract_drawing4e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing4e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item4e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item4e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing5e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing5e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5e" name="constract_drawing5e"
                                    value="<?php if(!empty($data->constract_drawing5e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing5e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing5e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5e"
                                  value="<?php if(!empty($data->constract_drawing5e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing5e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item5e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item5e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing6e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing6e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6e" name="constract_drawing6e"
                                    value="<?php if(!empty($data->constract_drawing6e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing6e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing6e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6e"
                                  value="<?php if(!empty($data->constract_drawing6e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing6e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item6e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item6e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing7e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing7e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7e" name="constract_drawing7e"
                                    value="<?php if(!empty($data->constract_drawing7e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing7e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing7e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7e"
                                  value="<?php if(!empty($data->constract_drawing7e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing7e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item7e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item7e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing8e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing8e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8e" name="constract_drawing8e"
                                    value="<?php if(!empty($data->constract_drawing8e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing8e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing8e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8e"
                                  value="<?php if(!empty($data->constract_drawing8e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing8e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item8e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item8e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing9e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing9e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9e" name="constract_drawing9e"
                                    value="<?php if(!empty($data->constract_drawing9e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing9e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing9e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9e"
                                  value="<?php if(!empty($data->constract_drawing9e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing9e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item9e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item9e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing10e;}?>"><?php if($data->category_name1e == "三者引継ぎ合意書"){echo $data->constract_drawing10e;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10e" name="constract_drawing10e"
                                    value="<?php if(!empty($data->constract_drawing10e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->constract_drawing10e; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing10e;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10e"
                                  value="<?php if(!empty($data->constract_drawing10e) && $data->category_name1e == "三者引継ぎ合意書") { echo $data->date_insert_drawing10e;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10e">コメント</label>
                                      <p>
                                        <?php if($data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item10e;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10e) && $data->category_name1e == "三者引継ぎ合意書"){ echo $data->desc_item10e; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10e) && $data->category_name1e == "三者引継ぎ合意書") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 6 -->
                      <div class="card">
                        <div class="card-header" id="headingSix">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                              <div class="leftSide">
                                <span>6</span>
                                <p>住宅仕様書・プレカット資料 </p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1f == "住宅仕様書・プレカット資料" && !empty($data->constract_drawing1f)){echo "show";} ?>" id="column6">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing1f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing1f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1f" name="constract_drawing1f"
                                    value="<?php if(!empty($data->constract_drawing1f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing1f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing1f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1f"
                                  value="<?php if(!empty($data->constract_drawing1f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing1f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item1f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item1f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing2f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing2f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2f" name="constract_drawing2f"
                                    value="<?php if(!empty($data->constract_drawing2f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing2f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing2f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2f"
                                  value="<?php if(!empty($data->constract_drawing2f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing2f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item2f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item2f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing3f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing3f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3f" name="constract_drawing3f"
                                    value="<?php if(!empty($data->constract_drawing3f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing3f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing3f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3f"
                                  value="<?php if(!empty($data->constract_drawing3f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing3f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item3f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item3f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing4f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing4f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4f" name="constract_drawing4f"
                                    value="<?php if(!empty($data->constract_drawing4f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing4f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing4f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4f"
                                  value="<?php if(!empty($data->constract_drawing4f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing4f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item4f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4f)){ echo $data->desc_item4f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing5f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing5f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5f" name="constract_drawing5f"
                                    value="<?php if(!empty($data->constract_drawing5f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing5f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing5f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5f"
                                  value="<?php if(!empty($data->constract_drawing5f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing5f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item5f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item5f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing6f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing6f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6f" name="constract_drawing6f"
                                    value="<?php if(!empty($data->constract_drawing6f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing6f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing6f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6f"
                                  value="<?php if(!empty($data->constract_drawing6f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing6f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item6f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item6f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing7f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing7f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7f" name="constract_drawing7f"
                                    value="<?php if(!empty($data->constract_drawing7f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing7f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing7f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7f"
                                  value="<?php if(!empty($data->constract_drawing7f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing7f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item7f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item7f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing8f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing8f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8f" name="constract_drawing8f"
                                    value="<?php if(!empty($data->constract_drawing8f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing8f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing8f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8f"
                                  value="<?php if(!empty($data->constract_drawing8f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing8f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item8f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item8f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing9f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing9f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9f" name="constract_drawing9f"
                                    value="<?php if(!empty($data->constract_drawing9f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing9f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing9f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9f"
                                  value="<?php if(!empty($data->constract_drawing9f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing9f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item9f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item9f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing10f;}?>"><?php if($data->category_name1f == "住宅仕様書・プレカット資料"){echo $data->constract_drawing10f;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10f" name="constract_drawing10f"
                                    value="<?php if(!empty($data->constract_drawing10f) && $data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->constract_drawing10f; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing10f;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10f"
                                  value="<?php if(!empty($data->constract_drawing10f) && $data->category_name1f == "住宅仕様書・プレカット資料") { echo $data->date_insert_drawing10f;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10f">コメント</label>
                                      <p>
                                        <?php if($data->category_name1f == "住宅仕様書・プレカット資料"){ echo $data->desc_item10f;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10f)){ echo $data->desc_item10f; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10f) && $data->category_name1f == "住宅仕様書・プレカット資料") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 7 --> 
                      <div class="card">
                        <div class="card-header" id="headingSeven">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                              <div class="leftSide">
                                <span>7</span>
                                <p>工事写真 </p>
                              </div>
                              <div class="rightSide">
                                <span>社内</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1g == "工事写真" && !empty($data->constract_drawing1g)){echo "show";} ?>" id="column7">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing1g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing1g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1g" name="constract_drawing1g"
                                    value="<?php if(!empty($data->constract_drawing1g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing1g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing1g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1g"
                                  value="<?php if(!empty($data->constract_drawing1g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing1g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item1g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1g) && $data->category_name1g == "工事写真"){ echo $data->desc_item1g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing2g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing2g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2g" name="constract_drawing2g"
                                    value="<?php if(!empty($data->constract_drawing2g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing2g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing2g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2g"
                                  value="<?php if(!empty($data->constract_drawing2g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing2g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item2g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2g) && $data->category_name1g == "工事写真"){ echo $data->desc_item2g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing3g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing3g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3g" name="constract_drawing3g"
                                    value="<?php if(!empty($data->constract_drawing3g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing3g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing3g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3g"
                                  value="<?php if(!empty($data->constract_drawing3g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing3g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item3g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3g) && $data->category_name1g == "工事写真"){ echo $data->desc_item3g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing4g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing4g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4g" name="constract_drawing4g"
                                    value="<?php if(!empty($data->constract_drawing4g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing4g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing4g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4g"
                                  value="<?php if(!empty($data->constract_drawing4g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing4g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item4g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4g) && $data->category_name1g == "工事写真"){ echo $data->desc_item4g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing5g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing5g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5g" name="constract_drawing5g"
                                    value="<?php if(!empty($data->constract_drawing5g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing5g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing5g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5g"
                                  value="<?php if(!empty($data->constract_drawing5g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing5g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item5g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5g) && $data->category_name1g == "工事写真"){ echo $data->desc_item5g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing6g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing6g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6g" name="constract_drawing6g"
                                    value="<?php if(!empty($data->constract_drawing6g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing6g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing6g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6g"
                                  value="<?php if(!empty($data->constract_drawing6g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing6g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item6g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6g) && $data->category_name1g == "工事写真"){ echo $data->desc_item6g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing7g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing7g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7g" name="constract_drawing7g"
                                    value="<?php if(!empty($data->constract_drawing7g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing7g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing7g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7g"
                                  value="<?php if(!empty($data->constract_drawing7g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing7g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item7g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7g) && $data->category_name1g == "工事写真"){ echo $data->desc_item7g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing8g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing8g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8g" name="constract_drawing8g"
                                    value="<?php if(!empty($data->constract_drawing8g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing8g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing8g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8g"
                                  value="<?php if(!empty($data->constract_drawing8g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing8g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item8g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8g) && $data->category_name1g == "工事写真"){ echo $data->desc_item8g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing9g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing9g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9g" name="constract_drawing9g"
                                    value="<?php if(!empty($data->constract_drawing9g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing9g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing9g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9g"
                                  value="<?php if(!empty($data->constract_drawing9g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing9g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item9g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9g) && $data->category_name1g == "工事写真"){ echo $data->desc_item9g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing10g;}?>"><?php if($data->category_name1g == "工事写真"){echo $data->constract_drawing10g;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10g" name="constract_drawing10g"
                                    value="<?php if(!empty($data->constract_drawing10g) && $data->category_name1g == "工事写真"){ echo $data->constract_drawing10g; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing10g;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10g"
                                  value="<?php if(!empty($data->constract_drawing10g) && $data->category_name1g == "工事写真") { echo $data->date_insert_drawing10g;}?>" hidden>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10g">コメント</label>
                                      <p>
                                        <?php if($data->category_name1g == "工事写真"){ echo $data->desc_item10g;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10g) && $data->category_name1g == "工事写真"){ echo $data->desc_item10g; }?>" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10g) && $data->category_name1g == "工事写真") :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <br>
                    
                      <!-- CATEGORY 1BLUE --> 
                      <div class="card blueCard">
                        <div class="card-header" id="headingOneBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                              <div class="leftSide">
                                <span>1</span>
                                <p>確認申請書一式 </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1bluea == "確認申請書一式" && !empty($data->constract_drawing1bluea)){echo "show";} ?>" id="column1blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing1bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing1bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluea" name="constract_drawing1bluea"
                                    value="<?php if(!empty($data->constract_drawing1bluea) && $data->category_name1bluea == "確認申請書一式"){ echo $data->constract_drawing1bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1bluea) && $data->category_name1bluea == "確認申請書一式") { echo $data->date_insert_drawing1bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluea"
                                  value="<?php if(!empty($data->constract_drawing1bluea) && $data->category_name1bluea == "確認申請書一式") { echo $data->date_insert_drawing1bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1bluea)){ echo $data->co_owner1bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner1bluea" name="co_owner1bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item1bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1bluea)){ echo $data->desc_item1bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing2bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing2bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluea" name="constract_drawing2bluea"
                                    value="<?php if(!empty($data->constract_drawing2bluea)){ echo $data->constract_drawing2bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2bluea)) { echo $data->date_insert_drawing2bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluea"
                                  value="<?php if(!empty($data->constract_drawing2bluea)) { echo $data->date_insert_drawing2bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2bluea)){ echo $data->co_owner2bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner2bluea" name="co_owner2bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item2bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2bluea)){ echo $data->desc_item2bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing3bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing3bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluea" name="constract_drawing3bluea"
                                    value="<?php if(!empty($data->constract_drawing3bluea)){ echo $data->constract_drawing3bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3bluea)) { echo $data->date_insert_drawing3bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluea"
                                  value="<?php if(!empty($data->constract_drawing3bluea)) { echo $data->date_insert_drawing3bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3bluea)){ echo $data->co_owner3bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner3bluea" name="co_owner3bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item3bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3bluea)){ echo $data->desc_item3bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing4bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing4bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluea" name="constract_drawing4bluea"
                                    value="<?php if(!empty($data->constract_drawing4bluea)){ echo $data->constract_drawing4bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4bluea)) { echo $data->date_insert_drawing4bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluea"
                                  value="<?php if(!empty($data->constract_drawing4bluea)) { echo $data->date_insert_drawing4bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4bluea)){ echo $data->co_owner4bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner4bluea" name="co_owner4bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item4bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4bluea)){ echo $data->desc_item4bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing5bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing5bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluea" name="constract_drawing5bluea"
                                    value="<?php if(!empty($data->constract_drawing5bluea)){ echo $data->constract_drawing5bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5bluea)) { echo $data->date_insert_drawing5bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluea"
                                  value="<?php if(!empty($data->constract_drawing5bluea)) { echo $data->date_insert_drawing5bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5bluea)){ echo $data->co_owner5bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner5bluea" name="co_owner5bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->checkbluea5)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item5bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5bluea)){ echo $data->desc_item5bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing6bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing6bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluea" name="constract_drawing6bluea"
                                    value="<?php if(!empty($data->constract_drawing6bluea)){ echo $data->constract_drawing6bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6bluea)) { echo $data->date_insert_drawing6bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluea"
                                  value="<?php if(!empty($data->constract_drawing6bluea)) { echo $data->date_insert_drawing6bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6bluea)){ echo $data->co_owner6bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner6bluea" name="co_owner6bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item6bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6bluea)){ echo $data->desc_item6bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing7bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing7bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluea" name="constract_drawing7bluea"
                                    value="<?php if(!empty($data->constract_drawing7bluea)){ echo $data->constract_drawing7bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7bluea)) { echo $data->date_insert_drawing7bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluea"
                                  value="<?php if(!empty($data->constract_drawing7bluea)) { echo $data->date_insert_drawing7bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7bluea)){ echo $data->co_owner7bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner7bluea" name="co_owner7bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item7bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7bluea)){ echo $data->desc_item7bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing8bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing8bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluea" name="constract_drawing8bluea"
                                    value="<?php if(!empty($data->constract_drawing8bluea)){ echo $data->constract_drawing8bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8bluea)) { echo $data->date_insert_drawing8bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluea"
                                  value="<?php if(!empty($data->constract_drawing8bluea)) { echo $data->date_insert_drawing8bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8bluea)){ echo $data->co_owner8bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner8bluea" name="co_owner8bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item8bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8bluea)){ echo $data->desc_item8bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing9bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing9bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluea" name="constract_drawing9bluea"
                                    value="<?php if(!empty($data->constract_drawing9bluea)){ echo $data->constract_drawing9bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9bluea)) { echo $data->date_insert_drawing9bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluea"
                                  value="<?php if(!empty($data->constract_drawing9bluea)) { echo $data->date_insert_drawing9bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9bluea)){ echo $data->co_owner9bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner9bluea" name="co_owner9bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item9bluea;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9bluea)){ echo $data->desc_item9bluea; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing10bluea;}?>"><?php if($data->category_name1bluea == "確認申請書一式"){echo $data->constract_drawing10bluea;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluea" name="constract_drawing10bluea"
                                    value="<?php if(!empty($data->constract_drawing10bluea)){ echo $data->constract_drawing10bluea; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10bluea)) { echo $data->date_insert_drawing10bluea;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluea"
                                  value="<?php if(!empty($data->constract_drawing10bluea)) { echo $data->date_insert_drawing10bluea;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10bluea)){ echo $data->co_owner10bluea;}?>
                                    </p>
                                    <input type="text" id="co_owner10bluea" name="co_owner10bluea" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10bluea)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluea">コメント</label>
                                      <p>
                                        <?php if($data->category_name1bluea == "確認申請書一式"){ echo $data->desc_item10;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10)){ echo $data->desc_item10; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 2BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingTwoBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                              <div class="leftSide">
                                <span>2</span>
                                <p>工事工程表 </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1blueb == "工事工程表" && !empty($data->constract_drawing1blueb)){echo "show";} ?>" id="column2blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing1blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing1blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1blueb" name="constract_drawing1blueb"
                                    value="<?php if(!empty($data->constract_drawing1blueb)){ echo $data->constract_drawing1blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1blueb)) { echo $data->date_insert_drawing1blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1blueb"
                                  value="<?php if(!empty($data->constract_drawing1blueb)) { echo $data->date_insert_drawing1blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1blueb)){ echo $data->co_owner1blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner1blueb" name="co_owner1blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item1blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1blueb)){ echo $data->desc_item1blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing2blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing2blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2blueb" name="constract_drawing2blueb"
                                    value="<?php if(!empty($data->constract_drawing2blueb)){ echo $data->constract_drawing2blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2blueb)) { echo $data->date_insert_drawing2blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2blueb"
                                  value="<?php if(!empty($data->constract_drawing2blueb)) { echo $data->date_insert_drawing2blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2blueb)){ echo $data->co_owner2blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner2blueb" name="co_owner2blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item2blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2blueb)){ echo $data->desc_item2blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing3blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing3blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3blueb" name="constract_drawing3blueb"
                                    value="<?php if(!empty($data->constract_drawing3blueb)){ echo $data->constract_drawing3blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3blueb)) { echo $data->date_insert_drawing3blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3blueb"
                                  value="<?php if(!empty($data->constract_drawing3blueb)) { echo $data->date_insert_drawing3blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3blueb)){ echo $data->co_owner3blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner3blueb" name="co_owner3blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item3blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3blueb)){ echo $data->desc_item3blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing4blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing4blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4blueb" name="constract_drawing4blueb"
                                    value="<?php if(!empty($data->constract_drawing4blueb)){ echo $data->constract_drawing4blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4blueb)) { echo $data->date_insert_drawing4blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4blueb"
                                  value="<?php if(!empty($data->constract_drawing4blueb)) { echo $data->date_insert_drawing4blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4blueb)){ echo $data->co_owner4blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner4blueb" name="co_owner4blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item4blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4blueb)){ echo $data->desc_item4blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing5blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing5blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5blueb" name="constract_drawing5blueb"
                                    value="<?php if(!empty($data->constract_drawing5blueb)){ echo $data->constract_drawing5blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5blueb)) { echo $data->date_insert_drawing5blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5blueb"
                                  value="<?php if(!empty($data->constract_drawing5blueb)) { echo $data->date_insert_drawing5blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5blueb)){ echo $data->co_owner5blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner5blueb" name="co_owner5blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item5blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5blueb)){ echo $data->desc_item5blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing6blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing6blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6blueb" name="constract_drawing6blueb"
                                    value="<?php if(!empty($data->constract_drawing6blueb)){ echo $data->constract_drawing6blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6blueb)) { echo $data->date_insert_drawing6blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6blueb"
                                  value="<?php if(!empty($data->constract_drawing6blueb)) { echo $data->date_insert_drawing6blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6blueb)){ echo $data->co_owner6blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner6blueb" name="co_owner6blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item6blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6blueb)){ echo $data->desc_item6blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing7blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing7blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7blueb" name="constract_drawing7blueb"
                                    value="<?php if(!empty($data->constract_drawing7blueb)){ echo $data->constract_drawing7blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7blueb)) { echo $data->date_insert_drawing7blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7blueb"
                                  value="<?php if(!empty($data->constract_drawing7blueb)) { echo $data->date_insert_drawing7blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7blueb)){ echo $data->co_owner7blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner7blueb" name="co_owner7blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item7blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7blueb)){ echo $data->desc_item7blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing8blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing8blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8blueb" name="constract_drawing8blueb"
                                    value="<?php if(!empty($data->constract_drawing8blueb)){ echo $data->constract_drawing8blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8blueb)) { echo $data->date_insert_drawing8blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8blueb"
                                  value="<?php if(!empty($data->constract_drawing8blueb)) { echo $data->date_insert_drawing8blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8blueb)){ echo $data->co_owner8blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner8blueb" name="co_owner8blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item8blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8blueb)){ echo $data->desc_item8blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing9blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing9blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9blueb" name="constract_drawing9blueb"
                                    value="<?php if(!empty($data->constract_drawing9blueb)){ echo $data->constract_drawing9blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9blueb)) { echo $data->date_insert_drawing9blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9blueb"
                                  value="<?php if(!empty($data->constract_drawing9blueb)) { echo $data->date_insert_drawing9blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9blueb)){ echo $data->co_owner9blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner9blueb" name="co_owner9blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item9blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9blueb)){ echo $data->desc_item9blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing10blueb;}?>"><?php if($data->category_name1blueb == "工事工程表"){echo $data->constract_drawing10blueb;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10blueb" name="constract_drawing10blueb"
                                    value="<?php if(!empty($data->constract_drawing10blueb)){ echo $data->constract_drawing10blueb; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10blueb)) { echo $data->date_insert_drawing10blueb;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10blueb"
                                  value="<?php if(!empty($data->constract_drawing10blueb)) { echo $data->date_insert_drawing10blueb;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10blueb)){ echo $data->co_owner10blueb;}?>
                                    </p>
                                    <input type="text" id="co_owner10blueb" name="co_owner10blueb" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10blueb)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10blueb">コメント</label>
                                      <p>
                                        <?php if($data->category_name1blueb == "工事工程表"){ echo $data->desc_item10blueb;}?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10blueb)){ echo $data->desc_item10blueb; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 3 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingThreeBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                              <div class="leftSide">
                                <span>3</span>
                                <p>工事着工資料  </p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1bluec == "工事着工資料" && !empty($data->constract_drawing1bluec)){echo "show";} ?>" id="column3blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing1bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing1bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluec" name="constract_drawing1bluec"
                                    value="<?php if(!empty($data->constract_drawing1bluec)){ echo $data->constract_drawing1bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1bluec)) { echo $data->date_insert_drawing1bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluec"
                                  value="<?php if(!empty($data->constract_drawing1bluec)) { echo $data->date_insert_drawing1bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1bluec)){ echo $data->co_owner1bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner1bluec" name="co_owner1bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item1bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1bluec)){ echo $data->desc_item1bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing2bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing2bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluec" name="constract_drawing2bluec"
                                    value="<?php if(!empty($data->constract_drawing2bluec)){ echo $data->constract_drawing2bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2bluec)) { echo $data->date_insert_drawing2bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluec"
                                  value="<?php if(!empty($data->constract_drawing2bluec)) { echo $data->date_insert_drawing2bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2bluec)){ echo $data->co_owner2bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner2bluec" name="co_owner2bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item2bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2bluec)){ echo $data->desc_item2bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing3bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing3bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluec" name="constract_drawing3bluec"
                                    value="<?php if(!empty($data->constract_drawing3bluec)){ echo $data->constract_drawing3bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3bluec)) { echo $data->date_insert_drawing3bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluec"
                                  value="<?php if(!empty($data->constract_drawing3bluec)) { echo $data->date_insert_drawing3bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3bluec)){ echo $data->co_owner3bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner3bluec" name="co_owner3bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item3bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3bluec)){ echo $data->desc_item3bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing4bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing4bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluec" name="constract_drawing4bluec"
                                    value="<?php if(!empty($data->constract_drawing4bluec)){ echo $data->constract_drawing4bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4bluec)) { echo $data->date_insert_drawing4bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluec"
                                  value="<?php if(!empty($data->constract_drawing4bluec)) { echo $data->date_insert_drawing4bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4bluec)){ echo $data->co_owner4bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner4bluec" name="co_owner4bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item4bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4bluec)){ echo $data->desc_item4bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing5bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing5bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluec" name="constract_drawing5bluec"
                                    value="<?php if(!empty($data->constract_drawing5bluec)){ echo $data->constract_drawing5bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5bluec)) { echo $data->date_insert_drawing5bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluec"
                                  value="<?php if(!empty($data->constract_drawing5bluec)) { echo $data->date_insert_drawing5bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5bluec)){ echo $data->co_owner5bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner5bluec" name="co_owner5bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item5bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5bluec)){ echo $data->desc_item5bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing6bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing6bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluec" name="constract_drawing6bluec"
                                    value="<?php if(!empty($data->constract_drawing6bluec)){ echo $data->constract_drawing6bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6bluec)) { echo $data->date_insert_drawing6bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluec"
                                  value="<?php if(!empty($data->constract_drawing6bluec)) { echo $data->date_insert_drawing6bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6bluec)){ echo $data->co_owner6bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner6bluec" name="co_owner6bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item6bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6bluec)){ echo $data->desc_item6bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing7bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing7bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluec" name="constract_drawing7bluec"
                                    value="<?php if(!empty($data->constract_drawing7bluec)){ echo $data->constract_drawing7bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7bluec)) { echo $data->date_insert_drawing7bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluec"
                                  value="<?php if(!empty($data->constract_drawing7bluec)) { echo $data->date_insert_drawing7bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7bluec)){ echo $data->co_owner7bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner7bluec" name="co_owner7bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item7bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7bluec)){ echo $data->desc_item7bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing8bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing8bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluec" name="constract_drawing8bluec"
                                    value="<?php if(!empty($data->constract_drawing8bluec)){ echo $data->constract_drawing8bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8bluec)) { echo $data->date_insert_drawing8bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluec"
                                  value="<?php if(!empty($data->constract_drawing8bluec)) { echo $data->date_insert_drawing8bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8bluec)){ echo $data->co_owner8bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner8bluec" name="co_owner8bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item8bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8bluec)){ echo $data->desc_item8bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing9bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing9bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluec" name="constract_drawing9bluec"
                                    value="<?php if(!empty($data->constract_drawing9bluec)){ echo $data->constract_drawing9bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9bluec)) { echo $data->date_insert_drawing9bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluec"
                                  value="<?php if(!empty($data->constract_drawing9bluec)) { echo $data->date_insert_drawing9bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9bluec)){ echo $data->co_owner9bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner9bluec" name="co_owner9bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item9bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9bluec)){ echo $data->desc_item9bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing10bluec;}?>"><?php if($data->category_name1bluec == "工事着工資料"){echo $data->constract_drawing10bluec;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluec" name="constract_drawing10bluec"
                                    value="<?php if(!empty($data->constract_drawing10bluec)){ echo $data->constract_drawing10bluec; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10bluec)) { echo $data->date_insert_drawing10bluec;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluec"
                                  value="<?php if(!empty($data->constract_drawing10bluec)) { echo $data->date_insert_drawing10bluec;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10bluec)){ echo $data->co_owner10bluec;}?>
                                    </p>
                                    <input type="text" id="co_owner10bluec" name="co_owner10bluec" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10bluec)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluec">コメント</label>
                                      <p>
                                        <?=$data->desc_item10bluec?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10bluec)){ echo $data->desc_item10bluec; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div> 

                      <!-- CATEGORY 4 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingFourBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                              <div class="leftSide">
                                <span>4</span>
                                <p>施工図面</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1blued == "施工図面" && !empty($data->constract_drawing1blued)){echo "show";} ?>" id="column4blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing1blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing1blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1blued" name="constract_drawing1blued"
                                    value="<?php if(!empty($data->constract_drawing1blued)){ echo $data->constract_drawing1blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1blued)) { echo $data->date_insert_drawing1blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1blued"
                                  value="<?php if(!empty($data->constract_drawing1blued)) { echo $data->date_insert_drawing1blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1blued)){ echo $data->co_owner1blued;}?>
                                    </p>
                                    <input type="text" id="co_owner1blued" name="co_owner1blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item1blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1blued)){ echo $data->desc_item1blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing2blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing2blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2blued" name="constract_drawing2blued"
                                    value="<?php if(!empty($data->constract_drawing2blued)){ echo $data->constract_drawing2blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2blued)) { echo $data->date_insert_drawing2blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2blued"
                                  value="<?php if(!empty($data->constract_drawing2blued)) { echo $data->date_insert_drawing2blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2blued)){ echo $data->co_owner2blued;}?>
                                    </p>
                                    <input type="text" id="co_owner2blued" name="co_owner2blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item2blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2blued)){ echo $data->desc_item2blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing3blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing3blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3blued" name="constract_drawing3blued"
                                    value="<?php if(!empty($data->constract_drawing3blued)){ echo $data->constract_drawing3blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3blued)) { echo $data->date_insert_drawing3blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3blued"
                                  value="<?php if(!empty($data->constract_drawing3blued)) { echo $data->date_insert_drawing3blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3blued)){ echo $data->co_owner3blued;}?>
                                    </p>
                                    <input type="text" id="co_owner3blued" name="co_owner3blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item3blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3blued)){ echo $data->desc_item3blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing4blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing4blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4blued" name="constract_drawing4blued"
                                    value="<?php if(!empty($data->constract_drawing4blued)){ echo $data->constract_drawing4blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4blued)) { echo $data->date_insert_drawing4blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4blued"
                                  value="<?php if(!empty($data->constract_drawing4blued)) { echo $data->date_insert_drawing4blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4blued)){ echo $data->co_owner4blued;}?>
                                    </p>
                                    <input type="text" id="co_owner4blued" name="co_owner4blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item4blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4blued)){ echo $data->desc_item4blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing5blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing5blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5blued" name="constract_drawing5blued"
                                    value="<?php if(!empty($data->constract_drawing5blued)){ echo $data->constract_drawing5blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5blued)) { echo $data->date_insert_drawing5blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5blued"
                                  value="<?php if(!empty($data->constract_drawing5blued)) { echo $data->date_insert_drawing5blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5blued)){ echo $data->co_owner5blued;}?>
                                    </p>
                                    <input type="text" id="co_owner5blued" name="co_owner5blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item5blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5blued)){ echo $data->desc_item5blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing6blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing6blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6blued" name="constract_drawing6blued"
                                    value="<?php if(!empty($data->constract_drawing6blued)){ echo $data->constract_drawing6blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6blued)) { echo $data->date_insert_drawing6blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6blued"
                                  value="<?php if(!empty($data->constract_drawing6blued)) { echo $data->date_insert_drawing6blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6blued)){ echo $data->co_owner6blued;}?>
                                    </p>
                                    <input type="text" id="co_owner6blued" name="co_owner6blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item6blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6blued)){ echo $data->desc_item6blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing7blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing7blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7blued" name="constract_drawing7blued"
                                    value="<?php if(!empty($data->constract_drawing7blued)){ echo $data->constract_drawing7blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7blued)) { echo $data->date_insert_drawing7blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7blued"
                                  value="<?php if(!empty($data->constract_drawing7blued)) { echo $data->date_insert_drawing7blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7blued)){ echo $data->co_owner7blued;}?>
                                    </p>
                                    <input type="text" id="co_owner7blued" name="co_owner7blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item7blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7blued)){ echo $data->desc_item7blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing8blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing8blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8blued" name="constract_drawing8blued"
                                    value="<?php if(!empty($data->constract_drawing8blued)){ echo $data->constract_drawing8blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8blued)) { echo $data->date_insert_drawing8blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8blued"
                                  value="<?php if(!empty($data->constract_drawing8blued)) { echo $data->date_insert_drawing8blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8blued)){ echo $data->co_owner8blued;}?>
                                    </p>
                                    <input type="text" id="co_owner8blued" name="co_owner8blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item8blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8blued)){ echo $data->desc_item8blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing9blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing9blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9blued" name="constract_drawing9blued"
                                    value="<?php if(!empty($data->constract_drawing9blued)){ echo $data->constract_drawing9blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9blued)) { echo $data->date_insert_drawing9blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9blued"
                                  value="<?php if(!empty($data->constract_drawing9blued)) { echo $data->date_insert_drawing9blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9blued)){ echo $data->co_owner9blued;}?>
                                    </p>
                                    <input type="text" id="co_owner9blued" name="co_owner9blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item9blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9blued)){ echo $data->desc_item9blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing10blued;}?>"><?php if($data->category_name1blued == "施工図面"){echo $data->constract_drawing10blued;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10blued" name="constract_drawing10blued"
                                    value="<?php if(!empty($data->constract_drawing10blued)){ echo $data->constract_drawing10blued; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10blued)) { echo $data->date_insert_drawing10blued;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10blued"
                                  value="<?php if(!empty($data->constract_drawing10blued)) { echo $data->date_insert_drawing10blued;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10blued">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10blued)){ echo $data->co_owner10blued;}?>
                                    </p>
                                    <input type="text" id="co_owner10blued" name="co_owner10blued" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10blued)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10blued">コメント</label>
                                      <p>
                                        <?=$data->desc_item10blued?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10blued)){ echo $data->desc_item10blued; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 5 BLUE -->
                      <div class="card blueCard">
                        <div class="card-header" id="headingFiveBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                              <div class="leftSide">
                                <span>5</span>
                                <p>変更図面</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1bluee == "変更図面" && !empty($data->constract_drawing1bluee)){echo "show";} ?>" id="column5blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing1bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing1bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluee" name="constract_drawing1bluee"
                                    value="<?php if(!empty($data->constract_drawing1bluee)){ echo $data->constract_drawing1bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1bluee)) { echo $data->date_insert_drawing1bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluee"
                                  value="<?php if(!empty($data->constract_drawing1bluee)) { echo $data->date_insert_drawing1bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1bluee)){ echo $data->co_owner1bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner1bluee" name="co_owner1bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item1bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1bluee)){ echo $data->desc_item1bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing2bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing2bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluee" name="constract_drawing2bluee"
                                    value="<?php if(!empty($data->constract_drawing2bluee)){ echo $data->constract_drawing2bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2bluee)) { echo $data->date_insert_drawing2bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluee"
                                  value="<?php if(!empty($data->constract_drawing2bluee)) { echo $data->date_insert_drawing2bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2bluee)){ echo $data->co_owner2bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner2bluee" name="co_owner2bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item2bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2bluee)){ echo $data->desc_item2bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing3bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing3bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluee" name="constract_drawing3bluee"
                                    value="<?php if(!empty($data->constract_drawing3bluee)){ echo $data->constract_drawing3bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3bluee)) { echo $data->date_insert_drawing3bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluee"
                                  value="<?php if(!empty($data->constract_drawing3bluee)) { echo $data->date_insert_drawing3bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3bluee)){ echo $data->co_owner3bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner3bluee" name="co_owner3bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item3bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3bluee)){ echo $data->desc_item3bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing4bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing4bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluee" name="constract_drawing4bluee"
                                    value="<?php if(!empty($data->constract_drawing4bluee)){ echo $data->constract_drawing4bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4bluee)) { echo $data->date_insert_drawing4bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluee"
                                  value="<?php if(!empty($data->constract_drawing4bluee)) { echo $data->date_insert_drawing4bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4bluee)){ echo $data->co_owner4bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner4bluee" name="co_owner4bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item4bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4bluee)){ echo $data->desc_item4bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing5bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing5bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluee" name="constract_drawing5bluee"
                                    value="<?php if(!empty($data->constract_drawing5bluee)){ echo $data->constract_drawing5bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5bluee)) { echo $data->date_insert_drawing5bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluee"
                                  value="<?php if(!empty($data->constract_drawing5bluee)) { echo $data->date_insert_drawing5bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5bluee)){ echo $data->co_owner5bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner5bluee" name="co_owner5bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item5bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5bluee)){ echo $data->desc_item5bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing6bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing6bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluee" name="constract_drawing6bluee"
                                    value="<?php if(!empty($data->constract_drawing6bluee)){ echo $data->constract_drawing6bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6bluee)) { echo $data->date_insert_drawing6bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluee"
                                  value="<?php if(!empty($data->constract_drawing6bluee)) { echo $data->date_insert_drawing6bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6bluee)){ echo $data->co_owner6bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner6bluee" name="co_owner6bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item6bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6bluee)){ echo $data->desc_item6bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing7bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing7bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluee" name="constract_drawing7bluee"
                                    value="<?php if(!empty($data->constract_drawing7bluee)){ echo $data->constract_drawing7bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7bluee)) { echo $data->date_insert_drawing7bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluee"
                                  value="<?php if(!empty($data->constract_drawing7bluee)) { echo $data->date_insert_drawing7bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7bluee)){ echo $data->co_owner7bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner7bluee" name="co_owner7bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item7bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7bluee)){ echo $data->desc_item7bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing8bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing8bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluee" name="constract_drawing8bluee"
                                    value="<?php if(!empty($data->constract_drawing8bluee)){ echo $data->constract_drawing8bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8bluee)) { echo $data->date_insert_drawing8bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluee"
                                  value="<?php if(!empty($data->constract_drawing8bluee)) { echo $data->date_insert_drawing8bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8bluee)){ echo $data->co_owner8bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner8bluee" name="co_owner8bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item8bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8bluee)){ echo $data->desc_item8bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing9bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing9bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluee" name="constract_drawing9bluee"
                                    value="<?php if(!empty($data->constract_drawing9bluee)){ echo $data->constract_drawing9bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9bluee)) { echo $data->date_insert_drawing9bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluee"
                                  value="<?php if(!empty($data->constract_drawing9bluee)) { echo $data->date_insert_drawing9bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9bluee)){ echo $data->co_owner9bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner9bluee" name="co_owner9bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item9bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9bluee)){ echo $data->desc_item9bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing10bluee;}?>"><?php if($data->category_name1bluee == "変更図面"){echo $data->constract_drawing10bluee;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluee" name="constract_drawing10bluee"
                                    value="<?php if(!empty($data->constract_drawing10bluee)){ echo $data->constract_drawing10bluee; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10bluee)) { echo $data->date_insert_drawing10bluee;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluee"
                                  value="<?php if(!empty($data->constract_drawing10bluee)) { echo $data->date_insert_drawing10bluee;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10bluee)){ echo $data->co_owner10bluee;}?>
                                    </p>
                                    <input type="text" id="co_owner10bluee" name="co_owner10bluee" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10bluee)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluee">コメント</label>
                                      <p>
                                        <?=$data->desc_item10bluee?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10bluee)){ echo $data->desc_item10bluee; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 6 BLUE --> 
                      <div class="card blueCard">
                        <div class="card-header" id="headingSixBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                              <div class="leftSide">
                                <span>6</span>
                                <p>設備プランシート</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1bluef == "設備プランシート" && !empty($data->constract_drawing1bluef)){echo "show";} ?>" id="column6blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing1bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing1bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1bluef" name="constract_drawing1bluef"
                                    value="<?php if(!empty($data->constract_drawing1bluef)){ echo $data->constract_drawing1bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1bluef)) { echo $data->date_insert_drawing1bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1bluef"
                                  value="<?php if(!empty($data->constract_drawing1bluef)) { echo $data->date_insert_drawing1bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1bluef)){ echo $data->co_owner1bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner1bluef" name="co_owner1bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item1bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1bluef)){ echo $data->desc_item1bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing2bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing2bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2bluef" name="constract_drawing2bluef"
                                    value="<?php if(!empty($data->constract_drawing2bluef)){ echo $data->constract_drawing2bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2bluef)) { echo $data->date_insert_drawing2bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2bluef"
                                  value="<?php if(!empty($data->constract_drawing2bluef)) { echo $data->date_insert_drawing2bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2bluef)){ echo $data->co_owner2bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner2bluef" name="co_owner2bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item2bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2bluef)){ echo $data->desc_item2bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing3bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing3bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3bluef" name="constract_drawing3bluef"
                                    value="<?php if(!empty($data->constract_drawing3bluef)){ echo $data->constract_drawing3bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3bluef)) { echo $data->date_insert_drawing3bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3bluef"
                                  value="<?php if(!empty($data->constract_drawing3bluef)) { echo $data->date_insert_drawing3bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3bluef)){ echo $data->co_owner3bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner3bluef" name="co_owner3bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item3bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3bluef)){ echo $data->desc_item3bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing4bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing4bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4bluef" name="constract_drawing4bluef"
                                    value="<?php if(!empty($data->constract_drawing4bluef)){ echo $data->constract_drawing4bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4bluef)) { echo $data->date_insert_drawing4bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4bluef"
                                  value="<?php if(!empty($data->constract_drawing4bluef)) { echo $data->date_insert_drawing4bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4bluef)){ echo $data->co_owner4bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner4bluef" name="co_owner4bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item4bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4bluef)){ echo $data->desc_item4bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing5bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing5bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5bluef" name="constract_drawing5bluef"
                                    value="<?php if(!empty($data->constract_drawing5bluef)){ echo $data->constract_drawing5bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5bluef)) { echo $data->date_insert_drawing5bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5bluef"
                                  value="<?php if(!empty($data->constract_drawing5bluef)) { echo $data->date_insert_drawing5bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5bluef)){ echo $data->co_owner5bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner5bluef" name="co_owner5bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item5bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5bluef)){ echo $data->desc_item5bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing6bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing6bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6bluef" name="constract_drawing6bluef"
                                    value="<?php if(!empty($data->constract_drawing6bluef)){ echo $data->constract_drawing6bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6bluef)) { echo $data->date_insert_drawing6bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6bluef"
                                  value="<?php if(!empty($data->constract_drawing6bluef)) { echo $data->date_insert_drawing6bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6bluef)){ echo $data->co_owner6bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner6bluef" name="co_owner6bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item6bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6bluef)){ echo $data->desc_item6bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing7bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing7bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7bluef" name="constract_drawing7bluef"
                                    value="<?php if(!empty($data->constract_drawing7bluef)){ echo $data->constract_drawing7bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7bluef)) { echo $data->date_insert_drawing7bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7bluef"
                                  value="<?php if(!empty($data->constract_drawing7bluef)) { echo $data->date_insert_drawing7bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7bluef)){ echo $data->co_owner7bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner7bluef" name="co_owner7bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item7bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7bluef)){ echo $data->desc_item7bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing8bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing8bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8bluef" name="constract_drawing8bluef"
                                    value="<?php if(!empty($data->constract_drawing8bluef)){ echo $data->constract_drawing8bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8bluef)) { echo $data->date_insert_drawing8bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8bluef"
                                  value="<?php if(!empty($data->constract_drawing8bluef)) { echo $data->date_insert_drawing8bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8bluef)){ echo $data->co_owner8bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner8bluef" name="co_owner8bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item8bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8bluef)){ echo $data->desc_item8bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing9bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing9bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9bluef" name="constract_drawing9bluef"
                                    value="<?php if(!empty($data->constract_drawing9bluef)){ echo $data->constract_drawing9bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9bluef)) { echo $data->date_insert_drawing9bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9bluef"
                                  value="<?php if(!empty($data->constract_drawing9bluef)) { echo $data->date_insert_drawing9bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9bluef)){ echo $data->co_owner9bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner9bluef" name="co_owner9bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item9bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9bluef)){ echo $data->desc_item9bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing10bluef;}?>"><?php if($data->category_name1bluef == "設備プランシート"){echo $data->constract_drawing10bluef;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10bluef" name="constract_drawing10bluef"
                                    value="<?php if(!empty($data->constract_drawing10bluef)){ echo $data->constract_drawing10bluef; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10bluef)) { echo $data->date_insert_drawing10bluef;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10bluef"
                                  value="<?php if(!empty($data->constract_drawing10bluef)) { echo $data->date_insert_drawing10bluef;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10bluef)){ echo $data->co_owner10bluef;}?>
                                    </p>
                                    <input type="text" id="co_owner10bluef" name="co_owner10bluef" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10bluef)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10bluef">コメント</label>
                                      <p>
                                        <?=$data->desc_item10bluef?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10bluef)){ echo $data->desc_item10bluef; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <!-- CATEGORY 7 BLUE --> 
                      <div class="card blueCard">
                        <div class="card-header" id="headingSevenBlue">
                          <h5 class="mb-0">
                            <a class="btn" data-toggle="collapse" href="#column7blue" role="button" aria-expanded="false" aria-controls="column7blue">
                              <div class="leftSide">
                                <span>7</span>
                                <p>その他</p>
                              </div>
                              <div class="rightSide">
                                <span>共有</span>
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                              </div>
                            </a>
                          </h5>
                        </div>

                        <div class="collapse <?php if($data->category_name1blueg == "その他" && !empty($data->constract_drawing1blueg)){echo "show";} ?>" id="column7blue">

                          <div class="card card-body">
                            <div class="number">1</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing1blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing1blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing1blueg" name="constract_drawing1blueg"
                                    value="<?php if(!empty($data->constract_drawing1blueg)){ echo $data->constract_drawing1blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing1blueg)) { echo $data->date_insert_drawing1blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing1blueg"
                                  value="<?php if(!empty($data->constract_drawing1blueg)) { echo $data->date_insert_drawing1blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner1blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner1blueg)){ echo $data->co_owner1blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner1blueg" name="co_owner1blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check1blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item1blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item1blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item1blueg" id="desc_item1blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item1blueg)){ echo $data->desc_item1blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">2</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing2blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing2blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing2blueg" name="constract_drawing2blueg"
                                    value="<?php if(!empty($data->constract_drawing2blueg)){ echo $data->constract_drawing2blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing2blueg)) { echo $data->date_insert_drawing2blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing2blueg"
                                  value="<?php if(!empty($data->constract_drawing2blueg)) { echo $data->date_insert_drawing2blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner2blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner2blueg)){ echo $data->co_owner2blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner2blueg" name="co_owner2blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check2blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item2blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item2blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item2blueg" id="desc_item2blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item2blueg)){ echo $data->desc_item2blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">3</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing3blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing3blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing3blueg" name="constract_drawing3blueg"
                                    value="<?php if(!empty($data->constract_drawing3blueg)){ echo $data->constract_drawing3blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing3blueg)) { echo $data->date_insert_drawing3blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing3blueg"
                                  value="<?php if(!empty($data->constract_drawing3blueg)) { echo $data->date_insert_drawing3blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner3blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner3blueg)){ echo $data->co_owner3blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner3blueg" name="co_owner3blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check3blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item3blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item3blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item3blueg" id="desc_item3blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item3blueg)){ echo $data->desc_item3blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">4</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing4blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing4blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing4blueg" name="constract_drawing4blueg"
                                    value="<?php if(!empty($data->constract_drawing4blueg)){ echo $data->constract_drawing4blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing4blueg)) { echo $data->date_insert_drawing4blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing4blueg"
                                  value="<?php if(!empty($data->constract_drawing4blueg)) { echo $data->date_insert_drawing4blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner4blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner4blueg)){ echo $data->co_owner4blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner4blueg" name="co_owner4blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check4blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item4blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item4blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item4blueg" id="desc_item4blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item4blueg)){ echo $data->desc_item4blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">5</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing5blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing5blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing5blueg" name="constract_drawing5blueg"
                                    value="<?php if(!empty($data->constract_drawing5blueg)){ echo $data->constract_drawing5blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing5blueg)) { echo $data->date_insert_drawing5blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing5blueg"
                                  value="<?php if(!empty($data->constract_drawing5blueg)) { echo $data->date_insert_drawing5blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner5blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner5blueg)){ echo $data->co_owner5blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner5blueg" name="co_owner5blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check5blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item5blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item5blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item5blueg" id="desc_item5blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item5blueg)){ echo $data->desc_item5blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">6</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing6blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing6blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing6blueg" name="constract_drawing6blueg"
                                    value="<?php if(!empty($data->constract_drawing6blueg)){ echo $data->constract_drawing6blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing6blueg)) { echo $data->date_insert_drawing6blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing6blueg"
                                  value="<?php if(!empty($data->constract_drawing6blueg)) { echo $data->date_insert_drawing6blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner6blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner6blueg)){ echo $data->co_owner6blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner6blueg" name="co_owner6blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check6blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item6blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item6blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item6blueg" id="desc_item6blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item6blueg)){ echo $data->desc_item6blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">7</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing7blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing7blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing7blueg" name="constract_drawing7blueg"
                                    value="<?php if(!empty($data->constract_drawing7blueg)){ echo $data->constract_drawing7blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing7blueg)) { echo $data->date_insert_drawing7blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing7blueg"
                                  value="<?php if(!empty($data->constract_drawing7blueg)) { echo $data->date_insert_drawing7blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner7blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner7blueg)){ echo $data->co_owner7blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner7blueg" name="co_owner7blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check7blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item7blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item7blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item7blueg" id="desc_item7blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item7blueg)){ echo $data->desc_item7blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">8</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing8blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing8blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing8blueg" name="constract_drawing8blueg"
                                    value="<?php if(!empty($data->constract_drawing8blueg)){ echo $data->constract_drawing8blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing8blueg)) { echo $data->date_insert_drawing8blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing8blueg"
                                  value="<?php if(!empty($data->constract_drawing8blueg)) { echo $data->date_insert_drawing8blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner8blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner8blueg)){ echo $data->co_owner8blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner8blueg" name="co_owner8blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check8blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item8blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item8blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item8blueg" id="desc_item8blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item8blueg)){ echo $data->desc_item8blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">9</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing9blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing9blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing9blueg" name="constract_drawing9blueg"
                                    value="<?php if(!empty($data->constract_drawing9blueg)){ echo $data->constract_drawing9blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing9blueg)) { echo $data->date_insert_drawing9blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing9blueg"
                                  value="<?php if(!empty($data->constract_drawing9blueg)) { echo $data->date_insert_drawing9blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner9blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner9blueg)){ echo $data->co_owner9blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner9blueg" name="co_owner9blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check9blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item9blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item9blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item9blueg" id="desc_item9blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item9blueg)){ echo $data->desc_item9blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="card card-body">
                            <div class="number">10</div>
                            <div class="form-group flex">
                              <div class="flex-content">
                                <div class="input-group">
                                  <div class="custom-file previewFile">
                                    <div class="pdf">
                                      <img src="<?=base_url()?>assets/dashboard/img/icon-pdf.svg">
                                      <p><a href="<?=base_url()?>dashboard/view_files/<?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing10blueg;}?>"><?php if($data->category_name1blueg == "その他"){echo $data->constract_drawing10blueg;}?></a></p>
                                    </div>
                                    <input type="text" id="constract_drawing10blueg" name="constract_drawing10blueg"
                                    value="<?php if(!empty($data->constract_drawing10blueg)){ echo $data->constract_drawing10blueg; }?>" hidden>
                                    <p class="date_insert">
                                      <?php if(!empty($data->constract_drawing10blueg)) { echo $data->date_insert_drawing10blueg;}?>
                                    </p>
                                    <input type="text" name="date_insert_drawing10blueg"
                                  value="<?php if(!empty($data->constract_drawing10blueg)) { echo $data->date_insert_drawing10blueg;}?>" hidden>
                                  </div>
                                </div>
                                <div class="second-input preview">
                                  <div class="dropdown-content">
                                    <label class="subTitle" for="co_owner10blueg">共有業者</label>
                                    <p>
                                      <?php if(!empty($data->co_owner10blueg)){ echo $data->co_owner10blueg;}?>
                                    </p>
                                    <input type="text" id="co_owner10blueg" name="co_owner10blueg" hidden>
                                  </div>
                                  <div class="checkbox-content preview-check">
                                    <?php if(!empty($data->check10blueg)) :
                                    ?>
                                      <p class="green">施主様共有中</p>
                                    <?php
                                        else :
                                    ?>
                                      <p class="red">施主様共有なし</p>
                                    <?php endif;?>
                                  </div>
                                </div>
                                <div class="third-input preview">
                                  <div class="form-group">
                                      <label for="desc_item10blueg">コメント</label>
                                      <p>
                                        <?=$data->desc_item10blueg?>
                                      </p>
                                      <input type="text" class="form-control" name="desc_item10blueg" id="desc_item10blueg" aria-describedby="helpId"
                                        placeholder="" value="<?php if(!empty($data->desc_item10blueg)){ echo $data->desc_item10blueg; }?>" hidden>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>


                  </div>


                    <div class="button" style="margin-top:20px;">
                      <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_construction/<?=$data->construct_id?>" role="button"><i class="fa fa-trash"></i></a>
                      <a class="btn btn-info" href="<?=base_url()?>dashboard/view_constructor_list/<?=$data->construct_id?>" role="button"><i class="fa fa-file"></i></a>
                    </div>

                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('.delete').confirm({
    title:'',
    content: "工事情報を削除します、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){ } }
  });
</script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>