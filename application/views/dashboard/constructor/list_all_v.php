<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
<link href="<?=base_url()?>assets/dashboard/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>assets/dashboard/js/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/datetime.js"></script>
<style>
#map { height: 540px; }

table.dataTable th.hidden, table.dataTable td.hidden {
  display: none;
}
table.dataTable th.dt-center { text-align: center; }
table.dataTable td.dt-center { text-align: center; }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-striped table-inverse" id="data_customer">
                      <a class="btn btn-primary" href="<?=base_url()?>dashboard/constructor_list/" style="margin-bottom: 15px;"><span>工事終了データを非表示にする</span></a>
                      <thead class="thead-inverse">
                        <tr>
                          <th>ID</th>
                          <th>顧客コード</th>
                          <th>顧客名</th>
                          <th>種別</th>
                          <th>工事No</th>
                          <th>着工日</th>
                          <th>引渡日</th>
                          <th>登録日時</th>
                          <th>工事状態</th>
                          <th class="no-sort"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                          <tr>
                            <td><?=$value->construct_id?></td>
                            <td><?=$value->cust_code?></td>
                            <td><?=$value->cust_name?></td>
                            <td><?php if($value->kinds == 'SOB'){ echo 'SWEET HOME';} else { echo $value->kinds;}?></td>
                            <td><?=$value->const_no?></td>
                            <td><?=$value->construction_start_date?></td>
                            <td><?=$value->delivery_date?></td>
                            <td><?=$value->date_insert?></td>
                            <td><?php if(empty($value->construct_stat)) echo '工事中'; else echo $value->construct_stat;?></td>
                            <td>
                            <a class="btn btn-info" href="<?=base_url()?>dashboard/view_construction/<?=$value->construct_id?>" role="button"><i class="fa fa-eye"></i></a>
                            </td>
                          </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="14">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "columnDefs": [
        {
            className: "dt-center", 
            targets: "_all"
        },
        {
          orderable: false,
          targets: "no-sort"
        }],
      "paging": true,
      "pageLength": 100,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "order": [[0, 'desc']],
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "language": {
            url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/ja.json'
      },
      dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
          {
            extend: 'csvHtml5',
            charset: 'UTF-8',
            fieldBoundary: '"',
            fieldSeparator: ',',
            bom: true 
          }
        ]
    });
    $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
</body>
</html>