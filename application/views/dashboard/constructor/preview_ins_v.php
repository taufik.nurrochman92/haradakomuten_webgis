<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">工事登録</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 工事登録</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/preview_data_ins" method="post">
                        
                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード</label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" value="<?php if(!empty($cust_code)){ echo $cust_code; }?>" >
                            </div>
                            <div class="col-md-6">
                              <label for="cust_name">顧客名</label>
                              <input type="text" class="form-control" name="cust_name" id="cust_name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($cust_name)){ echo $cust_name; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6 dropdowns">
                              <label for="kinds">種別</label>
                              <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                              <select class="form-control" name="kinds">
                                <option <?php if ($kinds == "") {echo "selected"; } ?> disabled value="">ご選択ください</option>
                                <option <?php if ($kinds == "SOB") {echo "selected"; } ?> value="SOB">SWEET HOME</option>
                                <option <?php if ($kinds == "ビリーブの家") {echo "selected"; } ?> value="ビリーブの家">ビリーブの家</option>
                                <option <?php if ($kinds == "リフォーム工事") {echo "selected"; } ?> value="リフォーム工事">リフォーム工事</option>
                                <option <?php if ($kinds == "メンテナンス工事") {echo "selected"; } ?> value="メンテナンス工事">メンテナンス工事</option>
                                <option <?php if ($kinds == "新築工事") {echo "selected"; } ?> value="新築工事">新築工事</option>
                                <option <?php if ($kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option <?php if ($kinds == "その他") {echo "selected"; } ?> value="その他">その他</option>
                                <option <?php if ($kinds == "削除") {echo "selected"; } ?> value="削除">削除</option>

                                <!-- <option  if ($kinds == "SOB") {echo "selected"; } ?> value="SOB">SWEET HOME</option>
                                <option  if ($kinds == "ビリーブの家") {echo "selected"; } ?> value="ビリーブの家">ビリーブの家</option>
                                <option  if ($kinds == "リフォーム工事") {echo "selected"; } ?> value="リフォーム工事">リフォーム工事</option>
                                <option  if ($kinds == "メンテナンス工事") {echo "selected"; } ?> value="メンテナンス工事">メンテナンス工事</option>
                                <option  if ($kinds == "新築工事") {echo "selected"; } ?> value="新築工事">新築工事</option>
                                <option  if ($kinds == "OB") {echo "selected"; } ?> value="OB">OB</option>
                                <option  if ($kinds == "その他") {echo "selected"; } ?> value="その他">その他</option> -->
                              </select>
                            </div>
                            <div class="col-md-6">
                              <div class="form-row">
                                <div class="col-md-12" style="margin-bottom:15px;">
                                  <label for="const_no">工事No</label>
                                  <input type="text" class="form-control" name="const_no" id="const_no" aria-describedby="helpId" placeholder="" value="<?php if(!empty($const_no)){ echo $const_no; }?>" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <label for="remarks">備考</label>
                          <textarea rows="4" cols="50" class="form-control col-md-12" name="remarks" id="remarks" aria-describedby="helpId" placeholder="" style="resize: none; height: 114px;"><?php if(!empty($remarks)){ echo $remarks; }?></textarea>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="contract_date">契約日</label>
                              <input type="date" class="form-control col-md-12" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($contract_date)){ echo $contract_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="construction_start_date">着工日</label>
                              <input type="date" class="form-control col-md-12" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($construction_start_date)){ echo $construction_start_date; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="upper_building_date">上棟日</label>
                              <input type="date" class="form-control col-md-12" name="upper_building_date" id="upper_building_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($upper_building_date)){ echo $upper_building_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="completion_date">竣工日</label>
                              <input type="date" class="form-control col-md-12" name="completion_date" id="completion_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($completion_date)){ echo $completion_date; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="delivery_date">引渡日</label>
                              <input type="date" class="form-control col-md-12" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="" value="<?php if(!empty($delivery_date)){ echo $delivery_date; }?>" >
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom:15px;">
                              <label for="amount_money">金額</label>
                              <input type="text" class="form-control number" name="amount_money" id="amount_money" aria-describedby="helpId" placeholder="円" style="text-align: right;" value="<?php if(!empty($amount_money)){ echo $amount_money; }?>" >
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="sales_staff">営業担当</label>
                              <input type="text" class="form-control col-md-12" name="sales_staff1" id="sales_staff1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="sales_staff"  id="sales_staff2">
                                <option <?php if ($sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($sales_staff == $sales_staff) {echo "selected"; } ?> value="<?=$sales_staff?>"><?=$sales_staff?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_construction">工務担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_construction1" id="incharge_construction1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_construction" id="incharge_construction2">
                                <option <?php if ($incharge_construction == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($incharge_construction == $incharge_construction) {echo "selected"; } ?> value="<?=$incharge_construction?>"><?=$incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                          </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="form-group dropdowns col-md-6">
                              <label for="incharge_coordinator">コーデ担当</label>
                              <input type="text" class="form-control col-md-12" name="incharge_coordinator1" id="incharge_coordinator1" aria-describedby="helpId" placeholder="">
                              <select class="form-control" name="incharge_coordinator" id="incharge_coordinator2">
                                <option <?php if ($incharge_coordinator == "") {echo "selected"; } ?> value="">ご選択ください</option>
                                <option <?php if ($incharge_coordinator == $incharge_coordinator) {echo "selected"; } ?> value="<?=$incharge_coordinator?>"><?=$incharge_construction?></option>
                                <?php 
                                if(!empty($data_employ))
                                {
                                foreach($data_employ as $row)
                                { 
                                    echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                                }
                                }
                                else
                                {
                                echo '<option value="">社員データなし</option>';
                                }
                                ?>
                              </select>
                              <input class="coupon_question3" type="checkbox" name="coupon_question3" value="1" />
                              <span class="item-text">手入力に切り替えます</span>
                            </div>
                            <div class="form-group dropdowns col-md-6">
                              <label for="construct_stat">工事状態</label>
                              <div class="radbut" style="flex:1;">
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat1" name="construct_stat" value="工事中" <?php if($construct_stat == "") echo "checked";?> >
                                      <label for="construct_stat1" class="form-check-label">工事中</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" id="construct_stat2" name="construct_stat" value="工事終了" <?php if($construct_stat == "工事終了") echo "checked";?> >
                                      <label for="construct_stat2" class="form-check-label">工事終了</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-xl-8 col-lg-12 col-md-12" style="padding: 0;margin-bottom:0;">
                          <div id="accordion" class="customAccordion">
                            
                            <!-- CATEGORY 1 -->
                            <input type="text"  name="category_name" id="category_name" value="請負契約書・土地契約書" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1" role="button" aria-expanded="false" aria-controls="column1">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>請負契約書・土地契約書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name == "請負契約書・土地契約書"){echo "show";} ?>" id="column1">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">>
                                            <input type="hidden" name="constract_drawing1text" id="constract_drawing1text" value="<?= $constract_drawing1text ?>">
                                            <input type="file" class="custom-file-input" id="constract_drawing1" name="constract_drawing1" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1" value="<?php if(!empty($constract_drawing1) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing1;}?>" hidden>
                                          </div>
                                          <p id="files1"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing1!="") echo $constract_drawing1; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1"></div>
                                        </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check1" name="check1" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1" id="desc_item1" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1) && $category_name == "請負契約書・土地契約書"){ echo $desc_item1; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <input type="hidden" name="constract_drawing2text" id="constract_drawing2text" value="<?= $constract_drawing2text ?>">
                                            <input type="file" class="custom-file-input" id="constract_drawing2" name="constract_drawing2" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2" value="<?php if(!empty($constract_drawing2) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing2;}?>" hidden>
                                          </div>
                                          <p id="files2"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing2!="") echo $constract_drawing2; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check2" name="check2" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2" id="desc_item2" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2) && $category_name == "請負契約書・土地契約書"){ echo $desc_item2; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">>
                                            <input type="hidden" name="constract_drawing3text" id="constract_drawing3text"value="<?= $constract_drawing3text ?>">
                                            <input type="file" class="custom-file-input" id="constract_drawing3" name="constract_drawing3" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3" value="<?php if(!empty($constract_drawing3) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing3;}?>" hidden>
                                          </div>
                                          <p id="files3"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing3!="") echo $constract_drawing3; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check3" name="check3" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3" id="desc_item3" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3) && $category_name == "請負契約書・土地契約書"){ echo $desc_item3; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php// if (!empty($constract_drawing4) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing4text" id="constract_drawing4text" value="<?= $constract_drawing4text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4" name="constract_drawing4" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4" value="<?php if(!empty($constract_drawing4) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing4;}?>" hidden>
                                          </div>
                                          <p id="files4"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing4!="") echo $constract_drawing4; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check4" name="check4" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4" id="desc_item4" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4) && $category_name == "請負契約書・土地契約書"){ echo $desc_item4; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing5text" id="constract_drawing5text" value="<?= $constract_drawing5text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5" name="constract_drawing5" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5" value="<?php if(!empty($constract_drawing5) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing5;}?>" hidden>
                                          </div>
                                          <p id="files5"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing5!="") echo $constract_drawing5; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check5" name="check5" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5" id="desc_item5" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5) && $category_name == "請負契約書・土地契約書"){ echo $desc_item5; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing6text" id="constract_drawing6text" value="<?= $constract_drawing6text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6" name="constract_drawing6" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6" value="<?php if(!empty($constract_drawing6) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing6;}?>" hidden>
                                          </div>
                                          <p id="files6"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing6!="") echo $constract_drawing6; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check6" name="check6" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6" id="desc_item6" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6) && $category_name == "請負契約書・土地契約書"){ echo $desc_item6; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing7text" id="constract_drawing7text" value="<?= $constract_drawing7text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7" name="constract_drawing7" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7" value="<?php if(!empty($constract_drawing7) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing7;}?>" hidden>
                                          </div>
                                          <p id="files7"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing7!="") echo $constract_drawing7; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check7" name="check7" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7" id="desc_item7" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7) && $category_name == "請負契約書・土地契約書"){ echo $desc_item7; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing8text" id="constract_drawing8text" value="<?= $constract_drawing8text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8" name="constract_drawing8" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8" value="<?php if(!empty($constract_drawing8) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing8;}?>" hidden>
                                          </div>
                                          <p id="files8"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing8!="") echo $constract_drawing8; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check8" name="check8" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8" id="desc_item8" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8) && $category_name == "請負契約書・土地契約書"){ echo $desc_item8; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing9text" id="constract_drawing9text" value="<?= $constract_drawing9text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9" name="constract_drawing9" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9" value="<?php if(!empty($constract_drawing9) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing9;}?>" hidden>
                                          </div>
                                          <p id="files9"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing9!="") echo $constract_drawing9; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check9" name="check9" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9" id="desc_item9" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9) && $category_name == "請負契約書・土地契約書"){ echo $desc_item9; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10) && $category_name == "請負契約書・土地契約書") : ?> -->
                                            <input type="hidden" name="constract_drawing10text" id="constract_drawing10text" value="<?= $constract_drawing10text ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10" name="constract_drawing10" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10 ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10" value="<?php if(!empty($constract_drawing10) && $category_name == "請負契約書・土地契約書") { echo $date_insert_drawing10;}?>" hidden>
                                          </div>
                                          <p id="files10"><?php if($category_name == "請負契約書・土地契約書" && $constract_drawing10!="") echo $constract_drawing10; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10) && $category_name == "請負契約書・土地契約書") { echo 'checked';}?> id="check10" name="check10" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10" id="desc_item10" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10) && $category_name == "請負契約書・土地契約書"){ echo $desc_item10; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 2 -->
                            <input type="text" name="category_name1b" id="category_name1b" value="長期優良住宅認定書・性能評価書" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2" role="button" aria-expanded="false" aria-controls="column2">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>長期優良住宅認定書・性能評価書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1b == "長期優良住宅認定書・性能評価書"){echo "show";} ?>" id="column2">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php// if (!empty($constract_drawing1b)) : ?> -->
                                            <input type="hidden" name="constract_drawing1btext" id="constract_drawing1btext" value="<?= $constract_drawing1btext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1b" name="constract_drawing1b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1b" value="<?php if(!empty($constract_drawing1b)) { echo $date_insert_drawing1b;}?>" hidden>
                                          </div>
                                          <p id="files1b"><?php if($constract_drawing1b!="") echo $constract_drawing1b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1b)) { echo 'checked';}?> id="check1b" name="check1b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1b" id="desc_item1b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1b)){ echo $desc_item1b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2b)) : ?> -->
                                            <input type="hidden" name="constract_drawing2btext" id="constract_drawing2btext" value="<?= $constract_drawing2btext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2b" name="constract_drawing2b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2b" value="<?php if(!empty($constract_drawing2b)) { echo $date_insert_drawing2b;}?>" hidden>
                                          </div>
                                          <p id="files2b"><?php if($constract_drawing2b!="") echo $constract_drawing2b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2b)) { echo 'checked';}?> id="check2b" name="check2b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2b" id="desc_item2b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2b)){ echo $desc_item2b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3b)) : ?> -->
                                            <input type="hidden" name="constract_drawing3btext" id="constract_drawing3btext" value="<?= $constract_drawing3btext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3b" name="constract_drawing3b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3b" value="<?php if(!empty($constract_drawing3b)) { echo $date_insert_drawing3b;}?>" hidden>
                                          </div>
                                          <p id="files3b"><?php if($constract_drawing3b!="") echo $constract_drawing3b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3b)) { echo 'checked';}?> id="check3b" name="check3b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3b" id="desc_item3b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3b)){ echo $desc_item3b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4b)) : ?> -->
                                            <input type="hidden" name="constract_drawing4btext" id="constract_drawing4btext" value="<?= $constract_drawing4btext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4b" name="constract_drawing4b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4b" value="<?php if(!empty($constract_drawing4b)) { echo $date_insert_drawing4b;}?>" hidden>
                                          </div>
                                          <p id="files4b"><?php if($constract_drawing4b!="") echo $constract_drawing4b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4b)) { echo 'checked';}?> id="check4b" name="check4b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4b" id="desc_item4b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4b)){ echo $desc_item4b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5b)) : ?> -->
                                            <input type="hidden" name="constract_drawing5btext" id="constract_drawing5btext" value="<?= $constract_drawing5btext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5b" name="constract_drawing5b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5b" value="<?php if(!empty($constract_drawing5b)) { echo $date_insert_drawing5b;}?>" hidden>
                                          </div>
                                          <p id="files5b"><?php if($constract_drawing5b!="") echo $constract_drawing5b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5b)) { echo 'checked';}?> id="check5b" name="check5b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5b" id="desc_item5b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5b)){ echo $desc_item5b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6b)) : ?> -->
                                            <input type="hidden" name="constract_drawing6btext" id="constract_drawing6btext" value="<?= $constract_drawing6btext ?>">
                                            <!-- <?php// endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6b" name="constract_drawing6b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6b" value="<?php if(!empty($constract_drawing6b)) { echo $date_insert_drawing6b;}?>" hidden>
                                          </div>
                                          <p id="files6b"><?php if($constract_drawing6b!="") echo $constract_drawing6b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6b)) { echo 'checked';}?> id="check6b" name="check6b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6b" id="desc_item6b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6b)){ echo $desc_item6b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7b)) : ?> -->
                                            <input type="hidden" name="constract_drawing7btext" id="constract_drawing7btext" value="<?= $constract_drawing7btext ?>" >
                                            <!-- <?php// endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7b" name="constract_drawing7b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7b" value="<?php if(!empty($constract_drawing7b)) { echo $date_insert_drawing7b;}?>" hidden>
                                          </div>
                                          <p id="files7b"><?php if($constract_drawing7b!="") echo $constract_drawing7b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7b)) { echo 'checked';}?> id="check7b" name="check7b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7b" id="desc_item7b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7b)){ echo $desc_item7b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8b)) : ?> -->
                                            <input type="hidden" name="constract_drawing8btext" id="constract_drawing8btext" value="<?= $constract_drawing8btext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8b" name="constract_drawing8b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8b" value="<?php if(!empty($constract_drawing8b)) { echo $date_insert_drawing8b;}?>" hidden>
                                          </div>
                                          <p id="files8b"><?php if($constract_drawing8b!="") echo $constract_drawing8b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8b)) { echo 'checked';}?> id="check8b" name="check8b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8b" id="desc_item8b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8b)){ echo $desc_item8b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9b)) : ?> -->
                                            <input type="hidden" name="constract_drawing9btext" id="constract_drawing9btext" value="<?= $constract_drawing9btext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9b" name="constract_drawing9b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9b" value="<?php if(!empty($constract_drawing9b)) { echo $date_insert_drawing9b;}?>" hidden>
                                          </div>
                                          <p id="files9b"><?php if($constract_drawing9b!="") echo $constract_drawing9b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9b)) { echo 'checked';}?> id="check9b" name="check9b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9b" id="desc_item9b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9b)){ echo $desc_item9b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10b)) : ?> -->
                                            <input type="hidden" name="constract_drawing10b" value="<?= $constract_drawing10btext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10b" name="constract_drawing10b" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10b ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10b" value="<?php if(!empty($constract_drawing10b)) { echo $date_insert_drawing10b;}?>" hidden>
                                          </div>
                                          <p id="files10b"><?php if($constract_drawing10b!="") echo $constract_drawing10b; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10b"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10b)) { echo 'checked';}?> id="check10b" name="check10b" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10b">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10b">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10b" id="desc_item10b" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10b)){ echo $desc_item10b; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>

                            <!-- CATEGORY 3 -->
                            <input type="text" name="category_name1c" id="category_name1c" value="地盤調査・地盤改良" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3" role="button" aria-expanded="false" aria-controls="column3">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>地盤調査・地盤改良</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1c == "地盤調査・地盤改良"){echo "show";} ?>" id="column3">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1c)) : ?> -->
                                            <input type="hidden" name="constract_drawing1ctext" id="constract_drawing1ctext" value="<?= $constract_drawing1ctext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1c" name="constract_drawing1c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1c" value="<?php if(!empty($constract_drawing1c)) { echo $date_insert_drawing1c;}?>" hidden>
                                          </div>
                                          <p id="files1c"><?php if($constract_drawing1c!="") echo $constract_drawing1c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1c)) { echo 'checked';}?> id="check1c" name="check1c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1c" id="desc_item1c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1c)){ echo $desc_item1c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2c)) : ?> -->
                                            <input type="hidden" name="constract_drawing2ctext" id="constract_drawing2ctext" value="<?= $constract_drawing2ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2c" name="constract_drawing2c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2c" value="<?php if(!empty($constract_drawing2c)) { echo $date_insert_drawing2c;}?>" hidden>
                                          </div>
                                          <p id="files2c"><?php if($constract_drawing2c!="") echo $constract_drawing2c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2c)) { echo 'checked';}?> id="check2c" name="check2c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2c" id="desc_item2c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2c)){ echo $desc_item2c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3c)) : ?> -->
                                            <input type="hidden" name="constract_drawing3ctext" id="constract_drawing3ctext" value="<?= $constract_drawing3ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3c" name="constract_drawing3c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3c" value="<?php if(!empty($constract_drawing3c)) { echo $date_insert_drawing3c;}?>" hidden>
                                          </div>
                                          <p id="files3c"><?php if($constract_drawing3c!="") echo $constract_drawing3c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3c)) { echo 'checked';}?> id="check3c" name="check3c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3c" id="desc_item3c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3c)){ echo $desc_item3c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4c)) : ?> -->
                                            <input type="hidden" name="constract_drawing4ctext" id="constract_drawing4ctext" value="<?= $constract_drawing4ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4c" name="constract_drawing4c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4c" value="<?php if(!empty($constract_drawing4c)) { echo $date_insert_drawing4c;}?>" hidden>
                                          </div>
                                          <p id="files4c"><?php if($constract_drawing4c!="") echo $constract_drawing4c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4c)) { echo 'checked';}?> id="check4c" name="check4c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4c" id="desc_item4c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4c)){ echo $desc_item4c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5c)) : ?> -->
                                            <input type="hidden" name="constract_drawing5ctext" id="constract_drawing5ctext" value="<?= $constract_drawing5ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5c" name="constract_drawing5c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5c" value="<?php if(!empty($constract_drawing5c)) { echo $date_insert_drawing5c;}?>" hidden>
                                          </div>
                                          <p id="files5c"><?php if($constract_drawing5c!="") echo $constract_drawing5c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5c)) { echo 'checked';}?> id="check5c" name="check5c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5c" id="desc_item5c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5c)){ echo $desc_item5c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6c)) : ?> -->
                                            <input type="hidden" name="constract_drawing6ctext" id="constract_drawing6ctext" value="<?= $constract_drawing6ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6c" name="constract_drawing6c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6c" value="<?php if(!empty($constract_drawing6c)) { echo $date_insert_drawing6c;}?>" hidden>
                                          </div>
                                          <p id="files6c"><?php if($constract_drawing6c!="") echo $constract_drawing6c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6c)) { echo 'checked';}?> id="check6c" name="check6c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6c" id="desc_item6c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6c)){ echo $desc_item6c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7c)) : ?> -->
                                            <input type="hidden" name="constract_drawing7ctext" id="constract_drawing7ctext" value="<?= $constract_drawing7ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7c" name="constract_drawing7c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7c" value="<?php if(!empty($constract_drawing7c)) { echo $date_insert_drawing7c;}?>" hidden>
                                          </div>
                                          <p id="files7c"><?php if($constract_drawing7c!="") echo $constract_drawing7c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7c)) { echo 'checked';}?> id="check7c" name="check7c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7c" id="desc_item7c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7c)){ echo $desc_item7c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8c)) : ?> -->
                                            <input type="hidden" name="constract_drawing8ctext" id="constract_drawing8ctext" value="<?= $constract_drawing8ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8c" name="constract_drawing8c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8c" value="<?php if(!empty($constract_drawing8c)) { echo $date_insert_drawing8c;}?>" hidden>
                                          </div>
                                          <p id="files8c"><?php if($constract_drawing8c!="") echo $constract_drawing8c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8c)) { echo 'checked';}?> id="check8c" name="check8c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8c" id="desc_item8c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8c)){ echo $desc_item8c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9c)) : ?> -->
                                            <input type="hidden" name="constract_drawing9ctext" id="constract_drawing9ctext" value="<?= $constract_drawing9ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9c" name="constract_drawing9c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9c" value="<?php if(!empty($constract_drawing9c)) { echo $date_insert_drawing9c;}?>" hidden>
                                          </div>
                                          <p id="files9c"><?php if($constract_drawing9c!="") echo $constract_drawing9c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9c)) { echo 'checked';}?> id="check9c" name="check9c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9c" id="desc_item9c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9c)){ echo $desc_item9c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10c)) : ?> -->
                                            <input type="hidden" name="constract_drawing10ctext" id="constract_drawing10ctext" value="<?= $constract_drawing10ctext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10c" name="constract_drawing10c" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10c ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10c" value="<?php if(!empty($constract_drawing10c)) { echo $date_insert_drawing10c;}?>" hidden>
                                          </div>
                                          <p id="files10c"><?php if($constract_drawing10c!="") echo $constract_drawing10c; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10c"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10c)) { echo 'checked';}?> id="check10c" name="check10c" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10c">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10c">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10c" id="desc_item10c" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10c)){ echo $desc_item10c; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4 --> 
                            <input type="text" name="category_name1d" id="category_name1d" value="点検履歴" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4" role="button" aria-expanded="false" aria-controls="column4">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>点検履歴</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1d == "点検履歴"){echo "show";} ?>" id="column4">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1d)) : ?> -->
                                            <input type="hidden" name="constract_drawing1dtext" id="constract_drawing1dtext" value="<?= $constract_drawing1dtext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1d" name="constract_drawing1d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1d" value="<?php if(!empty($constract_drawing1d)) { echo $date_insert_drawing1d;}?>" hidden>
                                          </div>
                                          <p id="files1d"><?php if($constract_drawing1d!="") echo $constract_drawing1d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1d)) { echo 'checked';}?> id="check1d" name="check1d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1d" id="desc_item1d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1d)){ echo $desc_item1d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2d)) : ?> -->
                                            <input type="hidden" name="constract_drawing2dtext" id="constract_drawing2dtext" value="<?= $constract_drawing2dtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2d" name="constract_drawing2d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2d" value="<?php if(!empty($constract_drawing2d)) { echo $date_insert_drawing2d;}?>" hidden>
                                          </div>
                                          <p id="files2d"><?php if($constract_drawing2d!="") echo $constract_drawing2d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2d)) { echo 'checked';}?> id="check2d" name="check2d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2d" id="desc_item2d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2d)){ echo $desc_item2d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3d)) : ?> -->
                                            <input type="text" name="constract_drawing1dtext" id="constract_drawing1dtext" value="<?= $constract_drawing3dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3d" name="constract_drawing3d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3d" value="<?php if(!empty($constract_drawing3d)) { echo $date_insert_drawing3d;}?>" hidden>
                                          </div>
                                          <p id="files3d"><?php if($constract_drawing3d!="") echo $constract_drawing3d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3d)) { echo 'checked';}?> id="check3d" name="check3d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3d" id="desc_item3d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3d)){ echo $desc_item3d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4d)) : ?> -->
                                            <input type="text" name="constract_drawing4dtext" id="constract_drawing4dtext" value="<?= $constract_drawing4dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4d" name="constract_drawing4d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4d" value="<?php if(!empty($constract_drawing4d)) { echo $date_insert_drawing4d;}?>" hidden>
                                          </div>
                                          <p id="files4d"><?php if($constract_drawing4d!="") echo $constract_drawing4d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4d)) { echo 'checked';}?> id="check4d" name="check4d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4d" id="desc_item4d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4d)){ echo $desc_item4d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5d)) : ?> -->
                                            <input type="text" name="constract_drawing5dtext" id="constract_drawing5dtext" value="<?= $constract_drawing5dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5d" name="constract_drawing5d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5d" value="<?php if(!empty($constract_drawing5d)) { echo $date_insert_drawing5d;}?>" hidden>
                                          </div>
                                          <p id="files5d"><?php if($constract_drawing5d!="") echo $constract_drawing5d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5d)) { echo 'checked';}?> id="check5d" name="check5d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5d" id="desc_item5d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5d)){ echo $desc_item5d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6d)) : ?> -->
                                            <input type="text" name="constract_drawing6dtext" id="constract_drawing6dtext" value="<?= $constract_drawing6dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6d" name="constract_drawing6d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6d" value="<?php if(!empty($constract_drawing6d)) { echo $date_insert_drawing6d;}?>" hidden>
                                          </div>
                                          <p id="files6d"><?php if($constract_drawing6d!="") echo $constract_drawing6d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6d)) { echo 'checked';}?> id="check6d" name="check6d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6d" id="desc_item6d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6d)){ echo $desc_item6d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7d)) : ?> -->
                                            <input type="text" name="constract_drawing7dtext" id="constract_drawing7dtext" value="<?= $constract_drawing7dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7d" name="constract_drawing7d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7d" value="<?php if(!empty($constract_drawing7d)) { echo $date_insert_drawing7d;}?>" hidden>
                                          </div>
                                          <p id="files7d"><?php if($constract_drawing7d!="") echo $constract_drawing7d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7d)) { echo 'checked';}?> id="check7d" name="check7d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7d" id="desc_item7d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7d)){ echo $desc_item7d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8d)) : ?> -->
                                            <input type="text" name="constract_drawing8dtext" id="constract_drawing8dtext" value="<?= $constract_drawing8dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8d" name="constract_drawing8d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8d" value="<?php if(!empty($constract_drawing8d)) { echo $date_insert_drawing8d;}?>" hidden>
                                          </div>
                                          <p id="files8d"><?php if($constract_drawing8d!="") echo $constract_drawing8d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8d)) { echo 'checked';}?> id="check8d" name="check8d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8d" id="desc_item8d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8d)){ echo $desc_item8d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9d)) : ?> -->
                                            <input type="text" name="constract_drawing9dtext" id="constract_drawing9dtext" value="<?= $constract_drawing9dtext?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9d" name="constract_drawing9d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9d" value="<?php if(!empty($constract_drawing9d)) { echo $date_insert_drawing9d;}?>" hidden>
                                          </div>
                                          <p id="files9d"><?php if($constract_drawing9d!="") echo $constract_drawing9d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9d)) { echo 'checked';}?> id="check9d" name="check9d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9d" id="desc_item9d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9d)){ echo $desc_item9d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10d)) : ?> -->
                                            <input type="text" name="constract_drawing10dtext" id="constract_drawing10dtext" value="<?= $constract_drawing10dtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10d" name="constract_drawing10d" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10d ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10d" value="<?php if(!empty($constract_drawing10d)) { echo $date_insert_drawing10d;}?>" hidden>
                                          </div>
                                          <p id="files10d"><?php if($constract_drawing10d!="") echo $constract_drawing10d; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10d"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10d)) { echo 'checked';}?> id="check10d" name="check10d" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10d">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10d">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10d" id="desc_item10d" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10d)){ echo $desc_item10d; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5 -->
                            <input type="text" name="category_name1e" id="category_name1e" value="三者引継ぎ合意書" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5" role="button" aria-expanded="false" aria-controls="column5">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>三者引継ぎ合意書</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1e == "三者引継ぎ合意書"){echo "show";} ?>" id="column5">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1e)) : ?> -->
                                            <input type="text" name="constract_drawing1etext" id="constract_drawing1etext" value="<?= $constract_drawing1etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1e" name="constract_drawing1e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1e" value="<?php if(!empty($constract_drawing1e)) { echo $date_insert_drawing1e;}?>" hidden>
                                          </div>
                                          <p id="files1e"><?php if($constract_drawing1e!="") echo $constract_drawing1e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1e)) { echo 'checked';}?> id="check1e" name="check1e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1e" id="desc_item1e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1e)){ echo $desc_item1e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2e)) : ?> -->
                                            <input type="text" name="constract_drawing2etext" id="constract_drawing2etext" value="<?= $constract_drawing2etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2e" name="constract_drawing2e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2e" value="<?php if(!empty($constract_drawing2e)) { echo $date_insert_drawing2e;}?>" hidden>
                                          </div>
                                          <p id="files2e"><?php if($constract_drawing2e!="") echo $constract_drawing2e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2e)) { echo 'checked';}?> id="check2e" name="check2e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2e" id="desc_item2e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2e)){ echo $desc_item2e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3e)) : ?> -->
                                            <input type="text" name="constract_drawing3etext" id="constract_drawing3etext" value="<?= $constract_drawing3etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3e" name="constract_drawing3e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3e" value="<?php if(!empty($constract_drawing3e)) { echo $date_insert_drawing3e;}?>" hidden>
                                          </div>
                                          <p id="files3e"><?php if($constract_drawing3e!="") echo $constract_drawing3e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3e)) { echo 'checked';}?> id="check3e" name="check3e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3e" id="desc_item3e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3e)){ echo $desc_item3e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4e)) : ?> -->
                                            <input type="text" name="constract_drawing4etext" id="constract_drawing4etext" value="<?= $constract_drawing4etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4e" name="constract_drawing4e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4e" value="<?php if(!empty($constract_drawing4e)) { echo $date_insert_drawing4e;}?>" hidden>
                                          </div>
                                          <p id="files4e"><?php if($constract_drawing4e!="") echo $constract_drawing4e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4e)) { echo 'checked';}?> id="check4e" name="check4e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4e" id="desc_item4e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4e)){ echo $desc_item4e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5e)) : ?> -->
                                            <input type="text" name="constract_drawing5etext" id="constract_drawing5etext" value="<?= $constract_drawing5etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5e" name="constract_drawing5e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5e" value="<?php if(!empty($constract_drawing5e)) { echo $date_insert_drawing5e;}?>" hidden>
                                          </div>
                                          <p id="files5e"><?php if($constract_drawing5e!="") echo $constract_drawing5e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5e)) { echo 'checked';}?> id="check5e" name="check5e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5e" id="desc_item5e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5e)){ echo $desc_item5e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6e)) : ?> -->
                                            <input type="text" name="constract_drawing6etext" id="constract_drawing6etext" value="<?= $constract_drawing6etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6e" name="constract_drawing6e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6e" value="<?php if(!empty($constract_drawing6e)) { echo $date_insert_drawing6e;}?>" hidden>
                                          </div>
                                          <p id="files6e"><?php if($constract_drawing6e!="") echo $constract_drawing6e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6e)) { echo 'checked';}?> id="check6e" name="check6e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6e" id="desc_item6e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6e)){ echo $desc_item6e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7e)) : ?> -->
                                            <input type="text" name="constract_drawing7etext" id="constract_drawing7etext" value="<?= $constract_drawing7etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7e" name="constract_drawing7e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7e" value="<?php if(!empty($constract_drawing7e)) { echo $date_insert_drawing7e;}?>" hidden>
                                          </div>
                                          <p id="files7e"><?php if($constract_drawing7e!="") echo $constract_drawing7e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7e)) { echo 'checked';}?> id="check7e" name="check7e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7e" id="desc_item7e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7e)){ echo $desc_item7e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8e)) : ?> -->
                                            <input type="text" name="constract_drawing8etext" id="constract_drawing8etext" value="<?= $constract_drawing8etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8e" name="constract_drawing8e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8e" value="<?php if(!empty($constract_drawing8e)) { echo $date_insert_drawing8e;}?>" hidden>
                                          </div>
                                          <p id="files8e"><?php if($constract_drawing8e!="") echo $constract_drawing8e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8e)) { echo 'checked';}?> id="check8e" name="check8e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8e" id="desc_item8e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8e)){ echo $desc_item8e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9e)) : ?> -->
                                            <input type="text" name="constract_drawing9etext" id="constract_drawing9etext" value="<?= $constract_drawing9etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9e" name="constract_drawing9e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9e" value="<?php if(!empty($constract_drawing9e)) { echo $date_insert_drawing9e;}?>" hidden>
                                          </div>
                                          <p id="files9e"><?php if($constract_drawing9e!="") echo $constract_drawing9e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9e)) { echo 'checked';}?> id="check9e" name="check9e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9e" id="desc_item9e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9e)){ echo $desc_item9e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10e)) : ?> -->
                                            <input type="text" name="constract_drawing10etext" id="constract_drawing10etext" value="<?= $constract_drawing10etext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10e" name="constract_drawing10e" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10e ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10e" value="<?php if(!empty($constract_drawing10e)) { echo $date_insert_drawing10e;}?>" hidden>
                                          </div>
                                          <p id="files10e"><?php if($constract_drawing10e!="") echo $constract_drawing10e; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10e"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10e)) { echo 'checked';}?> id="check10e" name="check10e" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10e">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10e">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10e" id="desc_item10e" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10e)){ echo $desc_item10e; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6 -->
                            <input type="text" name="category_name1f" id="category_name1f" value="住宅仕様書・プレカット資料" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6" role="button" aria-expanded="false" aria-controls="column6">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>住宅仕様書・プレカット資料</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1f == "住宅仕様書・プレカット資料"){echo "show";} ?>" id="column6">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1f)) : ?> -->
                                            <input type="text" name="constract_drawing1ftext" id="constract_drawing1ftext" value="<?= $constract_drawing1ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1f" name="constract_drawing1f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1f" value="<?php if(!empty($constract_drawing1f)) { echo $date_insert_drawing1f;}?>" hidden>
                                          </div>
                                          <p id="files1f"><?php if($constract_drawing1f!="") echo $constract_drawing1f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1f)) { echo 'checked';}?> id="check1f" name="check1f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1f" id="desc_item1f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1f)){ echo $desc_item1f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php// if (!empty($constract_drawing2f)) : ?> -->
                                            <input type="text" name="constract_drawing2ftext" id="constract_drawing2ftext" value="<?= $constract_drawing2ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2f" name="constract_drawing2f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2f" value="<?php if(!empty($constract_drawing2f)) { echo $date_insert_drawing2f;}?>" hidden>
                                          </div>
                                          <p id="files2f"><?php if($constract_drawing2f!="") echo $constract_drawing2f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2f)) { echo 'checked';}?> id="check2f" name="check2f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2f" id="desc_item2f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1f)){ echo $desc_item1f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3f)) : ?> -->
                                            <input type="text" name="constract_drawing3ftext" id="constract_drawing3ftext" value="<?= $constract_drawing3ftext ?>" hidden>
                                            <!-- <?php// endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3f" name="constract_drawing3f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3f" value="<?php if(!empty($constract_drawing3f)) { echo $date_insert_drawing3f;}?>" hidden>
                                          </div>
                                          <p id="files3f"><?php if($constract_drawing3f!="") echo $constract_drawing3f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3f)) { echo 'checked';}?> id="check3f" name="check3f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3f" id="desc_item3f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3f)){ echo $desc_item3f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4f)) : ?> -->
                                            <input type="text" name="constract_drawing4ftext" id="constract_drawing4ftext" value="<?= $constract_drawing4ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4f" name="constract_drawing4f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4f" value="<?php if(!empty($constract_drawing4f)) { echo $date_insert_drawing4f;}?>" hidden>
                                          </div>
                                          <p id="files4f"><?php if($constract_drawing4f!="") echo $constract_drawing4f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4f)) { echo 'checked';}?> id="check4f" name="check4f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4f" id="desc_item4f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4f)){ echo $desc_item4f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5f)) : ?> -->
                                            <input type="text" name="constract_drawing5ftext" id="constract_drawing5ftext" value="<?= $constract_drawing5ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5f" name="constract_drawing5f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5f" value="<?php if(!empty($constract_drawing5f)) { echo $date_insert_drawing5f;}?>" hidden>
                                          </div>
                                          <p id="files5f"><?php if($constract_drawing5f!="") echo $constract_drawing5f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5f)) { echo 'checked';}?> id="check5f" name="check5f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5f" id="desc_item5f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5f)){ echo $desc_item5f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6f)) : ?> -->
                                            <input type="text" name="constract_drawing6ftext" id="constract_drawing6ftext" value="<?= $constract_drawing6ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6f" name="constract_drawing6f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6f" value="<?php if(!empty($constract_drawing6f)) { echo $date_insert_drawing6f;}?>" hidden>
                                          </div>
                                          <p id="files6f"><?php if($constract_drawing6f!="") echo $constract_drawing6f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6f)) { echo 'checked';}?> id="check6f" name="check6f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6f" id="desc_item6f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6f)){ echo $desc_item6f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7f)) : ?> -->
                                            <input type="text" name="constract_drawing7ftext" id="constract_drawing7ftext" value="<?= $constract_drawing7ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7f" name="constract_drawing7f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7f" value="<?php if(!empty($constract_drawing7f)) { echo $date_insert_drawing7f;}?>" hidden>
                                          </div>
                                          <p id="files7f"><?php if($constract_drawing7f!="") echo $constract_drawing7f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7f)) { echo 'checked';}?> id="check7f" name="check7f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7f" id="desc_item7f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7f)){ echo $desc_item7f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8f)) : ?> -->
                                            <input type="text" name="constract_drawing8ftext" id="constract_drawing8ftext" value="<?= $constract_drawing8ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8f" name="constract_drawing8f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8f" value="<?php if(!empty($constract_drawing8f)) { echo $date_insert_drawing8f;}?>" hidden>
                                          </div>
                                          <p id="files8f"><?php if($constract_drawing8f!="") echo $constract_drawing8f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8f)) { echo 'checked';}?> id="check8f" name="check8f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8f" id="desc_item8f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8f)){ echo $desc_item8f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9f)) : ?> -->
                                            <input type="text" name="constract_drawing9ftext" id="constract_drawing9ftext" value="<?= $constract_drawing9ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9f" name="constract_drawing9f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9f" value="<?php if(!empty($constract_drawing9f)) { echo $date_insert_drawing9f;}?>" hidden>
                                          </div>
                                          <p id="files9f"><?php if($constract_drawing9f!="") echo $constract_drawing9f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9f)) { echo 'checked';}?> id="check9f" name="check9f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9f" id="desc_item9f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9f)){ echo $desc_item9f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10f)) : ?> -->
                                            <input type="text" name="constract_drawing10ftext" id="constract_drawing10ftext" value="<?= $constract_drawing10ftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10f" name="constract_drawing10f" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10f ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10f" value="<?php if(!empty($constract_drawing10f)) { echo $date_insert_drawing10f;}?>" hidden>
                                          </div>
                                          <p id="files10f"><?php if($constract_drawing10f!="") echo $constract_drawing10f; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10f"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10f)) { echo 'checked';}?> id="check10f" name="check10f" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10f">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10f">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10f" id="desc_item10f" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10f)){ echo $desc_item10f; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 7 -->
                            <input type="text" name="category_name1g" id="category_name1g" value="工事写真" hidden>
                            <input type="text" name="type_item" id="type_item" value="admin" hidden>
                            <div class="card">
                              <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7" role="button" aria-expanded="false" aria-controls="column7">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>工事写真</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>社内</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1g == "工事写真"){echo "show";} ?>" id="column7">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1g)) : ?> -->
                                            <input type="text" name="constract_drawing1gtext" id="constract_drawing1gtext" value="<?= $constract_drawing1gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1g" name="constract_drawing1g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1g" value="<?php if(!empty($constract_drawing1g)) { echo $date_insert_drawing1g;}?>" hidden>
                                          </div>
                                          <p id="files1g"><?php if($constract_drawing1g!="") echo $constract_drawing1g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1g)) { echo 'checked';}?> id="check1g" name="check1g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1g" id="desc_item1g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1g)){ echo $desc_item1g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2g)) : ?> -->
                                            <input type="text" name="constract_drawing2gtext" id="constract_drawing2gtext" value="<?= $constract_drawing2gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2g" name="constract_drawing2g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2g" value="<?php if(!empty($constract_drawing2g)) { echo $date_insert_drawing2g;}?>" hidden>
                                          </div>
                                          <p id="files2g"><?php if($constract_drawing2g!="") echo $constract_drawing2g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2g)) { echo 'checked';}?> id="check2g" name="check2g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2g" id="desc_item2g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2g)){ echo $desc_item2g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3g)) : ?> -->
                                            <input type="text" name="constract_drawing3gtext" id="constract_drawing3gtext" value="<?= $constract_drawing3gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3g" name="constract_drawing3g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3g" value="<?php if(!empty($constract_drawing3g)) { echo $date_insert_drawing3g;}?>" hidden>
                                          </div>
                                          <p id="files3g"><?php if($constract_drawing3g!="") echo $constract_drawing3g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3g)) { echo 'checked';}?> id="check3g" name="check3g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3g" id="desc_item3g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3g)){ echo $desc_item3g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4g)) : ?> -->
                                            <input type="text" name="constract_drawing4gtext" id="constract_drawing4gtext" value="<?= $constract_drawing4gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4g" name="constract_drawing4g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4g" value="<?php if(!empty($constract_drawing4g)) { echo $date_insert_drawing4g;}?>" hidden>
                                          </div>
                                          <p id="files4g"><?php if($constract_drawing4g!="") echo $constract_drawing4g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4g)) { echo 'checked';}?> id="check4g" name="check4g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4g" id="desc_item4g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4g)){ echo $desc_item4g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5g)) : ?> -->
                                            <input type="text" name="constract_drawing5gtext" id="constract_drawing5gtext" value="<?= $constract_drawing5gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5g" name="constract_drawing5g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5g" value="<?php if(!empty($constract_drawing5g)) { echo $date_insert_drawing5g;}?>" hidden>
                                          </div>
                                          <p id="files5g"><?php if($constract_drawing5g!="") echo $constract_drawing5g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5g)) { echo 'checked';}?> id="check5g" name="check5g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5g" id="desc_item5g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5g)){ echo $desc_item5g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6g)) : ?> -->
                                            <input type="text" name="constract_drawing6gtext" id="constract_drawing6gtext" value="<?= $constract_drawing6gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6g" name="constract_drawing6g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6g" value="<?php if(!empty($constract_drawing6g)) { echo $date_insert_drawing6g;}?>" hidden>
                                          </div>
                                          <p id="files6g"><?php if($constract_drawing6g!="") echo $constract_drawing6g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6g)) { echo 'checked';}?> id="check6g" name="check6g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6g" id="desc_item6g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6g)){ echo $desc_item6g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7g)) : ?> -->
                                            <input type="text" name="constract_drawing7gtext" id="constract_drawing7gtext" value="<?= $constract_drawing7gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7g" name="constract_drawing7g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7g" value="<?php if(!empty($constract_drawing7g)) { echo $date_insert_drawing7g;}?>" hidden>
                                          </div>
                                          <p id="files7g"><?php if($constract_drawing7g!="") echo $constract_drawing7g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7g)) { echo 'checked';}?> id="check7g" name="check7g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7g" id="desc_item7g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7g)){ echo $desc_item7g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8g)) : ?> -->
                                            <input type="text" name="constract_drawing8gtext" id="constract_drawing8gtext" value="<?= $constract_drawing8gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8g" name="constract_drawing8g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8g" value="<?php if(!empty($constract_drawing8g)) { echo $date_insert_drawing8g;}?>" hidden>
                                          </div>
                                          <p id="files8g"><?php if($constract_drawing8g!="") echo $constract_drawing8g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8g)) { echo 'checked';}?> id="check8g" name="check8g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8g" id="desc_item8g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8g)){ echo $desc_item8g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9g)) : ?> -->
                                            <input type="text" name="constract_drawing9gtext" id="constract_drawing9gtext" value="<?= $constract_drawing9gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9g" name="constract_drawing9g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9g" value="<?php if(!empty($constract_drawing9g)) { echo $date_insert_drawing9g;}?>" hidden>
                                          </div>
                                          <p id="files9g"><?php if($constract_drawing9g!="") echo $constract_drawing9g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9g)) { echo 'checked';}?> id="check9g" name="check9g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9g" id="desc_item9g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9g)){ echo $desc_item9g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10g)) : ?> -->
                                            <input type="text" name="constract_drawing10gtext" id="constract_drawing10gtext" value="<?= $constract_drawing10gtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10g" name="constract_drawing10g" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10g ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10g" value="<?php if(!empty($constract_drawing10g)) { echo $date_insert_drawing10g;}?>" hidden>
                                          </div>
                                          <p id="files10g"><?php if($constract_drawing10g!="") echo $constract_drawing10g; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10g"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10g)) { echo 'checked';}?> id="check10g" name="check10g" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10g">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10g">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10g" id="desc_item10g" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10g)){ echo $desc_item10g; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 1BLUE -->
                            <input type="text" name="category_name1bluea" id="category_name1bluea" value="確認申請書一式" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingOneBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column1blue" role="button" aria-expanded="false" aria-controls="column1blue">
                                    <div class="leftSide">
                                      <span>1</span>
                                      <p>確認申請書一式 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1bluea == "確認申請書一式"){echo "show";} ?>" id="column1blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <!-- <?php //if (!empty($constract_drawing1bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing1blueatext" name="constract_drawing1blueatext" value="<?= $constract_drawing1blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluea" name="constract_drawing1bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1bluea" value="<?php if(!empty($constract_drawing1bluea)) { echo $date_insert_drawing1bluea;}?>" hidden>
                                          </div>
                                          <p id="files1bluea"><?php if($constract_drawing1bluea!="") echo $constract_drawing1bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluea" name="co_owner1bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1bluea)) {
                                              $selected_array = explode(';',$co_owner1bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address" name="duplicate-address">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1bluea)) { echo 'checked';}?> id="check1bluea" name="check1bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluea" id="desc_item1bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1bluea)){ echo $desc_item1bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing2blueatext" name="constract_drawing2blueatext" value="<?= $constract_drawing2blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluea" name="constract_drawing2bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2bluea" value="<?php if(!empty($constract_drawing2bluea)) { echo $date_insert_drawing2bluea;}?>" hidden>
                                          </div>
                                          <p id="files2bluea"><?php if($constract_drawing2bluea!="") echo $constract_drawing2bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluea" name="co_owner2bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2bluea)) {
                                              $selected_array = explode(';',$co_owner2bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2bluea)) { echo 'checked';}?> id="check2bluea" name="check2bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluea" id="desc_item2bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2bluea)){ echo $desc_item2bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing3blueatext" name="constract_drawing3blueatext" value="<?= $constract_drawing3blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluea" name="constract_drawing3bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3bluea" value="<?php if(!empty($constract_drawing3bluea)) { echo $date_insert_drawing3bluea;}?>" hidden>
                                          </div>
                                          <p id="files3bluea"><?php if($constract_drawing3bluea!="") echo $constract_drawing3bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluea" name="co_owner3bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3bluea)) {
                                              $selected_array = explode(';',$co_owner3bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3bluea)) { echo 'checked';}?> id="check3bluea" name="check3bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluea" id="desc_item3bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3bluea)){ echo $desc_item3bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing4blueatext" name="constract_drawing4blueatext" value="<?= $constract_drawing4blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluea" name="constract_drawing4bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4bluea" value="<?php if(!empty($constract_drawing4bluea)) { echo $date_insert_drawing4bluea;}?>" hidden>
                                          </div>
                                          <p id="files4bluea"><?php if($constract_drawing4bluea!="") echo $constract_drawing4bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluea" name="co_owner4bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4bluea)) {
                                              $selected_array = explode(';',$co_owner4bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4bluea)) { echo 'checked';}?> id="check4bluea" name="check4bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluea" id="desc_item4bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4bluea)){ echo $desc_item4bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing5blueatext" name="constract_drawing5blueatext" value="<?= $constract_drawing5blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluea" name="constract_drawing5bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5bluea" value="<?php if(!empty($constract_drawing5bluea)) { echo $date_insert_drawing5bluea;}?>" hidden>
                                          </div>
                                          <p id="files5bluea"><?php if($constract_drawing5bluea!="") echo $constract_drawing5bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluea" name="co_owner5bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5bluea)) {
                                              $selected_array = explode(';',$co_owner5bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5bluea)) { echo 'checked';}?> id="check5bluea" name="check5bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluea" id="desc_item5bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5bluea)){ echo $desc_item5bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php// if (!empty($constract_drawing6bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing6blueatext" name="constract_drawing6blueatext" value="<?= $constract_drawing6blueatext ?>" >
                                            <!-- <?php// endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluea" name="constract_drawing6bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6bluea" value="<?php if(!empty($constract_drawing6bluea)) { echo $date_insert_drawing6bluea;}?>" hidden>
                                          </div>
                                          <p id="files6bluea"><?php if($constract_drawing6bluea!="") echo $constract_drawing6bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6bluea"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluea" name="co_owner6bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6bluea)) {
                                              $selected_array = explode(';',$co_owner6bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6bluea)) { echo 'checked';}?> id="check6bluea" name="check6bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluea" id="desc_item6bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6bluea)){ echo $desc_item6bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing7blueatext" name="constract_drawing7blueatext" value="<?= $constract_drawing7blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluea" name="constract_drawing7bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7bluea" value="<?php if(!empty($constract_drawing7bluea)) { echo $date_insert_drawing7bluea;}?>" hidden>
                                          </div>
                                          <p id="files7bluea"><?php if($constract_drawing7bluea!="") echo $constract_drawing7bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluea" name="co_owner7bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7bluea)) {
                                              $selected_array = explode(';',$co_owner7bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7bluea)) { echo 'checked';}?> id="check7bluea" name="check7bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluea" id="desc_item7bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7bluea)){ echo $desc_item7bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing8blueatext" name="constract_drawing8blueatext" value="<?= $constract_drawing8blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluea" name="constract_drawing8bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8bluea" value="<?php if(!empty($constract_drawing8bluea)) { echo $date_insert_drawing8bluea;}?>" hidden>
                                          </div>
                                          <p id="files8bluea"><?php if($constract_drawing8bluea!="") echo $constract_drawing8bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluea" name="co_owner8bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8bluea)) {
                                              $selected_array = explode(';',$co_owner8bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8bluea)) { echo 'checked';}?> id="check8bluea" name="check8bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluea" id="desc_item8bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8bluea)){ echo $desc_item8bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing9blueatext" name="constract_drawing9blueatext" value="<?= $constract_drawing9blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluea" name="constract_drawing9bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9bluea" value="<?php if(!empty($constract_drawing9bluea)) { echo $date_insert_drawing9bluea;}?>" hidden>
                                          </div>
                                          <p id="files9bluea"><?php if($constract_drawing9bluea!="") echo $constract_drawing9bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluea" name="co_owner9bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9bluea)) {
                                              $selected_array = explode(';',$co_owner9bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9bluea)) { echo 'checked';}?> id="check9bluea" name="check9bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluea" id="desc_item9bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9bluea)){ echo $desc_item9bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10bluea)) : ?> -->
                                            <input type="hidden" id="constract_drawing10blueatext" name="constract_drawing10blueatext" value="<?= $constract_drawing10blueatext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluea" name="constract_drawing10bluea" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10bluea ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10bluea" value="<?php if(!empty($constract_drawing10bluea)) { echo $date_insert_drawing10bluea;}?>" hidden>
                                          </div>
                                          <p id="files10bluea"><?php if($constract_drawing10bluea!="") echo $constract_drawing10bluea; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10bluea"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluea">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluea" name="co_owner10bluea[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10bluea)) {
                                              $selected_array = explode(';',$co_owner10bluea);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            } 
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10bluea)) { echo 'checked';}?> id="check10bluea" name="check10bluea" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluea">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluea">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluea" id="desc_item10bluea" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10bluea)){ echo $desc_item10bluea; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 2BLUE -->
                            <input type="text" name="category_name1blueb" id="category_name1blueb" value="工事工程表" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingTwoBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column2blue" role="button" aria-expanded="false" aria-controls="column2blue">
                                    <div class="leftSide">
                                      <span>2</span>
                                      <p>工事工程表 </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1blueb == "工事工程表"){echo "show";} ?>" id="column2blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing1bluebtext" name="constract_drawing1bluebtext" value="<?= $constract_drawing1bluebtext ?>" >
                                            <!-- <?php// endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1blueb" name="constract_drawing1blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1blueb" value="<?php if(!empty($constract_drawing1blueb)) { echo $date_insert_drawing1blueb;}?>" hidden>
                                          </div>
                                          <p id="files1blueb"><?php if($constract_drawing1blueb!="") echo $constract_drawing1blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueb" name="co_owner1blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1blueb)) {
                                              $selected_array = explode(';',$co_owner1blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address2" name="duplicate-address2">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1blueb)) { echo 'checked';}?> id="check1blueb" name="check1blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueb" id="desc_item1blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1blueb)){ echo $desc_item1blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing2bluebtext" name="constract_drawing2bluebtext" value="<?= $constract_drawing2bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2blueb" name="constract_drawing2blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2blueb" value="<?php if(!empty($constract_drawing2blueb)) { echo $date_insert_drawing2blueb;}?>" hidden>
                                          </div>
                                          <p id="files2blueb"><?php if($constract_drawing2blueb!="") echo $constract_drawing2blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueb" name="co_owner2blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2blueb)) {
                                              $selected_array = explode(';',$co_owner2blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2blueb)) { echo 'checked';}?>  id="check2blueb" name="check2blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueb" id="desc_item2blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2blueb)){ echo $desc_item2blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing3bluebtext" name="constract_drawing3bluebtext" value="<?= $constract_drawing3bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3blueb" name="constract_drawing3blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3blueb" value="<?php if(!empty($constract_drawing3blueb)) { echo $date_insert_drawing3blueb;}?>" hidden>
                                          </div>
                                          <p id="files3blueb"><?php if($constract_drawing3blueb!="") echo $constract_drawing3blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueb" name="co_owner3blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3blueb)) {
                                              $selected_array = explode(';',$co_owner3blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3blueb)) { echo 'checked';}?> id="check3blueb" name="check3blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueb" id="desc_item3blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3blueb)){ echo $desc_item3blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4blueb)) : ?> -->
                                            <input type="hidden"  id="constract_drawing4bluebtext" name="constract_drawing4bluebtext" value="<?= $constract_drawing4bluebtext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4blueb" name="constract_drawing4blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4blueb" value="<?php if(!empty($constract_drawing4blueb)) { echo $date_insert_drawing4blueb;}?>" hidden>
                                          </div>
                                          <p id="files4blueb"><?php if($constract_drawing4blueb!="") echo $constract_drawing4blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueb" name="co_owner4blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4blueb)) {
                                              $selected_array = explode(';',$co_owner4blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4blueb)) { echo 'checked';}?> id="check4blueb" name="check4blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueb" id="desc_item4blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4blueb)){ echo $desc_item4blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing5bluebtext" name="constract_drawing5bluebtext" value="<?= $constract_drawing5bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5blueb" name="constract_drawing5blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5blueb" value="<?php if(!empty($constract_drawing5blueb)) { echo $date_insert_drawing5blueb;}?>" hidden>
                                          </div>
                                          <p id="files5blueb"><?php if($constract_drawing5blueb!="") echo $constract_drawing5blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueb" name="co_owner5blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5blueb)) {
                                              $selected_array = explode(';',$co_owner5blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5blueb)) { echo 'checked';}?> id="check5blueb" name="check5blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueb" id="desc_item5blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5blueb)){ echo $desc_item5blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing6bluebtext" name="constract_drawing6bluebtext" value="<?= $constract_drawing6bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6blueb" name="constract_drawing6blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6blueb" value="<?php if(!empty($constract_drawing6blueb)) { echo $date_insert_drawing6blueb;}?>" hidden>
                                          </div>
                                          <p id="files6blueb"><?php if($constract_drawing6blueb!="") echo $constract_drawing6blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6blueb"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueb" name="co_owner6blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6blueb)) {
                                              $selected_array = explode(';',$co_owner6blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6blueb)) { echo 'checked';}?> id="check6blueb" name="check6blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueb" id="desc_item6blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6blueb)){ echo $desc_item6blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing7bluebtext" name="constract_drawing7bluebtext" value="<?= $constract_drawing7bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7blueb" name="constract_drawing7blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7blueb" value="<?php if(!empty($constract_drawing7blueb)) { echo $date_insert_drawing7blueb;}?>" hidden>
                                          </div>
                                          <p id="files7blueb"><?php if($constract_drawing7blueb!="") echo $constract_drawing7blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueb" name="co_owner7blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7blueb)) {
                                              $selected_array = explode(';',$co_owner7blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            } 
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7blueb)) { echo 'checked';}?>  id="check7blueb" name="check7blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueb" id="desc_item7blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7blueb)){ echo $desc_item7blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing8bluebtext" name="constract_drawing8bluebtext" value="<?= $constract_drawing8bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8blueb" name="constract_drawing8blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8blueb" value="<?php if(!empty($constract_drawing8blueb)) { echo $date_insert_drawing8blueb;}?>" hidden>
                                          </div>
                                          <p id="files8blueb"><?php if($constract_drawing8blueb!="") echo $constract_drawing8blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueb" name="co_owner8blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8blueb)) {
                                              $selected_array = explode(';',$co_owner8blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8blueb)) { echo 'checked';}?> id="check8blueb" name="check8blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueb" id="desc_item8blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8blueb)){ echo $desc_item8blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9blueb)) : ?> -->
                                            <input type="hidden" id="constract_drawing9bluebtext" name="constract_drawing9bluebtext" value="<?= $constract_drawing9bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9blueb" name="constract_drawing9blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9blueb" value="<?php if(!empty($constract_drawing9blueb)) { echo $date_insert_drawing9blueb;}?>" hidden>
                                          </div>
                                          <p id="files9blueb"><?php if($constract_drawing9blueb!="") echo $constract_drawing9blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueb" name="co_owner9blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9blueb)) {
                                              $selected_array = explode(';',$co_owner9blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9blueb)) { echo 'checked';}?> id="check9blueb" name="check9blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueb" id="desc_item9blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9blueb)){ echo $desc_item9blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10blueb)) : ?> -->
                                            <input type="hidden"  id="constract_drawing10bluebtext" name="constract_drawing10bluebtext" value="<?= $constract_drawing10bluebtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10blueb" name="constract_drawing10blueb" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10blueb ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10blueb" value="<?php if(!empty($constract_drawing10blueb)) { echo $date_insert_drawing10blueb;}?>" hidden>
                                          </div>
                                          <p id="files10blueb"><?php if($constract_drawing10blueb!="") echo $constract_drawing10blueb; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10blueb"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueb">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueb" name="co_owner10blueb[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10blueb)) {
                                              $selected_array = explode(';',$co_owner10blueb);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10blueb)) { echo 'checked';}?> id="check10blueb" name="check10blueb" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blueb">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueb">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueb" id="desc_item10blueb" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10blueb)){ echo $desc_item10blueb; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 3BLUE -->
                            <input type="text" name="category_name1bluec" id="category_name1bluec" value="工事着工資料" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingThreeBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column3blue" role="button" aria-expanded="false" aria-controls="column3blue">
                                    <div class="leftSide">
                                      <span>3</span>
                                      <p>工事着工資料  </p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse  <?php if($category_name1bluec == "工事着工資料"){echo "show";} ?>" id="column3blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing1bluectext" name="constract_drawing1bluectext" value="<?= $constract_drawing1bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluec" name="constract_drawing1bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1bluec" value="<?php if(!empty($constract_drawing1bluec)) { echo $date_insert_drawing1bluec;}?>" hidden>
                                          </div>
                                          <p id="files1bluec"><?php if($constract_drawing1bluec!="") echo $constract_drawing1bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluec" name="co_owner1bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1bluec)) {
                                              $selected_array = explode(';',$co_owner1bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address3" name="duplicate-address3">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1bluec)) { echo 'checked';}?> id="check1bluec" name="check1bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluec" id="desc_item1bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1bluec)){ echo $desc_item1bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing2bluectext" name="constract_drawing2bluectext" value="<?= $constract_drawing2bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluec" name="constract_drawing2bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2bluec" value="<?php if(!empty($constract_drawing2bluec)) { echo $date_insert_drawing2bluec;}?>" hidden>
                                          </div>
                                          <p id="files2bluec"><?php if($constract_drawing2bluec!="") echo $constract_drawing2bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluec" name="co_owner2bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2bluec)) {
                                              $selected_array = explode(';',$co_owner2bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2bluec)) { echo 'checked';}?> id="check2bluec" name="check2bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluec" id="desc_item2bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2bluec)){ echo $desc_item2bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing3bluectext" name="constract_drawing3bluectext" value="<?= $constract_drawing3bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluec" name="constract_drawing3bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3bluec" value="<?php if(!empty($constract_drawing3bluec)) { echo $date_insert_drawing3bluec;}?>" hidden>
                                          </div>
                                          <p id="files3bluec"><?php if($constract_drawing3bluec!="") echo $constract_drawing3bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluec" name="co_owner3bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3bluec)) {
                                              $selected_array = explode(';',$co_owner3bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3bluec)) { echo 'checked';}?> id="check3bluec" name="check3bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluec" id="desc_item3bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3bluec)){ echo $desc_item3bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing4bluectext" name="constract_drawing4bluectext" value="<?= $constract_drawing4bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluec" name="constract_drawing4bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4bluec" value="<?php if(!empty($constract_drawing4bluec)) { echo $date_insert_drawing4bluec;}?>" hidden>
                                          </div>
                                          <p id="files4bluec"><?php if($constract_drawing4bluec!="") echo $constract_drawing4bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluec" name="co_owner4bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4bluec)) {
                                              $selected_array = explode(';',$co_owner4bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4bluec)) { echo 'checked';}?> id="check4bluec" name="check4bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluec" id="desc_item4bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4bluec)){ echo $desc_item4bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing5bluectext" name="constract_drawing5bluectext" value="<?= $constract_drawing5bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluec" name="constract_drawing5bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5bluec" value="<?php if(!empty($constract_drawing5bluec)) { echo $date_insert_drawing5bluec;}?>" hidden>
                                          </div>
                                          <p id="files5bluec"><?php if($constract_drawing5bluec!="") echo $constract_drawing5bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluec" name="co_owner5bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5bluec)) {
                                              $selected_array = explode(';',$co_owner5bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5bluec)) { echo 'checked';}?> id="check5bluec" name="check5bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluec" id="desc_item5bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5bluec)){ echo $desc_item5bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing6bluectext" name="constract_drawing6bluectext" value="<?= $constract_drawing6bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluec" name="constract_drawing6bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6bluec" value="<?php if(!empty($constract_drawing6bluec)) { echo $date_insert_drawing6bluec;}?>" hidden>
                                          </div>
                                          <p id="files6bluec"><?php if($constract_drawing6bluec!="") echo $constract_drawing6bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6bluec"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluec" name="co_owner6bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6bluec)) {
                                              $selected_array = explode(';',$co_owner6bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6bluec)) { echo 'checked';}?> id="check6bluec" name="check6bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluec" id="desc_item6bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6bluec)){ echo $desc_item6bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing7bluectext" name="constract_drawing7bluectext" value="<?= $constract_drawing7bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluec" name="constract_drawing7bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7bluec" value="<?php if(!empty($constract_drawing7bluec)) { echo $date_insert_drawing7bluec;}?>" hidden>
                                          </div>
                                          <p id="files7bluec"><?php if($constract_drawing7bluec!="") echo $constract_drawing7bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluec" name="co_owner7bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7bluec)) {
                                              $selected_array = explode(';',$co_owner7bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            } 
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7bluec)) { echo 'checked';}?> id="check7bluec" name="check7bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluec" id="desc_item7bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7bluec)){ echo $desc_item7bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing8bluectext" name="constract_drawing8bluectext" value="<?= $constract_drawing8bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluec" name="constract_drawing8bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8bluec" value="<?php if(!empty($constract_drawing8bluec)) { echo $date_insert_drawing8bluec;}?>" hidden>
                                          </div>
                                          <p id="files8bluec"><?php if($constract_drawing8bluec!="") echo $constract_drawing8bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluec" name="co_owner8bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8bluec)) {
                                              $selected_array = explode(';',$co_owner8bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8bluec)) { echo 'checked';}?> id="check8bluec" name="check8bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluec" id="desc_item8bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8bluec)){ echo $desc_item8bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing9bluectext" name="constract_drawing9bluectext" value="<?= $constract_drawing9bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluec" name="constract_drawing9bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9bluec" value="<?php if(!empty($constract_drawing9bluec)) { echo $date_insert_drawing9bluec;}?>" hidden>
                                          </div>
                                          <p id="files9bluec"><?php if($constract_drawing9bluec!="") echo $constract_drawing9bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluec" name="co_owner9bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9bluec)) {
                                              $selected_array = explode(';',$co_owner9bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9bluec)) { echo 'checked';}?> id="check9bluec" name="check9bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluec" id="desc_item9bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9bluec)){ echo $desc_item9bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10bluec)) : ?> -->
                                            <input type="hidden" id="constract_drawing10bluectext" name="constract_drawing10bluectext" value="<?= $constract_drawing10bluectext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluec" name="constract_drawing10bluec" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10bluec ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10bluec" value="<?php if(!empty($constract_drawing10bluec)) { echo $date_insert_drawing10bluec;}?>" hidden>
                                          </div>
                                          <p id="files10bluec"><?php if($constract_drawing10bluec!="") echo $constract_drawing10bluec; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10bluec"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluec">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluec" name="co_owner10bluec[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10bluec)) {
                                              $selected_array = explode(';',$co_owner10bluec);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10bluec)) { echo 'checked';}?> id="check10bluec" name="check10bluec" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluec">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluec">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluec" id="desc_item10bluec" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10bluec)){ echo $desc_item10bluec; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 4BLUE -->
                            <input type="text" name="category_name1blued" id="category_name1blued" value="施工図面" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingFourBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column4blue" role="button" aria-expanded="false" aria-controls="column4blue">
                                    <div class="leftSide">
                                      <span>4</span>
                                      <p>施工図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1blued == "工事着工資料"){echo "show";} ?>" id="column4blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1blued)) : ?> -->
                                            <input type="hidden" id="constract_drawing1bluedtext" name="constract_drawing1bluedtext" value="<?= $constract_drawing1bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1blued" name="constract_drawing1blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1blued" value="<?php if(!empty($constract_drawing1blued)) { echo $date_insert_drawing1blued;}?>" hidden>
                                          </div>
                                          <p id="files1blued"><?php if($constract_drawing1blued!="") echo $constract_drawing1blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blued" name="co_owner1blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1blued)) {
                                              $selected_array = explode(';',$co_owner1blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address4" name="duplicate-address4">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1blued)) { echo 'checked';}?> id="check1blued" name="check1blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blued" id="desc_item1blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1blued)){ echo $desc_item1blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2blued)) : ?> -->
                                            <input type="hidden" id="constract_drawing2bluedtext" name="constract_drawing2bluedtext" value="<?= $constract_drawing2bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2blued" name="constract_drawing2blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2blued" value="<?php if(!empty($constract_drawing2blued)) { echo $date_insert_drawing2blued;}?>" hidden>
                                          </div>
                                          <p id="files2blued"><?php if($constract_drawing2blued!="") echo $constract_drawing2blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blued" name="co_owner2blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2blued)) {
                                              $selected_array = explode(';',$co_owner2blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2blued)) { echo 'checked';}?> id="check2blued" name="check2blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blued" id="desc_item2blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2blued)){ echo $desc_item2blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3blued)) : ?> -->
                                            <input type="hidden" id="constract_drawing3bluedtext" name="constract_drawing3bluedtext" value="<?= $constract_drawing3bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3blued" name="constract_drawing3blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3blued" value="<?php if(!empty($constract_drawing3blued)) { echo $date_insert_drawing3blued;}?>" hidden>
                                          </div>
                                          <p id="files3blued"><?php if($constract_drawing3blued!="") echo $constract_drawing3blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blued" name="co_owner3blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3blued)) {
                                              $selected_array = explode(';',$co_owner3blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3blued)) { echo 'checked';}?> id="check3blued" name="check3blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blued" id="desc_item3blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3blued)){ echo $desc_item3blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4blued)) : ?> -->
                                            <input type="hidden" id="constract_drawing4bluedtext" name="constract_drawing4bluedtext" value="<?= $constract_drawing4bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4blued" name="constract_drawing4blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4blued" value="<?php if(!empty($constract_drawing4blued) && $category_name == "工事着工資料") { echo $date_insert_drawing4blued;}?>" hidden>
                                          </div>
                                          <p id="files4blued"><?php if($constract_drawing4blued!="" && $category_name == "工事着工資料") echo $constract_drawing4blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blued" name="co_owner4blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4blued)) {
                                              $selected_array = explode(';',$co_owner4blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check4blued" name="check4blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blued" id="desc_item4blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4blued) && $category_name == "工事着工資料"){ echo $desc_item4blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing5bluedtext" name="constract_drawing5bluedtext" value="<?= $constract_drawing5bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5blued" name="constract_drawing5blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5blued" value="<?php if(!empty($constract_drawing5blued) && $category_name == "工事着工資料") { echo $date_insert_drawing5blued;}?>" hidden>
                                          </div>
                                          <p id="files5blued"><?php if($constract_drawing5blued!="" && $category_name == "工事着工資料") echo $constract_drawing5blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blued" name="co_owner5blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5blued)) {
                                              $selected_array = explode(';',$co_owner5blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check5blued" name="check5blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blued" id="desc_item5blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5blued) && $category_name == "工事着工資料"){ echo $desc_item5blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing6bluedtext" name="constract_drawing6bluedtext" value="<?= $constract_drawing6bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6blued" name="constract_drawing6blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6blued" value="<?php if(!empty($constract_drawing6blued) && $category_name == "工事着工資料") { echo $date_insert_drawing6blued;}?>" hidden>
                                          </div>
                                          <p id="files6blued"><?php if($constract_drawing6blued!="" && $category_name == "工事着工資料") echo $constract_drawing6blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6blued"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blued" name="co_owner6blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6blued)) {
                                              $selected_array = explode(';',$co_owner6blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check6blued" name="check6blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blued" id="desc_item6blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6blued) && $category_name == "工事着工資料"){ echo $desc_item6blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing7bluedtext" name="constract_drawing7bluedtext" value="<?= $constract_drawing7bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7blued" name="constract_drawing7blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7blued" value="<?php if(!empty($constract_drawing7blued) && $category_name == "工事着工資料") { echo $date_insert_drawing7blued;}?>" hidden>
                                          </div>
                                          <p id="files7blued"><?php if($constract_drawing7blued!="" && $category_name == "工事着工資料") echo $constract_drawing7blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blued" name="co_owner7blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7blued)) {
                                              $selected_array = explode(';',$co_owner7blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check7blued" name="check7blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blued" id="desc_item7blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7blued) && $category_name == "工事着工資料"){ echo $desc_item7blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing8bluedtext" name="constract_drawing8bluedtext" value="<?= $constract_drawing8bluedtext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8blued" name="constract_drawing8blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8blued" value="<?php if(!empty($constract_drawing8blued) && $category_name == "工事着工資料") { echo $date_insert_drawing8blued;}?>" hidden>
                                          </div>
                                          <p id="files8blued"><?php if($constract_drawing8blued!="" && $category_name == "工事着工資料") echo $constract_drawing8blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blued" name="co_owner8blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8blued)) {
                                              $selected_array = explode(';',$co_owner8blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check8blued" name="check8blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blued" id="desc_item8blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8blued) && $category_name == "工事着工資料"){ echo $desc_item8blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing9bluedtext" name="constract_drawing9bluedtext" value="<?= $constract_drawing9bluedtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9blued" name="constract_drawing9blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9blued" value="<?php if(!empty($constract_drawing9blued) && $category_name == "工事着工資料") { echo $date_insert_drawing9blued;}?>" hidden>
                                          </div>
                                          <p id="files9blued"><?php if($constract_drawing9blued!="" && $category_name == "工事着工資料") echo $constract_drawing9blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blued" name="co_owner9blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9blued)) {
                                              $selected_array = explode(';',$co_owner9blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check9blued" name="check9blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blued" id="desc_item9blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9blued) && $category_name == "工事着工資料"){ echo $desc_item9blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10blued) && $category_name == "工事着工資料") : ?> -->
                                            <input type="hidden" id="constract_drawing10bluedtext" name="constract_drawing10bluedtext" value="<?= $constract_drawing10bluedtext ?>">
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10blued" name="constract_drawing10blued" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10blued ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10blued" value="<?php if(!empty($constract_drawing10blued) && $category_name == "工事着工資料") { echo $date_insert_drawing10blued;}?>" hidden>
                                          </div>
                                          <p id="files10blued"><?php if($constract_drawing10blued!="" && $category_name == "工事着工資料") echo $constract_drawing10blued; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10blued"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blued">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blued" name="co_owner10blued[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10blued)) {
                                              $selected_array = explode(';',$co_owner10blued);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10blued) && $category_name == "工事着工資料") { echo 'checked';}?> id="check10blued" name="check10blued" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blued">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blued">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blued" id="desc_item10blued" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10blued) && $category_name == "工事着工資料"){ echo $desc_item10blued; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 5BLUE -->
                            <input type="text" name="category_name1bluee" id="category_name1bluee" value="変更図面" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingFiveBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column5blue" role="button" aria-expanded="false" aria-controls="column5blue">
                                    <div class="leftSide">
                                      <span>5</span>
                                      <p>変更図面</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1bluee == "変更図面"){echo "show";} ?>" id="column5blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing1blueetext" name="constract_drawing1blueetext" value="<?= $constract_drawing1blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluee" name="constract_drawing1bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1bluee" value="<?php if(!empty($constract_drawing1bluee)) { echo $date_insert_drawing1bluee;}?>" hidden>
                                          </div>
                                          <p id="files1bluee"><?php if($constract_drawing1bluee!="") echo $constract_drawing1bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluee" name="co_owner1bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1bluee)) {
                                              $selected_array = explode(';',$co_owner1bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address5" name="duplicate-address5">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1bluee)) { echo 'checked';}?> id="check1bluee" name="check1bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluee" id="desc_item1bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1bluee)){ echo $desc_item1bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing2blueetext" name="constract_drawing2blueetext" value="<?= $constract_drawing2blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluee" name="constract_drawing2bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2bluee" value="<?php if(!empty($constract_drawing2bluee)) { echo $date_insert_drawing2bluee;}?>" hidden>
                                          </div>
                                          <p id="files2bluee"><?php if($constract_drawing2bluee!="") echo $constract_drawing2bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluee" name="co_owner2bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2bluee)) {
                                              $selected_array = explode(';',$co_owner2bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            } 
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2bluee)) { echo 'checked';}?> id="check2bluee" name="check2bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluee" id="desc_item2bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2bluee)){ echo $desc_item2bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing3blueetext" name="constract_drawing3blueetext" value="<?= $constract_drawing3blueetext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluee" name="constract_drawing3bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3bluee" value="<?php if(!empty($constract_drawing3bluee)) { echo $date_insert_drawing3bluee;}?>" hidden>
                                          </div>
                                          <p id="files3bluee"><?php if($constract_drawing3bluee!="") echo $constract_drawing3bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluee" name="co_owner3bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3bluee)) {
                                              $selected_array = explode(';',$co_owner3bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3bluee)) { echo 'checked';}?> id="check3bluee" name="check3bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluee" id="desc_item3bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3bluee)){ echo $desc_item3bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing4blueetext" name="constract_drawing4blueetext" value="<?= $constract_drawing4blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluee" name="constract_drawing4bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4bluee" value="<?php if(!empty($constract_drawing4bluee)) { echo $date_insert_drawing4bluee;}?>" hidden>
                                          </div>
                                          <p id="files4bluee"><?php if($constract_drawing4bluee!="") echo $constract_drawing4bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluee" name="co_owner4bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4bluee)) {
                                              $selected_array = explode(';',$co_owner4bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4bluee)) { echo 'checked';}?> id="check4bluee" name="check4bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluee" id="desc_item4bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4bluee)){ echo $desc_item4bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing5blueetext" name="constract_drawing5blueetext" value="<?= $constract_drawing5blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluee" name="constract_drawing5bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5bluee" value="<?php if(!empty($constract_drawing5bluee)) { echo $date_insert_drawing5bluee;}?>" hidden>
                                          </div>
                                          <p id="files5bluee"><?php if($constract_drawing5bluee!="") echo $constract_drawing5bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluee" name="co_owner5bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5bluee)) {
                                              $selected_array = explode(';',$co_owner5bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5bluee)) { echo 'checked';}?> id="check5bluee" name="check5bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluee" id="desc_item5bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5bluee)){ echo $desc_item5bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing6blueetext" name="constract_drawing6blueetext" value="<?= $constract_drawing6blueetext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluee" name="constract_drawing6bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6bluee" value="<?php if(!empty($constract_drawing6bluee)) { echo $date_insert_drawing6bluee;}?>" hidden>
                                          </div>
                                          <p id="files6bluee"><?php if($constract_drawing6bluee!="") echo $constract_drawing6bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6bluee"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluee" name="co_owner6bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6bluee)) {
                                              $selected_array = explode(';',$co_owner6bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6bluee)) { echo 'checked';}?> id="check6bluee" name="check6bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluee" id="desc_item6bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6bluee)){ echo $desc_item6bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing7blueetext" name="constract_drawing7blueetext" value="<?= $constract_drawing7blueetext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluee" name="constract_drawing7bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7bluee" value="<?php if(!empty($constract_drawing7bluee)) { echo $date_insert_drawing7bluee;}?>" hidden>
                                          </div>
                                          <p id="files7bluee"><?php if($constract_drawing7bluee!="") echo $constract_drawing7bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluee" name="co_owner7bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7bluee)) {
                                              $selected_array = explode(';',$co_owner7bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7bluee)) { echo 'checked';}?> id="check7bluee" name="check7bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluee" id="desc_item7bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7bluee)){ echo $desc_item7bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing8blueetext" name="constract_drawing8blueetext" value="<?= $constract_drawing8blueetext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluee" name="constract_drawing8bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8bluee" value="<?php if(!empty($constract_drawing8bluee)) { echo $date_insert_drawing8bluee;}?>" hidden>
                                          </div>
                                          <p id="files8bluee"><?php if($constract_drawing8bluee!="") echo $constract_drawing8bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluee" name="co_owner8bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8bluee)) {
                                              $selected_array = explode(';',$co_owner8bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8bluee)) { echo 'checked';}?> id="check8bluee" name="check8bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluee" id="desc_item8bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8bluee)){ echo $desc_item8bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing9blueetext" name="constract_drawing9blueetext" value="<?= $constract_drawing9blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluee" name="constract_drawing9bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawingbluee" value="<?php if(!empty($constract_drawing9bluee)) { echo $date_insert_drawing9bluee;}?>" hidden>
                                          </div>
                                          <p id="files9bluee"><?php if($constract_drawing9bluee!="") echo $constract_drawing9bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluee" name="co_owner9bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9bluee)) {
                                              $selected_array = explode(';',$co_owner9bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9bluee)) { echo 'checked';}?> id="check9bluee" name="check9bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluee" id="desc_item9bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9bluee)){ echo $desc_item9bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10bluee)) : ?> -->
                                            <input type="hidden" id="constract_drawing10blueetext" name="constract_drawing10blueetext" value="<?= $constract_drawing10blueetext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluee" name="constract_drawing10bluee" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10bluee ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10bluee" value="<?php if(!empty($constract_drawing10bluee)) { echo $date_insert_drawing10bluee;}?>" hidden>
                                          </div>
                                          <p id="files10bluee"><?php if($constract_drawing10bluee!="") echo $constract_drawing10bluee; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10bluee"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluee">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluee" name="co_owner10bluee[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10bluee)) {
                                              $selected_array = explode(';',$co_owner10bluee);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10bluee)) { echo 'checked';}?> id="check10bluee" name="check10bluee" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluee">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluee">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluee" id="desc_item10bluee" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item10bluee)){ echo $desc_item10bluee; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 6BLUE -->
                            <input type="text" name="category_name1bluef" id="category_name1bluef" value="設備プランシート" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingSixBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column6blue" role="button" aria-expanded="false" aria-controls="column6blue">
                                    <div class="leftSide">
                                      <span>6</span>
                                      <p>設備プランシート</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1bluef == "設備プランシート"){echo "show";} ?>" id="column6blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing1bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing1blueftext" name="constract_drawing1blueftext" value="<?= $constract_drawing1blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1bluef" name="constract_drawing1bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1bluef" value="<?php if(!empty($constract_drawing1bluef)) { echo $date_insert_drawing1bluef;}?>" hidden>
                                          </div>
                                          <p id="files1bluef"><?php if($constract_drawing1bluef!="") echo $constract_drawing1bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1bluef" name="co_owner1bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1bluef)) {
                                              $selected_array = explode(';',$co_owner1bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address6" name="duplicate-address6">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1bluef)) { echo 'checked';}?> id="check1bluef" name="check1bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1bluef" id="desc_item1bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1bluef)){ echo $desc_item1bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing2blueftext" name="constract_drawing2blueftext" value="<?= $constract_drawing2blueftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2bluef" name="constract_drawing2bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2bluef" value="<?php if(!empty($constract_drawing2bluef)) { echo $date_insert_drawing2bluef;}?>" hidden>
                                          </div>
                                          <p id="files2bluef"><?php if($constract_drawing2bluef!="") echo $constract_drawing2bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2bluef" name="co_owner2bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2bluef)) {
                                              $selected_array = explode(';',$co_owner2bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2bluef)) { echo 'checked';}?> id="check2bluef" name="check2bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2bluef" id="desc_item2bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2bluef)){ echo $desc_item2bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing3blueftext" name="constract_drawing3blueftext" value="<?= $constract_drawing3blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3bluef" name="constract_drawing3bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3bluef" value="<?php if(!empty($constract_drawing3bluef)) { echo $date_insert_drawing3bluef;}?>" hidden>
                                          </div>
                                          <p id="files3bluef"><?php if($constract_drawing3bluef!="") echo $constract_drawing3bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3bluef" name="co_owner3bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3bluef)) {
                                              $selected_array = explode(';',$co_owner3bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3bluef)) { echo 'checked';}?> id="check3bluef" name="check3bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3bluef" id="desc_item3bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3bluef)){ echo $desc_item3bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing4blueftext" name="constract_drawing4blueftext" value="<?= $constract_drawing4blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4bluef" name="constract_drawing4bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4bluef" value="<?php if(!empty($constract_drawing4bluef)) { echo $date_insert_drawing4bluef;}?>" hidden>
                                          </div>
                                          <p id="files4bluef"><?php if($constract_drawing4bluef!="") echo $constract_drawing4bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4bluef" name="co_owner4bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4bluef)) {
                                              $selected_array = explode(';',$co_owner4bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            } 
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4bluef)) { echo 'checked';}?> id="check4bluef" name="check4bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4bluef" id="desc_item4bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4bluef)){ echo $desc_item4bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing5blueftext" name="constract_drawing5blueftext" value="<?= $constract_drawing5blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5bluef" name="constract_drawing5bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5bluef" value="<?php if(!empty($constract_drawing5bluef)) { echo $date_insert_drawing5bluef;}?>" hidden>
                                          </div>
                                          <p id="files5bluef"><?php if($constract_drawing5bluef!="") echo $constract_drawing5bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5bluef" name="co_owner5bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5bluef)) {
                                              $selected_array = explode(';',$co_owner5bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5bluef)) { echo 'checked';}?> id="check5bluef" name="check5bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5bluef" id="desc_item5bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5bluef)){ echo $desc_item5bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing6blueftext" name="constract_drawing6blueftext" value="<?= $constract_drawing6blueftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6bluef" name="constract_drawing6bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6bluef" value="<?php if(!empty($constract_drawing6bluef)) { echo $date_insert_drawing6bluef;}?>" hidden>
                                          </div>
                                          <p id="files6bluef"><?php if($constract_drawing6bluef!="") echo $constract_drawing6bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6bluef"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6bluef" name="co_owner6bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6bluef)) {
                                              $selected_array = explode(';',$co_owner6bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6bluef)) { echo 'checked';}?> id="check6bluef" name="check6bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6bluef" id="desc_item6bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6bluef)){ echo $desc_item6bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing7blueftext" name="constract_drawing7blueftext" value="<?= $constract_drawing7blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7bluef" name="constract_drawing7bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7bluef" value="<?php if(!empty($constract_drawing7bluef)) { echo $date_insert_drawing7bluef;}?>" hidden>
                                          </div>
                                          <p id="files7bluef"><?php if($constract_drawing7bluef!="") echo $constract_drawing7bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7bluef" name="co_owner7bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7bluef)) {
                                              $selected_array = explode(';',$co_owner7bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7bluef)) { echo 'checked';}?> id="check7bluef" name="check7bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7bluef" id="desc_item7bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7bluef)){ echo $desc_item7bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing8blueftext" name="constract_drawing8blueftext" value="<?= $constract_drawing8blueftext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8bluef" name="constract_drawing8bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8bluef" value="<?php if(!empty($constract_drawing8bluef)) { echo $date_insert_drawing8bluef;}?>" hidden>
                                          </div>
                                          <p id="files8bluef"><?php if($constract_drawing8bluef!="") echo $constract_drawing8bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8bluef" name="co_owner8bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8bluef)) {
                                              $selected_array = explode(';',$co_owner8bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8bluef)) { echo 'checked';}?> id="check8bluef" name="check8bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8bluef" id="desc_item8bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8bluef)){ echo $desc_item8bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing9blueftext" name="constract_drawing9blueftext" value="<?= $constract_drawing9blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9bluef" name="constract_drawing9bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9bluef" value="<?php if(!empty($constract_drawing9bluef)) { echo $date_insert_drawing9bluef;}?>" hidden>
                                          </div>
                                          <p id="files9bluef"><?php if($constract_drawing9bluef!="") echo $constract_drawing9bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9bluef" name="co_owner9bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9bluef)) {
                                              $selected_array = explode(';',$co_owner9bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9bluef)) { echo 'checked';}?> id="check9bluef" name="check9bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9bluef" id="desc_item9bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9bluef)){ echo $desc_item9bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10bluef)) : ?> -->
                                            <input type="hidden" id="constract_drawing10blueftext" name="constract_drawing10blueftext" value="<?= $constract_drawing10blueftext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10bluef" name="constract_drawing10bluef" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10bluef ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10bluef" value="<?php if(!empty($constract_drawing10bluef)) { echo $date_insert_drawing10bluef;}?>" hidden>
                                          </div>
                                          <p id="files10bluef"><?php if($constract_drawing10bluef!="") echo $constract_drawing10bluef; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10bluef"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10bluef">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10bluef" name="co_owner10bluef[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10bluef)) {
                                              $selected_array = explode(';',$co_owner10bluef);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10bluef)) { echo 'checked';}?> id="check10bluef" name="check10bluef" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10bluef">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10bluef">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10bluef" id="desc_item10bluef" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9bluef)){ echo $desc_item9bluef; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- CATEGORY 7BLUE -->
                            <input type="text" name="category_name1blueg" id="category_name1blueg" value="その他" hidden>
                            <input type="text" name="type_item" id="type_item" value="user" hidden>
                            <div class="card blueCard">
                              <div class="card-header" id="headingSevenBlue">
                                <h5 class="mb-0">
                                  <a class="btn" data-toggle="collapse" href="#column7blue" role="button" aria-expanded="false" aria-controls="column7blue">
                                    <div class="leftSide">
                                      <span>7</span>
                                      <p>その他</p>
                                    </div>
                                    <div class="rightSide">
                                      <span>共有</span>
                                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </div>
                                  </a>
                                </h5>
                              </div>

                              <div class="collapse <?php if($category_name1blueg == "その他"){echo "show";} ?>" id="column7blue">

                                <div class="card card-body">
                                  <div class="number">1</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                            <!-- <?php //if (!empty($constract_drawing1blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing1bluegtext" name="constract_drawing1bluegtext" value="<?= $constract_drawing1bluegtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing1blueg" name="constract_drawing1blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing1blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing1blueg" value="<?php if(!empty($constract_drawing1blueg)) { echo $date_insert_drawing1blueg;}?>" hidden>
                                          </div>
                                          <p id="files1blueg"><?php if($constract_drawing1blueg!="") echo $constract_drawing1blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading1blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner1blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner1blueg" name="co_owner1blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner1blueg)) {
                                              $selected_array = explode(';',$co_owner1blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <label><input type="checkbox" id="duplicate-address7" name="duplicate-address7">&nbsp;&nbsp;コピー</label>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check1blueg)) { echo "checked";}?> id="check1blueg" name="check1blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check1blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item1blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item1blueg" id="desc_item1blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item1blueg)){ echo $desc_item1blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">2</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing2blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing2bluegtext" name="constract_drawing2bluegtext" value="<?= $constract_drawing2bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing2blueg" name="constract_drawing2blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing2blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing2blueg" value="<?php if(!empty($constract_drawing2blueg)) { echo $date_insert_drawing2blueg;}?>" hidden>
                                          </div>
                                          <p id="files2blueg"><?php if($constract_drawing2blueg!="") echo $constract_drawing2blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading2blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner2blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner2blueg" name="co_owner2blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner2blueg)) {
                                              $selected_array = explode(';',$co_owner2blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check2blueg)) { echo "checked";}?> id="check2blueg" name="check2blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check2blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item2blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item2blueg" id="desc_item2blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item2blueg)){ echo $desc_item2blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">3</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing3blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing3bluegtext" name="constract_drawing3bluegtext" value="<?= $constract_drawing3bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing3blueg" name="constract_drawing3blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing3blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing3blueg" value="<?php if(!empty($constract_drawing3blueg)) { echo $date_insert_drawing3blueg;}?>" hidden>
                                          </div>
                                          <p id="files3blueg"><?php if($constract_drawing3blueg!="") echo $constract_drawing3blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading3blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner3blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner3blueg" name="co_owner3blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner3blueg)) {
                                              $selected_array = explode(';',$co_owner3blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check3blueg)) { echo 'checked';}?> id="check3blueg" name="check3blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check3blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item3blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item3blueg" id="desc_item3blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item3blueg)){ echo $desc_item3blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">4</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing4blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing4bluegtext" name="constract_drawing4bluegtext" value="<?= $constract_drawing4bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing4blueg" name="constract_drawing4blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing4blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing4blueg" value="<?php if(!empty($constract_drawing4blueg)) { echo $date_insert_drawing4blueg;}?>" hidden>
                                          </div>
                                          <p id="files4blueg"><?php if($constract_drawing4blueg!="") echo $constract_drawing4blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading4blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner4blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner4blueg" name="co_owner4blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner4blueg)) {
                                              $selected_array = explode(';',$co_owner4blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check4blueg)) { echo 'checked';}?> id="check4blueg" name="check4blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check4blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item4blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item4blueg" id="desc_item4blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item4blueg)){ echo $desc_item4blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">5</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing5blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing5bluegtext" name="constract_drawing5bluegtext" value="<?= $constract_drawing5bluegtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing5blueg" name="constract_drawing5blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing5blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing5blueg" value="<?php if(!empty($constract_drawing5blueg)) { echo $date_insert_drawing5blueg;}?>" hidden>
                                          </div>
                                          <p id="files5blueg"><?php if($constract_drawing5blueg!="") echo $constract_drawing5blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading5blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner5blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner5blueg" name="co_owner5blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner5blueg)) {
                                              $selected_array = explode(';',$co_owner5blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check5blueg)) { echo 'checked';}?> id="check5blueg" name="check5blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check5blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item5blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item5blueg" id="desc_item5blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item5blueg)){ echo $desc_item5blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">6</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing6blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing6bluegtext" name="constract_drawing6bluegtext" value="<?= $constract_drawing6bluegtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing6blueg" name="constract_drawing6blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing6blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing6blueg" value="<?php if(!empty($constract_drawing6blueg)) { echo $date_insert_drawing6blueg;}?>" hidden>
                                          </div>
                                          <p id="files6blueg"><?php if($constract_drawing6blueg!="") echo $constract_drawing6blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading6blueg"></div>
                                      </div>
                                      <div class="second-input">
                                      <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner6blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner6blueg" name="co_owner6blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner6blueg)) {
                                              $selected_array = explode(';',$co_owner6blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check6blueg)) { echo 'checked';}?> id="check6blueg" name="check6blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check6blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item6blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item6blueg" id="desc_item6blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item6blueg)){ echo $desc_item6blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">7</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing7blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing7bluegtext" name="constract_drawing7bluegtext" value="<?= $constract_drawing7bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing7blueg" name="constract_drawing7blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing7blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing7blueg" value="<?php if(!empty($constract_drawing7blueg)) { echo $date_insert_drawing7blueg;}?>" hidden>
                                          </div>
                                          <p id="files7blueg"><?php if($constract_drawing7blueg!="") echo $constract_drawing7blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading7blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner7blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner7blueg" name="co_owner7blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner7blueg)) {
                                              $selected_array = explode(';',$co_owner7blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check7blueg)) { echo 'checked';}?> id="check7blueg" name="check7blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check7blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item7blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item7blueg" id="desc_item7blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item7blueg)){ echo $desc_item7blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">8</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing8blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing8bluegtext" name="constract_drawing8bluegtext" value="<?= $constract_drawing8bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing8blueg" name="constract_drawing8blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing8blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing8blueg" value="<?php if(!empty($constract_drawing8blueg)) { echo $date_insert_drawing8blueg;}?>" hidden>
                                          </div>
                                          <p id="files8blueg"><?php if($constract_drawing8blueg!="") echo $constract_drawing8blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading8blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner8blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner8blueg" name="co_owner8blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner8blueg)) {
                                              $selected_array = explode(';',$co_owner8blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check8blueg)) { echo 'checked';}?> id="check8blueg" name="check8blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check8blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item8blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item8blueg" id="desc_item8blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item8blueg)){ echo $desc_item8blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">9</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing9blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing9bluegtext" name="constract_drawing9bluegtext" value="<?= $constract_drawing9bluegtext ?>" >
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing9blueg" name="constract_drawing9blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing9blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing9blueg" value="<?php if(!empty($constract_drawing9blueg)) { echo $date_insert_drawing9blueg;}?>" hidden>
                                          </div>
                                          <p id="files9blueg"><?php if($constract_drawing9blueg!="") echo $constract_drawing9blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading9blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner9blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner9blueg" name="co_owner9blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner9blueg)) {
                                              $selected_array = explode(';',$co_owner9blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check9blueg)) { echo 'checked';}?> id="check9blueg" name="check9blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check9blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item9blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item9blueg" id="desc_item9blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9blueg)){ echo $desc_item9blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card card-body">
                                  <div class="number">10</div>
                                  <div class="form-group flex">
                                    <div class="flex-content">
                                      <div class="input-group">
                                          <div class="custom-file">
                                          <!-- <?php //if (!empty($constract_drawing10blueg)) : ?> -->
                                            <input type="hidden" id="constract_drawing10bluegtext" name="constract_drawing10bluegtext" value="<?= $constract_drawing10bluegtext ?>" hidden>
                                            <!-- <?php //endif;?> -->
                                            <input type="file" class="custom-file-input" id="constract_drawing10blueg" name="constract_drawing10blueg" accept=".pdf,.docx,.doc,.ppt,.pptx,.csv,.xls" value="<?= $constract_drawing10blueg ?>">
                                            <label class="custom-file-label" for="pdf2" id="pdf2">ファイルを選択</label>
                                            <input type="text" name="date_insert_drawing10blueg" value="<?php if(!empty($constract_drawing10blueg)) { echo $date_insert_drawing10blueg;}?>" hidden>
                                          </div>
                                          <p id="files10blueg"><?php if($constract_drawing10blueg!="") echo $constract_drawing10blueg; else echo"ファイルが選択されていません。";?></p>
                                          <div id="loading10blueg"></div>
                                      </div>
                                      <div class="second-input">
                                        <div class="dropdown-content">
                                          <label class="subTitle" for="co_owner10blueg">共有業者</label>
                                          <select class="form-control js-example-responsive" id="co_owner10blueg" name="co_owner10blueg[]" multiple style="width:100%;">
                                          <?php
                                            if(!empty($co_owner10blueg)) {
                                              $selected_array = explode(';',$co_owner10blueg);
                                              foreach($data as $list){
                                              $mark_as_select = (in_array($list->name,$selected_array)) ? 'selected' : NULL;
                                                echo '<option value="'.$list->name.'" '.$mark_as_select.'>'.$list->name.'</option>';
                                              }  
                                            }
                                            else
                                            {
                                              foreach($data as $row)
                                              { 
                                                echo '<option value="'.$row->name.'">'.$row->name.'</option>';
                                              }
                                            }
                                          ?>
                                          </select>
                                        </div>
                                        <div class="checkbox-content">
                                          <!-- <h4 class="subTitle">施主様共有</h4>
                                          <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="1" <?php if(!empty($check10blueg)) { echo 'checked';}?> id="check10blueg" name="check10blueg" aria-describedby="helpId" />
                                            <label class="form-check-label" for="check10blueg">共有する</label>
                                          </div> -->
                                        </div>
                                      </div>
                                      <div class="third-input">
                                          <div class="form-group">
                                              <label for="desc_item10blueg">コメント</label>
                                              <input type="text" class="form-control" name="desc_item10blueg" id="desc_item10blueg" aria-describedby="helpId" placeholder="" value="<?php if(!empty($desc_item9blueg)){ echo $desc_item9blueg; }?>">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>


                          </div>
                        </div>

                        <!-- <input name="" id="" class="btn btn-primary" type="submit" value="追加"> -->
                        <br><br>
                        <button name="" id="myBtn" class="btn custom-btn hvr-sink" type="submit">登録する</button>
                        <br><br>
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<!-- <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
<script src="<?= base_url() ?>assets/dashboard/js/select2.min.js"></script>
<script src="<?= base_url() ?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->
<script>

  $(document).ready(function(){
    
    $('#cust_code').autocomplete({
        source: "<?php echo site_url('dashboard/get_autocomplete');?>",

        select: function (event, ui) {
            $('[name="cust_code"]').val(ui.item.label); 
            $('[name="cust_name"]').val(ui.item.description); 
        }
    });

  });

  $("#cust_code").keydown(function(e) {
    var EnterKeyPressed = verifyKeyPressed(e, 13);
    if (EnterKeyPressed === true) {
        //here is where i want to disable the text box
        // var that = this;
        // that.disabled = true;
        
        // $('#cust_name').fadeOut(500, function() {
        //     //i want to re enable it here
        //     that.disabled = false;    
        //   });
        document.getElementById('cust_name').disabled = true; 
        
      }
  });
  

  $('input.form-control.number').keyup(function(event) {
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
          return value
          .replace(/\D/g, "")
          .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      });
  });

  // $('select').js-example-responsive();

  // $(document).ready(function() {
  //     $('.selectpicker').select2();
  // });
  var i = 1;
  for(let i=1;i<10;i++){

    $('#constract_drawing'+i).on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i).innerText = cleanFileName;
    });

  }

  for(let i=1;i<10;i++){

    $('#constract_drawing'+i+'b').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'b').innerText = cleanFileName;
    });

  }

  for(let i=1;i<10;i++){

    $('#constract_drawing'+i+'c').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'c').innerText = cleanFileName;
    });

  }

  for(let i=1;i<10;i++){

    $('#constract_drawing'+i+'d').on('change',function(){
        var fileName = $(this).val();
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
        document.getElementById('files'+i+'d').innerText = cleanFileName;
    });

  }

  for(let i=1;i<10;i++){

  $('#constract_drawing'+i+'e').on('change',function(){
      var fileName = $(this).val();
      var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
      document.getElementById('files'+i+'e').innerText = cleanFileName;
  });

  }

  for(let i=1;i<10;i++){

$('#constract_drawing'+i+'f').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'f').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'g').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'g').innerText = cleanFileName;
});

}


for(let i=1;i<10;i++){

$('#constract_drawing'+i+'bluea').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluea').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'blueb').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blueb').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'bluec').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluec').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'blued').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blued').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'bluee').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluee').innerText = cleanFileName;
});

}


for(let i=1;i<10;i++){

$('#constract_drawing'+i+'bluef').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'bluef').innerText = cleanFileName;
});

}

for(let i=1;i<10;i++){

$('#constract_drawing'+i+'blueg').on('change',function(){
    var fileName = $(this).val();
    var cleanFileName = fileName.replace('C:\\fakepath\\', " ");
    document.getElementById('files'+i+'blueg').innerText = cleanFileName;
});

}

    // $(document).ready(function() {
    //   $(window).keydown(function(event){
    //     if(event.keyCode == 13) {
    //       event.preventDefault();
    //       return false;
    //     }
    //   });
    // });

    $(document).on("keydown", ":input:not(textarea)", function(event) {
    return event.key != "Enter";
});
</script>
<script>
$(document).ready(function() {

var i = 1;
for(let i=1;i<11;i++){
  $('#constract_drawing'+i).on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i, $('#constract_drawing'+i)[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i;
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i).html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i).html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'text').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i).html(result);
          var text = document.getElementById('files'+i).innerHTML;
          document.getElementById('constract_drawing'+i+'text').value = text;
          $('#loading'+i).hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'b').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'b', $('#constract_drawing'+i+'b')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'b';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'b').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'b').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'btext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'b').html(result);
          var text = document.getElementById('files'+i+'b').innerHTML;
          document.getElementById('constract_drawing'+i+'btext').value = text;
          $('#loading'+i+'b').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'c').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'c', $('#constract_drawing'+i+'c')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'c';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'c').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'c').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'ctext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'c').html(result);
          var text = document.getElementById('files'+i+'c').innerHTML;
          document.getElementById('constract_drawing'+i+'ctext').value = text;
          $('#loading'+i+'c').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'d').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'d', $('#constract_drawing'+i+'d')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'d';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'d').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'d').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'dtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'d').html(result);
          var text = document.getElementById('files'+i+'d').innerHTML;
          document.getElementById('constract_drawing'+i+'dtext').value = text;
          $('#loading'+i+'d').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'e').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'e', $('#constract_drawing'+i+'e')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'e';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'e').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'e').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'etext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'e').html(result);
          var text = document.getElementById('files'+i+'e').innerHTML;
          document.getElementById('constract_drawing'+i+'etext').value = text;
          $('#loading'+i+'e').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'f').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'f', $('#constract_drawing'+i+'f')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'f';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'f').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'f').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'ftext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'f').html(result);
          var text = document.getElementById('files'+i+'f').innerHTML;
          document.getElementById('constract_drawing'+i+'ftext').value = text;
          $('#loading'+i+'f').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'g').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'g', $('#constract_drawing'+i+'g')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'g';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'g').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'g').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'gtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'g').html(result);
          var text = document.getElementById('files'+i+'g').innerHTML;
          document.getElementById('constract_drawing'+i+'gtext').value = text;
          $('#loading'+i+'g').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluea').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluea', $('#constract_drawing'+i+'bluea')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluea';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluea').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'bluea').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueatext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'bluea').html(result);
          var text = document.getElementById('files'+i+'bluea').innerHTML;
          document.getElementById('constract_drawing'+i+'blueatext').value = text;
          $('#loading'+i+'bluea').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blueb').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blueb', $('#constract_drawing'+i+'blueb')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueb';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blueb').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'blueb').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluebtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'blueb').html(result);
          var text = document.getElementById('files'+i+'blueb').innerHTML;
          document.getElementById('constract_drawing'+i+'bluebtext').value = text;
          $('#loading'+i+'blueb').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluec').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluec', $('#constract_drawing'+i+'bluec')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluec';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluec').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'bluec').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluectext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'bluec').html(result);
          var text = document.getElementById('files'+i+'bluec').innerHTML;
          document.getElementById('constract_drawing'+i+'bluectext').value = text;
          $('#loading'+i+'bluec').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blued').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blued', $('#constract_drawing'+i+'blued')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blued';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blued').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'blued').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluedtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'blued').html(result);
          var text = document.getElementById('files'+i+'blued').innerHTML;
          document.getElementById('constract_drawing'+i+'bluedtext').value = text;
          $('#loading'+i+'blued').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluee').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluee', $('#constract_drawing'+i+'bluee')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluee';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluee').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'bluee').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueetext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'bluee').html(result);
          var text = document.getElementById('files'+i+'bluee').innerHTML;
          document.getElementById('constract_drawing'+i+'blueetext').value = text;
          $('#loading'+i+'bluee').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'bluef').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'bluef', $('#constract_drawing'+i+'bluef')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'bluef';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'bluef').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'bluef').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'blueftext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'bluef').html(result);
          var text = document.getElementById('files'+i+'bluef').innerHTML;
          document.getElementById('constract_drawing'+i+'blueftext').value = text;
          $('#loading'+i+'bluef').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

for(let i=1;i<11;i++){
  $('#constract_drawing'+i+'blueg').on('change', function() {
    var imageData = new FormData();
    imageData.append('constract_drawing'+i+'blueg', $('#constract_drawing'+i+'blueg')[0].files[0]);
    var ajax_url = '<?php echo base_url();?>dashboard/upload_afiles/constract_drawing'+i+'blueg';
    //Make ajax call here:
    $.ajax({
      url: ajax_url,
      type: 'POST',
      processData: false, // important
      contentType: false, // important
      data: imageData,
      beforeSend: function() {    
            // $('#loading'+i+'blueg').html('<img src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">');
            // document.getElementById("myBtn").disabled = true;
                        $.dialog({
                title: '',
                content: 'アプロード中です。少々お待ちください。<br><img class="loadingImg" src="<?=base_url()?>assets/dashboard/img/loadingAnimation.gif">',
            });
        },
      success: function(result) {
        if (result == 'error') {
          // invalid file format.
          console.error("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          alert("は投稿不可能です。投稿可能なファイル種は : pdf,csv,xls,xlsx,docx,doc,ppt,pptx");
          $('#files'+i+'blueg').html("ファイルが選択されていません。");
          document.getElementById('constract_drawing'+i+'bluegtext').value = "";
          return false;
        } else {
          // console.log(result);
          $('#files'+i+'blueg').html(result);
          var text = document.getElementById('files'+i+'blueg').innerHTML;
          document.getElementById('constract_drawing'+i+'bluegtext').value = text;
          $('#loading'+i+'blueg').hide();
          $('.jconfirm').remove();
          // document.getElementById("myBtn").disabled = false;
        }
      },
      error: function(result) {
        console.error(result);
      }
    });
  });
}

});
</script>
<script>
  
  $('.js-example-responsive').select2({
    width: 'resolve', // need to override the changed default
    placeholder: 'ご選択ください',
    language: { noResults: () => "業者データがありません",},
    escapeMarkup: function (markup) {
        return markup;
    },
    allowClear: true
  });



  
  $("#duplicate-address").change(function() {
    if ($("#duplicate-address:checked").length > 0) {
      bindGroups();
    } else {
      unbindGroups();
    }
  });

  $("#duplicate-address2").change(function() {
    if ($("#duplicate-address2:checked").length > 0) {
      bindGroups2();
    } else {
      unbindGroups2();
    }
  });

  $("#duplicate-address3").change(function() {
    if ($("#duplicate-address3:checked").length > 0) {
      bindGroups3();
    } else {
      unbindGroups3();
    }
  });


  $("#duplicate-address4").change(function() {
    if ($("#duplicate-address4:checked").length > 0) {
      bindGroups4();
    } else {
      unbindGroups4();
    }
  });

  $("#duplicate-address5").change(function() {
    if ($("#duplicate-address5:checked").length > 0) {
      bindGroups5();
    } else {
      unbindGroups5();
    }
  });

  $("#duplicate-address6").change(function() {
    if ($("#duplicate-address6:checked").length > 0) {
      bindGroups6();
    } else {
      unbindGroups6();
    }
  });

  $("#duplicate-address7").change(function() {
    if ($("#duplicate-address7:checked").length > 0) {
      bindGroups7();
    } else {
      unbindGroups7();
    }
  });



  var bindGroups = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluea').on("change.mychange", function() {
          $("#co_owner"+i+"bluea").val($("#co_owner1bluea").val()).trigger("change");
      });
    }
  };

  var unbindGroups = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluea').off("change.mychange");
    // }
  };


  var bindGroups2 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueb').on("change.mychange", function() {
          $("#co_owner"+i+"blueb").val($("#co_owner1blueb").val()).trigger("change");
      });
    }
  };

  var unbindGroups2 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueb').off("change.mychange");
    // }
  };


  var bindGroups3 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluec').on("change.mychange", function() {
          $("#co_owner"+i+"bluec").val($("#co_owner1bluec").val()).trigger("change");
      });
    }
  };

  var unbindGroups3 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluec').off("change.mychange");
    // }
  };


  var bindGroups4 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blued').on("change.mychange", function() {
          $("#co_owner"+i+"blued").val($("#co_owner1blued").val()).trigger("change");
      });
    }
  };

  var unbindGroups4 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blued').off("change.mychange");
    // }
  };

  var bindGroups5 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluee').on("change.mychange", function() {
          $("#co_owner"+i+"bluee").val($("#co_owner1bluee").val()).trigger("change");
      });
    }
  };

  var unbindGroups5 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluee').off("change.mychange");
    // }
  };

  var bindGroups6 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");

      // Then bind fields
      $('#co_owner1bluef').on("change.mychange", function() {
          $("#co_owner"+i+"bluef").val($("#co_owner1bluef").val()).trigger("change");
      });
    }
  };

  var unbindGroups6 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1bluef').off("change.mychange");
    // }
  };


  var bindGroups7 = function() {
    var i = 2;
    for(let i=2;i<11;i++){
      // First copy values
      $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");

      // Then bind fields
      $('#co_owner1blueg').on("change.mychange", function() {
          $("#co_owner"+i+"blueg").val($("#co_owner1blueg").val()).trigger("change");
      });
    }
  };

  var unbindGroups7 = function() {
    // var i = 2;
    // for(let i=2;i<11;i++){
      $('#co_owner1blueg').off("change.mychange");
    // }
  };

  var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("contract_date").setAttribute("max", "9999-12-31");
document.getElementById("construction_start_date").setAttribute("max", "9999-12-31");
document.getElementById("upper_building_date").setAttribute("max", "9999-12-31");
document.getElementById("completion_date").setAttribute("max", "9999-12-31");
document.getElementById("delivery_date").setAttribute("max", "9999-12-31");

$("#sales_staff1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#sales_staff1").show();
        $("#sales_staff2").hide();
    } else {
        $("#sales_staff1").hide();
        $("#sales_staff2").show();
    }
});

$("#incharge_construction1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_construction1").show();
        $("#incharge_construction2").hide();
    } else {
        $("#incharge_construction1").hide();
        $("#incharge_construction2").show();
    }
});

$("#incharge_coordinator1").hide();
$(".coupon_question3").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_coordinator1").show();
        $("#incharge_coordinator2").hide();
    } else {
        $("#incharge_coordinator1").hide();
        $("#incharge_coordinator2").show();
    }
});
</script>
</body>
</html>