<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規OB顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規OB顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
          <?php endif;?>
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_customers" method="post" enctype="multipart/form-data" onsubmit="return validateForm();">
                        <input type="text" name="gid" value="<?=$for_id->gid+1?>" hidden>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="latitude">緯度</label>
                              <input type="text" class="form-control" name="latitude" id="latitude" aria-describedby="helpId" placeholder="" value="<?php if(!empty($b)) echo $b; else echo '';?>" >
                              <span id="lblLat" style="color: Red;display:none;"></span>
                            </div>
                            <div class="col-md-6">
                              <label for="longitude">経度</label>
                              <input type="text" class="form-control" name="longitude" id="longitude" aria-describedby="helpId" placeholder="" value="<?php if(!empty($l)) echo $l; else echo '';?>">
                              <span id="lblLong" style="color: Red;display:none;"></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="cust_code">顧客コード <span class="red">※必須</span></label>
                              <input type="text" class="form-control" name="cust_code" id="cust_code" aria-describedby="helpId" placeholder="" value="<?php if(!empty($cust_code)) echo $cust_code; else echo '';?>" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name">氏名 (漢字) <span class="red">※必須</span></label>
                              <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($氏名)) echo $氏名; else echo '';?>" required oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_kana">氏名 (かな)</label>
                              <input type="text" class="form-control" name="name_kana" id="name_kana" aria-describedby="helpId" placeholder="" value="<?php if(!empty($氏名_かな)) echo $氏名_かな; else echo '';?>" oninvalid="this.setCustomValidity('入力してください')" oninput="setCustomValidity('')">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="joint_name">連名</label>
                          <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($連名)) echo $連名; else echo '';?>">
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-4" style="margin-bottom:15px;">
                              <label for="zip_code">〒</label>
                              <input type="text" class="form-control" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="" value="<?php if(!empty($〒)) echo $〒; else echo '';?>" onkeydown="return event.keyCode !== 69" onkeyup="AjaxZip3.zip2addr(this,'','prefecture','address');">
                            </div>
                            <div class="col-md-6">
                              <label for="prefecture">都道府県</label>
                              <input type="text" class="form-control" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder="" value="<?php if(!empty($都道府県)) echo $都道府県; else echo '';?>">
                            </div>
                            <div class="col-md-12">
                              <label for="address">住所</label>
                              <input type="text" class="form-control" name="address" id="address" aria-describedby="helpId" placeholder="" value="<?php if(!empty($住　　　所)) echo $住　　　所; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="building_name">建物名・部屋番号</label>
                          <input type="text" class="form-control col-md-6" name="building_name" id="building_name" aria-describedby="helpId" placeholder="" value="<?php if(!empty($共同住宅)) echo $共同住宅; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="phone_number">電話番号</label>
                          <input type="text" class="form-control col-md-6" name="phone_number" id="phone_number" aria-describedby="helpId" placeholder="" value="<?php if(!empty($電　　話)) echo $電　　話; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="birthday">生年月日</label>
                          <div class="">
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap1" name="date_jap" value="1">昭和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap2" name="date_jap" value="2">平成　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap3" name="date_jap" value="3">令和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap4" name="date_jap" value="4" checked>西暦　
                          </div>
                          <div class="dateinput">
                            <input type="text" class="form-control col-md-1" name="year" id="year"
                              aria-describedby="helpId" placeholder="" maxlength="9999" value="<?php if(!empty($year)) echo $year; else echo '';?>" autocomplete="off" onkeyup='canShowText()'><label>年　</label>
                            <input type="text" class="form-control col-md-1" name="month" id="month"
                            aria-describedby="helpId" placeholder="" maxlength="12" value="<?php if(!empty($month)) echo $month; else echo '';?>" autocomplete="off" onkeyup='canShowText()'><label>月　</label>
                            <input type="text" class="form-control col-md-1" name="date" id="date"
                            aria-describedby="helpId" placeholder="" maxlength="31" value="<?php if(!empty($date)) echo $date; else echo '';?>" autocomplete="off" onkeyup='canShowText()'><label>日　</label>
                          </div>
                          
                          <input type="hidden" class="form-control col-md-6" name="birthday" id="birthday"
                            aria-describedby="helpId" placeholder="" value="<?php if(!empty($生年月日)) echo $生年月日; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="age">年齢</label>
                          <input type="text" class="form-control col-md-1" name="age" id="age"
                            aria-describedby="helpId" placeholder="" value="<?php if(!empty($age)) echo $age; else echo '';?>">
                        </div>
                        <div class="form-group dropdowns">
                          <label for="kinds">種別</label>
                          <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control" name="kinds">
                            <option selected disabled value="">選択してください</option>
                            <option <?php if($種別 == "SOB") echo 'selected';?> value="SOB">SWEET HOME</option>
                            <option <?php if($種別 == "ビリーブの家") echo 'selected';?> value="ビリーブの家">ビリーブの家</option>
                            <option <?php if($種別 == "リフォーム工事") echo 'selected';?> value="リフォーム工事">リフォーム工事</option>
                            <option <?php if($種別 == "メンテナンス工事") echo 'selected';?> value="メンテナンス工事">メンテナンス工事</option>
                            <option <?php if($種別 == "新築工事") echo 'selected';?> value="新築工事">新築工事</option>
                            <option <?php if($種別 == "OB") echo 'selected';?> value="OB">OB</option>
                            <option <?php if($種別 == "その他") echo 'selected';?> value="その他">その他</option>
                            <option <?php if($種別 == "削除") echo 'selected';?> value="削除">削除</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="first_contact">初回接点</label>
                          <input type="text" class="form-control col-md-6" name="first_contact" id="first_contact" aria-describedby="helpId" placeholder="" value="<?php if(!empty($初回接点)) echo $初回接点; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="calendar">初回年月日</label>
                          <input type="date" class="form-control col-md-6" name="calendar" id="calendar" aria-describedby="helpId" placeholder="" value="<?php if(!empty($初回年月日)) echo $初回年月日; else echo '';?>">
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="text" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="" value="<?php if(!empty($メールアドレス)) echo $メールアドレス; else echo '';?>">
                        </div>
                        <div class="form-group dropdowns">
                          <label for="sales_staff">営業担当</label>
                          <input type="text" class="form-control col-md-12" name="sales_staff1" id="sales_staff1" aria-describedby="helpId" placeholder="">
                          <select class="form-control" name="sales_staff" id="sales_staff2">
                            <option value="">選択してください</option>
                            <?php if(!empty($営業担当)) :?>
                              <option selected value="<?=$営業担当?>"><?=$営業担当?></option>
                            <?php
                            endif; 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                          <input class="coupon_question" type="checkbox" name="coupon_question" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="pic_construction">工務担当</label>
                          <input type="text" class="form-control col-md-12" name="pic_construction1" id="pic_construction1" aria-describedby="helpId" placeholder="">
                          <select class="form-control" name="pic_construction" id="pic_construction2">
                            <option value="">選択してください</option>
                            <?php if(!empty($工務担当)) :?>
                              <option selected value="<?=$工務担当?>"><?=$工務担当?></option>
                            <?php
                            endif; 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                          <input class="coupon_question2" type="checkbox" name="coupon_question2" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="incharge_coordinator">コーデ担当</label>
                          <input type="text" class="form-control col-md-12" name="incharge_coordinator1" id="incharge_coordinator1" aria-describedby="helpId" placeholder="">
                          <select class="form-control" name="incharge_coordinator" id="incharge_coordinator2">
                            <option value="">選択してください</option>
                            <?php if(!empty($コーデ担当)) :?>
                              <option selected value="<?=$コーデ担当?>"><?=$コーデ担当?></option>
                            <?php
                            endif; 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                          <input class="coupon_question3" type="checkbox" name="coupon_question3" value="1" />
                          <span class="item-text">手入力に切り替えます</span>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="marks">備考</label>
                          <textarea type="text" class="form-control col-md-12" name="marks" id="marks" aria-describedby="helpId" placeholder="" rows="5"><?php if(!empty($備考)) echo $備考; else echo '';?></textarea>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_wife">奥様名前</label>
                              <input type="text" class="form-control" name="name_wife" id="name_wife" aria-describedby="helpId" placeholder="" value="<?php if(!empty($奥様名前)) echo $奥様名前; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_wife_kana">奥様名前 (かな)</label>
                              <input type="text" class="form-control" name="name_wife_kana" id="name_wife_kana" aria-describedby="helpId" placeholder="" value="<?php if(!empty($奥様名前_かな)) echo $奥様名前_かな; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="date_birth_wife">奥様生年月日</label>
                          <div class="">
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap5" name="date_jap2" value="1">昭和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap6" name="date_jap2" value="2">平成　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap7" name="date_jap2" value="3">令和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap8" name="date_jap2" value="4" checked>西暦　
                          </div>
                          <div class="dateinput">
                            <input type="text" class="form-control col-md-1" name="year" id="year2"
                              aria-describedby="helpId" placeholder="" maxlength="9999" value="<?php if(!empty($year)) echo $year; else echo '';?>" autocomplete="off" onkeyup='canShowText2()'><label>年　</label>
                            <input type="text" class="form-control col-md-1" name="month" id="month2"
                            aria-describedby="helpId" placeholder="" maxlength="12" value="<?php if(!empty($month)) echo $month; else echo '';?>" autocomplete="off" onkeyup='canShowText2()'><label>月　</label>
                            <input type="text" class="form-control col-md-1" name="date" id="date2"
                            aria-describedby="helpId" placeholder="" maxlength="31" value="<?php if(!empty($date)) echo $date; else echo '';?>" autocomplete="off" onkeyup='canShowText2()'><label>日　</label>
                          </div>

                          <input type="hidden" class="form-control col-md-6" name="date_birth_wife" id="date_birth_wife" aria-describedby="helpId" placeholder="" value="<?php if(!empty($奥様生年月日)) echo $奥様生年月日; else echo '';?>">
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child1">お子様1名前</label>
                              <input type="text" class="form-control" name="name_child1" id="name_child1" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様1名前)) echo $お子様1名前; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child1_kana">お子様1名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child1_kana" id="name_child1_kana" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様1名前_かな)) echo $お子様1名前_かな; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child1">お子様1生年月日</label>
                          <div class="">
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap9" name="date_jap3" value="1">昭和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap10" name="date_jap3" value="2">平成　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap11" name="date_jap3" value="3">令和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap12" name="date_jap3" value="4" checked>西暦　
                          </div>
                          <div class="dateinput">
                            <input type="text" class="form-control col-md-1" name="year" id="year3"
                              aria-describedby="helpId" placeholder="" maxlength="9999" value="<?php if(!empty($year)) echo $year; else echo '';?>" autocomplete="off" onkeyup='canShowText3()'><label>年　</label>
                            <input type="text" class="form-control col-md-1" name="month" id="month3"
                            aria-describedby="helpId" placeholder="" maxlength="12" value="<?php if(!empty($month)) echo $month; else echo '';?>" autocomplete="off" onkeyup='canShowText3()'><label>月　</label>
                            <input type="text" class="form-control col-md-1" name="date" id="date3"
                            aria-describedby="helpId" placeholder="" maxlength="31" value="<?php if(!empty($date)) echo $date; else echo '';?>" autocomplete="off" onkeyup='canShowText3()'><label>日　</label>
                          </div>
                          <input type="hidden" class="form-control col-md-6" name="birth_child1" id="birth_child1" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様1生年月日)) echo $お子様1生年月日; else echo '';?>">
                        </div>
                        
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child2">お子様2名前</label>
                              <input type="text" class="form-control" name="name_child2" id="name_child2" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様2名前)) echo $お子様2名前; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child2_kana">お子様2名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child2_kana" id="name_child2_kana" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様2名前_かな)) echo $お子様2名前_かな; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child2">お子様2生年月日</label>
                          <div class="">
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap13" name="date_jap4" value="1">昭和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap14" name="date_jap4" value="2">平成　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap15" name="date_jap4" value="3">令和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap16" name="date_jap4" value="4" checked>西暦　
                          </div>
                          <div class="dateinput">
                            <input type="text" class="form-control col-md-1" name="year" id="year4"
                              aria-describedby="helpId" placeholder="" maxlength="9999" value="<?php if(!empty($year)) echo $year; else echo '';?>" autocomplete="off" onkeyup='canShowText4()'><label>年　</label>
                            <input type="text" class="form-control col-md-1" name="month" id="month4"
                            aria-describedby="helpId" placeholder="" maxlength="12" value="<?php if(!empty($month)) echo $month; else echo '';?>" autocomplete="off" onkeyup='canShowText4()'><label>月　</label>
                            <input type="text" class="form-control col-md-1" name="date" id="date4"
                            aria-describedby="helpId" placeholder="" maxlength="31" value="<?php if(!empty($date)) echo $date; else echo '';?>" autocomplete="off" onkeyup='canShowText4()'><label>日　</label>
                          </div>
                          <input type="hidden" class="form-control col-md-6" name="birth_child2" id="birth_child2" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様2生年月日)) echo $お子様2生年月日; else echo '';?>">
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child3">お子様3名前</label>
                              <input type="text" class="form-control" name="name_child3" id="name_child3" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様3名前)) echo $お子様3名前; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child3_kana">お子様3名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child3_kana" id="name_child3_kana" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様3名前_かな)) echo $お子様3名前_かな; else echo '';?>">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child3">お子様3生年月日</label>
                          <div class="">
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap17" name="date_jap5" value="1">昭和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap18" name="date_jap5" value="2">平成　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap19" name="date_jap5" value="3">令和　
                            <input type="checkbox" onclick="selectOnlyThis(this.id)" id="date_jap20" name="date_jap5" value="4" checked>西暦　
                          </div>
                          <div class="dateinput">
                            <input type="text" class="form-control col-md-1" name="year" id="year5"
                              aria-describedby="helpId" placeholder="" maxlength="9999" value="<?php if(!empty($year)) echo $year; else echo '';?>" autocomplete="off" onkeyup='canShowText5()'><label>年　</label>
                            <input type="text" class="form-control col-md-1" name="month" id="month5"
                            aria-describedby="helpId" placeholder="" maxlength="12" value="<?php if(!empty($month)) echo $month; else echo '';?>" autocomplete="off" onkeyup='canShowText5()'><label>月　</label>
                            <input type="text" class="form-control col-md-1" name="date" id="date5"
                            aria-describedby="helpId" placeholder="" maxlength="31" value="<?php if(!empty($date)) echo $date; else echo '';?>" autocomplete="off" onkeyup='canShowText5()'><label>日　</label>
                          </div>
                          <input type="hidden" class="form-control col-md-6" name="birth_child3" id="birth_child3" aria-describedby="helpId" placeholder="" value="<?php if(!empty($お子様3生年月日)) echo $お子様3生年月日; else echo '';?>">
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="longterm1" name="longterm" value="1" <?php if(!empty($長期優良住宅)) echo 'checked';?> >
                                <label for="longterm">長期優良住宅</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="warranty1" name="warranty" value="1" <?php if(!empty($設備10年保証加入)) echo 'checked';?> >
                                <label for="warranty">設備10年保証加入</label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="delivery_date">引渡日</label>
                          <input type="date" class="form-control col-md-6" name="delivery_date" id="input_delivery_date" aria-describedby="helpId" placeholder="" onkeyup="handler(event);" value="<?php if(!empty($引渡日)) echo $引渡日; else echo '';?>">
                        </div>

                        <div class="form-group">
                          <label for="inspection_time1">定期点検項目（3ヶ月）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time1" id="inspection_time1" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_3ヶ月)) echo $定期点検項目_3ヶ月; else echo '';?>" readonly>
                        </div>
                        
                        <div class="form-group">
                          <label for="inspection_time2">定期点検項目（6ヶ月）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time2" id="inspection_time2" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_6ヶ月)) echo $定期点検項目_6ヶ月; else echo '';?>" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time3">定期点検項目（1年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time3" id="inspection_time3" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_1年)) echo $定期点検項目_1年; else echo '';?>" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time4">定期点検項目（2年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time4" id="inspection_time4" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_2年)) echo $定期点検項目_2年; else echo '';?>" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time5">定期点検項目（3年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time5" id="inspection_time5" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_3年)) echo $定期点検項目_3年; else echo '';?>" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time6">定期点検項目（5年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time6" id="inspection_time6" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_5年)) echo $定期点検項目_5年; else echo '';?>" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time7">定期点検項目（10年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time7" id="inspection_time7" aria-describedby="helpId" placeholder="" value="<?php if(!empty($定期点検項目_10年)) echo $定期点検項目_10年; else echo '';?>" readonly>
                        </div>

                        <div class="form-group dropdowns">
                          <label for="bank">借入金融機関</label>
                          <!-- <input type="text" class="form-control col-md-6" name="bank" id="bank" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control" name="bank">
                            <option value="">選択してください</option>
                            <?php if(!empty($借入金融機関)) :?>
                              <option selected value="<?=$借入金融機関?>"><?=$借入金融機関?></option>
                            <?php
                            endif; 
                            if(!empty($for_bank))
                            {
                              foreach($for_bank as $row)
                              { 
                                echo '<option value="'.$row->金融機関名.'">'.$row->金融機関名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="insurance_company">火災保険</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company1" name="insurance_company" value="自社"  <?php if($火災保険 == '自社') echo 'checked';?> >
                                <label for="insurance_company1" class="form-check-label">自社</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company2" name="insurance_company" value="他社"  <?php if($火災保険 == '他社') echo 'checked';?> >
                                <label for="insurance_company2" class="form-check-label">他社</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="insurance">火災保険備考</label>
                          <input type="text" class="form-control col-md-6" name="insurance" id="insurance" aria-describedby="helpId" placeholder="" value="<?php if(!empty($火災保険備考)) echo $火災保険備考; else echo '';?>">
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="subsidy1" name="subsidy" value="1" <?php if(!empty($グリーン化事業補助金)) echo 'checked';?> >
                                <label for="subsidy">グリーン化事業補助金</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="ground_improve1" name="ground_improve" value="1" <?php if(!empty($地盤改良工事)) echo 'checked';?> >
                                <label for="ground_improve">地盤改良工事</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="thanksgiving">感謝祭</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="thanksgiving1" name="thanksgiving" value="〇" <?php if($感謝祭 == '〇') echo 'checked';?> >
                                <label for="thanksgiving1" class="form-check-label">〇</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="thanksgiving2" name="thanksgiving" value="✕" <?php if($感謝祭 == '✕') echo 'checked';?> >
                                <label for="thanksgiving2" class="form-check-label">✕</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="dm">その他DM</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="dm1" name="dm" value="〇" <?php if($イベントDM == '〇') echo 'checked';?> >
                                <label for="dm1" class="form-check-label">〇</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="dm2" name="dm" value="✕" <?php if($イベントDM == '✕') echo 'checked';?> >
                                <label for="dm2" class="form-check-label">✕</label>
                            </div>
                          </div>
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
//   $('#birth_date').change(function(){
//   console.log("change");
//   var dob = new Date(document.getElementById('birth_date').value);
//   var today = new Date();
//   var age = Math.floor((today-dob)/(365.25*24*60*60*1000));
//   document.getElementById('age').value = age;
// });

function selectOnlyThis(id) {
    for (var i = 1;i <= 4; i++)
    {
        document.getElementById("Check" + i).checked = false;
    }
    document.getElementById(id).checked = true;
}

    function handler(e){
      var input = document.getElementById('input_delivery_date').value;
      var input2 = document.getElementById('input_delivery_date').value;
      var input3 = document.getElementById('input_delivery_date').value;
      var input4 = document.getElementById('input_delivery_date').value;
      var input5 = document.getElementById('input_delivery_date').value;
      var input6 = document.getElementById('input_delivery_date').value;
      var input7 = document.getElementById('input_delivery_date').value;

      var manDate = moment(input).local();
      var manDate2 = moment(input2).local();
      var manDate3 = moment(input3).local();
      var manDate4 = moment(input4).local();
      var manDate5 = moment(input5).local();
      var manDate6 = moment(input6).local();
      var manDate7 = moment(input7).local();

      document.getElementById('inspection_time1').value = manDate.add(3, 'M').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time2').value = manDate2.add(6, 'M').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time3').value = manDate3.add(1, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time4').value = manDate4.add(2, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time5').value = manDate5.add(3, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time6').value = manDate6.add(5, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time7').value = manDate7.add(10, 'y').format('YYYY-MM-DD'); 
    }


//     $('#latitude').keyup(function(){
//     var val = $(this).val();
//     if(isNaN(val)){
//          val = val.replace(/[^0-9\.]/g,'');
//          if(val.split('.').length>2) 
//              val =val.replace(/\.+$/,"");
//     }
//     $(this).val(val); 
// });

// $('#longitude').keyup(function(){
//     var val = $(this).val();
//     if(isNaN(val)){
//          val = val.replace(/[^0-9\.]/g,'');
//          if(val.split('.').length>2) 
//              val =val.replace(/\.+$/,"");
//     }
//     $(this).val(val); 
// });
// function ValidateLatitude() {
//     $("#lblLat").hide();
//     var regexLat = new RegExp('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}');
//     if (!regexLat.test($("#latitude").val())) {
//         $("#lblLat").html("緯度の形式が違います").show();
//     }
//     else
//     {
//       $("#lblLat").html("緯度の形式が違います").hide();
//     }
// }

// function ValidateLongitude() {
//     $("#lblLong").hide();
//     var regexLong = new RegExp('^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$');
//     if (!regexLong.test($("#longitude").val())) {
//         $("#lblLong").html("経度の形式が違います").show();
//     }
//     else
//     {
//       $("#lblLong").html("経度の形式が違います").hide();
//     }
// }

// validateForm = function () {
//     return checkName();
// }

// function checkName() {
//   var latitude = document.getElementById('latitude').value;
//   var longitude = document.getElementById('longitude').value;

//   var regLat = new RegExp("^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}");
//   var regLang = new RegExp("^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$");
  
//   if( regLat.exec(latitude) ) 
//     {
//     //do nothing
//     } 
//     else 
//     {
//       alert('緯度の形式が違います');
//       return false;
//     }

//     if( regLang.exec(longitude) ) 
//     {
//     //do nothing
//     } else {
//       alert('経度の形式が違います');
//       return false;
//     }

// }

// $('#birthday').change(function(){
//   console.log("change");
//   var dob = new Date(document.getElementById('birthday').value);
//   var today = new Date();
//   var age = Math.floor((today-dob)/(365.25*24*60*60*1000));
//   document.getElementById('age').value = age;
// });


function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function canShowText()
{ 
  var year = document.getElementById('year').value;
  var month = document.getElementById('month').value;
  var date = document.getElementById('date').value;
  var selector = document.querySelector('input[name="date_jap"]:checked');

  if($('#year').val().length>0 && $('#month').val().length>0 && $('#date').val().length>0) 
  { 
    if(selector.value == '3') {
        Lokyer = parseInt(year) + 2019 +1;
      }
      else if(selector.value == '2') {
        Lokyer = parseInt(year) + 1988;
      }
      else if(selector.value == '1') {
        Lokyer = parseInt(year) + 1925;
      }
      else {
        Lokyer = parseInt(year);
      }

      var combine = Lokyer+'-'+month+'-'+date;
      var age = getAge(combine);
      document.getElementById('age').value = age;
      document.getElementById('birthday').value = combine;

      console.log(combine);
  } 
  else 
  {

  }
} 


function canShowText2()
{ 
  var year2 = document.getElementById('year2').value;
  var month2 = document.getElementById('month2').value;
  var date2 = document.getElementById('date2').value;
  var selector2 = document.querySelector('input[name="date_jap2"]:checked');

  if($('#year2').val().length>0 && $('#month2').val().length>0 && $('#date2').val().length>0) 
  { 
    if(selector2.value == '3') {
        Lokyer2 = parseInt(year2) + 2019 +1;
      }
      else if(selector2.value == '2') {
        Lokyer2 = parseInt(year2) + 1988;
      }
      else if(selector2.value == '1') {
        Lokyer2 = parseInt(year2) + 1925;
      }
      else {
        Lokyer2 = parseInt(year2);
      }

      var combine2 = Lokyer2+'-'+month2+'-'+date2;
      document.getElementById('date_birth_wife').value = combine2;

      console.log(combine2);
  } 
  else 
  {

  }
} 

function canShowText3()
{ 
  var year3 = document.getElementById('year3').value;
  var month3 = document.getElementById('month3').value;
  var date3 = document.getElementById('date3').value;
  var selector3 = document.querySelector('input[name="date_jap3"]:checked');

  if($('#year3').val().length>0 && $('#month3').val().length>0 && $('#date3').val().length>0) 
  { 
    if(selector3.value == '3') {
        Lokyer3 = parseInt(year3) + 2019 +1;
      }
      else if(selector3.value == '2') {
        Lokyer3 = parseInt(year3) + 1988;
      }
      else if(selector3.value == '1') {
        Lokyer3 = parseInt(year3) + 1925;
      }
      else {
        Lokyer3 = parseInt(year3);
      }

      var combine3 = Lokyer3+'-'+month3+'-'+date3;
      document.getElementById('birth_child1').value = combine3;

      console.log(combine3);
  } 
  else 
  {

  }
} 


function canShowText4()
{ 
  var year4 = document.getElementById('year4').value;
  var month4 = document.getElementById('month4').value;
  var date4 = document.getElementById('date4').value;
  var selector4 = document.querySelector('input[name="date_jap4"]:checked');

  if($('#year4').val().length>0 && $('#month4').val().length>0 && $('#date4').val().length>0) 
  { 
    if(selector4.value == '3') {
        Lokyer4 = parseInt(year4) + 2019 +1;
      }
      else if(selector4.value == '2') {
        Lokyer4 = parseInt(year4) + 1988;
      }
      else if(selector4.value == '1') {
        Lokyer4 = parseInt(year4) + 1925;
      }
      else {
        Lokyer4 = parseInt(year4);
      }

      var combine4 = Lokyer4+'-'+month4+'-'+date4;
      document.getElementById('birth_child2').value = combine4;

      console.log(combine4);
  } 
  else 
  {

  }
} 

function canShowText5()
{ 
  var year5 = document.getElementById('year5').value;
  var month5 = document.getElementById('month5').value;
  var date5 = document.getElementById('date5').value;
  var selector5 = document.querySelector('input[name="date_jap5"]:checked');

  if($('#year5').val().length>0 && $('#month5').val().length>0 && $('#date5').val().length>0) 
  { 
    if(selector5.value == '3') {
        Lokyer5 = parseInt(year5) + 2019 +1;
      }
      else if(selector5.value == '2') {
        Lokyer5 = parseInt(year5) + 1988;
      }
      else if(selector5.value == '1') {
        Lokyer5 = parseInt(year5) + 1925;
      }
      else {
        Lokyer5 = parseInt(year5);
      }

      var combine5 = Lokyer5+'-'+month5+'-'+date5;
      document.getElementById('birth_child3').value = combine5;

      console.log(combine5);
  } 
  else 
  {

  }
} 


$('input[name="date_jap"]').on('click', function() {
   if ($(this).val() === '') {

   }
   else {
    document.getElementById('year').value ='';
   }
});

$('input[name="date_jap2"]').on('click', function() {
   if ($(this).val() === '') {

   }
   else {
    document.getElementById('year2').value ='';
   }
});

$('input[name="date_jap3"]').on('click', function() {
   if ($(this).val() === '') {

   }
   else {
    document.getElementById('year3').value ='';
   }
});

$('input[name="date_jap4"]').on('click', function() {
   if ($(this).val() === '') {

   }
   else {
    document.getElementById('year4').value ='';
   }
});

$('input[name="date_jap5"]').on('click', function() {
   if ($(this).val() === '') {

   }
   else {
    document.getElementById('year5').value ='';
   }
});


var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
} 
    
today = yyyy + '-' + mm + '-' + dd;
document.getElementById("birthday").setAttribute("max", "9999-12-31");
document.getElementById("calendar").setAttribute("max", "9999-12-31");
document.getElementById("date_birth_wife").setAttribute("max", "9999-12-31");
document.getElementById("birth_child1").setAttribute("max", "9999-12-31");
document.getElementById("birth_child2").setAttribute("max", "9999-12-31");
document.getElementById("birth_child3").setAttribute("max", "9999-12-31");
document.getElementById("input_delivery_date").setAttribute("max", "9999-12-31");

$("#sales_staff1").hide();
$(".coupon_question").click(function() {
    if($(this).is(":checked")) {
        $("#sales_staff1").show();
        $("#sales_staff2").hide();
    } else {
        $("#sales_staff1").hide();
        $("#sales_staff2").show();
    }
});

$("#pic_construction1").hide();
$(".coupon_question2").click(function() {
    if($(this).is(":checked")) {
        $("#pic_construction1").show();
        $("#pic_construction2").hide();
    } else {
        $("#pic_construction1").hide();
        $("#pic_construction2").show();
    }
});

$("#incharge_coordinator1").hide();
$(".coupon_question3").click(function() {
    if($(this).is(":checked")) {
        $("#incharge_coordinator1").show();
        $("#incharge_coordinator2").hide();
    } else {
        $("#incharge_coordinator1").hide();
        $("#incharge_coordinator2").show();
    }
});
</script>
</body>
</html>