<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">OB顧客詳細情報</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">管理ページ / OB顧客詳細情報</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content view_custom">
    <div class="container-fluid">
    <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 mx-auto">
          <div class="card">
            <div class="card-body">

              <div class="button" style="margin-bottom:20px;">
                <a class="btn btn-info" href="<?=base_url()?>dashboard/customers_list"
                  role="button"><i class="fa fa-arrow-left"> 戻る</i></a>
                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_customers/<?=$data->gid?>"
                  role="button"><i class="fa fa-edit"></i></a>
                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_customers/<?=$data->gid?>"
                  role="button"><i class="fa fa-trash"></i></a>
              </div>

              <div class="col-md-12" style="padding:0;">
                <div class="form-row">
                  <div class="col-md-6" style="margin-bottom:15px;">
                    <div class="flex-box double">
                      <div class="label-view">
                        <label for="latitude">緯度</label>
                      </div>
                      <div class="text-view">
                        <p>
                          <?=$data->b;?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="flex-box double">
                      <div class="label-view">
                        <label for="longitude">経度</label>
                      </div>
                      <div class="text-view">
                        <p>
                          <?=$data->l;?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="cust_code">顧客コード</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->cust_code?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">氏名 (漢字)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->氏名?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">氏名 (かな)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->氏名_かな?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">連名</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->連名?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="address_1">住所</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <span>〒
                        <?=$data->〒;?>
                      </span>
                      &nbsp;&nbsp;
                      <span>
                        <?=$data->都道府県?>
                      </span>
                      <span>
                        <?=$data->住　　　所;?>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="phone_number1">建物名・部屋番号</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->共同住宅;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="phone_number2">電話番号</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->電　　話;?>
                    </p>
                  </div>
                </div>
              </div>

              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="joint_name">生年月日</label>
                      </div>
                      <?php
                                        
                          date_default_timezone_set('Asia/Tokyo');    

                          $formatter = new IntlDateFormatter('ja_JP@calendar=japanese', IntlDateFormatter::MEDIUM,
                          IntlDateFormatter::MEDIUM, 'Asia/Tokyo', IntlDateFormatter::TRADITIONAL);

                          $he = $data->生年月日;
                          $now = new DateTime($he);
                          $str = substr($formatter->format($now), 0, strrpos($formatter->format($now), ' '));
                      

                      ?>
                      <div class="text-view">
                          <p><?=$data->生年月日?> (年齢 : <?=$data->age?>)</p>
                      </div>

                  </div>
              </div>


              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="cell_phone">種別</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?php if($data->種別 == 'SOB'){ echo 'SWEET HOME';} else { echo $data->種別;}?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="email">初回接点</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->初回接点;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="birthday">初回年月日</label>
                  </div>
                  <?php
  
                      $he2 = $data->初回年月日;
                      $now2 = new DateTime($he2);
                      $str2 = substr($formatter->format($now2), 0, strrpos($formatter->format($now2), ' '));
                  
                  ?>
                  <div class="text-view">
                    <p>
                      <?=$data->初回年月日?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="email">メールアドレス</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->メールアドレス;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="fb_account">営業担当</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->営業担当;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="twitter_account">工務担当</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->工務担当;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="twitter_account">コーデ担当</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->コーデ担当;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="line_account">備考</label>
                  </div>
                  <div class="text-view">
                    <p>
                      <?=$data->備考;?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">奥様名前</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->奥様名前?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">奥様名前 (かな)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->奥様名前_かな?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">奥様生年月日</label>
                      </div>
                      <?php

                          $he3 = $data->奥様生年月日;
                          $now3 = new DateTime($he3);
                          $str3 = substr($formatter->format($now3), 0, strrpos($formatter->format($now3), ' '));
                      
                      ?>
                      <div class="text-view">
                          <p>
                              <?=$data->奥様生年月日?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様1名前</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様1名前?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様1名前 (かな)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様1名前_かな?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様1生年月日</label>
                      </div>
                      <?php

                      $he4 = $data->お子様1生年月日;
                      $now4 = new DateTime($he4);
                      $str4 = substr($formatter->format($now4), 0, strrpos($formatter->format($now4), ' '));

                      ?>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様1生年月日?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様2名前</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様2名前?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様2名前 (かな)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様2名前_かな?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様2生年月日</label>
                      </div>
                      <?php

                      $he5 = $data->お子様2生年月日;
                      $now5 = new DateTime($he5);
                      $str5 = substr($formatter->format($now5), 0, strrpos($formatter->format($now5), ' '));

                      ?>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様2生年月日?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様3名前</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様3名前?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様3名前 (かな)</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様3名前_かな?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">お子様3生年月日</label>
                      </div>
                      <?php

                      $he6 = $data->お子様3生年月日;
                      $now6 = new DateTime($he6);
                      $str6 = substr($formatter->format($now6), 0, strrpos($formatter->format($now6), ' '));

                      ?>
                      <div class="text-view">
                          <p>
                              <?=$data->お子様3生年月日?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">長期優良住宅</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?php if(!empty($data->長期優良住宅)){echo "✓";}?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">設備10年保証加入</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?php if(!empty($data->設備10年保証加入)){echo "✓";}?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">引渡日</label>
                      </div>
                      <?php

                      $he7 = $data->引渡日;
                      $now7 = new DateTime($he7);
                      $he8 = $data->定期点検項目_3ヶ月;
                      $now8 = new DateTime($he8);
                      $he9 = $data->定期点検項目_6ヶ月;
                      $now9 = new DateTime($he9);
                      $he10 = $data->定期点検項目_1年;
                      $now10 = new DateTime($he10);
                      $he11 = $data->定期点検項目_2年;
                      $now11 = new DateTime($he11);
                      $he12 = $data->定期点検項目_3年;
                      $now12 = new DateTime($he12);
                      $he13 = $data->定期点検項目_5年;
                      $now13 = new DateTime($he13);
                      $he14 = $data->定期点検項目_10年;
                      $now14 = new DateTime($he14);


                      $str7 = substr($formatter->format($now7), 0, strrpos($formatter->format($now7), ' '));
                      $str8 = substr($formatter->format($now8), 0, strrpos($formatter->format($now8), ' '));
                      $str9 = substr($formatter->format($now9), 0, strrpos($formatter->format($now9), ' '));
                      $str10 = substr($formatter->format($now10), 0, strrpos($formatter->format($now10), ' '));
                      $str11 = substr($formatter->format($now11), 0, strrpos($formatter->format($now11), ' '));
                      $str12 = substr($formatter->format($now12), 0, strrpos($formatter->format($now12), ' '));
                      $str13 = substr($formatter->format($now13), 0, strrpos($formatter->format($now13), ' '));
                      $str14 = substr($formatter->format($now14), 0, strrpos($formatter->format($now14), ' '));


                      ?>
                      <div class="text-view">
                          <p>
                              <?=$data->引渡日?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（3ヶ月）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_3ヶ月?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（6ヶ月）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_6ヶ月?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（1年）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_1年?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（2年）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_2年?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（3年）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_3年?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（5年）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_5年?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">定期点検項目（10年）</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->定期点検項目_10年?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">借入金融機関</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->借入金融機関?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">火災保険</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->火災保険?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">火災保険備考</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->火災保険備考?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">グリーン化事業補助金</label>
                      </div>
                      <div class="text-view">
                          <p>
                            <?php if(!empty($data->グリーン化事業補助金)){echo "✓";}?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">地盤改良工事</label>
                      </div>
                      <div class="text-view">
                          <p>
                            <?php if(!empty($data->地盤改良工事)){echo "✓";}?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">感謝祭</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->感謝祭?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="full_name">その他DM</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data->イベントDM?>
                          </p>
                      </div>
                  </div>
              </div>
              <?php if(!empty($data_const->kinds)) :?>
              <p>-------------------------------------------</p>
              <label>工事情報</label>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="dm">種別</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data_const->kinds?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="rank">工事No</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data_const->const_no?>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="flex-box">
                      <div class="label-view">
                          <label for="remarks">引渡日</label>
                      </div>
                      <div class="text-view">
                          <p>
                              <?=$data_const->delivery_date?>
                          </p>
                      </div>
                  </div>
              </div>
              <p><a href="<?=base_url()?>dashboard/view_construction/<?=$data_const->construct_id ?>" >工事情報ページへ</a></p>
              <?php endif; ?>

              <div class="button">
                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_customers/<?=$data->gid?>"
                  role="button"><i class="fa fa-edit"></i></a>
                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_customers/<?=$data->gid?>"
                  role="button"><i class="fa fa-trash"></i></a>
                  <!-- <a class="btn btn-info" href="<?=base_url()?>map/map2/<?=$data->b.','.$data->l?>"
                  role="button"><i class="fa fa-search"></i></a> -->
              </div>

            </div>
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
    $('.delete').confirm({
    title:'',
    content: "顧客情報を削除します、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
</body>

</html>