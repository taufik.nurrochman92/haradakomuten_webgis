<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit OB顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit OB顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_customers/<?=$data->gid;?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <label for="customer_code">顧客コード</label>
                          <input type="text" class="form-control col-md-6" name="customer_code" id="customer_code" aria-describedby="helpId" placeholder="" value="<?=$data->customer_code;?>">
                        </div>
                        <div class="form-group">
                          <label for="last_name">氏名 (姓)</label>
                          <input type="text" class="form-control col-md-6" name="last_name" id="last_name" aria-describedby="helpId" placeholder="" value="<?=$data->sei;?>">
                        </div>
                        <div class="form-group">
                          <label for="first_name">氏名 (名)</label>
                          <input type="text" class="form-control col-md-6" name="first_name" id="first_name" aria-describedby="helpId" placeholder="" value="<?=$data->名;?>"> 
                        </div>
                        <div class="form-group">
                          <label for="last_furigana">氏名 (せい)</label>
                          <input type="text" class="form-control col-md-6" name="last_furigana" id="last_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->last_furigana;?>">
                        </div>
                        <div class="form-group">
                          <label for="first_furigana">氏名 (めい)</label>
                          <input type="text" class="form-control col-md-6" name="first_furigana" id="first_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->first_furigana;?>">
                        </div>
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" aria-describedby="helpId" placeholder="" value="<?=$data->b;?>">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" aria-describedby="helpId" placeholder="" value="<?=$data->l;?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code1">郵便番号1</label>
                          <input type="text" class="form-control col-md-6" name="zip_code1" id="zip_code1" aria-describedby="helpId" placeholder="" value="<?=$data->zip_code1;?>">
                        </div>
                        <div class="form-group">
                          <label for="address_1">住所1</label>
                          <input type="text" class="form-control col-md-6" name="address_1" id="address_1" aria-describedby="helpId" placeholder="" value="<?=$data->住所;?>">
                        </div>
                        <div class="form-group">
                          <label for="address_line1">住所1 番地</label>
                          <input type="text" class="form-control col-md-6" name="address_line1" id="address_line1" aria-describedby="helpId" placeholder="" value="<?=$data->address_line1;?>">
                        </div>
                        <div class="form-group">
                          <label for="phone_number1">電話番号1</label>
                          <input type="text" class="form-control col-md-6" name="phone_number1" id="phone_number1" aria-describedby="helpId" placeholder="" value="<?=$data->電話番号;?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code2">郵便番号2</label>
                          <input type="text" class="form-control col-md-6" name="zip_code2" id="zip_code2" aria-describedby="helpId" placeholder="" value="<?=$data->zip_code2;?>">
                        </div>
                        <div class="form-group">
                          <label for="address_line2">住所2</label>
                          <input type="text" class="form-control col-md-6" name="address_line2" id="address_line2" aria-describedby="helpId" placeholder="" value="<?=$data->address_line2;?>">
                        </div>
                        <div class="form-group">
                          <label for="address_2">住所2 番地</label>
                          <input type="text" class="form-control col-md-6" name="address_2" id="address_2" aria-describedby="helpId" placeholder="" value="<?=$data->住所_2;?>">
                        </div>
                        <div class="form-group">
                          <label for="phone_number2">電話番号2</label>
                          <input type="text" class="form-control col-md-6" name="phone_number2" id="phone_number2" aria-describedby="helpId" placeholder="" value="<?=$data->電話番号_2;?>">
                        </div>
                        <div class="form-group">
                          <label for="cell_phone">携帯電話</label>
                          <input type="text" class="form-control col-md-6" name="cell_phone" id="cell_phone" aria-describedby="helpId" placeholder="" value="<?=$data->cell_phone;?>">
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="text" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="" value="<?=$data->メールアドレス;?>">
                        </div>
                        <div class="form-group">
                          <label for="birthday">生年月日</label>
                          <input type="date" class="form-control col-md-6" name="birthday" id="birthday" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->誕生日)) { echo isset($data->誕生日) ? set_value('birthday', date('Y-m-d', strtotime($data->誕生日))) : set_value('birthday'); };?>">
                        </div>
                        <div class="form-group">
                          <label for="fb_account">FBアカウント</label>
                          <input type="text" class="form-control col-md-6" name="fb_account" id="fb_account" aria-describedby="helpId" placeholder="" value="<?=$data->fb_account;?>">
                          <p style="font-size: 12px;color: red;">※半角英数字。</p>
                        </div>
                        <div class="form-group">
                          <label for="twitter_account">Twitterアカウント</label>
                          <input type="text" class="form-control col-md-6" name="twitter_account" id="twitter_account" aria-describedby="helpId" placeholder="" value="<?=$data->twitter_account;?>">
                          <p style="font-size: 12px;color: red;">※半角英数字。</p>
                        </div>
                        <div class="form-group">
                          <label for="line_account">LINEアカウント</label>
                          <input type="text" class="form-control col-md-6" name="line_account" id="line_account" aria-describedby="helpId" placeholder="" value="<?=$data->line_account;?>">
                          <p style="font-size: 12px;color: red;">※半角英数字。</p>
                        </div>
                        <div class="form-group">
                          <label for="instagram_account">Instagramアカウント</label>
                          <input type="text" class="form-control col-md-6" name="instagram_account" id="instagram_account" aria-describedby="helpId" placeholder="" value="<?=$data->instagram_account;?>">
                          <p style="font-size: 12px;color: red;">※(@)抜きで入力ください。</p>
                        </div>
                        <div class="form-group">
                          <label for="thanksgiving">感謝祭</label>
                          <input type="date" class="form-control col-md-6" name="thanksgiving" id="thanksgiving" aria-describedby="helpId" placeholder="" value="<?=$data->thanksgiving;?>">
                        </div>
                        <div class="form-group">
                          <label for="dm">DM</label>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="dm1" name="dm" value="〇" <?php if($data->dm == "〇") { echo "checked";}?>>
                                <label for="dm1" class="form-check-label">〇</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="dm2" name="dm" value="△" <?php if($data->dm == "△") { echo "checked";}?>>
                                <label for="dm2" class="form-check-label">△</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="dm3" name="dm" value="X" <?php if($data->dm == "X") { echo "checked";}?>>
                                <label for="dm3" class="form-check-label">X</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="introducer">紹介者</label>
                          <input type="text" class="form-control col-md-6" name="introducer" id="introducer" aria-describedby="helpId" placeholder="" value="<?=$data->introducer;?>">
                        </div>
                        <!-- <div class="form-group">
                          <label for="mid_and_year_end_gift">お中元・お歳暮</label>
                          <input type="text" class="form-control col-md-6" name="mid_and_year_end_gift" id="mid_and_year_end_gift" aria-describedby="helpId" placeholder="" value="<?=$data->mid_and_year_end_gift;?>">
                        </div> -->
                        <div class="form-group">
                          <label for="mid_and_year_end_gift">お中元・お歳暮</label>
                          <!-- <input type="checkbox" class="form-control col-md-6" name="mid_and_year_end_gift" id="mid_and_year_end_gift" aria-describedby="helpId" placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="mid_and_year_end_gift1" name="mid_and_year_end_gift[]" value="お中元" <?php if (strpos($data->mid_and_year_end_gift, 'お中元') !== false) { echo "checked"; } ?>>
                                <label for="mid_and_year_end_gift1" class="form-check-label">お中元</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="mid_and_year_end_gift2" name="mid_and_year_end_gift[]" value="お歳暮" <?php if (strpos($data->mid_and_year_end_gift, 'お歳暮') !== false) { echo "checked"; } ?>>
                                <label for="mid_and_year_end_gift2" class="form-check-label">お歳暮</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="contract_date">契約日</label>
                          <input type="date" class="form-control col-md-6" name="contract_date" id="contract_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->完了検査)) { echo isset($data->完了検査) ? set_value('contract_date', date('Y-m-d', strtotime($data->完了検査))) : set_value('contract_date'); }; ?>">
                        </div>
                        <div class="form-group">
                          <label for="delivery_date">引渡日</label>
                          <input type="date" class="form-control col-md-6" name="delivery_date" id="delivery_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->引渡)) { echo isset($data->引渡) ? set_value('delivery_date', date('Y-m-d', strtotime($data->引渡))) : set_value('delivery_date'); }; ?>">
                        </div>
                        <div class="form-group">
                          <label for="construction_start_date">工期開始日</label>
                          <input type="date" class="form-control col-md-6" name="construction_start_date" id="construction_start_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->construction_start_date)) { echo isset($data->construction_start_date) ? set_value('construction_start_date', date('Y-m-d', strtotime($data->construction_start_date))) : set_value('construction_start_date'); }; ?>">
                        </div>
                        <div class="form-group">
                          <label for="construction_end_date">工期終了日</label>
                          <input type="date" class="form-control col-md-6" name="construction_end_date" id="construction_end_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->construction_end_date)) { echo isset($data->construction_end_date) ? set_value('construction_end_date', date('Y-m-d', strtotime($data->construction_end_date))) : set_value('construction_end_date'); };?>">
                        </div>
                        <div class="form-group">
                          <label for="blog_url">ブログカテゴリURL</label>
                          <input type="text" class="form-control col-md-6" name="blog_url" id="blog_url" aria-describedby="helpId" placeholder="" value="<?=$data->blog_url;?>">
                        </div>

                        <div class="form-group">
                          <label for="contract">契約書</label>
                          <?php
                            if ($data->contract != "") :
                          ?>
                                <div class="col-md-3 col-sm-3" style="margin-top:10px;margin-bottom:10px;">
                                    <img src="<?= base_url() ?>uploads/customer/<?= $data->contract ?>" class="img-fluid" id="contract">
                                    <br />
                                </div>
                            <?php else : ?>

                          <?php
                            endif; 
                          ?>
                          <input type="file" class="form-control col-md-6" name="contract" id="contract" aria-describedby="helpId" placeholder="" value="<?=$data->contract;?>">
                        </div>
                        <div class="form-group">
                          <label for="drawing">図面</label>
                          <?php
                            if ($data->drawing != "") :
                          ?>
                                <div class="col-md-3 col-sm-3" style="margin-top:10px;margin-bottom:10px;">
                                    <img src="<?= base_url() ?>uploads/customer/<?= $data->drawing ?>" class="img-fluid" id="drawing">
                                    <br />
                                </div>
                            <?php else : ?>

                          <?php
                            endif; 
                          ?>
                          <input type="file" class="form-control col-md-6" name="drawing" id="drawing" aria-describedby="helpId" placeholder="" value="<?=$data->drawing;?>">
                        </div>
                        <div class="form-group">
                          <label for="photo">写真</label>
                          <?php
                            if ($data->photo != "") :
                          ?>
                                <div class="col-md-3 col-sm-3" style="margin-top:10px;margin-bottom:10px;">
                                    <img src="<?= base_url() ?>uploads/customer/<?= $data->photo ?>" class="img-fluid" id="photo">
                                    <br />
                                </div>
                            <?php else : ?>

                          <?php
                            endif; 
                          ?>
                          <input type="file" class="form-control col-md-6" name="photo" id="photo" aria-describedby="helpId" placeholder="" value="<?=$data->photo;?>">
                        </div>

                        <div class="form-group dropdowns">
                          <label for="sales_staff">営業担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="sales_staff" id="sales_staff" aria-describedby="helpId" placeholder="" value="<?=$data->sales_staff;?>"> -->
                          <select class="form-control" name="sales_staff">
                            <option <?php if ($data->sales_staff == "") {echo "selected"; } ?> value="">ご選択ください</option>
                            <option <?php if ($data->sales_staff == "$data->sales_staff") {echo "selected"; } ?> value="<?=$data->sales_staff?>"><?=$data->sales_staff?></option>
                            <?php 
                            if(!empty($dataks))
                            {
                              foreach($dataks as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="pic_construction">工務担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="pic_construction" id="pic_construction" aria-describedby="helpId" placeholder="" value="<?=$data->pic_construction;?>"> -->
                          <select class="form-control" name="pic_construction">
                            <option <?php if ($data->pic_construction == "") {echo "selected"; } ?> value="">ご選択ください</option>
                            <option <?php if ($data->pic_construction == "$data->pic_construction") {echo "selected"; } ?> value="<?=$data->pic_construction?>"><?=$data->pic_construction?></option>
                            <?php 
                            if(!empty($dataks))
                            {
                              foreach($dataks as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="coordinator">コーディネート担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="coordinator" id="coordinator" aria-describedby="helpId" placeholder="" value="<?=$data->coordinator;?>"> -->
                          <select class="form-control" name="coordinator">
                            <option <?php if ($data->coordinator == "") {echo "selected"; } ?> value="">ご選択ください</option>
                            <option <?php if ($data->coordinator == "$data->coordinator") {echo "selected"; } ?> value="<?=$data->coordinator?>"><?=$data->coordinator?></option>
                            <?php 
                            if(!empty($dataks))
                            {
                              foreach($dataks as $row)
                              { 
                                echo '<option value="'.$row->姓.' '.$row->名.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="kinds">種別</label>
                          <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder="" value="<?=$data->kinds;?>"> -->
                          <select class="form-control" name="kinds">
                            <option <?php if ($data->種別 == "") {echo "selected"; } ?> disabled value="">ご選択ください</option>
                            <option <?php if ($data->種別 == "sweet") {echo "selected"; } ?> value="sweet">Sweet Home</option>
                            <option <?php if ($data->種別 == "believe") {echo "selected"; } ?> value="believe">ビリーブの家</option>
                            <option <?php if ($data->種別 == "reform") {echo "selected"; } ?> value="reform">小工事</option>
                            <option <?php if ($data->種別 == "建設中") {echo "selected"; } ?> value="建設中">建設中</option>
                            <option <?php if ($data->種別 == "OB") {echo "selected"; } ?> value="OB">OB</option>
                            <option <?php if ($data->種別 == "材料販売") {echo "selected"; } ?> value="材料販売">材料販売</option>
                            <option <?php if ($data->種別 == "その他") {echo "selected"; } ?> value="その他">その他</option>
                            <option <?php if ($data->種別 == "NL") {echo "selected"; } ?> value="NL">NL</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <!-- <label for="ten_year_warranty">10年保証</label>
                          <input type="text" class="form-control col-md-6" name="ten_year_warranty" id="ten_year_warranty" aria-describedby="helpId" placeholder="" value="<?=$data->ten_year_warranty;?>"> -->
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="ten_year_warranty" name="ten_year_warranty" value="10年保証" <?php if ($data->ten_year_warranty == "10年保証") {echo "checked"; } ?>>
                              <label for="ten_year_warranty" >10年保証</label>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- <label for="thermo_hygrometer">温湿度計</label>
                          <input type="text" class="form-control col-md-6" name="thermo_hygrometer" id="thermo_hygrometer" aria-describedby="helpId" placeholder="" value="<?=$data->thermo_hygrometer;?>"> -->
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="thermo_hygrometer" name="thermo_hygrometer" value="温湿度計" <?php if ($data->thermo_hygrometer == "温湿度計") {echo "checked"; } ?>>
                              <label for="thermo_hygrometer" >温湿度計</label>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- <label for="solar_power">太陽光発電</label>
                          <input type="text" class="form-control col-md-6" name="solar_power" id="solar_power" aria-describedby="helpId" placeholder="" value="<?=$data->solar_power;?>"> -->
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="solar_power" name="solar_power" value="太陽光発電" <?php if ($data->solar_power == "太陽光発電") {echo "checked"; } ?>>
                              <label for="solar_power" >太陽光発電</label>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- <label for="long_term">長期優良住宅</label>
                          <input type="text" class="form-control col-md-6" name="long_term" id="long_term" aria-describedby="helpId" placeholder="" value="<?=$data->long_term;?>"> -->
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" id="long_term" name="long_term" value="長期優良住宅" <?php if ($data->long_term == "長期優良住宅") {echo "checked"; } ?>>
                              <label for="long_term" >長期優良住宅</label>
                          </div>
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>