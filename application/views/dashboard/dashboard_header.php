<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>原田工務店 webGIS | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/datepicker.css">
  <!-- datatable -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" /> -->
  <!-- CUSTOM -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/custom.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/custom-accordion.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/bootstrap-datepicker.min.css">
  <script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.css"></script>
  <style>
    .select2-selection__choice__display {
      color: #000;
    }

    .select2-container .select2-search, 
    .select2-container .select2-search__field {
        width: 100% !important;
    }

    .bootstrap-select .dropdown-toggle .filter-option {
      border: 1px solid #ced4da;
    }

    .btn.hvr-sink {
      color: #ffffff;
      background-color: #64a727;
      border-color: #64a727;
      box-shadow: none;
    }

    .date_insert {
      padding-left: 30px;
      font-weight: bold;
      color: #888;
    }

    #btn-example-file-reset1, #btn-example-file-reset2, #btn-example-file-reset3, #btn-example-file-reset4,
    #btn-example-file-reset5, #btn-example-file-reset6, #btn-example-file-reset7, #btn-example-file-reset8,
    #btn-example-file-reset9, #btn-example-file-reset10 {
      position: relative;
      right: -80px;
      margin: 5px;
    }

    #btn-example-file-replace1, #btn-example-file-replace2, #btn-example-file-replace3, #btn-example-file-replace4,
    #btn-example-file-replace5, #btn-example-file-replace6, #btn-example-file-replace7, #btn-example-file-replace8,
    #btn-example-file-replace9, #btn-example-file-replace10 {
      position: relative;
      right: -80px;
    }

    .dt-button {
      color: #ffffff;
      background-color: #64a727;
      border-color: #64a727;
      box-shadow: none;
    }
    
    .ui-autocomplete {
        position: absolute;
        z-index: 2;
        cursor: default;
        padding: 0;
        margin-top: 10px;
        list-style: none;
        background-color: #ffffff;
        border: 1px solid #ccc
        -webkit-border-radius: 5px;
          -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
          -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    }
    .ui-autocomplete > li {
      padding: 3px 20px;
    }
    .ui-autocomplete > li.ui-state-focus {
      background-color: #DDD;
    }
    .ui-helper-hidden-accessible {
      display: none;
    }

    .err p {
      color: red;
      font-weight: bold;
      font-size: 13px;
      padding-top: 5px;
    }

    .red {
      color: red;
      font-size: 12px;
    }

    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type="number"] {
        -moz-appearance: textfield;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button {
      padding: 0 !important;
    }

    .dateinput {
      display: flex;
      justify-content: flex-start;
      align-items: end;
    }

    .jconfirm-closeIcon {
      display: none !important;
    }

    .jconfirm.jconfirm-white .jconfirm-bg, .jconfirm.jconfirm-light .jconfirm-bg {
      opacity: 0.8 !important;
    }

    .jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content {
      text-align: center;
    }

    .jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content .loadingImg {
      max-width: 100%;
      display: block;
      margin: 15px auto 0;
      display: block;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed" onload="StartTimers();" onmousemove="ResetTimers();">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
        <li>
            <a name="" id="" class="btn btn-primary" href="<?=base_url()?>dashboard/logout" role="button">ログアウト</a>
        </li>  
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=base_url()?>dashboard" class="brand-link">
      <!-- <img src="<?=base_url()?>assets/dashboard/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <img src="<?=base_url()?>assets/dashboard/img/logo.png" width="100%">
      <!-- <span class="brand-text font-weight-light">AdminLTE 3</span> -->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-user-circle"></i>
              <p style="border-bottom: #f9edda 3px solid;">
                営業中顧客
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/client_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>営業中顧客一覧 <?php if(!empty($selector_client)){ echo $selector_client;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_client" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規営業中顧客 <?php if(!empty($selector_add_client)){ echo $selector_add_client;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-user"></i>
              <p style="border-bottom: #f9edda 3px solid;">
                OB顧客
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/customers_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>OB顧客一覧 <?php if(!empty($selector_customers)){ echo $selector_customers;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_customers" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規OB顧客 <?php if(!empty($selector_add_customers)){ echo $selector_add_customers;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <?php if($_SESSION["access"] == 'super_admin') : ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-users"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              社員マスター
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/employees_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>社員一覧 <?php if(!empty($selector_employees)){ echo $selector_employees;}?></p>
                </a>
              </li>
              <?php if($_SESSION["access"] == 'super_admin') : ?>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_employees" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規社員追加 <?php if(!empty($selector_add_employees)){ echo $selector_add_employees;}?></p>
                </a>
              </li>
              <?php endif;?>
            </ul>
          </li>
          <?php endif; ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-building"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              業者管理
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/contractor_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>業者一覧 <?php if(!empty($selector_contractor)){ echo $selector_contractor;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_contractor" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規業者追加 <?php if(!empty($selector_add_contractor)){ echo $selector_add_contractor;}?></p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="<?=base_url()?>dashboard/list_user" class="nav-link">
                  <i class="fa fa-list nav-icon"></i>
                  <p>ID・パスワード一覧 <?php if(!empty($selector_list_affiliate)){ echo $selector_list_affiliate;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_user" class="nav-link">
                  <i class="fa fa-user-plus nav-icon"></i>
                  <p>ID・パスワード管理 <?php if(!empty($selector_add_affiliate)){ echo $selector_add_affiliate;}?></p>
                </a>
              </li> -->
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-wrench"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              工事情報
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/constructor_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>工事一覧 <?php if(!empty($selector_construction)){ echo $selector_construction;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_construction" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>工事登録 <?php if(!empty($selector_add_construction)){ echo $selector_add_construction;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/search_construction" class="nav-link">
                  <i class="fa fa-search nav-icon"></i>
                  <p>工事検索 <?php if(!empty($selector_search_construction)){ echo $selector_search_construction;}?></p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>