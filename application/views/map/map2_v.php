<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>地図上で顧客検索 | 原田工務店</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/common.css">
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-light" style="background-color: #e3f2fd;">
        <a class="navbar-brand" href="<?=base_url()?>"><img style="height: 49px;" src="<?=base_url()?>assets/dashboard/img/harada_logo_white.png"></a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation"></button>
    </nav>
<!-- leaflet map -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
 
<style>
#map { height: 540px; }
</style>
<div class="container">
    <div>
        <center>
        <h1>地図上で顧客検索</h1>
        </center>
    </div>
    <div class="map-wrapper">
        <div id="map" style="margin-top:2%"></div>
        <div class="map-category">
            <div class="_top">
                <a href="<?=base_url()?>map/map2/?category=sweet">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-1.png" alt="">
                </a>
                <a href="<?=base_url()?>map/map2/?category=believe">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-2.png" alt="">
                </a>
                <a href="<?=base_url()?>map/map2/?category=reform">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/cat-3.png" alt="">
                </a>
            </div>

            <div class="_bottom">
                <p>担 当</p>

                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/user1.png" alt="">
                    <span>大井</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/user2.png" alt="">
                    <span>菅沼</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/user3.png" alt="">
                    <span>山下</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/user4.png" alt="">
                    <span>松元</span>
                </a>
            </div>
        </div>
    </div>
    <br>
</div>
<script>
    $( document ).ready( function() {
        var BL = "<?=$latlong?>";

        var LATLON = BL.split(',');
        if(LATLON.length != 2) return;
        var wb = LATLON[0];
        var wl = LATLON[1];
        var wbl = new L.LatLng(wb, wl);

        map.setView(wbl, 17);
        // var MarkerOption = L.icon({
        //         iconUrl: 'markers/pin/gmap_cross.gif',
        //         iconSize:     [32, 32]
        // });

        // addMarker = L.marker(wbl).addTo(map);
    });
</script>
<script>
    var map = L.map('map').setView([34.7941333, 138.0062839], 12);

    // var believeIcon = L.icon({
    //     iconUrl: 'haradakomuten_believenoie_pin_1.png',
    //     iconSize:[25,41],
    //     iconAnchor:[12,41]
    // });

    // var reformIcon = L.icon({
    //     iconUrl: 'haradakomuten_haradareform_pin_1.png',
    //     iconSize:[25,41],
    //     iconAnchor:[12,41]
    // });

    var LeafIcon = L.Icon.extend({
        options: {
            iconSize:[25,41],
            iconAnchor:[12,41]
        }
    });

    var sweetIcon = new LeafIcon({iconUrl: '<?= base_url(); ?>assets/leaflet/images/marker-icon.png'}),
        believeIcon = new LeafIcon({iconUrl: '<?= base_url(); ?>assets/leaflet/images/haradakomuten_believenoie_pin_1.png'}),
        reformIcon = new LeafIcon({iconUrl: '<?= base_url(); ?>assets/leaflet/images/haradakomuten_haradareform_pin_1.png'});

    L.icon = function (options) {
        return new L.Icon(options);
    };
  
    
    // L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png', {
    //     attribution: '© OpenStreetMap contributors',
    //       maxZoom: 17,
    //       minZoom: 9   
    // }).addTo(map);
 
    // bike lanes
    // L.tileLayer('http://tiles.mapc.org/trailmap-onroad/{z}/{x}/{y}.png', {
    //     maxZoom: 17,
    //     minZoom: 9
    // }).addTo(map); , {icon: greenIcon}
 
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
          maxZoom: 18,
          minZoom: 3   
    }).addTo(map);

    <?php
    foreach ($data as $mark) {
    ?>
    L.marker([<?=$mark->b?>, <?=$mark->l?>], <?php if($mark->種別 == 'BOB') { echo '{icon: believeIcon}';}else if($mark->種別 == '小工事'){ echo '{icon: reformIcon}';}else{echo '{icon: sweetIcon}';}?> ).addTo(map)
		.bindPopup(`
            <div class="popup-mark">
                <div class="popup-mark__header <?php if($mark->種別 == 'BOB'){ echo 'believe';} if($mark->種別 == '小工事'){ echo 'reform';}?>">
                    <?php if($mark->種別 == 'SOB') : ?>
                        <img src="<?= base_url(); ?>assets/dashboard/img/maps/icon1.png">
                    <?php endif;
                        if($mark->種別 == 'BOB') :
                    ?>
                        <img src="<?= base_url(); ?>assets/dashboard/img/maps/haradakomuten_believenoie_icon_1.png">
                    <?php endif;
                        if($mark->種別 == '小工事') :
                    ?>
                        <img src="<?= base_url(); ?>assets/dashboard/img/maps/haradakomuten_haradareform_icon_1.png">
                    <?php endif;?>
                    <span>引渡し日：<?=$mark->初回年月日?></span>
                </div>

                <div class="popup-mark__body">
                    <div class="_spec">
                        <?php if(!empty($mark->photo)) : ?>
                        <img src="<?= base_url(); ?>uploads/customer/<?=$mark->photo?>">
                        <?php endif;?>
                        <div class="_right">
                            <span><?=$mark->氏名?></span>
                            <?php if(!empty($mark->営業担当)) :?>
                            <div>
                                <span>担当：<?=$mark->営業担当?></span>
                                <img src="<?= base_url(); ?>assets/dashboard/img/maps/user1.png">
                            </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="_btn-wrapper">
                        <?php if(!empty($mark->電　　話)) : ?>
                        <a href="tel:<?=str_replace("-","",$mark->電　　話)?>">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/icon-phone.svg">
                            <span>TEL</span>
                        </a>
                        <?php endif;
                            if(!empty($mark->メールアドレス)) :
                        ?>
                        <a href="mailto:<?=$mark->メールアドレス?>">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/icon-mail.svg">
                            <span>Mail</span>
                        </a>
                        <?php endif?>
                    </div>

                    <div class="_desc">
                        <?php if(!empty($mark->住所)) :?>
                        <div class="_flex">
                            <span class="_left">住所</span>
                            <span class="_right"><?= $mark->住所?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->b)) :
                        ?>
                        <div class="_flex">
                            <span class="_left">B</span>
                            <span class="_right"><?= $mark->b?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->l)) :
                        ?>
                        <div class="_flex">
                            <span class="_left">L</span>
                            <span class="_right"><?=$mark->l?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->customer_code)) :
                        ?>
                        <div class="_flex">
                            <span class="_left">番号</span>
                            <span class="_right"><?=$mark->customer_code?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->sei) && !empty($mark->名)) :
                        ?>
                        <div class="_flex">
                            <span class="_left">名称</span>
                            <span class="_right"><?=$mark->sei.' '.$mark->名?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->dm)) : 
                        ?>
                        <div class="_flex">
                            <span class="_left">連名</span>
                            <span class="_right"><?=$mark->dm?></span>
                        </div>
                        <?php endif;
                        if(!empty($mark->zip_code1)) :
                        ?>
                        <div class="_flex">
                            <span class="_left">い</span>
                            <span class="_right"><?=$mark->zip_code1?></span>
                        </div>
                        <?php endif;?>
                        <div class="_flex">
                            <span class="_left">都道府県</span>
                            <span class="_right">静岡県</span>
                        </div>
                        <?php if(!empty($mark->電話番号)) :?>
                        <div class="_flex">
                            <span class="_left">電話</span>
                            <span class="_right"><?=$mark->電話番号?></span>
                        </div>
                        <?php endif;?>
                        <div class="_flex">
                            <span class="_left">構成</span>
                            <span class="_right">共通</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">調節</span>
                            <span class="_right">HP</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">確認年月日</span>
                            <span class="_right">42292</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">調節</span>
                            <span class="_right">大井</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">土地</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">訪問</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">熟</span>
                            <span class="_right">××</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">ランク</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">備考</span>
                            <span class="_right">ＨＰより資料があります。30代</span>
                        </div>
                    </div>
                </div>
            </div>
        `);
    <?php
    }
    ?>
    
    
</script>
</body>
</html>