<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Map Mark</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-light" style="background-color: #e3f2fd;">
        <a class="navbar-brand" href="<?=base_url()?>">Map GIS</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation"></button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="<?=base_url()?>">Map </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>map/input_mark">Add Marker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url()?>map/input_mark">Marker List</a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="#">Action 1</a>
                        <a class="dropdown-item" href="#">Action 2</a>
                    </div>
                </li> -->
            </ul>
            <!-- <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>
    </nav>
<!-- leaflet map -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
 
<style>
#map { height: 540px; }
</style>
<div class="container">
    <div>
        <center>
        <h1>Daftar Marker</h1>
        </center>
    </div>
    <div class="map-wrapper">
        <div id="map" style="margin-top:2%"></div>
        <div class="map-category map-category--v3">
            <div class="_top">
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i1.svg" alt="">
                    <span>道路</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i2.svg" alt="">
                    <span>橋梁</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i3.svg" alt="">
                    <span>河川</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i4.svg" alt="">
                    <span>崖</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i5.svg" alt="">
                    <span>商業施設</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i6.svg" alt="">
                    <span>医療機関</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/i7.svg" alt="">
                    <span>公共建築</span>
                </a>
            </div>

            <div class="_bottom">
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                    <span>洪水ハザードマップ</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                    <span>津波ハザードマップ</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                    <span>大規模既存集落</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                    <span>緑辺集落</span>
                </a>
                <a href="">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                    <span>下水道整備区域</span>
                </a>
            </div>
        </div>
    </div>
    <br>
    <div>
    <a name="" id="" class="btn btn-primary" href="<?=base_url()?>map/?category=A" role="button">Kategori A</a>
    <a name="" id="" class="btn btn-primary" href="<?=base_url()?>map/?category=B" role="button">Kategori B</a>
    <a name="" id="" class="btn btn-primary" href="<?=base_url()?>map/?category=ALL" role="button">All</a>
    </div>
</div>
 
<script>
    var map = L.map('map').setView([35.109425, 137.7649554], 9);
  
    
    // L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png', {
    //     attribution: '© OpenStreetMap contributors',
    //       maxZoom: 17,
    //       minZoom: 9   
    // }).addTo(map);
 
    // bike lanes
    // L.tileLayer('http://tiles.mapc.org/trailmap-onroad/{z}/{x}/{y}.png', {
    //     maxZoom: 17,
    //     minZoom: 9
    // }).addTo(map);
 
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
          maxZoom: 17,
          minZoom: 9   
    }).addTo(map);

    <?php
    foreach ($data as $mark) {
    ?>
    L.marker([<?=$mark->longitude?>, <?=$mark->latitude?>]).addTo(map)
		.bindPopup(`
            <div class="popup-mark popup-mark--v3">
                <div class="popup-mark__header">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/icon.png">
                    <span>平成25年度<br>舘山寺鹿谷線復旧工事</span>
                </div>

                <div class="popup-mark__body">
                    <div class="_spec">
                        <img src="<?= base_url(); ?>assets/dashboard/img/maps/uhuy.png">
                        <p>ここに写真が入ります</p>
                    </div>

                    <div class="_desc">
                        <div class="_flex">
                            <span class="_left">工事名</span>
                            <span class="_right">舘山寺鹿谷線復旧工事</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">発注者</span>
                            <span class="_right">浜松市</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">金額</span>
                            <span class="_right">1,000,000円</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">工事内容</span>
                            <span class="_right">ここにテキストが入ります。ここにテキストが入ります。</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">契約書</span>
                            <span class="_right">
                                <a href="" class="_pdf-btn">PDF</a>
                            </span>
                        </div>
                        <div class="_flex">
                            <span class="_left">図面</span>
                            <span class="_right">
                                <a href="" class="_pdf-btn">PDF</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        `);
    <?php
    }
    ?>
    
    
</script>
</body>
</html>