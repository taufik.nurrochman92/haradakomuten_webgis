<?php $this->load->view('login/login_head');?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?=base_url()?>"><b>ログイン</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <!-- <p class="login-box-msg">Sign in to start your session</p> -->
      <?php 
      if($this->session->flashdata('error')):
      ?>
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong><?=$this->session->flashdata('error')?></strong> 
      </div>
      
      <script>
        $(".alert").alert();
      </script>
      <?php 
      endif;
      ?>
      <form action="<?=base_url()?>login/check_login" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="ユーザー名" name="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="パスワード" name="password" id="myInput">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">ログインする</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<?php $this->load->view('login/login_footer');
?>
<script>
function mouseoverPass(obj) {
  var obj = document.getElementById('myInput');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('myInput');
  obj.type = "password";
}
</script>
</body>
</html>
