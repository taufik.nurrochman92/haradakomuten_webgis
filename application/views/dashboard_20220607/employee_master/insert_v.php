<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規追加社員マスタ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規追加社員マスタ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_employee_master" method="post">
                      <div class="form-group">
                        <label for="employee_code">社員コード</label>
                        <input type="text" class="form-control col-md-6" name="employee_code" id="employee_code" value="" aria-describedby="helpId" placeholder="例: A001">
                      </div>
                      <div class="form-group">
                        <label for="first_name">氏名 姓</label>
                        <input type="text" class="form-control col-md-6" name="first_name" id="first_name" value="" aria-describedby="helpId" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="last_name">氏名 名</label>
                        <input type="text" class="form-control col-md-6" name="last_name" id="last_name" aria-describedby="helpId" value="" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="first_kana">カナ氏名 姓</label>
                        <input type="text" class="form-control col-md-6" name="first_kana" id="first_kana" aria-describedby="helpId" value="" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="last_kana">カナ氏名 名</label>
                        <input type="text" class="form-control col-md-6" name="last_kana" id="last_kana" aria-describedby="helpId" value="" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="gender">性別</label>
                        <div class="row" style="margin-left:10px">
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="gender" id="" value="男">男
                            </label>
                          </div>
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input class="form-check-input" type="radio" name="gender" id="" value="女">女
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>生年月日</label>
                        <div class="input-group col-md-6 reset-p">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="birthday" id="birthday" class="form-control datepicker-input" placeholder="MM/DD/YYYY">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>入社日</label>
                        <div class="input-group col-md-6 reset-p">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="hire_date" id="hire_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>退職日</label>
                        <div class="input-group col-md-6 reset-p">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="retirement_date" id="retirement_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY">
                        </div>
                      </div>
                      
                      <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>