<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規追加役職マスタ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規追加役職マスタ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_job_master" method="post">
                    <div class="form-group">
                          <label for="job_code">役職コード</label>
                          <input type="text" class="form-control col-md-6" name="job_code" id="job_code" value="" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_rank1">役職ランク	レベル①</label>
                          <input type="text" class="form-control col-md-6" name="job_rank1" id="job_rank1" value="" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_rank2">役職ランク	レベル②</label>
                          <input type="text" class="form-control col-md-6" name="job_rank2" id="job_rank2" value="" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_name">役職名</label>
                          <input type="text" class="form-control col-md-6" name="job_name" id="job_name" aria-describedby="helpId" value="" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>