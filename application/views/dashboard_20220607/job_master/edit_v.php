<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 役職マスタ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 役職マスタ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_job_master/<?=$data->job_code?>" method="post">
                        <div class="form-group">
                          <label for="job_code">役職コード</label>
                          <input type="text" class="form-control col-md-6" name="job_code" id="job_code" value="<?=$data->job_code?>" readonly aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_rank1">役職ランク	レベル①</label>
                          <input type="text" class="form-control col-md-6" name="job_rank1" id="job_rank1" value="<?=$data->job_rank1?>" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_rank2">役職ランク	レベル②</label>
                          <input type="text" class="form-control col-md-6" name="job_rank2" id="job_rank2" value="<?=$data->job_rank2?>" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_name">役職名</label>
                          <input type="text" class="form-control col-md-6" name="job_name" id="job_name" aria-describedby="helpId" value="<?=$data->job_name?>" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>