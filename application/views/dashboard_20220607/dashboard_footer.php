<footer class="main-footer">
    <strong>Copyright &copy; <?=date("Y",strtotime("now"));?> <a href="https://haradakomuten.com/">原田工務店</a>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>assets/dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>assets/dashboard/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>assets/dashboard/plugins/moment/moment-with-locales.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>assets/dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dashboard/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>assets/dashboard/js/pages/dashboard.js"></script>
<!-- bs-custom-file-input -->
<script src="<?=base_url()?>assets/dashboard/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dashboard/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dashboard/js/datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.min.js" integrity="sha512-6+P1bat5JJUm3ZeQKup1qbRitLnE7NE8z47htcKFs8LqH/XAbauzUfg1tGdXJKDJlecq9I/lTm9iCO/0uuho/w==" crossorigin="anonymous"></script>
<script>
  $(document).ready(function () {
    var wat = 0;
    var now = new Date();
    now.setSeconds(0);
    now.setMilliseconds(0);
    var options = {
        language: 'en',
        position: 'top right',
        autoClose: true,
        view: 'days',
        minView: 'days',
        dateFormat: 'mm/dd/yyyy',
        minDate: false, //now
        timepicker: false,
        onSelect: function (fd, d, picker) {
            if (!d) {
                console.log("No selected date!")
                return;
            }

            if (picker.minDate && d < picker.minDate && wat < 3) {
                wat++;
                console.log(fd.toString());
                console.log(d.toString());
                console.log(picker.date);
                console.log(picker.minDate);

                setTimeout(function () {
                    picker.selectDate(new Date(picker.minDate.getTime()))
                    //picker.date = new Date("December 17, 2020 03:24:00");
                    //picker.date = new Date(picker.minDate.getTime());
                });
            }
        }
    };

    $(".datepicker-input").datepicker(options);
});
</script>
</body>
</html>
