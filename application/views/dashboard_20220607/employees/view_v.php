<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">社員詳細情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 社員詳細情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
        <!-- Main row -->
        <div class="row">
          <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="button">
                        <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_employees/<?=$data->社員コード?>" role="button"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_employees/<?=$data->社員コード?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                    <div class="col-md-6 mx-auto" style="text-align:center;">
                        <div class="form-group">
                          <label for="employee_code">社員コード</label>
                          <p><?=$data->社員コード?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name">氏名 (姓)</label>
                        <p><?=$data->姓?></p>
                    </div>
                    <div class="form-group">
                        <label for="first_name">氏名 (名)</label>
                        <p><?=$data->名?></p>
                    </div>
                    <div class="form-group">
                        <label for="last_furigana">氏名 (せい)</label>
                        <p><?=$data->せい?></p>
                    </div>
                    <div class="form-group">
                        <label for="first_furigana">氏名 (めい)</label>
                        <p><?=$data->めい?></p>
                    </div>
                    <div class="form-group">
                        <label for="qualification_1">保有資格1</label>
                        <p><?=$data->保有資格1?></p>
                    </div>
                    <div class="form-group">
                        <label for="qualification_2">保有資格2</label>
                        <p><?=$data->保有資格2?></p>
                    </div>
                    <div class="form-group">
                        <label for="qualification_3">保有資格3</label>
                        <p><?=$data->保有資格3?></p>
                    </div>
                    <div class="form-group">
                        <label for="qualification_4">保有資格4</label>
                        <p><?=$data->保有資格4?></p>
                    </div>
                    <div class="form-group">
                        <label for="qualification_5">保有資格5</label>
                        <p><?=$data->保有資格5?></p>
                    </div>
                    <div class="form-group">
                        <label for="hire_date">入社日</label>
                        <p><?=$data->入社日?></p>
                    </div>
                    <div class="form-group">
                        <label for="zip_code">郵便番号1</label>
                        <p><?=$data->郵便番号1?></p>
                    </div>
                    <div class="form-group">
                        <label for="address_line">住所1</label>
                        <p><?=$data->住所1?></p>
                    </div>
                    <div class="form-group">
                        <label for="address">住所1 番地</label>
                        <p><?=$data->住所1番地?></p>
                    </div>
                    <div class="form-group">
                        <label for="telephone_number">電話番号</label>
                        <p><?=$data->電話番号?></p>
                    </div>
                    <div class="form-group">
                        <label for="cellphone_number">携帯番号</label>
                        <p><?=$data->携帯番号?></p>
                    </div>
                    <div class="button">
                    <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_employees/<?=$data->社員コード?>" role="button"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_employees/<?=$data->社員コード?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>