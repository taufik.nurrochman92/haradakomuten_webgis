<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_employees/<?=$data->社員コード?>" method="post">
                        <div class="form-group">
                          <label for="employee_code">社員コード</label>
                          <input type="text" class="form-control col-md-6" name="employee_code" id="employee_code" aria-describedby="helpId" placeholder="" value="<?=$data->社員コード?>">
                        </div>
                        <div class="form-group">
                          <label for="last_name">氏名 (姓)</label>
                          <input type="text" class="form-control col-md-6" name="last_name" id="last_name" aria-describedby="helpId" placeholder="" value="<?=$data->姓?>">
                        </div>
                        <div class="form-group">
                          <label for="first_name">氏名 (名)</label>
                          <input type="text" class="form-control col-md-6" name="first_name" id="first_name" aria-describedby="helpId" placeholder="" value="<?=$data->名?>">
                        </div>
                        <div class="form-group">
                          <label for="last_furigana">氏名 (せい)</label>
                          <input type="text" class="form-control col-md-6" name="last_furigana" id="last_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->せい?>">
                        </div>
                        <div class="form-group">
                          <label for="first_furigana">氏名 (めい)</label>
                          <input type="text" class="form-control col-md-6" name="first_furigana" id="first_furigana" aria-describedby="helpId" placeholder="" value="<?=$data->めい?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_1">保有資格1</label>
                          <input type="text" class="form-control col-md-6" name="qualification_1" id="qualification_1" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格1?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_2">保有資格2</label>
                          <input type="text" class="form-control col-md-6" name="qualification_2" id="qualification_2" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格2?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_3">保有資格3</label>
                          <input type="text" class="form-control col-md-6" name="qualification_3" id="qualification_3" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格3?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_4">保有資格4</label>
                          <input type="text" class="form-control col-md-6" name="qualification_4" id="qualification_4" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格4?>">
                        </div>
                        <div class="form-group">
                          <label for="qualification_5">保有資格5</label>
                          <input type="text" class="form-control col-md-6" name="qualification_5" id="qualification_5" aria-describedby="helpId" placeholder="" value="<?=$data->保有資格5?>">
                        </div>
                        <div class="form-group">
                          <label for="hire_date">入社日</label>
                          <input type="date" class="form-control col-md-6" name="hire_date" id="hire_date" aria-describedby="helpId" placeholder="" value="<?php if (strtotime($data->入社日)) { echo isset($data->入社日) ? set_value('hire_date', date('Y-m-d', strtotime($data->入社日))) : set_value('hire_date'); };?>">
                        </div>
                        <div class="form-group">
                          <label for="zip_code">郵便番号1</label>
                          <input type="text" class="form-control col-md-6" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="" value="<?=$data->郵便番号1?>">
                          
                        </div>
                        <div class="form-group">
                          <label for="address_line">住所1</label>
                          <input type="text" class="form-control col-md-6" name="address_line" id="address_line" aria-describedby="helpId" placeholder="" value="<?=$data->住所1?>">
                        </div>
                        <div class="form-group">
                          <label for="address">住所1 番地</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder="" value="<?=$data->住所1番地?>">
                        </div>
                        <div class="form-group">
                          <label for="telephone_number">電話番号</label>
                          <input type="text" class="form-control col-md-6" name="telephone_number" id="telephone_number" aria-describedby="helpId" placeholder="" value="<?=$data->電話番号?>">
                          
                        </div>
                        <div class="form-group">
                          <label for="cellphone_number">携帯番号</label>
                          <input type="text" class="form-control col-md-6" name="cellphone_number" id="cellphone_number" aria-describedby="helpId" placeholder="" value="<?=$data->携帯番号?>">
                          
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>