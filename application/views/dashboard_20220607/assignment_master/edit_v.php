<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 配属マスタ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 配属マスタ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_assignment_master/<?=$data->employee_code?>" method="post">
                        <div class="form-group">
                          <label for="employee_code">メインキー社員コード</label>
                          <input type="text" class="form-control col-md-6" name="employee_code" id="employee_code" value="<?=$data->employee_code?>" readonly aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label>メインキー開始日</label>
                          <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="start_date" id="start_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY" value="<?=date("m-d-Y",strtotime($data->start_date))?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label>終了日</label>
                          <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="end_date" id="end_date" class="form-control datepicker-input" placeholder="MM/DD/YYYY" value="<?=date("m-d-Y",strtotime($data->end_date))?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="department_code">部署コード</label>
                          <input type="text" class="form-control col-md-6" name="department_code" id="department_code" aria-describedby="helpId" value="<?=$data->department_code?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="job_code">役職コード</label>
                          <input type="text" class="form-control col-md-6" name="job_code" id="job_code" aria-describedby="helpId" value="<?=$data->job_code?>" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="保存">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>