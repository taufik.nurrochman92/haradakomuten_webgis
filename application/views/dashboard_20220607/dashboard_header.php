<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>原田工務店 webGIS | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/datepicker.css">
  <!-- datatable -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <!-- CUSTOM -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/custom.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
        <li>
            <a name="" id="" class="btn btn-primary" href="<?=base_url()?>dashboard/logout" role="button">ログアウト</a>
        </li>  
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=base_url()?>dashboard" class="brand-link">
      <!-- <img src="<?=base_url()?>assets/dashboard/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <img src="<?=base_url()?>assets/dashboard/img/logo.png" width="100%">
      <!-- <span class="brand-text font-weight-light">AdminLTE 3</span> -->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link " >
            <i class="fa fa-user-circle"></i>
              <p style="border-bottom: #f9edda 3px solid;">
                営業中顧客
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
            <ul class="nav">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/client_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>営業中顧客一覧 <?php if(!empty($selector_client)){ echo $selector_client;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_client" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規営業中顧客 <?php if(!empty($selector_add_client)){ echo $selector_add_client;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link " >
            <i class="fa fa-user"></i>
              <p style="border-bottom: #f9edda 3px solid;">
                OB顧客
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
            <ul class="nav">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/customers_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>OB顧客一覧 <?php if(!empty($selector_customers)){ echo $selector_customers;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_customers" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規OB顧客 <?php if(!empty($selector_add_customers)){ echo $selector_add_customers;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
              <i class="fa fa-search"></i>
              <p style="border-bottom: #ffcf89 3px solid;">
              点検管理
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/inspection_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_inspection" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規追加</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-handshake"></i>
              <p style="border-bottom: #b1e799 3px solid;">
              協力会社
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/partner_list" class="nav-link ">
                  <i class="fa fa-list nav-icon"></i>
                  <p>一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_partner" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規追加</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-building"></i>
              <p style="border-bottom: #d5f4ff 3px solid;">
              公共事業
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/public_works_list" class="nav-link ">
                  <i class="fa fa-list nav-icon"></i>
                  <p>一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_public_works" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規追加</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-building"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              役職マスタ
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/job_master_list" class="nav-link ">
                  <i class="fa fa-list nav-icon"></i>
                  <p>一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_job_master" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規追加</p>
                </a>
              </li>
            </ul>
          </li> -->
          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link " >
            <i class="fa fa-building"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              社員マスタ
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/employee_master_list" class="nav-link ">
                  <i class="fa fa-list nav-icon"></i>
                  <p>一覧</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_employee_master" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規追加</p>
                </a>
              </li>
            </ul>
          </li> -->
          <li class="nav-item">
            <a href="#" class="nav-link " >
            <i class="fa fa-users"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              社員マスター
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
            <ul class="nav">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/employees_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>社員一覧 <?php if(!empty($selector_employees)){ echo $selector_employees;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_employees" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規社員追加 <?php if(!empty($selector_add_employees)){ echo $selector_add_employees;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link " >
            <i class="fa fa-building"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              業者管理
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>
            <ul class="nav">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/contractor_list" class="nav-link " >
                  <i class="fa fa-list nav-icon"></i>
                  <p>業者一覧 <?php if(!empty($selector_contractor)){ echo $selector_contractor;}?></p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/add_contractor" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規業者追加 <?php if(!empty($selector_add_contractor)){ echo $selector_add_contractor;}?></p>
                </a>
              </li>
            </ul>
          </li>
          <!-- <li class="nav-item">
            <a href="#" class="nav-link " >
            <i class="fa fa-envelope"></i>
              <p style="border-bottom: #f9edda 3px solid;">
              メールアドレス -->
                <!-- <i class="right fas fa-angle-left"></i> -->
              <!-- </p>
            </a>
            <ul class="nav">
              <li class="nav-item">
                <a href="<?=base_url()?>dashboard/email" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>新規メールアドレス</p>
                </a>
              </li>
            </ul>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>