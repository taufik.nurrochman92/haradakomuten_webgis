<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">業者詳細情報</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 業者詳細情報</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
        <div class="row">
          <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="button" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_contractor/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                    <div class="form-group">
                      <label for="kinds">種別</label>
                      <p><?=$data->種別?></p>
                    </div>
                    <div class="form-group">
                      <label for="latitude">緯度</label>
                      <p><?=$data->b?></p>
                    </div>
                    <div class="form-group">
                      <label for="longitude">経度</label>
                      <p><?=$data->l?></p>
                    </div>
                    <div class="form-group">
                      <label for="no">No.</label>
                      <p><?=$data->no?></p>
                    </div>
                    <div class="form-group">
                      <label for="name">協力業者名</label>
                      <p><?=$data->name?></p>
                    </div>
                    <div class="form-group">
                      <label for="joint_name">連名</label>
                      <p><?=$data->連名?></p>
                    </div>
                    <div class="form-group">
                      <label for="zip_code">〒</label>
                      <p><?=$data->〒?></p>
                    </div>
                    <div class="form-group">
                      <label for="prefecture">都道府県</label>
                      <p><?=$data->都道府県?></p>
                    </div>
                    <div class="form-group">
                      <label for="address">住　所</label>
                      <p><?=$data->住　　　所?></p>
                    </div>
                    <div class="form-group">
                      <label for="address2">建物名・部屋番号</label>
                      <p><?=$data->住所2?></p>
                    </div>
                    <div class="form-group">
                      <label for="telephone">電　話</label>
                      <p><?=$data->電　　話?></p>
                    </div>
                    <div class="button" style="margin-top:20px;">
                        <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_contractor/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_contractor/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>