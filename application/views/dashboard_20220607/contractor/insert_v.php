<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規業者追加</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規業者追加</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_contractor" method="post">
                    <input type="text" class="form-control col-md-6" name="contractor_code" id="contractor_code" aria-describedby="helpId" placeholder="" value="<?=$for_id->gid+1?>" hidden>
                        <div class="form-group">
                          <label for="kinds">種別</label>
                          <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder="" >
                        </div>
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="no">No.</label>
                          <input type="text" class="form-control col-md-6" name="no" id="no" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="name">協力業者名</label>
                          <input type="text" class="form-control col-md-6" name="name" id="name" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="joint_name">連名</label>
                          <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="zip_code">〒</label>
                          <input type="text" class="form-control col-md-6" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="prefecture">都道府県</label>
                          <input type="text" class="form-control col-md-6" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="address">住　所</label>
                          <input type="text" class="form-control col-md-6" name="address" id="address" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="address2">建物名・部屋番号</label>
                          <input type="text" class="form-control col-md-6" name="address2" id="address2" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="telephone">電　話</label>
                          <input type="text" class="form-control col-md-6" name="telephone" id="telephone" aria-describedby="helpId" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>
</html>