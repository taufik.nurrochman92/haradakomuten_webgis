<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
 
<style>
#map { height: 540px; }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#ffcf89">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">点検一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 点検一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body">
                  <div class="card-body table-responsive">
                      <table class="table table-striped table-inverse" id="data_customer">
                        <thead class="thead-inverse">
                          <tr>
                            <th>GID</th>
                            <th>緯度</th>
                            <th>経度</th>
                            <th>種別</th>
                            <th>氏</th>
                            <th>名</th>
                            <th>様</th>
                            <th>住所</th>
                            <th>電話番号</th>
                            <th>完了検査日</th>
                            <th>引渡し日</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                            <tr>
                              <td scope="row"><?=$value->gid?></td>
                              <td><?=$value->latitude?></td>
                              <td><?=$value->longitude?></td>
                              <td><?=$value->type?></td>
                              <td><?=$value->family_name?></td>
                              <td><?=$value->name?></td>
                              <td><?=$value->sama?></td>
                              <td><?=$value->address?></td>
                              <td><?=$value->phone?></td>
                              <td><?=date("m/d/Y",strtotime($value->inspection_date))?></td>
                              <td><?=date("m/d/Y",strtotime($value->delivery_date))?></td>
                              <td>
                              <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_inspection/<?=$value->gid?>" role="button"><i class="fa fa-edit"></i></a>
                              <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_inspection/<?=$value->gid?>" role="button"><i class="fa fa-trash"></i></a>
                              </td>
                            </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="12">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                          </tbody>
                      </table>
                  </div>
                </div>
            </div>
            <div class="card">
              <div class="map-wrapper">
                <div id="map" style="margin-top:2%"></div>
              </div>
            </div>
          </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
<script>
    var map = L.map('map').setView([35.109425, 137.7649554], 9);
  
    
    // L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png', {
    //     attribution: '© OpenStreetMap contributors',
    //       maxZoom: 17,
    //       minZoom: 9   
    // }).addTo(map);
 
    // bike lanes
    // L.tileLayer('http://tiles.mapc.org/trailmap-onroad/{z}/{x}/{y}.png', {
    //     maxZoom: 17,
    //     minZoom: 9
    // }).addTo(map);
 
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
          maxZoom: 18,
          minZoom: 3   
    }).addTo(map);

    <?php
    foreach ($data as $mark) {
    ?>
    L.marker([<?=$mark->longitude?>, <?=$mark->latitude?>]).addTo(map)
		.bindPopup(`
    <div class="popup-mark popup-mark--v2">
                <div class="popup-mark__header">
                    <span>顧客情報詳細</span>
                </div>

                <div class="popup-mark__body">
                    <div class="_desc">
                        <div class="_flex">
                            <span class="_left">B</span>
                            <span class="_right">35.053631</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">L</span>
                            <span class="_right">137.133833</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">番号</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">名称</span>
                            <span class="_right">丸野幸子</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">連名</span>
                            <span class="_right">DM×</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">い</span>
                            <span class="_right">473-0907</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">都道府県</span>
                            <span class="_right">静岡県</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">共同住宅</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">電話</span>
                            <span class="_right">080-3111-8583</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">構成</span>
                            <span class="_right">共通</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">調節</span>
                            <span class="_right">HP</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">確認年月日</span>
                            <span class="_right">42292</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">調節</span>
                            <span class="_right">大井</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">土地</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">訪問</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">熟</span>
                            <span class="_right">××</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">ランク</span>
                            <span class="_right"></span>
                        </div>
                        <div class="_flex">
                            <span class="_left">備考</span>
                            <span class="_right">ＨＰより資料があります。30代</span>
                        </div>
                    </div>
                </div>
            </div>
        `);
    <?php
    }
    ?>
    
</script>
</body>
</html>