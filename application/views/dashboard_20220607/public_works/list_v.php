<?php $this->load->view('dashboard/dashboard_header');?>
<!-- leaflet map -->
<link rel="stylesheet" href="<?=base_url()?>assets/dashboard/css/maps.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/leaflet/leaflet.css" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?= base_url(); ?>assets/leaflet/leaflet.js"></script>
 
<style>
#map { height: 540px; }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#d5f4ff">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">公共事業一覧</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 公共事業一覧</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('success')?></strong> 
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>
            <script>
              $(".alert").alert();
            </script>
            <div class="card">
                <div class="card-body">
                <div class="card-body table-responsive">
                      <table class="table table-striped table-inverse" id="data_customer">
                        <thead class="thead-inverse">
                          <tr>
                            <th>GID</th>
                            <th>緯度</th>
                            <th>経度</th>
                            <th>整理番号</th>
                            <th>書類画像</th>
                            <th>発注者</th>
                            <th>工種</th>
                            <th>工事名</th>
                            <th>施工場所</th>
                            <th>路線名/河川名</th>
                            <th>路線図番号</th>
                            <th>年度</th>
                            <th>契約年月日</th>
                            <th>契約時金額（税込）</th>
                            <th>着手日</th>
                            <th>完工日</th>
                            <th>契約最終金額（税込）</th>
                            <th>施工内容</th>
                            <th>工事成績</th>
                            <th>主任技術者</th>
                            <th>代理人</th>
                            <th>工事担当者</th>
                            <th>発注担当者</th>
                            <th>営業担当</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php 
                          if(count($data) > 0):
                            foreach($data as $key => $value):
                          ?>
                            <tr>
                              <td scope="row"><?=$value->gid?></td>
                              <td><?=$value->latitude?></td>
                              <td><?=$value->longitude?></td>
                              <td style="width:100px"><?=$value->reference_number?></td>
                              <td style="width:100px"><a href="<?=base_url()?>uploads/<?=$value->document_image?>" target="_blank">Link <i class="fa fa-external-link-alt"></i></a></td>
                              <td><?=$value->ordering_party?></td>
                              <td><?=$value->work_type?></td>
                              <td><?=$value->construction_name?></td>
                              <td><?=$value->construction_site?></td>
                              <td><?=$value->route_name?></td>
                              <td><?=$value->route_number?></td>
                              <td><?=$value->year?></td>
                              <td><?=date("m/d/Y",strtotime($value->contract_date))?></td>
                              <td><?=$value->first_contract_price?></td>
                              <td><?=date("m/d/Y",strtotime($value->start_date))?></td>
                              <td><?=date("m/d/Y",strtotime($value->end_date))?></td>
                              <td><?=$value->final_contract_price?></td>
                              <td><?=$value->construction_detail?></td>
                              <td><?=$value->construction_result?></td>
                              <td><?=$value->lead_engineer?></td>
                              <td><?=$value->agent?></td>
                              <td><?=$value->construction_staff?></td>
                              <td><?=$value->requester?></td>
                              <td><?=$value->sales_staff?></td>
                              <td>
                              <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_public_works/<?=$value->gid?>" role="button"><i class="fa fa-edit"></i></a>
                              <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_public_works/<?=$value->gid?>" role="button"><i class="fa fa-trash"></i></a>
                              </td>
                            </tr>
                          <?php 
                            endforeach;
                          else:
                          ?>
                            <tr>
                              <td colspan="25">
                                <center>No Data</center>
                              </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                          </tbody>
                      </table>
                  </div>
                </div>
            </div>
            <div class="card">
              <div class="map-wrapper">
                <div id="map" style="margin-top:2%"></div>
                <div class="map-category map-category--v3">
                    <div class="_top">
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i1.svg" alt="">
                            <span>道路</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i2.svg" alt="">
                            <span>橋梁</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i3.svg" alt="">
                            <span>河川</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i4.svg" alt="">
                            <span>崖</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i5.svg" alt="">
                            <span>商業施設</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i6.svg" alt="">
                            <span>医療機関</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/i7.svg" alt="">
                            <span>公共建築</span>
                        </a>
                    </div>

                    <div class="_bottom">
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                            <span>洪水ハザードマップ</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                            <span>津波ハザードマップ</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                            <span>大規模既存集落</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                            <span>緑辺集落</span>
                        </a>
                        <a href="">
                            <img src="<?= base_url(); ?>assets/dashboard/img/maps/imap.svg" alt="">
                            <span>下水道整備区域</span>
                        </a>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- datatable -->
<script src="<?=base_url()?>assets/dashboard/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/dashboard/js/jquery-confirm.min.js"></script>
<script>
  $('#data_customer').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  $('.delete').confirm({
    title:'',
    content: "物件情報を削除されますが、よろしいですか。",
    buttons: {
      削除: {
        btnClass: 'btn-blue',
        action: function(){
            location.href = this.$target.attr('href');
          }
        },
        キャンセル: function(){

        }
    }
  });
</script>
<script>
    var map = L.map('map').setView([35.109425, 137.7649554], 9);
  
    
    // L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png', {
    //     attribution: '© OpenStreetMap contributors',
    //       maxZoom: 17,
    //       minZoom: 9   
    // }).addTo(map);
 
    // bike lanes
    // L.tileLayer('http://tiles.mapc.org/trailmap-onroad/{z}/{x}/{y}.png', {
    //     maxZoom: 17,
    //     minZoom: 9
    // }).addTo(map);
 
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
          maxZoom: 17,
          minZoom: 9   
    }).addTo(map);

    <?php
    foreach ($data as $mark) {
    ?>
    L.marker([<?=$mark->longitude?>, <?=$mark->latitude?>]).addTo(map)
		.bindPopup(`
            <div class="popup-mark popup-mark--v3">
                <div class="popup-mark__header">
                    <img src="<?= base_url(); ?>assets/dashboard/img/maps/icon.png">
                    <span>平成25年度<br>舘山寺鹿谷線復旧工事</span>
                </div>

                <div class="popup-mark__body">
                    <div class="_spec">
                        <img src="<?= base_url(); ?>assets/dashboard/img/maps/uhuy.png">
                        <p>ここに写真が入ります</p>
                    </div>

                    <div class="_desc">
                        <div class="_flex">
                            <span class="_left">工事名</span>
                            <span class="_right">舘山寺鹿谷線復旧工事</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">発注者</span>
                            <span class="_right">浜松市</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">金額</span>
                            <span class="_right">1,000,000円</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">工事内容</span>
                            <span class="_right">ここにテキストが入ります。ここにテキストが入ります。</span>
                        </div>
                        <div class="_flex">
                            <span class="_left">契約書</span>
                            <span class="_right">
                                <a href="" class="_pdf-btn">PDF</a>
                            </span>
                        </div>
                        <div class="_flex">
                            <span class="_left">図面</span>
                            <span class="_right">
                                <a href="" class="_pdf-btn">PDF</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        `);
    <?php
    }
    ?>
    
    
</script>
</body>
</html>