<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="background-color:#d5f4ff">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit 公共事業</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / Edit 公共事業</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/update_public_works/<?=$data->gid?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <label for="latitude">緯度</label>
                          <input type="text" class="form-control col-md-6" name="latitude" id="latitude" value="<?=$data->latitude?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="longitude">経度</label>
                          <input type="text" class="form-control col-md-6" name="longitude" id="longitude" value="<?=$data->longitude?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="reference_number">整理番号</label>
                          <input type="text" class="form-control col-md-6" name="reference_number" id="reference_number" value="<?=$data->reference_number?>" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>書類画像</label>
                            <div class="col-md-4" style="margin-bottom:15px">
                                <img src="<?=base_url()?>uploads/<?=$data->document_image?>" class="img-fluid" alt="">
                            </div>
                            <div class="input-group">
                                <div class="custom-file col-md-6">
                                    <input type="file" class="custom-file-input" name="document_image" id="document_image">
                                    <label class="custom-file-label" for="document_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="ordering_party">発注者</label>
                          <input type="text" class="form-control col-md-6" name="ordering_party" id="ordering_party" value="<?=$data->ordering_party?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="work_type">工種</label>
                          <input type="text" class="form-control col-md-6" name="work_type" id="work_type" value="<?=$data->work_type?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="construction_name">工事名</label>
                          <input type="text" class="form-control col-md-6" name="construction_name" id="construction_name" value="<?=$data->construction_name?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="construction_site">施工場所</label>
                          <input type="text" class="form-control col-md-6" name="construction_site" id="construction_site" value="<?=$data->construction_site?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="route_name">路線名/河川名</label>
                          <input type="text" class="form-control col-md-6" name="route_name" id="route_name" value="<?=$data->route_name?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="route_number">路線図番号</label>
                          <input type="text" class="form-control col-md-6" name="route_number" id="route_number" value="<?=$data->route_number?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="year">年度</label>
                          <input type="text" class="form-control col-md-6" name="year" id="year" value="<?=$data->year?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label>契約年月日</label>
                          <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="contract_date" id="contract_date" class="form-control datepicker-input" value="<?=date("m/d/Y",strtotime($data->contract_date))?>" placeholder="MM/DD/YYYY">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="first_contract_price">契約時金額（税込）</label>
                          <input type="text" class="form-control col-md-6" name="first_contract_price" id="first_contract_price" value="<?=$data->first_contract_price?>" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>着手日</label>
                            <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="start_date" id="start_date" class="form-control datepicker-input" value="<?=date("m/d/Y",strtotime($data->start_date))?>" placeholder="MM/DD/YYYY">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>完工日</label>
                            <div class="input-group col-md-6 reset-p">
                              <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input type="text" name="end_date" id="contract_date" class="form-control datepicker-input" value="<?=date("m/d/Y",strtotime($data->contract_date))?>" placeholder="MM/DD/YYYY">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="final_contract_price">契約最終金額（税込）</label>
                          <input type="text" class="form-control col-md-6" name="final_contract_price" id="final_contract_price" value="<?=$data->final_contract_price?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="construction_detail">施工内容</label>
                          <input type="text" class="form-control col-md-6" name="construction_detail" id="construction_detail" value="<?=$data->construction_detail?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="construction_result">工事成績</label>
                          <input type="text" class="form-control col-md-6" name="construction_result" id="construction_result" value="<?=$data->construction_result?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="lead_engineer">主任技術者</label>
                          <input type="text" class="form-control col-md-6" name="lead_engineer" id="lead_engineer" value="<?=$data->lead_engineer?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="agent">代理人</label>
                          <input type="text" class="form-control col-md-6" name="agent" id="agent" value="<?=$data->agent?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="construction_staff">工事担当者</label>
                          <input type="text" class="form-control col-md-6" name="construction_staff" id="construction_staff" value="<?=$data->construction_staff?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="requester">発注担当者</label>
                          <input type="text" class="form-control col-md-6" name="requester" id="requester" value="<?=$data->requester?>" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="sales_staff">営業担当</label>
                          <input type="text" class="form-control col-md-6" name="sales_staff" id="sales_staff" value="<?=$data->sales_staff?>" placeholder="">
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>