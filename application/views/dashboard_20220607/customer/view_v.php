<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">営業中顧客一覧</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">管理ページ / 営業中顧客一覧</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content view_custom">
        <div class="container-fluid">
        <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('success')?></strong> 
        </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong><?=$this->session->flashdata('error')?></strong> 
        </div>
        <?php endif;?>
        <script>
          $(".alert").alert();
        </script>
            <!-- Main row -->
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <div class="button" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_client/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_client/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
                            </div>
                                <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="flex-box double">
                                                <div class="label-view">
                                                    <label for="latitude">緯度</label>
                                                </div>
                                                <div class="text-view">
                                                    <p>
                                                        <?=$data->b?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="flex-box double">
                                                <div class="label-view">
                                                    <label for="longitude">経度</label>
                                                </div>
                                                <div class="text-view">
                                                    <p>
                                                        <?=$data->l?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="full_name">氏名 (漢字)</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->氏名?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="full_name">氏名（かな）</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->氏名_かな?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="joint_name">連名</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->連名?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                                    <div class="form-group">
                                        <div class="flex-box">
                                            <div class="label-view">
                                                <label for="address">住所</label>
                                            </div>
                                            <div class="text-view">
                                                <p>
                                                    <span>〒<?=$data->〒?></span>
                                                    &nbsp;&nbsp;
                                                    <span><?=$data->都道府県?><?=$data->住所?><?=$data->共同住宅?></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="apart_name">建物名・部屋番号</label>
                                    <p>
                                        
                                    </p>
                                </div> -->
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="phone">電話</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->電話?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="kind">種別</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->種別?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="first_contact">初回接点</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->初回接点?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="first_date">初回年月日</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->初回年月日?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="first_time_change">初回担当</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->初回担当?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="pic_person">担当</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->担当?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="with_or_without">土地　有無</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->土地　有無?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="visit">訪問</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->訪問?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="dm">DM</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->ＤＭ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="rank">ランク</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->ランク?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="flex-box">
                                        <div class="label-view">
                                            <label for="remarks">備考</label>
                                        </div>
                                        <div class="text-view">
                                            <p>
                                                <?=$data->備考?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <div class="button" style="margin-top:20px;">
                                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_client/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_client/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right col -->
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>

</html>