<?php $this->load->view('dashboard/dashboard_header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">管理ページ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>
                <?=$this->session->flashdata('success');?>
              </strong> 
            </div>
            
            <script>
              $(".alert").alert();
            </script>
            <?php endif; ?>
            <div class="card">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <!-- PIE CHART -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="card card-success">
                          <div class="card-header">
                            <h3 class="card-title">General Tracking Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="generalChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Customer Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="customerChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-success">
                          <div class="card-header">
                            <h3 class="card-title">Inspection Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="inspectionChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Partner Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="partnerChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-success">
                          <div class="card-header">
                            <h3 class="card-title">Public Work Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="publicWorkChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Job Master Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="jobMasterChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-success">
                          <div class="card-header">
                            <h3 class="card-title">Assignment Master Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="assignmentMasterChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-danger">
                          <div class="card-header">
                            <h3 class="card-title">Employee Master Menu Statistic</h3>
                            <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                          </div>
                          <div class="card-body">
                            <canvas id="employeeMasterChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                          </div>
                          <!-- /.card-body -->
                        </div>
                      </div>
                    </div>
                    
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<!-- ChartJS -->
<script src="<?=base_url()?>assets/dashboard/plugins/chart.js/Chart.min.js"></script>
<script>
  /*
  'Dashboard', 
        'Customer',
        'customer_list',
        'add_customer', 
        'edit_customer', 
        'delete_customer', 
        'inspection_list',
        'add_inspection', 
        'edit_inspection',
        'delete_inspection', 
        'partner_list', 
        'add_partner', 
        'edit_partner',
        'delete_partner', 
        'public_work_list',
        'add_public_work', 
        'edit_public_work', 
        'delete_public_work', 
        'job_master_list',
        'add_job_master', 
        'edit_job_master',
        'delete_job_master', 
        'assignment_master_list', 
        'add_assignment_master', 
        'edit_assignment_master',
        'delete_assignment_master', 
        'employee_master_list', 
        'add_employee_master', 
        'edit_employee_master',
        'delete_employee_master', 
  */
var generalData        = {
    labels: [
        'Dashboard', 
        'Customer',
        'Inspection',
        'Partner', 
        'Public Work',
        'Job Master',
        'Assignment Master', 
        'Employee Master', 
    ],
    datasets: [
      {
        data: [<?=$dashboard?>,<?=$customer?>,<?=$inspection?>,<?=$partner?>,<?=$public_work?>,<?=$job_master?>,<?=$assignment_master?>,<?=$employee_master?>],
        backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de',"#ad0663","#778899"],
      }
    ]
  }

var customerData        = {
  labels: [
      'View List', 
      'Add Customer - Form',
      'Add Customer - Execute',
      'Edit Customer - Form', 
      'Edit Customer - Execute',
      'Delete Customer',
  ],
  datasets: [
    {
      data: [<?=$customer_list?>,<?=$add_customer_view?>,<?=$add_customer_execute?>,<?=$edit_customer_view?>,<?=$edit_customer_execute?>,<?=$delete_customer?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var inspectionData        = {
  labels: [
      'View List', 
      'Add Inspection - Form',
      'Add Inspection - Execute',
      'Edit Inspection - Form', 
      'Edit Inspection - Execute',
      'Delete Inspection',
  ],
  datasets: [
    {
      data: [<?=$inspection_list?>,<?=$add_inspection_view?>,<?=$add_inspection_execute?>,<?=$edit_inspection_view?>,<?=$edit_inspection_execute?>,<?=$delete_inspection?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var partnerData        = {
  labels: [
      'View List', 
      'Add Partner - Form',
      'Add Partner - Execute',
      'Edit Partner - Form', 
      'Edit Partner - Execute',
      'Delete Partner',
  ],
  datasets: [
    {
      data: [<?=$partner_list?>,<?=$add_partner_view?>,<?=$add_partner_execute?>,<?=$edit_partner_view?>,<?=$edit_partner_execute?>,<?=$delete_partner?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var publicWorkData        = {
  labels: [
      'View List', 
      'Add Public Work - Form',
      'Add Public Work - Execute',
      'Edit Public Work - Form', 
      'Edit Public Work - Execute',
      'Delete Public Work',
  ],
  datasets: [
    {
      data: [<?=$public_work_list?>,<?=$add_public_work_view?>,<?=$add_public_work_execute?>,<?=$edit_public_work_view?>,<?=$edit_public_work_execute?>,<?=$delete_public_work?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var jobMasterData        = {
  labels: [
      'View List', 
      'Add Job Master - Form',
      'Add Job Master - Execute',
      'Edit Job Master - Form', 
      'Edit Job Master - Execute',
      'Delete Job Master',
  ],
  datasets: [
    {
      data: [<?=$job_master_list?>,<?=$add_job_master_view?>,<?=$add_job_master_execute?>,<?=$edit_job_master_view?>,<?=$edit_job_master_execute?>,<?=$delete_job_master?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var assignmentMasterData        = {
  labels: [
      'View List', 
      'Add Assignment Master - Form',
      'Add Assignment Master - Execute',
      'Edit Assignment Master - Form', 
      'Edit Assignment Master - Execute',
      'Delete Assignment Master',
  ],
  datasets: [
    {
      data: [<?=$assignment_master_list?>,<?=$add_assignment_master_view?>,<?=$add_assignment_master_execute?>,<?=$edit_assignment_master_view?>,<?=$edit_assignment_master_execute?>,<?=$delete_assignment_master?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}

var employeeMasterData        = {
  labels: [
      'View List', 
      'Add Employee Master - Form',
      'Add Employee Master - Execute',
      'Edit Employee Master - Form', 
      'Edit Employee Master - Execute',
      'Delete Employee Master',
  ],
  datasets: [
    {
      data: [<?=$employee_master_list?>,<?=$add_employee_master_view?>,<?=$add_employee_master_execute?>,<?=$edit_employee_master_view?>,<?=$edit_employee_master_execute?>,<?=$delete_employee_master?>],
      backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
var generalCanvas = $('#generalChart').get(0).getContext('2d')
var genData        = generalData;
var customerCanvas = $('#customerChart').get(0).getContext('2d')
var cusData        = customerData;
var inspectionCanvas = $('#inspectionChart').get(0).getContext('2d')
var insData        = inspectionData;
var partnerCanvas = $('#partnerChart').get(0).getContext('2d')
var parData        = partnerData;
var publicWorkCanvas = $('#publicWorkChart').get(0).getContext('2d')
var pubData        = publicWorkData;
var jobMasterCanvas = $('#jobMasterChart').get(0).getContext('2d')
var jobData        = jobMasterData;
var assignmentMasterCanvas = $('#assignmentMasterChart').get(0).getContext('2d')
var assData        = assignmentMasterData;
var employeeMasterCanvas = $('#employeeMasterChart').get(0).getContext('2d')
var empData        = employeeMasterData;
var pieOptions     = {
  maintainAspectRatio : false,
  responsive : true,
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var generalChart = new Chart(generalCanvas, {
  type: 'pie',
  data: genData,
  options: pieOptions      
});


var customerChart = new Chart(customerCanvas, {
  type: 'pie',
  data: cusData,
  options: pieOptions      
});

var inspectionChart = new Chart(inspectionCanvas, {
  type: 'pie',
  data: insData,
  options: pieOptions      
});


var partnerChart = new Chart(partnerCanvas, {
  type: 'pie',
  data: parData,
  options: pieOptions      
});

var publicWorkChart = new Chart(publicWorkCanvas, {
  type: 'pie',
  data: pubData,
  options: pieOptions      
});

var jobMasterChart = new Chart(jobMasterCanvas, {
  type: 'pie',
  data: jobData,
  options: pieOptions      
});

var assignmentMasterChart = new Chart(assignmentMasterCanvas, {
  type: 'pie',
  data: assData,
  options: pieOptions      
});

var employeeMasterChart = new Chart(employeeMasterCanvas, {
  type: 'pie',
  data: empData,
  options: pieOptions      
});

</script>
</body>
</html>