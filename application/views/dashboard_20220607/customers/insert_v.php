<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">新規OB顧客</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">管理ページ / 新規OB顧客</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="<?=base_url()?>dashboard/insert_customers" method="post" enctype="multipart/form-data">
                        <input type="text" name="gid" value="<?=$for_id->gid+1?>" hidden>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-6" style="margin-bottom:15px;">
                              <label for="latitude">緯度</label>
                              <input type="text" class="form-control" name="latitude" id="latitude" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="col-md-6">
                              <label for="longitude">経度</label>
                              <input type="text" class="form-control" name="longitude" id="longitude" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name">氏名 (漢字)</label>
                              <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_kana">氏名 (かな)</label>
                              <input type="text" class="form-control" name="name_kana" id="name_kana" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="joint_name">連名</label>
                          <input type="text" class="form-control col-md-6" name="joint_name" id="joint_name" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-2" style="margin-bottom:15px;">
                              <label for="zip_code">〒</label>
                              <input type="text" class="form-control" name="zip_code" id="zip_code" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="col-md-4">
                              <label for="prefecture">都道府県</label>
                              <input type="text" class="form-control" name="prefecture" id="prefecture" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="col-md-6">
                              <label for="address">住所</label>
                              <input type="text" class="form-control" name="address" id="address" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="building_name">建物名・部屋番号</label>
                          <input type="text" class="form-control col-md-6" name="building_name" id="building_name" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="phone_number">電話番号</label>
                          <input type="text" class="form-control col-md-6" name="phone_number" id="phone_number" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group dropdowns">
                          <label for="kinds">種別</label>
                          <!-- <input type="text" class="form-control col-md-6" name="kinds" id="kinds" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control col-md-6" name="kinds">
                            <option selected disabled value="">選択してください</option>
                            <option value="SOB">SWEET HOME</option>
                            <option value="ビリーブの家">ビリーブの家</option>
                            <option value="リフォーム工事">リフォーム工事</option>
                            <option value="メンテナンス工事">メンテナンス工事</option>
                            <option value="新築工事">新築工事</option>
                            <option value="OB">OB</option>
                            <option value="その他">その他</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="first_contact">初回接点</label>
                          <input type="text" class="form-control col-md-6" name="first_contact" id="first_contact" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="calendar">初回年月日</label>
                          <input type="date" class="form-control col-md-6" name="calendar" id="calendar" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="email">メールアドレス</label>
                          <input type="text" class="form-control col-md-6" name="email" id="email" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="birthday">生年月日</label>
                          <input type="date" class="form-control col-md-6" name="birthday" id="birthday" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group dropdowns">
                          <label for="sales_staff">営業担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="sales_staff" id="sales_staff" aria-describedby="helpId" placeholder="">
                         -->
                          <select class="form-control" name="sales_staff">
                            <option value="">選択してください</option>
                            <?php 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="pic_construction">工務担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="pic_construction" id="pic_construction" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control" name="pic_construction">
                            <option value="">選択してください</option>
                            <?php 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group dropdowns">
                          <label for="coordinator">コーデ担当</label>
                          <!-- <input type="text" class="form-control col-md-6" name="coordinator" id="coordinator" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control" name="coordinator">
                            <option value="">選択してください</option>
                            <?php 
                            if(!empty($data))
                            {
                              foreach($data as $row)
                              { 
                                echo '<option value="'.$row->姓.'">'.$row->姓.' '.$row->名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group col-md-12">
                          <label for="marks">備考</label>
                          <textarea type="text" class="form-control col-md-12" name="marks" id="marks" aria-describedby="helpId" placeholder="" rows="5"></textarea>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_wife">奥様名前</label>
                              <input type="text" class="form-control" name="name_wife" id="name_wife" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_wife_kana">奥様名前 (かな)</label>
                              <input type="text" class="form-control" name="name_wife_kana" id="name_wife_kana" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="date_birth_wife">奥様生年月日</label>
                          <input type="date" class="form-control col-md-6" name="date_birth_wife" id="date_birth_wife" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child1">お子様1名前</label>
                              <input type="text" class="form-control" name="name_child1" id="name_child1" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child1_kana">お子様1名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child1_kana" id="name_child1_kana" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child1">お子様1生年月日</label>
                          <input type="date" class="form-control col-md-6" name="birth_child1" id="birth_child1" aria-describedby="helpId" placeholder="">
                        </div>
                        
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child2">お子様2名前</label>
                              <input type="text" class="form-control" name="name_child2" id="name_child2" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child2_kana">お子様2名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child2_kana" id="name_child2_kana" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child2">お子様2生年月日</label>
                          <input type="date" class="form-control col-md-6" name="birth_child2" id="birth_child2" aria-describedby="helpId" placeholder="">
                        </div>

                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child3">お子様3名前</label>
                              <input type="text" class="form-control" name="name_child3" id="name_child3" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="padding: 0;margin-bottom:15px;">
                          <div class="form-row">
                            <div class="col-md-12" style="margin-bottom:15px;">
                              <label for="name_child3_kana">お子様3名前 (かな)</label>
                              <input type="text" class="form-control" name="name_child3_kana" id="name_child3_kana" aria-describedby="helpId" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="birth_child3">お子様3生年月日</label>
                          <input type="date" class="form-control col-md-6" name="birth_child3" id="birth_child3" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="longterm1" name="longterm" value="1"  >
                                <label for="longterm">長期優良住宅</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="warranty1" name="warranty" value="1"  >
                                <label for="warranty">設備10年保証加入</label>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="delivery_date">引渡日</label>
                          <input type="date" class="form-control col-md-6" name="delivery_date" id="input_delivery_date" aria-describedby="helpId" placeholder="" onchange="handler(event);">
                        </div>

                        <div class="form-group">
                          <label for="inspection_time1">定期点検項目（3ヶ月）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time1" id="inspection_time1" aria-describedby="helpId" placeholder="" readonly>
                        </div>
                        
                        <div class="form-group">
                          <label for="inspection_time2">定期点検項目（6ヶ月）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time2" id="inspection_time2" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time3">定期点検項目（1年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time3" id="inspection_time3" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time4">定期点検項目（2年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time4" id="inspection_time4" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time5">定期点検項目（3年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time5" id="inspection_time5" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time6">定期点検項目（5年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time6" id="inspection_time6" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group">
                          <label for="inspection_time7">定期点検項目（10年）</label>
                          <input type="date" class="form-control col-md-6" name="inspection_time7" id="inspection_time7" aria-describedby="helpId" placeholder="" readonly>
                        </div>

                        <div class="form-group dropdowns">
                          <label for="bank">借入金融機関</label>
                          <!-- <input type="text" class="form-control col-md-6" name="bank" id="bank" aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control" name="bank">
                            <option value="">選択してください</option>
                            <?php 
                            if(!empty($for_bank))
                            {
                              foreach($for_bank as $row)
                              { 
                                echo '<option value="'.$row->金融機関名.'">'.$row->金融機関名.'</option>';
                              }
                            }
                            else
                            {
                              echo '<option value="">社員データなし</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="insurance_company">火災保険</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company1" name="insurance_company" value="自社"  >
                                <label for="insurance_company1" class="form-check-label">自社</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="insurance_company2" name="insurance_company" value="他社"  >
                                <label for="insurance_company2" class="form-check-label">他社</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="insurance">火災保険備考</label>
                          <input type="text" class="form-control col-md-6" name="insurance" id="insurance" aria-describedby="helpId" placeholder="">
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="subsidy1" name="subsidy" value="1"  >
                                <label for="subsidy">グリーン化事業補助金</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="ground_improve1" name="ground_improve" value="1"  >
                                <label for="ground_improve">地盤改良工事</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="thanksgiving">感謝祭</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="thanksgiving1" name="thanksgiving" value="〇"  >
                                <label for="thanksgiving1" class="form-check-label">〇</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="thanksgiving2" name="thanksgiving" value="✕"  >
                                <label for="thanksgiving2" class="form-check-label">✕</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="dm">イベントDM</label>
                          <!-- <input type="text" class="form-control col-md-6" name="dm" id="dm" aria-describedby="helpId"
                            placeholder=""> -->
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="dm1" name="dm" value="〇"  >
                                <label for="dm1" class="form-check-label">〇</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="dm2" name="dm" value="✕"  >
                                <label for="dm2" class="form-check-label">✕</label>
                            </div>
                          </div>
                        </div>
                        <input name="" id="" class="btn btn-primary" type="submit" value="追加">
                    </form>
                </div>
            </div>
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
<script>
    function handler(e){
      var input = document.getElementById('input_delivery_date').value;
      var input2 = document.getElementById('input_delivery_date').value;
      var input3 = document.getElementById('input_delivery_date').value;
      var input4 = document.getElementById('input_delivery_date').value;
      var input5 = document.getElementById('input_delivery_date').value;
      var input6 = document.getElementById('input_delivery_date').value;
      var input7 = document.getElementById('input_delivery_date').value;

      var manDate = moment(input).local();
      var manDate2 = moment(input2).local();
      var manDate3 = moment(input3).local();
      var manDate4 = moment(input4).local();
      var manDate5 = moment(input5).local();
      var manDate6 = moment(input6).local();
      var manDate7 = moment(input7).local();

      document.getElementById('inspection_time1').value = manDate.add(3, 'M').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time2').value = manDate2.add(6, 'M').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time3').value = manDate3.add(1, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time4').value = manDate4.add(2, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time5').value = manDate5.add(3, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time6').value = manDate6.add(5, 'y').format('YYYY-MM-DD'); 
      document.getElementById('inspection_time7').value = manDate7.add(10, 'y').format('YYYY-MM-DD'); 
    }
</script>
</body>
</html>