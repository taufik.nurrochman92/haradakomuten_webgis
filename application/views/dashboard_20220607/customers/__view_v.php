<?php $this->load->view('dashboard/dashboard_header');?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">OB顧客詳細情報</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">管理ページ / OB顧客詳細情報</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content view_custom">
    <div class="container-fluid">
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 mx-auto">
          <div class="card">
            <div class="card-body">
              <div class="button" style="margin-bottom:20px;">
                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_customers/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_customers/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
              </div>
              <!-- <div class="col-md-6 mx-auto" style="text-align:center;">
                <label for="customer_code">顧客コード</label>
                <p>
                  <?=$data->customer_code;?>
                </p>
              </div> -->
              <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                <div class="form-row">
                  <div class="col-md-6" style="margin-bottom:15px;">
                    <div class="flex-box double">
                      <div class="label-view">
                        <label for="last_name">氏名 (姓)</label>
                      </div>
                      <div class="text-view">
                          <p>
                            <?=$data->sei;?>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="flex-box double">
                      <div class="label-view">
                      <label for="first_name">氏名 (名)</label>
                      </div>
                      <div class="text-view">
                          <p>
                          <?=$data->名;?>
                          </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                <div class="form-row">
                  <div class="col-md-6" style="margin-bottom:15px;">
                  <div class="flex-box double">
                      <div class="label-view">
                      <label for="last_furigana">氏名 (せい)</label>
                      </div>
                      <div class="text-view">
                          <p>
                          <?=$data->last_furigana;?>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="flex-box double">
                      <div class="label-view">
                      <label for="first_furigana">氏名 (めい)</label>
                      </div>
                      <div class="text-view">
                          <p>
                          <?=$data->first_furigana;?>
                          </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                <div class="form-row">
                  <div class="col-md-6" style="margin-bottom:15px;">
                  <div class="flex-box double">
                      <div class="label-view">
                        <label for="latitude">緯度</label>
                      </div>
                      <div class="text-view">
                          <p>
                          <?=$data->b;?>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="flex-box double">
                      <div class="label-view">
                      <label for="longitude">経度</label>
                      </div>
                      <div class="text-view">
                          <p>
                          <?=$data->l;?>
                          </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                    <div class="label-view">
                      <label for="address_1">住所1</label>
                    </div>
                    <div class="text-view">
                        <p>
                        <span>〒<?=$data->zip_code1;?></span>
                        &nbsp;&nbsp;
                        <span><?=$data->住所?></span>
                        <span><?=$data->address_line1;?></span>
                        </p>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                    <div class="label-view">
                      <label for="phone_number1">電話番号1</label>
                    </div>
                    <div class="text-view">
                        <p>
                        <?=$data->電話番号;?>
                        </p>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <div class="flex-box">
                  <div class="label-view">
                    <label for="address_2">住所2</label>
                  </div>
                  <div class="text-view">
                      <p>
                        <span>〒<?=$data->zip_code2;?></span>
                        &nbsp;&nbsp;
                        <span><?=$data->address_line2;?></span>
                        <span><?=$data->住所_2;?></span>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="phone_number2">電話番号2</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->電話番号_2;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="cell_phone">携帯電話</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->cell_phone;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="email">メールアドレス</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->メールアドレス;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="birthday">生年月日</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->誕生日;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="fb_account">FBアカウント</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->fb_account;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="twitter_account">Twitterアカウント</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->twitter_account;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="line_account">LINEアカウント</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->line_account;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="instagram_account">Instagramアカウント</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->instagram_account;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="thanksgiving">感謝祭</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->thanksgiving;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="dm">DM</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->dm;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="introducer">紹介者</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->introducer;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="mid_and_year_end_gift">お中元・お歳暮</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->mid_and_year_end_gift;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="contract_date">契約日</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->完了検査;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="delivery_date">引渡日</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->引渡;?>
                      </p>
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="padding: 0;margin-bottom:15px;">
                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="flex-box double">
                              <div class="label-view">
                              <label for="construction_start_date">工期開始日</label>
                              </div>
                              <div class="text-view">
                                  <p>
                                  <?=$data->construction_start_date;?>
                                  </p>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="flex-box double">
                              <div class="label-view">
                              <label for="construction_end_date">工期終了日</label>
                              </div>
                              <div class="text-view">
                                  <p>
                                  <?=$data->construction_end_date;?>
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="blog_url">ブログカテゴリURL</label>
                  </div>
                  <div class="text-view">
                      <p>
                      <?=$data->blog_url;?>
                      </p>
                  </div>
                </div>
              </div>

              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="contract">契約書</label>
                  </div>
                  <div class="text-view">
                  <?php if(!empty($data->contract)) : ?>
                <img src="<?= base_url() ?>uploads/customer/<?=$data->contract;?>" class="img-fluid">
                <?php else : ?>
                <img src="<?= base_url() ?>/assets/dashboard/img/image-not-found-1.png" class="img-fluid">
                <?php endif;?>
                  </div>
                </div>
                </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="drawing">図面</label>
                  </div>
                  <div class="text-view">
                  <?php if(!empty($data->contract)) : ?>
                <img src="<?= base_url() ?>uploads/customer/<?=$data->drawing;?>" class="img-fluid">
                <?php else : ?>
                <img src="<?= base_url() ?>/assets/dashboard/img/image-not-found-1.png" class="img-fluid">
                <?php endif;?>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="photo">写真</label>
                  </div>
                  <div class="text-view">
                  <?php if(!empty($data->contract)) : ?>
                <img src="<?= base_url() ?>uploads/customer/<?=$data->photo;?>" class="img-fluid">
                <?php else : ?>
                <img src="<?= base_url() ?>/assets/dashboard/img/image-not-found-1.png" class="img-fluid">
                <?php endif;?>
                  </div>
                </div>
              </div>

              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="sales_staff">営業担当</label>
                  </div>
                  <div class="text-view">
                  <p>
                    <?=$data->sales_staff;?>
                  </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="pic_construction">工務担当</label>
                  </div>
                  <div class="text-view">
                  <p>
                  <?=$data->pic_construction;?>
                  </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="coordinator">コーディネート担当</label>
                  </div>
                  <div class="text-view">
                  <p>
                  <?=$data->coordinator;?>
                  </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <div class="flex-box">
                  <div class="label-view">
                  <label for="kinds">種別</label>
                  </div>
                  <div class="text-view">
                  <p>
                  <?=$data->種別;?>
                  </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <p>
                  <?=$data->ten_year_warranty;?>
                </p>
              </div>
              <div class="form-group">
                <p>
                  <?=$data->thermo_hygrometer;?>
                </p>
              </div>
              <div class="form-group">
                <p>
                  <?=$data->solar_power;?>
                </p>
              </div>
              <div class="form-group">
                <p>
                  <?=$data->long_term;?>
                </p>
              </div>
              <div class="button">
                <a class="btn btn-primary" href="<?=base_url()?>dashboard/edit_customers/<?=$data->gid?>" role="button"><i class="fa fa-edit"></i></a>
                <a class="btn btn-danger delete" href="<?=base_url()?>dashboard/delete_customers/<?=$data->gid?>" role="button"><i class="fa fa-trash"></i></a>
              </div>
            </div>
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<?php $this->load->view('dashboard/dashboard_footer');?>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/ajaxzip3.js"></script>
</body>

</html>