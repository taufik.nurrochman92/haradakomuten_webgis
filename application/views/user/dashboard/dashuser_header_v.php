<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- TITLE -->
        <title></title>
        <!-- Keywords & Description -->
        <meta name="description" content="">
        <meta name="keywords" content="">
        <!-- End keywords & Description -->
        <!-- Favicon -->
        <link rel="icon" href="assets/img/icon/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon" />
        <!-- Link Base CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0-6/css/all.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/dashboard/css/style.css">
        <link rel="stylesheet" media="print" type="text/css" href="<?=base_url()?>assets/dashboard/css/print.css">
        <!-- META OG-->
        <meta property="og:url" content="" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:site_name" content="" />
        <meta property="og:image" content="" />
        <style>
            .categori {
                background-color: #FFF;
                margin-bottom: 15px;
                padding: 2px;
                text-align: center;
                font-size: 20px;
                font-weight: bold;
            }

            .filesList {
                display:none;
            }

            .filesList.show {
                display:block;
            }

            strong {
                word-break: break-all;
            }

            .backButton {
                position: fixed;
                top: 30px;
                left: 15px;
                width: 63px;
                height: 25px;
                background-color: #7CC046;
                color: #FFF;
                font-size: 15px;
                font-weight: bold;
                box-shadow: 0px 6px 0px 0px rgb(191 131 80 / 40%);
            }


            .backButton a i {
                margin-right: 5px;
            }

            button:disabled,
            button[disabled]{
                background-color: #CCC !important;
                pointer-events: none;
                cursor: default;
            }
        </style>
    </head>