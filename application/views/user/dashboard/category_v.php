<?php $this->load->view('user/dashboard/dashuser_header_v');?>
<body onload="StartTimers();" onmousemove="ResetTimers();">
        <!-- HEADER -->
        <header id="header">
            <div class="logo">
                <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
            </div>
            <div class="backButton">
                <a href="<?=base_url()?>user/dashboard"><i class="fa fa-reply" aria-hidden="true"></i>戻る</a>
            </div>
        </header>
        <!-- MAIN CONTENT -->
        <div class="space-top"></div>
        <main class="category width">
            <ul class="steps">
                <li class="active">
                    <span>1</span>
                    <p>担当工事選択</p>
                </li>
                <li>
                    <span>2</span>
                    <p>項目選択</p>
                </li>
                <li>
                    <span>3</span>
                    <p>写真投稿</p>
                </li>
                <li>
                    <span>4</span>
                    <p>完了</p>
                </li>
            </ul>
            <h2 class="customer_name"><?=$customer?> 様邸</h2>
            <div class="inner">
                <ul>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1001" id="brown">木工事<br>土台</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1002" id="brown">木工事<br>建て方</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1003" id="brown">木工事<br>断熱前</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1004" id="brown">木工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1005" id="orange">着工前<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1006" id="orange">地盤<br>改良</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1007" id="green">基礎<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1008" id="blue">給排水<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1009" id="pink">足場<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1010" id="red">板金<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1011" id="red">瓦屋根<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1012" id="ocean">防水<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1013" id="grey">電気<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1014" id="purple">外壁<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1015" id="purple">防蟻<br>工事</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>user/select_category/1016" id="purple">断熱<br>工事</a>
                    </li>
                </ul>
            </div>
        </main>
        <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
    </body>
</html>