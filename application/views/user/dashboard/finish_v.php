<?php $this->load->view('user/dashboard/dashuser_header_v');?>
<body onload="StartTimers();" onmousemove="ResetTimers();">
        <!-- HEADER -->
        <header id="header">
            <div class="logo">
                <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
            </div>
        </header>
        <!-- MAIN CONTENT -->
        <div class="space-top"></div>
        <main class="complete width">
            <ul class="steps">
                <li>
                    <span>1</span>
                    <p>担当工事選択</p>
                </li>
                <li>
                    <span>2</span>
                    <p>項目選択</p>
                </li>
                <li>
                    <span>3</span>
                    <p>写真投稿</p>
                </li>
                <li class="active">
                    <span>4</span>
                    <p>完了</p>
                </li>
            </ul>
            <div class="inner">
                <h3>工事現場写真の登録
                    <br>ありがとうございました</h3>
                <div class="box" id="<?=$color?>">
                    <div class="img">
                    <?php
                        if(!empty($photo_images1)) :
                    ?>
                        <img src="<?=base_url()?>user/view_files_images/<?=$photo_images1?>" alt="">
                    <?php
                        endif;
                        if(!empty($photo_images2)) :
                    ?>
                        <img src="<?=base_url()?>user/view_files_images/<?=$photo_images2?>" alt="">
                    <?php
                        endif;
                        if(!empty($photo_images3)) :
                    ?>
                        <img src="<?=base_url()?>user/view_files_images/<?=$photo_images3?>" alt="">
                    <?php
                        endif;
                        if(!empty($photo_images4)) :
                    ?>
                        <img src="<?=base_url()?>user/view_files_images/<?=$photo_images4?>" alt="">
                    <?php
                        endif;
                    ?>
                    </div>
                    <table>
                        <tr>
                            <td>現場</td>
                            <td>:</td>
                            <td><?=$customer?> 様邸</td>
                        </tr>
                        <tr>
                            <td>工事名</td>
                            <td>:</td>
                            <td><?=$category_name?></td>
                        </tr>
                        <tr>
                            <td>小項目</td>
                            <td>:</td>
                            <td>
                                <ul>
                                    <?php
                                        if(!empty($selection1)) :
                                    ?>
                                        <li><?=$selection_name?>
                                            <br><?=$selection1?></li>
                                    <?php
                                        endif;
                                    ?>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>業者</td>
                            <td>:</td>
                            <td><?=$category_name?></td>
                        </tr>
                        <tr>
                            <td>緯度</td>
                            <td>:</td>
                            <td><?=$lat?></td>
                        </tr>
                        <tr>
                            <td>経度</td>
                            <td>:</td>
                            <td><?=$lang?></td>
                        </tr>
                    </table>
                </div>
                <div class="btnBox">
                    <a href="<?=base_url()?>user/dashboard">現場・機能選択に戻る</a>
                </div>
            </div>
        </main>
        <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
    </body>
</html>