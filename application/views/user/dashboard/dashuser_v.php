<?php $this->load->view('user/dashboard/dashuser_header_v');?>
<body style="background-image: url(<?=base_url()?>assets/dashboard/img/bg-body.png);" onload="StartTimers();" onmousemove="ResetTimers();">
        <!-- HEADER -->
        <header id="header">
            <div class="logo">
                <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
            </div>
        </header>
        <!-- MAIN CONTENT -->
        <div class="space-top"></div>
        <main class="site-function width">
            <form action="<?=base_url()?>user/menu" method="post" enctype="multipart/form-data">
                <div class="inner site">
                    <h2>現場選択</h2>
                    <select class="form-select" name="customer" id="customer">
                        <option value="">現場を選択してください</option>
                        <?php 
                        if(!empty($customers))
                        {
                            foreach($customers as $row)
                            { 
                            echo '<option value="'.$row->construct_id.';'.$row->cust_name.'">'.$row->cust_name.' - '.$row->const_no.' - '.$row->kinds.'</option>';
                            }
                        }
                        else
                        {
                            echo '<option value="">社員データなし</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="inner function">
                    <h2>機能選択</h2>
                    <div class="btnBox">
                        <!-- <a class="list disable" id="batten1">図面<br>確認</a>
                        <a class="upload disable" id="batten2">現場写真<br>投稿</a> -->
                        <button name="btn_menu" value="list" class="list disable" id="batten1">図面
                            <br>確認</button>
                        <button name="btn_menu" value="upload" class="upload disable" id="batten2">現場写真
                            <br>投稿</button>
                    </div>
                </div>
            </form>
            <div class="butLogout" style="
                display: flex;
                align-items: center;
                justify-content: center;
                margin-top: 90px;
            ">
                <a href="<?=base_url()?>user/logout" style="
                width: 30.1%;
                height: 44px;
                background-color: #f92626;
                display: flex;
                justify-content: center;
                align-items: center;
                text-align: center;
                font-size: 20px;
                color: #FFF;
                font-weight: bold;
                border-radius: 3px;
                box-shadow: 0px 6px 0px 0px rgb(191 131 80 / 40%);
            ">ログアウト</a>
            </div>
        </main>
        <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
        <script>
            $( document ).ready(function() {
                // bind change event to select
                $('#customer').on('change', function () {
                    var str = $(this).val(); 
                    var element = document.getElementById( 'batten1' );
                    var element2 = document.getElementById( 'batten2' );
                    if (str === "") { 
                        element.classList.add("disable");
                        element2.classList.add("disable");
                    }
                    else
                    {
                        element.classList.remove("disable");
                        element2.classList.remove("disable");
                    }
                });
            });  
        </script>
        <script type="text/javascript" language="javascript" >
            // $(window).on("pageshow", function() {
            //     $('select').prop('selectedIndex', function () {
            //         var selected = $(this).children('[selected]').index();
            //         return selected != -1 ? selected : 0;
            //     });
            // });

            var select = document.querySelector(".form-select");
            var selectOption = select.options[select.selectedIndex];
            var lastSelected = localStorage.getItem('select');

            if(lastSelected) {
                select.value = lastSelected; 
            }

            select.onchange = function () {
                lastSelected = select.options[select.selectedIndex].value;
                console.log(lastSelected);
                localStorage.setItem('select', lastSelected);
            }
        </script>
    </body>
</html>