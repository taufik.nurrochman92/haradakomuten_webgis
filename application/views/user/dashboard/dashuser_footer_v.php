<!-- JAVASCRIPT -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/vendor/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/vendor/inlineSVG.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/vendor/aos.js"></script>
<script>
inlineSVG.init({
    svgSelector: 'img.svg', // the class attached to all images that should be inlined
    initClass: 'js-inlinesvg', // class added to <html>
}, function() {
    console.log('All SVGs inlined');
});
AOS.init({
    offset: 100,
    once: true,
    duration: 1000,
})
</script>
<script type="text/javascript">
    // Set timeout variables.
    var timoutNow = 300000; // Timeout of 30 mins - time in ms
    var logoutUrl = "<?php echo base_url('user/logout') ?>"; // URL to logout page.

    var timeoutTimer;

    // Start timer
    function StartTimers() {
      timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
    }

    // Reset timer
    function ResetTimers() {
      clearTimeout(timeoutTimer);
      StartTimers();
    }

    // Logout user
    function IdleTimeout() {
      window.location = logoutUrl;
    }
 </script>
<script type="text/javascript">
        //    onload=function(){
        //        var e=document.getElementById("refreshed");
        //        if(e.value=="no")e.value="yes";
        //        else{e.value="no";location.reload();}
        //    }
</script>
<script type="text/javascript" src="<?=base_url()?>assets/dashboard/js/main.js"></script>
<!-- SCRIPT COOKIE POLICY BOX -->