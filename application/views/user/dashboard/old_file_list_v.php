<?php $this->load->view('user/dashboard/dashuser_header_v');?>

<body onload="StartTimers();" onmousemove="ResetTimers();">
    <!-- HEADER -->
    <header id="header">
        <div class="logo">
            <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
        </div>
        <div class="backButton">
            <a href="<?=base_url()?>user/dashboard"><i class="fa fa-reply" aria-hidden="true"></i>戻る</a>
        </div>
    </header>
    <!-- MAIN CONTENT -->
    <div class="space-top"></div>
    <main class="construction-list width">
        <h2 class="customer_name">
            <?=$customer?> 様邸
        </h2>

        <div class="inner">
            <ul>
                <?php 
                    if(count($file_project) > 0):
                        foreach($file_project as $key => $value):

                        if(!empty($value->category_name1bluea)) :
                    ?>
                <div class="filesList" id="1">
                    <p class="categori">
                        <?=$value->category_name1bluea?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1bluea) && (strpos($value->co_owner1bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2bluea) && (strpos($value->co_owner2bluea, $aff)!== false)) :
                    ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3bluea) && (strpos($value->co_owner3bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4bluea) && (strpos($value->co_owner4bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5bluea) && (strpos($value->co_owner5bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6bluea) && (strpos($value->co_owner6bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7bluea) && (strpos($value->co_owner7bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8bluea) && (strpos($value->co_owner8bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9bluea) && (strpos($value->co_owner9bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10bluea) && (strpos($value->co_owner10bluea, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10bluea?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10bluea?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluea?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluea?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10bluea?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>
                <?php
                        if(!empty($value->category_name1blueb)) :
                    ?>
                <div class="filesList" id="2">
                    <p class="categori">
                        <?=$value->category_name1blueb?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1blueb) && (strpos($value->co_owner1blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2blueb) && (strpos($value->co_owner2blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3blueb) && (strpos($value->co_owner3blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4blueb) && (strpos($value->co_owner4blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5blueb) && (strpos($value->co_owner5blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5lueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6blueb) && (strpos($value->co_owner6blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7blueb) && (strpos($value->co_owner7blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8blueb) && (strpos($value->co_owner8blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9blueb) && (strpos($value->co_owner9blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10blueb) && (strpos($value->co_owner10blueb, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10blueb?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10blueb?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10blueb?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10blueb?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10blueb?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>

                <?php
                        if(!empty($value->category_name1bluec)) :
                    ?>
                <div class="filesList" id="3">
                    <p class="categori">
                        <?=$value->category_name1bluec?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1bluec) && (strpos($value->co_owner1bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2bluec) && (strpos($value->co_owner2bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3bluec) && (strpos($value->co_owner3bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4bluec) && (strpos($value->co_owner4bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5bluec) && (strpos($value->co_owner5bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6bluec) && (strpos($value->co_owner6bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7bluec) && (strpos($value->co_owner7bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8bluec) && (strpos($value->co_owner8bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9bluec) && (strpos($value->co_owner9bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10bluec) && (strpos($value->co_owner10bluec, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10bluec?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10bluec?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluec?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluec?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10bluec?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                        endif;
                        ?>
                </div>

                <?php
                        if(!empty($value->category_name1blued)) :
                    ?>
                <div class="filesList" id="4">
                    <p class="categori">
                        <?=$value->category_name1blued?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1blued) && (strpos($value->co_owner1blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2blued) && (strpos($value->co_owner2blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3blued) && (strpos($value->co_owner3blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4blued) && (strpos($value->co_owner4blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5blued) && (strpos($value->co_owner5blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6blued) && (strpos($value->co_owner6blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7blued) && (strpos($value->co_owner7blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8blued) && (strpos($value->co_owner8blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9blued) && (strpos($value->co_owner9blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10blued) && (strpos($value->co_owner10blued, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10blued?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10blued?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10blued?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10blued?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10blued?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>

                <?php
                        if(!empty($value->category_name1bluee)) :
                    ?>
                <div class="filesList" id="5">
                    <p class="categori">
                        <?=$value->category_name1bluee?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1bluee) && (strpos($value->co_owner1bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2bluee) && (strpos($value->co_owner2bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3bluee) && (strpos($value->co_owner3bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4bluee) && (strpos($value->co_owner4bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5bluee) && (strpos($value->co_owner5bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6bluee) && (strpos($value->co_owner6bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7bluee) && (strpos($value->co_owner7bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8bluee) && (strpos($value->co_owner8bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9bluee) && (strpos($value->co_owner9bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10bluee) && (strpos($value->co_owner10bluee, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10bluee?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10bluee?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluee?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluee?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10bluee?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>

                <?php
                        if(!empty($value->category_name1bluef)) :
                    ?>
                <div class="filesList" id="6">
                    <p class="categori">
                        <?=$value->category_name1bluef?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1bluef) && (strpos($value->co_owner1bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2bluef) && (strpos($value->co_owner2bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3bluef) && (strpos($value->co_owner3bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4bluef) && (strpos($value->co_owner4bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5bluef) && (strpos($value->co_owner5bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6bluef) && (strpos($value->co_owner6bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7bluef) && (strpos($value->co_owner7bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8bluef) && (strpos($value->co_owner8bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9bluef) && (strpos($value->co_owner9bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10bluef) && (strpos($value->co_owner10bluef, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10bluef?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10bluef?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluef?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10bluef?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10bluef?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>

                <?php
                        if(!empty($value->category_name1blueg)) :
                    ?>
                <div class="filesList" id="7">
                    <p class="categori">
                        <?=$value->category_name1blueg?>
                    </p>
                    <?php
                            if(!empty($value->constract_drawing1blueg) && (strpos($value->co_owner1blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing1blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing1blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item1blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing1blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing1blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing1blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing2blueg) && (strpos($value->co_owner2blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing2blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing2blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item2blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing2blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing2blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing2blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing3blueg) && (strpos($value->co_owner3blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing3blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing3blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item3blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing3blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing3blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing3blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing4blueg) && (strpos($value->co_owner4blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing4blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing4blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item4blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing4blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing4blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing4blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing5blueg) && (strpos($value->co_owner5blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing5blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing5blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item5blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing5blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing5blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing5blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing6blueg) && (strpos($value->co_owner6blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing6blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing6blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item6blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing6blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing6blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing6blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing7blueg) && (strpos($value->co_owner7blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing7blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing7blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item7blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing7blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing7blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing7blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing8blueg) && (strpos($value->co_owner8blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing8blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing8blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item8blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing8blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing8blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing8blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing9blueg) && (strpos($value->co_owner9blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing9blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing9blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item9blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing9blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing9blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing9blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            if(!empty($value->constract_drawing10blueg) && (strpos($value->co_owner10blueg, $aff)!== false)) :
                        ?>
                    <li>
                        <div class="content">
                            <div class="img">
                                <img src="<?=base_url()?>assets/dashboard/img/PDF_file_icon.svg" alt="">
                            </div>
                            <div class="text">
                                <div>
                                    <p>ファイル名</p>
                                    <p>
                                        <strong>
                                            <?=$value->constract_drawing10blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>更新日時</p>
                                    <p>
                                        <strong>
                                            <?=$value->date_insert_drawing10blueg?>
                                        </strong>
                                    </p>
                                </div>
                                <div>
                                    <p>コメント</p>
                                    <p>
                                        <?=$value->desc_item10blueg?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="btnBox">
                            <a href="<?=base_url()?>user/view_files/<?=$value->constract_drawing10blueg?>" target="popup" onclick="window.open('<?=base_url()?>user/view_files/<?=$value->constract_drawing10blueg?>','name','width=1366,height=768')">閲覧</a>
                            <a href="<?=base_url()?>user/download/<?=$value->constract_drawing10blueg?>">ダウンロード</a>
                        </div>
                    </li>
                    <?php
                            endif;
                            ?>
                </div>
                    <?php
                        endif;
                        ?>
                <?php    
                        endforeach;
                    endif;
                    ?>
            </ul>
        </div>
    </main>
    <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
    <script>
        $(document).ready(function () {
            $(".filesList:has(li)").addClass("show");
        });

    </script>
</body>

</html>