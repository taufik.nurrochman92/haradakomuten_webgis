<?php $this->load->view('user/dashboard/dashuser_header_v');?>
<body onload="StartTimers();" onmousemove="ResetTimers();">
        <!-- HEADER -->
        <header id="header">
            <div class="logo">
                <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
            </div>
        </header>
        <!-- MAIN CONTENT -->
        <div class="space-top"></div>
        <main class="item-selection photo-selection width">
            <ul class="steps">
                <li>
                    <span>1</span>
                    <p>担当工事選択</p>
                </li>
                <li>
                    <span>2</span>
                    <p>項目選択</p>
                </li>
                <li class="active">
                    <span>3</span>
                    <p>写真投稿</p>
                </li>
                <li>
                    <span>4</span>
                    <p>完了</p>
                </li>
            </ul>
            <h2 class="customer_name"><?=$customer?> 様邸</h2>
            <div class="inner">
                <form action="<?=base_url()?>user/check_data" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="customer" value="<?=$customer?>">
                    <input type="hidden" name="code_project" value="<?=$code_project?>">
                    <input type="hidden" name="color" value="<?=$color?>">
                    <input type="hidden" name="joblist_files" value="<?=$joblist_files?>">
                    <input type="hidden" name="category_name" value="<?=$category_name?>">
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lang" id="lang">
                    <input type="hidden" name="const_id" id="const_id" value="<?=$const_id?>">
                    <input type="hidden" name="upload_files" value="<?=$joblist_files?>">
                    <div class="box-item" id="<?=$color?>">
                        <h3 class="item_name"><?=$category_name?></h3>
                        <div class="box">
                        <?php
                            if($code_project == '1001') :
                                if(!empty($selection1)) :
                        ?>
                            <div class="item">
                                <p>土台</p>
                                <input type="hidden" name="selection_name" value="土台">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>床合板</p>
                                <input type="hidden" name="selection_name2" value="床合板">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1002') :
                        ?>
                            <div class="item">
                                <p>建て方</p>
                                <input type="hidden" name="selection_name" value="建て方">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1003') :
                                if(!empty($selection1)) :
                        ?>
                            <div class="item">
                                <p>構造金物</p>
                                <input type="hidden" name="selection_name" value="構造金物">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>下地工事</p>
                                <input type="hidden" name="selection_name2" value="下地工事">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection3)) :
                        ?>
                            <div class="item">
                                <p>サッシ工事</p>
                                <input type="hidden" name="selection_name3" value="サッシ工事">
                                <span><?=$selection3?></span>
                                <input type="hidden" name="selection3" value="<?=$selection3?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1004') :
                                if(!empty($selection1)) :
                        ?>
                            <div class="item">
                                <p>外部木工事</p>
                                <input type="hidden" name="selection_name" value="外部木工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>内部 木工事</p>
                                <input type="hidden" name="selection_name2" value="木工事">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1005') :
                                if(!empty($selection1)) :
                        ?>
                            <div class="item">
                                <p>現場調査</p>
                                <input type="hidden" name="selection_name" value="現場調査">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>地縄確認</p>
                                <input type="hidden" name="selection_name2" value="地縄確認">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1006') :
                        ?>
                            <div class="item">
                                <p>地盤改良</p>
                                <input type="hidden" name="selection_name" value="地盤改良">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1007') :
                        ?>
                            <div class="item">
                                <p>基礎工事</p>
                                <input type="hidden" name="selection_name" value="基礎工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1008') :
                        ?>
                            <div class="item">
                                <p>給排水</p>
                                <input type="hidden" name="selection_name" value="給排水">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1009') :
                        ?>
                            <div class="item">
                                <p>足場工事</p>
                                <input type="hidden" name="selection_name" value="足場工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1010') :
                                if(!empty($selection1)) :
                        ?>
                            <div class="item">
                                <p>屋根 防水下地</p>
                                <input type="hidden" name="selection_name" value="屋根 防水下地">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>屋根仕上げ</p>
                                <input type="hidden" name="selection_name2" value="屋根仕上げ">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection3)) :
                        ?>
                            <div class="item">
                                <p>瓦工事</p>
                                <input type="hidden" name="selection_name3" value="瓦工事">
                                <span><?=$selection3?></span>
                                <input type="hidden" name="selection3" value="<?=$selection3?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection4)) :
                        ?>
                            <div class="item">
                                <p>破風・樋</p>
                                <input type="hidden" name="selection_name4" value="破風・樋">
                                <span><?=$selection4?></span>
                                <input type="hidden" name="selection4" value="<?=$selection4?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1011') :
                                if(!empty($selection1)) :
                        ?>
                             <div class="item">
                                <p>屋根 防水下地</p>
                                <input type="hidden" name="selection_name" value="屋根 防水下地">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection2)) :
                        ?>
                            <div class="item">
                                <p>屋根仕上げ</p>
                                <input type="hidden" name="selection_name2" value="屋根仕上げ">
                                <span><?=$selection2?></span>
                                <input type="hidden" name="selection2" value="<?=$selection2?>">
                            </div>
                        <?php
                                endif;
                                if(!empty($selection3)) :
                        ?>
                            <div class="item">
                                <p>その他</p>
                                <input type="hidden" name="selection_name3" value="その他">
                                <span><?=$selection3?></span>
                                <input type="hidden" name="selection3" value="<?=$selection3?>">
                            </div>
                        <?php
                                endif;
                            endif;
                            if($code_project == '1012') :
                        ?>
                            <div class="item">
                                <p>防水工事</p>
                                <input type="hidden" name="selection_name" value="防水工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1013') :
                        ?>
                            <div class="item">
                                <p>電気工事</p>
                                <input type="hidden" name="selection_name" value="電気工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1014') :
                        ?>
                            <div class="item">
                                <p>外壁工事</p>
                                <input type="hidden" name="selection_name" value="外壁工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                            if($code_project == '1015') :
                        ?>
                            <div class="item">
                                <p>防蟻工事</p>
                                <input type="hidden" name="selection_name" value="防蟻工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>                    
                        <?php
                            endif;
                            if($code_project == '1016') :
                        ?>
                            <div class="item">
                                <p>防蟻工事</p>
                                <input type="hidden" name="selection_name" value="防蟻工事">
                                <span><?=$selection1?></span>
                                <input type="hidden" name="selection1" value="<?=$selection1?>">
                            </div>
                        <?php
                            endif;
                        ?>

                            <div class="photo-input">
                                <h3>写真<?=$joblist_files?>枚</h3>
                                <?php 
                                    if($joblist_files > 1) :
                                ?>
                                <div class="inputBtn">
                                    <!-- <div class="input camera">
                                        <input type="file" accept="image/*;capture=camera" id="image1" name="camera[]" multiple >
                                        <img src="<?=base_url()?>assets/dashboard/img/icon-camera.svg" alt="" class="img-camera">
                                    </div> -->
                                    <div class="input upload">
                                        <input type="file" class="file-input" id="image2" name="photo[]" multiple onchange="checkFiles(this.files); ValidateSingleInput(this);">
                                        <img src="<?=base_url()?>assets/dashboard/img/icon-images.svg" alt="" class="img-photo">
                                    </div>
                                </div>
                                <?php
                                    else :
                                ?>
                                <div class="inputBtn">
                                    <!-- <div class="input camera">
                                        <input type="file" accept="image/*;capture=camera" id="image1" name="camera">
                                        <img src="<?=base_url()?>assets/dashboard/img/icon-camera.svg" alt="" class="img-camera">
                                    </div> -->
                                    <div class="input upload">
                                        <input type="file" class="file-input" id="image2" name="photo" onchange="ValidateSingleInput(this);">
                                        <img src="<?=base_url()?>assets/dashboard/img/icon-images.svg" alt="" class="img-photo">
                                    </div>
                                </div>
                                <?php
                                    endif;
                                ?>
                                <div class="photo-selected" id="frames">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btnBox">
                        <button class="back" name="edit_btn" value="back">前の画面に戻る</button>
                        <button>写真投稿に進む</button>
                    </div>
                </form>
            </div>
            <!-- <div class="offcanvas offcanvas-bottom <?php if(!empty($showing)){ echo $showing;}?>" tabindex="-1" id="offcanvasBottom" aria-labelledby="offcanvasBottomLabel">
                <h3>GPSをオンにし、
                    <br>再度写真を撮影／登録しなおしてください</h3>
                <button type="button" data-bs-dismiss="offcanvas">前の画面に戻る</button>
            </div> -->
        </main>
        <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
        <script>
            window.onload = function(event) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(onGeoSuccess);
                } else {
                    alert('GeoLocation not supported or not allowed');
                }
            };


            function onGeoSuccess (position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;

                // NEXT 2 LINES WILL SET VALUE OF RESPECTIVE INPUTS
                document.getElementById('lat').value = lat;
                document.getElementById('lang').value = lng;

                // MAKE API CALL HERE, OR ANY OTHER NEXT STEPS
            }


            $(function() {
                // Multiple images preview in browser
                var imagesPreview = function(input, placeToInsertImagePreview) {

                    if (input.files) {
                        var filesAmount = input.files.length;

                        for (i = 0; i < filesAmount; i++) {
                            var reader = new FileReader();

                            reader.onload = function(event) {
                                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }

                            reader.readAsDataURL(input.files[i]);
                        }
                    }

                };

                $('#image1').on('change', function() {
                    imagesPreview(this, 'div.photo-selected');
                    $('div.photo-selected').html('');
                });
                
                $('#image2').on('change', function() {
                    imagesPreview(this, 'div.photo-selected');
                    $('div.photo-selected').html('');
                });
            });
        </script>
        <script>
            var filechose = <?php echo $joblist_files;?>;

            
            function checkFiles(files) {       
                if(files.length>filechose) {
                    alert("投稿容量を超えています。 投稿写真が減りました。");

                    let list = new DataTransfer;
                    for(let i=0; i<filechose; i++)
                    list.items.add(files[i]) 

                    document.getElementById('image2').files = list.files
                }       
            }
        </script>
        <script>
            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".JPEG"];    
            function ValidateSingleInput(oInput) {
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        
                        if (!blnValid) {
                            alert(sFileName + " は投稿不可能です。投稿可能なファイル種は " + _validFileExtensions.join(", "));
                            oInput.value = "";
                            return false;
                        }
                    }
                }
                return true;
            }
        </script>
    </body>
</html>