<?php $this->load->view('user/dashboard/dashuser_header_v');?>
<body onload="StartTimers();" onmousemove="ResetTimers();">
        <!-- HEADER -->
        <header id="header">
            <div class="logo">
                <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
            </div>
        </header>
        <!-- MAIN CONTENT -->
        <div class="space-top"></div>
        <main class="item-selection width">
            <ul class="steps">
                <li>
                    <span>1</span>
                    <p>担当工事選択</p>
                </li>
                <li class="active">
                    <span>2</span>
                    <p>項目選択</p>
                </li>
                <li>
                    <span>3</span>
                    <p>写真投稿</p>
                </li>
                <li>
                    <span>4</span>
                    <p>完了</p>
                </li>
            </ul>
            <?php if($this->session->flashdata('error')): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong><?=$this->session->flashdata('error')?></strong> 
            </div>
            <?php endif;?>

            <h2 class="customer_name"><?=$customer?> 様邸</h2>
            <!-- <p><?=$const_id?></p> -->
            <div class="inner">
                <form action="<?=base_url()?>user/photo_selection" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="customer" value="<?=$customer?>">
                    <input type="hidden" name="code_project" value="<?=$code_project?>">
                    <input type="hidden" name="const_id" value="<?=$const_id?>">
                    <input type="hidden" name="color" value="<?php if(!empty($color)){echo $color;}else{ echo $list_category->color;}?>">
                    <div class="box-item" id="<?php if(!empty($color)){echo $color;}else{ echo $list_category->color;}?>">
                        <?php
                            if(!empty($category_name)) :
                        ?>
                            <h3 class="item_name"><?=$category_name?></h3>
                        <?php
                            else :
                        ?>
                            <h3 class="item_name"><?=$list_category->category_name?></h3>
                        <?php
                            endif;
                        ?>
                        
                        <input type="hidden" name="category_name" value="<?php if(!empty($category_name)){ echo $category_name;}else{ echo $list_category->category_name;}?>">
                        <div class="box">

                        <?php
                            if($code_project == '1001') :
                        ?>
                            <div class="item">
                                <p>土台</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>    
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                    <?php
                                    foreach($dropdown1 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>床合板</p>
                                <select class="form-select" name="selection2" id="selection2">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown2 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1002') :
                        ?>
                            <div class="item">
                                <p>建て方</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                    <?php
                                    foreach($dropdown3 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1003') :
                        ?>
                            <div class="item">
                                <p>構造金物</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                    <?php
                                    foreach($dropdown4 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>下地工事</p>
                                <select class="form-select" name="selection2" id="selection2">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown5 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>サッシ工事</p>
                                <select class="form-select" name="selection3" id="selection3">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection3)) :?>
                                <option <?php if ($selection3 == "$selection3") {echo "selected"; } ?> value="<?php echo $selection3.';'.$joblist_files?>"><?=$selection3?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown6 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1004') :
                        ?>
                            <div class="item">
                                <p>外部木工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                    <?php
                                    foreach($dropdown7 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>内部 木工事</p>
                                <select class="form-select" name="selection2" id="selection2">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown8 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1005') :
                        ?>
                            <div class="item">
                                <p>現場調査</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                    <?php
                                    foreach($dropdown9 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>地縄確認</p>
                                <select class="form-select" name="selection2" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown10 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1006') :
                        ?>
                            <div class="item">
                                <p>地盤改良</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown11 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1007') :
                        ?>
                            <div class="item">
                                <p>基礎工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown12 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1008') :
                        ?>
                            <div class="item">
                                <p>給排水</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown13 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1009') :
                        ?>
                            <div class="item">
                                <p>足場工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown14 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1010') :
                        ?>
                            <div class="item">
                                <p>屋根 防水下地</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown15 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>屋根仕上げ</p>
                                <select class="form-select" name="selection2" id="selection2">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown16 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>瓦工事</p>
                                <select class="form-select" name="selection3" id="selection3">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection3)) :?>
                                <option <?php if ($selection3 == "$selection3") {echo "selected"; } ?> value="<?php echo $selection3.';'.$joblist_files?>"><?=$selection3?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown17 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>破風・樋</p>
                                <select class="form-select" name="selection4" id="selection4">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection4)) :?>
                                <option <?php if ($selection4 == "$selection4") {echo "selected"; } ?> value="<?php echo $selection4.';'.$joblist_files?>"><?=$selection4?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown18 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1011') :
                        ?>
                             <div class="item">
                                <p>屋根 防水下地</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown19 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>屋根仕上げ</p>
                                <select class="form-select" name="selection2" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection2)) :?>
                                <option <?php if ($selection2 == "$selection2") {echo "selected"; } ?> value="<?php echo $selection2.';'.$joblist_files?>"><?=$selection2?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown20 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="item">
                                <p>その他</p>
                                <select class="form-select" name="selection3" id="selection3">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection3)) :?>
                                <option <?php if ($selection3 == "$selection3") {echo "selected"; } ?> value="<?php echo $selection3.';'.$joblist_files?>"><?=$selection3?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown21 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1012') :
                        ?>
                            <div class="item">
                                <p>防水工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown22 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1013') :
                        ?>
                            <div class="item">
                                <p>電気工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown23 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1014') :
                        ?>
                            <div class="item">
                                <p>外壁工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown24 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                            if($code_project == '1015') :
                        ?>
                            <div class="item">
                                <p>防蟻工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown25 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>                    
                        <?php
                            endif;
                            if($code_project == '1016') :
                        ?>
                            <div class="item">
                                <p>防蟻工事</p>
                                <select class="form-select" name="selection1" id="selection1">
                                <option value="">ご選択ください</option>
                                <?php if(!empty($selection1)) :?>
                                <option <?php if ($selection1 == "$selection1") {echo "selected"; } ?> value="<?php echo $selection1.';'.$joblist_files?>"><?=$selection1?></option>
                                <?php endif;?>
                                <?php
                                    foreach($dropdown26 as $row)
                                    { 
                                      echo '<option value="'.$row['joblist_name'].';'.$row["joblist_files"].'">'.$row['joblist_name'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
                            endif;
                        ?>
                        </div>
                    </div>
                    <div class="btnBox">
                        <button class="back" name="edit_btn" value="back">前の画面に戻る</button>
                        <button id="send">写真投稿に進む</button>
                    </div>
                </form>
            </div>
        </main>
        <?php $this->load->view('user/dashboard/dashuser_footer_v');?>
        <script>
            $( document ).ready(function() {
                $("#selection1").on("change",function() {
                    event.preventDefault();
                    var str = $('#selection1').val();
                    if (!str) { 
                        $("#selection2").prop('disabled', false);
                        $("#selection3").prop('disabled', false);
                        $("#selection4").prop('disabled', false);
                    }
                    else
                    {
                        $("#selection2").prop('disabled', true);
                        $("#selection3").prop('disabled', true);
                        $("#selection4").prop('disabled', true);
                    }
                });

                
                $("#selection2").change(function() {
                    event.preventDefault();
                    var str = $('#selection2').val();
                    if (!str) { 
                        $("#selection1").prop('disabled', false);
                        $("#selection3").prop('disabled', false);
                        $("#selection4").prop('disabled', false);
                    }
                    else
                    {
                        $("#selection1").prop('disabled', true);
                        $("#selection3").prop('disabled', true);
                        $("#selection4").prop('disabled', true);
                    }
                });

                $("#selection3").change(function() {
                    event.preventDefault();
                    var str = $('#selection3').val();
                    if (!str) { 
                        $("#selection1").prop('disabled', false);
                        $("#selection2").prop('disabled', false);
                        $("#selection4").prop('disabled', false);
                    }
                    else
                    {
                        $("#selection1").prop('disabled', true);
                        $("#selection2").prop('disabled', true);
                        $("#selection4").prop('disabled', true);
                    }   
                });

                $("#selection4").change(function() {
                    event.preventDefault();
                    var str = $('#selection4').val();
                    if (!str) { 
                        $("#selection1").prop('disabled', false);
                        $("#selection2").prop('disabled', false);
                        $("#selection3").prop('disabled', false);
                    }
                    else
                    {
                        $("#selection1").prop('disabled', true);
                        $("#selection2").prop('disabled', true);
                        $("#selection3").prop('disabled', true);
                    }   
                });

            });
        </script>
        <script type="text/javascript" language="javascript" >
            $(window).on("pageshow", function() {
                $('select').prop('selectedIndex', function () {
                    var selected = $(this).children('[selected]').index();
                    return selected != -1 ? selected : 0;
                });
            });

            $('#selection1').on('change', function () {
                $('#send').prop('disabled', !$(this).val());
            }).trigger('change');
            $('#selection2').on('change', function () {
                $('#send').prop('disabled', !$(this).val());
            }).trigger('change');
            $('#selection3').on('change', function () {
                $('#send').prop('disabled', !$(this).val());
            }).trigger('change');
            $('#selection4').on('change', function () {
                $('#send').prop('disabled', !$(this).val());
            }).trigger('change');
        </script>
    </body>
</html>