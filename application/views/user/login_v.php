<?php $this->load->view('user/login_head');?>
<body>
    <div class="login-box">
        <img src="<?=base_url()?>assets/dashboard/img/logo.svg" alt="">
        <h1>現場写真管理システム</h1>
    </div>
    <?php 
      if($this->session->flashdata('error')):
      ?>
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong><?=$this->session->flashdata('error')?></strong> 
      </div>
      
      <script>
        $(".alert").alert();
      </script>
      <?php 
      endif;
      ?>
    <form action="<?=base_url()?>user_login/check_login" method="post">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="ID" name="username">
            <i class="fas fa-user"></i>
        </div>
        <div class="input-group">
            <input type="password" class="form-control" placeholder="パスワード" name="password" id="myInput">
            <i class="fas fa-lock" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();"></i>
        </div>
        <button type="submit" class="btn">ログイン</button>
    </form>
</body>
<!-- /.login-box -->

<?php $this->load->view('user/login_footer');
?>
<script>
function mouseoverPass(obj) {
  var obj = document.getElementById('myInput');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('myInput');
  obj.type = "password";
}
</script>
</body>
</html>
