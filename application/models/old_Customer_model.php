<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Customer_model extends CI_Model{
    public $table = "customer_management";
    public $table_cus = "customer_table";
    public $table_emplo = "employee_table";
    public $table_cont = "contractor_table";
    // public $table_ipazz = "harada_inspection_dupe";
    public $table_ipazz = "harada_client_ob";
    public $table_contractor = "harada_affiliate";
    public $table_employee = "harada_employee";
    public $table_client = "harada_client";
    public $table_construct = "harada_construct";


    // private $db;

    public function __construct()
    {
        // $this->db_b = $this->load->database('ipazz', TRUE); 
    }

    public function insert_customer($customer)
    {
        return $this->db->insert($this->table, $customer);
    }

    public function insert_customers($customer)
    {
        return $this->db->insert($this->table_cus, $customer);
    }

    public function insert_customers_new($customer)
    {
        return $this->db->insert($this->table_ipazz, $customer);
    }

    public function insert_employees($customer)
    {
        return $this->db->insert($this->table_emplo, $customer);
    }

    public function insert_employees_new($customer)
    {
        return $this->db->insert($this->table_employee, $customer);
    }

    public function insert_contractor($customer)
    {
        return $this->db->insert($this->table_cont, $customer);
    }

    public function insert_contractor_new($customer)
    {
        return $this->db->insert($this->table_contractor, $customer);
    }

    public function insert_client_new($customer)
    {
        return $this->db->insert($this->table_client, $customer);
    }

    public function insert_constructor_new($customer)
    {
        return $this->db->insert($this->table_construct, $customer);
    }




    public function update_customer($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $customer);
    }


    public function update_customers($customer, $gid)
    {
        $this->db->where('customer_code', $gid);
        return $this->db->update($this->table_cus, $customer);
    }

    public function update_customers_new($customer, $gid)
    {
        // $this->db->set($customer);
        $this->db->where('gid', $gid);
        // print_r($this->db->get_compiled_update($this->table_ipazz)); die();
        return $this->db->update($this->table_ipazz, $customer);
    }
    
    public function update_employees($customer, $gid)
    {
        $this->db->where('employee_code', $gid);
        return $this->db->update($this->table_emplo, $customer);
    }

    public function update_employees_new($customer, $gid)
    {
        $this->db->where('社員コード', $gid);
        return $this->db->update($this->table_employee, $customer);
    }
    
    public function update_contractor($customer, $gid)
    {
        $this->db->where('contractor_code', $gid);
        return $this->db->update($this->table_cont, $customer);
    }

        
    public function update_contractor_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_contractor, $customer);
    }

    public function update_client_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_client, $customer);
    }

    public function update_constructor_new($construct, $gid)
    {
        $this->db->where('construct_id', $gid);
        return $this->db->update($this->table_construct, $construct);
    }


    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_all_client()
    {
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_all_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }


    public function fetch_all_customers()
    {
        return $this->db->get($this->table_cus)->result();
    }

    public function fetch_all_customers_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_all_employees()
    {
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_employees_names()
    {
        $query = $this->db->query('SELECT 姓, 名 FROM harada_employee');
        return $query->result();
    }

    public function fetch_affiliate_names()
    {
        $query = $this->db->query('SELECT name FROM harada_affiliate');
        return $query->result();
    }

    public function fetch_finances()
    {
        $query = $this->db->query('SELECT id, 金融機関名 FROM harada_finance');
        return $query->result();
    }

    public function fetch_all_employees_new()
    {
        return $this->db->get($this->table_employee)->result();
    }
    
    public function fetch_all_contractor()
    {
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_all_contractor_new()
    {
        return $this->db->get($this->table_contractor)->result();
    }

    public function fetch_all_construction_new()
    {
        return $this->db->get($this->table_construct)->result();
    }

    public function fetch_all_construction_by($name)
    {
        $this->db->group_start();
            $this->db->like('co_owner1 ',$name['afiliate_name']);
            $this->db->or_like('co_owner2 ',$name['afiliate_name']);
            $this->db->or_like('co_owner3 ',$name['afiliate_name']);
            $this->db->or_like('co_owner4 ',$name['afiliate_name']);
            $this->db->or_like('co_owner5 ',$name['afiliate_name']);
            $this->db->or_like('co_owner6 ',$name['afiliate_name']);
            $this->db->or_like('co_owner7 ',$name['afiliate_name']);
            $this->db->or_like('co_owner8 ',$name['afiliate_name']);
            $this->db->or_like('co_owner9 ',$name['afiliate_name']);
        $this->db->group_end();

        $query = $this->db->get($this->table_construct);

        // print_r($this->db->get_compiled_select($this->table_construct)); die();
        return $query->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function fetch_one_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_client($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }

    public function fetch_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->get($this->table_cus)->result();
    }

    
    public function fetch_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->get($this->table_employee)->result();
    }

    public function fetch_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_one_contractor_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_contractor)->result();
    }

    public function fetch_one_construction_new($id)
    {
        $this->db->where('construct_id', $id);
        return $this->db->get($this->table_construct)->result();
    }

    public function get_last_id_inspect()
    {
        return $this->db->query('SELECT gid FROM harada_client_ob ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_client()
    {
        return $this->db->query('SELECT gid FROM harada_client ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_affiliate()
    {
        return $this->db->query('SELECT gid FROM harada_affiliate ORDER BY gid DESC LIMIT 1')->result();
    }

    // public function get_last_id_employee($id)
    // {
    //     return $this->db->query('SELECT gid FROM harada_inspection_dupe ORDER BY gid DESC LIMIT 1')->result();
    // }

    public function get_last_contract()
    {
        return $this->db->query('SELECT `contract` FROM harada_construct')->result();
    }

    public function get_last_date_contract()
    {
        return $this->db->query('SELECT date_insert_contract1 FROM harada_construct')->result();
    }

    public function get_last_historycontract()
    {
        return $this->db->query('SELECT contract_history FROM harada_construct')->result();
    }

    public function get_last_historydate_contract()
    {
        return $this->db->query('SELECT date_insert_history1 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing1()
    {
        return $this->db->query('SELECT constract_drawing1 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing1()
    {
        return $this->db->query('SELECT date_insert_drawing1 FROM harada_construct')->result();
    }

    public function get_last_historydrawing1()
    {
        return $this->db->query('SELECT contract_drawing1_history FROM harada_construct')->result();
    }

    public function get_last_historydate_drawing1()
    {
        return $this->db->query('SELECT date_insert_history2 FROM harada_construct')->result();
    }


    



    public function get_last_constract_drawing2()
    {
        return $this->db->query('SELECT constract_drawing2 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing2()
    {
        return $this->db->query('SELECT date_insert_drawing2 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing3()
    {
        return $this->db->query('SELECT constract_drawing3 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing3()
    {
        return $this->db->query('SELECT date_insert_drawing3 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing4()
    {
        return $this->db->query('SELECT constract_drawing4 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing4()
    {
        return $this->db->query('SELECT date_insert_drawing4 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing5()
    {
        return $this->db->query('SELECT constract_drawing5 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing5()
    {
        return $this->db->query('SELECT date_insert_drawing5 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing6()
    {
        return $this->db->query('SELECT constract_drawing6 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing6()
    {
        return $this->db->query('SELECT date_insert_drawing6 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing7()
    {
        return $this->db->query('SELECT constract_drawing7 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing7()
    {
        return $this->db->query('SELECT date_insert_drawing7 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing8()
    {
        return $this->db->query('SELECT constract_drawing8 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing8()
    {
        return $this->db->query('SELECT date_insert_drawing8 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing9()
    {
        return $this->db->query('SELECT constract_drawing9 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing9()
    {
        return $this->db->query('SELECT date_insert_drawing9 FROM harada_construct')->result();
    }






    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }

    
    public function delete_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->delete($this->table_cus);
    }

    public function delete_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_client);
    }

    public function delete_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_ipazz);
    }
    
    public function delete_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->delete($this->table_emplo);
    }

    public function delete_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->delete($this->table_employee);
    }

    public function delete_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->delete($this->table_cont);
    }

    public function delete_one_contractor_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_contractor);
    }


    public function get_datas_client($data)
    {
        $this->db->select("*");
        if($data['住所'] != null)
        {
            $this->db->group_start();
                $this->db->like('住所',$data['住所']);
            $this->db->group_end();
        }
        if($data['full_name'] != null)
        {
            $this->db->group_start();
                $this->db->like('氏名',$data['full_name']);
            $this->db->group_end();
        }
        if($data['full_name_kana'] != null)
        {
            $this->db->group_start();
                $this->db->like('氏名_かな',$data['full_name_kana']);
            $this->db->group_end();
        }
        if($data['ＤＭ'] != null)
        {
            $this->db->group_start();
                $this->db->like("ＤＭ",$data['ＤＭ']);
            $this->db->group_end();
        }
        $this->db->order_by('gid', 'ASC');
        $query = $this->db->get($this->table_client);
        // print_r($this->db->get_compiled_select($this->table_client)); die();
        return $query->result();
    }

    public function get_datas_client_ob($data)
    {
        $this->db->select("*");
        if($data['cust_code'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_code',$data['cust_code']);
            $this->db->group_end();
        }
        if($data['cust_name'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_name',$data['cust_name']);
            $this->db->group_end();
        }
        if($data['kinds'] != null)
        {
            $this->db->group_start();
                $this->db->like('kinds',$data['kinds']);
            $this->db->group_end();
        }
        if($data['kinds'] != null)
        {
            $this->db->group_start();
                $this->db->like("種別",$data['kinds']);
            $this->db->group_end();
        }
        if($data['営業担当'] != null)
        {
            $this->db->group_start();
                $this->db->like("営業担当",$data['営業担当']);
            $this->db->group_end();
        }
        if($data['工務担当'] != null)
        {
            $this->db->group_start();
                $this->db->like("工務担当",$data['工務担当']);
            $this->db->group_end();
        }
        if($data['コーデ担当'] != null)
        {
            $this->db->group_start();
                $this->db->like("コーデ担当",$data['コーデ担当']);
            $this->db->group_end();
        }
        if($data['感謝祭'] != null)
        {
            $this->db->group_start();
                $this->db->like("感謝祭",$data['感謝祭']);
            $this->db->group_end();
        }
        if($data['イベントDM'] != null)
        {
            $this->db->group_start();
                $this->db->like("イベントDM",$data['イベントDM']);
            $this->db->group_end();
        }
        if($data['長期優良住宅'] != null)
        {
            $this->db->group_start();
                $this->db->like("長期優良住宅",$data['長期優良住宅']);
            $this->db->group_end();
        }
        if($data['設備10年保証加入'] != null)
        {
            $this->db->group_start();
                $this->db->like("設備10年保証加入",$data['設備10年保証加入']);
            $this->db->group_end();
        }
        if($data['グリーン化事業補助金'] != null)
        {
            $this->db->group_start();
                $this->db->like("グリーン化事業補助金",$data['グリーン化事業補助金']);
            $this->db->group_end();
        }
        if($data['地盤改良工事'] != null)
        {
            $this->db->group_start();
                $this->db->like("地盤改良工事",$data['地盤改良工事']);
            $this->db->group_end();
        }
        if($data['借入金融機関'] != null)
        {
            $this->db->group_start();
                $this->db->like("借入金融機関",$data['借入金融機関']);
            $this->db->group_end();
        }
        if($data['火災保険'] != null)
        {
            $this->db->group_start();
                $this->db->like("火災保険",$data['火災保険']);
            $this->db->group_end();
        }
        if($data['引渡日_1'] != null)
        {
            $this->db->group_start();
                $this->db->where("引渡日 BETWEEN '".$data['引渡日_1']."' AND '".$data['引渡日_2']."'");
            $this->db->group_end();
        }
        if($data['生年月日_1'] != null)
        {
            $this->db->group_start();
                $this->db->where("生年月日 BETWEEN '".$data['生年月日_1']."' AND '".$data['生年月日_2']."'");
            $this->db->group_end();
        }
        if($data['inspection_from'] != null)
        {
            $this->db->group_start();
                $this->db->where("定期点検項目_3ヶ月 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_6ヶ月 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_1年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_2年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_3年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_5年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_10年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
            $this->db->group_end();
        }
        $this->db->order_by('gid', 'ASC');
        $query = $this->db->get($this->table_ipazz);

        // print_r($this->db->get_compiled_select($this->table_ipazz)); die();
        return $query->result();
    }


    public function get_data_construct($data)
    {
        if($data['cust_code'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_code',$data['cust_code']);
            $this->db->group_end();
        }
        if($data['cust_name'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_name',$data['cust_name']);
            $this->db->group_end();
        }
        if($data['kinds'] != null)
        {
            $this->db->group_start();
                $this->db->like('kinds',$data['kinds']);
            $this->db->group_end();
        }
        
        if($data['from_contract_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("contract_date BETWEEN '".$data['from_contract_date']."' AND '".$data['to_contract_date']."'");
            $this->db->group_end();
        }

        if($data['from_construction_start_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("construction_start_date BETWEEN '".$data['from_construction_start_date']."' AND '".$data['to_construction_start_date']."'");
            $this->db->group_end();
        }


        if($data['from_upper_building_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("upper_building_date BETWEEN '".$data['from_upper_building_date']."' AND '".$data['to_upper_building_date']."'");
            $this->db->group_end();
        }


        if($data['from_completion_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("completion_date BETWEEN '".$data['from_completion_date']."' AND '".$data['to_completion_date']."'");
            $this->db->group_end();
        }


        if($data['from_delivery_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("delivery_date BETWEEN '".$data['from_delivery_date']."' AND '".$data['to_delivery_date']."'");
            $this->db->group_end();
        }
        $this->db->order_by('construct_id ', 'ASC');
        $query = $this->db->get($this->table_construct);

        // print_r($this->db->get_compiled_select($this->table_ipazz)); die();
        return $query->result();
    }

    public function get_project_category($code)
    {
        $this->db->select("category_name, category_id, color");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_category');
        return $query->result();
    }

    public function get_categoryitem($code){
        $this->db->select("id_item, item_name, category_id");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_categoryitem');
        return $query->result_array();
    }


    public function get_projectjoblist($code){
        $this->db->select("joblist_name, joblist_files, category_id, id_item");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_joblist');
        return $query->result_array();
    }


    public function view_file_project($ehe)
    {
        // $this->db->group_start();
        //     $this->db->like('co_owner1 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner2 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner3 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner4 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner5 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner6 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner7 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner8 ',$ehe['afiliator']);
        //     $this->db->or_like('co_owner9 ',$ehe['afiliator']);
        // $this->db->group_end();

        // $query = $this->db->get($this->table_construct);

        // print_r($this->db->get_compiled_select($this->table_construct)); die();
        // return $query->result();

        $this->db->where('cust_name', $ehe['afiliator']);
        return $this->db->get($this->table_construct)->result();
    }


    function search_id($cust_code){
        $this->db->like('cust_code', $cust_code , 'both');
        $this->db->order_by('gid', 'ASC');
        $this->db->limit(10);
        return $this->db->get('harada_client_ob')->result();
    }
    
}
?>