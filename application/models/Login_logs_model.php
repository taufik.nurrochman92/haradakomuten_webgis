<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Login_logs_model extends CI_Model{
    public $table = "harada_login_logs";
    public $table_user = "harada_affiliate_login_cms";
    
    public function insert_log($log)
    {
        return $this->db->insert($this->table, $log);
    }

    public function fetch_all()
    {
        $this->db->select('username, access, COUNT(username)');
        $this->db->group_by('username');
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->table)->result();
    }

    public function update_date($log)
    {
        $this->db->set('last_login', $log['login_time']);
        $this->db->where('username', $log['username']);
        return $this->db->update($this->table_user);
    }
}
?>