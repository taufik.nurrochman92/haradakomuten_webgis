<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Menu_logs_model extends CI_Model{
    public $table = "harada_menu_logs";

    public function insert_log($logs)
    {
        return $this->db->insert($this->table, $logs);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function count_all()
    {
        $this->db->select('COUNT(id) as total');
        return $this->db->get($this->table)->result()[0];
    }

    public function count_menu($menu)
    {
        $this->db->select('COUNT(id) as total');
        $this->db->where('menu', $menu);
        return $this->db->get($this->table)->result()[0];
    }

    public function count_menu_action($menu, $action)
    {
        $this->db->select('COUNT(id) as total');
        $this->db->where('menu', $menu);
        $this->db->where('action', $action);
        return $this->db->get($this->table)->result()[0];
    }

    public function count_menu_like($menu)
    {
        $this->db->select('COUNT(id) as total');
        $this->db->like('menu', $menu);
        return $this->db->get($this->table)->result()[0];
    }

    public function fetch_one($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->table)->result();
    }
}
?>