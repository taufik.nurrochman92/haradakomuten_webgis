<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Partner_model extends CI_Model{
    public $table = "partner_company";

    public function insert_partner($partner)
    {
        return $this->db->insert($this->table, $partner);
    }

    public function update_partner($partner, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $partner);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }
}
?>