<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Items_model extends CI_Model{
    public $table = "items";
    public $table_ipazz = "harada_client_ob";

    // private $db_b;

    public function __construct()
    {
        // $this->db_b = $this->load->database('ipazz', TRUE); 
    }

    public function insert_item($items)
    {
        return $this->db->insert($this->table, $items);
    }

    public function fetch_items($category = null)
    {
        if($category != null)
        {
            $this->db->where('category', $category);
        }
        return $this->db->get($this->table)->result();
    }

    public function fetch_items_new($category = null)
    {
        if($category != null)
        {
            $this->db->where('種別', $category);
        }
        return $this->db->get($this->table_ipazz)->result();
    }
}
?>