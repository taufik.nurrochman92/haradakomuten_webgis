<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Customer_model extends CI_Model{
    public $table = "customer_management";
    public $table_cus = "customer_table";
    public $table_emplo = "employee_table";
    public $table_cont = "contractor_table";
    // public $table_ipazz = "harada_inspection_dupe";
    public $table_ipazz = "harada_client_ob";
    public $table_contractor = "harada_affiliate";
    public $table_employee = "harada_employee";
    public $table_client = "harada_client";
    public $table_construct = "harada_construct";


    // private $db;

    public function __construct()
    {
        // $this->db_b = $this->load->database('ipazz', TRUE); 
    }

    public function insert_customer($customer)
    {
        return $this->db->insert($this->table, $customer);
    }

    public function insert_customers($customer)
    {
        return $this->db->insert($this->table_cus, $customer);
    }

    public function insert_customers_new($customer)
    {
        return $this->db->insert($this->table_ipazz, $customer);
    }

    public function insert_employees($customer)
    {
        return $this->db->insert($this->table_emplo, $customer);
    }

    public function insert_employees_new($customer)
    {
        // return $this->db->insert($this->table_employee, $customer);

        $data = array(
            "社員コード" => $customer['社員コード'],
            "姓" => $customer['姓'],
            "名" => $customer['名'],
            "せい" => $customer['せい'],
            "めい" => $customer['めい'],
            "保有資格1" => $customer['保有資格1'],
            "保有資格2" => $customer['保有資格2'],
            "保有資格3" => $customer['保有資格3'],
            "保有資格4" => $customer['保有資格4'],
            "保有資格5" => $customer['保有資格5'],
            "sertificate_1" => $customer['sertificate_1'],
            "sertificate_2" => $customer['sertificate_2'],
            "sertificate_3" => $customer['sertificate_3'],
            "sertificate_4" => $customer['sertificate_4'],
            "sertificate_5" => $customer['sertificate_5'],
            "入社日" => $customer['入社日'],
            "郵便番号1" => $customer['郵便番号1'],
            "住所1" => $customer['住所1'],
            "住所1番地" => $customer['住所1番地'],
            "電話番号" => $customer['電話番号'],
            "携帯番号" => $customer['携帯番号'],
            "email" => $customer['email'],
        );


        if($this->db->insert($this->table_employee,$data)){
            return true;
        }
        else
        {
            // return false;
            //or
            print_r($this->db->error());
            die();
        } 
    }

    public function insert_contractor($customer)
    {
        return $this->db->insert($this->table_cont, $customer);
    }

    public function insert_contractor_new($customer)
    {
        return $this->db->insert($this->table_contractor, $customer);
    }

    public function insert_client_new($customer)
    {
        return $this->db->insert($this->table_client, $customer);
    }

    public function insert_constructor_new($customer)
    {
        return $this->db->insert($this->table_construct, $customer);
        //  $this->db->error(); die();
    }




    public function update_customer($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $customer);
    }


    public function update_customers($customer, $gid)
    {
        $this->db->where('customer_code', $gid);
        return $this->db->update($this->table_cus, $customer);
    }

    public function update_customers_new($customer, $gid)
    {
        // $this->db->set($customer);
        $this->db->where('gid', $gid);
        // print_r($this->db->get_compiled_update($this->table_ipazz)); die();
        return $this->db->update($this->table_ipazz, $customer);
    }
    
    public function update_employees($customer, $gid)
    {
        $this->db->where('employee_code', $gid);
        return $this->db->update($this->table_emplo, $customer);
    }

    public function update_employees_new($customer, $gid)
    {
        $this->db->where('社員コード', $gid);
        // return $this->db->update($this->table_employee, $customer);
        $data = array(
            "社員コード" => $customer['社員コード'],
            "姓" => $customer['姓'],
            "名" => $customer['名'],
            "せい" => $customer['せい'],
            "めい" => $customer['めい'],
            "保有資格1" => $customer['保有資格1'],
            "保有資格2" => $customer['保有資格2'],
            "保有資格3" => $customer['保有資格3'],
            "保有資格4" => $customer['保有資格4'],
            "保有資格5" => $customer['保有資格5'],
            "sertificate_1" => $customer['sertificate_1'],
            "sertificate_2" => $customer['sertificate_2'],
            "sertificate_3" => $customer['sertificate_3'],
            "sertificate_4" => $customer['sertificate_4'],
            "sertificate_5" => $customer['sertificate_5'],
            "入社日" => $customer['入社日'],
            "郵便番号1" => $customer['郵便番号1'],
            "住所1" => $customer['住所1'],
            "住所1番地" => $customer['住所1番地'],
            "電話番号" => $customer['電話番号'],
            "携帯番号" => $customer['携帯番号'],
            "email" => $customer['email'],
        );


        if($this->db->update($this->table_employee,$data)){
            return true;
        }
        else
        {
            // return false;
            //or
            print_r($this->db->error());
            die();
        } 
    }
    
    public function update_contractor($customer, $gid)
    {
        $this->db->where('contractor_code', $gid);
        return $this->db->update($this->table_cont, $customer);
    }

        
    public function update_contractor_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_contractor, $customer);
    }

    public function update_client_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_client, $customer);
    }

    public function update_constructor_new($construct, $gid)
    {
        $this->db->where('construct_id', $gid);
        return $this->db->update($this->table_construct, $construct);

        // if (! $this->db->update($this->table_construct, $construct)) {
        //     return $error = $db->error(); // Has keys 'code' and 'message'
        // }
        // else
        // {
        //     return true;
        // }

        // return $this->db->error(); die();
    }


    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_all_client()
    {
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_all_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }


    public function fetch_all_customers()
    {
        return $this->db->get($this->table_cus)->result();
    }

    public function fetch_all_customers_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_all_employees()
    {
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_employees_names()
    {
        $query = $this->db->query('SELECT 姓, 名 FROM harada_employee');
        return $query->result();
    }

    public function fetch_affiliate_names()
    {
        $query = $this->db->query('SELECT name FROM harada_affiliate');
        return $query->result();
    }

    public function fetch_affiliate_names_by($id)
    {
        $query = $this->db->query('SELECT `name` FROM harada_affiliate WHERE gid ='.$id);
        return $query->row();
    }

    public function fetch_finances()
    {
        $query = $this->db->query('SELECT id, 金融機関名 FROM harada_finance');
        return $query->result();
    }

    public function fetch_all_employees_new()
    {
        return $this->db->get($this->table_employee)->result();
    }
    
    public function fetch_all_contractor()
    {
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_all_contractor_new()
    {
        return $this->db->get($this->table_contractor)->result();
    }

    public function fetch_all_construction_new()
    {
        return $this->db->get($this->table_construct)->result();
    }


    public function fetch_all_construction_by($name)
    {
        $this->db->group_start();
            $this->db->like('co_owner1bluea',$name);
            $this->db->or_like('co_owner2bluea',$name);
            $this->db->or_like('co_owner3bluea',$name);
            $this->db->or_like('co_owner4bluea',$name);
            $this->db->or_like('co_owner5bluea',$name);
            $this->db->or_like('co_owner6bluea',$name);
            $this->db->or_like('co_owner7bluea',$name);
            $this->db->or_like('co_owner8bluea',$name);
            $this->db->or_like('co_owner9bluea',$name);
            $this->db->or_like('co_owner10bluea',$name);
            $this->db->or_like('co_owner1blueb',$name);
            $this->db->or_like('co_owner2blueb',$name);
            $this->db->or_like('co_owner3blueb',$name);
            $this->db->or_like('co_owner4blueb',$name);
            $this->db->or_like('co_owner5blueb',$name);
            $this->db->or_like('co_owner6blueb',$name);
            $this->db->or_like('co_owner7blueb',$name);
            $this->db->or_like('co_owner8blueb',$name);
            $this->db->or_like('co_owner9blueb',$name);
            $this->db->or_like('co_owner10blueb',$name);
            $this->db->or_like('co_owner1bluec',$name);
            $this->db->or_like('co_owner2bluec',$name);
            $this->db->or_like('co_owner3bluec',$name);
            $this->db->or_like('co_owner4bluec',$name);
            $this->db->or_like('co_owner5bluec',$name);
            $this->db->or_like('co_owner6bluec',$name);
            $this->db->or_like('co_owner7bluec',$name);
            $this->db->or_like('co_owner8bluec',$name);
            $this->db->or_like('co_owner9bluec',$name);
            $this->db->or_like('co_owner10bluec',$name);
            $this->db->or_like('co_owner1blued',$name);
            $this->db->or_like('co_owner2blued',$name);
            $this->db->or_like('co_owner3blued',$name);
            $this->db->or_like('co_owner4blued',$name);
            $this->db->or_like('co_owner5blued',$name);
            $this->db->or_like('co_owner6blued',$name);
            $this->db->or_like('co_owner7blued',$name);
            $this->db->or_like('co_owner8blued',$name);
            $this->db->or_like('co_owner9blued',$name);
            $this->db->or_like('co_owner10blued',$name);
            $this->db->or_like('co_owner1bluee',$name);
            $this->db->or_like('co_owner2bluee',$name);
            $this->db->or_like('co_owner3bluee',$name);
            $this->db->or_like('co_owner4bluee',$name);
            $this->db->or_like('co_owner5bluee',$name);
            $this->db->or_like('co_owner6bluee',$name);
            $this->db->or_like('co_owner7bluee',$name);
            $this->db->or_like('co_owner8bluee',$name);
            $this->db->or_like('co_owner9bluee',$name);
            $this->db->or_like('co_owner10bluee',$name);
            $this->db->or_like('co_owner1bluef',$name);
            $this->db->or_like('co_owner2bluef',$name);
            $this->db->or_like('co_owner3bluef',$name);
            $this->db->or_like('co_owner4bluef',$name);
            $this->db->or_like('co_owner5bluef',$name);
            $this->db->or_like('co_owner6bluef',$name);
            $this->db->or_like('co_owner7bluef',$name);
            $this->db->or_like('co_owner8bluef',$name);
            $this->db->or_like('co_owner9bluef',$name);
            $this->db->or_like('co_owner10bluef',$name);
            $this->db->or_like('co_owner1blueg',$name);
            $this->db->or_like('co_owner2blueg',$name);
            $this->db->or_like('co_owner3blueg',$name);
            $this->db->or_like('co_owner4blueg',$name);
            $this->db->or_like('co_owner5blueg',$name);
            $this->db->or_like('co_owner6blueg',$name);
            $this->db->or_like('co_owner7blueg',$name);
            $this->db->or_like('co_owner8blueg',$name);
            $this->db->or_like('co_owner9blueg',$name);
            $this->db->or_like('co_owner10blueg',$name);
        $this->db->group_end();

        // print_r($this->db->get_compiled_select($this->table_construct)); die();

        $query = $this->db->get($this->table_construct);
        return $query->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function fetch_one_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_client($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }

    public function fetch_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->get($this->table_cus)->result();
    }

    
    public function fetch_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->get($this->table_employee)->result();
    }

    public function fetch_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_one_contractor_new($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->table_contractor)->result();
    }

    public function fetch_one_construction_new($id)
    {
        $this->db->where('construct_id', $id);
        return $this->db->get($this->table_construct)->result();
    }

    public function fetch_one_construction_data($id)
    {
        $this->db->where('cust_code', $id);
        return $this->db->get($this->table_construct)->result();
        // print_r($this->db->get_compiled_select($this->table_construct)); die();
    }

    public function fetch_one_construction_newer($id,$category)
    {
        $this->db->where('construct_id', $id);
        $this->db->where('category_name', $category);
        return $this->db->get($this->table_construct)->result();
        // print_r($this->db->get_compiled_select($this->table_construct)); die();
    }

    public function fetch_category_name($id)
    {
        $query = $this->db->query('SELECT category_name FROM harada_construct WHERE construct_id='.$id);
        return $query->result();
    }




    public function get_last_id_inspect()
    {
        return $this->db->query('SELECT gid FROM harada_client_ob ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_client()
    {
        return $this->db->query('SELECT gid FROM harada_client ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_affiliate()
    {
        return $this->db->query('SELECT gid FROM harada_affiliate ORDER BY gid DESC LIMIT 1')->result();
    }


    public function get_last_id_employee($id)
    {
        return $this->db->query('SELECT 社員コード FROM harada_employee ORDER BY 社員コード DESC LIMIT 1')->result();
    }

    public function get_last_contract()
    {
        return $this->db->query('SELECT `contract` FROM harada_construct')->result();
    }

    public function get_last_date_contract()
    {
        return $this->db->query('SELECT date_insert_contract1 FROM harada_construct')->result();
    }

    public function get_last_historycontract()
    {
        return $this->db->query('SELECT contract_history FROM harada_construct')->result();
    }

    public function get_last_historydate_contract()
    {
        return $this->db->query('SELECT date_insert_history1 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing1()
    {
        return $this->db->query('SELECT constract_drawing1 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing1()
    {
        return $this->db->query('SELECT date_insert_drawing1 FROM harada_construct')->result();
    }

    public function get_last_historydrawing1()
    {
        return $this->db->query('SELECT contract_drawing1_history FROM harada_construct')->result();
    }

    public function get_last_historydate_drawing1()
    {
        return $this->db->query('SELECT date_insert_history2 FROM harada_construct')->result();
    }


    



    public function get_last_constract_drawing2()
    {
        return $this->db->query('SELECT constract_drawing2 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing2()
    {
        return $this->db->query('SELECT date_insert_drawing2 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing3()
    {
        return $this->db->query('SELECT constract_drawing3 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing3()
    {
        return $this->db->query('SELECT date_insert_drawing3 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing4()
    {
        return $this->db->query('SELECT constract_drawing4 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing4()
    {
        return $this->db->query('SELECT date_insert_drawing4 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing5()
    {
        return $this->db->query('SELECT constract_drawing5 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing5()
    {
        return $this->db->query('SELECT date_insert_drawing5 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing6()
    {
        return $this->db->query('SELECT constract_drawing6 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing6()
    {
        return $this->db->query('SELECT date_insert_drawing6 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing7()
    {
        return $this->db->query('SELECT constract_drawing7 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing7()
    {
        return $this->db->query('SELECT date_insert_drawing7 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing8()
    {
        return $this->db->query('SELECT constract_drawing8 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing8()
    {
        return $this->db->query('SELECT date_insert_drawing8 FROM harada_construct')->result();
    }






    public function get_last_constract_drawing9()
    {
        return $this->db->query('SELECT constract_drawing9 FROM harada_construct')->result();
    }

    public function get_last_date_constract_drawing9()
    {
        return $this->db->query('SELECT date_insert_drawing9 FROM harada_construct')->result();
    }






    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }

    
    public function delete_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->delete($this->table_cus);
    }

    public function delete_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_client);
    }

    public function delete_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_ipazz);
    }
    
    public function delete_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->delete($this->table_emplo);
    }

    public function delete_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->delete($this->table_employee);
    }

    public function delete_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->delete($this->table_cont);
    }

    public function delete_one_contractor_new($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->table_contractor);
    }

    public function delete_one_construction_new($id)
    {
        $this->db->where('construct_id', $id);
        return $this->db->delete($this->table_construct);
    }


    public function get_datas_client($data)
    {
        $this->db->select("*");
        if(!empty($data['氏名']))
        {
            $this->db->group_start();
                $this->db->like('氏名',$data['氏名']);
            $this->db->group_end();
        }
        if(!empty($data['連名']))
        {
            $this->db->group_start();
                $this->db->like('連名',$data['連名']);
            $this->db->group_end();
        }
        if(!empty($data['住所']))
        {
            $this->db->group_start();
                $this->db->like('住所',$data['住所']);
            $this->db->group_end();
        }
        if(!empty($data['ＤＭ']))
        {
            $this->db->group_start();
                $this->db->like("ＤＭ",$data['ＤＭ']);
            $this->db->group_end();
        }
        if(!empty($data['土地　有無']))
        {
            $this->db->group_start();
                $this->db->like("土地　有無",$data['土地　有無']);
            $this->db->group_end();
        }
        if(!empty($data['初回接点']))
        {
            $this->db->group_start();
                $this->db->like("初回接点",$data['初回接点']);
            $this->db->group_end();
        }
        if(!empty($data['age']))
        {
            if($data['age'] == '20') {
                $this->db->group_start();
                $this->db->where("age BETWEEN '20' AND '29'");
                $this->db->group_end();
            }
            elseif($data['age'] == '30')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '30' AND '39'");
                $this->db->group_end();
            }
            elseif($data['age'] == '40')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '40' AND '49'");
                $this->db->group_end();
            }
            elseif($data['age'] == '50')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '50' AND '59'");
                $this->db->group_end();
            }
            elseif($data['age'] == '60')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '60' AND '69'");
                $this->db->group_end();
            }
            elseif($data['age'] == '70')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '70' AND '79'");
                $this->db->group_end();
            }
            elseif($data['age'] == '80')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '80' AND '89'");
                $this->db->group_end();
            }
            else
            {
                $this->db->group_start();
                $this->db->where("age","");
                $this->db->group_end();
            }
        }

        $this->db->order_by('gid', 'ASC');
        $query = $this->db->get($this->table_client);
        // print_r($this->db->get_compiled_select($this->table_client)); die();
        return $query->result();
    }

    public function get_datas_client_ob($data)
    {
        $this->db->select("*");
        if(!empty($data['氏名']))
        {
            $this->db->group_start();
                $this->db->like('氏名',$data['氏名']);
            $this->db->group_end();
        }
        if(!empty($data['連名']))
        {
            $this->db->group_start();
                $this->db->like('連名',$data['連名']);
            $this->db->group_end();
        }
        if(!empty($data['cust_code']))
        {
            $this->db->group_start();
                $this->db->like('cust_code',$data['cust_code']);
            $this->db->group_end();
        }
        if(!empty($data['住　　　所']))
        {
            $this->db->group_start();
                $this->db->like('住　　　所',$data['住　　　所']);
            $this->db->group_end();
        }
        if(!empty($data['kinds']))
        {
            $this->db->group_start();
                $this->db->like("種別",$data['kinds']);
            $this->db->group_end();
        }
        if(!empty($data['営業担当']))
        {
            $this->db->group_start();
                $this->db->like("営業担当",$data['営業担当']);
            $this->db->group_end();
        }
        if(!empty($data['工務担当']))
        {
            $this->db->group_start();
                $this->db->like("工務担当",$data['工務担当']);
            $this->db->group_end();
        }
        if(!empty($data['コーデ担当']))
        {
            $this->db->group_start();
                $this->db->like("コーデ担当",$data['コーデ担当']);
            $this->db->group_end();
        }
        if(!empty($data['感謝祭']))
        {
            $this->db->group_start();
                $this->db->like("感謝祭",$data['感謝祭']);
            $this->db->group_end();
        }
        if(!empty($data['イベントDM']))
        {
            $this->db->group_start();
                $this->db->like("イベントDM",$data['イベントDM']);
            $this->db->group_end();
        }
        if(!empty($data['長期優良住宅']))
        {
            $this->db->group_start();
                $this->db->like("長期優良住宅",$data['長期優良住宅']);
            $this->db->group_end();
        }
        if(!empty($data['設備10年保証加入']))
        {
            $this->db->group_start();
                $this->db->like("設備10年保証加入",$data['設備10年保証加入']);
            $this->db->group_end();
        }
        if(!empty($data['グリーン化事業補助金']))
        {
            $this->db->group_start();
                $this->db->like("グリーン化事業補助金",$data['グリーン化事業補助金']);
            $this->db->group_end();
        }
        if(!empty($data['地盤改良工事']))
        {
            $this->db->group_start();
                $this->db->like("地盤改良工事",$data['地盤改良工事']);
            $this->db->group_end();
        }
        if(!empty($data['借入金融機関']))
        {
            $this->db->group_start();
                $this->db->like("借入金融機関",$data['借入金融機関']);
            $this->db->group_end();
        }
        if(!empty($data['火災保険']))
        {
            $this->db->group_start();
                $this->db->like("火災保険",$data['火災保険']);
            $this->db->group_end();
        }
        if(!empty($data['引渡日_1']))
        {
            $this->db->group_start();
                $this->db->where("引渡日 BETWEEN '".$data['引渡日_1']."' AND '".$data['引渡日_2']."'");
            $this->db->group_end();
        }
        if(!empty($data['生年月日_1']))
        {
            $this->db->group_start();
                $this->db->where("生年月日 BETWEEN '".$data['生年月日_1']."' AND '".$data['生年月日_2']."'");
            $this->db->group_end();
        }
        if(!empty($data['inspection_from']))
        {
            $this->db->group_start();
                $this->db->where("定期点検項目_3ヶ月 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_6ヶ月 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_1年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_2年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_3年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_5年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
                $this->db->or_where("定期点検項目_10年 BETWEEN '".$data['inspection_from']."' AND '".$data['inspection_to']."'");
            $this->db->group_end();
        }
        if(!empty($data['age']))
        {
            if($data['age'] == '20') {
                $this->db->group_start();
                $this->db->where("age BETWEEN '20' AND '29'");
                $this->db->group_end();
            }
            elseif($data['age'] == '30')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '30' AND '39'");
                $this->db->group_end();
            }
            elseif($data['age'] == '40')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '40' AND '49'");
                $this->db->group_end();
            }
            elseif($data['age'] == '50')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '50' AND '59'");
                $this->db->group_end();
            }
            elseif($data['age'] == '60')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '60' AND '69'");
                $this->db->group_end();
            }
            elseif($data['age'] == '70')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '70' AND '79'");
                $this->db->group_end();
            }
            elseif($data['age'] == '80')
            {
                $this->db->group_start();
                $this->db->where("age BETWEEN '80' AND '89'");
                $this->db->group_end();
            }
            else
            {
                $this->db->group_start();
                $this->db->where("age","");
                $this->db->group_end();
            }
        }

        $this->db->order_by('gid', 'ASC');
        $query = $this->db->get($this->table_ipazz);

        // print_r($this->db->get_compiled_select($this->table_ipazz)); die();
        return $query->result();
    }


    public function get_data_construct($data)
    {
        if($data['cust_code'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_code',$data['cust_code']);
            $this->db->group_end();
        }
        if($data['cust_name'] != null)
        {
            $this->db->group_start();
                $this->db->like('cust_name',$data['cust_name']);
            $this->db->group_end();
        }
        if($data['kinds'] != null)
        {
            $this->db->group_start();
                $this->db->like('kinds',$data['kinds']);
            $this->db->group_end();
        }
        
        if($data['from_contract_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("contract_date BETWEEN '".$data['from_contract_date']."' AND '".$data['to_contract_date']."'");
            $this->db->group_end();
        }

        if($data['from_construction_start_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("construction_start_date BETWEEN '".$data['from_construction_start_date']."' AND '".$data['to_construction_start_date']."'");
            $this->db->group_end();
        }


        if($data['from_upper_building_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("upper_building_date BETWEEN '".$data['from_upper_building_date']."' AND '".$data['to_upper_building_date']."'");
            $this->db->group_end();
        }


        if($data['from_completion_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("completion_date BETWEEN '".$data['from_completion_date']."' AND '".$data['to_completion_date']."'");
            $this->db->group_end();
        }


        if($data['from_delivery_date'] != null)
        {
            $this->db->group_start();
                $this->db->where("delivery_date BETWEEN '".$data['from_delivery_date']."' AND '".$data['to_delivery_date']."'");
            $this->db->group_end();
        }
        $this->db->order_by('construct_id ', 'ASC');
        $query = $this->db->get($this->table_construct);

        // print_r($this->db->get_compiled_select($this->table_ipazz)); die();
        return $query->result();
    }


    public function fetch_search_client($postData)
    {

        $氏名 = $postData['full_name'];
        $住所 = $postData['street'];
        $電話 = $postData['phone'];
        $土地_有無 = $postData['with_or_without'];
        $初回担当 = $postData['first_time_change'];
        $担当 = $postData['pic_person'];
        $date_insertmin = $postData['min'];
        $date_insertmax = $postData['max'];
        $DM = $postData['dm'];


        if(!empty($氏名))
        {
            $this->db->group_start();
                $this->db->like('氏名',$氏名);
            $this->db->group_end();
        }
        if(!empty($住所))
        {
            $this->db->group_start();
                $this->db->like('住所',$住所);
            $this->db->group_end();
        }
        if(!empty($電話))
        {
            $this->db->group_start();
                $this->db->like('電話',$電話);
            $this->db->group_end();
        }
        if(!empty($土地_有無))
        {
            $this->db->group_start();
                $this->db->like('土地　有無',$土地_有無);
            $this->db->group_end();
        }

        if(!empty($初回担当))
        {
            $this->db->group_start();
                $this->db->like('初回担当',$初回担当);
            $this->db->group_end();
        }

        if(!empty($担当))
        {
            $this->db->group_start();
                $this->db->like('担当',$担当);
            $this->db->group_end();
        }

        if(!empty($date_insertmin && $date_insertmax))
        {
            $this->db->group_start();
            $this->db->where("date_insert BETWEEN '".$date_insertmin."' AND '".$date_insertmax."'");
            $this->db->group_end();
        }

        if(!empty($DM))
        {
            $this->db->group_start();
                $this->db->like('ＤＭ',$DM);
            $this->db->group_end();
        }

        $query = $this->db->get($this->table_client);

        // print_r($this->db->get_compiled_select($this->table_client)); die();
        return $query->result();
    }


    public function get_project_category($code)
    {
        $this->db->select("category_name, category_id, color");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_category');
        return $query->result();
    }

    public function get_categoryitem($code){
        $this->db->select("id_item, item_name, category_id");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_categoryitem');
        return $query->result_array();
    }


    public function get_projectjoblist($code){
        $this->db->select("joblist_name, joblist_files, category_id, id_item");
        $this->db->where("category_id", $code);
        $query = $this->db->get('harada_project_joblist');
        return $query->result_array();
    }


    public function get_data_client($id){
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }



    public function input_new_client_data($data) {

        $this->db->where('氏名',$data['氏名']);
        $query = $this->db->get($this->table_ipazz);
        if ($query->num_rows() > 0){
            return false;
        }
        else
        {
            
            $whee = $this->db->query('SELECT gid FROM harada_client_ob ORDER BY gid DESC LIMIT 1')->result()[0];
            $dataz = [
                "gid"=>$whee->gid+1,
                "b"=>$data['b'],
                "l"=>$data['l'],
                "氏名"=>$data['氏名'],
                "連名"=>$data['連名'],
                "〒"=>$data['〒'],
                "birth_date"=>$data['birth_date'],
                "age"=>$data['age'],
                "都道府県"=>$data['都道府県'],
                "住　　　所"=>$data['住所'],
                "共同住宅"=>$data['共同住宅'],
                "電　　話"=>$data['電話'],
                "奥様名前" => $data['奥様名前'],
                "奥様名前_かな" => $data['奥様名前_かな'],
                "奥様生年月日" => $data['奥様生年月日'],
                "お子様1名前" => $data['お子様1名前'],
                "お子様1名前_かな" => $data['お子様1名前_かな'],
                "お子様1生年月日" => $data['お子様1生年月日'],
                "お子様2名前" => $data['お子様2名前'],
                "お子様2名前_かな" => $data['お子様2名前_かな'],
                "お子様2生年月日" => $data['お子様2生年月日'],
                "お子様3名前" => $data['お子様3名前'],
                "お子様3名前_かな" => $data['お子様3名前_かな'],
                "お子様3生年月日" => $data['お子様3生年月日'],
                "種別"=>$data['種別'],
                "初回接点"=>$data['初回接点'],
                "初回年月日"=>$data['初回年月日'],
                "営業担当"=>$data['営業担当'],
                "コーデ担当"=>$data['担当'],
                "土地　有無"=>$data['土地　有無'],
                "訪問"=>$data['訪問'],
                "ＤＭ"=>$data['ＤＭ'],
                "ランク"=>$data['ランク'],
                "備考"=>$data['備考'],
                "geom"=>$data['geom'],
                "氏名_かな"=>$data['氏名_かな'],
                "メールアドレス"=>$data['メールアドレス']
            ];

            return $this->db->insert($this->table_ipazz,$dataz);
            // return $dataz;
        }

    }


    public function check_no($data){
        $this->db->where('no',$data);
        $query = $this->db->get('harada_affiliate');
        if ($query->num_rows() > 0){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function view_file_project($ehe,$split)
    {
        $this->db->group_start();
            $this->db->like('co_owner1bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner2bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner3bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner4bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner5bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner6bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner7bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner8bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner9bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner10bluea ',$ehe['afiliator']);
            $this->db->or_like('co_owner1blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner2blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner3blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner4blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner5blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner6blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner7blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner8blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner9blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner10blueb ',$ehe['afiliator']);
            $this->db->or_like('co_owner1bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner2bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner3bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner4bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner5bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner6bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner7bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner8bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner9bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner10bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner1blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner2blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner3blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner4blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner5blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner6blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner7blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner8blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner9blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner10blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner1bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner2bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner3bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner4bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner5bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner6bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner7bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner8bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner9bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner10bluec ',$ehe['afiliator']);
            $this->db->or_like('co_owner1blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner2blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner3blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner4blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner5blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner6blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner7blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner8blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner9blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner10blued ',$ehe['afiliator']);
            $this->db->or_like('co_owner1bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner2bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner3bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner4bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner5bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner6bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner7bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner8bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner9bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner10bluee ',$ehe['afiliator']);
            $this->db->or_like('co_owner1bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner2bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner3bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner4bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner5bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner6bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner7bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner8bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner9bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner10bluef ',$ehe['afiliator']);
            $this->db->or_like('co_owner1blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner2blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner3blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner4blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner5blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner6blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner7blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner8blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner9blueg ',$ehe['afiliator']);
            $this->db->or_like('co_owner10blueg ',$ehe['afiliator']);
        $this->db->group_end();
        // $query = $this->db->get($this->table_construct);

        // return $query->result();

        // $this->db->where('cust_name', $ehe['afiliator']);
        $this->db->where('type_item', 'user');
        $this->db->where('construct_id', $split);

                // print_r($this->db->get_compiled_select($this->table_construct)); die();
        
        return $this->db->get($this->table_construct)->result();

        
    }


    function search_id($cust_code){
        $this->db->like('cust_code', $cust_code , 'both');
        $this->db->order_by('gid', 'ASC');
        $this->db->limit(10);
        return $this->db->get('harada_client_ob')->result();
    }
    

    public function get_project_list($id){
        // $this->db->where("const_id", $id);
        // $query = $this->db->get('harada_fileproject');

        $this->db->where('const_id', $id);
        // print_r($this->db->get_compiled_select('harada_fileproject')); die();
        return $this->db->get('harada_fileproject')->result();

        
        // return $query->result();
    }
    
    public function get_customer_name($id){
        $this->db->select("cust_name");
        $this->db->where("construct_id", $id);
        $query = $this->db->get('harada_construct');
        return $query->result()[0]->cust_name;
    }


    public function update_post($id,$table_name,$category)
    {
        $this->db->set($table_name, NULL);
        // $this->db->set($category, NULL);
        $this->db->where('construct_id', $id);
        return $this->db->update('harada_construct');
    }

    public function check_admin()
    {
        $this->db->select("access");
        $this->db->where("access", '1');
        $query = $this->db->get('harada_login_cms');
        return $query->num_rows();
    }


    public function check_name($data){
        $this->db->where('name',$data);
        $query = $this->db->get('harada_affiliate');
        if ($query->num_rows() > 0){
            return 'have';
        }
        else{
            return 'not';
        }
    }


}
?>