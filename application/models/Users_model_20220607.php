<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Users_model extends CI_Model{
    public $table = "harada_login_cms";

    public function check_username($username)
    {
        $this->db->where('username', $username);
        return $this->db->get($this->table)->result();
    }
}
?>