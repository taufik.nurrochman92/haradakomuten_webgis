<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Employee_master_model extends CI_Model{
    public $table = "employee_master";

    public function insert_employee_master($employee_master)
    {
        return $this->db->insert($this->table, $employee_master);
    }

    public function update_employee_master($employee_master, $employee_code)
    {
        $this->db->where('employee_code', $employee_code);
        return $this->db->update($this->table, $employee_master);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->get($this->table)->result();
    }

    public function delete_one($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->delete($this->table);
    }
}
?>