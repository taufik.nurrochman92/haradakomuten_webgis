<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Inspection_model extends CI_Model{
    public $table = "inspection_management";

    public function insert_inspection($inspection)
    {
        return $this->db->insert($this->table, $inspection);
    }

    public function update_inspection($inpection, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $inpection);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }
}
?>