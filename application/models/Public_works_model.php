<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Public_works_model extends CI_Model{
    public $table = "public_works";

    public function insert_public_works($public_works)
    {
        return $this->db->insert($this->table, $public_works);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function update_public_works($public_works, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $public_works);
    }
    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }
}
?>