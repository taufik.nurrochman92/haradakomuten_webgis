<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Customer_model extends CI_Model{
    public $table = "customer_management";
    public $table_cus = "customer_table";
    public $table_emplo = "employee_table";
    public $table_cont = "contractor_table";
    // public $table_ipazz = "harada_inspection_dupe";
    public $table_ipazz = "harada_client_ob";
    public $table_contractor = "harada_affiliate";
    public $table_employee = "harada_employee";
    public $table_client = "harada_client";


    // private $db;

    public function __construct()
    {
        // $this->db_b = $this->load->database('ipazz', TRUE); 
    }

    public function insert_customer($customer)
    {
        return $this->db->insert($this->table, $customer);
    }

    public function insert_customers($customer)
    {
        return $this->db->insert($this->table_cus, $customer);
    }

    public function insert_customers_new($customer)
    {
        return $this->db->insert($this->table_ipazz, $customer);
    }

    public function insert_employees($customer)
    {
        return $this->db->insert($this->table_emplo, $customer);
    }

    public function insert_employees_new($customer)
    {
        return $this->db->insert($this->table_employee, $customer);
    }

    public function insert_contractor($customer)
    {
        return $this->db->insert($this->table_cont, $customer);
    }

    public function insert_contractor_new($customer)
    {
        return $this->db->insert($this->table_contractor, $customer);
    }

    public function insert_client_new($customer)
    {
        return $this->db->insert($this->table_client, $customer);
    }



    public function update_customer($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table, $customer);
    }


    public function update_customers($customer, $gid)
    {
        $this->db->where('customer_code', $gid);
        return $this->db->update($this->table_cus, $customer);
    }

    public function update_customers_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_ipazz, $customer);
    }
    
    public function update_employees($customer, $gid)
    {
        $this->db->where('employee_code', $gid);
        return $this->db->update($this->table_emplo, $customer);
    }

    public function update_employees_new($customer, $gid)
    {
        $this->db->where('社員コード', $gid);
        return $this->db->update($this->table_employee, $customer);
    }
    
    public function update_contractor($customer, $gid)
    {
        $this->db->where('contractor_code', $gid);
        return $this->db->update($this->table_cont, $customer);
    }

        
    public function update_contractor_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_contractor, $customer);
    }

    public function update_client_new($customer, $gid)
    {
        $this->db->where('gid', $gid);
        return $this->db->update($this->table_client, $customer);
    }


    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_all_client()
    {
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_all_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }


    public function fetch_all_customers()
    {
        return $this->db->get($this->table_cus)->result();
    }

    public function fetch_all_customers_new()
    {
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_all_employees()
    {
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_employees_names()
    {
        $query = $this->db->query('SELECT 姓, 名 FROM harada_employee');
        return $query->result();
    }

    public function fetch_finances()
    {
        $query = $this->db->query('SELECT id, 金融機関名 FROM harada_finance');
        return $query->result();
    }

    public function fetch_all_employees_new()
    {
        return $this->db->get($this->table_employee)->result();
    }
    
    public function fetch_all_contractor()
    {
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_all_contractor_new()
    {
        return $this->db->get($this->table_contractor)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table)->result();
    }

    public function fetch_one_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_client($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }

    public function fetch_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->get($this->table_cus)->result();
    }

    
    public function fetch_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_ipazz)->result();
    }

    public function fetch_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->get($this->table_emplo)->result();
    }

    public function fetch_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_client)->result();
    }


    public function fetch_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->get($this->table_employee)->result();
    }

    public function fetch_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->get($this->table_cont)->result();
    }

    public function fetch_one_contractor_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->get($this->table_contractor)->result();
    }

    public function get_last_id_inspect()
    {
        return $this->db->query('SELECT gid FROM harada_client_ob ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_client()
    {
        return $this->db->query('SELECT gid FROM harada_client ORDER BY gid DESC LIMIT 1')->result();
    }

    public function get_last_id_affiliate()
    {
        return $this->db->query('SELECT gid FROM harada_affiliate ORDER BY gid DESC LIMIT 1')->result();
    }

    // public function get_last_id_employee($id)
    // {
    //     return $this->db->query('SELECT gid FROM harada_inspection_dupe ORDER BY gid DESC LIMIT 1')->result();
    // }


    public function delete_one($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table);
    }

    
    public function delete_one_customers($id)
    {
        $this->db->where('customer_code', $id);
        return $this->db->delete($this->table_cus);
    }

    public function delete_one_client_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_client);
    }

    public function delete_one_customers_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_ipazz);
    }
    
    public function delete_one_employee($id)
    {
        $this->db->where('employee_code', $id);
        return $this->db->delete($this->table_emplo);
    }

    public function delete_one_employee_new($id)
    {
        $this->db->where('社員コード', $id);
        return $this->db->delete($this->table_employee);
    }

    public function delete_one_contractor($id)
    {
        $this->db->where('contractor_code', $id);
        return $this->db->delete($this->table_cont);
    }

    public function delete_one_contractor_new($id)
    {
        $this->db->where('gid', $id);
        return $this->db->delete($this->table_contractor);
    }
}
?>