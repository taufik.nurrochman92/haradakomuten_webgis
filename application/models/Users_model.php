<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Users_model extends CI_Model{
    public $table = "harada_login_cms";
    public $table_user = "harada_affiliate_login_cms";
    public $table_project = "harada_fileproject";

    public function check_username($username)
    {
        $this->db->where('username', $username);
        return $this->db->get($this->table)->result();
    }

    public function check_username_affiliate($username)
    {
        $this->db->where('username', $username);
        return $this->db->get($this->table_user)->result();
    }

    public function insert_user_new($customer)
    {
        $this->db->where('username',$customer['username']);
        $query = $this->db->get($this->table_user);
        if ($query->num_rows() > 0){
            return false;
        }
        else{
            return $this->db->insert($this->table_user, $customer);
        }
        
    }


    public function fetch_all_user()
    {
        $this->db->where('access', '3');
        return $this->db->get($this->table_user)->result();
    }

    public function fetch_all_user_by($name)
    {
        $this->db->where('access', '3');
        $this->db->where('affilliate_from', $name->name);
        return $this->db->get($this->table_user)->result();

        // print_r($this->db->get_compiled_select($this->table_user)); die();
    }

    public function get_name_affiliate($id)
    {
        return $this->db->query("SELECT name FROM harada_affiliate WHERE id = '$id' ")->result();
    }

    public function fetch_one_user($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->table_user)->result();
    }

    public function fetch_one_user_by($data)
    {
        $this->db->where('affilliate_from', $data->name);
        $query = $this->db->get($this->table_user);
        if (($query->num_rows() != 0)) {
            return $query->result();
        } else {
            return FALSE;
        }
    }


    public function fetch_one_username($id)
    {
        $this->db->where('employee_id', $id);
        return $this->db->get($this->table)->result();
    }

    public function update_user($data,$id)
    {
        $this->db->where('id', $id);
        return $this->db->update($this->table_user, $data);
    }

    public function update_user_aff($data)
    {
        $this->db->where('username', $data["username"]);
        return $this->db->update($this->table_user, $data);
    }

    public function update_userpass($data)
    {
        if(empty($data['privilage']))
        {
            $ehehe = '2';
        }
        else
        {
            $ehehe = $data['privilage'];
        }
        // return $this->db->update($this->table_user, $data);
        $datas = array(
            'employee_id' => $data['社員コード'],
            'username' => $data['username'],
            'password' => $data['password'],
            'access' => $ehehe,
            'view_password' => $data['view_password']
        );

        return $this->db->replace('harada_login_cms',$datas);
        // print_r($this->db->error());
        // die();

    }

    public function insert_user($data)
    {
        // return $this->db->insert($this->table_user, $data);

        if(empty($data['privilage']))
        {
            $ehehe = '2';
        }
        else
        {
            $ehehe = $data['privilage'];
        }

        $data = array(
            'employee_id' => $data['社員コード'],
            'username' => $data['username'],
            'password' => $data['password'],
            'access' => $ehehe,
            'view_password' => $data['view_password']
        );


        $this->db->where('username',$data['username']);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0){
            return false;
        }
        else{
            return $this->db->insert($this->table,$data);
        }
        // if($this->db->insert($this->table,$data)){
        //     return true;
        // }
        // else
        // {
        //     // return false;
        //     //or
        //     print_r($this->db->error());
        //     die();
        // } 
    }

    public function delete_one_user($id)
    {
        $this->db->where('username', $id);
        return $this->db->delete($this->table_user);
    }


    public function insert_project($data)
    {
        $datas = array(
            'username' => $data['username'],
            'customer' => $data['customer'],
            'category_name' => $data['category_name'],
            'selection_name' => $data['selection_name'],
            'const_id' => $data['const_id'],
            'latlang1' => $data['latlng1'],
            'selection1' => $data['selection1'],
            'photo_images1' => $data['photo_images1'],
            'latlang2' => $data['latlang2'],
            'photo_images2' => $data['photo_images2'],
            'latlang3' => $data['latlang3'],
            'photo_images3' => $data['photo_images3'],
            'latlang4' => $data['latlang4'],
            'photo_images4' => $data['photo_images4']
        );
        
        // return $data;

        // $insert = $this->db->insert($this->table_project, $datas);

        if($this->db->insert($this->table_project,$datas)){
            return true;
        }
        else
        {
            // return false;
            //or
            print_r($this->db->error());
            die();
        } 
    }

}
?>