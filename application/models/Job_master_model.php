<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Job_master_model extends CI_Model{
    public $table = "job_master";

    public function insert_job_master($job_master)
    {
        return $this->db->insert($this->table, $job_master);
    }

    public function update_job_master($job_master, $job_code)
    {
        $this->db->where('job_code', $job_code);
        return $this->db->update($this->table, $job_master);
    }

    public function fetch_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function fetch_one($id)
    {
        $this->db->where('job_code', $id);
        return $this->db->get($this->table)->result();
    }

    public function delete_one($id)
    {
        $this->db->where('job_code', $id);
        return $this->db->delete($this->table);
    }
}
?>