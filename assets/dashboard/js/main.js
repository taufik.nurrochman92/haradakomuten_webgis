$(document).ready(function () {

	//nav menu
	$('.headerSpBtn').click(function () {
		$(this).toggleClass('_active');
		$('.navSp').toggleClass('_active');
		$('.floatBtn').toggleClass('_hide');
		$('body').toggleClass('noOverflow');
	});

	$('.navSp li a').click(function () {
		$('.headerSpBtn').removeClass('_active');
		$('.navSp').removeClass('_active');
		$('.floatBtn').removeClass('_hide');
		$('body').removeClass('noOverflow');
	});
	
	$('.navSp .hasChild').click(function () {
		$(this).find('.childMenu').slideToggle();
		$(this).find('a i').toggleClass('_rotate');
	});
		
});